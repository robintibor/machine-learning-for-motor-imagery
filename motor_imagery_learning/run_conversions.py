#!/usr/bin/env python
import os
import glob
import subprocess
import sys
script_template = """\
#!/bin/bash
#$ -o /home/schirrmr/motor-imagery/data/jobs_out/ -e /home/schirrmr/motor-imagery/data/jobs_out/

### Transform all models under model folder to cpu models
cd ${HOME}/motor-imagery/code/motor_imagery_learning/
export PYTHONPATH=$PYTHONPATH:`pwd`/../

echo "Working directory is $PWD"

export GPU_ID=`cat ${HOME}/${JOB_ID}_${SGE_TASK_ID}_${JOB_NAME}`
echo HOME=$HOME
echo USER=$USER
echo JOB_ID=$JOB_ID
echo JOB_NAME=$JOB_NAME
echo HOSTNAME=$HOSTNAME
echo SGE_TASK_ID=$SGE_TASK_ID
echo GPU_ID=$GPU_ID
echo $CMD

export THEANO_FLAGS="floatX=float32,device=cpu,nvcc.fastmath=True"
echo THEANO_FLAGS=$THEANO_FLAGS

"""
if __name__ == "__main__":
    # set queue name to rz or tf
    # TODELAY: write 
    if (len(sys.argv) > 1):
        assert sys.argv[1] in ("rz", "tf"), ("Please supply either rz or tf "
            "as queue, e.g. ./run_conversions rz")
        queue_name = sys.argv[1]
    else:
        queue_name = 'tf'
    to_run_filenames = glob.glob('data/jobs/conversions/*.torun')
    if (len(to_run_filenames) > 0):
        # Collect filenames, first create filename for job from first file
        # just move  whcih can be deleted later
        new_job_name = to_run_filenames[0].split("/")[-1].split(".")[0]
        full_new_job_file_path = 'data/jobs/conversions/running/' +\
            new_job_name + ".pbs"
        job_string = script_template
        script_call = "./mypylearn2/gpu_pkl_to_cpu_pkl.py {:s} {:s}"
        for to_run_name in to_run_filenames:
            with open(to_run_name, 'r') as content_file:
                pkl_file_name = content_file.read().strip('\n')
                this_script_call = script_call.format(pkl_file_name,
                    pkl_file_name)
                job_string += this_script_call + "\n"
        with open(full_new_job_file_path, "w") as job_file:
            job_file.write(job_string)
        subprocess.call(["qsub -q meta_gpu-" + queue_name + ".q " + full_new_job_file_path], 
            shell=True)
        # now delete all torun files...
        for to_run_name in to_run_filenames:
            os.remove(to_run_name)
    else:
        print("no conversions to run!")
    