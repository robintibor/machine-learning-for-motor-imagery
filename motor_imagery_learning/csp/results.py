import numpy as np
from pylearn2.utils import serial
from copy import deepcopy

class TrainCSPResult(object):
    """ For storing a result"""
    def __init__(self, csp_trainer, experiment_parameters):
        self.multi_class = csp_trainer.multi_class
        self.templates = {}
        self.training_times = -1
        self.parameters = deepcopy(experiment_parameters)
        # Copy cleaning results
        self.rejected_chans = csp_trainer.rejected_chans
        self.rejected_trials = csp_trainer.rejected_trials
        self.clean_trials = csp_trainer.clean_trials
    
    def get_misclasses(self):
        return {
        'train': 
                np.array([ 1 -acc for acc in self.multi_class.train_accuracy]),
        'test': 
            np.array([ 1 -acc for acc in self.multi_class.test_accuracy])
        }

    def save(self, filename):
        serial.save(filename, self)
    

class TrainCSPModel(object):
    """ For storing a model. Warning can be quite big"""
    def __init__(self, csp_trainer):
        self.csp_trainer = csp_trainer
    
    def save(self, filename):
        if (hasattr(self.csp_trainer.binary_csp, 'cnt')):
            del self.csp_trainer.binary_csp.cnt
        del self.csp_trainer.bbci_set
        del self.csp_trainer.cnt
        serial.save(filename, self.csp_trainer)
