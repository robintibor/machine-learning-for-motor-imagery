#!/usr/bin/env python

import sys
import subprocess
import time

if __name__ == "__main__":
    if len(sys.argv) < 2 or len(sys.argv) > 4:
        print("Usage: ./train_scripts/train_final_eval.py configfilename [start] [stop] [step]")
    config_filename = sys.argv[1]
    start = 1
    stop = 144
    step = 1
    if len(sys.argv) > 2:
        start = int(sys.argv[2])
    if len(sys.argv) > 3:
        stop = int(sys.argv[3])
    if len(sys.argv) > 4:
        step = int(sys.argv[4])
        
    train_script_file = "./csp/train_on_cluster.py"
    
    for i_start in range(start,stop+1, step):
        if i_start > start:
            print("Sleeping 5 minutes until starting next experiment...")
            time.sleep(300)
        i_stop = min(i_start + step - 1, stop)
        command = "{:s} {:s} --start {:d} --stop {:d}".format(
            train_script_file, config_filename, i_start, i_stop)
        subprocess.call([command],shell=True)
        #print command
