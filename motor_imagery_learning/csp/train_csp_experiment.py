#!/usr/bin/env python
import argparse
from pylearn2.config import yaml_parse
import time
import logging
from pylearn2.utils.logger import (
    CustomStreamHandler, CustomFormatter)
import theano
from motor_imagery_learning.train_scripts.train_experiments import (
    ExperimentsRunner)
from motor_imagery_learning.train_scripts.train_with_params import (
    extract_yaml_parameters, process_parameters_in_string)
from motor_imagery_learning.csp.results import TrainCSPResult, TrainCSPModel

def process_yaml_string(train_template_string, command_line_train_parameters, 
                      parameter_list_name):
    """ Replace parameters in train template string by given parameters or
    by (default) parameters in parameter from given parameter list
    Difference to other one: do not adjust for early stop etc.
    """
    # Extract default parameters at start of file
    train_string, parameters = extract_yaml_parameters(train_template_string,
         parameter_list_name)
    # Merge default parameters with user parameters 
    # (user can override parameters)
    parameters.update(command_line_train_parameters)
    parsed_train_string = process_parameters_in_string(train_string, parameters)
    return parsed_train_string

def create_training_object_from_file(filename):
    train_string = open(filename).read()
    return create_training_object(train_string)

def create_training_object(yaml_training_string):
    train_dict = yaml_parse.load(yaml_training_string)
    return train_dict

class CSPExperimentsRunner(ExperimentsRunner):
    
    #TODELAY: imeplement test?
    def __init__(self, experiments_file_name,  
        template_file_name='configs/train/csp.yaml',
         start_id=None, stop_id=None, 
        quiet=False, dry_run=False, 
        command_line_params=[], 
        first_five_sets=False,
        test=False):
        # If we are running on gpu typically we should not run in parallel...
        assert not (theano.config.device.startswith('gpu') and parallel)
        self._experiments_file_name = experiments_file_name
        self._template_file_name =  template_file_name
        self._start_id = start_id
        self._stop_id = stop_id
        self._quiet = quiet
        self._dry_run = dry_run
        self._command_line_params = command_line_params
        self._logger = logging.getLogger(__name__)
        self._first_five_sets = first_five_sets
        self._test = test

    def _read_train_template(self):
        """ Read the template, probably csp.yaml """
        self.csp_template_str = open(self._template_file_name, 'r').read()
        
    def _create_save_folder_path(self):
        folder_path = self._yaml_obj['save_model_path']
        if self._first_five_sets:
            folder_path += "/first-5/"
        return folder_path

    def _run_all_experiments(self):
        if (self._quiet):
            logging.getLogger("pylearn2").setLevel(logging.WARNING)
            logging.getLogger("mypylearn2").setLevel(logging.WARNING)
        
        for i in range(self._get_start_id(),  self._get_stop_id() + 1):
            self._run_experiment(i)

    def _run_experiments_with_parameters(self, experiment_index, params):
        train_string = process_yaml_string(self.csp_template_str,
            command_line_train_parameters=params,
            parameter_list_name='default')
        self._save_train_string(train_string, experiment_index)
        self.run_training_sequential(experiment_index)

    def run_training_sequential(self, experiment_index):
        starttime = time.time()
        file_name = self._base_save_paths[experiment_index] + ".yaml"
        train_dict = create_training_object_from_file(file_name)
        csp_trainer = train_dict['train_obj']
        store_model = train_dict['store_model']
        result_save_path = self._get_result_save_path(experiment_index)
        model_save_path = self._get_model_save_path(experiment_index)
        
        csp_trainer.run()
        endtime = time.time()
        result = TrainCSPResult(csp_trainer.get_trainer(), 
            self._all_experiments[experiment_index])
        result.training_time = endtime -starttime
        result.save(result_save_path)
        if (store_model):
            model = TrainCSPModel(csp_trainer.get_trainer())
            model.save(model_save_path) 

def parse_command_line_arguments():
    #TODELAY: implement test
    parser = argparse.ArgumentParser(
        description="""Launch an experiment from a YAML experiment file.
        Example: ./train_experiments.py yaml-scripts/experiments.yaml """
    )
    parser.add_argument('experiments_file_name', action='store',
                        choices=None,
                        help='A YAML configuration file specifying the '
                             'experiment')
    parser.add_argument('--template_file_name', action='store',
                        default='configs/train/csp.yaml',
                        help='A YAML configuration file specifying the '
                             'template for all experiments.')
    parser.add_argument('--quiet', action="store_true",
        help="Run algorithm quietly without progress output")
    parser.add_argument('--dryrun', action="store_true",
        help="Only show parameters for experiment, don't train.")
    parser.add_argument('--params', nargs='*', default=[],
                        help='''Parameters to override default values/other values given in experiment file.
                        Supply it in the form parameter1=value1 parameters2=value2, ...''')
    parser.add_argument('--startid', type=int,
                        help='''Start with experiment at specified id....''')
    parser.add_argument('--stopid', type=int,
                        help='''Stop with experiment at specified id....''')
    parser.add_argument('--first5sets', action="store_true", dest="first_five_sets",
                        help='''Use only first 5 datasets.''')
    args = parser.parse_args()
    
    # dictionary values are given with = inbetween, parse them here by hand
    param_dict =  dict([param_and_value.split('=') 
                        for param_and_value in args.params])
    args.params = param_dict
    # transfer learning always implies early stop(?) 
    # TODELAY: check if this is true/necessary when refactoring
    if (args.startid is  not None):
        args.startid = args.startid - 1 # model ids printed are 1-based, python is zerobased
    if (args.stopid is  not None):
        args.stopid = args.stopid - 1 # model ids printed are 1-based, python is zerobased
    return args

def setup_logging():
    """ Set up a root logger so that other modules can use logging
    Adapted from scripts/train.py from pylearn"""
        
    root_logger = logging.getLogger()
    prefix = '%(asctime)s '
    formatter = CustomFormatter(prefix=prefix, only_from='pylearn2')
    handler = CustomStreamHandler(formatter=formatter)
    root_logger.addHandler(handler)
    root_logger.setLevel(logging.INFO)

def main():
    args = parse_command_line_arguments()
    setup_logging()
    experiments_runner = CSPExperimentsRunner(
         args.experiments_file_name, args.template_file_name,
          start_id = args.startid,
          stop_id = args.stopid,
          quiet=args.quiet,
          dry_run=args.dryrun,
          command_line_params = args.params,
          first_five_sets=args.first_five_sets)
    experiments_runner.run()
    
if __name__ == "__main__":
    main()
