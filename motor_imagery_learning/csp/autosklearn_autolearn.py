from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
import numpy as np
import os

from pylearn2.utils import serial
import autosklearn

from autolearn import shorten_dataset_name, load_features_labels

if __name__ == '__main__':
    parser = ArgumentParser(description="", prog="",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    # General Options
    parser.add_argument("--file", dest="i_file", type=int, required=True)
    parser.add_argument("--output", dest="output", required=True)
    args, unknown = parser.parse_known_args()

    assert 109 > args.i_file >= 91

    filename = '/home/eggenspk/metahome/eggenspk/BrainLinksBrainTools/Neurobot/CSP_AutoSklearn_07_10_2015/csp-standardized/' + str(args.i_file) + '.pkl'
    print ("Loading...%s" % filename)

    csp_trainer = serial.load(filename)
    print "Training {:20s}".format(shorten_dataset_name(csp_trainer.filename))
    X_train, y_train = load_features_labels(csp_trainer.binary_csp.train_feature_full_fold,
                                       csp_trainer.binary_csp.train_labels_full_fold)
    X_test, y_test = load_features_labels(csp_trainer.binary_csp.test_feature_full_fold,
                                       csp_trainer.binary_csp.test_labels_full_fold)

    tmp_folder = os.path.join(args.output, "file_%d" % args.i_file)
    clf = autosklearn.AutoSklearnClassifier(time_left_for_this_task=10800,
                                            initial_configurations_via_metalearning=0,
                                            ensemble_size=1,
                                            ensemble_nbest=1,
                                            per_run_time_limit=600,
                                            seed=1,
                                            ml_memory_limit=3000,
                                            tmp_folder=tmp_folder,
                                            output_folder=None)
    print "Start training...save output to %s" % tmp_folder
    clf.fit(X_train, y_train)
    old_acc = csp_trainer.multi_class.test_accuracy[0]
    new_acc = clf.score(X_test, y_test)
    print ("Master Thesis Accuracy: {:5.2f}%".format(old_acc * 100))
    print ("New Accuracy:           {:5.2f}%".format(new_acc * 100))

    #print("Master Thesis average(std): {:5.2f}% ({:5.2f}%)".format(
    #        np.mean(all_old_accs) * 100, np.std(all_old_accs) * 100))
    #print("New           average(std): {:5.2f}% ({:5.2f}%)".format(
    #        np.mean(all_new_accs) * 100, np.std(all_new_accs) * 100))