import inspect
import os
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentparentdir = os.path.dirname(os.path.dirname(currentdir))
os.sys.path.insert(0,parentparentdir) 
from motor_imagery_learning.train_scripts.train_experiments import ExperimentsRunner
from pylearn2.utils import serial
import numpy as np

def expect_results(filename, transfer_learning, results):
    experiments_runner = create_experiments_runner(filename, transfer_learning)
    experiments_runner.run()
    compare_results(experiments_runner, results)
    delete_results(experiments_runner._base_save_paths)
    
def compare_results(experiments_runner, expected_results):
    for i, path in enumerate(experiments_runner._base_save_paths):
        train_state = serial.load(path + '.pkl')
        result = expected_results[i]
        all_same = compare_result(train_state, result)
        if not all_same:
            assert False, "Mismatch in " + path

def compare_result(train_state, result):
    all_results_same = True
    for chan in result:
        actual_chans = train_state.model.monitor.channels
        actual = np.squeeze(actual_chans[chan].val_record)
        expected = result[chan]
        if (len(actual) != len(expected) or not
                np.allclose(actual, expected)):
            print("Mismatch between results:")
            print chan
            print("Expected, Actual")
            print expected
            print np.squeeze(actual).tolist()
            all_results_same = False
    return all_results_same
    
def delete_results(paths):
    for path in paths:
        os.remove(path + '.pkl')
        base_path, filenr = os.path.split(path)
        # recreate transfer file path... ending should be added with +
        # and not part of os path join to prevent slashes... I think :)
        transfer_file = os.path.join(base_path, 'pretrained', filenr) + '.pkl'
        if (os.path.isfile(transfer_file)):
            os.remove(transfer_file)

def create_experiments_runner(filename, transfer_learning):
    experiments_runner = ExperimentsRunner(
              filename,
             './configs/train/motor_eeg.yaml',
              cross_validation = False, 
              transfer_learning=transfer_learning,
              test=False,
              start_id = None,
              stop_id = None,
              quiet=True,
              dry_run=False, 
              parallel = False,
              early_stop=False,
              command_line_params = {},
              processes=12,
              sleep_between_processes=0,
              print_results=False,
              transform_models_to_cpu=False)
    return experiments_runner