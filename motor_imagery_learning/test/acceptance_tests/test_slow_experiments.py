#!/usr/bin/env python
# I don't know how to properly import python packages, so I do it like this ;)
# http://stackoverflow.com/a/9806045/1469195
import os
import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentparentdir = os.path.dirname(os.path.dirname(currentdir))
os.sys.path.insert(0,parentparentdir) 
from motor_imagery_learning.test.acceptance_tests.experiments import (
    expect_results)
import pytest

@pytest.skip
def test_slow_experiments():
    expect_results('./configs/experiments/test/test_conv_nets.yaml',
        transfer_learning=False, results=
        [
            {
                'train_y_misclass': [0.6, 0.5, 0.5, 0.5],
                'valid_y_misclass' : [0.8, 0.7, 0.7, 0.6],
                'test_y_misclass': [0.7, 0.6, 0.6, 0.6],
            },
        ])
    

if __name__ == '__main__':
    test_slow_experiments()
