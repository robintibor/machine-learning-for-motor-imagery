#!/usr/bin/env python
# I don't know how to properly import python packages, so I do it like this ;)
# http://stackoverflow.com/a/9806045/1469195
import os
import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentparentdir = os.path.dirname(os.path.dirname(currentdir))
os.sys.path.insert(0,parentparentdir) 
from motor_imagery_learning.test.acceptance_tests.experiments import (
    expect_results)
import pytest

@pytest.skip
def test_fast_experiments():
    expect_results('./configs/experiments/test/test_softmax_raw_data.yaml',
        transfer_learning=False, results=
        [
            {
                'train_y_misclass': [0.85, 0.0, 0.0, 0.0,0.0,0.0,0.0],
                'valid_y_misclass' : [0.7, 0.6, 0.6, 0.5, 0.0, 0.0, 0.0],
                'test_y_misclass': [0.7, 0.5, 0.5, 0.5, 0.6, 0.6, 0.6],
            },
        ])

    expect_results('./configs/experiments/test/test_softmax_faster.yaml',
        transfer_learning=False, results=
        [
            {
                'train_y_misclass': [0.7, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                'valid_y_misclass' : [0.6, 0.8, 0.8, 0.8, 0.8, 0.8, 0.0],
                'test_y_misclass': [0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8],
            },
            {
                'train_y_misclass': [0.8, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                'valid_y_misclass' : [0.8, 0.0, 0.2, 0.0, 0.4, 0.2, 0.0, 0.0, 0.0],
                'test_y_misclass': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.4, 0.4, 0.4],
            }
        ])
    
    """
    expect_results('./configs/experiments/test/test_csp_remake.yaml',
        transfer_learning=False, results=
        [
            {
                'train_y_misclass': [0.9, 0.7, 0.7, 0.7,0.7],
                'valid_y_misclass' : [2/3.0, 1/3.0, 1/3.0, 1/3.0, 1/3.0],
                'test_y_misclass': [2/3.0, 1, 1, 1, 1.0],
            },
        ])"""

if __name__ == '__main__':
    test_fast_experiments()
