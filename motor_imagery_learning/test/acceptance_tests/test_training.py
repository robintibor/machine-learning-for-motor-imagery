from motor_imagery_learning.bbci_pylearn_dataset import (
    BBCIPylearnCleanFFTDataset, compute_power_spectra, BBCISetCleaner,
    BBCISetSecondSetCleaner)
from motor_imagery_learning.mywyrm.processing import resample_cnt
from motor_imagery_learning.mypylearn2.learning_rule_adam import Adam
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from pylearn2.models.mlp import MLP, Softmax
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
import numpy as np
from pylearn2.termination_criteria import EpochCounter
from motor_imagery_learning.mypylearn2.dataset_splitters import DatasetSingleFoldSplitter, DatasetTwoFileSingleFoldSplitter
def test_single_file():
    train_set = BBCIPylearnCleanFFTDataset(filenames='data/BBCI-without-last-runs/BhNoMoSc1S001R01_ds10_1-12.BBCI.mat',
                                           cleaner = BBCISetCleaner(),
                                           transform_function_and_args=(compute_power_spectra, 
                                            {'divide_win_length': False, 'square_amplitude': False}),
                                       cnt_preprocessors=[(resample_cnt, {'newfs': 100})],
                                                          load_sensor_names=['C3', 'C4'])
    
    max_epochs_crit = EpochCounter(max_epochs=2)
    algorithm = SimilarBatchSizeSGD(learning_rule=Adam(), learning_rate=1, 
                                    batch_size=10,seed=np.uint64(hash('test_train_sgd')),
                                    termination_criterion=max_epochs_crit)
    
    softmax = Softmax(n_classes=4, layer_name='y', irange=0.01)
    mlp = MLP(layers=[softmax], seed= np.uint64(hash('test_train_mlp')))
    
    dataset_splitter = DatasetSingleFoldSplitter(dataset=train_set, num_folds=10, test_fold_nr=9)
    trainer = TrainEarlyStop(dataset_splitter, mlp, algorithm=algorithm, save_path=None, preprocessor=None, 
                             keep_best_lrule_params=False, 
                             best_model_channel='valid_y_misclass', fraction_epochs_after_stop=1.0)
    
    trainer.main_loop()
    train_misclass_expected = [0.76205754, 0.57073933, 0.72025692, 0.54180044, 0.40200272]
    valid_misclass_expected = [0.74025971, 0.59740263, 0.74025971, 0.59740263, 0.45454544]
    test_misclass_expected = [0.71428573, 0.58441561, 0.67532468, 0.5454545, 0.33766234]
    assert np.allclose(train_misclass_expected, trainer.model.monitor.channels['train_y_misclass'].val_record)
    assert np.allclose(valid_misclass_expected, trainer.model.monitor.channels['valid_y_misclass'].val_record)
    assert np.allclose(test_misclass_expected, trainer.model.monitor.channels['test_y_misclass'].val_record)
    
""" Not working for some strange reason i get different results
from copy pasted code from ipython notebook....
def test_two_files():
    
    first_set_cleaner = BBCISetCleaner()
    train_set = BBCIPylearnCleanFFTDataset(filenames='data/BBCI-without-last-runs/BhNoMoSc1S001R01_ds10_1-12.BBCI.mat',
                                           cleaner = first_set_cleaner,
                                           transform_function_and_args=(compute_power_spectra, 
                                            {'divide_win_length': False, 'square_amplitude': False}),
                                       cnt_preprocessors=[(resample_cnt, {'newfs': 100})],
                                                          load_sensor_names=['CP3', 'CP4', 'C3', 'C4','C1','C2','CP1', 'CP2',
                                                                            'CPP6h'])
    
    test_set = BBCIPylearnCleanFFTDataset(filenames='data/BBCI-without-last-runs/JoBeMoSc01S001R01_ds10_1-11.BBCI.mat',
                                          cleaner=BBCISetSecondSetCleaner(first_set_cleaner=first_set_cleaner),
                                           transform_function_and_args=(compute_power_spectra, 
                                            {'divide_win_length': False, 'square_amplitude': False}),
                                       cnt_preprocessors=[(resample_cnt, {'newfs': 100})],
                                                          load_sensor_names=['CP3', 'CP4', 'C3', 'C4','C1','C2','CP1', 'CP2',
                                                                            'CPP6h'])
    max_epochs_crit = EpochCounter(max_epochs=3)
    algorithm = SimilarBatchSizeSGD(learning_rule=Adam(), learning_rate=1, 
                                    batch_size=10,seed=np.uint64(hash('test_train_sgd')),
                                    termination_criterion=max_epochs_crit)
    
    softmax = Softmax(n_classes=4, layer_name='y', irange=0.01)
    mlp = MLP(layers=[softmax], seed= np.uint64(hash('test_train_mlp')))
    
    dataset_splitter = DatasetTwoFileSingleFoldSplitter(train_set = train_set, test_set=test_set, num_folds=10)
    trainer = TrainEarlyStop(dataset_splitter, mlp, algorithm=algorithm, save_path=None, preprocessor=None, 
                             keep_best_lrule_params=False, 
                             best_model_channel='valid_y_misclass', fraction_epochs_after_stop=1.0)
    trainer.main_loop()
    train_misclass_expected = [0.73390543, 0.68812579, 0.49213159, 0.46494988, 0.41917023, 0.44974226]
    valid_misclass_expected = [0.74025977, 0.6493507, 0.46753246, 0.50649351, 0.41558444, 0.41558444]
    test_misclass_expected = [0.72521234, 0.68271953, 0.75779033, 0.71246451, 0.73796016, 0.69546765]
    assert np.allclose(train_misclass_expected, trainer.model.monitor.channels['train_y_misclass'].val_record)
    assert np.allclose(valid_misclass_expected, trainer.model.monitor.channels['valid_y_misclass'].val_record)
    assert np.allclose(test_misclass_expected, trainer.model.monitor.channels['test_y_misclass'].val_record)
    """