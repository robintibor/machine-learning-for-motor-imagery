import scipy.io
from motor_imagery_learning.bbci_dataset import BBCIDataset
import os
import numpy as np
import pytest

"""Have to redo this for h5 file"""

@pytest.skip
def test_loading():
    # check that data is correctly transformed into epochs
    # Setup data
    raw_trial_1 = np.array([np.arange(400), np.arange(400,800)]).T # transpose to get samples x channels
    raw_trial_2 = np.array([np.arange(200,600), np.arange(300,700)]).T # transpose to get samples x channels

    cnt_signal = np.concatenate((raw_trial_1, raw_trial_2))
    event_times_ms = [0,4000]
    event_classes = [1,2]
    
    # structure determined by trial and error /
    # comparison to actual bbci.mat files :)
    mat_dict = {'dat': {'fs': 100, 'clab': [[['Fp1'], ['Fp2']]]},
                'ch1': cnt_signal[:,0],
                'ch2': cnt_signal[:,1],
               'mrk': {'time': [event_times_ms], 
                       'event': [[[np.array([event_classes,event_classes]).T]]]},
               }
    testfilename = 'test-to-delete.mat'
    scipy.io.savemat(testfilename, mat_dict, oned_as='column')
    # Load and compare
    bbci_set = BBCIDataset(testfilename)
    bbci_set.load()
    assert bbci_set.epo.data.shape == (2,400,2)
    assert np.array_equal(bbci_set.epo.data[0], raw_trial_1)
    assert np.array_equal(bbci_set.epo.data[1], raw_trial_2)
    os.unlink(testfilename)

def test_loading_specific_sensors():
    # check that data is correctly transformed into epochs
    # Setup data
    raw_trial_1 = np.array([np.arange(400), np.arange(400,800), 
        np.arange(800,1200)]).T # transpose to get samples x channels
    raw_trial_2 = np.array([np.arange(200,600), np.arange(300,700),
        np.arange(500,900)]).T # transpose to get samples x channels

    cnt_signal = np.concatenate((raw_trial_1, raw_trial_2))

    event_times_ms = [0,4000]
    event_classes = [1,2]

    mat_dict = {'dat': {'fs': 100, 'clab': [[['Fp1'], ['Fpz'], ['Fp2']]]},
                'ch1': cnt_signal[:,0],
                'ch2': cnt_signal[:,1],
                'ch3': cnt_signal[:,2],
               'mrk': {'time': [event_times_ms], 
                       'event': [[[np.array([event_classes,event_classes]).T]]]},
               }
    testfilename = 'test-to-delete.mat'
    scipy.io.savemat(testfilename, mat_dict, oned_as='column')
    # Load and compare
    bbci_set = BBCIDataset(testfilename, sensor_names=['Fp1', 'Fp2'])
    bbci_set.load()
    assert bbci_set.epo.data.shape == (2,400,2)
    assert np.array_equal(bbci_set.epo.data[0], raw_trial_1[:, [0,2]])
    assert np.array_equal(bbci_set.epo.data[1], raw_trial_2[:, [0,2]])
    os.unlink(testfilename)
    
if __name__ == "__main__":
    test_loading()
    test_loading_specific_sensors()