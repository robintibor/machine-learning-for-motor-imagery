from numpy.random import  RandomState
import numpy as np
from motor_imagery_learning.csp.pipeline import BinaryCSP
from motor_imagery_learning.analysis.data_generation import create_sine_signal
from wyrm.types import Data 
from motor_imagery_learning.csp.ival_optimizers import AucIntervalOptimizer

def test_binary_csp():
    """ Regression test, making sure that computation results stay same.
    Example data is having sines on one chan for one class, and nothing on all chans
     for the other class."""
    fs = 100
    samples_per_trial = 100
    
    fake_cnt_data = np.ones((6 * samples_per_trial, 2)) * np.nan
    
    sine_signal = create_sine_signal(samples=samples_per_trial, freq=5, sampling_freq=fs)
    
    # put sine signal on last three trials on first chan
    fake_cnt_data[3*samples_per_trial:4*samples_per_trial:,0] = sine_signal
    fake_cnt_data[4*samples_per_trial:5*samples_per_trial,0] = sine_signal
    fake_cnt_data[5*samples_per_trial:6*samples_per_trial:,0] = sine_signal
    
    # put zeros on first three  trials on first chan
    fake_cnt_data[0:1*samples_per_trial:,0] = 0
    fake_cnt_data[1*samples_per_trial:2*samples_per_trial:,0] = 0
    fake_cnt_data[2*samples_per_trial:3*samples_per_trial:,0] = 0
    
    # put zeros on second chan for all trials
    fake_cnt_data[:,1] = 0
    
    # put some noise on everything
    noise = RandomState(np.uint32(hash('fake_cnt'))).randn(fake_cnt_data.shape[0], 1)
    
    fake_cnt_data += noise * 0.2
    
    assert not np.any(np.isnan(fake_cnt_data))
    
    axes = [np.arange(0,6*1000,10), ['Sensor1', 'Sensor2']]
    
    
    artificial_cnt = Data(fake_cnt_data, axes=axes, names=['time', 'channel'], 
                          units=['ms', '#'])
    markers = [(0.0, 0), (1000.0, 0), (2000.0, 0), (3000.0, 1), (4000.0, 1), (5000.0, 1)]
    
    # make sure to have both classes in train and test
    fake_folds = [{'train': [0,1,3,4], 'test':[2,5]}]
    
    artificial_cnt.markers = markers
    artificial_cnt.fs = fs
    
    fake_class_pairs = [[0,1]]
    
    fake_ival = [0,1000.0]
    num_filters = 1
    ival_optimizer = AucIntervalOptimizer()
    binary_csp = BinaryCSP(
                artificial_cnt,
                [[3, 6]],
                3,
                fake_folds,
                fake_class_pairs,
                fake_ival,
                num_filters,
                ival_optimizer,
                marker_def={'0':[0], '1':[1]})
    binary_csp.run()
    assert np.array_equal([1,1,1], binary_csp.test_accuracy.shape)
    assert binary_csp.test_accuracy[0,0,0] == 1.0
    assert np.array_equal([1,1,1], binary_csp.train_accuracy.shape)
    assert binary_csp.train_accuracy[0,0,0] == 1.0
    # regression test
    assert np.allclose([[-0.00281766, -1.],
                       [1. ,  1.]],
                       binary_csp.filters[0,0,0]), (
       "regression test, should stay exactly the same unless some parts of computation changed")
    assert np.allclose([0.12347028, -1.],binary_csp.variances[0,0,0]), (
       "regression test, should stay exactly the same unless some parts of computation changed")