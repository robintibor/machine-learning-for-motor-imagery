from motor_imagery_learning.analysis.data_generation import spread_to_sensors
import numpy as np
from numpy.random import RandomState


def test_spread_to_sensors():
    """ Test simplest mixing -1 of one sensor +1 for the other sensor """
    rng = RandomState(np.uint64(hash('test_sensor_spread')))
    topo_view = rng.randn(33,2,5,1)
    mixing_matrix = [[-1,1], [1,-1]]
    test_topo_view = spread_to_sensors(topo_view, mixing_matrix)
    
    assert np.allclose(topo_view[:,1,:, 0] - topo_view[:,0,:, 0], 
        test_topo_view[:,0,:,0])
    assert np.allclose(topo_view[:,0,:, 0] - topo_view[:,1,:, 0], 
        test_topo_view[:,1,:,0])