#from motor_imagery_learning.bbci_dataset import clean_and_load_set, BBCIDataset
import numpy as np
import scipy.io
import os
import wyrm
from wyrm.processing import segment_dat
from motor_imagery_learning.mywyrm.clean import (compute_outlier_chans,
    compute_outlier_trials, compute_unstable_chans,
    compute_excessive_outlier_trials)
import pytest

"""Have to redo this for h5 file"""
@pytest.skip
def test_reject_channels():
    # Create three trials with each 400 samples, three channels
    raw_trial_1 = np.array([np.arange(400), np.arange(400,800), 
            np.arange(800,1200)]).T # transpose to get samples x channels
    raw_trial_2 = np.array([np.arange(200,600), np.arange(300,700),
        np.arange(500,900)]).T # transpose to get samples x channels
    raw_trial_3 = np.array([np.arange(500,900), np.arange(100,500),
        np.arange(350,750)]).T # transpose to get samples x channels
    
    cnt_signal = np.concatenate((raw_trial_1, raw_trial_2, raw_trial_3))
    
    event_times_ms = [0,4000,8000]
    event_classes = [1,2,1]
    
    mat_dict = {'dat': {'fs': 100, 'clab': [[['Fp1'], ['Fpz'], ['Fp2']]]},
                'ch1': cnt_signal[:,0],
                'ch2': cnt_signal[:,1],
                'ch3': cnt_signal[:,2],
               'mrk': {'time': [event_times_ms], 
                       'event': [[[np.array([event_classes,event_classes]).T]]]},
               }
    testfilename = 'test-to-delete.mat'
    scipy.io.savemat(testfilename, mat_dict, oned_as='column')
    # Load and compare
    bbci_set = BBCIDataset(testfilename, sensor_names=['Fp1', 'Fpz', 'Fp2'])
    bbci_set = clean_and_load_set(bbci_set, rejected_sensors=['Fpz'])
    os.unlink(testfilename)
    assert bbci_set.epo.data.shape == (3,400,2)
    assert np.array_equal(bbci_set.epo.data[0], raw_trial_1[:, [0,2]])
    assert np.array_equal(bbci_set.epo.data[1], raw_trial_2[:, [0,2]])
    assert np.array_equal(bbci_set.epo.data[2], raw_trial_3[:, [0,2]])
    assert np.all(bbci_set.sensor_names == bbci_set.epo.axes[-1])
    assert bbci_set.epo.markers == [(0,1), (4000,2), (8000,1)]
    
def test_reject_trials():
    # Create three trials with each 400 samples, three channels
    raw_trial_1 = np.array([np.arange(400), np.arange(400,800), 
            np.arange(800,1200)]).T # transpose to get samples x channels
    raw_trial_2 = np.array([np.arange(200,600), np.arange(300,700),
        np.arange(500,900)]).T # transpose to get samples x channels
    raw_trial_3 = np.array([np.arange(500,900), np.arange(100,500),
        np.arange(350,750)]).T # transpose to get samples x channels
    
    cnt_signal = np.concatenate((raw_trial_1, raw_trial_2, raw_trial_3))
    
    event_times_ms = [0,4000,8000]
    event_classes = [1,2,1]
    
    mat_dict = {'dat': {'fs': 100, 'clab': [[['Fp1'], ['Fpz'], ['Fp2']]]},
                'ch1': cnt_signal[:,0],
                'ch2': cnt_signal[:,1],
                'ch3': cnt_signal[:,2],
               'mrk': {'time': [event_times_ms], 
                       'event': [[[np.array([event_classes,event_classes]).T]]]},
               }
    testfilename = 'test-to-delete.mat'
    scipy.io.savemat(testfilename, mat_dict, oned_as='column')
    # Load and compare
    bbci_set = BBCIDataset(testfilename, sensor_names=['Fp1', 'Fpz', 'Fp2'])
    bbci_set = clean_and_load_set(bbci_set, rejected_trials=[1])
    os.unlink(testfilename)
    assert bbci_set.epo.data.shape == (2,400,3)
    assert np.array_equal(bbci_set.epo.data[0], raw_trial_1)
    assert np.array_equal(bbci_set.epo.data[1], raw_trial_3)
    assert bbci_set.epo.markers == [(0,1), (8000,1)]
    
def test_reject_trials_and_channels():
    # Create three trials with each 400 samples, three channels
    raw_trial_1 = np.array([np.arange(400), np.arange(400,800), 
            np.arange(800,1200)]).T # transpose to get samples x channels
    raw_trial_2 = np.array([np.arange(200,600), np.arange(300,700),
        np.arange(500,900)]).T # transpose to get samples x channels
    raw_trial_3 = np.array([np.arange(500,900), np.arange(100,500),
        np.arange(350,750)]).T # transpose to get samples x channels
    
    cnt_signal = np.concatenate((raw_trial_1, raw_trial_2, raw_trial_3))
    
    event_times_ms = [0,4000,8000]
    event_classes = [1,2,1]
    
    mat_dict = {'dat': {'fs': 100, 'clab': [[['Fp1'], ['Fpz'], ['Fp2']]]},
                'ch1': cnt_signal[:,0],
                'ch2': cnt_signal[:,1],
                'ch3': cnt_signal[:,2],
               'mrk': {'time': [event_times_ms], 
                       'event': [[[np.array([event_classes,event_classes]).T]]]},
               }
    testfilename = 'test-to-delete.mat'
    scipy.io.savemat(testfilename, mat_dict, oned_as='column')
    # Load and compare
    bbci_set = BBCIDataset(testfilename, sensor_names=['Fp1', 'Fpz', 'Fp2'])
    bbci_set = clean_and_load_set(bbci_set, rejected_trials=[1], rejected_sensors=['Fpz'])
    assert bbci_set.epo.data.shape == (2,400,2)
    assert np.array_equal(bbci_set.epo.data[0], raw_trial_1[:, [0,2]])
    assert np.array_equal(bbci_set.epo.data[1], raw_trial_3[:, [0,2]])
    assert np.all(bbci_set.sensor_names == bbci_set.epo.axes[-1])
    assert bbci_set.epo.markers == [(0,1), (8000,1)]
    os.unlink(testfilename)

def create_set(trials, sensor_names):
    cnt_signal = np.concatenate(trials)
    fs = 2
    timesteps_in_ms = range(0, 3000, 1000/fs)
    cnt = wyrm.types.Data(cnt_signal, 
                [timesteps_in_ms, sensor_names], ['time', 'channel'], 
                ['ms', '#'])
    cnt.fs = fs
    # always one event per second
    event_classes = [0] * len(trials)
    event_times_in_ms = np.arange(0, len(trials) * 1000, 1000)
    cnt.markers =  zip(event_times_in_ms, event_classes)
    epo = segment_dat(cnt, marker_def ={'1': [0], '2': [1]}, ival=[0,1000])
    return epo

def test_reject_max_min():
    # Create three trials with each 2 samples, always 400 units apart
    raw_trial_1 = np.array([[-200,200], [-150,250], [-300,100]]).T # transpose to get samples x channels
    raw_trial_2 = np.array([[-300,100], [0,400], [10,410]]).T # transpose to get samples x channels
    raw_trial_3 = np.array([[100,500], [1100,1500], [0,400]]).T # transpose to get samples x channels
    
    sensor_names = ['Fp1', 'Fpz', 'Fp2']

    cnt_signal = np.concatenate((raw_trial_1, raw_trial_2, raw_trial_3))
    fs = 2
    timesteps_in_ms = range(0, 3000, 1000/fs)
    cnt = wyrm.types.Data(cnt_signal, 
                [timesteps_in_ms, sensor_names], ['time', 'channel'], 
                ['ms', '#'])
    cnt.fs = fs
    event_classes = [0,1,0]
    event_times_in_ms = [0,1000,2000]
    cnt.markers =  zip(event_times_in_ms, event_classes)
    epo = segment_dat(cnt, marker_def ={'1': [0], '2': [1]}, ival=[0,1000])
    assert epo.data.shape == (3,2,3)
    from motor_imagery_learning.mywyrm.clean import compute_rejected_trials_max_min
    rejected_max_min = compute_rejected_trials_max_min(epo, max_min=500)
    assert len(rejected_max_min) == 0
    rejected_max_min = compute_rejected_trials_max_min(epo, max_min=399)
    assert np.array_equal(rejected_max_min, [0,1,2])
    
def test_reject_outlier_chans():
    # Create three trials with each 2 samples,
    # with variance 1 for channels 1, variance 4 for channel 2, variance 9 for channel 3
    raw_trial_1 = np.array([[-1,1], [-2,2], [-3,3]]).T
    raw_trial_2 = np.array([[-1,1], [-2,2], [-3,3]]).T
    raw_trial_3 = np.array([[-1,1], [-2,2], [-3,3]]).T
    sensor_names = ['Fp1', 'Fpz', 'Fp2']
    epo = create_set((raw_trial_1, raw_trial_2, raw_trial_3), sensor_names)
    assert epo.data.shape == (3,2,3)
    rejected_outlier_chans = compute_outlier_chans(np.var(epo.data, axis=1), whisker_percent=25, whisker_length=0)
    assert len(rejected_outlier_chans) == 0
    # whisker percent 26 => high percentrile 100-26=74% will put threhsold just below 9
    rejected_outlier_chans = compute_outlier_chans(np.var(epo.data, axis=1), whisker_percent=26, whisker_length=0)
    assert np.array_equal(rejected_outlier_chans, [2])
    rejected_outlier_chans = compute_outlier_chans(np.var(epo.data, axis=1), whisker_percent=26, whisker_length=1)
    assert len(rejected_outlier_chans) == 0
    # whisker percent 50 will put threshold at 4.0 and whisker length will not matter!!
    rejected_outlier_chans = compute_outlier_chans(np.var(epo.data, axis=1), whisker_percent=50, whisker_length=10)
    assert np.array_equal(rejected_outlier_chans, [2])
    
    
def test_reject_outlier_trials():
    # Create three trials with each 2 samples,with variances 1,9,4
    raw_trial_1 = np.array([[-1,1], [-1,1], [-1,1]]).T 
    raw_trial_2 = np.array([[-3,3], [-3,3], [-3,3]]).T
    raw_trial_3 = np.array([[-2,2], [-2,2], [-2,2]]).T
    
    sensor_names = ['Fp1', 'Fpz', 'Fp2']
    epo = create_set((raw_trial_1, raw_trial_2, raw_trial_3), sensor_names)
    assert epo.data.shape == (3,2,3)
    
    rejected_outlier_trials = compute_outlier_trials(np.var(epo.data, axis=1), 
        whisker_percent=25, whisker_length=0)
    assert len(rejected_outlier_trials) == 0
    # whisker percent 26 => high percentrile 100-26=74% will put threhsold just below 9
    rejected_outlier_trials = compute_outlier_trials(np.var(epo.data, axis=1), whisker_percent=26, whisker_length=0)
    assert np.array_equal(rejected_outlier_trials, [1])
    rejected_outlier_trials = compute_outlier_trials(np.var(epo.data, axis=1), whisker_percent=26, whisker_length=1)
    assert len(rejected_outlier_trials) == 0
    # whisker percent 50 will put threshold at 4.0 and whisker length will not matter!!
    rejected_outlier_trials = compute_outlier_trials(np.var(epo.data, axis=1), whisker_percent=50, whisker_length=10)
    assert np.array_equal(rejected_outlier_trials, [1])

def test_reject_unstable_chans():
    # Create three trials with each 2 samples,
    # with last channel having a varying variance
    raw_trial_1 = np.array([[-3,3], [-3,3], [-1,1]]).T
    raw_trial_2 = np.array([[-3,3], [-3,3], [-3,3]]).T
    raw_trial_3 = np.array([[-3,3], [-3,3], [-2,2]]).T
    
    sensor_names = ['Fp1', 'Fpz', 'Fp2']
    epo = create_set((raw_trial_1, raw_trial_2, raw_trial_3), sensor_names)
    assert epo.data.shape == (3,2,3)
    
    rejected_unstable_chans = compute_unstable_chans(np.var(epo.data, axis=1), 
        whisker_percent=0, whisker_length=0)
    assert len(rejected_unstable_chans) == 0
    rejected_unstable_chans = compute_unstable_chans(np.var(epo.data, axis=1), 
        whisker_percent=1, whisker_length=0)
    assert np.array_equal(rejected_unstable_chans, [2])

def test_reject_excessive_trials():
    # Create three trials with each 2 samples,
    # with last channel having a varying variance
    raw_trial_1 = np.array([[-1,1], [-1,1], [-1,1], [-1,1], [-1,1]]).T 
    raw_trial_2 = np.array([[-1,1], [-1,1], [-1,1], [-1,1], [-3,3]]).T 
    raw_trial_3 = np.array([[-1,1], [-1,1], [-1,1], [-1,1], [-1,1]]).T
    
    sensor_names = ['Fp1', 'Fpz', 'Fp2', 'C2', 'C4']
    epo = create_set((raw_trial_1, raw_trial_2, raw_trial_3), sensor_names)
    assert epo.data.shape == (3,2,5)
    
    # First only 20% of chans (1/5) with higher variance in trial 2,
    # just below threshold for excessive variance
    rejected_excessive_trials = compute_excessive_outlier_trials(np.var(epo.data, axis=1), 
        whisker_percent=50, whisker_length=0)
    assert len(rejected_excessive_trials) == 0
    
    # now 40% (2/5) so trial should get rejected
    raw_trial_2 = np.array([[-1,1], [-1,1], [-1,1], [-3,3], [-3,3]]).T 
    epo = create_set((raw_trial_1, raw_trial_2, raw_trial_3), sensor_names)
    rejected_excessive_trials = compute_excessive_outlier_trials(np.var(epo.data, axis=1), 
        whisker_percent=50, whisker_length=0)
    assert np.array_equal(rejected_excessive_trials, [1])