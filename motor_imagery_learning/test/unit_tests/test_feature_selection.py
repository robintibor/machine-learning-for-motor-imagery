from motor_imagery_learning.csp.feature_selection import (
    cond_prob_label_given_trial, cond_entropy_class_given_features,
    select_features)
import numpy as np

def test_cond_prob():
    labels = [0,1,0,1]
    # three features
    trials = np.array([ 
                        [2,0,0,1],
                        [0,1,0,1],
                        [1,2,0,0]]).T
    invcov_trials = np.linalg.inv(np.atleast_2d(np.cov(trials.T)))
    result = cond_prob_label_given_trial(0, trials[0], labels, trials, k=0.5,
        invcov=invcov_trials)
    assert np.allclose(0.99613876597618423, result)
    
    #not supplying invcov should lead to same result
    result = cond_prob_label_given_trial(0, trials[0], labels, trials, k=0.5)
    assert np.allclose(0.99613876597618423, result)
    
def test_cond_prob_different_labels():
    # should also work with non-0/1 labels
    labels = [1,3,1,3]
    # three features
    trials = np.array([
                        [2,0,0,1],
                        [0,1,0,1],
                        [1,2,0,0]]).T
    #not supplying invcov should lead to same result
    result = cond_prob_label_given_trial(1, trials[0], labels, trials, k=0.5)
    assert np.allclose(0.99613876597618423, result)
    
def test_cond_entropy_class_given_features(): 
    labels = [0,1,0,1]
    # three features
    trials = np.array([ 
                        [2,0,0,1],
                        [0,1,0,1],
                        [1,2,0,0]]).T
    result = cond_entropy_class_given_features(labels, trials, k=0.5)
    assert np.allclose(0.025309753253206044, result)

def test_cond_entropy_class_given_features_different_labels(): 
    labels = [1,3,1,3]
    # three features
    trials = np.array([ 
                        [2,0,0,1],
                        [0,1,0,1],
                        [1,2,0,0]]).T
    result = cond_entropy_class_given_features(labels, trials, k=0.5)
    assert np.allclose(0.025309753253206044, result)
    
def test_cond_entropy_class_given_features_excluding_trial(): 
    labels = [0,1,0,1]
    # three features
    trials = np.array([ 
                        [2,0,0,1],
                        [0,1,0,1],
                        [1,2,0,0]]).T
    result = cond_entropy_class_given_features(labels, trials, 
        exclude_trial_itself=True, k=0.5)
    assert np.allclose(0.63651416829481255, result)
    
    
def test_select_features():
    labels = [0,1,0,1]
    # three features
    trials = np.array([ 
                        [2,0,0,1],
                        [0,1,0,1],
                        [1,2,0,0]]).T
    result = select_features(labels,trials, num_features=1, k=0.5)
    assert np.array_equal([1], result)
    
    
