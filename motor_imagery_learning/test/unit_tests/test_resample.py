import numpy as np
from wyrm.types import Data
from motor_imagery_learning.mywyrm.processing import resample_epo
import scipy
import pytest

def test_resample(): 
    # Create dat with 2 channels both signals
    # just linearly going up, first from 0 to 400
    # second form 400 to 800
    # trials x samples x channels shapes
    raw_trial_1 = np.array([np.arange(400), np.arange(400,800)]).T # transpose to get samples x channels
    raw_trial_2 = np.array([np.arange(200,600), np.arange(300,700)]).T # transpose to get samples x channels
    raw_trials = np.array([raw_trial_1, raw_trial_2]);
    classes = [0,1]
    channels = ['ca1', 'ca2']
    time = np.linspace(0, 4000, 400, endpoint=False)
    fs = 100
    dat = Data(raw_trials, [classes, time, channels], ['classes', 'time', 'channels'], ['#', 'ms', '#'])
    dat.fs = fs
    resampled_data = scipy.signal.resample(dat.data,num=200,axis=1)
    newdat = resample_epo(dat, newfs=50)
    assert newdat.data.shape == (2,200,2)
    assert np.array_equal(resampled_data, newdat.data)
    assert newdat.fs == 50
    
def test_fails_on_cnt_data():
    with pytest.raises(AssertionError) as excinfo:
        raw_cnt_data = np.array([np.arange(400), np.arange(400,800)]).T # transpose to get samples x channels
        channels = ['ca1', 'ca2']
        time = np.linspace(0, 4000, 400, endpoint=False)
        fs = 100
        dat = Data(raw_cnt_data, [time, channels], ['time', 'channels'], ['ms', '#'])
        dat.fs = fs
        newdat = resample_epo(dat, newfs=50)
    assert excinfo.value.message == "Expect 3 dimensions/epoched data"
        
if __name__ == "__main__":
    test_resample()
    test_fails_on_cnt_data()