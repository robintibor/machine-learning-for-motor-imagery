from motor_imagery_learning.train_scripts.train_with_params import (
    process_yaml_string)
import re
""" (End-to-end) Test for replacement of variables in yaml string."""
template_string = """#STARTPARAMETERS
{
     default: &default {
        ### Parameters usable by hyperparameter optimization
        frequency_start: 2,
        frequency_stop: 50,
        sensor_names: ['CP3', 'CP4'],
        preprocessor: "*standardize",
        topo_view_channel: 'auto_clean_trials_500Hz_500ms_len_250ms_stride_ps',    
        dataset_type: "*fft_type",
        time_bin_start: !!null '',
        time_bin_stop: !!null '',
        cross_validation_start: !!python/none '',
        cross_validation_stop: !!python/none '',
        transfer_filenames: !!python/none '',
        transfer_preprocessor: !!python/none '',
        training_type: "*training_test_set",    
     },
}
#ENDPARAMETERS
{
    $training_start
    $training_stop
    $dataset_filename
    $frequency_start
    $frequency_stop
    $time_bin_start
    $time_bin_stop
    $sensor_names
    $dataset_type
    $topo_view_channel
    $preprocessor
    $training_type
    $transfer_filenames
    $transfer_preprocessor
}
"""


def test_yaml_creation():
    expected_train_string = """
{
    !!python/none ''
    !!python/none ''
    ./data/motor-imagery-tonio/AnWeMoSc1S001R01_ds10_1-12_autoclean_trials_ival_0_4000.mat
    2
    50
    !!python/none ''
    !!python/none ''
    ['CP3', 'CP4']
    *fft_type
    auto_clean_trials_500Hz_500ms_len_250ms_stride_ps
    *standardize
    *training_cross_validation_early_stop
    !!python/none ''
    !!python/none ''
}
"""
    # for empty dict expect keyerror as dataset filename is missing
    train_string = process_yaml_string(template_string, {'dataset_filename':
        './data/motor-imagery-tonio/AnWeMoSc1S001R01_ds10_1-12_autoclean_trials_ival_0_4000.mat'},
        'default', cross_validation=True, early_stop=True, transfer_learning=False)
    # replace backslashes + quote in text (remove backslash)
    # somehow this works...
    train_string = re.sub(r'''(.)['"]''', r"\1'", train_string)
    print train_string[0:600]
    assert train_string == expected_train_string

if __name__=='__main__':
    test_yaml_creation()

