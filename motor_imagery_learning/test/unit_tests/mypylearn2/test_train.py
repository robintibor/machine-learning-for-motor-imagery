from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from pylearn2.models.mlp import MLP, Softmax
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
from numpy.random import RandomState
import numpy as np
from pylearn2.format.target_format import OneHotFormatter
from pylearn2.termination_criteria import EpochCounter
from motor_imagery_learning.mypylearn2.dataset_splitters import DatasetSingleFoldSplitter, DatasetTwoFileSingleFoldSplitter
from motor_imagery_learning.mypylearn2.learning_rule_adam import Momentum

def test_train_split():
    input_shape = (13,5,2,3)
    
    max_epochs_crit = EpochCounter(max_epochs=1)
    learning_rule = Momentum(0.1)
    algorithm = SimilarBatchSizeSGD(learning_rate=0.1,
        learning_rule=learning_rule, batch_size=10,seed=np.uint64(hash('test_train_sgd')),
        termination_criterion=max_epochs_crit)
    
    softmax = Softmax(n_classes=2, layer_name='y', irange=0.01)
    mlp = MLP(layers=[softmax], seed= np.uint64(hash('test_train_mlp')))
    
    rng = RandomState(np.uint64(hash('test_dataset')))
    topo_view = rng.randn(*input_shape)
    
    y = OneHotFormatter(2).format(rng.random_integers(0,1,size=input_shape[0]))
    dataset = DenseDesignMatrix(topo_view=topo_view, y=y, rng=rng)
    dataset_splitter = DatasetSingleFoldSplitter(dataset, num_folds=10, test_fold_nr=-1)
    trainer = TrainEarlyStop(dataset_splitter, mlp, algorithm=algorithm, save_path=None, preprocessor=None, 
                         keep_best_lrule_params=True, 
                         best_model_channel='valid_y_misclass', fraction_epochs_after_stop=1.0)
    
    trainer.main_loop()
    train_misclass_expected = [0.36363637, 0.0, 0.0, 0.083333336, 0.083333336, 0.083333336]
    valid_misclass_expected = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
    test_misclass_expected = [1.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    assert np.allclose(train_misclass_expected, trainer.model.monitor.channels['train_y_misclass'].val_record)
    assert np.array_equal(valid_misclass_expected, trainer.model.monitor.channels['valid_y_misclass'].val_record)
    assert np.array_equal(test_misclass_expected, trainer.model.monitor.channels['test_y_misclass'].val_record)
    
    
def test_two_file_split():
    input_shape = (13,5,2,3)
    max_epochs_crit = EpochCounter(max_epochs=1)
    learning_rule = Momentum(0.1)
    algorithm = SimilarBatchSizeSGD(learning_rate=0.1,
        learning_rule=learning_rule, batch_size=10,seed=np.uint64(hash('test_train_sgd')),
        termination_criterion=max_epochs_crit)
    
    softmax = Softmax(n_classes=2, layer_name='y', irange=0.01)
    mlp = MLP(layers=[softmax], seed= np.uint64(hash('test_train_mlp')))
    
    rng = RandomState(np.uint64(hash('test_dataset')))
    topo_view = rng.randn(*input_shape)
    
    y = OneHotFormatter(2).format(rng.random_integers(0,1,size=input_shape[0]))
    train_set = DenseDesignMatrix(topo_view=topo_view, y=y, rng=rng)
    
    test_topo_view = rng.randn(*input_shape)
    test_y = OneHotFormatter(2).format(rng.random_integers(0,1,size=input_shape[0]))
    test_set = DenseDesignMatrix(topo_view=test_topo_view, y=test_y, rng=rng)
    
    dataset_splitter = DatasetTwoFileSingleFoldSplitter(train_set = train_set, test_set=test_set, num_folds=10)
    trainer = TrainEarlyStop(dataset_splitter, mlp, algorithm=algorithm, save_path=None, preprocessor=None, 
                         keep_best_lrule_params=True, 
                         best_model_channel='valid_y_misclass', fraction_epochs_after_stop=1.0)
    
    trainer.main_loop()
    train_misclass_expected = [0.41666669, 0.083333336, 0.0, 0.0]
    valid_misclass_expected = [1.0, 0.0, 0.0, 0.0]
    test_misclass_expected = [0.46153846, 0.23076925, 0.23076925, 0.23076925]
    assert np.allclose(train_misclass_expected, trainer.model.monitor.channels['train_y_misclass'].val_record)
    assert np.array_equal(valid_misclass_expected, trainer.model.monitor.channels['valid_y_misclass'].val_record)
    assert np.allclose(test_misclass_expected, trainer.model.monitor.channels['test_y_misclass'].val_record)
    
    
def test_two_file_split_train_test_same_set():
    input_shape = (13,5,2,3)
    max_epochs_crit = EpochCounter(max_epochs=1)
    learning_rule = Momentum(0.1)
    algorithm = SimilarBatchSizeSGD(learning_rate=0.1,
        learning_rule=learning_rule, batch_size=10,seed=np.uint64(hash('test_train_sgd')),
        termination_criterion=max_epochs_crit)
    
    softmax = Softmax(n_classes=2, layer_name='y', irange=0.01)
    mlp = MLP(layers=[softmax], seed= np.uint64(hash('test_train_mlp')))
    
    rng = RandomState(np.uint64(hash('test_dataset')))
    topo_view = rng.randn(*input_shape)
    
    y = OneHotFormatter(2).format(rng.random_integers(0,1,size=input_shape[0]))
    train_set = DenseDesignMatrix(topo_view=topo_view, y=y, rng=rng)
    
    dataset_splitter = DatasetTwoFileSingleFoldSplitter(train_set=train_set, test_set=train_set, num_folds=10)
    trainer = TrainEarlyStop(dataset_splitter, mlp, algorithm=algorithm, save_path=None, preprocessor=None, 
                         keep_best_lrule_params=True, 
                         best_model_channel='valid_y_misclass', fraction_epochs_after_stop=1.0)
    
    trainer.main_loop()
   
    train_misclass_expected = [0.41666669, 0.083333336, 0.0, 0.0]
    valid_misclass_expected = [1.0, 0.0, 0.0, 0.0]
    test_misclass_expected = [0.46153849, 0.07692308, 0.0, 0.0]
    assert np.allclose(train_misclass_expected, trainer.model.monitor.channels['train_y_misclass'].val_record)
    assert np.array_equal(valid_misclass_expected, trainer.model.monitor.channels['valid_y_misclass'].val_record)
    assert np.allclose(test_misclass_expected, trainer.model.monitor.channels['test_y_misclass'].val_record)
    