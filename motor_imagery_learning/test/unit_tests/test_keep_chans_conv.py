import numpy as np
import theano.tensor as T
import theano
from motor_imagery_learning.mypylearn2.conv_channel_first import convolve_keeping_chans
import pytest

# its quite slow...
@pytest.skip
def test_chans_keep_conv():
    test_inputs = np.array([[[1,-1,2], [2,1,3]], [[2,1,3], [1,-1,2]]])
    test_inputs = test_inputs[:,:,:,np.newaxis] # expand to four dims, height is empty dim
    
    test_spatial_filters = [[1,-1], [-1,1]]
    test_filtered_result = np.dot(test_spatial_filters, test_inputs.T)
    
    test_filtered_result = test_filtered_result.transpose(3,0,2,1).astype('float32')
    assert np.array_equal(
          [[[[-1],
             [-2],
             [-1]],
    
            [[ 1],
             [ 2],
             [ 1]]],
           [[[ 1],
             [ 2],
             [ 1]],
    
            [[-1],
             [-2],
             [-1]]]]
            ,test_filtered_result)
    
    test_kernel_weights = [[-2, 1], [3, 0]]
    test_kernel_weights = np.array(test_kernel_weights, dtype='float32')[:, np.newaxis, :, np.newaxis]
    inputs = T.ftensor4()
    filters = T.ftensor4() # filter x 1, inputshape2, inputshape3
    result = convolve_keeping_chans(inputs, filters, len(test_spatial_filters),
        subsample=(1,1), border_mode='valid')
    test_convolve_func = theano.function([inputs, filters], result)
    test_result = test_convolve_func(test_filtered_result, test_kernel_weights)
    assert np.array_equal( # remember that filters are flipped
          [[[[ 3.],
             [ 0.]],
    
            [[ 6.],
             [ 3.]]],
    
    
           [[[-3.],
             [ 0.]],
    
            [[-6.],
             [-3.]]]], test_result)