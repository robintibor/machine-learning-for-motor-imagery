import scipy.io
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanDataset
import os
import numpy as np
import pytest
"""Have to redo this for h5 file"""
@pytest.skip
def test_loading():
    # check that data is correctly transformed into epochs
    # Setup data
    raw_trial_1 = np.array([np.arange(10), np.arange(10,20)]).T # transpose to get samples x channels
    raw_trial_2 = np.array([np.arange(20,30), np.arange(30,40)]).T # transpose to get samples x channels
    raw_trial_3 = np.array([np.arange(25,35), np.arange(15,25)]).T # transpose to get samples x channels

    cnt_signal = np.concatenate((raw_trial_1, raw_trial_2, raw_trial_3))
    event_times_ms = [0,1000,2000]
    event_classes = [1,2,1]
    
    # structure determined by trial and error /
    # comparison to actual bbci.mat files :)
    mat_dict = {'dat': {'fs': 10, 'clab': [[['Fp1'], ['Fp2']]]},
                'ch1': cnt_signal[:,0],
                'ch2': cnt_signal[:,1],
               'mrk': {'time': [event_times_ms], 
                       'event': [[[np.array([event_classes,event_classes]).T]]]},
               }
    testfilename = 'test-to-delete.mat'
    scipy.io.savemat(testfilename, mat_dict, oned_as='column')
    # Load and compare
    bbci_set = BBCIPylearnCleanDataset(testfilename, segment_ival=[0,1000],
         whisker_length=0, whisker_percent=0, max_min=float('inf'))
    bbci_set.load()
    topo_view = bbci_set.get_topological_view()
    assert topo_view.shape == (3,2,10,1)
    # ignore empty last dimension 
    topo_view = topo_view[:,:,:,0]
    assert np.array_equal(topo_view[0], raw_trial_1.T)
    assert np.array_equal(topo_view[1], raw_trial_2.T)
    assert np.array_equal(topo_view[2], raw_trial_3.T)
    os.unlink(testfilename)

def test_loading_start_stop():
    # check that data is correctly transformed into epochs
    # Setup data
    raw_trial_1 = np.array([np.arange(10), np.arange(10,20)]).T # transpose to get samples x channels
    raw_trial_2 = np.array([np.arange(20,30), np.arange(30,40)]).T # transpose to get samples x channels
    raw_trial_3 = np.array([np.arange(25,35), np.arange(15,25)]).T # transpose to get samples x channels

    cnt_signal = np.concatenate((raw_trial_1, raw_trial_2, raw_trial_3))
    event_times_ms = [0,1000,2000]
    event_classes = [1,2,1]
    
    # structure determined by trial and error /
    # comparison to actual bbci.mat files :)
    mat_dict = {'dat': {'fs': 10, 'clab': [[['Fp1'], ['Fp2']]]},
                'ch1': cnt_signal[:,0],
                'ch2': cnt_signal[:,1],
               'mrk': {'time': [event_times_ms], 
                       'event': [[[np.array([event_classes,event_classes]).T]]]},
               }
    testfilename = 'test-to-delete.mat'
    scipy.io.savemat(testfilename, mat_dict, oned_as='column')
    # Load and compare
    bbci_set = BBCIPylearnCleanDataset(testfilename, segment_ival=[0,1000],
        start=0, stop=2,  whisker_length=0, whisker_percent=0,
        maxmin=float('inf'))
    bbci_set.load()
    topo_view = bbci_set.get_topological_view()
    assert topo_view.shape == (2,2,10,1)
    # ignore empty last dimension 
    topo_view = topo_view[:,:,:,0]
    assert np.array_equal(topo_view[0], raw_trial_1.T)
    assert np.array_equal(topo_view[1], raw_trial_2.T)
    os.unlink(testfilename)
    
    
def test_loading_limits():
    # check that data is correctly transformed into epochs
    # Setup data
    raw_trial_1 = np.array([np.arange(10), np.arange(10,20)]).T # transpose to get samples x channels
    raw_trial_2 = np.array([np.arange(20,30), np.arange(30,40)]).T # transpose to get samples x channels
    raw_trial_3 = np.array([np.arange(25,35), np.arange(15,25)]).T # transpose to get samples x channels

    cnt_signal = np.concatenate((raw_trial_1, raw_trial_2, raw_trial_3))
    event_times_ms = [0,1000,2000]
    event_classes = [1,2,1]
    
    # structure determined by trial and error /
    # comparison to actual bbci.mat files :)
    mat_dict = {'dat': {'fs': 10, 'clab': [[['Fp1'], ['Fp2']]]},
                'ch1': cnt_signal[:,0],
                'ch2': cnt_signal[:,1],
               'mrk': {'time': [event_times_ms], 
                       'event': [[[np.array([event_classes,event_classes]).T]]]},
               }
    testfilename = 'test-to-delete.mat'
    scipy.io.savemat(testfilename, mat_dict, oned_as='column')
    # Load, leave out trial 2 and compare
    bbci_set = BBCIPylearnCleanDataset(testfilename, segment_ival=[0,1000],
        limits=[[0,1],[2,3]], whisker_length=0, whisker_percent=0,
        maxmin=float('inf'))
    bbci_set.load()
    topo_view = bbci_set.get_topological_view()
    assert topo_view.shape == (2,2,10,1)
    # ignore empty last dimension 
    topo_view = topo_view[:,:,:,0]
    assert np.array_equal(topo_view[0], raw_trial_1.T)
    # Trial 2 should be left out!
    assert np.array_equal(topo_view[1], raw_trial_3.T)
    os.unlink(testfilename)