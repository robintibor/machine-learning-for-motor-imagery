from motor_imagery_learning.preprocessing_util import (
    exponential_running_mean, exponential_running_var)
import numpy as np

def test_exponential_running_mean_var_with_init_block():
    data = [0,1,0,2]
    running_mean = exponential_running_mean(data, factor_new=0.1,
        init_block_size=2)
    print running_mean
    assert running_mean[0] == 0.5 # part of init block
    assert running_mean[1] == 0.5 # part of init block
    assert running_mean[2] == 0.45 # running_mean[1] * 0.9 + 0.1*0
    assert running_mean[3] == 0.605 # running_mean[2] * 0.9 + 0.1*2
    
    running_var = exponential_running_var(data, running_mean, factor_new=0.1,
        init_block_size=2)
    assert running_var[0] == 0.25
    assert running_var[1] == 0.25
    assert np.allclose(running_var[2], 0.24525) # running_var[1] * 0.9 + 0.1 * (0 - 0.45)**2
    assert np.allclose(running_var[3], 0.4153275) # running_var[2] * 0.9 + 0.1 * (2 - 0.605)**2
    
    
def test_exponential_running_mean_var_with_start_values():
    data = [0,1,0,2]
    running_mean = exponential_running_mean(data, factor_new=0.1, start_mean=1)
    assert running_mean[0] == 0.9 #0.9*1 + 0.1*0
    assert running_mean[1] == 0.91  # running_mean[0] * 0.9 + 0.1*1
    assert np.allclose(running_mean[2], 0.819) # running_mean[1] * 0.9 + 0.1*0
    assert np.allclose(running_mean[3], 0.9371) # running_mean[2] * 0.9 + 0.1*2
    
    running_var = exponential_running_var(data, running_mean, factor_new=0.1,
        start_var=1)
    assert np.allclose(running_var[0], 0.981) # 1*0.9 + 0.1*(0-0.9)**2
    assert np.allclose(running_var[1], 0.88371) # running_var[0] * 0.9 + 0.1 * (1 - 0.91)**2 
    assert np.allclose(running_var[2], 0.8624151) # running_var[1] * 0.9 + 0.1 * (0 - 0.819)**2 
    assert np.allclose(running_var[3], 0.889149231) # running_var[2] * 0.9 + 0.1 * (2 - 0.9371)**2 

def test_exponential_running_mean_var_with_axis():
    running_mean = exponential_running_mean(np.ones((3,4,5)), 
        factor_new=1, start_mean=1,axis=(1))
    assert running_mean.shape == (3,5)
    running_var = exponential_running_var(np.ones((3,4,5)), running_mean, 
        factor_new=0.9, start_var=1, axis=(1))
    assert running_var.shape == (3,5)
    
def test_exponential_mean_axis_as_int():
    running_means = exponential_running_mean(np.ones((5,10)), factor_new=0.1, init_block_size=2, axis=1)
    assert running_means.shape == (5,)