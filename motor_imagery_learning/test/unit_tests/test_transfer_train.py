# from pylearn2.models.mlp import MLP, Softmax
# from pylearn2.space import Conv2DSpace 
# import numpy as np
# from pylearn2.training_algorithms.sgd import SGD
# from pylearn2.termination_criteria import EpochCounter
# from motor_imagery_learning.mypylearn2.train_transfer import TrainTransfer
# from motor_imagery_learning.mypylearn2.preprocessing import NoPreprocessing
# from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
# 
# def test_transfer_train():
#     # create mlp
#     input_shape = (10,4,5,3)
#     inputs = np.random.normal(size=input_shape)
#     inputs_valid = np.random.normal(size=input_shape)
#     inputs_test = np.random.normal(size=input_shape)
#     targets = np.ones((input_shape[0], 2))
#     targets[::2, 0] = 0
#     targets[1::2, 1] = 0
#     
#     train_set = DenseDesignMatrix(topo_view=inputs, y=targets, 
#         axes=('b', 0, 1, 'c'))
#     valid_set = DenseDesignMatrix(topo_view=inputs_valid, y=targets, 
#         axes=('b', 0, 1, 'c'))
#     test_set = DenseDesignMatrix(topo_view=inputs_test, y=targets, 
#         axes=('b', 0, 1, 'c'))
#     
#     input_space = Conv2DSpace(inputs.shape[1:3], 
#         num_channels=inputs.shape[3], axes=('b', 0, 1, 'c'))
#     layers = [Softmax(n_classes=2, layer_name='y', irange=0.001)]
#     mlp = MLP(input_space=input_space, layers=layers)
#     weights_before = np.copy(mlp.get_weights_topo())
#     
#     sgd = SGD(learning_rate=0.1, termination_criterion=EpochCounter(max_epochs=3),
#         batch_size=1)
#     datasets = {'train': train_set, 'valid': valid_set, 'test': test_set}
#     trainer= TrainTransfer(datasets, mlp, sgd, preprocessor=NoPreprocessing(), 
#         save_path=None, transfer_learning_rate=0.01, inner_folds=3)
#     trainer.main_loop()
#     
#         
#     """
#     model: &model !obj:pylearn2.models.mlp.MLP {
#         input_space: $input_space,
#         layers: $layers,
#         seed: [2013, 1, 4], # taken from pylearn source
#     },"""
#     # create dataset
#     # create sgd
#     # 
#     # copy weights
#     # put 
# if __name__ == "__main__":
#     test_transfer_train()