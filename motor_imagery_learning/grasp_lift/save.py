from pylearn2.utils import serial
import numpy as np
class GraspLiftResult():
    def __init__(self, grasp_lift_trainer,
            experiment_parameters, training_time):
        self.training_time= training_time
        self.misclasses = dict(test= 1 - 
            np.array(grasp_lift_trainer.valid_aucs), 
            train= 1 - np.array(grasp_lift_trainer.train_aucs))
        self.parameters = experiment_parameters
        self.templates = {}
        
    def get_misclasses(self):
        return self.misclasses
    
    def save(self, filename):
        serial.save(filename, self)
        
class GraspLiftModel(object):
    """ For storing a model. Warning can be quite big"""
    def __init__(self, grasp_lift_trainer):
        self.grasp_lift_trainer = grasp_lift_trainer
    
    def save(self, filename):
        self.bla = "" # TODELAY: think abt what to save
        del self.grasp_lift_trainer
        serial.save(filename, self)