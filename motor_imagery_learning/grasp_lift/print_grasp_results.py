#!/usr/bin/env python
from motor_imagery_learning.analysis.print_results import ResultPrinter
import argparse
import numpy as np
from copy import deepcopy

class GraspResultPrinter(ResultPrinter):
    def __init__(self, folder_name, only_last_fold=False):
        self.only_last_fold=only_last_fold
        self._folder_name = folder_name
    
    def _add_best_and_best_epoch(self, formatted_result, misclasses):
        if 'test' in misclasses:
            # Determine minimal number of epochs and 
            # only take misclass rates until that epoch
            test_misclasses = deepcopy(misclasses['test'])
            # transform to list of one list in case of only one experiment
            test_misclasses = self._atleast_2d_or_1d_of_arr(test_misclasses)
            test_misclasses = test_misclasses [0] # somehow necessary...
            min_epoch_num = np.min([len(a) for a in test_misclasses])
            same_epochs_misclasses = [a[0:min_epoch_num] for a in test_misclasses]
            same_epochs_misclasses = np.array(same_epochs_misclasses)
            average_misclass = np.mean(same_epochs_misclasses, axis=0)
            best_epoch = np.argmin(average_misclass)
            best_misclass = average_misclass[best_epoch]
            formatted_result['best'] = "{:5.2f}%".format((1 - best_misclass) * 100)
            # +1 for 1-based indexing
            formatted_result['best_epoch'] = "{:3d}".format(best_epoch + 1)
    
    def _print_templates(self):
        return # TODELAY: reactivate, maybe also store templates?
    
    def _add_misclasses(self, formatted_result, misclasses):
        for key in misclasses:
            # get last epoch from all folds
            # need transform in case we just have one experiment(?)
            this_misclasses = misclasses[key]
            if this_misclasses.dtype == object:
                # in case we have some with 10 folds, some with three folds
                # it can happen that the inner array is wrapped...
                this_misclasses = this_misclasses[0]
            if self.only_last_fold:
                this_misclasses = this_misclasses[-1]
            this_mean = np.mean(this_misclasses)
            #this_mean = this_misclasses[-1]
            formatted_result[key] = "{:5.2f}%".format((1 - this_mean) * 100)
            if (isinstance(this_misclasses, list) and 
                    len(this_misclasses) > 1): # only for crossval
                this_std = np.std(this_misclasses)
                formatted_result[key + '_std'] = "{:4.2f}%".format(
                    this_std * 100)
     
    def _compute_final_misclasses(self, result_list, add_valid_test=False):
        """ Compute final fold-averaged misclasses for all experiments.
        Also works if there are no folds(train-test case)"""
        final_misclasses = {}
        misclasses = [r['misclasses'] for r in result_list]
        misclass_keys = misclasses[0].keys() # train,test etc
        for key in misclass_keys:
            this_misclasses = [m[key] for m in misclasses]
            if self.only_last_fold:
                fold_averaged_misclasses = np.array(this_misclasses)[:,-1]
            else:
                # avg over folds
                fold_averaged_misclasses = np.mean(this_misclasses, axis=1)
            
            final_misclasses[key] = fold_averaged_misclasses
            
        return final_misclasses
def parse_command_line_arguments():
    parser = argparse.ArgumentParser(
        description="""Print results stored in a folder.
        Example: ./print_results data/models/conv_nets/ """
    )
    parser.add_argument('results_folder', action='store',
                        choices=None,
                        help='A folder with results to print')
    parser.add_argument('--last_fold', action='store_true',
                        help='Only print results for last fold.')
    args = parser.parse_args()
    return args

def main():
    args = parse_command_line_arguments()
    result_printer = GraspResultPrinter(args.results_folder, 
        only_last_fold=args.last_fold)
    result_printer.print_results()

if __name__ == "__main__":
    main()
 