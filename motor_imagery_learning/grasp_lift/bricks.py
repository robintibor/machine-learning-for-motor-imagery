from blocks.bricks.base import application, Brick
from blocks.bricks import Sequence
from blocks.bricks.recurrent import LSTM, GatedRecurrent
import numpy as np
from theano import tensor
import theano 
from blocks.utils import pack
from motor_imagery_learning.mypylearn2.pool import  log_sum

class SequenceAll(Sequence):
    def __init__(self, bricks, **kwargs):
        application_methods = [b.apply for b in bricks]
        super(SequenceAll, self).__init__(application_methods, **kwargs)

    """Sequence Class that has an argument for returning all outputs.
    Also, this accepts bricks not apply methods."""
    @application
    def apply(self, return_all, *args):
        child_input = args
        all_outputs = []
        for application_method in self.application_methods:
            output = application_method(*pack(child_input))
            all_outputs.append(output)
            child_input = output
        if return_all:
            return all_outputs
        else:
            return output
    
    
class ApplyLambda(Brick):
    def __init__(self, apply_func):
        self.apply_func = apply_func
        super(ApplyLambda, self).__init__()
        
    @application(inputs=['input_'], outputs=['output'])
    def apply(self, input_):
        """Apply the linear transformation.
        Parameters
        ----------
        input_ : :class:`~tensor.TensorVariable`
            The input on which to apply the transformation
        Returns
        -------
        output : :class:`~tensor.TensorVariable`
            The transformed input plus optional bias
        """
        output = self.apply_func(input_)
        return output


    
class Prepender(Brick):
    def __init__(self, n_to_prepend):
        self.n_to_prepend = n_to_prepend
        super(Prepender, self).__init__()
        
    @application(inputs=['input_'], outputs=['output'])
    def apply(self, input_):
        """Apply the linear transformation.
        Parameters
        ----------
        input_ : :class:`~tensor.TensorVariable`
            The input on which to apply the transformation
        Returns
        -------
        output : :class:`~tensor.TensorVariable`
            The transformed input plus optional bias
        """
        # do not set to 0 and 1 as this will lead to nan costs
        array_to_prepend = 0.05 + np.zeros((self.n_to_prepend, 7), dtype=theano.config.floatX)
        array_to_prepend[:,-1] = 0.95
        output = tensor.concatenate((array_to_prepend, 
                                    input_), 
                                    axis=0)
        return output
    
class DummyOutput(Brick):
    @application(inputs=['input_'], outputs=['output'])
    def apply(self, input_):
        """Apply the linear transformation.
        Parameters
        ----------
        input_ : :class:`~tensor.TensorVariable`
            The input on which to apply the transformation
        Returns
        -------
        output : :class:`~tensor.TensorVariable`
            The transformed input plus optional bias
        """
        output = tensor.zeros((input_.shape[0], 7), dtype=theano.config.floatX)
        # set last class to 1 everywhere
        output = tensor.set_subtensor(output[:,-1], 1)
        output = output + 1e-4 * tensor.std(input_) # just make input part of compgraph
        return output   
    
class DimShuffle(Brick):
    def __init__(self, shuffle_order):
        self.shuffle_order = shuffle_order
        super(DimShuffle, self).__init__()
        
    @application(inputs=['input_'], outputs=['output'])
    def apply(self, input_):
        """Apply the linear transformation.
        Parameters
        ----------
        input_ : :class:`~tensor.TensorVariable`
            The input on which to apply the transformation
        Returns
        -------
        output : :class:`~tensor.TensorVariable`
            The transformed input plus optional bias
        """
        output = input_.dimshuffle(*self.shuffle_order)
        return output
    
class Reshape(Brick):
    def __init__(self, newshape):
        self.newshape = newshape
        super(Reshape, self).__init__()
        
    @application(inputs=['input_'], outputs=['output'])
    def apply(self, input_):
        """Apply the linear transformation.
        Parameters
        ----------
        input_ : :class:`~tensor.TensorVariable`
            The input on which to apply the transformation
        Returns
        -------
        output : :class:`~tensor.TensorVariable`
            The transformed input plus optional bias
        """
        output = input_.reshape((self.newshape))
        return output

class Square(Brick):
    def __init__(self,):
        super(Square, self).__init__()
        
    @application(inputs=['input_'], outputs=['output'])
    def apply(self, input_):
        output = input_ * input_
        return output
    
class LogSum(Brick):
    def __init__(self, pool_shape, pool_stride, image_shape):
        self.pool_shape = pool_shape
        self.pool_stride = pool_stride
        self.image_shape = image_shape
        super(LogSum, self).__init__()

    @application(inputs=['input_'], outputs=['output'])
    def apply(self, input_):
        output = log_sum(input_, self.pool_shape, self.pool_stride, 
            self.image_shape)
        return output
    

class ApplyFunc(Brick):
    """ Untried, accepts a function and keyword agruments for the function."""
    def __init__(self, apply_func, kwargs):
        self.apply_func = apply_func
        self.kwargs = kwargs
        super(ApplyFunc, self).__init__()
        
    @application(inputs=['input_'], outputs=['output'])
    def apply(self, input_):
        """Apply the linear transformation.
        Parameters
        ----------
        input_ : :class:`~tensor.TensorVariable`
            The input on which to apply the transformation
        Returns
        -------
        output : :class:`~tensor.TensorVariable`
            The transformed input plus optional bias
        """
        output = self.apply_func(input_, **self.kwargs)
        return output

class SelectFirstInput(Brick):
    @application(inputs=['input1', 'input2'], outputs=['output'])
    def apply(self, input1, input2):
        """Apply the linear transformation.
        Parameters
        ----------
        input_ : :class:`~tensor.TensorVariable`
            The input on which to apply the transformation
        Returns
        -------
        output : :class:`~tensor.TensorVariable`
            The transformed input plus optional bias
        """
        output = input2
        return output