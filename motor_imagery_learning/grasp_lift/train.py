from copy import deepcopy
from numpy.random import RandomState
from collections import OrderedDict
import numpy as np
from theano import tensor
import theano
from blocks.bricks.cost import CategoricalCrossEntropy, BinaryCrossEntropy
from blocks.roles import WEIGHT, PARAMETER, BIAS
from blocks.graph import ComputationGraph
from blocks.filter import VariableFilter
from blocks.bricks.conv import Convolutional
from motor_imagery_learning.mypylearn2.learning_rule_adam import Adam
from motor_imagery_learning.mypylearn2.pool import  log_sum
from blocks.bricks import Softmax, NDimensionalSoftmax, Logistic
from blocks.bricks import Linear
from blocks import initialization
from sklearn.metrics import roc_auc_score
import pandas as pd
from motor_imagery_learning.mywyrm.processing import bandpass_cnt
import wyrm.types
import os.path

from blocks.graph import apply_dropout

import logging
log = logging.getLogger(__name__)

class GraspLiftTrain(object):
    def __init__(self, i_subject, sequence, max_increasing_epochs, 
                 max_epochs, adam_alpha, updates_per_epoch, 
                 fraction_outside_trials, dropout):
        local_variables = locals()
        local_variables.pop('self')
        self.__dict__.update(local_variables)

    def  run(self):
        self.load_data()
        log.info("Compile Theano graphs...")
        self.create_and_compile_theano_graphs()
        log.info("Setup training vars...")
        self.setup_early_stop_and_monitors()
        log.info("Run Training...")
        self.run_training()
        return True

    def load_data(self):
        log.info("Load train data...")
        self.load_train_data()
        log.info("Load test data...")
        self.load_val_data()
        
    def load_train_data(self):
        X, y = load_train(self.i_subject,1)
        all_train_means, all_train_stds = [],[] 
        X_train, train_mean, train_std = preproc_X(X, can_fit=True)
        all_train_means.append(train_mean)
        all_train_stds.append(train_std)
        
        y = preprocess_y(y)
        train_in = X_train

        # target preprocessing
        train_targets= y

        for i_series in xrange(2,8):
            X, y = load_train(self.i_subject,i_series)
            X_train, train_mean, train_std = preproc_X(X, can_fit=True)
            y = preprocess_y(y) 
            train_in = np.concatenate((train_in, X_train), axis=2)
            train_targets = np.concatenate((train_targets, y), axis=0)
            all_train_means.append(train_mean)
            all_train_stds.append(train_std)
        self.train_in = train_in
        self.train_targets = train_targets
        self.train_mean = np.mean(all_train_means, axis=0)
        self.train_std = np.mean(all_train_stds, axis=0)
        
    def load_val_data(self):
        X, y = load_train(self.i_subject,8)
        X_val = preproc_X(X, can_fit=False, 
                          old_mean=self.train_mean, 
                          old_std=self.train_std)
        y_val = preprocess_y(y)
        self.val_in = X_val
        self.val_targets = y_val
        
    def create_and_compile_theano_graphs(self):
        self.sequence.initialize()
        x = tensor.tensor4('x', dtype='floatX')
        out = self.sequence.apply(False, x)

        y = tensor.lmatrix('targets')

        #cost = CategoricalCrossEntropy().apply(y.flatten(), out)
        
        # not sure if this is necessary, trying to prevent nan problem
        out = tensor.minimum(tensor.maximum(out, 1e-4), 1 - 1e-4) 
        cost = BinaryCrossEntropy().apply(y, out)
        cg = ComputationGraph(cost)

        params = VariableFilter(roles=[PARAMETER])(cg.variables)
        if self.dropout:
            cg_dropout = apply_dropout(cg, params, drop_prob=0.5)
            cost_grad = theano.grad(cg_dropout.outputs[0], params)
        else:
            cost_grad = theano.grad(cost, params)
        

        param_to_grad = OrderedDict()
        for param, grad in zip(params, cost_grad):
            param_to_grad[param] = grad

        adam_rule = Adam(self.adam_alpha)

        adam_updates = adam_rule.get_updates(learning_rate=None, grads=param_to_grad)
        
        self.params = params
        self.adam_rule = adam_rule
        self.fixed_sgd_update = theano.function([x,y], adam_updates.values(), updates=adam_updates)
        self.out_func = theano.function([x], out)
        self.cost_func = theano.function([x,y], cost)
    
    def setup_early_stop_and_monitors(self):
        self.i_epoch = 0
        self.last_best_epoch = 0
        self.best_params = deepcopy(self.params)
        self.best_adam_rule = deepcopy(self.adam_rule)
        self.last_best_auc = 0
        self.valid_aucs = []
        self.train_aucs = []
    
    def run_training(self):
        log.info("Run Until Early Stop...")
        self.run_until_early_stop()
        self.setup_after_early_stop()
        log.info("Run After Early Stop...")
        self.run_after_early_stop()
    
    def run_until_early_stop(self):
        self.find_trial_starts()
        while(not self.stop()):
            self.run_one_epoch()
            self.report_current_epoch()
    
    def find_trial_starts(self):
        trial_1_inds = np.flatnonzero(self.train_targets[:,0] == 1)
        trial_1_switches = np.flatnonzero(np.diff(trial_1_inds) != 1)
        trial_starts = trial_1_inds[trial_1_switches]
        self.trial_starts = trial_starts

    def stop(self):
        early_stop = (self.i_epoch - self.last_best_epoch) > self.max_increasing_epochs
        hard_stop = self.i_epoch > self.max_epochs
        return early_stop or hard_stop
    
    def run_one_epoch(self):
        rng = RandomState(132098)
        batch_size = 900
        updates_outside_trials = int(self.fraction_outside_trials * 
            self.updates_per_epoch)
        for _ in xrange(self.updates_per_epoch):
            start = rng.choice(self.trial_starts, size=1 , replace=False)[0]
            start = start - 250
            self.fixed_sgd_update(self.train_in[:,:,start:start+batch_size], 
                             self.train_targets[start:start+batch_size])
        for _ in xrange(updates_outside_trials):
            start = rng.choice(len(self.train_targets) - batch_size, 
                size=1 , replace=False)[0]
            self.fixed_sgd_update(self.train_in[:,:,start:start+batch_size], 
                             self.train_targets[start:start+batch_size])
            
            
            
    def report_current_epoch(self):
        log.info("Epoch {:d}".format(self.i_epoch))
        train_preds = self.out_func(self.train_in)
        val_preds = self.out_func(self.val_in)
        train_cost = self.cost_func(self.train_in, self.train_targets)
        #valid_accuracy = np.sum(np.argmax(val_preds, axis=1) == self.val_targets[0]) / float(len(self.val_targets[0]))
        log.info("Train Cost: {:f}".format(float(train_cost)))
        #log.info("Valid Accuracy: {:f}".format(valid_accuracy))
        # For AUCs only use first 6 classes, not new virtual class (as that is
        # how the competition is also evaluating it)
        train_auc = np.mean([roc_auc_score(
            np.int32(self.train_targets[:,i] == 1), train_preds[:,i]) 
            for i in range(6)])
        valid_auc = np.mean([roc_auc_score(
            np.int32(self.val_targets[:,i] == 1), val_preds[:,i]) 
            for i in range(6)])
        log.info("Train AUC: {:f}".format(train_auc))
        log.info("Valid AUC: {:f}".format(valid_auc))
        self.train_aucs.append(train_auc)
        self.valid_aucs.append(valid_auc)
        self.i_epoch += 1
        if valid_auc > self.last_best_auc:
            self.last_best_auc = valid_auc
            self.last_best_train_auc = train_auc
            self.last_best_epoch = self.i_epoch
            self.best_params = deepcopy(self.params)
            self.best_adam_rule = deepcopy(self.adam_rule)
            
    def setup_after_early_stop(self):
        # remember since they will be overwritten again
        self.best_epoch = self.last_best_epoch 
        self.best_train_auc = self.last_best_train_auc 
        for i, param in enumerate(self.params):
            param.set_value(self.best_params[i].get_value())
        
        for key in self.adam_rule.grad:
            old_val = self.best_adam_rule.grad[key].get_value()
            new_var = self.adam_rule.grad[key]
            new_var.set_value(old_val)
                
        for key in self.adam_rule.sq_grad:
            old_val = self.best_adam_rule.sq_grad[key].get_value()
            new_var = self.adam_rule.sq_grad[key]
            new_var.set_value(old_val)
        self.adam_rule.time.set_value(self.best_adam_rule.time.get_value())

        # add val to train
        self.train_targets = np.concatenate((self.train_targets, self.val_targets), axis=0)
        self.train_in = np.concatenate((self.train_in, self.val_in), axis=2)
        self.i_epoch = self.last_best_epoch

    def run_after_early_stop(self):
        self.find_trial_starts()
        while(not self.second_stop()):
            self.run_one_epoch()
            self.report_current_epoch()
            
    def second_stop(self):
        return (self.valid_aucs[-1] > self.best_train_auc or 
            self.i_epoch > (1.5 * self.best_epoch))
        
    def write_csv(self, filename):
        all_series_preds = []
        for i_series in (9,10):
            print ("Testing on Series {:d}".format(i_series))
            X_test = load_test(self.i_subject, i_series)
            X_test = preproc_X(X_test, can_fit=False, 
                              old_mean=self.train_mean, 
                              old_std=self.train_std)
            y_out = self.out_func(X_test)
            #print y_out.shape
            preds_for_csv = restore_original_prediction(y_out, 
                self.i_subject, i_series)
            all_series_preds.append(preds_for_csv)
        cols = ['HandStart','FirstDigitTouch',
        'BothStartLoadPhase','LiftOff',
        'Replace','BothReleased']

        # collect ids
        all_ids = []
        all_preds = []
        for i_series in (9,10):
            id_prefix = "subj{:d}_series{:d}_".format(self.i_subject, i_series)
            this_preds = all_series_preds[i_series-9] # respect offsets
            all_preds.extend(this_preds)
            this_ids = [id_prefix + str(i_sample) for i_sample in range(this_preds.shape[0])]
            all_ids.extend(this_ids)
        all_ids = np.array(all_ids)
        all_preds = np.array(all_preds)
        submission_file_name = filename
        submission = pd.DataFrame(index=all_ids,
                                  columns=cols,
                                  data=all_preds)
        
        submission.to_csv(submission_file_name,
            index_label='id',float_format='%.3f')
        
def load_train(i_subject, i_series):
    train_folder = '../../../grasp-lift/data/train/'
    data_filename = 'subj{:d}_series{:d}_data.csv'.format(
        i_subject, i_series)
    data_file_path = os.path.join(train_folder, data_filename)
    data = pd.read_csv(data_file_path)
    # events file
    events_file_path = data_file_path.replace('_data','_events')
    # read event file
    labels= pd.read_csv(events_file_path)
    clean = data.drop(['id' ], axis=1)#remove id
    labels = labels.drop(['id' ], axis=1)#remove id
    return clean,labels

def load_test(i_subject, i_series):
    train_folder = '../../../grasp-lift/data/test/'
    data_filename = 'subj{:d}_series{:d}_data.csv'.format(
        i_subject, i_series)
    data_file_path = os.path.join(train_folder, data_filename)
    data = pd.read_csv(data_file_path)
    clean = data.drop(['id' ], axis=1)#remove id
    return clean

def preprocess_y(y):
    y = y[::2]
    y = np.atleast_2d(y.astype(int))
    y = y.T.astype(np.int32)
    # add last column for virtual class indicating no other class is active
    y = np.concatenate((y, np.zeros((1, y.shape[1]))), axis=0)
    y[-1,np.sum(y, axis=0) == 0] = 1
    y = y.astype(np.int32).T # now should be #samples x #classes
    return y


def preproc_X(X, can_fit=False, old_mean=None, old_std=None):
    sensors = X.keys()
    X = np.asarray(X.astype(float))
    fs = 500.0
    timesteps_in_ms =np.arange(len(X)) * 1000.0 / fs

    cnt = wyrm.types.Data(X, [timesteps_in_ms, sensors], ['time', 'channel'], 
                ['ms', '#'])
    cnt.fs = 500.0
    #lowpass to remove drifts, highpass for later subsampling all forward filtering
    cnt = bandpass_cnt(cnt, 0.5, 100)
    X = cnt.data
    if can_fit:
        mean = np.mean(X, axis=0)
        std = np.std(X, axis=0)
    else:
        assert old_mean is not None and old_std is not None
        mean  = old_mean
        std = old_std
    X = (X - mean) / std
    #subsample
    X = X[::2]
    X = X.astype(theano.config.floatX)[np.newaxis,np.newaxis,:,:]
    if can_fit:
        return X, mean, std
    else:
        return X

def restore_original_prediction(y_out, i_subject, i_series):
    preds_for_csv = y_out[:,:6]
    # double since we halved the input
    preds_for_csv = np.repeat(preds_for_csv,2, axis=0)
    X_orig = load_test(i_subject,i_series)
    assert X_orig.shape[0] == len(preds_for_csv) or X_orig.shape[0] == len(preds_for_csv) - 1
    if X_orig.shape[0] == len(preds_for_csv) - 1:
        preds_for_csv = preds_for_csv[:-1]
    assert X_orig.shape[0] == len(preds_for_csv)
    return preds_for_csv