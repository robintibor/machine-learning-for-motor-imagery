
import HPOlib.benchmarks.benchmark_util as benchmark_util

import time

import motor_imagery_learning.hyperopt.optimize as optimize

__authors__ = ["Katharina Eggensperger", "Matthias Feurer"]
__contact__ = "automl.org"


def main(params, **kwargs):
    #print 'Params: ', params,
    y = optimize.train_hyperopt(params, config_dir=config_dir)
    print 'Result: ', y
    return y

if __name__ == "__main__":
    starttime = time.time()
    args, params = benchmark_util.parse_cli()
    print params

    # transform hyperparameter
    freq_start = int(float(params["frequency_start"]))
    if freq_start == 250:
        frequency_stop = 251
    else:
        # freq_start + 1: Because frequency_stop = 0 means we use one bin
        frequency_stop = freq_start + 1 + ((251 - freq_start) *
                                           float(params["frequency_stop"]))
        frequency_stop = int(frequency_stop)
    params["frequency_stop"] = str(frequency_stop)

    frequency_bins = frequency_stop - freq_start

    kernel_shape_freq = int(((frequency_bins-1) *
                             float(params["kernel_shape_freq"])) + 1)
    params["kernel_shape_freq"] = str(kernel_shape_freq)

    pool_shape_time = int((8 - int(float(params["kernel_shape_time"])) - 1) *
                          float(params["pool_shape_time"]) + 1)
    params["pool_shape_time"] = str(pool_shape_time)

    pool_shape_freq = int((frequency_bins -
                           float(params["kernel_shape_freq"])) *
                          float(params["pool_shape_freq"])) + 1
    params["pool_shape_freq"] = str(pool_shape_freq)

    print params

    config_dir = args['config_dir']

    result = main(params, **args)
    duration = time.time() - starttime
    print "Result for ParamILS: %s, %f, 1, %f, %d, %s" % \
        ("SAT", abs(duration), result, -1, str(__file__))
