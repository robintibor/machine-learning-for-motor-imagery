from pylearn2.format.target_format import OneHotFormatter
import numpy as np
import h5py
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import logging
from analysis.sensor_positions import sort_topologically
from pylearn2.config import yaml_parse
import re
log = logging.getLogger(__name__)

def get_dataset(**kwargs):
    dataset_type = kwargs.pop('type')
    dataset = dataset_type(**kwargs)
    return dataset

class MotorEEGDataset(DenseDesignMatrix):
    @staticmethod
    def get_min_num_trials(filenames, topo_view_channel):
        if (isinstance(filenames, basestring)):
            filenames = [filenames]
        min_num_trials = float('inf')
        for filename in filenames:
            dataset_file = h5py.File(filename, 'r')
            topo_view = dataset_file[topo_view_channel]
            num_trials = topo_view.shape[0] # axis 0 should be num of trials
            if (num_trials < min_num_trials): min_num_trials = num_trials
            dataset_file.close()
        return min_num_trials

    # DenseDesignMatrix hasn't implemented so let's implement ourselves
    # TODO: remove once it is implemented in DenseDesignMatrix
    def get(self, source, index):
        assert(source[0] == 'features' and source[1] == 'targets')
        return self.X[index], self.y[index]

    def __init__(self, filenames, topo_view_channel, target_channel,
                 start=None, stop=None, limits=None,
                 axes=['b', 'c', 0, 1],
                 sensor_names=None,
                 time_bin_start=None,
                 time_bin_stop=None, 
                 frequency_bin_start=None,
                 frequency_bin_stop=None,
                 **kwargs):
        """ Start/stop may be either trial indices as ints or
        fractions of the entire dataset as float (e.g. start=0.7, start at 
        70% of dataset)"""
        # allow empty dataset to be created
        if filenames is None: return
        # cant use both limits and start and stop...
        assert((limits is None) or (start is None and stop is None))
        assert target_channel == 'targets'
        # laod inputs several files
        # same for raw
        # change raw so that chans are 1-axis of bc01
        # make topological sorting of chans when determining chan indices
                # use existing grid, make docstring tests(!!)
                # also store in model actual chan names? yes using your cv class etc
                # maybe also create pylearn trian class, shift into mypylearn2 module
        # create bandpass filtered data, write function, use only small
        # subset of gujo, see if you can use raw or if you have to use sth else
        # If loading just one file, put it into a one-sized list
        if isinstance(filenames, basestring):
            filenames = [filenames]
        
        self._topo_view_channel = topo_view_channel
        self._target_channel = target_channel
        self._start_trial = start
        self._stop_trial = stop
        self._trial_limits = limits
        self._time_bin_start = time_bin_start
        self._time_bin_stop = time_bin_stop
        self._freq_bin_start = frequency_bin_start
        self._freq_bin_stop = frequency_bin_stop
        self._sensor_names = get_available_sensors_topo(filenames, sensor_names)
        topo_view, y = self._load_all_data(filenames)
        log.info("Loaded {:d} trials".format(topo_view.shape[0]))
        super(MotorEEGDataset, self).__init__(topo_view=topo_view, y=y, 
                                              axes=axes, **kwargs)

    def _load_all_data(self, filenames) :
        # Concatenate all datasets (datasets consist of topo_view and y(!)
        dataset = self._get_single_file_dataset(filenames[0])
        dataset.load_data()
        topo_view, y = dataset.topo_view, dataset.y
        for filename in filenames[1:]:
            dataset = self._get_single_file_dataset(filename)
            dataset.load_data()
            topo_view = np.append(topo_view, dataset.topo_view, 0)
            y = np.append(y, dataset.y, 0)
        return topo_view, y
    
    def _get_single_file_dataset(self, filename):
        return MotorEEGDatasetSingleFile(filename, 
            self._topo_view_channel, self._target_channel, 
            self._start_trial, self._stop_trial, self._trial_limits,
            self._sensor_names, self._time_bin_start, self._time_bin_stop,
            self._freq_bin_start, self._freq_bin_stop)
    
class MotorEEGDatasetSingleFile:
    def __init__(self, filename,  topo_view_channel, target_channel, 
            start, stop, limits,sensor_names, time_bin_start, time_bin_stop,
            freq_bin_start, freq_bin_stop):
        self.filename = filename
        self._sensor_names = sensor_names
        self._time_bin_start = time_bin_start
        self._time_bin_stop = time_bin_stop
        self._freq_bin_start = freq_bin_start
        self._freq_bin_stop = freq_bin_stop
        self._start_trial = start
        self._stop_trial = stop
        self.limits_trial = limits
        self.topo_view_channel = topo_view_channel
        self.target_channel = target_channel
        
    def load_data(self):
        self.channel_indices = get_sensor_indices(self.filename,
            self._sensor_names)
        h5file = h5py.File(self.filename, 'r')
        topo_view_set = h5file[self.topo_view_channel]
        target_set = h5file[self.target_channel]
        self._time_bin_start = self._time_bin_start or 0
        self._time_bin_stop =  self._time_bin_stop or topo_view_set.shape[2]
        topo_view, y = self._load_input_data(topo_view_set, target_set)
        h5file.close()
        # Format targets into one hot
        y = format_targets(y)
        self.topo_view = topo_view
        self.y = y
        
    def _load_input_data(self, topo_view_set, target_set):
        # cant use both limits and start and stop...
        assert(self.limits_trial is None) or \
            (self._start_trial is None and self._stop_trial is None)
        if (self.limits_trial is None):
            start, stop = adjust_start_stop(topo_view_set.shape[0], 
                self._start_trial, self._stop_trial)
            topo_view, y = self._load_input_data_part(start, stop,
                topo_view_set, target_set)
        else:
            # limits is a 2d array of sub-dataset-limits like:
            # [[0,2], [7,10]] for indices [0,1,7,8,9]
            # This way with loop is much faster (30sec vs 1min30)
            # than computing the indices and using them directly...
            start = self.limits_trial[0][0]
            stop = self.limits_trial[0][1]

            topo_view, y = self._load_input_data_part(start, stop,
                topo_view_set, target_set)
            for limit in self.limits_trial[1:]:
                start = limit[0]
                stop = limit[1]
                topo_view_part, y_part = self._load_input_data_part(
                    start, stop, topo_view_set, target_set)
                topo_view = np.append(topo_view, topo_view_part, 0)
                y = np.append(y, y_part, 0)
        return topo_view, y

    def _load_input_data_part(self, start, stop, 
            topo_view_set, target_set):
        topo_view = topo_view_set[start:stop] \
                                 [:, self.channel_indices, :, :]\
                                 [:, :, self._time_bin_start:self._time_bin_stop, \
                                     self._freq_bin_start:self._freq_bin_stop]
        y = target_set[start:stop]
        return topo_view, y

class FFTMotorEEGDataset(MotorEEGDataset):
    def __init__(self, filenames, topo_view_channel,
        frequency_start=None, frequency_stop=None, **kwargs):
        """ target channel needs to be supplied too"""
        # TODELAY: remove taret_channel from superclass or add it here
        # allow empty dataset to be created
        if filenames is None: return
        if isinstance(filenames, basestring):
            filenames = [filenames]
        self.topo_view_channel = topo_view_channel
        self._frequency_start = frequency_start
        self._frequency_stop = frequency_stop
        freq_bin_start, freq_bin_stop = self._determine_frequency_indices(
            filenames[0])#shd be same for all 
        super(FFTMotorEEGDataset, self).__init__(filenames,
            topo_view_channel,
            frequency_bin_start = freq_bin_start,
            frequency_bin_stop = freq_bin_stop,
            **kwargs)

    def _determine_frequency_indices(self, filename):
        h5file = h5py.File(filename, 'r')
        topo_view_set = h5file[self.topo_view_channel]
        # frequencies are on last axis(fourth with zero-based index 3)
        num_freq_bins = topo_view_set.shape[3]
        h5file.close()
        # We assume given frequencies to be frequencies
        # not bin indices(!) 
        # If else mainly for tests where we disable frequency indices
        if (self._frequency_start is not None):
            freq_bin_start = get_frequency_bin(filename, 
                self.topo_view_channel,
                num_freq_bins,
                self._frequency_start)
            assert freq_bin_start >= 0
        else:
            freq_bin_start = 0
        if (self._frequency_stop is not None):
            freq_bin_stop = get_frequency_bin(filename, 
                self.topo_view_channel,
                num_freq_bins,
                self._frequency_stop)
            freq_bin_stop += 1 # stop is exclusive in python!
            assert freq_bin_stop <= num_freq_bins - 1
        else:
            freq_bin_stop = num_freq_bins
        return freq_bin_start, freq_bin_stop
        
class RawMotorEEGDataset(MotorEEGDataset):
    """ For raw or bandpass filtered data (not spectogram data)
    """
    def _get_single_file_dataset(self, filename):
        return RawMotorEEGDatasetSingleFile(filename, 
            self._topo_view_channel, self._target_channel, 
            self._start_trial, self._stop_trial, self._trial_limits,
            self._sensor_names, self._time_bin_start, self._time_bin_stop, 
            None, None) # last are for freqs

class RawMotorEEGDatasetSingleFile(MotorEEGDatasetSingleFile):
    def _load_input_data_part(self, start, stop, 
            topo_view_set, target_set):   
        if (len(topo_view_set.shape) == 3): # if only one signal per chan and samplepoint   
            assert self._freq_bin_start is None
            assert self._freq_bin_stop is None
            topo_view = topo_view_set[start:stop] \
                         [:, self.channel_indices, :]\
                         [:, :, self._time_bin_start:self._time_bin_stop]
            topo_view = np.expand_dims(topo_view, axis=3)
        else:
            topo_view = topo_view_set[start:stop] \
                             [:, self.channel_indices, :, :]\
                             [:, :, self._time_bin_start:self._time_bin_stop, \
                                 self._freq_bin_start:self._freq_bin_stop]
            
        y = target_set[start:stop]
        return topo_view, y

def adjust_start_stop(num_trials, given_start, given_stop):
    # allow to specify start trial as percentage of total dataset
    if isinstance(given_start, float):
        assert given_start >= 0 and given_start <= 1
        given_start = int(num_trials * given_start)
    if isinstance(given_stop, float):
        assert given_stop >= 0 and given_stop <= 1
        # use -1 to ensure that if stop given as e.g. 0.6
        # and next set uses start as 0.6 they are completely
        # seperate/non-overlapping
        given_stop = int(num_trials * given_stop) - 1
    # if start or stop not existing set to 0 and end of dataset :)
    start = given_start or 0
    stop = given_stop or num_trials
    return start, stop
    

def get_available_sensors_topo(filenames, sensor_names):
    available_sensor_names = determine_available_sensors(filenames,
        sensor_names)
    # sort sensors topologically to allow topological convololutions
    topo_sensor_names = sort_topologically(available_sensor_names)
    # just some warnings for debug
    if (sensor_names is not None):
        missing_sensors = set(sensor_names) - set(topo_sensor_names)
        if len(missing_sensors) != 0:
            logging.warning("Missing sensors: {:s}".format(list(missing_sensors)))
    return topo_sensor_names    

def determine_available_sensors(filenames, sensor_names):
    """ Determine sensors available in _all_ specified files 
    Check that if not present in one
    >>> anwefile = './data/motor-imagery-tonio/AnWeMoSc1S001R01_ds10_1-12_autoclean_trials_ival_0_4000.mat'
    >>> gujofile = './data/motor-imagery-tonio/GuJoMoSc1S001R01_ds10_1-11_autoclean_trials_ival_0_4000.mat'
    >>> files = [anwefile, gujofile]
    >>> determine_available_sensors(files, ['Fp1'])
    []
    >>> determine_available_sensors([gujofile], ['Fp1'])
    ['Fp1']
    >>> determine_available_sensors(files, ['C3', 'Fp1'])
    ['C3']
    """
    available_sensors = set(available_sensors_one_file(filenames[0],
        sensor_names))
    for filename in filenames[1:]:
        available_this_file = available_sensors_one_file(filename,
            sensor_names)
        # make intersection of this file and remaining ones
        available_sensors = set.intersection(available_sensors, 
            set(available_this_file))
    return list(available_sensors)

def available_sensors_one_file(filename, sensor_names):
    h5file = h5py.File(filename, 'r')
    channel_refs = h5file['/channel_names'][:,0]
    names_as_2d_arrays = [h5file[ref] for ref in channel_refs]
    names_as_1d_arrays = [array[:, 0] for array in names_as_2d_arrays]
    names_as_char = [map(chr, array) for array in names_as_1d_arrays]
    names_as_strings = ["".join(array) for array in names_as_char]
    if sensor_names is not None:
        available_sensor_names = set(sensor_names).intersection(
            set(names_as_strings))
        num_missing_sensors = len(sensor_names) - len(available_sensor_names)
        assert num_missing_sensors <= 128 - len(names_as_strings)
        h5file.close()
        return available_sensor_names
    else: # sensor names is none
        h5file.close()
        return names_as_strings

def get_sensor_indices(filename, sensor_names):
    """ Get indices of sensor names in given dataset
    >>> get_sensor_indices('./data/motor-imagery-tonio/GuJoMoSc1S001R01_ds10_1-11_autoclean_trials_ival_0_4000.mat',
    ...     ['CP3', 'CP4'])
    [47, 49]
    >>> get_sensor_indices('./data/motor-imagery-tonio/AnWeMoSc1S001R01_ds10_1-12_autoclean_trials_ival_0_4000.mat',
    ...     ['CP3', 'CP4'])
    [40, 42]
    >>> get_sensor_indices('./data/motor-imagery-tonio/AnWeMoSc1S001R01_ds10_1-12_autoclean_trials_ival_0_4000.mat',
    ...     ['CP3', 'CP4', 'O1', 'O2'])
    [40, 42, 25, 26]
    """
    # this uses that avaiable sensors one file returns sensors in order
    # they are inside the matrix
    all_sensors = available_sensors_one_file(filename, sensor_names=None)
    # Assume all sensors present
    channel_indices = [all_sensors.index(name) for name in sensor_names]
    return channel_indices

def get_frequency_bin(filename, topo_view_channel, num_freq_bins, frequency):
    # recalculate frequency indices based on sampling rate
    h5file = h5py.File(filename, 'r')
    topo_view_set = h5file[topo_view_channel]
    # frequencies are on last axis(fourth with zero-based index 3)
    num_freq_bins = topo_view_set.shape[3]
    
    # sampling rate determined by channel name
    # TODELAY: improve this, store sampling rate while preprocessing for 
    # fft
    sampling_match = re.search(r'_([0-9]*)Hz', topo_view_channel)
    sampling_rate = int(sampling_match.groups()[0])
    h5file.close()
    freq_bin = get_freq_bin(sampling_rate, num_freq_bins, frequency)
    return freq_bin

def get_freq_bin(sampling_rate, num_freq_bins, frequency):
    """ factoring out logic from filename etc, probably rename this
    >>> get_freq_bin(500, 126, 0)
    0
    >>> get_freq_bin(500, 126, 2)
    1

    Highest frequency should be last bin (125 because of 0-based indexing)
    >>> get_freq_bin(500, 126, 250)
    125
    >>> get_freq_bin(300, 76, 0)
    0
    >>> get_freq_bin(300, 76, 4)
    2
    >>> get_freq_bin(300, 76, 150)
    75
    """
    # assume we always used half a second to compute fft
    window_samples = sampling_rate / 2
    #assert sampling_rate == 500
    #assert num_freq_bins == 126, "if freq bins different, they should be even" + \
    #    "or you need to change formula into if/else case or sth"
    all_freq_bins = np.fft.rfftfreq(window_samples, d=1/float(sampling_rate))
    assert len(all_freq_bins) == num_freq_bins
    assert frequency in all_freq_bins, \
        "Please specify frequencies that are exactly matching " + \
        "the center of a fft bin"
    freq_bin_index = np.nonzero(all_freq_bins == frequency)[0][0]
    return freq_bin_index

def format_targets(y):
    # matlab has one-based indexing and one-based labels
    # have to convert to zero-based labels so subtract 1...
    y = y - 1
    # we need only a 1d-array of integers
    # squeeze in case of 2 dimensions, make sure it is still 1d in case of
    # a single number (can happen for test runs with just one trial)
    y = np.atleast_1d(y.squeeze())
    y = y.astype(int)
    target_formatter = OneHotFormatter(4)
    y = target_formatter.format(y)
    return y