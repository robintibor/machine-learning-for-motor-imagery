
# coding: utf-8

# In[1]:

import os
# make sure to use cpu for printing results...
os.environ['THEANO_FLAGS'] = "floatX=float32,device=cpu,nvcc.fastmath=True"

import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7
from analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file
from motor_imagery_learning.analysis.print_results import ResultPrinter
from motor_imagery_learning.csp.print_csp_results import CSPResultPrinter


# In[4]:

get_ipython().run_cell_magic(u'javascript', u'', u'$(\'body\').append(\'<script src="https://dl.dropboxusercontent.com/u/34637013/work/ipython-notebooks/js-for-table-sorting/jquery.tablesorter.min.js"></script>\')\n// Sort tables every 5 seconds...\nvar makeTablesSortable;\nmakeTablesSortable = function() { \n  $(\'.text_cell_render table\').not(\'.sorted_table\').tablesorter().addClass(\'sorted_table\');\n}\n\nsetInterval(makeTablesSortable, 5000);')


# In[49]:


_ = plot_misclasses_for_file('data/models/debug/bandpower-adam//early-stop/first-5/7.result.pkl')
_ = plot_misclasses_for_file('data/models/debug/bandpower-adam//early-stop/first-5/8.result.pkl')


# In[53]:

_ = plot_misclasses_for_file('data/models/debug/bandpower-adam//early-stop/1.result.pkl')
_ = plot_misclasses_for_file('data/models/debug/bandpower-adam//early-stop/2.result.pkl')
_ = plot_misclasses_for_file('data/models/debug/bandpower-adam//early-stop/3.result.pkl')
_ = plot_misclasses_for_file('data/models/debug/bandpower-adam//early-stop/first-5/1.result.pkl')
_ = plot_misclasses_for_file('data/models/debug/bandpower-adam//early-stop/first-5/2.result.pkl')
_ = plot_misclasses_for_file('data/models/debug/bandpower-adam//early-stop/first-5/3.result.pkl')
_ = plot_misclasses_for_file('data/models/debug/bandpower-adam//early-stop/first-5/11.result.pkl')


# In[3]:

ResultPrinter('data/models/debug/raw-data-adam/early-stop/first-5/').print_results(print_templates_and_constants=False)


# In[2]:

ResultPrinter('data/models/debug/bandpower-adam//early-stop/first-5//').print_results(print_templates_and_constants=False)


# In[31]:

ResultPrinter('data/models/debug/csp-net-cpu/early-stop/').print_results(print_templates_and_constants=False)


# In[13]:

ResultPrinter('data/models/debug/csp-net/early-stop/').print_results(print_templates_and_constants=False)


# In[81]:

from motor_imagery_learning.csp.print_csp_results import CSPResultPrinter
CSPResultPrinter('data/models/csp/full/', only_last_fold=True).print_results()


# In[ ]:


16  data/BBCI-without-last-runs/RoScMo1-11.BBCI.mat  0:42:36  86.67%   100.00%  
17  data/BBCI-without-last-runs/StHeMo1-10.BBCI.mat  0:41:14  90.14%   100.00%  
18  data/BBCI-without-last-runs/SvMuMo1-12.BBCI.mat  0:43:43  72.06%   100.00%  
1   data/BBCI-without-last-runs/AnWeMo1-12.BBCI.mat  0:45:26  67.12%   99.70%   
2   data/BBCI-without-last-runs/BhNoMo1-12.BBCI.mat  0:47:35  100.00%  100.00%  
3   data/BBCI-without-last-runs/FaMaMo1-14.BBCI.mat  0:34:57  78.57%   100.00%  


# In[64]:

import numpy as np
test_accs = [67.12, 100, 78.57, 94.87, 100, 70, 69.74, 91.67, 79.66, 87.5, 88.73, 77.78, 76.32, 96.55, 93.67, 86.67, 90.14, 72.06]
print np.mean(test_accs)
print np.std(test_accs)


# In[31]:

CSPResultPrinter('data/models/csp/short-bands//').print_results()


# |id|filename|init_block|validation_start|validation_stop|factor_new|max_epochs|test_stop|training_start|epo_preprocessors|sensor_names|test_start|training_stop|time|valid_test|test|best|epoch|train|valid|
# |-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
# |1|data/BBCI-without-last-runs/BhNoMo1-12.BBCI.mat|-|-|-|-|10|-|-|resample|['C2', 'C4']|-|-|0:01:53|51.73%|51.73%|55.18%|  9|51.62%|51.73%|
# |2|data/BBCI-without-last-runs/BhNoMo1-12.BBCI.mat|-|-|-|-|10|-|-|resample|['C2', 'C4']|-|-|0:01:17|51.73%|51.73%|55.18%|  9|51.62%|51.73%|
# |3|data/BBCI-without-last-runs/BhNoMo1-12.BBCI.mat|-|-|-|-|10|-|-|resample|['C2', 'C4']|-|-|0:01:17|51.73%|51.73%|55.18%|  9|51.51%|51.73%|
# |4|data/BBCI-without-last-runs/BhNoMo1-12.BBCI.mat|-|-|-|-|10|-|-|resample|['C2', 'C4']|-|-|0:01:15|51.73%|51.73%|54.96%|  9|51.51%|51.73%|
# |5|data/BBCI-without-last-runs/BhNoMo1-12.BBCI.mat|-|-|-|-|10|-|-|resample|['C2', 'C4']|-|-|0:01:15|51.73%|51.73%|55.07%|  9|51.51%|51.73%|
# |6|data/BBCI-without-last-runs/BhNoMo1-12.BBCI.mat|-|-|-|-|10|-|-|resample|['C2', 'C4']|-|-|0:01:24|55.06%|53.93%|60.67%| 11|48.88%|56.18%|
# |7|data/BBCI-without-last-runs/BhNoMo1-12.BBCI.mat|-|-|-|-|10|-|-|resample|['C2', 'C4']|-|-|0:01:05|55.06%|53.93%|60.67%| 11|48.88%|56.18%|
# |8|data/BBCI-without-last-runs/BhNoMo1-12.BBCI.mat|-|-|-|-|10|-|-|resample|['C2', 'C4']|-|-|0:01:05|55.06%|53.93%|60.67%| 11|48.88%|56.18%|
# |9|data/BBCI-without-last-runs/BhNoMo1-12.BBCI.mat|-|-|-|-|10|-|-|resample|C_sensors|-|-|0:02:11|69.66%|66.29%|66.29%| 12|72.35%|73.03%|
# |10|data/BBCI-without-last-runs/BhNoMo1-12.BBCI.mat|-|-|-|-|20|-|-|resample|C_sensors|-|-|0:02:26|71.91%|71.91%|73.03%| 18|77.51%|71.91%|
# |11|data/BBCI-without-last-runs/BhNoMo1-12.BBCI.mat|-|-|-|-|-|-|-|resample|C_sensors|-|-|0:13:12|97.19%|94.38%|96.63%|100|100.00%|100.00%|
# |12|data/BBCI-without-last-runs/JoBeMo1-11.BBCI.mat|-|-|-|-|-|-|-|resample|C_sensors|-|-|0:10:35|77.59%|64.37%|66.67%|318|97.72%|90.80%|
# |13|data/BBCI-without-last-runs/JoBeMo1-11.BBCI.mat|-|504|567|-|-|630|0|resample|C_sensors|567|504|0:08:22|87.30%|77.78%|79.37%|283|99.60%|96.83%|
# |14|data/BBCI-without-last-runs/JoBeMo1-11.BBCI.mat|-|504|567|-|-|630|0|resample|C_sensors|567|504|0:10:16|88.10%|76.19%|84.13%|239|100.00%|100.00%|
# |15|data/BBCI-without-last-runs/JoBeMo1-11.BBCI.mat|50|504|567|0.1|-|630|0|resample_run_standardize|C_sensors|567|504|0:07:19|84.13%|69.84%|71.43%|287|99.60%|98.41%|
# |16|data/BBCI-without-last-runs/JoBeMo1-11.BBCI.mat|50|-|-|0.1|-|-|-|resample_run_standardize|C_sensors|-|-|0:07:27|77.59%|67.82%|68.97%|265|93.46%|87.36%|

# In[5]:

ResultPrinter('data/models/old-150-Hz/raw-data-demeaned/early-stop/').print_results()


# In[4]:

ResultPrinter('data/models/old-150-Hz/csp-remake-from-500-ms/early-stop/').print_results()


# In[9]:

ResultPrinter('data/models/debug/raw-data/early-stop/').print_results()

