
# coding: utf-8

# # Optimizing CSP Remake

# ## Cross Validation 

# |id|files|time|std|test|std|train|std|valid|std|
# |-|-|-|-|-|-|-|-|-|-|
# |0|18|8:51:00|2:04:55|90.10%|8.24%|96.84%|3.75%|95.26%|5.16%|

# |id|filename|time|test|std|best|epoch|train|std|valid|std|
# |-|-|-|-|-|-|-|-|-|-|-|
# |1|AnWeMo1-12|7:51:45|65.71%|17.78%|40.16%| 78|84.70%|13.83%|78.73%|19.05%|
# |2|BhNoMo1-12|9:54:09|98.61%|1.05%|98.23%|563|99.70%|0.18%|99.24%|0.62%|
# |3|FaMaMo1-14|9:20:32|91.96%|5.06%|91.81%|514|99.35%|0.38%|99.37%|0.77%|
# |4|FrThMo1-11|10:14:34|96.88%|1.80%|96.16%|746|98.92%|0.21%|98.56%|0.72%|
# |5|GuJoMo1-11|11:05:58|98.27%|1.30%|97.47%|636|99.48%|0.13%|99.42%|0.58%|
# |6|JoBeMo1-11|6:47:26|80.63%|6.58%|77.19%|370|93.02%|2.36%|90.68%|2.94%|
# |7|KaUsMo1-11|7:55:05|84.73%|5.46%|82.94%|524|95.61%|0.48%|93.62%|2.57%|
# |8|LaKaMo1-9|4:41:58|95.25%|3.01%|93.82%|554|99.64%|0.26%|99.05%|1.17%|
# |9|MaGlMo1-12|7:59:47|85.37%|20.40%|81.19%|531|98.73%|1.10%|90.75%|23.32%|
# |10|MaJaMo1-11|9:04:07|91.24%|3.03%|89.72%|585|96.40%|0.79%|95.18%|2.11%|
# |11|MaVoMo1-11|7:59:45|95.72%|1.99%|95.11%|556|98.47%|0.58%|98.04%|1.12%|
# |12|NaMaMo1-11|10:50:55|96.17%|3.30%|95.78%|966|99.37%|0.27%|98.94%|1.15%|
# |13|OlIlMo1-11|6:34:29|88.68%|4.13%|87.33%|801|96.41%|1.16%|94.76%|2.33%|
# |14|PiWiMo1-11|7:54:07|97.02%|3.14%|96.75%|559|99.15%|0.43%|99.29%|0.95%|
# |15|RoBoMo1-11|12:42:49|95.76%|1.76%|95.64%|784|98.89%|0.34%|98.71%|1.11%|
# |16|RoScMo1-11|12:45:17|91.04%|2.52%|90.37%|1211|96.84%|0.47%|95.71%|1.56%|
# |17|StHeMo1-10|6:42:04|89.51%|4.74%|87.74%|382|97.27%|1.51%|95.50%|1.51%|
# |18|SvMuMo1-12|8:53:06|79.27%|4.15%|76.73%|599|91.13%|2.42%|89.14%|3.32%|
# 

# ## Channelwise preprocessing

# Winners:
# 
# |id|files|lrate|output_channels|pool_type|kernel_shape|input_probs|time|std|valid_test|std|test|std|train|std|valid|std|
# |-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
# |5|18|0.001|40|sumlog|[1, 1]|1.0|0:45:14|0:13:11|82.96%|7.47%|82.37%|7.47%|92.79%|4.33%|83.56%|8.45%|
# |13|18|0.001|80|sumlog|[1, 1]|1.0|0:47:23|0:16:34|83.54%|8.51%|83.53%|7.42%|95.19%|2.98%|83.55%|10.47%|
# 
# With
# <pre>
# 
#         max_col_norm: [0.5],
#         max_kernel_norm: [2.],
#         preprocessor: [$channelwise],
# </pre>

# 
# |id|files|lrate|output_channels|pool_type|kernel_shape|input_probs|time|std|valid_test|std|test|std|train|std|valid|std|
# |-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
# |0|18|0.01|40|mean|[1, 1]|0.8|0:28:55|0:15:08|60.15%|14.92%|60.17%|15.07%|67.80%|15.07%|60.13%|15.54%|
# |1|18|0.001|40|sumlog|[1, 1]|0.8|0:26:02|0:11:28|65.09%|12.02%|66.82%|13.62%|76.39%|11.98%|63.36%|11.79%|
# |2|18|0.01|40|mean|[1, 12]|0.8|0:38:30|0:20:09|61.65%|15.41%|61.74%|15.99%|68.28%|14.87%|61.57%|15.44%|
# |3|18|0.001|40|sumlog|[1, 12]|0.8|0:05:38|0:02:15|27.59%|5.55%|27.55%|5.59%|27.88%|4.49%|27.63%|5.82%|
# |4|18|0.01|40|mean|[1, 1]|1.0|0:36:11|0:16:34|66.65%|16.61%|65.66%|16.04%|75.40%|15.81%|67.63%|17.63%|
# |5|18|0.001|40|sumlog|[1, 1]|1.0|0:45:14|0:13:11|82.96%|7.47%|82.37%|7.47%|92.79%|4.33%|83.56%|8.45%|
# |6|18|0.01|40|mean|[1, 12]|1.0|0:48:12|0:19:50|69.17%|14.23%|68.90%|13.85%|77.69%|11.69%|69.44%|15.04%|
# |7|18|0.001|40|sumlog|[1, 12]|1.0|0:16:38|0:29:12|32.17%|11.57%|31.91%|11.16%|33.28%|12.27%|32.44%|12.22%|
# |8|18|0.01|80|mean|[1, 1]|0.8|0:26:16|0:12:27|60.50%|15.29%|60.72%|15.92%|66.87%|15.24%|60.28%|15.10%|
# |9|18|0.001|80|sumlog|[1, 1]|0.8|0:25:24|0:12:50|64.26%|16.21%|64.48%|17.82%|76.95%|13.44%|64.03%|15.18%|
# |10|18|0.01|80|mean|[1, 12]|0.8|0:39:58|0:26:15|63.55%|15.90%|63.12%|16.76%|70.57%|14.66%|63.98%|15.36%|
# |11|18|0.001|80|sumlog|[1, 12]|0.8|0:05:29|0:01:33|26.74%|3.01%|27.02%|3.22%|27.90%|4.05%|26.46%|3.53%|
# |12|18|0.01|80|mean|[1, 1]|1.0|0:35:58|0:16:14|66.85%|15.56%|67.58%|15.46%|74.92%|13.76%|66.12%|16.54%|
# |13|18|0.001|80|sumlog|[1, 1]|1.0|0:47:23|0:16:34|83.54%|8.51%|83.53%|7.42%|95.19%|2.98%|83.55%|10.47%|
# |14|17|0.01|80|mean|[1, 12]|1.0|0:53:54|0:31:16|71.86%|10.81%|71.30%|11.88%|81.43%|7.60%|72.41%|10.67%|
# |15|17|0.001|80|sumlog|[1, 12]|1.0|0:04:43|0:01:11|27.47%|3.88%|27.63%|3.82%|28.92%|4.19%|27.31%|4.39%|

#  (von mir, ausserhalb besprechung):
#  
#  * noramlisieren, evtl. channelweise
#  * varianz/log vberechnungen durchchecke ob sie achsen korrekt beachten (channelachse sollte dableiben, restlcihe weg, also vermutlich axis=3,4... gib auch mal dimensionen aus zum check (printing node von shape, shape irgendwie adden (*0.0002 vorher oder so)...
#  * check nochmal architektur aus paper zum nachbauen

# ## Make Tables sortable

# In[2]:

get_ipython().run_cell_magic(u'javascript', u'', u'$(\'body\').append(\'<script src="https://dl.dropboxusercontent.com/u/34637013/work/ipython-notebooks/js-for-table-sorting/jquery.tablesorter.min.js"></script>\')\n// Sort tables every 5 seconds...\nvar makeTablesSortable;\nmakeTablesSortable = function() { \n  $(\'.text_cell_render table\').not(\'.sorted_table\').tablesorter().addClass(\'sorted_table\');\n}\n\nsetInterval(makeTablesSortable, 5000);')


# In[4]:

ResultPrinter('data/models/old-150-Hz/csp-remake-from-500-ms/cross-validation/early-stop/').print_results()


# In[1]:

import os

import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# switch to cpu
os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
from analysis.plot_util import plot_confusion_matrix_for_averaged_result
from motor_imagery_learning.analysis.print_results import ResultPrinter
import numpy as np

