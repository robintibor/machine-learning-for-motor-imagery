
# coding: utf-8

# # Optimize 3d conv

# In[4]:

get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
from matplotlib import cm
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7
import numpy as np


# ### Without pooling

# <pre>
# twolayer: [
#     {
#         kernel_shape: [5,5,5],
#         kernel_stride: [1,1,1],
#         pool_type: !!null '',
#         output_channels: 32,
#     },
#     {
#         kernel_shape: [5,5,5],
#         kernel_stride: [1,1,1],
#         pool_type: !!null '',
#         output_channels: 32,
#     }
# ]
# </pre>

# In[5]:

frames = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40]
cudnn_time_1 = [28.39871909883287, 30.497688235658586, 36.86676820119222, 39.187399546305336, 41.67821407318115, 60.93536218007406, 87.10423759792162, 110.82991361618042, 136.56686544418335, 200.96486806869507, 326.5988230705261, 383.6233445576259, 442.79323021570843, 481.6636172207919, 682.7227771282196, 761.7307390485491, 777.2877216339111, 833.9846928914388, 1091.5184020996094, 1178.0426025390625, 1141.5492057800293, 1177.0541667938232, 1493.9197897911072, 1583.6485028266907, 1450.8190751075745, 1550.2987504005432, 1928.7335872650146, 2049.070676167806, 1828.057050704956, 1937.4206860860188, 2374.405304590861, 2457.5870831807456, 2269.1903114318848, 2390.491247177124, 2877.6925802230835, 2967.113971710205, 2665.538549423218, 2742.56694316864, 3354.9065589904785, 3411.1114740371704]
cudnn_time_2 = [27.804924382103813, 30.08146846995634, 35.92924276987711, 38.00288041432699, 39.79520003000895, 58.41959317525228, 82.59289741516113, 106.14683628082275, 131.10361099243164, 195.36840915679932, 319.5723444223404, 379.2611530848912, 437.42529551188153, 475.18938237970525, 677.5778532028198, 756.2335559300014, 731.3331535884312, 769.8344162532261, 1033.9122295379639, 1108.857822418213, 1039.8861408233643, 1148.4792709350586, 1490.4593467712402, 1555.2870273590088, 1442.1878337860107, 1546.4495658874512, 1912.5582218170166, 2041.6415691375732, 1867.3174381256104, 1935.279893875122, 2376.0413646698, 2466.544246673584, 2276.192808151245, 2413.075590133667, 2903.196668624878, 3005.4986000061035, 2735.103988647461, 2822.366189956665, 3359.0643405914307, 3431.065893173218]
cudnn_avg_time = np.mean(np.vstack((cudnn_time_2, cudnn_time_1)), axis=0)
cublas_time = [43.603857358296715, 41.625404357910156, 45.02748648325602, 49.112606048583984, 53.977457682291664, 84.758460521698, 122.07456827163696, 160.22181510925293, 204.36103343963623, 358.01758084978377, 537.9386186599731, 714.7965431213379, 890.5950387318929, 1037.3882293701172, 1187.9215717315674, 1324.0610122680664, 1474.0572452545166, 1606.2389373779297, 1780.4551601409912, 1957.1973323822021, 2133.717632293701, 2332.1043014526367, 2458.4208488464355, 2595.7223892211914, 2832.5878143310547, 2947.459030151367, 3150.4425525665283, 3266.491651535034, 3428.037929534912, 3578.0269622802734, 3701.8962383270264, 3811.5334510803223, 3977.321195602417, 4089.6233558654785, 4231.501817703247, 4375.030755996704, 4556.079578399658, 4691.700792312622, 4801.169586181641, 4887.798070907593]
conv_2d_time = [18.5] * len(cudnn_avg_time)
pyplot.plot(frames, np.vstack((cudnn_avg_time, cublas_time, conv_2d_time)).T)
pyplot.title('Computation time for gradient of weights ')
pyplot.legend(['cudnn', 'cublas', 'conv2d'])
pyplot.xlabel('frames')
pyplot.ylabel('ms')
print("Startdiff dnn    {:4.1f}ms, {:4.1f}ms to {:4.1f}ms".format(cudnn_avg_time[0] - conv_2d_time[0], conv_2d_time[0], cudnn_avg_time[0]))
print("Startdiff cublas {:4.1f}ms, {:4.1f}ms to {:4.1f}ms".format(cublas_time[0] - conv_2d_time[0], conv_2d_time[0], cublas_time[0]))
print("Slope dnn    from 10th to 40th: {:4.1f}".format(np.polyfit(frames[9:], cudnn_avg_time[9:], deg=1)[0]))
print("Slope cublas from 10th to 40th: {:4.1f}".format(np.polyfit(frames[9:], cublas_time[9:], deg=1)[0]))


# ### With Pooling

# <pre>
# twolayerpool: [
#     {
#         kernel_shape: [5,5,5],
#         kernel_stride: [1,1,1],
#         pool_type: 'max',
#         pool_shape: [2,2,2],
#         pool_stride: [2,2,2],
#         output_channels: 32,
#     },
#     {
#         kernel_shape: [5,5,5],
#         kernel_stride: [1,1,1],
#         pool_type: 'max',
#         pool_shape: [2,2,2],
#         pool_stride: [2,2,2],
#         output_channels: 32,
#     }
# ]
# </pre>

# In[6]:

frames = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40]
cudnn_time = [48.18275769551595, 50.217906634012856, 55.54258028666178, 58.25049877166748, 60.00839869181315, 98.06850978306362, 133.12238454818726, 176.68542861938477, 215.85830450057983, 266.08779555872866, 296.06340913211596, 354.8837184906006, 389.01831553532526, 440.88006019592285, 459.5677202398127, 514.3285989761353, 572.7854039933947, 633.9787542819977, 676.6121089458466, 703.2451331615448, 745.889629636492, 827.1106992449079, 865.4785950978597, 1226.5168190002441, 946.5494950612386, 1053.467607498169, 1076.0114669799805, 1127.244758605957, 1159.6333980560303, 1260.1383686065674, 1263.6912822723389, 1328.683853149414, 1356.3221454620361, 1486.0714435577393, 1506.3107013702393, 1531.9886207580566, 1581.017827987671, 1728.3854007720947, 1727.6423454284668, 1724.3465900421143]
cublas_time = [67.1072244644165, 74.35155797887731, 73.64355666296822, 77.93822655310997, 85.7722560564677, 122.29411602020264, 147.54692316055298, 188.6695146560669, 217.44471788406372, 255.40108680725098, 296.7891552869011, 353.4650643666585, 376.20634692055836, 424.79876677195233, 455.7913216677579, 520.9712505340576, 563.562446170383, 631.7280530929565, 671.8487441539764, 763.2835592542376, 781.4262935093471, 831.8065915788923, 878.9518276850382, 952.335516611735, 978.7124395370483, 1037.1225833892822, 1071.5847492218018, 1152.4213790893555, 1180.8867931365967, 1271.7244148254395, 1257.121753692627, 1354.3242931365967, 1392.9701805114746, 1471.7755317687988, 1492.3706531524658, 1550.4231929779053, 1589.113426208496, 1679.3094158172607, 1699.2419719696045, 1745.9481716156006]
conv_2d_time = [16.5] * len(cudnn_time)

pyplot.plot(frames, np.vstack((cudnn_time, cublas_time, conv_2d_time)).T)


pyplot.title('Computation time for gradient of weights ')
pyplot.legend(['cudnn3d', 'cublas', 'conv2d'])
pyplot.xlabel('frames')
pyplot.ylabel('ms')
print("Startdiff dnn    {:4.1f}ms, {:4.1f}ms to {:4.1f}ms".format(cudnn_time[0] - conv_2d_time[0], conv_2d_time[0], cudnn_time[0]))
print("Startdiff cublas {:4.1f}ms, {:4.1f}ms to {:4.1f}ms".format(cublas_time[0] - conv_2d_time[0], conv_2d_time[0], cublas_time[0]))
print("Slope dnn    from 20th to 40th: {:4.1f}".format(np.polyfit(frames[19:], cudnn_time[19:], deg=1)[0]))
print("Slope cublas from 20th to 40th: {:4.1f}".format(np.polyfit(frames[19:], cublas_time[19:], deg=1)[0]))


# ## Smaller Test for FFT Conv

# Test crashes for fft conv with 80x80
# 
# New Input: 32x40x40x[frames]x3
# 
# !!These tests are on different GPU GTX 780 (instead of titan black) (TF queue, RZ was full)

# ### Without Pooling

# In[7]:

frames = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40]
cudnn_time = [8.063757419586182, 8.538362979888916, 9.733030796051025, 10.163307189941406, 10.510429739952087, 18.24066855690696, 25.504833459854126, 33.29026314520067, 40.46061038970947, 54.27170594533285, 75.96121893988715, 89.87766763438349, 123.30812215805054, 136.37045621871948, 160.1282835006714, 180.63803911209106, 209.40767526626587, 221.50505781173706, 242.71059036254883, 263.45990833483245, 299.8316848979277, 309.30916000815, 330.6705802679062, 352.4178663889567, 375.5632979529245, 399.2849680093619, 423.4149058659871, 449.0747054417928, 469.86863829872823, 491.74508181485265, 514.2482995986938, 539.090085029602, 562.4892976548937, 587.3234272003174, 611.4074124230278, 635.9398365020752, 660.6726348400116, 684.8213970661163, 709.4761729240417, 731.4864567347935]
cufft_time = [43.499271074930824, 43.49546432495117, 46.59678936004639, 46.89145088195801, 40.95625082651774, 50.124080975850426, 39.07111485799154, 45.84364891052246, 55.264767011006676, 57.246081034342446, 68.45250924428304, 66.02021058400472, 72.6853438786098, 73.89849424362183, 83.55256915092468, 84.47495102882385, 94.89207917993718, 96.05811891101655, 109.72356796264648, 111.44751310348511, 129.57160472869873, 131.0012698173523, 132.72889852523804, 133.24886560440063, 154.73486185073853, 156.06689453125, 155.9775948524475, 156.860613822937, 164.8772120475769, 166.29626750946045, 170.2742338180542, 171.40337228775024, 210.2639079093933, 210.32440662384033, 202.5769591331482, 202.84271240234375, 235.56134700775146, 236.7250919342041, 223.19573163986206, 223.53414297103882]
conv_2d_time = [5.5] * len(cudnn_time)

pyplot.plot(frames, np.vstack((cudnn_time, cufft_time, conv_2d_time)).T)


pyplot.title('Computation time for gradient of weights ')
pyplot.legend(['cudnn3d', 'cufft', 'conv2d'])
pyplot.xlabel('frames')
pyplot.ylabel('ms')
print("Startdiff dnn    {:4.1f}ms, {:4.1f}ms to {:4.1f}ms".format(cudnn_time[0] - conv_2d_time[0], conv_2d_time[0], cudnn_time[0]))
print("Startdiff cufft  {:4.1f}ms, {:4.1f}ms to {:4.1f}ms".format(cufft_time[0] - conv_2d_time[0], conv_2d_time[0], cufft_time[0]))
print("Slope dnn    from 10th to 40th: {:4.1f}".format(np.polyfit(frames[9:], cudnn_time[9:], deg=1)[0]))
print("Slope cufft  from 10th to 40th: {:4.1f}".format(np.polyfit(frames[9:], cufft_time[9:], deg=1)[0]))


# ### With Pooling

# In[169]:

frames = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40]
cudnn_time = [11.955457074301583, 12.236068888408381, 13.326390793449, 13.82377376295116, 14.127392164418396, 21.485764929588804, 27.72724306261217, 36.312548319498696, 43.682122230529785, 50.89714527130127, 59.729814529418945, 68.73727639516194, 78.48783639761118, 88.17654070646867, 95.40278570992606, 109.75503921508789, 114.50619697570801, 116.35665893554688, 120.2916145324707, 136.07022762298584, 141.71653985977173, 160.39273738861084, 163.6218786239624, 178.5452127456665, 184.87286567687988, 191.04379415512085, 192.7251100540161, 210.4492425918579, 213.36153745651245, 237.60058879852295, 243.69473457336426, 254.54332828521729, 261.24380826950073, 265.34913715563323, 270.2201040167558, 288.10417652130127, 290.08487860361737, 314.7948086261749, 322.541743516922, 335.73803901672363]
cufft_time = [58.358160654703774, 58.434693018595375, 64.65226014455159, 65.444016456604, 54.682413736979164, 59.17712847391764, 68.77267360687256, 78.89403746678279, 90.57456514109735, 85.53569515546162, 98.34197589329311, 104.26206588745117, 115.25682210922241, 120.58292627334595, 124.17541742324829, 125.67157745361328, 136.7899775505066, 145.0741410255432, 156.7559003829956, 165.34607410430908, 179.65822219848633, 188.16076517105103, 192.21251010894775, 197.2588300704956, 213.83116245269775, 222.91566133499146, 224.0998387336731, 232.42294788360596, 238.94164562225342, 249.78700876235962, 255.9121608734131, 258.6515545845032, 283.83884165022107, 294.5016692666446, 295.4094409942627, 301.8806541667265, 322.20833003520966, 332.5199633836746, 330.89007437229156, 333.8907877604167]
conv_2d_time = [4.8] * len(cudnn_time)

pyplot.plot(frames, np.vstack((cudnn_time, cufft_time, conv_2d_time)).T)


pyplot.title('Computation time for gradient of weights ')
pyplot.legend(['cudnn3d', 'cufft', 'conv2d'])
pyplot.xlabel('frames')
pyplot.ylabel('ms')
print("Startdiff dnn    {:4.1f}ms, {:4.1f}ms to {:4.1f}ms".format(cudnn_time[0] - conv_2d_time[0], conv_2d_time[0], cudnn_time[0]))
print("Startdiff cufft  {:4.1f}ms, {:4.1f}ms to {:4.1f}ms".format(cufft_time[0] - conv_2d_time[0], conv_2d_time[0], cufft_time[0]))
print("Slope dnn    from 20th to 40th: {:4.1f}".format(np.polyfit(frames[19:], cudnn_time[19:], deg=1)[0]))
print("Slope cufft  from 20th to 40th: {:4.1f}".format(np.polyfit(frames[19:], cufft_time[19:], deg=1)[0]))


# ### With Stride

# <pre>
# twolayerstride: [
#     {
#         kernel_shape: [5,5,5],
#         kernel_stride: [5,5,5],
#         pool_type: !!null '',
#         output_channels: 32,
#     },
#     {
#         kernel_shape: [5,5,5],
#         kernel_stride: [5,5,5],
#         pool_type: !!null '',
#         output_channels: 32,
#     }
# ]
# </pre>

# In[172]:

frames = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40]
cudnn_time = [1.447904109954834, 1.621100902557373, 1.837921142578125, 2.0056533813476562, 2.170131206512451, 2.315659523010254, 2.429068088531494, 2.5905394554138184, 2.716820240020752, 3.281559944152832, 3.4491920471191406, 3.575427532196045, 3.7331604957580566, 3.8752031326293945, 4.366767406463623, 4.510455131530762, 4.644830226898193, 4.808907508850098, 4.939768314361572, 5.37806510925293, 5.520279407501221, 5.656068325042725, 5.818450450897217, 5.940771102905273, 6.317098140716553, 6.445052623748779, 6.5755486488342285, 6.7407965660095215, 6.8685126304626465, 7.158029079437256, 7.296044826507568, 7.431018352508545, 7.576136589050293, 7.748537063598633, 8.093409538269043, 8.212072849273682, 8.358488082885742, 8.487672805786133, 8.62635850906372, 8.910877704620361]
cufft_time = [18.486963618885387, 35.436455408732094, 51.39876206715902, 68.35817495981853, 83.91622702280681, 84.09025271733601, 83.17239761352539, 84.74627137184143, 86.21952931086223, 157.6024055480957, 162.14653253555298, 156.61627054214478, 203.36369276046753, 212.0991587638855, 249.93540048599243, 251.84152126312256, 262.71889209747314, 267.1747458608527, 269.06319668418485, 340.4804547627767, 345.4631010691325, 343.8385645548503, 350.79018274943036, 353.0624548594157, 425.61739683151245, 427.1818995475769, 433.78353118896484, 430.75668811798096, 444.83373562494916, 510.2428436279297, 517.3937559127808, 524.9135971069336, 520.9887266159058, 534.3308687210083, 592.7390522427029, 602.4008327060276, 592.6602416568333, 600.084039900038, 600.9018156263563, 669.9934899806976]
conv_2d_time = [19.0] * len(cudnn_time)

pyplot.plot(frames, np.vstack((cudnn_time, cufft_time, conv_2d_time)).T)


pyplot.title('Computation time for gradient of weights ')
pyplot.legend(['cudnn3d', 'cufft', 'conv2d'])
pyplot.xlabel('frames')
pyplot.ylabel('ms')
print("Startdiff dnn    {:4.1f}ms, {:4.1f}ms to {:4.1f}ms".format(cudnn_time[0] - conv_2d_time[0], conv_2d_time[0], cudnn_time[0]))
print("Startdiff cufft  {:4.1f}ms, {:4.1f}ms to {:4.1f}ms".format(cufft_time[0] - conv_2d_time[0], conv_2d_time[0], cufft_time[0]))
print("Slope dnn    from 20th to 40th: {:4.1f}".format(np.polyfit(frames[19:], cudnn_time[19:], deg=1)[0]))
print("Slope cufft  from 20th to 40th: {:4.1f}".format(np.polyfit(frames[19:], cufft_time[19:], deg=1)[0]))


# ## Performance Improvement 

# Conv 3d without optimizations
# <pre>
# Gradient Single layer     runtime per iteration (   9 iterations): 224.0529ms
# Gradient Single layer     runtime per iteration (  10 iterations): 205.5625ms
# Gradient Single layer     runtime per iteration (  10 iterations): 217.7420ms
# </pre>
# 
# <pre>
# time       sum         apply time   time per call #call id Apply name
#   56.4%    56.4%       1.896s       1.26e-01s     15    20   GpuDnn3dConvGradI{inplace=False}(GpuContiguous.0, GpuContiguous.0, GpuAllocEmpty.0, Constant{PyCObject...b}, Constant{1.0}, Constant{0.0})
#   16.4%    72.7%       0.550s       3.67e-02s     15    22   HostFromGpu(GpuDimShuffle{0,2,3,4,1}.0)
#   14.5%    87.2%       0.487s       3.25e-02s     15    18   GpuAlloc(CudaNdarrayConstant{[[[[[ 1.]]]]]}, Shape_i{0}.0, Shape_i{0}.0, Elemwise{Composite{(i0 + ((i1 - i2) // i0))}}[(0, 1)].0, Elemwise{Composite{(i0 + ((i1 - i2) // i0))}}[(0, 1)].0, Elemwise{Composite{(i0 + ((i1 - i2) // i0))}}[(0, 1)].0)
#   10.5%    97.8%       0.354s       2.36e-02s     15     5   GpuFromHost(TensorType(float32, 5D))
#    2.1%    99.9%       0.072s       4.81e-03s     15    16   GpuContiguous(GpuDimShuffle{0,4,1,2,3}.0)
# 
# </pre>
# 
# with gpu_form_host at end:
# 
# <pre>
# Gradient Single layer     runtime per iteration (  20 iterations): 191.2825ms
# 66.1% in 3dconvgradI
# </pre>
# 
# with optimizations added in theano dnn3d:
# <pre>
# Gradient Single layer     runtime per iteration (  20 iterations): 182.8178ms
# </pre>

# First results:   
# <pre>
# python perf_convs.py --inputs 20 20 20 20 3 --filters 7 3 6 2 3      
# Input shape  [20, 20, 20, 20, 3]
# Filter shape [7, 3, 6, 2, 3]
# Output shape (20, 18, 15, 19, 7)
# #Inputs   480000
# #Weights     756
# #Outputs  718200
# #Multiplications 542959200
# Theano 3d                 runtime per iteration (  14 iterations): 143.8815ms
# Theano 3d2d               runtime per iteration (  78 iterations):  12.9532ms
# Python Vectorized         runtime per iteration (  25 iterations):  82.0083ms
# Cudamat Vectorized        runtime per iteration (  23 iterations):  87.3997ms
# Theano Mul Vectorized     runtime per iteration (  28 iterations):  72.9542ms
# GPU Loop                  runtime per iteration (   1 iterations): 20829.8290ms
# 
# 
# </pre>

# After Cudnn theano 3d2d faster:
# 
# <pre>
# python perf_convs.py --inputs 20 20 20 20 3 --filters 7 3 6 2 3
# Using gpu device 0: GeForce GTX 780
# Batches/Filters, rows, columns, times, channels
# Input shape  [20, 20, 20, 20, 3]
# Filter shape [7, 3, 6, 2, 3]
# Output shape (20, 18, 15, 19, 7)
# #Inputs   480000
# #Weights     756
# #Outputs  718200
# #Multiplications 542959200
# 
# Theano 3d                 runtime per iteration (  14 iterations): 143.8013ms
# Theano 3d2d               runtime per iteration ( 286 iterations):   3.5014ms
# Python Vectorized         runtime per iteration (  25 iterations):  80.9831ms
# Cudamat Vectorized        runtime per iteration (  24 iterations):  86.5586ms
# Theano Mul Vectorized     runtime per iteration (  28 iterations):  71.6942ms
# GPU Loop                  runtime per iteration (   1 iterations): 20715.0171ms
# 
# </pre>

# In[1]:

# 80 x 80 x 3 image input with 5 - 40 frames batch size 64, 256
# 5 x  5 x 3 x 5(zeit) filter, 128 davon
# wenn im vergleich yuzu 2d nur um faktor zeit oder ein bisschen mehr groesser dann ok


# In[2]:

# wrap as a layer usable in theano:  https://github.com/benanne/Lasagne/blob/master/lasagne/layers/corrmm.py


# In[ ]:

# or pylearn2


# ## Debug

# In[1]:

import os
os.sys.path.insert(0,'/lokal/schirrmr/3dconv/') 
get_ipython().magic(u'cd /lokal/schirrmr/3dconv/')
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')


# In[23]:

from layers.conv_3d_theano import Conv3dElemwise, Conv3DSpace
import numpy as np
from pylearn2.models.mlp import IdentityConvNonlinearity
import theano
import theano.tensor as T


# In[3]:

class FakeMLP():
    def __init__(self,rng,batch_size):
        self.rng = rng
        self.batch_size = batch_size


# In[24]:

inputs_shape = [10,6,6,6,3]
filters_shape = [11,5,5,5,3]
mlp = FakeMLP(rng=np.random,batch_size=inputs_shape[0])
conv_3d_layer = Conv3dElemwise(output_channels=filters_shape[0], 
        kernel_shape=filters_shape[1:4],
        layer_name='conv3d_lin', nonlinearity=IdentityConvNonlinearity(),
        irange=0.001)
conv_3d_layer.set_mlp(mlp)
conv_3d_input_space = Conv3DSpace(inputs_shape[1:4], num_channels=inputs_shape[4], axes=('b',0,1,'t','c'))
conv_3d_layer.set_input_space(conv_3d_input_space)


# In[53]:

print conv_3d_layer.input_space
print conv_3d_layer.detector_space
print conv_3d_layer.output_space
print conv_3d_layer.detector_space.get_origin().shape


# In[36]:

ftensor5 = T.TensorType('float32', (False,)*5)
inputs_3d_layer_theano = ftensor5()
conv3d__layer_result = conv_3d_layer.fprop(inputs_3d_layer_theano)


conv3d_fprop = theano.function([inputs_3d_layer_theano], conv3d__layer_result)


# In[38]:

inputs_3d_nnet_theano = ftensor5()
filters_3d_nnet_theano = ftensor5()
bias_3d_nnet_theano = T.fvector()

conv3d_nnet_result = theano.tensor.nnet.conv3D(inputs_3d_nnet_theano, filters_3d_nnet_theano, bias_3d_nnet_theano, d=(1,1,1))

conv3d_nnet = theano.function([inputs_3d_nnet_theano, filters_3d_nnet_theano, bias_3d_nnet_theano], conv3d_nnet_result)


# In[54]:

# compare here theano 3dconv result and result from using this layer.. fix filters inputs etc.. use set_weights etc
# create random data with size of input and half positive half negative labels
# create a proper mlp model for this layer with softmax as second layer, see if it is possible to train it


# In[50]:

inputs = np.random.rand(*inputs_shape).astype('float32')
filters = np.random.rand(*filters_shape).astype('float32')
bias = np.zeros(filters.shape[0]).astype('float32')
#bias = np.random.rand(filters.shape[0]).astype('float32')


# In[51]:

correct_result = conv3d_nnet(inputs, filters, bias)
conv_3d_layer.set_weights(filters)
layer_result = conv3d_fprop(inputs)

assert np.sum(np.square(correct_result - layer_result)) < 1e-4


# In[ ]:

targets = np.zeros(inputs.shape.[0]).astype('float32')
# later half of targets as positive
# add 1 to later half (later try going down to make it harder)
# create model: 3dlayer->softmax
# algorithm: pure sgd
# make output before and compare, compute accuracy
# for proper tests fix seed of random stuff

