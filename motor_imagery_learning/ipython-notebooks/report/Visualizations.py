
# coding: utf-8

# In[4]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nimport os\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n#os.environ[\'CUDA_LAUNCH_BLOCKING\'] = \'1\'\n#os.environ[\'THEANO_FLAGS\'] = os.environ[\'THEANO_FLAGS\'] + ",profile=True"\n# switch to cpu\n#os.environ[\'THEANO_FLAGS\'] = \'floatX=float32,device=cpu,nvcc.fastmath=True\'\n%load_ext autoreload\n%autoreload 2\n\nfrom motor_imagery_learning.bbci_pylearn_dataset import (compute_power_spectra)\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset\nimport numpy as np\n\nimport matplotlib\nfrom matplotlib import pyplot\nfrom matplotlib import cm\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (8.0, 2.0)\nmatplotlib.rcParams[\'font.size\'] = 7\n\nfrom pylearn2.costs.mlp import Default\nfrom pylearn2.models.mlp import Softmax, MLP\nfrom pylearn2.termination_criteria import EpochCounter\nimport seaborn\nseaborn.set_style(\'darkgrid\')\nfrom copy import deepcopy\nfrom motor_imagery_learning.train_scripts.train_experiments import ExperimentsRunner\nfrom motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer\nfrom motor_imagery_learning.analysis.data_generation import (create_sine_signal,\n    pipeline, create_shifted_sines,put_noise, convolve_with_weight,\n    max_pool_topo_view, log_sum_pool)\nfrom motor_imagery_learning.analysis.util import create_trainer\nfrom motor_imagery_learning.mypylearn2.conv_squared import ConvSquared\nfrom motor_imagery_learning.analysis.input_reconstruction import (\n    create_rand_input, gradient_descent_input, create_input_grad_for_wanted_class_func,\n    create_input_grad_with_smooth_penalty_for_wanted_class_func, \n    reconstruct_inputs_for_class)\n\nfrom motor_imagery_learning.analysis.plot_util import (plot_misclasses_for_model, \n    plot_nlls_for_model, plot_sensor_signals)\n\nimport h5py\nimport numpy as np\nfrom pylearn2.utils import serial\nfrom pprint import pprint\nfrom  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted\nimport h5py\nfrom motor_imagery_learning.csp.train_csp import CSPTrain\nfrom motor_imagery_learning.mypylearn2.train import TrainEarlyStop\nfrom motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD\nfrom motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset\nfrom motor_imagery_learning.bbci_dataset import BBCIDataset\nfrom pylearn2.costs.mlp import Default\nfrom pylearn2.models.mlp import Softmax, MLP\nfrom pylearn2.termination_criteria import EpochCounter\nfrom sklearn.cross_validation import KFold\nfrom pylearn2.datasets.dense_design_matrix import DenseDesignMatrix\nimport numpy as np\nfrom numpy.random import RandomState\nimport theano.tensor as T\nimport theano\nfrom pylearn2.space import Conv2DSpace\nfrom pylearn2.models.mlp import ConvElemwise\nfrom theano.sandbox.cuda.dnn import dnn_conv\nfrom theano.sandbox.cuda.basic_ops import gpu_alloc_empty\nfrom motor_imagery_learning.analysis.print_results import ResultPrinter\nfrom pylearn2.utils import serial\nfrom motor_imagery_learning.analysis.data_generation import generate_single_sensor_data\nfrom motor_imagery_learning.analysis.plot.raw_signals import (\n    plot_sensor_signals_two_classes)\nfrom motor_imagery_learning.mypylearn2.pool import PoolLayer, log_sum\nfrom motor_imagery_learning.analysis.util import bps_and_freqs\nfrom motor_imagery_learning.analysis.deconv import deconv_model\nfrom motor_imagery_learning.analysis.plot_util import plot_head_signals_tight_two_signals\nfrom motor_imagery_learning.analysis.plot.raw_signals import (\n    plot_sensor_signals_multiple_classes)\nfrom motor_imagery_learning.analysis.util import  create_activations_func\nfrom motor_imagery_learning.train_scripts.train_with_params import (\n    create_training_object_from_file)\nfrom motor_imagery_learning.analysis.plot_util import plot_sensor_signals')


# In[2]:

get_ipython().run_cell_magic(u'javascript', u'', u"IPython.load_extensions('cite2c/main');")


# In[2]:

get_ipython().run_cell_magic(u'html', u'', u'<style>\ndiv.input {\nwidth: 105ex; /* about 80 chars + buffer */\n...\n}\ndiv.text_cell {\nwidth: 105ex /* instead of 100%, */\n...\n}\n\ndiv.text_cell_render {\n/*font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;*/\nfont-family: "Charis SIL", serif; /* Make non-code text serif. */\nline-height: 145%; /* added for some line spacing of text. */\nwidth: 105ex; /* instead of \'inherit\' for shorter lines */\n...\n}\n\n/* Set the size of the headers */\ndiv.text_cell_render h1 {\nfont-size: 18pt;\n}\n\ndiv.text_cell_render h2 {\nfont-size: 14pt;\n}\n\n\n\n.CodeMirror {\nfont-family: Consolas, monospace;\n}\n\n.todo {\n    background-color: hsla(120, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n.todecide {\n    background-color: hsla(240, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n\n</style>\n\n<style>\ndiv.summary  {\n    background-color: hsla(40,0%,50%,0.2);\n    color:black;\n    border-radius: 5px;\n    padding: 2px;\n    margin-top:0px;\n}\ndiv.summary ul\n{\n    list-style-type: none;\n}\n</style>\n<link rel="stylesheet" href="./motivation.css" type="text/css">\n<script async defer src="//hypothes.is/embed.js"></script>')


# In[3]:

def get_reconstructions_artificial_data():
    model = get_artificial_model()
    in_grad_func = create_input_grad_for_wanted_class_func(model)
    batches = 3
    start_in = create_rand_input(model, batches)
    ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000)
    inputs_class_1 = gradient_descent_input(start_in, in_grad_func, [1,0], **ascent_args)
    inputs_class_2 = gradient_descent_input(start_in, in_grad_func, [0,1], **ascent_args)
    return inputs_class_1, inputs_class_2

def get_reconstructions_kaus_data():
    kaus_topo, kaus_y = generate_single_sensor_data(newfs=150)
    # convolve + square + maxpool
    layers = [ConvSquared(output_channels=1, kernel_shape=[150,1], pool_shape=[50,1], pool_stride=[50,1],
                         pool_type="sumlog",layer_name="conv_squared", irange=0.05, max_kernel_norm=2.0, tied_b=True),
              Softmax(n_classes=2, layer_name='y', irange=0.001, max_col_norm=0.5)]
    trainer = create_trainer(kaus_topo, kaus_y,max_epochs=100, layers=layers, cost=None)
    
    trainer.run_until_earlystop()
    in_grad_func = create_input_grad_for_wanted_class_func(trainer.model)
    batches = 3
    start_in = create_rand_input(trainer.model, batches)
    ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000)
    inputs_class_1 = gradient_descent_input(start_in, in_grad_func, [1,0], **ascent_args)
    inputs_class_2 = gradient_descent_input(start_in, in_grad_func, [0,1], **ascent_args)
    return inputs_class_1, inputs_class_2
    
def get_artificial_model():
    input_shape=(600,1,600,1)
    start_topo_view, y = create_shifted_sines(input_shape, RandomState(np.uint64(hash("pooled_pipeline"))), 
                                              sampling_freq=150.0)
    topo_view_noised, y = pipeline(start_topo_view, y,
                             lambda x,y: put_noise(x, weight_old_data=0.05))
    # convolve + square + maxpool
    layers = [ConvSquared(output_channels=1, kernel_shape=[150,1], pool_shape=[-1,1], pool_stride=[-1,1],
                         pool_type=None,layer_name="conv_squared", irange=0.05, max_kernel_norm=2.0, tied_b=True),
              PoolLayer(pool_func=log_sum, layer_name="pool_squared", pool_shape=[50,1], pool_stride=[50,1]),
              Softmax(n_classes=2, layer_name='y', irange=0.001, max_col_norm=0.5)]
    trainer = create_trainer(topo_view_noised, y,max_epochs=100, layers=layers, cost=None)

    trainer.run_until_earlystop()
    return trainer.model
    
from pylearn2.models.mlp import ConvElemwise, IdentityConvNonlinearity
from motor_imagery_learning.mypylearn2.nonlinearities import (
    NonlinearityLayer, square_nonlin)

def transform_raw_net_model(model):
    transformed_model = deepcopy(model)
    oldlayer = transformed_model.layers[1]
    new_layer = ConvElemwise(output_channels=oldlayer.output_channels, 
                             kernel_shape=oldlayer.kernel_shape,
                             layer_name='new_layer',
                             nonlinearity=IdentityConvNonlinearity(),
                             kernel_stride=oldlayer.kernel_stride,
                             tied_b=True,
                             irange=0.01
                            )
    new_layer.mlp = lambda: None
    new_layer.mlp.rng = RandomState(92138)
    new_layer.mlp.batch_size = 2
    new_layer.set_input_space(oldlayer.get_input_space())
    new_layer.set_weights(oldlayer.get_param_values()[0])
    new_layer.set_biases(oldlayer.get_param_values()[1])
    nonlin_layer = NonlinearityLayer(layer_name='square_nonlin', 
                                 nonlinearity_func=square_nonlin)
    nonlin_layer.set_input_space(new_layer.get_output_space())
    transformed_model.layers = [transformed_model.layers[0],
                            new_layer, nonlin_layer, 
                           transformed_model.layers[2],
                           transformed_model.layers[3]]
    return transformed_model

def transform_to_patterns(combined_weights, train_topo, acts_out):
    """Transform raw net combined filters to combined patterns.
    Assumes kernel shape 30"""
    acts_out_for_cov = acts_out[1].transpose(1,0,2,3)
    acts_out_for_cov = acts_out_for_cov.reshape(acts_out_for_cov.shape[0], -1)
    acts_cov = np.cov(acts_out_for_cov)
    new_train_topo = np.empty((train_topo.shape[0], train_topo.shape[1] * 30,
                             train_topo.shape[2] - 30 + 1, 1))


    n_chans = train_topo.shape[1]
    n_samples = 30

    for i_sample in xrange(n_samples):
        end = -30+1+i_sample
        if end == 0:
            end = None
        for i_chan in xrange(n_chans):
            new_train_topo[:, i_chan*n_samples+i_sample, :] =                 train_topo[:,i_chan,i_sample:end]

    new_train_topo_for_cov = new_train_topo.transpose(1,0,2,3)

    new_train_topo_for_cov = new_train_topo_for_cov.reshape(
        new_train_topo_for_cov.shape[0], -1)
    new_train_topo_cov = np.cov(new_train_topo_for_cov)
    combined_vectors = combined_weights.reshape(combined_weights.shape[0],-1)
    transformed_vectors = np.dot(new_train_topo_cov, combined_vectors.T).T
    transformed_vectors = np.dot(transformed_vectors.T, acts_cov).T
    patterns = transformed_vectors.reshape(*combined_weights.shape)
    return patterns


# In[ ]:

## plan to redo motivation for using covariance in netoworks first layer:

# we know strong covariances between sensors exist class independently, especially for close sensors
# it is useful for decoding to cancel them out by assigning negative weights to sensors around
# by multiplying with the covariances we will alleviate this effect
# (hope it will show more braodly which region a class-relevant signal is projected to)
# 



# # Visualizations 

# <div class="summary">
# <ul>
# <li>It can be difficult to show what a trained model has learned</li>
# <li>Interpreting weights has to be done carefully</li>
# <li>Visualizations can give clues what a model might have learned</li>
# </ul>
# </div>

# In this chapter, we will:
# 
# * motivate why we want to understand what our models have learned
# * explain one of the difficulties of understanding (discriminatively) trained models in ...
# * show how this difficulty can be alleviated using the covariance of the features in ...
# * explain the additional difficulties of understanding convolutional networks 
# * show what we can visualize from our networks
# 
# Building on this, we will visualize our networks in ... We aim to show indications that:
# 
# * all of them learn the spatially localized event-related synchronization/desynchronization features
# * the raw net additionally learns to use the temporal dynamics of the ERD/ERS
# * the raw net additionally learns use a specific potential at the start of a trial

# ## Motivation

# Once you have trained a machine learning model, you are often interested to know what it has learned. In our case, we are interested to know how our model uses the signal:
# 
# * How is the model weighing the sensors?
# * What frequencies is it using for which class? 
# * Or in the case of the raw net, is it using only oscillations or something else? 
# 
# Also, we want to know what we can learn from the model about the brain and motor imagery. For example, can the sensor weights show us where in the brain the important signals are generated?
# 
# 

# ### Example for Difficulties

# It is often not straightforward to interpret what a model has learned. We will present a simple example where looking at the weights of the model might lead to misinterpretations due to a distractor signal affecting several sensors.
# 
# Consider a case with two sensors S1 and S2 with values $x_1(t)$ and $x_2(t)$, where you want to classify each sample $x_1(t)$, $x_2(t)$ as either class left or right. Assume, all the class-relevant information is contained in S1: For class left, $x_1$ will always be positive and for class right, $x_1$ will always be negative. Further assume, $x_2$ is always just zero. 

# In[6]:

target = [-1] * 10 + [1] * 10
sensor_1 = [-1] * 10 + [1] * 10
sensor_2 = [0] * 20
fig = plot_sensor_signals(np.array([target, sensor_1, sensor_2]),
                          ('Class', 'Sensor 1', 'Sensor 2'), sharey=False, figsize=(4,2))

fig.axes[0].grid(False, axis='y')
fig.axes[0].set_yticks((-0.8,0.8))

fig.axes[0].set_yticklabels(('Left','Right'), fontsize=8)
fig.suptitle('Simple example without distractor', fontsize=14, y=1.02)
None


# This case can be completely unproblematic. A linear classifier could assign weight 1 to sensor 1 and weight 0 to sensor 2 (and use a threshold of zero to distinguish class right from left). The interpretation would be easy: The information is contained in sensor 1. Now assume, we add the same distractor signal to both sensors, which has no correlation to the class at all. In our case, we will add the same sine wave to both sensors. In EEG motor decoding, this kind of "noise" could arise if we have an oscillation like the heartbeat or some oscillation within in the brain that is not related to the movement.

# In[8]:

noise = create_sine_signal(20,freq=2, sampling_freq=20) * 2.5
noised_sensor_1 = sensor_1 + noise
noised_sensor_2 = sensor_2 + noise


# In[10]:

fig = plot_sensor_signals(np.array([target, noised_sensor_1, noised_sensor_2]), 
                          ('Class', 'Sensor 1', 'Sensor 2'), 
                          sharey=False,
                          figsize=(4,2))
fig.axes[0].grid(False, axis='y')
fig.axes[0].set_yticks((-0.8,0.8))

fig.axes[0].set_yticklabels(('Left','Right'), fontsize=8)

fig.suptitle('Simple example with distractor', fontsize=14, y=1.02)
None


# Now, merely assigning a positive weight to sensor 1 and weight 0 to sensor 2 could not separate the classes anymore (sensor 1 has both positive and negative samples for the left class and also for the right class).
# Instead, a linear classifier could still classify this perfectly by assigning weight 1 to sensor 1 and weight -1 to sensor 2. This removes the distractor and leads to a perfect prediction again:

# In[12]:

fig = plot_sensor_signals(np.array([target, noised_sensor_1 - noised_sensor_2]), 
                          ('Class', 'Sensor 1 - Sensor 2'), 
                          sharey=False,
                          figsize=(5,1.5))
fig.axes[0].grid(False, axis='y')
fig.axes[0].set_yticks((-0.8,0.8))

fig.axes[0].set_yticklabels(('Left','Right'), fontsize=8)
fig.suptitle('Simple example with distractor removed', fontsize=14, y=1.05)
None


# Just looking at the weights of the model can now lead to misinterpretations. We know that sensor 1 contains the information and is actually the same as the target, plus our distractor signal, whereas sensor 2 only contains the distractor signal. However, the negative weight could also imply that sensor 2 contains a class-discriminative signal, namely that sensor 2 is typically negative for class right and positive for class left.

# 
# To sum up, the weights in a model trained only to discriminate between classes typically reflect atleast two things: the relationship between the weighted features and the output and the covariances of the noise among the features. For a longer discussion about interpreting weights of machine learning models in the context of EEG, see <a data-cite="349468/D6IVJ2G2" target="_blank" href="http://ieeexplore.ieee.org/xpls/icp.jsp?arnumber=4408441">(Blankertz et al., 2008)</a>, <a data-cite="349468/SI5ABXCA"target="_blank" href="http://www.sciencedirect.com/science/article/pii/S1053811910009067">(Blankertz et al., May 15, 2011)</a> and <a data-cite="349468/DHKZP7IQ"target="_blank" href="http://www.sciencedirect.com/science/article/pii/S1053811913010914">(Haufe et al., February 15, 2014)</a>.

# ## Using the covariance to compute activation patterns

# <div class="summary">
# <ul>
# <li>Covariance of the signals can be used to compute interpretable patterns</li>
# <li>You can try to visualize the network directly or show its reaction to different inputs</li>
# </ul>
# </div>
# 
# As we have shown, a linear discriminative model can have misinterpretable weights. This is due to covariances between the sensors. We can compute a more interpretable *activation pattern* from our weight vector. One computes the entry in the activation pattern for a specific sensor by  multiplying the weight of that sensor with its variance and the weights of the other sensors with their covariance like this:
# $$ a_i = \sum_{j} w_i \cdot cov(x_i,x_j) $$ 
# 
# We get our entire activation pattern in matrix notation like this:
# 
# $$ A = \Sigma_Xw$$
# 
# Intuitively, this makes the activation for a sensor reflect a combination its own weight and the weights of sensors it has high covariances with. It can be shown that this transforms a discriminative model into a corresponding generative model <a data-cite="349468/DHKZP7IQ"target="_blank" href="http://www.sciencedirect.com/science/article/pii/S1053811913010914">(Haufe et al., February 15, 2014)</a>. While the discriminative model answers the question how to reconstruct the source/class from given data, the generative model answers the question we are more interested in: Given a source/class, how does the data look like?
# 
# For our example, we have $w_1= 1$, $w_2=-1$, $var(x_1) = 4.35$, $var(x_2) = 3.29$ and $cov(x_1,x_2) = 3.29$. If we use these to compute the activation pattern, we get $a_1 = 1.05$  and $a_2 = 0$, correctly showing that the class is not encoded in sensor 2 at all.
# 
# 
# 

# ## Visualizing Networks Methods

# <div class="summary">
# <ul>
# <li>It is difficult to show what exactly a network has learned</li>
# <li>You can try to visualize the network directly or show its reaction to different inputs</li>
# </ul>
# </div>

# Understanding what a convolutional neural network has learned can be difficult. A network can have a large number of weights an additionally, weights of one unit interact with weights of other units to produce the nets' output. 
# 
# To show what the network has learned, one can either only use only the network itself (*network-centric*) or use the network together with input data (*dataset-centric*, e.g., to highlight what parts of the input the network focusses on) <a data-cite="349468/XH48TN6Q"target="_blank" href="http://arxiv.org/abs/1506.06579">(Yosinski et al., 2015-06-22)</a>. For our networks, we will show several things to get an idea how the network classifies our data. First, we will show the weights of the network and transform them to activation patterns. Second, we will reconstruct the desired input of a unit using the gradient with regard to the input.

# We will use a running examples to illustrate the techniques:
# A raw net trained on data from two sensors, C3 and C4 for the classes right hand and left hand.

# ### Weights

# We will visualize weights despite the pitfalls mentioned before. We do so, since we expect atleast very broad phenomena like the lateralization of the signal for a certain class to not be completely obscured by the covariance of distractor signals/noise.
# 
# For the raw net, we can combine the weights of the first two layers, since there is no activation inbetween. For that, we have to compute the dot product of the two weight tensors. This leads to "combined filters" with the dimensions #sensors x #kernel_time_shape. Our kernel time shape is 30.
# 
# You can see a lateralization when comparing combined filters 1 and 4 to combined filters 2 and 3. Also note that combined filter 1 is pretty much a vertically flipped version of combined filter 4.

# In[5]:

bhno_right_left_model = serial.load(
    'data/models/two-class/raw-square/increasing-filters/test/36.pkl').model
temporal_weights = bhno_right_left_model.get_weights_topo()[:,::-1,::-1,:]

spat_filt_weights = bhno_right_left_model.layers[1].get_weights_topo()[:,::-1,::-1,:]
combined_weights = np.tensordot(spat_filt_weights, temporal_weights, axes=(3,0))

combined_weights = combined_weights.squeeze()


# In[6]:


filter_names = ['Combined Filter ' + str(i_weight + 1) for i_weight in xrange(4)]
fig = plot_sensor_signals_multiple_classes(combined_weights, ['C3', 'C4'],
                                           filter_names,
                                           figsize=(2,4))
for i_ax in [0,3,5,6]:
    fig.axes[i_ax].axvspan(0, 30, color=seaborn.color_palette()[2], alpha=0.2, lw=0)

pyplot.xticks(range(0,31,5), map(lambda ms: "{:.0f}".format(ms),
                                     np.arange(0,31,5) * 1000 / 150.0))
pyplot.xlabel("Time [ms]")
None


# Now we can look at the final weights of the softmax layer. The softmax layer has weights for each filter and each time-pooled output from the layer before. You can see that the net seems more sensitive to the first half of the trial.
# The weights are plausible. For example, filter 1, which matches an oscillation in sensor C3, has larger weights for left hand and smaller weights for right hand. This is consistent with a *decreasing* oscillation on the left-sided C3 sensor for the right hand.

# In[27]:

x_pools = np.arange(0,53)
x_times = np.arange(0,521, 10) * 1000 / 150.0
x_times = np.array(["{:.1f}".format(time) for time in x_times])
soft_weights = bhno_right_left_model.layers[-1].get_weights_topo().squeeze()
sensor_names = map(lambda i: "Combined Filter {:d}".format(i), range(1,5))
fig = plot_sensor_signals(soft_weights[0].T, sensor_names, figsize=(3,2.5))
fig.suptitle(
    "Right Hand Softmax Weights", y=1.02, fontsize=16)
fig.axes[1].axvspan(0, 52, ymin=0.5, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
fig.axes[2].axvspan(0, 52, ymin=0.5, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
pyplot.xticks([0,15,30,45,60], [0,1000,2000,3000,4000])
pyplot.xlabel('Time [ms] (at start of pool region)')
fig = plot_sensor_signals(soft_weights[1].T, sensor_names, figsize=(3,2.5))
fig.suptitle(
    "Left Hand Softmax Weights", y=1.02, fontsize=16)
fig.axes[3].set_xlim(0,60)
pyplot.xticks([0,15,30,45,60], [0,1000,2000,3000,4000])
pyplot.xlabel('Time [ms] (at start of pool region)')
fig.axes[0].axvspan(0, 52, ymin=0.5, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
fig.axes[3].axvspan(0, 52, ymin=0.5, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
None


# ### Patterns

# We now also attempt to answer the question: How is the output of a certain unit encoded in the input (either the raw input or the input of the layer before)?
# 
# We cannot just multiply the covariance of the sensors with out weights anymore, since we have two differences to our linear classifier example. First, our weights also have a temporal dimension, they span 30 samples. Second, the outputs of the different units of our layer are not uncorrelated. We solve the first problem by adding new virtual channels V, that are time-translated. For every samplenumber between 1 and 30 we add new channels with the original input, translated forwards in time by the respective number of samples.
# We solve the second problem by also multiplying with the inverse of the layer outputs' covariance (see <a data-cite="349468/DHKZP7IQ"target="_blank" href="http://www.sciencedirect.com/science/article/pii/S1053811913010914">(Haufe et al., 2014)</a> for the derivation of this). The computed patterns then answer the question we posed at the start, i.e. they estimate the pattern matrix in:
# 
# $$ input = Pattern \cdot outputs + noise$$
# $$ x(n) = A o(n) + \epsilon(n)$$
# 
# We show these patterns here. We still see the lateralization, but we see that some of the signal is always present in both sensors. The signal is also more smooth, with the higher frequencies removed.

# In[8]:

get_ipython().run_cell_magic(u'capture', u'', u"transformed_model = transform_raw_net_model(bhno_right_left_model)\n\nact_func_new = create_activations_func(transformed_model)\n\nact_func_old = create_activations_func(bhno_right_left_model)\ninputs = create_rand_input(bhno_right_left_model,3)\nact_outs = act_func_new(inputs)\nact_outs_old = act_func_old(inputs)\n\nact_outs_restricted = deepcopy(act_outs)\nact_outs_restricted.pop(1) # remove activations not exsting in other model\nassert all([np.allclose(a1,a2) for a1,a2 in zip(act_outs_restricted, act_outs_old)])\n\nbhno_train = create_training_object_from_file(\n    'data/models/two-class/raw-square/increasing-filters/test/36.yaml')\nbhno_train.dataset_splitter.ensure_dataset_is_loaded()\ndatasets = bhno_train.get_train_fitted_valid_test()\ntrain_topo = datasets['train'].get_topological_view()\nacts_out = act_func_new(train_topo)\nacts_out[1].shape\n\npatterns = transform_to_patterns(combined_weights,train_topo, acts_out)")


# In[9]:


pattern_names = ['Combined Pattern ' + str(i_weight + 1) for i_weight in xrange(4)]
fig = plot_sensor_signals_multiple_classes(patterns, ['C3', 'C4'],
                                           pattern_names,
                                           figsize=(2,4))
for i_ax in [0,3,5,6]:
    fig.axes[i_ax].axvspan(0, 30, color=seaborn.color_palette()[2], alpha=0.2, lw=0)

pyplot.xticks(range(0,31,5), map(lambda ms: "{:.0f}".format(ms),
                                     np.arange(0,31,5) * 1000 / 150.0))
pyplot.xlabel("Time [ms]")
None


# We can transform our weights in the softmax layer in the same way as in the example, since it is a linear classifier. The transformed patterns are more smooth, while still showing a similar timecourse.

# In[10]:

act_3 = acts_out[3]
act_3_soft_weight_shape = act_3.transpose(0,2,3,1)
assert np.array_equal(act_3_soft_weight_shape.squeeze().shape[1:], soft_weights.shape[1:]) 
act_3_for_covar = act_3_soft_weight_shape.reshape(act_3_soft_weight_shape.shape[0], -1).T
features_covar = np.cov(act_3_for_covar)


# In[11]:

soft_weights_for_pattern = soft_weights.reshape(soft_weights.shape[0],-1)
assert soft_weights_for_pattern.shape[0] == soft_weights.shape[0]
assert soft_weights_for_pattern.shape[1] == np.prod(soft_weights.shape[1:])
soft_weights_pattern = np.dot(features_covar , soft_weights_for_pattern.T)
assert soft_weights_pattern.shape[1] == soft_weights.shape[0]
assert soft_weights_pattern.shape[0] == np.prod(soft_weights.shape[1:])
# have to transpose to again have class dimension as first dimension
soft_weights_pattern = soft_weights_pattern.T.reshape(soft_weights.shape)
assert soft_weights_pattern.shape == soft_weights.shape


# In[28]:

x_pools = np.arange(0,53)
x_times = np.arange(0,521, 10) * 1000 / 150.0
x_times = np.array(["{:.1f}".format(time) for time in x_times])
sensor_names = map(lambda i: "Combined Filter {:d}".format(i), range(1,5))
fig = plot_sensor_signals(soft_weights_pattern[0].T, sensor_names, figsize=(3,2.5))
fig.suptitle(
    "Right Hand Softmax Pattern", y=1.02, fontsize=16)
fig.axes[1].axvspan(0, 52, ymin=0.5, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
fig.axes[2].axvspan(0, 52, ymin=0.5, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
pyplot.xticks([0,15,30,45,60], [0,1000,2000,3000,4000])
pyplot.xlabel('Time [ms] (at start of pool region)')
fig = plot_sensor_signals(soft_weights_pattern[1].T, sensor_names, figsize=(3,2.5))
fig.suptitle(
    "Left Hand Softmax Pattern", y=1.02, fontsize=16)
fig.axes[3].set_xlim(0,60)
pyplot.xticks([0,15,30,45,60], [0,1000,2000,3000,4000])
pyplot.xlabel('Time [ms] (at start of pool region)')
fig.axes[0].axvspan(0, 52, ymin=0.5, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
fig.axes[3].axvspan(0, 52, ymin=0.5, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
None


# ### Reconstructing input by the gradient

# <div class="summary">
# <ul>
# <li>To get a "desired" input of a unit, you can use the gradient of the activation of that unit on random input
# </ul>
# </div>

# We want to try another, more generic way to visualize what kind of feature a particular unit might encode. To do that, one can show what kind of input would lead to a high activation <a data-cite="349468/C66WEZ6B"target="_blank" href="http://arxiv.org/abs/1312.6034">(Simonyan et al., 2013-12-20)</a>.
# 
# 1. Start with random input
# 2. Compute the gradient of the unit on that input 
# 3. Weight gradient by a learning rate and add it to the input
# 4. Repeat steps 2-3 (until you reach a specified activation or after a certain number of repetitions)
# 
# Now we again run into the problems of visualizing discriminatively trained models. Since the covariance, including the noise covariance, influences our model, the reconstructed input will also be affected by it. The approach to solving this here is putting a prior on the reconstructed input to ensure it is resembling a naturally occuring input. We try to use just a simple prior that penalizes the squared sum of the inputs. Only using this simple prior is motivated by the fact that we only want to reconstruct some coarse structure of the input, such as the lateralization and do not want to make many assumptions on natural inputs. We only do the reconstruction for the last softmax layer units, also penalizing if the wrong class has a high output.

# So, our variation works like this:
# 
# 1. Start with random input
# 2. Compute the gradient of the softmax cost for the desired class on that input
# 3. Decay the input by a factor (e.g 0.99)
# 4. Weight gradient by a learning rate and **subtract** it from the input
# 5. Repeat steps 2-4 for a predefined number of repetitions
# 
# The decay in step 3 is mathematically equivalent to adding the squared sum of the input to the costs, i.e. rewarding inputs with a lower squared sum.

# #### Artificial Data

# <div class="summary">
# <ul>
# <li>Artificial data roughly shows the expected reconstruction</li>
# <li>Valid convolution affects reconstruction</li>
# </ul>
# </div>

# We will start with one artificial example dataset to emphasize some precautions you have to make when interpreting the reconstruction. Then we continue showing the reconstruction for our running example.

# We show one example for an artificial dataset with just one channel, each trial has 600 samples:
# 
# * Class 1: flat zeroline + random gaussian noise
# * Class 2: sine of frequency 11 + random gaussian noise
# 
# Our net consists of:
# 
# 1. Convolution + sumlog pooling 
# 2. Softmax

# We show the visualizations for class 0 and class 1 after 1000 epochs with decay factor 0.99 and learning rate 0.1:

# In[18]:

get_ipython().run_cell_magic(u'capture', u'', u'inputs_class_1, inputs_class_2 = get_reconstructions_artificial_data()')


# In[20]:

fig, axes = pyplot.subplots(2,1, figsize=(5,3), sharex=True)
axes[0].set_title("Class 1")
axes[0].plot(np.squeeze(inputs_class_1[-1,2]).T)
axes[0].set_yticks([-0.0001,0,0.0001])
#axes[0].set_ylabel('f(Volt)')
axes[1].set_title("Class 2")
axes[1].set_yticks([-0.05,0,0.05])
axes[1].plot(np.squeeze(inputs_class_2[-1,2]).T)
#pyplot.ylabel('Voltage')
pyplot.xticks(range(0,601,150), range(0,4001,1000))
pyplot.xlabel('Time [ms]')
fig.subplots_adjust(hspace=0.5)


# We can nicely see the recovered sine wave for class 2. Note the much smaller overall variance for class 1.
# You have to be cautios when interpreting the start and the end of the input. The smaller amplitude at the start and end can be explained by the fact that we use *valid* convolutions: We convolve over the input without any padding. The first sample and last sample therefore only contribute to one convolution output, whereas a sample in the middle will contribute $\#kernel\_shape$ times to the convolutional output, in our case, 150 times. 

# #### Real Two-Class Data

# <div class="summary">
# <ul>
# <li>Right hand vs left hand, lateralization visible</li>
# </ul>
# </div>

# We now show the same visualization for our running right vs left example.

# In[21]:

#bhno_right_left_model = serial.load('data/models/two-class/raw-square/identity-bp/test/6.pkl').model
in_grad_func = create_input_grad_for_wanted_class_func(bhno_right_left_model)
batches = 3
start_in = create_rand_input(bhno_right_left_model, batches)
ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000)
inputs_class_1 = gradient_descent_input(start_in, in_grad_func, [1,0], **ascent_args)
inputs_class_2 = gradient_descent_input(start_in, in_grad_func, [0,1], **ascent_args)


# In[22]:

fig = plot_sensor_signals_two_classes(inputs_class_1[-1,0], inputs_class_2[-1,0], ['C3', 'C4'],
                               'Right Hand', 'Left Hand', figsize=(5,3))

pyplot.xticks(range(0,601,150), range(0,4001,1000))
pyplot.xlabel('Time [ms]')
fig.text(0.02, 0.5, 'Voltage', va='center', rotation='vertical', 
         fontsize=12)
None


# We see that the left hand shows a higher variance overall, especially for sensor C3. This is consistent with a variance decrease for sensor C3 (on the left side) for the right hand.
# Note that it correctly only shows an increase of variance after around 500 milliseconds, this is longer than could be explained just by the valid convolution/border effect.
# We also can see that the network seems to focus mostly on the first half of the trial.
