
# coding: utf-8

# In[12]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nimport os\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n#os.environ[\'CUDA_LAUNCH_BLOCKING\'] = \'1\'\n#os.environ[\'THEANO_FLAGS\'] = os.environ[\'THEANO_FLAGS\'] + ",profile=True"\n# switch to cpu\n#os.environ[\'THEANO_FLAGS\'] = \'floatX=float32,device=cpu,nvcc.fastmath=True\'\n%load_ext autoreload\n%autoreload 2\n\nfrom motor_imagery_learning.bbci_pylearn_dataset import (compute_power_spectra)\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset\nimport numpy as np\n\nimport matplotlib\nfrom matplotlib import pyplot\nfrom matplotlib import cm\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (8.0, 2.0)\nmatplotlib.rcParams[\'font.size\'] = 7\n\nfrom pylearn2.costs.mlp import Default\nfrom pylearn2.models.mlp import Softmax, MLP\nfrom pylearn2.termination_criteria import EpochCounter\nimport seaborn\nseaborn.set_style(\'darkgrid\')\nfrom copy import deepcopy\nfrom motor_imagery_learning.train_scripts.train_experiments import ExperimentsRunner\nfrom motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer\nfrom motor_imagery_learning.analysis.data_generation import (create_sine_signal,\n    pipeline, create_shifted_sines,put_noise, convolve_with_weight,\n    max_pool_topo_view, log_sum_pool)\nfrom motor_imagery_learning.analysis.util import create_trainer\nfrom motor_imagery_learning.mypylearn2.conv_squared import ConvSquared\nfrom motor_imagery_learning.analysis.input_reconstruction import (\n    create_rand_input, gradient_descent_input, create_input_grad_for_wanted_class_func,\n    create_input_grad_with_smooth_penalty_for_wanted_class_func, \n    reconstruct_inputs_for_class)\n\nfrom motor_imagery_learning.analysis.plot_util import (plot_misclasses_for_model, \n    plot_nlls_for_model, plot_sensor_signals)\n\nimport h5py\nimport numpy as np\nfrom pylearn2.utils import serial\nfrom pprint import pprint\nfrom  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted\nimport h5py\nfrom motor_imagery_learning.csp.train_csp import CSPTrain\nfrom motor_imagery_learning.mypylearn2.train import TrainEarlyStop\nfrom motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD\nfrom motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset\nfrom motor_imagery_learning.bbci_dataset import BBCIDataset\nfrom pylearn2.costs.mlp import Default\nfrom pylearn2.models.mlp import Softmax, MLP\nfrom pylearn2.termination_criteria import EpochCounter\nfrom sklearn.cross_validation import KFold\nfrom pylearn2.datasets.dense_design_matrix import DenseDesignMatrix\nimport numpy as np\nfrom numpy.random import RandomState\nimport theano.tensor as T\nimport theano\nfrom pylearn2.space import Conv2DSpace\nfrom pylearn2.models.mlp import ConvElemwise\nfrom theano.sandbox.cuda.dnn import dnn_conv\nfrom theano.sandbox.cuda.basic_ops import gpu_alloc_empty\nfrom motor_imagery_learning.analysis.print_results import ResultPrinter\nfrom pylearn2.utils import serial\nfrom motor_imagery_learning.analysis.data_generation import generate_single_sensor_data\nfrom motor_imagery_learning.analysis.plot.raw_signals import (\n    plot_sensor_signals_two_classes)\nfrom motor_imagery_learning.mypylearn2.pool import PoolLayer, log_sum\nfrom motor_imagery_learning.analysis.util import bps_and_freqs\nfrom motor_imagery_learning.analysis.deconv import deconv_model\nfrom motor_imagery_learning.analysis.plot_util import (\n    plot_head_signals_tight_two_signals, plot_head_signals_tight)\nfrom motor_imagery_learning.csp.print_csp_results import CSPResultPrinter\nfrom motor_imagery_learning.analysis.filterbands import (\n    sum_abs_filterband_weights, compute_center_freqs, \n    compute_sum_abs_weights_for_csp_model)\nfrom motor_imagery_learning.analysis.plot.filterbands import (\n    show_freq_importances, plot_filterband_weights_for_train_state)\nfrom motor_imagery_learning.csp.train_csp import generate_filterbank\nfrom motor_imagery_learning.train_scripts.train_with_params import (\n    create_training_object_from_file)\nfrom motor_imagery_learning.mywyrm.plot import ax_scalp, CHANNEL_10_20\nfrom motor_imagery_learning.analysis.util import create_activations_func')


# In[2]:

get_ipython().run_cell_magic(u'javascript', u'', u"IPython.load_extensions('cite2c/main');")


# In[2]:

get_ipython().run_cell_magic(u'html', u'', u'<style>\ndiv.input {\nwidth: 105ex; /* about 80 chars + buffer */\n...\n}\ndiv.text_cell {\nwidth: 105ex /* instead of 100%, */\n...\n}\n\ndiv.text_cell_render {\n/*font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;*/\nfont-family: "Charis SIL", serif; /* Make non-code text serif. */\nline-height: 145%; /* added for some line spacing of text. */\nwidth: 105ex; /* instead of \'inherit\' for shorter lines */\n...\n}\n\n/* Set the size of the headers */\ndiv.text_cell_render h1 {\nfont-size: 18pt;\n}\n\ndiv.text_cell_render h2 {\nfont-size: 14pt;\n}\n\n\n\n.CodeMirror {\nfont-family: Consolas, monospace;\n}\n\n.todo {\n    background-color: hsla(120, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n.todecide {\n    background-color: hsla(240, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n\n</style>\n\n<style>\ndiv.summary  {\n    background-color: hsla(40,0%,50%,0.2);\n    color:black;\n    border-radius: 5px;\n    padding: 2px;\n    margin-top:0px;\n}\ndiv.summary ul\n{\n    list-style-type: none;\n}\n</style>\n<link rel="stylesheet" href="./motivation.css" type="text/css">\n<script async defer src="//hypothes.is/embed.js"></script>')


# ## Filter bank Net Visualizations

# <div class="summary">
# <ul>
# <li>Filter bank net shows sensible spatial filters</li>
# <li>Tranformation to spatial patterns looks plausible</li>
# <li>Filter bank net uses certain frequency bands more and others less</li>
# </ul>
# </div>

# In[5]:

# load to determine rejected sensors
#bhno_fb_train = create_training_object_from_file(
#    'data/models/debug/csp-net/car/chan-freq-standardize-misclass-stop/early-stop/2.yaml')

#bhno_fb_train.dataset_splitter.dataset.max_freq=4
#bhno_fb_train.dataset_splitter.ensure_dataset_is_loaded()
#rejected_chans = bhno_fb_train.dataset_splitter.rejected_chans
#bhno_fb_train.dataset_splitter.rejected_chans
rejected_chans = ['T7']


# In[6]:

from motor_imagery_learning.analysis.sensor_positions import get_EEG_sensors_sorted
sensor_names = get_EEG_sensors_sorted()
sensor_names = [s for s in sensor_names if s not in rejected_chans]


# For the filter bank net, we can get a good idea of what the network is learning by analyzing the weights of the net. Therefore, we will show the weights itself and also try to transform the weights to a more interpretable form using the covariances of the input.

# ### Spatial Filters and Softmax Weights

# First, we visualize the spatial filters together with their softmax weights for all 4 classes. The softmax weights for a filter show how that filter is weighted for different frequencies.
# We show the 20 spatial filters with the highest sum of absolute softmax weights in order to show those filters most influential on the classification result. 
# As always with weights from our nets, note that these not straightforward to interpret. The weights might also reflect class-independent information such as the covariance of the noise. Nevertheless, we do see plausible structures in our weights.
# Colors go from blue over grey to red from negative to zero to positive values.

# We see some lateralized filters such as filter 3 and 4. They also show plausible weights for the frequencies. Whereas the left hand is weighted negatively in the lower frequencies until 30 Hz, the left hand is weighted positively in the higher frequencies between 60 and 80 Hz. We also see centralized filters such as filters 1 and 2, that show a similar behavior for the rest and feet classes. We can also observe some difficult to interprete weights, such as the very low frequency weights for filter 1.
# 
# Note that a filter with flipped colors will lead to the same result in our network due to the squaring after the filtering. See for example filter 8 and 11.

# In[7]:

bhno_fb_net = serial.load(
    'data/models/debug/csp-net/car/chan-freq-standardize-misclass-stop/early-stop/2.pkl').model
spat_filters  = bhno_fb_net.layers[0].get_weights_topo().squeeze()
soft_weights = bhno_fb_net.layers[1].get_weights_topo()
filt_weights = np.sum(np.abs(soft_weights), axis=(0,1,2))
i_important_filt_weights = np.argsort(filt_weights)[::-1]


# In[39]:

from motor_imagery_learning.mywyrm.plot import get_channelpos

def add_circle_around_chan(ax, chan):
    point = get_channelpos(chan, CHANNEL_10_20)
    ax.add_artist(pyplot.Circle(point, 0.2, linestyle='solid', 
                                linewidth=1, fill=False,
                             color=[0.3,0.3,0.3]))
    


# In[47]:


#fig, axes = pyplot.subplots(1,1,figsize=(3,3))
#ax_scalp(spat_filters[i_important_filt_weights[2]], 
#         sensor_names, ax=axes, colormap=cm.coolwarm_r, annotate=False,
#        chan_pos_list=CHANNEL_10_20)
#add_circle_around_chan(axes, 'FCC4h')



# In[46]:



center_freqs = compute_center_freqs(bhno_fb_net)
n_rows = 6
n_filts = 18 
n_cols = 2 * (n_filts / n_rows)
assert n_filts % n_rows == 0
class_names = ('Right Hand', 'Left Hand', 'Rest', 'Feet')

fig, axes = pyplot.subplots(n_rows,n_cols,figsize=(12,12))
for i in range(n_filts):
    plot_i = i * 2
    i_filter = i_important_filt_weights[i]
    row, col = plot_i // n_cols, plot_i % n_cols
    ax_scalp(spat_filters[i_filter], 
             sensor_names, ax=axes[row,col], colormap=cm.coolwarm_r, annotate=False,
            chan_pos_list=CHANNEL_10_20)
    filt_soft_weight = soft_weights[:,:,:,i_filter].squeeze()
    axes[row, col].set_title('Filter ' + str(i + 1))
    if i == 0 or i == 1:
        add_circle_around_chan(axes[row, col], 'Cz')
    if i == 2 or i == 3:
        add_circle_around_chan(axes[row, col], 'FCC4h')
        
    axes[row, col+1].plot(center_freqs, filt_soft_weight.T, linewidth=1)
    axes[row, col+1].set_xticks(range(0,63,20) + [80,110,140])
    axes[row, col+1].set_yticks([0])
    axes[row, col+1].set_yticklabels([])

axes[0, n_cols - 1].legend(class_names, bbox_to_anchor=(2.05,1))
None


# ### Spatial patterns and softmax patterns

# We now want to transform the weights so that they show how strongly a source signal is projected to the sensors and do not reflect the noise structure as much anymore. 
# However, we cannot use our regular transformation to a pattern for the first layer, since the weights are convolved over signals from several filterbands. We therefore resort to only multiplying with the covariances of the sensors.
# The intuition here is that a source probably projects to neighbouring sensors with a high covariance and the classifier might give positive weights to the sensor(s) where the source is strongest and negative weights to the others to remove the common noise. Multiplying with the covariances will then more accurately reflect how the source is projected to the sensors, probably transforming the negative weights into small positive weights. We will call these transformed spatial filters spatial patterns.
# 
# However, it is not clear how to compute the covariance for the sensors as we have several filterbands. The different filterbands might have very different sensor covariances. Therefore, we first plot the sensor covariances per filterband to check if we can safely use the mean sensor covariance across all filterbands. The rows are sorted from top to bottom like this (same for the columns from left to right):
# 
# 1. First (front) sensor row, from left to right
# 2. Second sensor row, from left to right
# 3. ...
# 
# So, for example the left top and right bottom blocks for 2 Hz show that the sensors in the front and the sensors in the back have high covariances among each other. The diagonal lines show that sensors on the same side of the head have high covariances.

# In[8]:

all_covariances = np.load('./all_covariances.npy')
mean_covariance = np.mean(all_covariances, axis=0)


# In[64]:

fig, axes = pyplot.subplots(6,6, figsize=(10,10))
for i_filt_band in range(35):
    covariance = all_covariances[i_filt_band]
    #covariance = covariance / np.sqrt(np.sum(np.square(covariance)))
    row, col = i_filt_band // 6, i_filt_band % 6
    ax = axes[row,col]
    ax.imshow(covariance, cmap=cm.coolwarm, vmin=-np.max(np.abs(covariance)),
                 vmax=np.max(np.abs(covariance)), interpolation='nearest')
    ax.set_title(str(int(center_freqs[i_filt_band])) + " Hz")
    ax.set_xticks([])
    ax.set_xticklabels([])
    ax.set_yticks([])
    ax.set_yticklabels([])
    None
fig.delaxes(axes[5,5])
fig.subplots_adjust(hspace=0.5)


# Overall, we can see that the covariances are somewhat similar between all filterbands, and therefore we will use the mean covariance across all filterbands shown below.

# In[151]:

pyplot.imshow(mean_covariance, cmap=cm.coolwarm, vmin=-np.max(np.abs(mean_covariance)),
                 vmax=np.max(np.abs(mean_covariance)), interpolation='nearest')

pyplot.xticks([])
pyplot.yticks([])
pyplot.title('Mean sensor covariance')
None


# The softmax weights can be transformed into softmax patterns as described in the sections before.

# In[398]:

#### Replace old module paths
from subprocess import check_output
replacement_pattern = 's/bbci_pylearn_dataset.BBCISetCleaner/mywyrm.clean.BBCISetCleaner/'
filepattern = 'data/models/debug/csp-net/car/early-stop/*.yaml'
command = 'sed -i ' + replacement_pattern + ' ' + filepattern
print check_output(command, shell=True)
replacement_pattern = 's/bbci_pylearn_dataset.BBCISetSecondSetCleaner/mywyrm.clean.BBCISecondSetCleaner/'
command = 'sed -i ' + replacement_pattern + ' ' + filepattern
print check_output(command, shell=True)


# In[9]:

act_0 = np.load('./activations_0.npy')
act_1 = np.load('./activations_1.npy')


# In[10]:

act_0_soft_weight_shape = act_0.transpose(0,2,3,1)
assert np.array_equal(act_0_soft_weight_shape.shape[1:], soft_weights.shape[1:]) 
act_0_for_covar = act_0_soft_weight_shape.reshape(act_0_soft_weight_shape.shape[0], -1).T
features_covar = np.cov(act_0_for_covar)


# In[11]:

soft_weights_for_pattern = soft_weights.reshape(soft_weights.shape[0],-1)
assert soft_weights_for_pattern.shape[0] == soft_weights.shape[0]
assert soft_weights_for_pattern.shape[0] == 4
assert soft_weights_for_pattern.shape[1] == np.prod(soft_weights.shape[1:])
soft_weights_pattern = np.dot(features_covar , soft_weights_for_pattern.T)
assert soft_weights_pattern.shape[1] == soft_weights.shape[0]
assert soft_weights_pattern.shape[1] == 4
assert soft_weights_pattern.shape[0] == np.prod(soft_weights.shape[1:])
# have to transpose to again have class dimension as first dimension
soft_weights_pattern = soft_weights_pattern.T.reshape(soft_weights.shape)
assert soft_weights_pattern.shape == soft_weights.shape


# Putting it all together we get this visualization for the spatial pattens and the softmax patterns. This results in spatially much smoother patterns.
# We see clearly centralized patterns for example for pattern 1 and 2. We also see lateralized patterns, for example pattern 4 and 5. The softmax patterns for rest and feet as well as for right hand and left hand have fairly high correlations for most spatial filters. This indicates, left vs right and rest vs feet are more difficult to distinguish than rest or feet vs left or right.

# In[12]:

n_rows = 6
n_filts = 18 
n_cols = 2 * (n_filts / n_rows)
assert n_filts % n_rows == 0
class_names = ('Right Hand', 'Left Hand', 'Rest', 'Feet')

fig, axes = pyplot.subplots(n_rows,n_cols,figsize=(12,12))
for i in range(n_filts):
    plot_i = i * 2
    i_filter = i_important_filt_weights[i]
    row, col = plot_i // n_cols, plot_i % n_cols
    pattern = np.dot(spat_filters[i_filter:i_filter+1], mean_covariance)
    pattern = pattern.squeeze()
    ax_scalp(pattern, 
             sensor_names, ax=axes[row,col], colormap=cm.coolwarm, annotate=False,
            chan_pos_list=CHANNEL_10_20)
    filt_soft_weight = soft_weights_pattern[:,:,:,i_filter].squeeze()
    axes[row, col].set_title('Pattern ' + str(i + 1))
    axes[row, col+1].plot(center_freqs, filt_soft_weight.T, linewidth=1)
    axes[row, col+1].set_xticks(range(0,63,20) + [80,110,140])
    axes[row, col+1].set_yticks([0])
    axes[row, col+1].set_yticklabels([])

axes[0, n_cols - 1].legend(class_names, bbox_to_anchor=(2.05,1))
None


# ### Used filterbands

# Now, we want to determine which filterbands our network uses the most for different subjects. 
# An easy way to visualize this is to show the sum of the absolute weights for each filterband.
# We do this for two subjects here. You can see that one is using the higher frequencies around 70 Hz and the other one not.
# <span class="todecide">Repeat this part with the chan-freq-standardized nets (data/models/debug/csp-net/chan-freq-standardize-misclass-stop/early-stop/)?</span>
# 

# In[9]:

from glob import glob
folder = 'data/models/debug/csp-net/chan-freq-standardize-misclass-stop/early-stop/'
models = []
for filenr in xrange(1,19):
    filename = folder + str(filenr) + ".pkl"
    model = serial.load(filename)
    models.append(model)


# In[10]:


center_freqs = compute_center_freqs(bhno_fb_net)
all_sum_abs_weights = [sum_abs_filterband_weights(m.model, center_freqs) for m in models]


# In[13]:

plot_filterband_weights_for_train_state(models[1])
pyplot.title('VP2', fontsize=16)
pyplot.xlabel('Frequency [Hz]')
plot_filterband_weights_for_train_state(models[9])
pyplot.title('VP10', fontsize=16)
pyplot.xlabel('Frequency [Hz]')
None


# We also show the same plots for the mean and standard deviation across our 18 datasets.
# The blue dots are the means and the black bars represent one standard deviation.
# As you can see, across all datasets there is a clear peak for 70 and 76 Hz.
# Also, for these frequencies, the standard deviation is quite high, which is consistent with our observation that for some subjects, the network uses these frequencies quite strongly and for others, not at all.

# In[14]:

mean_fb = np.mean(all_sum_abs_weights, axis=0)
std_fb = np.std(all_sum_abs_weights, axis=0)


# In[15]:

pyplot.figure(figsize=(8,3))
pyplot.plot(center_freqs, mean_fb, marker='o', linestyle='None')
_ = pyplot.xticks(np.array(center_freqs[:16:2].tolist() + center_freqs[16:].tolist()).astype(np.int))
eb = pyplot.errorbar(center_freqs, mean_fb, yerr=std_fb, fmt=None, ecolor='k')
pyplot.xlim(-0.5,145)
pyplot.title('Sum of absolute weights for filterbands', fontsize=16)
pyplot.xlabel('Frequency [Hz]')
None


# In[ ]:

# Some attempts at descend, be careful might use up too much memory sicne it
# always saves all inputs during gradient descent


# In[145]:

#%%time
activations_func = create_activations_func(bhno_fb_net)
in_grad_func = create_input_grad_for_wanted_class_func(bhno_fb_net)
batches = 3
start_in = create_rand_input(bhno_fb_net, batches)
start_in = start_in * 0 + 0.5
ascent_args = dict(lrate=30, decay=0.99, epochs=100)
inputs_right = gradient_descent_input(start_in, in_grad_func, np.float32([1,0,0,0]), **ascent_args)
inputs_right = inputs_right.squeeze()


# In[120]:

test_in = inputs_right[-1,-1][:,0,:]


# In[124]:

real_center_freqs = [0.25] + center_freqs[1:].tolist()


# In[141]:

sine_signals = [create_sine_signal(samples=300, freq=f, sampling_freq=300) for f in real_center_freqs]
sine_signals = np.array(sine_signals)


# In[142]:

recovered_signal =np.dot(test_in, sine_signals)


# In[139]:

from motor_imagery_learning.analysis.plot_util import plot_head_signals


# In[144]:

#plot_head_signals(recovered_signal, sensor_names)

