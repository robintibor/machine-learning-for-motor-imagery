
# coding: utf-8

# In[2]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nimport os\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n#os.environ[\'CUDA_LAUNCH_BLOCKING\'] = \'1\'\n#os.environ[\'THEANO_FLAGS\'] = os.environ[\'THEANO_FLAGS\'] + ",profile=True"\n# switch to cpu\n#os.environ[\'THEANO_FLAGS\'] = \'floatX=float32,device=cpu,nvcc.fastmath=True\'\n%load_ext autoreload\n%autoreload 2\n\nfrom motor_imagery_learning.bbci_pylearn_dataset import (compute_power_spectra)\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset\nimport numpy as np\n\nimport matplotlib\nfrom matplotlib import pyplot\nfrom matplotlib import cm\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (8.0, 2.0)\nmatplotlib.rcParams[\'font.size\'] = 7\n\nfrom pylearn2.costs.mlp import Default\nfrom pylearn2.models.mlp import Softmax, MLP\nfrom pylearn2.termination_criteria import EpochCounter\nimport seaborn\nseaborn.set_style(\'darkgrid\')\nfrom copy import deepcopy\nfrom motor_imagery_learning.train_scripts.train_experiments import ExperimentsRunner\nfrom motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer\nfrom motor_imagery_learning.analysis.data_generation import (create_sine_signal,\n    pipeline, create_shifted_sines,put_noise, convolve_with_weight,\n    max_pool_topo_view, log_sum_pool)\nfrom motor_imagery_learning.analysis.util import create_trainer\nfrom motor_imagery_learning.mypylearn2.conv_squared import ConvSquared\nfrom motor_imagery_learning.analysis.input_reconstruction import (\n    create_rand_input, gradient_descent_input, create_input_grad_for_wanted_class_func,\n    create_input_grad_with_smooth_penalty_for_wanted_class_func, \n    reconstruct_inputs_for_class)\n\nfrom motor_imagery_learning.analysis.plot_util import (plot_misclasses_for_model, \n    plot_nlls_for_model, plot_sensor_signals)\n\nimport h5py\nimport numpy as np\nfrom pylearn2.utils import serial\nfrom pprint import pprint\nfrom  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted\nimport h5py\nfrom motor_imagery_learning.csp.train_csp import CSPTrain\nfrom motor_imagery_learning.mypylearn2.train import TrainEarlyStop\nfrom motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD\nfrom motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset\nfrom motor_imagery_learning.bbci_dataset import BBCIDataset\nfrom pylearn2.costs.mlp import Default\nfrom pylearn2.models.mlp import Softmax, MLP\nfrom pylearn2.termination_criteria import EpochCounter\nfrom sklearn.cross_validation import KFold\nfrom pylearn2.datasets.dense_design_matrix import DenseDesignMatrix\nimport numpy as np\nfrom numpy.random import RandomState\nimport theano.tensor as T\nimport theano\nfrom pylearn2.space import Conv2DSpace\nfrom pylearn2.models.mlp import ConvElemwise\nfrom theano.sandbox.cuda.dnn import dnn_conv\nfrom theano.sandbox.cuda.basic_ops import gpu_alloc_empty\nfrom motor_imagery_learning.analysis.print_results import ResultPrinter\nfrom pylearn2.utils import serial\nfrom motor_imagery_learning.analysis.data_generation import generate_single_sensor_data\nfrom motor_imagery_learning.analysis.plot.raw_signals import (\n    plot_sensor_signals_two_classes)\nfrom motor_imagery_learning.mypylearn2.pool import PoolLayer, log_sum\nfrom motor_imagery_learning.analysis.util import bps_and_freqs\nfrom motor_imagery_learning.analysis.deconv import deconv_model\nfrom motor_imagery_learning.analysis.plot_util import (\n    plot_head_signals_tight_two_signals, plot_head_signals_tight)\nfrom motor_imagery_learning.analysis.util import (\ncreate_activations_func)')


# In[3]:


from pylearn2.models.mlp import ConvElemwise, IdentityConvNonlinearity
from motor_imagery_learning.mypylearn2.nonlinearities import (
    NonlinearityLayer, square_nonlin)

def transform_raw_net_model(model):
    transformed_model = deepcopy(model)
    oldlayer = transformed_model.layers[1]
    new_layer = ConvElemwise(output_channels=oldlayer.output_channels, 
                             kernel_shape=oldlayer.kernel_shape,
                             layer_name='new_layer',
                             nonlinearity=IdentityConvNonlinearity(),
                             kernel_stride=oldlayer.kernel_stride,
                             tied_b=True,
                             irange=0.01
                            )
    new_layer.mlp = lambda: None
    new_layer.mlp.rng = RandomState(92138)
    new_layer.mlp.batch_size = 2
    new_layer.set_input_space(oldlayer.get_input_space())
    new_layer.set_weights(oldlayer.get_param_values()[0])
    new_layer.set_biases(oldlayer.get_param_values()[1])
    nonlin_layer = NonlinearityLayer(layer_name='square_nonlin', 
                                 nonlinearity_func=square_nonlin)
    nonlin_layer.set_input_space(new_layer.get_output_space())
    transformed_model.layers = [transformed_model.layers[0],
                            new_layer, nonlin_layer, 
                           transformed_model.layers[2],
                           transformed_model.layers[3]]
    return transformed_model

def transform_to_patterns(combined_weights, train_topo, acts_out):
    """Transform raw net combined filters to combined patterns.
    Assumes kernel shape 30"""
    acts_out_for_cov = acts_out[1].transpose(1,0,2,3)
    acts_out_for_cov = acts_out_for_cov.reshape(acts_out_for_cov.shape[0], -1)
    acts_cov = np.cov(acts_out_for_cov)
    new_train_topo = np.empty((train_topo.shape[0], train_topo.shape[1] * 30,
                             train_topo.shape[2] - 30 + 1, 1))


    n_chans = train_topo.shape[1]
    n_samples = 30

    for i_sample in xrange(n_samples):
        end = -30+1+i_sample
        if end == 0:
            end = None
        for i_chan in xrange(n_chans):
            new_train_topo[:, i_chan*n_samples+i_sample, :] =                 train_topo[:,i_chan,i_sample:end]

    new_train_topo_for_cov = new_train_topo.transpose(1,0,2,3)

    new_train_topo_for_cov = new_train_topo_for_cov.reshape(
        new_train_topo_for_cov.shape[0], -1)
    new_train_topo_cov = np.cov(new_train_topo_for_cov)
    combined_vectors = combined_weights.reshape(combined_weights.shape[0],-1)
    transformed_vectors = np.dot(new_train_topo_cov, combined_vectors.T).T
    transformed_vectors = np.dot(transformed_vectors.T, acts_cov).T
    patterns = transformed_vectors.reshape(*combined_weights.shape)
    return patterns


# In[2]:

get_ipython().run_cell_magic(u'javascript', u'', u"IPython.load_extensions('cite2c/main');")


# In[4]:

get_ipython().run_cell_magic(u'html', u'', u'<style>\ndiv.input {\nwidth: 105ex; /* about 80 chars + buffer */\n...\n}\ndiv.text_cell {\nwidth: 105ex /* instead of 100%, */\n...\n}\n\ndiv.text_cell_render {\n/*font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;*/\nfont-family: "Charis SIL", serif; /* Make non-code text serif. */\nline-height: 145%; /* added for some line spacing of text. */\nwidth: 105ex; /* instead of \'inherit\' for shorter lines */\n...\n}\n\n/* Set the size of the headers */\ndiv.text_cell_render h1 {\nfont-size: 18pt;\n}\n\ndiv.text_cell_render h2 {\nfont-size: 14pt;\n}\n\n\n\n.CodeMirror {\nfont-family: Consolas, monospace;\n}\n\n.todo {\n    background-color: hsla(120, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n.todecide {\n    background-color: hsla(240, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n\n</style>\n\n<style>\ndiv.summary  {\n    background-color: hsla(40,0%,50%,0.2);\n    color:black;\n    border-radius: 5px;\n    padding: 2px;\n    margin-top:0px;\n}\ndiv.summary ul\n{\n    list-style-type: none;\n}\n</style>\n<link rel="stylesheet" href="./motivation.css" type="text/css">\n<script async defer src="//hypothes.is/embed.js"></script>')


# ## Raw Net Visualizations

# We will show that the raw nets can learn to use:
# 
# * oscillations with different frequencies at specific locations
# * a signal potential at the beginning of the trial
# 
# For that, we will show the weights and patterns of the network and reconstruct the desired inputs for a class, starting from random input and from actual trials of our dataset.

# ### Weights

# We will use a network trained on our 4 classes with input downsampled to 150 Hz. Keep in mind, that the weights of our network are not straightforward to interpret as they can reflect a mix of how the signal is shaped for the different classes and how the noise is structured. Nevertheless, we believe the weights of our network show some plausible expected structures.
# 
# To get an idea what the network has learned, we will go backwards. We will first look at the softmax weights to see what kind of filters might discriminate between what classes. Then we will look closer at what these filters might detect.
# 
# The softmax layer has one weight per class per filter from the layer before per pooled time-interval. Since we have 4 classes, 40 filters in the layer before and 53 pooled time-intervals, we have 8480 weights. They can be nicely visualized by plotting them as timecourses (over the pooled time-intervals) per class per filter of the layer before.

# In[37]:

bhno_model = serial.load(
    'data/models/debug/raw-square/car/30-kernel-shape/early-stop/2.pkl').model


# In[38]:

temporal_weights = bhno_model.get_weights_topo()[:,::-1,::-1,:]


# In[39]:

spat_filt_weights = bhno_model.layers[1].get_weights_topo()[:,::-1,::-1,:]

combined_weights = np.tensordot(spat_filt_weights, temporal_weights, axes=(3,0))

combined_weights = combined_weights.squeeze()


# In[40]:

soft_weights = bhno_model.layers[-1].get_weights_topo().squeeze()


# In[41]:

soft_sum =np.sum(np.abs(soft_weights), axis=(0,1))
imp_weights = np.argsort(soft_sum)[::-1]


# We show the weights of the softmax layer for 12 combined filters of the layer before (combined, since they combine a spatial and a temporal filter). We show the 12 filters with the highest absolute sum of the softmax weights to show those filters with the highest influence on the classification. As said, the softmax weights show you a timecourse over the pooled time-intervals for each class. 
# 
# What you can see is that a lot of filters seem to discriminate between right and left and between rest and feet. See for example weights for combined filters 2 and 8 for right vs left and 3 and 11 for rest vs feet. 

# In[42]:

sensor_names = map(lambda i: "Combined Filter {:d}".format(i), range(1,13))
class_names = ('Right Hand', 'Left Hand', 'Rest', 'Feet')
fig = plot_sensor_signals(soft_weights[:,:,imp_weights[:6]].T, sensor_names[:6], 
                          figsize=(4,4))#4,6
fig.axes[0].set_title("Softmax Weights", fontsize=16, y=1.2)
pyplot.xticks([0,15,30,45], [0,1000,2000,3000])
pyplot.xlabel('Time [ms] (at start of pool region)')

#fig.axes[0].legend(class_names, bbox_to_anchor=(1.45, 1.45), loc='upper right')
fig.axes[1].axvspan(0, 52, color=seaborn.color_palette()[2], alpha=0.15, lw=0)
#fig.axes[7].axvspan(0, 52, color=seaborn.color_palette()[2], alpha=0.15, lw=0)
fig.axes[2].axvspan(0, 52, color=seaborn.color_palette()[1], alpha=0.15, lw=0)
#fig.axes[10].axvspan(0, 52, color=seaborn.color_palette()[1], alpha=0.15, lw=0)

fig = plot_sensor_signals(soft_weights[:,:,imp_weights[6:12]].T, 
                          map(str, range(7,13)), 
                          figsize=(4,4))
fig.axes[0].set_title("Softmax Weights", fontsize=16, y=1.2)
pyplot.xticks([0,15,30,45], [0,1000,2000,3000])
pyplot.xlabel('Time [ms] (at start of pool region)')
fig.axes[0].legend(class_names, bbox_to_anchor=(1.45, 1.45), loc='upper right')
fig.axes[1].axvspan(0, 52, color=seaborn.color_palette()[2], alpha=0.15, lw=0)
fig.axes[4].axvspan(0, 52, color=seaborn.color_palette()[1], alpha=0.15, lw=0)
None


# Now we want to look at combined filters that are strongly discriminative between right/left and rest/feet. We therefore compute the mean of the softmax weights across all pool regions (so we have one mean per class and per combined filter). We then compute where the difference between the mean for right and the mean for left is the largest and vice versa. We do the same for rest vs feet. Now we should have combined filters that are strongly discriminating between right/left and rest/feet. In the following, we will show these combined filters together with their softmax weights. We will start with a filter that indicates the right hand.

# In[15]:

def argsort_by_class(soft_weights, class_1, class_2, start_time_bin=None, end_time_bin=None):
    
    wanted_vs_other = soft_weights[class_1] - soft_weights[class_2]
    sorted_diffs = np.argsort(np.mean(wanted_vs_other[start_time_bin:end_time_bin], axis=0))
    return sorted_diffs


# In[16]:

sorted_diffs = argsort_by_class(soft_weights, 0, 1)

right_best = sorted_diffs[[-1,-2]]
left_best = sorted_diffs[[0,1]]


# The filters have a length of 30 samples, i.e., 200 milliseconds.
# We see that this filter is strongly lateralized. For example, we see  a low-frequency sine-like shape for FCC4h and C2 on the right side, while we see almost no low-frequency shapes on the left side. This makes sense, as the network should recognize a decrease of variance in a low frequency on the left side for the right hand.
# 
# The softmax weights also show that the network has learned a timecourse, for example mostly ignoring the start of the trial.

# In[17]:

def highlight_chans(fig, chan_names, color=None, alpha=0.2, xstart=0, xstop=29):
    if color is None:
        color = seaborn.color_palette()[2]
    all_chan_names = np.array(get_C_sensors_sorted())
    highlight_chan_inds = [np.nonzero(all_chan_names == c)[0][0] for c in chan_names]
    for i_chan in highlight_chan_inds:
        fig.axes[i_chan].axvspan(xstart, xstop, color=color, alpha=alpha, lw=0)
        
    


# In[18]:

for i_filt in right_best[0:1]:
    fig = plot_sensor_signals(soft_weights[:,:,i_filt:i_filt+1].T, 
                        [''], 
                        figsize=(5,1.5))
    fig.axes[0].set_title("Combined Filter " + str(i_filt + 1) + ": Softmax Weights", fontsize=16)
    fig.axes[0].legend(class_names, bbox_to_anchor=(1.35, 1.), loc='upper right')
    pyplot.xticks([0,15,30,45], [0,1000,2000,3000])
    pyplot.xlabel('Time [ms] (at start of pool region)')
    fig = plot_head_signals_tight(combined_weights[i_filt], get_C_sensors_sorted())
    pyplot.xticks([15], [])
    highlight_chans(fig,  ['FCC4h', 'C2'])


# 
# 
# To find out what frequencies this filter represents, we can plot the frequency amplitudes of the fourier transformation. We see that the filters mostly contain frequencies around 10-15 Hz, and frequencies above 65 Hz. We know that we have useful information for our classification task in the the 10-15 Hz range, so our network seems to learn to match atleast some relevant frequencies. The higher frequncies above 65 Hz can also contain important information, but we would typically expect an increase on the left side for the right hand.

# In[30]:

bps, freqs = bps_and_freqs(combined_weights[right_best[0]])
plot_head_signals_tight(bps, get_C_sensors_sorted(), hspace=1.5)
pyplot.xticks(np.arange(0,bps.shape[1]), [int(freqs[i]) if i % 3 == 0 else '' for i in range(0,bps.shape[1])])
None


# We now show the same for a filter that indicates the left hand. Here we also see a strong lateralization, now to the left side.

# In[19]:

# 21, 25 and 32 negative for right hand?
# 29 positive
for i_filt in left_best[0:1]:
    fig = plot_sensor_signals(soft_weights[:,:,i_filt:i_filt+1].T, 
                        [''], 
                        figsize=(5,1.5))
    fig.axes[0].set_title("Combined Filter " + str(i_filt + 1) + ": Softmax Weights", fontsize=16)
    fig.axes[0].legend(class_names, bbox_to_anchor=(1.35, 1.), loc='upper right')
    pyplot.xticks([0,15,30,45], [0,1000,2000,3000])
    pyplot.xlabel('Time [ms] (at start of pool region)')
    fig = plot_head_signals_tight(combined_weights[i_filt], get_C_sensors_sorted())
    pyplot.xticks([15], [])
    highlight_chans(fig,  ['FC3', 'FCC3h'])


# In[21]:

sorted_diffs = argsort_by_class(soft_weights, 2, 3)

rest_best = sorted_diffs[[-1,-2]]
feet_best = sorted_diffs[[0,1]]


# The combined filter indicating the rest class looks different, it seems to mostly match a high frequency. It has the largest amplitudes around the central Cz electrode.

# In[22]:

for i_filt in rest_best[0:1]:
    fig = plot_sensor_signals(soft_weights[:,:,i_filt:i_filt+1].T, 
                        [''], 
                        figsize=(5,1.5))
    fig.axes[0].set_title("Combined Filter " + str(i_filt + 1) + ": Softmax Weights", fontsize=16)
    fig.axes[0].legend(class_names, bbox_to_anchor=(1.35, 1.), loc='upper right')
    pyplot.xticks([0,15,30,45], [0,1000,2000,3000])
    pyplot.xlabel('Time [ms] (at start of pool region)')
    fig = plot_head_signals_tight(combined_weights[i_filt], get_C_sensors_sorted())
    pyplot.xticks([15], [])
    highlight_chans(fig,  ['Cz', 'CCP1h'])
    
    


# Indeed, looking at the amplitudes of the fourier transform, we see peaks mostly around 40 Hz.

# In[34]:

bps, freqs = bps_and_freqs(combined_weights[rest_best[0]])
plot_head_signals_tight(bps, get_C_sensors_sorted(), hspace=1.5)
pyplot.xticks(np.arange(0,bps.shape[1]), [int(freqs[i]) if i % 3 == 0 else '' for i in range(0,bps.shape[1])])
None


# Our final filter for this dataset shows a filter indicative for feet. The filters show a high variance for the central Cz and Cpz sensors. The filter seems to match several frequencies. It shows peaks around 30, 45 and 65 Hz. <span class="todo"> talk more about where it is localized and check if it makes sense with increase around 45 Hz on these sensors? compare to csp/csp-net results?</span>

# In[23]:

for i_filt in feet_best[0:1]:
    fig = plot_sensor_signals(soft_weights[:,:,i_filt:i_filt+1].T, 
                        [''], 
                        figsize=(5,1.5))
    fig.axes[0].set_title("Combined Filter " + str(i_filt + 1) + ": Softmax Weights", fontsize=16)
    fig.axes[0].legend(class_names, bbox_to_anchor=(1.35, 1.), loc='upper right')
    pyplot.xticks([0,15,30,45], [0,1000,2000,3000])
    pyplot.xlabel('Time [ms] (at start of pool region)')
    fig = plot_head_signals_tight(combined_weights[i_filt], get_C_sensors_sorted())
    pyplot.xticks([15], [])
    highlight_chans(fig,  ['Cz', 'CPz'])


# In[36]:

bps, freqs = bps_and_freqs(combined_weights[feet_best[0]])
plot_head_signals_tight(bps, get_C_sensors_sorted(), hspace=1.5)
pyplot.xticks(np.arange(0,bps.shape[1]), [int(freqs[i]) if i % 3 == 0 else '' for i in range(0,bps.shape[1])])
None


# In conclusion, we can see that the network does learn to match oscillations in specific frequencies at specific locations and also learns a timecourse over the time of the trial.

# ### Patterns

# In[24]:

get_ipython().run_cell_magic(u'capture', u'', u"from motor_imagery_learning.train_scripts.train_with_params import (\n    create_training_object_from_file)\ninputs = create_rand_input(bhno_model, 3)\ntransformed_model = transform_raw_net_model(bhno_model)\n\nact_func_new = create_activations_func(transformed_model)\n\nact_func_old = create_activations_func(bhno_model)\nact_outs = act_func_new(inputs)\nact_outs_old = act_func_old(inputs)\n\nact_outs_restricted = deepcopy(act_outs)\nact_outs_restricted.pop(1) # remove activations not exsting in other model\nassert all([np.allclose(a1,a2) for a1,a2 in zip(act_outs_restricted, act_outs_old)])\n\nbhno_train = create_training_object_from_file(\n    'data/models/debug/raw-square/car/30-kernel-shape/early-stop/2.yaml')\nbhno_train.dataset_splitter.ensure_dataset_is_loaded()\ndatasets = bhno_train.get_train_fitted_valid_test()\ntrain_topo = datasets['train'].get_topological_view()\nacts_out = act_func_new(train_topo)\nacts_out[1].shape\n\npatterns = transform_to_patterns(combined_weights,train_topo, acts_out)")


# In[25]:

act_3 = acts_out[3]
act_3_soft_weight_shape = act_3.transpose(0,2,3,1)
assert np.array_equal(act_3_soft_weight_shape.squeeze().shape[1:], soft_weights.shape[1:]) 
act_3_for_covar = act_3_soft_weight_shape.reshape(act_3_soft_weight_shape.shape[0], -1).T
features_covar = np.cov(act_3_for_covar)


# In[26]:

soft_weights_for_pattern = soft_weights.reshape(soft_weights.shape[0],-1)
assert soft_weights_for_pattern.shape[0] == soft_weights.shape[0]
assert soft_weights_for_pattern.shape[1] == np.prod(soft_weights.shape[1:])
soft_weights_pattern = np.dot(features_covar , soft_weights_for_pattern.T)
assert soft_weights_pattern.shape[1] == soft_weights.shape[0]
assert soft_weights_pattern.shape[0] == np.prod(soft_weights.shape[1:])
# have to transpose to again have class dimension as first dimension
soft_weights_pattern = soft_weights_pattern.T.reshape(soft_weights.shape)
assert soft_weights_pattern.shape == soft_weights.shape


# We now show the corresponding patterns. The signal is now more "smeared", i.e. more spatially smooth, and higher frequencies are not present anymore. For the right hand, we can still see the larger amplitudes on the right side compared to the left side. We also see that while this features discriminates between right and left, it is more strongly activated by the rest and feet classes.

# In[27]:

# 21, 25 and 32 negative for right hand?
# 29 positive
for i_filt in right_best[0:1]:
    fig = plot_sensor_signals(soft_weights_pattern[:,:,i_filt:i_filt+1].T, 
                        [''], 
                        figsize=(5,1.5))
    fig.axes[0].set_title("Combined Pattern " + str(i_filt + 1) + ": Softmax Pattern", fontsize=16)
    fig.axes[0].legend(class_names, bbox_to_anchor=(1.35, 1.), loc='upper right')
    pyplot.xticks([0,15,30,45], [0,1000,2000,3000])
    pyplot.xlabel('Time [ms] (at start of pool region)')
    fig = plot_head_signals_tight(patterns[i_filt], get_C_sensors_sorted())
    pyplot.xticks([15], [])
    
    highlight_chans(fig,  ['FCC4h', 'C2'])
    highlight_chans(fig,  ['FCC1h', 'C3'], color=seaborn.color_palette()[1])


# We also see it has higher amplitudes on the expected side and is more strongly activated by rest and feet.

# In[28]:

for i_filt in left_best[0:1]:
    fig = plot_sensor_signals(soft_weights_pattern[:,:,i_filt:i_filt+1].T, 
                        [''], 
                        figsize=(5,1.5))
    fig.axes[0].set_title("Combined Pattern " + str(i_filt + 1) + ": Softmax Pattern", fontsize=16)
    fig.axes[0].legend(class_names, bbox_to_anchor=(1.35, 1.), loc='upper right')
    pyplot.xticks([0,15,30,45], [0,1000,2000,3000])
    pyplot.xlabel('Time [ms] (at start of pool region)')
    fig = plot_head_signals_tight(patterns[i_filt], get_C_sensors_sorted())
    pyplot.xticks([15], [])
    
    highlight_chans(fig,  ['CCP3h', 'C3'])
    highlight_chans(fig,  ['CCP4h', 'C4'], color=seaborn.color_palette()[1])


# For the rest class, we see mostly high amplitude low frequency signals, with some surprising exceptions such as C2 and CCP4h.

# In[29]:

class_names = ('Right Hand', 'Left Hand', 'Rest', 'Feet')
for i_filt in rest_best[0:1]:
    fig = plot_sensor_signals(soft_weights_pattern[:,:,i_filt:i_filt+1].T, 
                        [''], 
                        figsize=(5,1.5))
    fig.axes[0].set_title("Combined Pattern " + str(i_filt + 1) + ": Softmax Pattern", fontsize=16)
    fig.axes[0].legend(class_names, bbox_to_anchor=(1.35, 1.), loc='upper right')
    pyplot.xticks([0,15,30,45], [0,1000,2000,3000])
    pyplot.xlabel('Time [ms] (at start of pool region)')
    fig = plot_head_signals_tight(patterns[i_filt], get_C_sensors_sorted())
    pyplot.xticks([15], [])
    highlight_chans(fig,  ['C2', 'CCP4h'], color=seaborn.color_palette()[4],
                   alpha=0.4)


# For feet, we see some signals which have a large positive mean, for example Cz. We also see that this pattern is most activated in the first half of the trial for the feet class. This might indicate a discriminative low frequency component in the first half of the trial. 

# In[30]:

for i_filt in feet_best[0:1]:
    fig = plot_sensor_signals(soft_weights_pattern[:,:,i_filt:i_filt+1].T, 
                        [''], 
                        figsize=(5,1.5))
    fig.axes[0].set_title("Combined Pattern " + str(i_filt + 1) + ": Softmax Pattern", fontsize=16)
    fig.axes[0].legend(class_names, bbox_to_anchor=(1.35, 1.), loc='upper right')
    pyplot.xticks([0,15,30,45], [0,1000,2000,3000])
    pyplot.xlabel('Time [ms] (at start of pool region)')
    fig = plot_head_signals_tight(patterns[i_filt], get_C_sensors_sorted())
    pyplot.xticks([15], [])
    highlight_chans(fig,  ['Cz', 'FCC1h', 'FCC2h'], color=seaborn.color_palette()[4],
                   alpha=0.4)


# ### Gradient Descent

# Now we use gradient descent to reconstruct inputs for the different classes.

# In[44]:

activations_func = create_activations_func(bhno_model) # for tests wheter reconstruction rly has correct outputs
in_grad_func = create_input_grad_for_wanted_class_func(bhno_model)
batches = 3
start_in = create_rand_input(bhno_model, batches)
ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000)
inputs_right = gradient_descent_input(start_in, in_grad_func, np.float32([1,0,0,0]), **ascent_args)
inputs_right = inputs_right.squeeze()


# In[45]:

inputs_left = gradient_descent_input(start_in, in_grad_func, np.float32([0,1,0,0]), **dict(ascent_args, epochs=1500))
inputs_left = inputs_left.squeeze()


# Starting with the right hand class, we can see that the network seems to mostly use FCC4h to recognize the right hand class. The frequency decomposition shows a peak around 12 Hz. We also see some rather confusing low-frequency shapes.

# In[52]:

fig = plot_head_signals_tight(inputs_right[-1,0],
                        get_C_sensors_sorted(), plot_args=dict(linewidth=0.5),
                             hspace=1.5)
pyplot.xticks(np.arange(0,601,150), (0,1,2,3,4))
highlight_chans(fig,  ['FCC4h'], xstart=0, xstop=600)
None
    


# In[38]:

bps, freqs = bps_and_freqs(inputs_right[-1,0])
fig = plot_head_signals_tight(bps, get_C_sensors_sorted(), hspace=1.5)
pyplot.xticks(np.arange(0,bps.shape[1],50), np.int32(freqs[::50]))
highlight_chans(fig,  ['FCC4h'], xstart=40, xstop=60)
None


# For the left hand class, we see that the reconstructed input shows a strong lateralization to the left side (which is consistent with a decrease on the right side).

# In[53]:

fig = plot_head_signals_tight(inputs_left[-1,0],
                        get_C_sensors_sorted(), plot_args=dict(linewidth=0.5),
                             hspace=1.5)
pyplot.xticks(np.arange(0,601,150), (0,1,2,3,4))
highlight_chans(fig,  ['FC3', 'FCC3h'], xstop=600)
None


# For the rest class, we see a lot of variance on most sensors, with higher frequencies on Cz and CCP1h.

# In[46]:

inputs_rest = gradient_descent_input(start_in, in_grad_func, np.float32([0,0,1,0]), **dict(ascent_args, epochs=1500))
inputs_rest = inputs_rest.squeeze()


# In[54]:

fig = plot_head_signals_tight(inputs_rest[-1,0],
                        get_C_sensors_sorted(), plot_args=dict(linewidth=0.5),
                             hspace=1.5)
pyplot.xticks(np.arange(0,601,150), (0,1,2,3,4))
highlight_chans(fig,  ['Cz', 'CCP1h'], xstop=600)
None


# In[49]:

bps, freqs = bps_and_freqs(inputs_rest[-1,0])
fig = plot_head_signals_tight(bps, get_C_sensors_sorted(), hspace=1.5)
pyplot.xticks(np.arange(0,bps.shape[1],50), np.int32(freqs[::50]))
highlight_chans(fig,  ['Cz', 'CCP1h'], xstart=145, xstop=170)
None


# The reconstructed feet shows high amplitudes on the left and the right side and low amplitudes in the center.
# 

# In[47]:

inputs_feet = gradient_descent_input(start_in, in_grad_func, np.float32([0,0,0,1]), **dict(ascent_args, epochs=1500))
inputs_feet = inputs_feet.squeeze()


# In[55]:

fig = plot_head_signals_tight(inputs_feet[-1,0],
                        get_C_sensors_sorted(), plot_args=dict(linewidth=0.5),
                             hspace=1.5)
pyplot.xticks(np.arange(0,601,150), (0,1,2,3,4))

highlight_chans(fig,  ['FCC3h', 'C3', 'FCC4h', 'C4'], xstart=0, xstop=600)
highlight_chans(fig,  ['Cz', 'CPz'], xstart=0, xstop=600, 
                color=seaborn.color_palette()[1])
None


# ### Recognizing signal potentials

# In[31]:

from motor_imagery_learning.train_scripts.train_with_params import (
create_training_object_from_file)
#before data/models/debug/raw-square/larger-kernel-shape/early-stop/8
laka_train_obj = create_training_object_from_file(
    'data/models/debug/raw-square/car/30-kernel-shape/early-stop/8.yaml')
laka_train_obj.dataset_splitter.ensure_dataset_is_loaded()
laka_sets = laka_train_obj.get_train_fitted_valid_test()
laka_model = serial.load(
    'data/models/debug/raw-square/car/30-kernel-shape/early-stop/8.pkl').model


temporal_weights = laka_model.get_weights_topo()[:,::-1,::-1,:]
spat_filt_weights = laka_model.layers[1].get_weights_topo()[:,::-1,::-1,:]

combined_weights = np.tensordot(spat_filt_weights, temporal_weights, axes=(3,0))

combined_weights = combined_weights.squeeze()

soft_weights = laka_model.layers[-1].get_weights_topo()
soft_weights = soft_weights.squeeze()
sorted_diffs = argsort_by_class(soft_weights, 0, 1)

right_best = sorted_diffs[[-1,-2]]
left_best = sorted_diffs[[0,1]]


# In[32]:

i_interesting_filt = 26 # determined by ploitting alll


# Now we go on to show a case where the network has possibly learned to recognize a signal potential (or atleast a low-frequency oscillation with just one period) and not an  oscillation, which oscillates for a longer time. 
# We show a combined filter (i.e., a filter from the second layer) that has a large difference between the weights for the right hand at the left hand at the start of the trial. If the filter learns to use the variance of an oscillatory signal, we would expect it to not use the start of the trial. The variance changes usually only some time after the start of the trial (e.g., 500 ms after the start). Furthermore, for some sensors, the filter is not oscillating around the dotted zero-line, for example it is clearly above the line for CCP2h and below for CCP4h.

# In[33]:

for i_filt in [i_interesting_filt]:
    fig = plot_sensor_signals(soft_weights[:,:,i_filt:i_filt+1].T, 
                        [''], 
                        figsize=(5,1.5))
    fig.axes[0].set_title("Combined Filter " + str(i_filt + 1) + ": Softmax Weights", fontsize=16)
    fig.axes[0].legend(class_names, bbox_to_anchor=(1.35, 1.), loc='upper right')
    pyplot.xticks([0,15,30,45], [0,1000,2000,3000])
    pyplot.xlabel('Time [ms] (at start of pool region)')
    fig = plot_head_signals_tight(combined_weights[i_filt], get_C_sensors_sorted())
    pyplot.xticks([15], [])
    
    highlight_chans(fig,  ['CCP2h'])
    highlight_chans(fig,  ['CCP4h'], color=seaborn.color_palette()[1])
    


# In order to better understand what this filter might encode, we can again use gradient descent on the input. We want to see how this filter might help recognize a trial for the left hand class. 
# First, we take some random input, remove all other filters from the second layer from the model and iteratively optimize the input to lead to a high prediction for class 2 and low predictions for the other classes as before.
# 
# We show this for six different random intializations. We see a recognizable shape at the start of the trial.

# In[53]:

from motor_imagery_learning.analysis.util import create_activations_func
activations_func = create_activations_func(laka_model)

activations = activations_func(laka_sets['train'].get_topological_view())


# In[54]:

copied_model = deepcopy(laka_model)


# In[55]:

get_ipython().run_cell_magic(u'capture', u'', u'spat_layer = copied_model.layers[1]\nspat_layer.kernel_shape = (1,-1)\nspat_layer.output_channels = 1\nspat_layer.set_input_space(copied_model.layers[0].get_output_space())\ncopied_model.layers[-2].set_input_space(copied_model.layers[-3].get_output_space())\ncopied_model.layers[-1].set_input_space(copied_model.layers[-2].get_output_space())')


# In[56]:

W, b = laka_model.layers[-3].get_param_values()

spat_layer.set_biases(np.copy(b[i_interesting_filt:i_interesting_filt+1]))

spat_layer.set_weights(np.copy(W[i_interesting_filt:i_interesting_filt+1]))


# In[57]:

soft_layer = copied_model.layers[-1]
b, W = laka_model.layers[-1].get_param_values()
soft_layer.set_biases(np.copy(b))

soft_weights_topo = laka_model.layers[-1].get_weights_topo()
soft_weights_topo = soft_weights_topo[:,:,0,i_interesting_filt]

soft_weights_topo = soft_weights_topo.T
soft_layer.set_weights(np.copy(soft_weights_topo))


# In[58]:

copied_model_acts_func = create_activations_func(copied_model)


# In[59]:

activations = activations_func(laka_sets['train'].get_topological_view())
other_acts = copied_model_acts_func(laka_sets['train'].get_topological_view())
assert np.allclose(activations[0], other_acts[0])
assert np.allclose(activations[1][:,i_interesting_filt:i_interesting_filt+1,:,:],
           other_acts[1])
assert np.allclose(activations[2][:,i_interesting_filt:i_interesting_filt+1,:,:],
           other_acts[2])
assert not np.allclose(activations[3], other_acts[3])


# In[60]:

in_grad_func = create_input_grad_for_wanted_class_func(copied_model)
batches = 6
start_in = create_rand_input(copied_model, batches)
ascent_args = dict(epochs=500, lrate=100, decay=0.98)
inputs_left = gradient_descent_input(start_in, in_grad_func, np.float32([0,1,0,0]), **ascent_args)
inputs_left = inputs_left.squeeze()


# In[62]:

fig = plot_head_signals_tight(inputs_left[-1,:].squeeze().transpose(1,2,0), get_C_sensors_sorted())
highlight_chans(fig,  ['CCP2h'], xstop=90)
highlight_chans(fig,  ['C4'], xstop=90)
None


# To confirm that this could indeed be a useful feature, we compute the mean of all trials for the left hand and the trial mean for all trials of the other classes. We show the difference $mean(Trials_{left}) - mean(Trials_{non-left})$. 
# 

# In[64]:


left_topo = laka_sets['train'].get_topological_view()[laka_sets['train'].y[:,1] ==1]

right_topo = laka_sets['train'].get_topological_view()[laka_sets['train'].y[:,0] ==1]
not_left_topo = laka_sets['train'].get_topological_view()[laka_sets['train'].y[:,0] != 1]


# In[66]:

diff_mean = np.mean(left_topo, axis=0) - np.mean(not_left_topo, axis=0)
fig = plot_head_signals_tight(diff_mean, get_C_sensors_sorted())
highlight_chans(fig,  ['CCP2h'], xstop=120)
highlight_chans(fig,  ['C4'], xstop=120)
None


# Indeed we see a recognizable shape atleast partially consistent with our reconstruction. For example the negativity for sensor C4 matches the reconstruction and the missing negativity for CCP2h also matches the reconstruction.
# 
# All in all, we have shown that the network can also recognize a signal potential or atleast recognize a single period in a low frequency <span class="todecide">maybe rewrite this sentence?</span>.
