
# coding: utf-8

# In[3]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nimport os\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n#os.environ[\'CUDA_LAUNCH_BLOCKING\'] = \'1\'\n#os.environ[\'THEANO_FLAGS\'] = os.environ[\'THEANO_FLAGS\'] + ",profile=True"\n# switch to cpu\n#os.environ[\'THEANO_FLAGS\'] = \'floatX=float32,device=cpu,nvcc.fastmath=True\'\n%load_ext autoreload\n%autoreload 2\n\nfrom motor_imagery_learning.bbci_pylearn_dataset import (compute_power_spectra)\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset\nimport numpy as np\n\nimport matplotlib\nfrom matplotlib import pyplot\nfrom matplotlib import cm\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (8.0, 2.0)\nmatplotlib.rcParams[\'font.size\'] = 7\n\nfrom pylearn2.costs.mlp import Default\nfrom pylearn2.models.mlp import Softmax, MLP\nfrom pylearn2.termination_criteria import EpochCounter\nimport seaborn\nseaborn.set_style(\'darkgrid\')\nfrom copy import deepcopy\nfrom motor_imagery_learning.train_scripts.train_experiments import ExperimentsRunner\nfrom motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer\nfrom motor_imagery_learning.analysis.data_generation import (create_sine_signal,\n    pipeline, create_shifted_sines,put_noise, convolve_with_weight,\n    max_pool_topo_view, log_sum_pool)\nfrom motor_imagery_learning.analysis.util import create_trainer\nfrom motor_imagery_learning.mypylearn2.conv_squared import ConvSquared\nfrom motor_imagery_learning.analysis.input_reconstruction import (\n    create_rand_input, gradient_descent_input, create_input_grad_for_wanted_class_func,\n    create_input_grad_with_smooth_penalty_for_wanted_class_func, \n    reconstruct_inputs_for_class)\n\nfrom motor_imagery_learning.analysis.plot_util import (plot_misclasses_for_model, \n    plot_nlls_for_model, plot_sensor_signals)\n\nimport h5py\nimport numpy as np\nfrom pylearn2.utils import serial\nfrom pprint import pprint\nfrom  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted\nimport h5py\nfrom motor_imagery_learning.csp.train_csp import CSPTrain\nfrom motor_imagery_learning.mypylearn2.train import TrainEarlyStop\nfrom motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD\nfrom motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset\nfrom motor_imagery_learning.bbci_dataset import BBCIDataset\nfrom pylearn2.costs.mlp import Default\nfrom pylearn2.models.mlp import Softmax, MLP\nfrom pylearn2.termination_criteria import EpochCounter\nfrom sklearn.cross_validation import KFold\nfrom pylearn2.datasets.dense_design_matrix import DenseDesignMatrix\nimport numpy as np\nfrom numpy.random import RandomState\nimport theano.tensor as T\nimport theano\nfrom pylearn2.space import Conv2DSpace\nfrom pylearn2.models.mlp import ConvElemwise\nfrom theano.sandbox.cuda.dnn import dnn_conv\nfrom theano.sandbox.cuda.basic_ops import gpu_alloc_empty\nfrom motor_imagery_learning.analysis.print_results import ResultPrinter\nfrom pylearn2.utils import serial\nfrom motor_imagery_learning.analysis.data_generation import generate_single_sensor_data\nfrom motor_imagery_learning.analysis.plot.raw_signals import (\n    plot_sensor_signals_two_classes)\nfrom motor_imagery_learning.mypylearn2.pool import PoolLayer, log_sum\nfrom motor_imagery_learning.analysis.util import bps_and_freqs\nfrom motor_imagery_learning.analysis.deconv import deconv_model\nfrom motor_imagery_learning.analysis.plot_util import (\n    plot_head_signals_tight_two_signals, plot_head_signals_tight)\nfrom motor_imagery_learning.analysis.util import (\ncreate_activations_func)\nfrom motor_imagery_learning.analysis.plot_util import plot_chan_matrices\nfrom motor_imagery_learning.train_scripts.train_with_params import (\ncreate_training_object_from_file)')


# In[2]:

get_ipython().run_cell_magic(u'javascript', u'', u"IPython.load_extensions('cite2c/main');")


# In[2]:

get_ipython().run_cell_magic(u'html', u'', u'<style>\ndiv.input {\nwidth: 105ex; /* about 80 chars + buffer */\n...\n}\ndiv.text_cell {\nwidth: 105ex /* instead of 100%, */\n...\n}\n\ndiv.text_cell_render {\n/*font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;*/\nfont-family: "Charis SIL", serif; /* Make non-code text serif. */\nline-height: 145%; /* added for some line spacing of text. */\nwidth: 105ex; /* instead of \'inherit\' for shorter lines */\n...\n}\n\n/* Set the size of the headers */\ndiv.text_cell_render h1 {\nfont-size: 18pt;\n}\n\ndiv.text_cell_render h2 {\nfont-size: 14pt;\n}\n\n\n\n.CodeMirror {\nfont-family: Consolas, monospace;\n}\n\n.todo {\n    background-color: hsla(120, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n.todecide {\n    background-color: hsla(240, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n\n</style>\n\n<style>\ndiv.summary  {\n    background-color: hsla(40,0%,50%,0.2);\n    color:black;\n    border-radius: 5px;\n    padding: 2px;\n    margin-top:0px;\n}\ndiv.summary ul\n{\n    list-style-type: none;\n}\n</style>\n<link rel="stylesheet" href="./motivation.css" type="text/css">\n<script async defer src="//hypothes.is/embed.js"></script>')


# ## FFT Net Visualizations

# <div class="summary">
# <ul>
# <li>FFT Net shows plausible patterns</li>
# <li> FFT Net learns to use higher frequencies</li>
# </ul>
# </div>
# 

# We now show some visualizations of our FFT Net. 
# Since our FFT Net has no activation functions between the layers, it is essentially a linear classifier.
# So we can transform our weights for the different layers into just one weight vector per class.
# We will show these weight vectors directly and transform the weight vectors into more interpretable patterns.

# In[4]:

bhno_model = serial.load('data/models/fft/merged-freqs/car/early-stop/2.pkl').model


# In[271]:

#bhno_model.monitor.channels['test_y_misclass'].val_record[-1]


# In[5]:

chan_weight = bhno_model.get_weights_topo()
chan_weight.shape
#flip convolution across freq dim
freq_weight = bhno_model.layers[1].get_weights_topo()[:,:,::-1,:]
freq_weight.shape
time_weight = bhno_model.layers[2].get_weights_topo()
time_weight.shape
chan_freq_weight = np.tensordot(freq_weight, chan_weight, axes=(3,0))
chan_freq_weight = chan_freq_weight[:,:,:,0,0,:]
chan_freq_weight.shape
combined_weight = np.tensordot(time_weight, chan_freq_weight, axes=(3,0))
combined_weight = combined_weight[:,:,0,0,:,:]
combined_weight = combined_weight.transpose(0,3,1,2)


# In[6]:


def unfold_fft_weight(weight):
    """ Unfolds an fft weight tensor, so that the higher frequencies are unmerged.
    I.e. it assumes frequencies above the 16h bin (30 hz) were merged in blocks of
    3. and now it unfolds them again to 3."""
    unchanged_part = weight[:,:,:16]
    part_to_unfold = weight[:,:,16:]
    #np.concatenate((, test_weight[:,:,16:]), axis=2).shape

    unfolded_part = np.ones((part_to_unfold.shape[0], part_to_unfold.shape[1], 
                       part_to_unfold.shape[2] * 3)) * np.nan

    unfolded_part[:,:,::3] = part_to_unfold
    unfolded_part[:,:,1::3] = part_to_unfold
    unfolded_part[:,:,2::3] = part_to_unfold

    unfolded = np.concatenate((unchanged_part, unfolded_part), axis=2)

    assert not np.any(np.isnan(unfolded_part))
    assert not np.any(np.isnan(unfolded))
    return unfolded


# ### Weights
# 
# We first show the weights for the right hand class. 
# Note that, for the frequencies until 30 Hz, the bins have height 2 Hz and for higher frequencies, the bins have height 6 Hz.
# However, the images are scaled so that the y axis always has the same scale for any interval, i.e., the distance between 20 Hz and 30 Hz is the same as the distance between 30 Hz and 40 Hz. Therefore, the weights from 30 Hz onwards will occupy three times the height per weight in each image. 
# Also note that our network was trained on standardized data. So, for each channel X frequency row we computed the mean and standard deviation and for all features of this chan and frequency, we subtracted this mean and divided by this standard deviation.
# 
# You can see positive weights for the higher frequencies on the left side and negative weights on the right side, as expected. Also you can see some negative weights for the lower frequencies on the left side, for example on C3 and CCP3h. These are less visible on the right side, also as expected.

# In[46]:

from matplotlib.patches import Ellipse, Rectangle



# In[65]:

def add_ellipse_to_chan_matrices(fig, chan_names, freq_start, freq_stop):
    all_chan_names = np.array(get_C_sensors_sorted())
    highlight_chan_inds = [np.nonzero(all_chan_names == c)[0][0] for c in chan_names]
    
    x = 8
    width = 14
    y = (freq_start + freq_stop) / 4
    height = (freq_start - freq_stop) / 2
    for i_chan in highlight_chan_inds:
        fig.axes[i_chan].add_artist(Ellipse(xy=(8,y), width=width, 
                                   height=height, linestyle='solid', 
                                       linewidth=2, fill=False))

def add_rectangle_to_chan_matrices(fig, chan_names, freq_start, freq_stop):
    all_chan_names = np.array(get_C_sensors_sorted())
    highlight_chan_inds = [np.nonzero(all_chan_names == c)[0][0] for c in chan_names]
    x = 8
    width = 15
    y = freq_stop / 2
    height = (freq_start - freq_stop) / 2
    for i_chan in highlight_chan_inds:
        fig.axes[i_chan].add_artist(Rectangle(xy=(0,y), width=width, 
                                   height=height, linestyle='solid', 
                                       linewidth=2, fill=False))


# In[66]:

fig = plot_chan_matrices(unfold_fft_weight(combined_weight[0]), get_C_sensors_sorted(),
                  yticks=[0,15,30,45,60], yticklabels=[0,30,60,90,120], figsize=(12,8),
                  colormap=cm.coolwarm)

for ax in fig.axes:
    ax.tick_params(axis='y', which='major', labelsize=8)
    
add_ellipse_to_chan_matrices(fig, ['C3', 'CCP3h', 'C2', 'CCP4h'], 4, 26)

    
add_ellipse_to_chan_matrices(fig, ['C3', 'CCP3h', 'C2', 'CCP4h'], 45, 120)
None


# The weights for the left hand also show what we expect.
# They look like a color-flipped version of the right hand weights. For example, the sensors on the right side have positive weights on the higher frequencies.

# In[68]:

fig = plot_chan_matrices(unfold_fft_weight(combined_weight[1]), get_C_sensors_sorted(),
                  yticks=[0,15,30,45,60], yticklabels=[0,30,60,90,120], figsize=(12,8),
                  colormap=cm.coolwarm)

for ax in fig.axes:
    ax.tick_params(axis='y', which='major', labelsize=8)
    
add_ellipse_to_chan_matrices(fig, ['C3', 'CCP3h', 'C2', 'CCP4h'], 4, 26)

    
add_ellipse_to_chan_matrices(fig, ['C3', 'CCP3h', 'C2', 'CCP4h'], 45, 120)
None


# The weights for the rest class also look plausible. They show negative weights for the higher frequencies, and higher weights on the low frequencies, more pronounced on the central and centre-right electrodes. 

# In[79]:

fig = plot_chan_matrices(unfold_fft_weight(combined_weight[2]), get_C_sensors_sorted(),
                  yticks=[0,15,30,45,60], yticklabels=[0,30,60,90,120], figsize=(12,8),
                  colormap=cm.coolwarm)

for ax in fig.axes:
    ax.tick_params(axis='y', which='major', labelsize=8)
    

    
add_ellipse_to_chan_matrices(fig, ['Cz', 'CCP1h', 'C2', 'CPP5h'], 70, 110)
add_ellipse_to_chan_matrices(fig, ['Cz', 'CCP1h', 'C2', 'CPP5h'], 30, 55)
add_ellipse_to_chan_matrices(fig, ['Cz', 'CCP1h', 'C2', 'CPP5h'], 6, 22)
None


# Finally, the feet class weights are mostly the color-flipped weights of the rest class, for example positive instead of negative weights on the higher frequencies for the central electrodes.

# In[81]:

fig = plot_chan_matrices(unfold_fft_weight(combined_weight[3]), get_C_sensors_sorted(),
                  yticks=[0,15,30,45,60], yticklabels=[0,30,60,90,120], figsize=(12,8),
                  colormap=cm.coolwarm)
for ax in fig.axes:
    ax.tick_params(axis='y', which='major', labelsize=8)
    

    
add_ellipse_to_chan_matrices(fig, ['Cz', 'CCP1h', 'C2', 'CPP5h'], 70, 110)
add_ellipse_to_chan_matrices(fig, ['Cz', 'CCP1h', 'C2', 'CPP5h'], 30, 55)
add_ellipse_to_chan_matrices(fig, ['Cz', 'CCP1h', 'C2', 'CPP5h'], 6, 22)
None


# In[82]:

bhno_train = create_training_object_from_file(
    'data/models/fft/merged-freqs/car/early-stop/2.yaml')

bhno_train.dataset_splitter.ensure_dataset_is_loaded()
bhno_sets = bhno_train.get_train_fitted_valid_test()


# In[83]:

train_topo = bhno_sets['train'].get_topological_view()


# In[84]:

train_for_covar = train_topo.reshape(train_topo.shape[0], -1).T


# In[85]:

#%%time
train_cov = np.cov(train_for_covar)


# ### Patterns
# 
# We now transform the weights to more interpretable patterns by multiplying the covariance matrix of all features with the weights. 
# For the right hand, we now see that the negative low frequencies are present on both sides, just a little stronger on the left side. The difference for the higher frequencies seems more pronounced if you compare C3 and C4 or FCC3h and FCC4h. 

# In[86]:

combined_pattern = np.dot(train_cov, combined_weight.reshape(
        combined_weight.shape[0],-1).T)
combined_pattern = combined_pattern.T
combined_pattern = combined_pattern.reshape(combined_weight.shape)


# In[88]:

fig = plot_chan_matrices(unfold_fft_weight(combined_pattern[0]), get_C_sensors_sorted(),
                  yticks=[0,15,30,45,60], yticklabels=[0,30,60,90,120], figsize=(12,8),
                  colormap=cm.coolwarm)
for ax in fig.axes:
    ax.tick_params(axis='y', which='major', labelsize=8)
    

    
add_ellipse_to_chan_matrices(fig, ['C3', 'FCC3h', 'C4', 'FCC4h'], 60, 100)
None


# We see the high-frequency pattern on the right side for the left hand. 

# In[89]:

fig = plot_chan_matrices(unfold_fft_weight(combined_pattern[1]), get_C_sensors_sorted(),
                  yticks=[0,15,30,45,60], yticklabels=[0,30,60,90,120], figsize=(12,8),
                  colormap=cm.coolwarm)
for ax in fig.axes:
    ax.tick_params(axis='y', which='major', labelsize=8)
    

    
add_ellipse_to_chan_matrices(fig, ['C3', 'FCC3h', 'C1', 'CCP3h',
                                   'C4', 'FCC4h', 'C2', 'CCP4h'], 60, 100)
None


# The rest class has large low-frequency patterns across all sensors and some decrease in the high frequencies.

# In[90]:

fig = plot_chan_matrices(unfold_fft_weight(combined_pattern[2]), get_C_sensors_sorted(),
                  yticks=[0,15,30,45,60], yticklabels=[0,30,60,90,120], figsize=(12,8),
                  colormap=cm.coolwarm)
for ax in fig.axes:
    ax.tick_params(axis='y', which='major', labelsize=8)
    

    
add_ellipse_to_chan_matrices(fig, ['C3', 'FCC3h',
                                   'C4', 'FCC4h', 'C2', 'CCP4h'], 60, 90)

None


# For the rest class we see some high frequency patterns mostly on the central electodes Cz, CCP1h, CCP2h.

# In[93]:

fig = plot_chan_matrices(unfold_fft_weight(combined_pattern[3]), get_C_sensors_sorted(),
                  yticks=[0,15,30,45,60], yticklabels=[0,30,60,90,120], figsize=(12,8),
                  colormap=cm.coolwarm)
for ax in fig.axes:
    ax.tick_params(axis='y', which='major', labelsize=8)
    

    
add_ellipse_to_chan_matrices(fig, ['Cz', 'CCP2h', 'CCP1h'], 60, 100)
add_ellipse_to_chan_matrices(fig, ['Cz', 'CCP2h', 'CCP1h'], 4, 22)
None


# ###Comparison to Softmax

# To show that our network succesfully regularizes the weights, we also show the weights for the regular softmax trained on the same data.
# We will only show the weights for the right hand here.
# We see that they indeed have a much more irregular, "noisy", structure.

# In[95]:

softmax_model = serial.load(
    'data/models/fft/merged-freqs/car/softmax/until-144/early-stop/4.pkl').model
#softmax_model.monitor.channels['test_y_misclass'].val_record[-1]


# In[96]:

soft_weights = softmax_model.get_weights().T.reshape(4,45,15,35)


# In[99]:

fig = plot_chan_matrices(unfold_fft_weight(soft_weights[0]), get_C_sensors_sorted(),
                  yticks=[0,15,30,45,60], yticklabels=[0,30,60,90,120], figsize=(12,8),
                  colormap=cm.coolwarm)
for ax in fig.axes:
    ax.tick_params(axis='y', which='major', labelsize=8)
    

    
add_ellipse_to_chan_matrices(fig, ['C3', 'FCC3h', 'C1', 'CCP3h',
                                   'C4', 'FCC4h', 'C2', 'CCP4h'], 60, 100)

add_ellipse_to_chan_matrices(fig, ['C3', 'FCC3h', 'C1', 'CCP3h',
                                   'C4', 'FCC4h', 'C2', 'CCP4h'], 2, 26)
None


# All in all, you can see that the fft net uses higher frequencies and spatial structure and seems to successfully regularize the weights.
