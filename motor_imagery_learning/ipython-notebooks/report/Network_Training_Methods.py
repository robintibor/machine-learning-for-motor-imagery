
# coding: utf-8

# In[1]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nimport os\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n#os.environ[\'CUDA_LAUNCH_BLOCKING\'] = \'1\'\n#os.environ[\'THEANO_FLAGS\'] = os.environ[\'THEANO_FLAGS\'] + ",profile=True"\n# switch to cpu\nos.environ[\'THEANO_FLAGS\'] = \'floatX=float32,device=cpu,nvcc.fastmath=True\'\n%load_ext autoreload\n%autoreload 2\n\nfrom motor_imagery_learning.bbci_pylearn_dataset import (compute_power_spectra)\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset\nimport numpy as np\n\nimport matplotlib\nfrom matplotlib import pyplot\nfrom matplotlib import cm\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (8.0, 2.0)\nmatplotlib.rcParams[\'font.size\'] = 7\n\nfrom pylearn2.costs.mlp import Default\nfrom pylearn2.models.mlp import Softmax, MLP\nfrom pylearn2.termination_criteria import EpochCounter\nimport seaborn\nseaborn.set_style(\'darkgrid\')\nfrom copy import deepcopy\nfrom motor_imagery_learning.train_scripts.train_experiments import ExperimentsRunner\nfrom motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer\nfrom motor_imagery_learning.analysis.data_generation import (create_sine_signal,\n    pipeline, create_shifted_sines,put_noise, convolve_with_weight,\n    max_pool_topo_view, log_sum_pool)\nfrom motor_imagery_learning.analysis.util import create_trainer\nfrom motor_imagery_learning.mypylearn2.conv_squared import ConvSquared\nfrom motor_imagery_learning.analysis.input_reconstruction import (\n    create_rand_input, gradient_descent_input, create_input_grad_for_wanted_class_func,\n    create_input_grad_with_smooth_penalty_for_wanted_class_func, \n    reconstruct_inputs_for_class)\n\nfrom motor_imagery_learning.analysis.plot_util import (plot_misclasses_for_model, \n    plot_nlls_for_model, plot_sensor_signals)\n\nimport h5py\nimport numpy as np\nfrom pylearn2.utils import serial\nfrom pprint import pprint\nfrom  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted\nimport h5py\nfrom motor_imagery_learning.csp.train_csp import CSPTrain\nfrom motor_imagery_learning.mypylearn2.train import TrainEarlyStop\nfrom motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD\nfrom motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset\nfrom motor_imagery_learning.bbci_dataset import BBCIDataset\nfrom pylearn2.costs.mlp import Default\nfrom pylearn2.models.mlp import Softmax, MLP\nfrom pylearn2.termination_criteria import EpochCounter\nfrom sklearn.cross_validation import KFold\nfrom pylearn2.datasets.dense_design_matrix import DenseDesignMatrix\nimport numpy as np\nfrom numpy.random import RandomState\nimport theano.tensor as T\nimport theano\nfrom pylearn2.space import Conv2DSpace\nfrom pylearn2.models.mlp import ConvElemwise\nfrom theano.sandbox.cuda.dnn import dnn_conv\nfrom theano.sandbox.cuda.basic_ops import gpu_alloc_empty\nfrom motor_imagery_learning.analysis.print_results import ResultPrinter\nfrom pylearn2.utils import serial\nfrom motor_imagery_learning.analysis.data_generation import generate_single_sensor_data\nfrom motor_imagery_learning.analysis.plot.raw_signals import (\n    plot_sensor_signals_two_classes)\nfrom motor_imagery_learning.mypylearn2.pool import PoolLayer, log_sum\nfrom motor_imagery_learning.analysis.util import bps_and_freqs\nfrom motor_imagery_learning.analysis.deconv import deconv_model\nfrom motor_imagery_learning.analysis.plot_util import (\n    plot_head_signals_tight_two_signals, plot_head_signals_tight)\nfrom motor_imagery_learning.csp.print_csp_results import CSPResultPrinter\nfrom motor_imagery_learning.analysis.filterbands import (\n    sum_abs_filterband_weights, compute_center_freqs, \n    compute_sum_abs_weights_for_csp_model)\nfrom motor_imagery_learning.analysis.plot.filterbands import (\n    show_freq_importances, plot_filterband_weights_for_train_state)\nfrom motor_imagery_learning.csp.train_csp import generate_filterbank\nfrom motor_imagery_learning.train_scripts.train_with_params import (\n    create_training_object_from_file)\nfrom motor_imagery_learning.mywyrm.plot import ax_scalp, CHANNEL_10_20\nfrom motor_imagery_learning.analysis.util import create_activations_func')


# In[2]:

get_ipython().run_cell_magic(u'javascript', u'', u"IPython.load_extensions('cite2c/main');")


# In[2]:

get_ipython().run_cell_magic(u'html', u'', u'<style>\ndiv.input {\nwidth: 105ex; /* about 80 chars + buffer */\n...\n}\ndiv.text_cell {\nwidth: 105ex /* instead of 100%, */\n...\n}\n\ndiv.text_cell_render {\n/*font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;*/\nfont-family: "Charis SIL", serif; /* Make non-code text serif. */\nline-height: 145%; /* added for some line spacing of text. */\nwidth: 105ex; /* instead of \'inherit\' for shorter lines */\n...\n}\n\n/* Set the size of the headers */\ndiv.text_cell_render h1 {\nfont-size: 18pt;\n}\n\ndiv.text_cell_render h2 {\nfont-size: 14pt;\n}\n\n\n\n.CodeMirror {\nfont-family: Consolas, monospace;\n}\n\n.todo {\n    background-color: hsla(120, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n.todecide {\n    background-color: hsla(240, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n\n</style>\n\n<style>\ndiv.summary  {\n    background-color: hsla(40,0%,50%,0.2);\n    color:black;\n    border-radius: 5px;\n    padding: 2px;\n    margin-top:0px;\n}\ndiv.summary ul\n{\n    list-style-type: none;\n}\n</style>\n<link rel="stylesheet" href="./motivation.css" type="text/css">\n<script async defer src="//hypothes.is/embed.js"></script>')


# # Network Training Methods

# <div class="summary">
# <ul>
# <li>We use the adaptive learning rule Adam for our gradients</li>
# <li>We use dropout-regularization on all layers except pooling and input layers</li>
# <li>We use early stopping and continue to train on the validation set after stopping</li>
# </ul>
# </div>

# We aim to have a network training procedure that:
# 
# * leads to reasonable performance for all of our architectures
# * keeps training time short
# * does not require a lot of manual tuning of hyperparameters
# 
# We describe the learning rule and the regularizations including the termination procedure that should achieve this in the following.

# ## Learning Rule - Adam

# We train our networks with stochastic gradient descent. When optimizing a neural network with stochastic gradient descent, you compute gradients of your loss function for all weights of the network on mini-batches from your training set. You know need a learning rule to tell you how to apply these gradients to your weights. The simplest learning rule is to subtract the gradients, maybe multiplied with a learning rate, from your weights. 
# 
# This has several problems:
# 
# * training might slow down when the gradients become small, even if they are always pointing in the same direction (i.e., you are on a plateau of the error function)
# * gradients for the different weights can be on very different scales which might disrupt learning
# 
# One learning rule to combat these problems is Adam <a data-cite="349468/KXPDSGXF"target="_blank" href="http://arxiv.org/abs/1412.6980">(Kingma & Ba, 2014-12-22)</a>. Adam uses an exponentially moving average of the gradients and an exponential moving average of the squared gradients for each weight to compute the weight update. It essentially updates a weight $w_i$ at timestep $t + 1$ like this:
# $$
# weight_{i, t+1} = weight_{i, t} - learning \ rate \cdot \frac{Exponential \ moving \ average \ of \ gradients_{t+1,i}}{\sqrt{Exponential \ moving \ average \ of \ squared \ gradients_{t+1,i}}}
# $$
# 
# It initializes the estimates of the moving averages to be zero and accounts for the bias of this initialization. The fraction in the equation above is supposed to capture the certainty about the whether the negative moving average of the gradient points in a direction that will minimize our loss. Those weights, whose negative gradient is more likely to point a loss-minimizing direction, can be updated more strongly. If the computed gradients on the mini-batches switch directions often, this ratio will become small: the moving average of the gradient will become relatively small compared to the moving average of the squared gradient. Conversely, if the computed gradients have roughly the same direction, the ratio will be larger. Also, Adam will give the same updates for gradients that are just scaled to be larger or smaller: The scale factor will cancel out and the "certainty" ratio will remain the same.
# 
# 
# Adam has three hyperparameters (the learning rate and two decay factors for the moving averages). In practice, Adam often achieves good results when leaving these hyperparameters to the default values suggested by the authors in their paper. Therefore, it is a quite useful learning rule for us. It especially helped training the raw nets, sometimes improving their accuracy by more than 10%. We train SGD with Adam with a batch size of 55.

# ## Regularization - Dropout and Weight Norms

# Regularization is typically important for any machine learning approach. This is especially true for neural networks due to their large number of parameters, which makes them easy to overfit to your training data.
# 
# We mainly use dropout <a data-cite="349468/6G2F6VE7"target="_blank" href="http://dl.acm.org/citation.cfm?id=2670313">(Srivastava et al., 2014)</a> to regularize our networks. Dropout tries to prevent units in the network from co-adapting too much and can also be seen as a form of model averaging.
# 
# During training, in every epoch, dropout randomly drops units from the network when computing the gradient. Therefore, it is  more difficult for a unit to overfit by co-adapting to another unit, since this other unit will sometimes be turned off (with dropout probability 0.5, it will be turned off in half of the epochs in expectation). Another way to interpret dropout is to see it as an online training procedure for $2^{|Units|}$ networks with shared weights, namely the networks for all combinations of dropped out and not dropped out units.
# 
# It is easily possible to have a single network that uses all units at test time. For this, during training time, we weigh the inputs to any layer by the inverse of their dropout probability (i.e., we multiply them by 2 for dropout probability 0.5). At test time, we do not weigh the inputs and instead multiply all weights by 0.5. This approximates averaging over the outputs of all the $2^{|Units|}$ networks.
# 
# In our case, we do not use dropout on the inputs and the pooling layers. Applying dropout to inputs almost always caused either no improvement or worsening of the accuracies of our models. For pooling layers, typically, dropout does not help, although now techniques exist to also use dropout on pooling layers, which we have not tried yet <a data-cite="349468/IKZ5VDWE"target="_blank" href="http://arxiv.org/abs/1506.02158">(Gal & Ghahramani, 2015-06-06) </a>
# <a data-cite="349468/QFD3NI5A"target="_blank" href="http://mlg.eng.cam.ac.uk/yarin/blog_3d801aa532c1ce.html#uncertainty-sense">(Gal, July 3rd, 2015)</a>.
# 
# We use dropout with probability 0.5 on all other layers.
# 
# To further regularize our networks, we enforce that our convolutional kernels have a maximum L2-norm of 2 and our softmax weight vectors have a maximum L2-norm of 0.5. That means, in case our weights grow to a norm larger than 2 or 0.5, they are projected to weights with a norm of 2 or 0.5. Constraining the weight vectors with max norms is known to work well with dropout <a data-cite="349468/6G2F6VE7"target="_blank" href="http://dl.acm.org/citation.cfm?id=2670313">(Srivastava et al., 2014)</a>.  These values of 2 and 0.5 could certainly be further optimized for the different architectures, but using other hand-picked values usually did not improve accuracies for us.

# ## Early Stopping
# 
# Typically, during training, networks will first learn relationships that mostly generalize from the training data to the test data and later start to overfit and learn relationships only present in the training data. One way to prevent the networks from overfitting and to reduce training time is to use early stopping. We use a procedure inspired by a [successful early stopping procedure for image classification](https://code.google.com/p/cuda-convnet/wiki/Methodology).
# 
# 1. Split the original train fold into a partial train fold, using 90% of the data (as a time-block, i.e., respecting the time-ordering of the trials), and a validation fold, using 10% of the data (i.e., the last 10%).
# 2. Train on the partial train fold until the misclassification rate on the validation fold has not increased for 200 epochs.
# 3. Across all epochs, pick the weights with the best misclassification rate on the validation fold (in case there are several weights with the same rate, use the last network).
# 4. Remember the negative log likelihood $partial\_train_{nll}$ of these best weights on the partial train fold
# 5. Add the validation fold to the training fold restoring the original train fold.
# 6. Train on the original training fold (includes the validation fold) until the negative log likelihood on the validation fold is smaller or equal to $partial\_train_{nll}$ stored before.
# 
# Additionally, we include two hard stopping criteria to prevent overly long training times. We always stop the first phase, before the reintegration of the validation fold, after 1500 epochs. Also, we stop the second phase once we have trained twice as many epochs as it took to reach the best weights in the first phase.
# 
# Without the latter hard stopping criterion, the second phase could run infinitely long, if the validation fold is more difficult, i.e. less separable, than the partial training fold.
# 
# Stopping in the second phase when the negative log likelihood is going below a certain value has another advantage for us: Typically our negative log likelihood values as well as our misclassification rates can vary quite a lot from epoch to epoch. The validation negative log likelihood is still reasonably correlated to the test misclassification rate. Therefore, if we stop when the validation negative log likelihood has just dropped compared to the epoch before, we will often stop at an epoch where the test misclassification rate has also just dropped.
# 
# To illustrate this effect, we show the mean of the test misclassification rates and the validation negative log likelihood values for the last 20 epochs of all our final network experiments. Note that, in the final epoch, the validation negative log likelihood drops together with the test misclass rate.

# In[3]:

from motor_imagery_learning.analysis.results import ResultPool


# In[4]:

raw_result_folder = 'data/models/final-eval/raw-net/early-stop/'
raw_resultpool = ResultPool()
raw_resultpool.load_results(raw_result_folder)


# In[5]:

fb_result_folder = 'data/models/final-eval/csp-net/early-stop/'
fb_resultpool = ResultPool()
fb_resultpool.load_results(fb_result_folder)


# In[6]:

fft_result_folder = 'data/models/final-eval/csp-net/early-stop/'
fft_resultpool = ResultPool()
fft_resultpool.load_results(fft_result_folder)


# In[10]:

all_results = (raw_resultpool.result_objects() + 
               fb_resultpool.result_objects())# + 
               #fft_resultpool.result_objects())

test_misclasses = [res.monitor_channels['test_y_misclass'].val_record[-20:]                   for res in all_results]


valid_nlls = [res.monitor_channels['valid_y_nll'].val_record[-20:]                   for res in all_results]


fig = pyplot.figure(figsize=(8,2))
pyplot.plot(np.mean(test_misclasses, axis=0))
ax1 = fig.axes[0]
ax1.legend(('test_misclass',), loc='upper right', bbox_to_anchor=(1.335,0.7))
ax2 = ax1.twinx()
ax2.plot(np.mean(valid_nlls, axis=0), color=seaborn.color_palette()[1])
ax2.legend(('validation_nll',), loc='upper right', bbox_to_anchor=(1.33,0.55))
pyplot.title('Mean rates over last 20 epochs')
ax1_ticks = np.linspace(ax1.get_ybound()[0], ax1.get_ybound()[1], 5)
ax2_ticks = np.linspace(ax2.get_ybound()[0], ax2.get_ybound()[1], 5)
ax1.set_yticks(ax1_ticks)
ax1.set_yticklabels(["{:.2f}".format(r * 100) for r in ax1_ticks])
ax1.set_ylabel('Misclassification rate on test set (%)')
ax2.set_yticks(ax2_ticks)
ax2.set_yticklabels(["{:.3f}".format(r) for r in ax2_ticks])
ax2.set_ylabel('Neg. log likelihood on validation set')
pyplot.xticks((4,9,14,19), (15,10,5,0))
ax1.set_xlabel('Number of epochs until stop')
None

