from motor_imagery_learning.train_scripts.train_with_params import (
    create_training_object_from_file)
from motor_imagery_learning.analysis.util import create_activations_func
from motor_imagery_learning.analysis.input_reconstruction import (
    create_rand_input) 

import numpy as np
from pylearn2.utils import serial
from pylearn2.utils.logger import (
    CustomStreamHandler, CustomFormatter)
import logging

def get_train_fitted_valid_test(trainer):
    """ make sure to have enough memory so remove dataset X inbetween """
    self = trainer
    this_datasets = self.dataset_splitter.split_into_train_valid_test()
    trainer.dataset_splitter.free_memory_if_reloadable()
    train_valid_set = self.concatenate_sets(this_datasets['train'],
        this_datasets['valid'])
    test_set = this_datasets['test']
    train_set_num_trials = len(this_datasets['train'].y)
    del this_datasets['train']
    if self.preprocessor is not None:
        self.preprocessor.apply(train_valid_set, can_fit=True)
        self.preprocessor.apply(test_set, can_fit=False)
    _, valid_set = self.split_sets(train_valid_set, 
        train_set_num_trials, len(this_datasets['valid'].y))
    # train valid is the new train set!!
    return {'train': train_valid_set, 'valid': valid_set, 
        'test': test_set}

root_logger = logging.getLogger()
prefix = '%(asctime)s '
formatter = CustomFormatter(prefix=prefix, only_from='pylearn2')
handler = CustomStreamHandler(formatter=formatter)
root_logger.addHandler(handler)
root_logger.setLevel(logging.INFO)
#logging.warning('')
log = logging.getLogger(__name__)
log.setLevel('INFO')

log.info("Load dataset...")
bhno_fb_train = create_training_object_from_file(
    'data/models/debug/csp-net/car/chan-freq-standardize-misclass-stop/early-stop/2.yaml')
# bhno_fb_train.dataset_splitter.dataset.bbci_set.sensor_names = ['C3', 'Cz', 'C4']
# bhno_fb_train.dataset_splitter.dataset.min_freq = 0
# bhno_fb_train.dataset_splitter.dataset.low_width = 2
# bhno_fb_train.dataset_splitter.dataset.max_freq = 2
# bhno_fb_train.dataset_splitter.dataset.last_low_freq = 2
bhno_fb_train.dataset_splitter.ensure_dataset_is_loaded()
bhno_sets = get_train_fitted_valid_test(bhno_fb_train)

train_topo = bhno_sets['train'].get_topological_view()
log.info("Compute covariances...")
all_covariances = []
n_chans = train_topo.shape[1]
for i_filt in xrange(train_topo.shape[3]):
    log.info("Filter {:d} of {:d}...".format(i_filt + 1, train_topo.shape[3]))
    filt_topo = train_topo[:,:,:,i_filt]
    covariance = np.cov(filt_topo.transpose(1,0,2).reshape(n_chans, -1))
    all_covariances.append(covariance)
    
all_covariances = np.array(all_covariances)

log.info("Save covariances...")
np.save('all_covariances.npy', all_covariances)

log.info("Check covariances...")
loaded_covariances = np.load('all_covariances.npy')
assert np.allclose(all_covariances, loaded_covariances)

log.info("Compute activations...")
bhno_fb_net = serial.load(
    'data/models/debug/csp-net/car/chan-freq-standardize-misclass-stop/early-stop/2.pkl').model

    

log.info("Create activations function...")
act_func = create_activations_func(bhno_fb_net)

#rand_in = create_rand_input(bhno_fb_net, 3)

log.info("Compute activations...")

acts_out = [act_func([t]) for t in train_topo.astype(np.float32)]
acts_out_0 = np.array([a[0] for a in acts_out])
acts_out_1 = np.array([a[1] for a in acts_out])

# remove empty second "trial" dimension! (trials are in first dimension)
acts_out_0 = acts_out_0[:,0]
acts_out_1 = acts_out_1[:,0]

log.info("Save activations...")
np.save('activations_0.npy', acts_out_0)
np.save('activations_1.npy', acts_out_1)

log.info("Check activations...")
act_loaded_0 = np.load('activations_0.npy')
act_loaded_1 = np.load('activations_1.npy')
assert np.allclose(act_loaded_0, acts_out_0)
assert np.allclose(act_loaded_1, acts_out_1)
