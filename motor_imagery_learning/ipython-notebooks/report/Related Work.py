
# coding: utf-8

# In[1]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nimport os\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n#os.environ[\'CUDA_LAUNCH_BLOCKING\'] = \'1\'\n#os.environ[\'THEANO_FLAGS\'] = os.environ[\'THEANO_FLAGS\'] + ",profile=True"\n# switch to cpu\n#os.environ[\'THEANO_FLAGS\'] = \'floatX=float32,device=cpu,nvcc.fastmath=True\'\n%load_ext autoreload\n%autoreload 2\n\nfrom motor_imagery_learning.bbci_pylearn_dataset import (compute_power_spectra)\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset\nimport numpy as np\n\nimport matplotlib\nfrom matplotlib import pyplot\nfrom matplotlib import cm\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (8.0, 2.0)\nmatplotlib.rcParams[\'font.size\'] = 7\n\nfrom pylearn2.costs.mlp import Default\nfrom pylearn2.models.mlp import Softmax, MLP\nfrom pylearn2.termination_criteria import EpochCounter\nimport seaborn\nseaborn.set_style(\'darkgrid\')\nfrom copy import deepcopy\nfrom motor_imagery_learning.train_scripts.train_experiments import ExperimentsRunner\nfrom motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer\nfrom motor_imagery_learning.analysis.data_generation import (create_sine_signal,\n    pipeline, create_shifted_sines,put_noise, convolve_with_weight,\n    max_pool_topo_view, log_sum_pool)\nfrom motor_imagery_learning.analysis.util import create_trainer\nfrom motor_imagery_learning.mypylearn2.conv_squared import ConvSquared\nfrom motor_imagery_learning.analysis.input_reconstruction import (\n    create_rand_input, gradient_descent_input, create_input_grad_for_wanted_class_func,\n    create_input_grad_with_smooth_penalty_for_wanted_class_func, \n    reconstruct_inputs_for_class)\n\nfrom motor_imagery_learning.analysis.plot_util import (plot_misclasses_for_model, \n    plot_nlls_for_model, plot_sensor_signals)\n\nimport h5py\nimport numpy as np\nfrom pylearn2.utils import serial\nfrom pprint import pprint\nfrom  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted\nimport h5py\nfrom motor_imagery_learning.csp.train_csp import CSPTrain\nfrom motor_imagery_learning.mypylearn2.train import TrainEarlyStop\nfrom motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD\nfrom motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset\nfrom motor_imagery_learning.bbci_dataset import BBCIDataset\nfrom pylearn2.costs.mlp import Default\nfrom pylearn2.models.mlp import Softmax, MLP\nfrom pylearn2.termination_criteria import EpochCounter\nfrom sklearn.cross_validation import KFold\nfrom pylearn2.datasets.dense_design_matrix import DenseDesignMatrix\nimport numpy as np\nfrom numpy.random import RandomState\nimport theano.tensor as T\nimport theano\nfrom pylearn2.space import Conv2DSpace\nfrom pylearn2.models.mlp import ConvElemwise\nfrom theano.sandbox.cuda.dnn import dnn_conv\nfrom theano.sandbox.cuda.basic_ops import gpu_alloc_empty\nfrom motor_imagery_learning.analysis.print_results import ResultPrinter\nfrom pylearn2.utils import serial\nfrom motor_imagery_learning.analysis.data_generation import generate_single_sensor_data\nfrom motor_imagery_learning.analysis.plot.raw_signals import (\n    plot_sensor_signals_two_classes)\nfrom motor_imagery_learning.mypylearn2.pool import PoolLayer, log_sum\nfrom motor_imagery_learning.analysis.util import bps_and_freqs\nfrom motor_imagery_learning.analysis.deconv import deconv_model\nfrom motor_imagery_learning.analysis.plot_util import (\n    plot_head_signals_tight_two_signals, plot_head_signals_tight)\nfrom motor_imagery_learning.csp.print_csp_results import CSPResultPrinter\nfrom motor_imagery_learning.analysis.filterbands import (\n    sum_abs_filterband_weights, compute_center_freqs, compute_sum_abs_weights_for_csp_model)\nfrom motor_imagery_learning.analysis.plot.filterbands import show_freq_importances\nfrom motor_imagery_learning.csp.train_csp import generate_filterbank\nfrom motor_imagery_learning.train_scripts.train_with_params import (\n    create_training_object_from_file)')


# In[2]:

get_ipython().run_cell_magic(u'javascript', u'', u"IPython.load_extensions('cite2c/main');")


# In[3]:

get_ipython().run_cell_magic(u'html', u'', u'<style>\ndiv.input {\nwidth: 105ex; /* about 80 chars + buffer */\n...\n}\ndiv.text_cell {\nwidth: 105ex /* instead of 100%, */\n...\n}\n\ndiv.text_cell_render {\n/*font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;*/\nfont-family: "Charis SIL", serif; /* Make non-code text serif. */\nline-height: 145%; /* added for some line spacing of text. */\nwidth: 105ex; /* instead of \'inherit\' for shorter lines */\n...\n}\n\n/* Set the size of the headers */\ndiv.text_cell_render h1 {\nfont-size: 18pt;\n}\n\ndiv.text_cell_render h2 {\nfont-size: 14pt;\n}\n\n\n\n.CodeMirror {\nfont-family: Consolas, monospace;\n}\n\n.todo {\n    background-color: hsla(120, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n.todecide {\n    background-color: hsla(240, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n\n</style>\n\n<style>\ndiv.summary  {\n    background-color: hsla(40,0%,50%,0.2);\n    color:black;\n    border-radius: 5px;\n    padding: 2px;\n    margin-top:0px;\n}\ndiv.summary ul\n{\n    list-style-type: none;\n}\n</style>\n<link rel="stylesheet" href="./motivation.css" type="text/css">\n<script async defer src="//hypothes.is/embed.js"></script>')


# Most close to us:
# 
# 
# http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=6944253 joint optimization as network
# http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=6889383 convolutional deep belief networks
# http://link.springer.com/chapter/10.1007/978-3-319-09330-7_25 http://www.bio.spbu.ru/faculty/departments/vnd/pdf/journal%202015/moskvich.pdf A Deep Learning Method for Classification of EEG Data Based on Motor Imagery .. a bit weird? confusing?, also deep belief networks 
# 
# http://ieeexplore.ieee.org/xpls/icp.jsp?arnumber=5492691 maybe add, convolutional for P300
# http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=6854950 maybe add, cnns on audio

# # Related Work

# <div class="summary">
# <ul>
# <li>Convolutional networks and deep belief networks have been tried with varying success for EEG decoding</li>
# <li>One published network is similar to our filterbank network </li>
# </ul>
# </div>

# Several attempts to use multi-layer networks to classify EEG signals have been published. We will describe two attempts to decode motor imagery.
# 
# 

# An architecture quite similar to ours was proposed in <a data-cite="349468/XHKKR4ZJ"target="_blank" href="http://ieeexplore.ieee.org/xpls/icp.jsp?arnumber=6944253">(Santana et al., August 2014)</a>. Their architecture consists of layers for:
# 
# 1. Temporal filtering, i.e., bandpass filtering
# 2. Spatial filtering
# 3. Quadratic non-linearity
# 4. Temporal projection, i.e., learning a timecourse
# 5. Logistic non-linearity
# 6. Softmax for classification
# 
# The architecture is summarized in this picture:
# <img height="100px" style="height:300px;" src="http://ieeexplore.ieee.org/ielx7/6923026/6943513/6944253/html/img/6944253-fig-1-large.gif"></img>
# <a data-cite="349468/XHKKR4ZJ"target="_blank" href="http://ieeexplore.ieee.org/xpls/icp.jsp?arnumber=6944253">(Santana et al., August 2014)</a> ©2014 IEEE
# 
# They tried to overcome the problem of overfitting on small datasets by fixing and intelligently initializing some weights of the network. They fixed the the first layer to be an IIR bandpass filter between 8 and 30 Hz (note, this means it is no longer a normal convolutional layer since an IIR filter works recursively). The spatial filters of the second layer were initialized by spatial filters computed with CSP. So their method learned:
# 
# * a finetuning of the CSP spatial filters
# * a timecourse (temporal projection) over the trial
# * softmax weights for each spatial filter
# 
# They show better results than CSP for BCI Competition II, Dataset IV (10% error vs 16% error) and BCI Competition III, Dataset IV-A (17% vs 27%) (<span class="todo"> references to competition papers </span>. They did not evaluate on the newest dataset, BCI Competition IV.
# 
# Our filterbank network also uses fixed bandpass filters and quadratic non-linearities. The difference of our filterbank network to their networks are:
# 
# * we use several bandpass filters and not just one
# * we do not pre-initialize the spatial filters
# * we do temporal pooling: logarithm of the sum of a timeinterval 
# * we do not use a logistic function anywhere 
# * we let the softmax learn a timecourse (over the timepooled intervals) and the spatial filter weights  (no extra temporal projection layer before)
# 
# 
# 
# 

# 
