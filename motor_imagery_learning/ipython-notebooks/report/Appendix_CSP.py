
# coding: utf-8

# In[1]:

get_ipython().run_cell_magic(u'javascript', u'', u"IPython.load_extensions('cite2c/main');")


# In[1]:

get_ipython().run_cell_magic(u'html', u'', u'<style>\ndiv.input {\nwidth: 105ex; /* about 80 chars + buffer */\n...\n}\ndiv.text_cell {\nwidth: 105ex /* instead of 100%, */\n...\n}\n\ndiv.text_cell_render {\n/*font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;*/\nfont-family: "Charis SIL", serif; /* Make non-code text serif. */\nline-height: 145%; /* added for some line spacing of text. */\nwidth: 105ex; /* instead of \'inherit\' for shorter lines */\n...\n}\n\n/* Set the size of the headers */\ndiv.text_cell_render h1 {\nfont-size: 18pt;\n}\n\ndiv.text_cell_render h2 {\nfont-size: 14pt;\n}\n\n\n\n.CodeMirror {\nfont-family: Consolas, monospace;\n}\n\n.todo {\n    background-color: hsla(120, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\nli.todo {\n    background-color: hsla(120, 40%, 50%, 0.1);\n    border-radius: 0px;\n    padding: 0px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n\n.todecide {\n    background-color: hsla(240, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n\n</style>\n\n<style>\ndiv.summary  {\n    background-color: hsla(40,0%,50%,0.2);\n    color:black;\n    border-radius: 5px;\n    padding: 2px;\n    margin-top:0px;\n}\ndiv.summary ul\n{\n    list-style-type: none;\n}\n</style>\n<link rel="stylesheet" href="./motivation.css" type="text/css">\n<script async defer src="//hypothes.is/embed.js"></script>')


# In[1]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nimport os\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n#os.environ[\'CUDA_LAUNCH_BLOCKING\'] = \'1\'\n#os.environ[\'THEANO_FLAGS\'] = os.environ[\'THEANO_FLAGS\'] + ",profile=True"\n# switch to cpu\n#os.environ[\'THEANO_FLAGS\'] = \'floatX=float32,device=cpu,nvcc.fastmath=True\'\n%load_ext autoreload\n%autoreload 2\n\nfrom motor_imagery_learning.bbci_pylearn_dataset import compute_power_spectra\nfrom motor_imagery_learning.mywyrm.clean import BBCISetNoCleaner\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset\nimport numpy as np\n\nimport matplotlib\nfrom matplotlib import pyplot\nfrom matplotlib import cm\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (12.0, 5.0)\nmatplotlib.rcParams[\'font.size\'] = 7\n\nimport matplotlib.lines as mlines\nimport seaborn\nseaborn.set_style(\'darkgrid\')')


# In[2]:

from motor_imagery_learning.mywyrm.processing import resample_cnt, highpass_cnt, common_average_reference_cnt
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset
from motor_imagery_learning.mypylearn2.preprocessing import RestrictToTwoClasses
from wyrm.processing import apply_csp
from motor_imagery_learning.analysis.plot_util import plot_sensor_signals
from motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
from mywyrm.plot import ax_scalp,get_channelpos, CHANNEL_10_20_exact, CHANNEL_10_20
import matplotlib.cm as cmap

def get_gujo_data():
    gujo_epo = get_gujo_epo()
    restricted_sensor_names = ['Cp1', 'Cz', 'Cp2']
    few_sensor_epo = select_channels(gujo_epo, restricted_sensor_names, chanaxis=-1)
    class_pair = [0,1]#[0,1]
    two_class_epo = select_classes(few_sensor_epo, class_pair) 
    timeval_epo = select_ival(two_class_epo, [500,4000])    
    n_trials =  timeval_epo.data.shape[0]
    train_epo = select_epochs(timeval_epo, range(0, int(np.floor(n_trials * 0.9))))
    test_epo = select_epochs(timeval_epo, range(int(np.floor(n_trials * 0.9)), n_trials))
    return two_class_epo, train_epo, test_epo
    
def get_gujo_epo():
    filename = 'data/BBCI-without-last-runs/GuJoMoSc01S001R01_ds10_1-11.BBCI.mat'

    filterbank_args = dict(min_freq=11, max_freq=11, last_low_freq=11, low_width=4, high_width=4)
    sensor_names = get_C_sensors_sorted()

    dataset = BBCIPylearnCleanFilterbankDataset(filenames=filename,
           load_sensor_names=sensor_names, cleaner=BBCISetNoCleaner(),
          cnt_preprocessors=((resample_cnt, dict(newfs= 300)), 
                             (highpass_cnt, dict(low_cut_off_hz=0.5)),
                            (common_average_reference_cnt, dict())),
                                               **filterbank_args)

    dataset.load_bbci_set()
    epo = dataset.bbci_set.epo
    epo.data = np.squeeze(epo.data)
    epo.class_names = ['Right Hand', 'Left Hand', 'Rest', 'Feet']
    epo.axes = epo.axes[0:3] # remove filterband axis
    return epo


from wyrm.processing import select_classes, select_epochs, calculate_csp, select_channels, select_ival


def get_csp_filters_train_test(train_epo):
    filters, patterns, variances = calculate_csp(train_epo)
    return filters

def get_normal_and_csp_filtered_data(two_class_epo, filters):
    columns =[0,-1]
    n_trials =  two_class_epo.data.shape[0]
    test_epo_full = select_epochs(two_class_epo, range(int(np.floor(n_trials * 0.9)), n_trials))
    csp_filtered_full = apply_csp(test_epo_full, filters, columns)
    return test_epo_full, csp_filtered_full
def plot_combined_example(test_epo_full, csp_filtered_full):
    combined_sensor_trials = np.concatenate((test_epo_full.data[36], test_epo_full.data[37]))
    combined_csp_trials = np.concatenate((csp_filtered_full.data[36], csp_filtered_full.data[37]))
    combined_both = np.concatenate((combined_sensor_trials, combined_csp_trials), axis=1)
    plot_sensor_names =  test_epo_full.axes[2].tolist() + ['CSP1', 'CSP2']
    fig = plot_sensor_signals(combined_both.T, sensor_names=plot_sensor_names, figsize=(8,4))
    for ax in fig.axes:
        ax.axvline(x=1200, color='black', lw=1.25)

    pyplot.plot([0.125, 0.9], [0.43,0.43], color='grey',
        lw=1,transform=fig.transFigure, clip_on=False, linestyle="-")
    pyplot.text(0.25,1.2,"Left Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
    pyplot.text(0.75,1.2,"Right Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
    pyplot.xticks(np.arange(0,2401,300), 
          [0,1000,2000,3000] + ['4000/0'] + [1000,2000,3000,4000])
    pyplot.xlabel("milliseconds")
    fig.axes[1].axvspan(300, 1200, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
    fig.axes[2].axvspan(300, 1200, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
    fig.axes[3].axvspan(300, 1200, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
    fig.axes[4].axvspan(300, 1200, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
    fig.axes[1].axvspan(1500, 2400, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
    fig.axes[2].axvspan(1500, 2400, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
    fig.axes[3].axvspan(1500, 2400, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
    fig.axes[4].axvspan(1500, 2400, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
    
def plot_csp_example(csp_filtered_full):
    combined_csp_trials = np.concatenate((csp_filtered_full.data[36], 
                                          csp_filtered_full.data[37]))
    plot_sensor_names =  ['CSP1', 'CSP2']
    fig = plot_sensor_signals(combined_csp_trials.T, sensor_names=plot_sensor_names,
                              figsize=(8,2))
    for ax in fig.axes:
        ax.axvline(x=1200, color='black', lw=1.25)

    pyplot.text(0.25,1.2,"Left Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
    pyplot.text(0.75,1.2,"Right Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
    pyplot.xticks(np.arange(0,2401,300), 
          [0,1000,2000,3000] + ['4000/0'] + [1000,2000,3000,4000])
    
    pyplot.xlabel("milliseconds")
    
def plot_sensor_example(test_epo_full):
    combined_sensor_trials = np.concatenate((test_epo_full.data[36], 
                                             test_epo_full.data[37]))
    plot_sensor_names =  test_epo_full.axes[2].tolist()
    fig = plot_sensor_signals(combined_sensor_trials.T, sensor_names=plot_sensor_names, 
                              figsize=(8,2.5))
    for ax in fig.axes:
        ax.axvline(x=1200, color='black', lw=1.25)

    pyplot.text(0.25,1.2,"Left Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
    pyplot.text(0.75,1.2,"Right Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
    pyplot.xticks(np.arange(0,2401,300), 
          [0,1000,2000,3000] + ['4000/0'] + [1000,2000,3000,4000])
    pyplot.xlabel("milliseconds")
    
    # highlight decreases and increases
    fig.axes[1].axvspan(300, 1200, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
    fig.axes[2].axvspan(300, 1200, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
    fig.axes[1].axvspan(1500, 2400, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
    fig.axes[2].axvspan(1500, 2400, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
    return None
    
def plot_csp_filters(filters):
    chan_pos_list = CHANNEL_10_20
    figsize=(6,4)
    fig, axes = pyplot.subplots(1,2,figsize=figsize)
    ax_scalp_example(filters[:, 0], test_epo_full.axes[2], ax=axes[0], 
             colormap=cmap.coolwarm, annotate=True, chan_pos_list=chan_pos_list)
    ax_scalp_example(filters[:, -1], test_epo_full.axes[2], ax=axes[1], 
             colormap=cmap.coolwarm, annotate=True, chan_pos_list=chan_pos_list)


from scipy import interpolate
def ax_scalp_example(v, channels, 
    ax=None, annotate=False, vmin=None, vmax=None, colormap=None,
    chan_pos_list=CHANNEL_10_20, fontsize=12):
    """ scalp plot just for example.. wihtout scalp :D
    """
    if ax is None:
        ax = plt.gca()
    assert len(v) == len(channels), "Should be as many values as channels"
    if vmin is None:
        # added by me (robintibor@gmail.com)
        assert vmax is None
        vmin, vmax = -np.max(np.abs(v)), np.max(np.abs(v))
    # what if we have an unknown channel?
    points = [get_channelpos(c, chan_pos_list) for c in channels]
    for c in channels:
        assert get_channelpos(c, chan_pos_list) is not None, ("Expect " + c + " "
            "to exist in positions")
    z = [v[i] for i in range(len(points))]
    # calculate the interpolation
    x = [i[0] for i in points]
    y = [i[1] for i in points]
    # interplolate the in-between values
    xx = np.linspace(min(x), max(x), 500)
    yy = np.linspace(min(y), max(y), 500)
    xx, yy = np.meshgrid(xx, yy)
    f = interpolate.LinearNDInterpolator(list(zip(x, y)), z)
    zz = f(xx, yy)
    # draw the contour map
    ax.contourf(xx, yy, zz, 20, vmin=vmin, vmax=vmax, cmap=colormap)
    ax.set_frame_on(False)
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    # draw the channel names
    if annotate:
        for i in zip(channels, list(zip(x, y))):
            channel = i[0]
            this_x, this_y = i[1]
            if channel == 'CP1':
                horizontalalignment="right"
            elif channel == 'CP2':
                horizontalalignment="left"
            elif channel == 'Cz':
                horizontalalignment="center"
            else:
                assert False
            ax.annotate(" " + channel, (this_x, this_y),
                        horizontalalignment=horizontalalignment,
                       fontsize=fontsize)
    ax.set_aspect(1)
    
    pyplot.subplots_adjust(wspace=0.5)
    return ax
    
from motor_imagery_learning.analysis.data_generation import randomly_shifted_sines
import scipy
from numpy.random import RandomState

def get_noisy_sine_signal():
    rng = RandomState(np.uint64(hash('noise_for_csp_2')))

    sine_signal = np.sin(np.linspace(0, 2*np.pi, 100))
    #class_1_signal = np.outer([0.4,0.6], sine_signal) + rng.randn(100) * 0.1
    #class_2_signal = np.outer([0.6,0.4], sine_signal) + rng.randn(100) * 0.1
    class_1_signal = np.outer([0.6,0.8], sine_signal) + rng.randn(100) * 0.1
    class_2_signal = np.outer([0.8,0.6], sine_signal) + rng.randn(100) * 0.1

    class_1_signal = class_1_signal - np.mean(class_1_signal, axis=1, keepdims=True)
    class_2_signal = class_2_signal - np.mean(class_2_signal, axis=1, keepdims=True)
    #class_1_signal = class_1_signal / np.std(class_1_signal, axis=1, keepdims=True)
    #class_2_signal = class_2_signal / np.std(class_2_signal, axis=1, keepdims=True)
    return class_1_signal, class_2_signal

def compute_csp(class_1_signal, class_2_signal):
    #pyplot.plot(sine_signal)
    c1 = np.cov(class_1_signal)
    c2 = np.cov(class_2_signal)
    #d, v = scipy.linalg.eig(c1-c2, c1+c2)
    #d, v = scipy.linalg.eig(c1-c2, c1 +c2)
    d, v = scipy.linalg.eig(c1, c2)
    d = d.real
    # make sure the eigenvalues and -vectors are correctly sorted
    indx = np.argsort(d)
    # reverse
    indx = indx[::-1]

    d = d.take(indx)
    v = v.take(indx, axis=1)
    a = scipy.linalg.inv(v).transpose()
    
    csp_filtered_class_1 = np.dot(class_1_signal.T, v)
    csp_filtered_class_2 = np.dot(class_2_signal.T, v)
    
    return v, d, csp_filtered_class_1, csp_filtered_class_2

def plot_csp_computation_example(class_1_signal, class_2_signal, eigenvectors, 
                             csp_filtered_class_1, csp_filtered_class_2):
    pyplot.figure(figsize=(2,2))
    pyplot.scatter(class_1_signal[0], class_1_signal[1], color=seaborn.color_palette()[2])
    pyplot.scatter(class_2_signal[0], class_2_signal[1], color=seaborn.color_palette()[3])
    pyplot.xlim(-1.2, 1.2)
    pyplot.ylim(-1.2, 1.2)
    pyplot.xticks((-1.0,-0.5,0,1.0,0.5))
    pyplot.xlabel('Sensor 1')
    pyplot.ylabel('Sensor 2')
    pyplot.legend(('Class 1', 'Class 2'),loc='right', bbox_to_anchor=(1.7, 0.55))
    pyplot.plot([0,eigenvectors[0,0] * 0.5], [0,eigenvectors[1,0] * 0.5], color=seaborn.color_palette()[2])
    pyplot.plot([0,eigenvectors[0,1] * 0.5], [0,eigenvectors[1,1] * 0.5], color=seaborn.color_palette()[3] )
    pyplot.figure(figsize=(2,2))

    pyplot.scatter(csp_filtered_class_1[:,0], csp_filtered_class_1[:,1], color=seaborn.color_palette()[2])
    pyplot.scatter(csp_filtered_class_2[:,0], csp_filtered_class_2[:,1], color=seaborn.color_palette()[3])
    pyplot.xlim(-0.4, 0.4)
    pyplot.ylim(-0.4, 0.4)
    pyplot.xticks((-0.4,-0.2, 0,0.2,0.4))
    pyplot.xlabel('CSP 1')
    pyplot.ylabel('CSP 2')
    pyplot.legend(('Class 1', 'Class 2'),loc='right', bbox_to_anchor=(1.7, 0.55))


# #  Common Spatial Patterns

# ## Mathematical derivation

# <div class="summary"><ul>
# <li> Common spatial patterns seeks to optimize ratio of variances </li>
# <li> Generalized eigenvalue decomposition can be used to compute spatial filters </li>
# </ul></div>

# We explain one way to compute spatial filters with the common spatial patterns algorithm, and why this is mathematically sound, mostly following <a data-cite="349468/D6IVJ2G2"target="_blank" href="undefined">(Blankertz et al., 2008)</a>.
# 
# We assume we have multivariate signals $X_1$ and $X_2$ for two classes 1 and 2. For EEG decoding, the multivariate signals would be signals from several sensors and the classes could be left hand and right hand.
# We want to find a spatial filter $w$ that maximizes the ratio of the variances:
# 
# $$w = argmax_w \frac{variance(w^{\top} X_1)}{variance(w^{\top} X_2)} = argmax_w \frac{||w^{\top} X_1||^2}{||w^{\top} X_2||^2} = 
# argmax_w \frac{w^{\top} X_1 X_1^{\top}w}{w^{\top} X_2 X_2^{\top}w}$$ (we assume $X_1$ and $X_2$ are mean-subtracted)
# 
# We compute covariance matrices $\Sigma_{1}$ and $\Sigma_{2}$ for classes 1 and 2 by averaging over the covariances for all trials for each class and rewrite our formula as:
# $$w = argmax_w \frac{w^{\top} \Sigma_{1} w}{w^{\top} \Sigma_{2}w}$$

# We put a further restriction on the possible $w$s by setting $w^{\top} \Sigma_2w = 1$. This should not affect the result $w$ as this term is only the denominator of our ratio.
# 
# Now we can solve for w using this generalized eigenvalue problem:
# 
# $$\Sigma_1w = \lambda \Sigma_2 w $$
# 
# This will not give us a single spatial filter w, but a matrix of spatial filters W, the generalized eigenvectors. However, as we will see, this matrix also contains the desired spatial filter w that maximizes our variance ratio. 
# The variance ratios of class 1 to class 2 for the signals filtered with the (generalized) eigenvector $w_i$ will be equal to the corresponding eigenvalues of the eigenvectors:
# 
# $$\frac{w_i^{\top} \Sigma_1 w_i}{w_i^{\top} \Sigma_2 w_i} = \lambda_i$$
# 
# The vectors with the highest eigenvalues have a high variance for class 1 compared to class 2 and the vectors with the lowest eigenvalues have a low variance for class 1 compared to class 2. The spatial filter maximizing this ratio is then the $w_i$ with the largest eigenvalue. Conversely, the spatial filter minimizing this ratio is the $w_i$ with the smallest eigenvalue.
# 
# Furthermore, all the eigenvectors applied to our data lead to uncorrelated signals, i.e.:
# 
# $$W^{\top}\Sigma_1 W=\Lambda_1 \\
# W^{\top}\Sigma_2 W=\Lambda_2$$
# 
# and $$(\Lambda_1 \text{ , } \Lambda_2 \text{ diagonal })$$
# 
# 
# <span class="todecide"> put proof somewhere or atleast link to proof when you say the spatial filter maximizing this ratio is then...., using http://www.sciencedirect.com/science/article/pii/001346949190163X http://csl.anthropomatik.kit.edu/downloads/vorlesungsinhalte/MBS12_CommonSpatialPatterns.pdf http://cdn.preterhuman.net/texts/science_and_technology/artificial_intelligence/Pattern_recognition/Introduction%20to%20Statistical%20Pattern%20Recognition%202nd%20Ed%20-%20%20Keinosuke%20Fukunaga.pdf</span>
# 

# ## Computation Example

# We illustrate the computation with an artificial example. We use one source signal, a sine signal $x$ plus some gaussian noise. It is projected to two sensors, differently for each class. For class 1, the signal is: $Sensor_1 = 0.6 x$, $Sensor_2 =0.8x$. For class 2, $Sensor_1 = 0.8 x$, $Sensor_2 =0.6x$. We show the mean-subtracted signals. You can see that the sensor 2 has a slightly larger variance than sensor 1 for class 1 and vice versa for class 2.

# In[289]:

class_1_signal, class_2_signal = get_noisy_sine_signal()
pyplot.figure(figsize=(6,1))
pyplot.plot(class_1_signal.T)
pyplot.legend(('Sensor 1','Sensor 2'), loc='right', bbox_to_anchor=(1.25, 0.55))
pyplot.title("Class 1")
pyplot.figure(figsize=(6,1))
pyplot.plot(class_2_signal.T)
pyplot.legend(('Sensor 1','Sensor 2'), loc='right', bbox_to_anchor=(1.25, 0.55))
_ = pyplot.title("Class 2")


# Now we visualize how CSP spatially filters the sensor signals to make them more separable by their variance. In the first plot, you see a scatterplot of all signal values for class 1 (red) and class 2 (violet). Each dot represents one sample of the signal, and the axes show the value of sensor 1 and the value of sensor 2 for that sample. You again see that sensor 2 has a larger variance than sensor 1 for class 1, since the red dots are more spread in y-direction than in x-direction.
# The spatial filters computed by CSP are shown as two lines in red and violet. The red spatial filter maximizes the variance for class 1 and the violet one for class 2. Note, how the spatial filters are almost orthogonal to the direction where the *opposite* class has the maximum variance. For example, the red spatial filter for class 1 is almost orthogonal to the direction of maximum variance for the violet dots of class 2.
# In the second plot, we show the same scatterplots for the signal values after they are filtered with the CSP-filters. Note that the two axes for the two CSP filters correspond to the directions of the largest variance for the corresponding class.

# In[297]:

eigenvectors, eigenvalues, csp_filtered_class_1, csp_filtered_class_2 = compute_csp(
    class_1_signal, class_2_signal)
plot_csp_computation_example(class_1_signal, class_2_signal, eigenvectors, 
                             csp_filtered_class_1, csp_filtered_class_2)


# You again see the effect of the CSP filters in the plots of the CSP-filtered signals. For class 1, CSP1 has a large variance, whereas for class 2, CSP2 has a large variance.

# In[291]:

pyplot.figure(figsize=(6,1))
pyplot.plot(csp_filtered_class_1)
pyplot.legend(('CSP 1','CSP 2'), loc='right', bbox_to_anchor=(1.25, 0.55))
pyplot.title("Class 1")
pyplot.figure(figsize=(6,1))
pyplot.plot(csp_filtered_class_2)
pyplot.legend(('CSP 1','CSP 2'), loc='right', bbox_to_anchor=(1.25, 0.55))
pyplot.title("Class 2")


# In[302]:

scipy.linalg.inv(eigenvectors).T # => patterns
w1 = [ 1.76998635, 2.32757113],
w2 = [-2.32757113 , -1.74578534]

w1 / np.sqrt(np.sum(np.square(w1)))
w2 / np.sqrt(np.sum(np.square(w2)))
None

