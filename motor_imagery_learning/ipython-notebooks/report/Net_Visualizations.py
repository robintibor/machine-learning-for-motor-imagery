
# coding: utf-8

# In[10]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nimport os\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n#os.environ[\'CUDA_LAUNCH_BLOCKING\'] = \'1\'\n#os.environ[\'THEANO_FLAGS\'] = os.environ[\'THEANO_FLAGS\'] + ",profile=True"\n# switch to cpu\n#os.environ[\'THEANO_FLAGS\'] = \'floatX=float32,device=cpu,nvcc.fastmath=True\'\n%load_ext autoreload\n%autoreload 2\n\nfrom motor_imagery_learning.bbci_pylearn_dataset import (compute_power_spectra)\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset\nimport numpy as np\n\nimport matplotlib\nfrom matplotlib import pyplot\nfrom matplotlib import cm\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (8.0, 2.0)\nmatplotlib.rcParams[\'font.size\'] = 7\n\nfrom pylearn2.costs.mlp import Default\nfrom pylearn2.models.mlp import Softmax, MLP\nfrom pylearn2.termination_criteria import EpochCounter\nimport seaborn\nseaborn.set_style(\'darkgrid\')\nfrom copy import deepcopy\nfrom motor_imagery_learning.train_scripts.train_experiments import ExperimentsRunner\nfrom motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer\nfrom motor_imagery_learning.analysis.data_generation import (create_sine_signal,\n    pipeline, create_shifted_sines,put_noise, convolve_with_weight,\n    max_pool_topo_view, log_sum_pool)\nfrom motor_imagery_learning.analysis.util import create_trainer\nfrom motor_imagery_learning.mypylearn2.conv_squared import ConvSquared\nfrom motor_imagery_learning.analysis.input_reconstruction import (\n    create_rand_input, gradient_descent_input, create_input_grad_for_wanted_class_func,\n    create_input_grad_with_smooth_penalty_for_wanted_class_func, \n    reconstruct_inputs_for_class)\n\nfrom motor_imagery_learning.analysis.plot_util import (plot_misclasses_for_model, \n    plot_nlls_for_model, plot_sensor_signals)\n\nimport h5py\nimport numpy as np\nfrom pylearn2.utils import serial\nfrom pprint import pprint\nfrom  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted\nimport h5py\nfrom motor_imagery_learning.csp.train_csp import CSPTrain\nfrom motor_imagery_learning.mypylearn2.train import TrainEarlyStop\nfrom motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD\nfrom motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset\nfrom motor_imagery_learning.bbci_dataset import BBCIDataset\nfrom pylearn2.costs.mlp import Default\nfrom pylearn2.models.mlp import Softmax, MLP\nfrom pylearn2.termination_criteria import EpochCounter\nfrom sklearn.cross_validation import KFold\nfrom pylearn2.datasets.dense_design_matrix import DenseDesignMatrix\nimport numpy as np\nfrom numpy.random import RandomState\nimport theano.tensor as T\nimport theano\nfrom pylearn2.space import Conv2DSpace\nfrom pylearn2.models.mlp import ConvElemwise\nfrom theano.sandbox.cuda.dnn import dnn_conv\nfrom theano.sandbox.cuda.basic_ops import gpu_alloc_empty\nfrom motor_imagery_learning.analysis.print_results import ResultPrinter\nfrom pylearn2.utils import serial\nfrom motor_imagery_learning.analysis.data_generation import generate_single_sensor_data\nfrom motor_imagery_learning.analysis.plot.raw_signals import (\n    plot_sensor_signals_two_classes)\nfrom motor_imagery_learning.mypylearn2.pool import PoolLayer, log_sum\nfrom motor_imagery_learning.analysis.util import bps_and_freqs\nfrom motor_imagery_learning.analysis.deconv import deconv_model\nfrom motor_imagery_learning.analysis.plot_util import plot_head_signals_tight_two_signals')


# In[2]:

get_ipython().run_cell_magic(u'javascript', u'', u"IPython.load_extensions('cite2c/main');")


# In[3]:

get_ipython().run_cell_magic(u'html', u'', u'<style>\ndiv.input {\nwidth: 105ex; /* about 80 chars + buffer */\n...\n}\ndiv.text_cell {\nwidth: 105ex /* instead of 100%, */\n...\n}\n\ndiv.text_cell_render {\n/*font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;*/\nfont-family: "Charis SIL", serif; /* Make non-code text serif. */\nline-height: 145%; /* added for some line spacing of text. */\nwidth: 105ex; /* instead of \'inherit\' for shorter lines */\n...\n}\n\n/* Set the size of the headers */\ndiv.text_cell_render h1 {\nfont-size: 18pt;\n}\n\ndiv.text_cell_render h2 {\nfont-size: 14pt;\n}\n\n\n\n.CodeMirror {\nfont-family: Consolas, monospace;\n}\n\n.todo {\n    background-color: hsla(120, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n.todecide {\n    background-color: hsla(240, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n\n</style>\n\n<style>\ndiv.summary  {\n    background-color: hsla(40,0%,50%,0.2);\n    color:black;\n    border-radius: 5px;\n    padding: 2px;\n    margin-top:0px;\n}\ndiv.summary ul\n{\n    list-style-type: none;\n}\n</style>\n<link rel="stylesheet" href="./motivation.css" type="text/css">\n<script async defer src="//hypothes.is/embed.js"></script>')


# In[4]:

def get_reconstructions_artificial_data():
    model = get_artificial_model()
    in_grad_func = create_input_grad_for_wanted_class_func(model)
    batches = 3
    start_in = create_rand_input(model, batches)
    ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000)
    inputs_class_1 = gradient_descent_input(start_in, in_grad_func, [1,0], **ascent_args)
    inputs_class_2 = gradient_descent_input(start_in, in_grad_func, [0,1], **ascent_args)
    return inputs_class_1, inputs_class_2

def get_reconstructions_kaus_data():
    kaus_topo, kaus_y = generate_single_sensor_data(newfs=150)
    # convolve + square + maxpool
    layers = [ConvSquared(output_channels=1, kernel_shape=[150,1], pool_shape=[50,1], pool_stride=[50,1],
                         pool_type="sumlog",layer_name="conv_squared", irange=0.05, max_kernel_norm=2.0, tied_b=True),
              Softmax(n_classes=2, layer_name='y', irange=0.001, max_col_norm=0.5)]
    trainer = create_trainer(kaus_topo, kaus_y,max_epochs=100, layers=layers, cost=None)
    
    trainer.run_until_earlystop()
    in_grad_func = create_input_grad_for_wanted_class_func(trainer.model)
    batches = 3
    start_in = create_rand_input(trainer.model, batches)
    ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000)
    inputs_class_1 = gradient_descent_input(start_in, in_grad_func, [1,0], **ascent_args)
    inputs_class_2 = gradient_descent_input(start_in, in_grad_func, [0,1], **ascent_args)
    return inputs_class_1, inputs_class_2
    
def get_artificial_model():
    input_shape=(600,1,600,1)
    start_topo_view, y = create_shifted_sines(input_shape, RandomState(np.uint64(hash("pooled_pipeline"))), 
                                              sampling_freq=150.0)
    topo_view_noised, y = pipeline(start_topo_view, y,
                             lambda x,y: put_noise(x, weight_old_data=0.05))
    # convolve + square + maxpool
    layers = [ConvSquared(output_channels=1, kernel_shape=[150,1], pool_shape=[-1,1], pool_stride=[-1,1],
                         pool_type=None,layer_name="conv_squared", irange=0.05, max_kernel_norm=2.0, tied_b=True),
              PoolLayer(pool_func=log_sum, layer_name="pool_squared", pool_shape=[50,1], pool_stride=[50,1]),
              Softmax(n_classes=2, layer_name='y', irange=0.001, max_col_norm=0.5)]
    trainer = create_trainer(topo_view_noised, y,max_epochs=100, layers=layers, cost=None)

    trainer.run_until_earlystop()
    return trainer.model
    


# # Visualizations Methods

# <div class="summary">
# <ul>
# <li>It can be difficult to show what a trained model has learned</li>
# <li>Interpreting weights has to be done carefully</li>
# <li>Visualizations can give clues what a model might have learned</li>
# </ul>
# </div>

# Once you have trained a machine learning model, you are often interested to know what it has learned. In our case, we are interested to know how our model uses the signal:
# 
# * How is the model weighing the sensors?
# * What frequencies is it using for which class? 
# * Or in the case of the raw net, is it using only oscillations or something else? 
# 
# Also, we want to know what we can learn from the model about the brain and motor imagery. For example, can the sensor weights show us where in the brain the important signals are generated?

# Knowing what the model has learned and what we can learn from it is usually not straightforward. 
# Consider a simple case with two sensors S1 and S2 and two classes Right and Left. A large value for sensor S1 indicates class Right and a small value indicates class Left, while sensor S2 always has the value zero. Additionally, the same noise is present on both sensors.
# Assume, we train a model trained to give a high output for class Right and a low output for class Left. It should have a  positive weight for S1, since a large value for S1 indicates class Right. However, it will likely also have a negative weight on S2 to remove the common noise. This could then be misinterpreted as a low value for sensor S2 indicating class Left.
# So, the weights in a model trained only to discriminate between classes typically reflect both the relationship between the weighted features and the output as well as the noise structure of the features. We will explain how to overcome this problem for CSP and visualize illustrative information about our spatial weights in <span class="todo"> CSP-Visualize-section</span>. For a longer discussion about interpreting weights of machine learning models in the context of EEG, see <a data-cite="349468/D6IVJ2G2" target="_blank" href="http://ieeexplore.ieee.org/xpls/icp.jsp?arnumber=4408441">(Blankertz et al., 2008)</a>, <a data-cite="349468/SI5ABXCA"target="_blank" href="http://www.sciencedirect.com/science/article/pii/S1053811910009067">(Blankertz et al., May 15, 2011)</a> and <a data-cite="349468/DHKZP7IQ"target="_blank" href="http://www.sciencedirect.com/science/article/pii/S1053811913010914">(Haufe et al., February 15, 2014)</a>. <span class="todecide"> and <a data-cite="349468/938T52UH"target="_blank" href="http://www.sciencedirect.com/science/article/pii/S105381191500052X">(Weichwald et al., April 15, 2015)</a>?</span>

# ## Visualizing Networks

# <div class="summary">
# <ul>
# <li>It is difficult to show what exactly a network has learned</li>
# <li>You can try to visualize the network directly or show its reaction to different inputs</li>
# </ul>
# </div>

# Understanding what a deep neural network has learned can be difficult. A network can have a large number of parameters and parameters of one unit interact with parameters of other units from the same layer and from other layers. 
# 
# To show what the network has learned, one can either only use only the network itself (*network-centric*) or use the network together with input data (*dataset-centric*, e.g., to highlight what parts of the input the network focusses on) <a data-cite="349468/XH48TN6Q"target="_blank" href="http://arxiv.org/abs/1506.06579">(Yosinski et al., 2015-06-22)</a>. For our networks, we will show several things to get an idea how the network classifies our data. We will show the weights of the network and activations of the network for different inputs. We will also explain what the weights and activations might mean in our context. Finally, we will reconstruct the desired input of a using the gradient of the input.

# We will use a running examples to illustrate the techniques:
# A raw net trained on data from two sensors, CP3 and CP4 for the classes right hand and left hand.

# ### Weights

# <div class="summary">
# <ul>
# <li>Weights in different layers give different insights</li>
# <li>For example, weights of raw net first layer might show what frequencies net is sensitive to</li>
# </ul>
# </div>

# The weights in the first layer can show us what kind of signal shapes or frequencies the network is sensitive to. We show them here for our right hand vs left hand network. We would expect that the temporal filters match some frequency around 9-13 Hz.

# In[326]:

bhno_right_left_model = serial.load(
    'data/models/two-class/raw-square/increasing-filters/test/36.pkl').model
temporal_weights = bhno_right_left_model.get_weights_topo()[:,::-1,::-1,:]
fig = plot_sensor_signals(temporal_weights.squeeze(), 
                          ['Filter 1', 'Filter 2'], figsize=(5,2.5))
fig.suptitle('Temporal Filters', fontsize=16, y=1.02)
pyplot.xticks(range(0,31,5), map(lambda ms: "{:.1f}".format(ms),
                                 np.arange(0,31,5) * 1000 / 150.0))
pyplot.xlabel("milliseconds")
None


# We see some sine-like shapes, especially well visible in filter 2. Now we might want to know what frequencies this net is sensitive to. To find out, we can transform the weights to the frequency domain using a fast fourier transformation. Plotting the amplitudes, we can see that the weights show a peak at 10 Hz.

# In[327]:

bps, freqs = bps_and_freqs(temporal_weights.squeeze())

fig = plot_sensor_signals(bps, ['Filter 1', 'Filter 2'], figsize=(5,2.5))
fig.suptitle('Temporal Filters Frequency Amplitudes', fontsize=16, y=1.02)

pyplot.xticks(range(len(freqs)), map(lambda freq: "{:.0f}".format(freq), freqs))
pyplot.xlabel('Hz')
None


# Now we can look how the output of the first layer is weighted in the second layer. Blue indicates a negative weight, red a positive weight, white corresponds to zero. All colormaps are on the same scale.
# 
# You can nicely see the lateralization for filter 2 by looking at the strength of the color, typically it will be mich higher for one sensor than for the other one, i.e., higher for C3 for the first and last images and higher for C4 for the two middle images.
# Note that only the color differences matter, since the output of the second layer will be squared. For example, the first and the last filters should be roughly equivalent after squaring.

# In[34]:

spat_filt_weights = bhno_right_left_model.layers[1].get_weights_topo()[:,::-1,::-1,:]
fig, axes = pyplot.subplots(1, 4, sharex=True, sharey=True)
for i_weight in range(4):
    axes[i_weight].imshow(spat_filt_weights[i_weight,0].T, cm.bwr, 
                          interpolation='nearest', origin='upper', 
                          vmin=-np.max(np.abs(spat_filt_weights)),
                         vmax=+np.max(np.abs(spat_filt_weights)))
    axes[i_weight].grid(False)
    axes[i_weight].set_adjustable('box-forced')
pyplot.xticks((0,1), ('C3', 'C4'))
pyplot.yticks((0,1), ('Temporal Filter 1', 'Temporal Filter 2'))
None


# In the case of the raw net, we can make another insightful visualization of the weights. We can use the fact that we do not have a non-linear activation function between the first and the second layer. Therefore we can multiply the weights of the first layer with the weights of the second layer to get combined weights.

# In[8]:

combined_weights = np.tensordot(spat_filt_weights, temporal_weights, axes=(3,0))

combined_weights = combined_weights.squeeze()

for i_weight in range(4):
    _ = plot_sensor_signals(combined_weights[i_weight], ['C3', 'C4']).suptitle(
    'Combined Filter ' + str(i_weight + 1), fontsize=16, y=1.2)
    pyplot.xticks(range(0,31,5), map(lambda ms: "{:.1f}".format(ms),
                                     np.arange(0,31,5) * 1000 / 150.0))
    pyplot.xlabel("milliseconds")


# Again you can see the lateralization when comparing combined filters 1 and 4 to combined filters 2 and 3. Also note that combined filter 1 is pretty much a vertically flipped version of combined filter 4. This corresponds to the flipped colors in the figure before.
# The fact that we see both filters appear almost identical twice (1/4 and 2/3) could be a result of training with dropout.

# Now we can look at the final weights of the softmax layer. The softmax layer has weights for each filter and each time-pooled output from the layer before. You can see, that the net seems more sensitive to the first half of the trial.
# The weights are plausible. For example, filter 1, which matches an oscillation in sensor C3, has larger weights for left hand and smaller weights for right hand. This is consistent with a *decreasing* oscillation on the left-sided C3 sensor for the right hand.

# In[328]:

x_pools = np.arange(0,53)
x_times = np.arange(0,521, 10) * 1000 / 150.0
x_times = np.array(["{:.1f}".format(time) for time in x_times])
soft_weights = bhno_right_left_model.layers[-1].get_weights_topo().squeeze()
sensor_names = map(lambda i: "Combined Filter {:d}".format(i), range(1,5))
_ = plot_sensor_signals(soft_weights[0].T, sensor_names, figsize=(5,2.5)).suptitle(
    "Right Hand Softmax Weights", y=1.02, fontsize=16)
pyplot.xticks([0,15,30,45], [0,1000,2000,3000])
pyplot.xlabel('milliseconds (at start of pool region)')
_ = plot_sensor_signals(soft_weights[1].T, sensor_names, figsize=(5,2.5)).suptitle(
    "Left Hand Softmax Weights", y=1.02, fontsize=16)
pyplot.xticks([0,15,30,45], [0,1000,2000,3000])
pyplot.xlabel('milliseconds (at start of pool region)')
None


# ### Activations

# <div class="summary">
# <ul>
# <li>Activations can show you how network transforms the data to get useful features</li>
# </ul>
# </div>

# We can also look at the activations for specific trials to see how the signals get transformed for the classification.
# We will look at a trial which the net confidently correctly classifies as left hand.
# We would expect a decrease of variance in a certain frequency band on sensor C4.
# In the trial, this decrease seems mostly visible between around 500 to 1000 milliseconds and after 2500 milliseconds.

# In[36]:

from motor_imagery_learning.train_scripts.train_with_params import (
    create_training_object_from_file)
bhno_left_right_train = create_training_object_from_file(
    'data/models/two-class/raw-square/increasing-filters/test/36.yaml')
bhno_left_right_train.dataset_splitter.ensure_dataset_is_loaded()
datasets= bhno_left_right_train.get_train_fitted_valid_test()


# In[244]:

test_preds = bhno_right_left_model.info_predictions['test']

test_topo = datasets['test'].get_topological_view()
test_y = datasets['test'].y

pred_correct = np.argmax(test_preds, axis=1) == np.argmax(test_y, axis=1)
np.sum(pred_correct) / float(len(test_preds))
pred_correct_inds = np.flatnonzero(pred_correct)


# In[334]:

#print test_preds[pred_correct_inds][0]
#print test_preds[pred_correct_inds][8]
#print pred_correct_inds[8]


# In[247]:

_ = plot_sensor_signals(test_topo[9].squeeze(), ['C3', 'C4'], figsize=(8,2)).suptitle(
    'Raw Signals', fontsize=16, y=1.03)
pyplot.xticks(range(0,601,150), [0,1000,2000,3000,4000])
pyplot.xlabel('milliseconds')
None


# Now we look at the activations after layer 1, the temporally filtered signals. We see that the filter 2 seems to resemble a bandpass, it mostly retains an oscillation in a certain frequency band.
# 

# In[329]:

from motor_imagery_learning.analysis.util import create_activations_func

activations_func = create_activations_func(bhno_right_left_model)
activations = activations_func(test_topo[9:10])
_ = plot_sensor_signals_two_classes(activations[0][0,0].T, activations[0][0,1].T, 
                                    sensor_names=['C3', 'C4'], 
                                    class_1_name='Temporal Filter 1',
                                    class_2_name='Temporal Filter 2',
                                   figsize=(8,4)).suptitle(
    'Temporally Filtered Signals (outputs of first layer)', fontsize=16)
pyplot.xticks([0,150,300,450], [0,1000,2000,3000])
pyplot.xlabel('milliseconds (at start of convolution region)')
None


# When we look at the squared outputs of the second layer, we see that the net manages to transform the signal to signals useful for classification. The combined filter 1 and 4 now show higher variances than 2 and 3.

# In[330]:

sensor_names = map(lambda i: "Combined Filter {:d}".format(i), range(1,5))
_ = plot_sensor_signals(activations[1].squeeze(), sensor_names, figsize=(8,4)).suptitle(
    'Squared output of the second layer', fontsize=16)
pyplot.xticks([0,150,300,450], [0,1000,2000,3000])
pyplot.xlabel('milliseconds (at start of convolution region)')
None


# Finally, the network pools together the outputs by a sliding window with 50 samples length and 10 samples stride.
# We use the logarithm of the sum of the window as a feature for our softmax classifier. 
# The output after pooling is shown below and you again can nicely see the difference between combined filters 1/4 and 2/3.

# In[331]:

sensor_names = map(lambda i: "Combined Filter {:d}".format(i), range(1,5))
_ = plot_sensor_signals(activations[2].squeeze(), sensor_names, figsize=(8,4)).suptitle(
    'Output after pooling', fontsize=16)
pyplot.xticks([0,15,30,45], [0,1000,2000,3000])
pyplot.xlabel('milliseconds (at start of pool region)')
None


# If we look at the pooled output weighted with the softmax weights and summed across the combined filters, we get a timecourse for both classes. We can see that the classifier is mostly able to seperate the trial by the time until around 1300 milliseconds and by the time between around 1550 and 1900 milliseconds.

# In[332]:

softmax_in = (bhno_right_left_model.layers[-1].get_weights_topo().transpose(0,3,1,2) *
              activations[2])
pyplot.plot(np.sum(softmax_in, axis=1).squeeze().T)

pyplot.xticks([0,15,19.5,23.25,30,45], [0,1000,1300,1550,2000,3000])
pyplot.legend(("Right Hand", "Left Hand"), loc='right', bbox_to_anchor=(1.225, 0.55))

pyplot.xlabel('milliseconds (at start of pool region)')
pyplot.title(
    'Weighted Softmax inputs, summed across units of second layer', fontsize=16)
None


# The network correctly classifies this with 98.9% as a left hand trial.

# ## 

# ## Reconstructing input by the gradient

# <div class="summary">
# <ul>
# <li>To get a "desired" input of a unit, you can use the gradient of the activation of that unit on random input
# </ul>
# </div>

# We want to visualize what kind of feature a particular unit might encode. To do that, one can show what kind of input would lead to a high activation. <span class="todo"> references</span>
# 
# 1. Start with random input
# 2. Compute the gradient of the unit on that input 
# 3. Weight gradient by a learning rate and add it to the input
# 4. Repeat steps 2-3 (until you reach a specified activation or after a certain number of repetitions)
# 

# We use a variation of this to show desired inputs for a desired class:
# 
# 1. Start with random input
# 2. Compute the gradient of the softmax cost for the desired class on that input
# 3. Decay the input by a factor (e.g 0.99)
# 4. Weight gradient by a learning rate and **subtract** it from the input
# 5. Repeat steps 2-4 for a predefined number of repetitions
# 
# The decay in step 3 is mathematically equivalent to adding the squared sum of the input to the costs, i.e. rewarding inputs with a lower squared sum.
# 
# 
# <span class="todo">
# The decay seems to help (reduces input for our zerolineclass), still have to recheck and explain properly, why.
# Maybe helps because: Our signal for one class should match a certain frequency (i.e., have high output when convolved with a sine of that frequency), and for second class should not match the frequency.
# For the second class any random noise signal might produce output 0 when convolved with the sine + sumpooling, not only the flat zeroline and also not only signals with smallvariance/magnitude.
# </span>

# ### Artificial Data

# <div class="summary">
# <ul>
# <li>Artificial data roughly shows the expected reconstruction</li>
# <li>Valid convolution affects reconstruction</li>
# </ul>
# </div>

# We will start with one artificial example dataset to emphasize some precautions you have to make when interpreting the reconstruction. Then we continue showing the reconstruction for our running example.

# We show one example for an artificial dataset with just one channel, each trial has 600 samples:
# 
# * Class 1: flat zeroline + random gaussian noise
# * Class 2: sine of frequency 11 + random gaussian noise
# 
# Our net consists of:
# 
# 1. Convolution + sumlog pooling 
# 2. Softmax

# We show the visualizations for class 0 and class 1 after 1000 epochs with decay factor 0.99 and learning rate 0.1:

# In[13]:

get_ipython().run_cell_magic(u'capture', u'', u'inputs_class_1, inputs_class_2 = get_reconstructions_artificial_data()')


# In[14]:

fig, axes = pyplot.subplots(2,1)
axes[0].set_title("Class 1")
axes[0].plot(np.squeeze(inputs_class_1[-1,2]).T)
axes[0].set_yticks([-0.0001,0,0.0001])
axes[1].set_title("Class 2")
axes[1].set_yticks([-0.05,0,0.05])
axes[1].plot(np.squeeze(inputs_class_2[-1,2]).T)
fig.subplots_adjust(hspace=1)


# We can nicely see the recovered sine wave for class 2. Note the much smaller overall variance for class 1.
# You have to be cautios when interpreting the start and the end of the input. The smaller amplitude at the start and end can be explained by the fact that we use *valid* convolutions: We convolve over the input without any padding. The first sample and last sample therefore only contribute to one convolution output, whereas a sample in the middle will contribute $\#kernel\_shape$ times to the convolutional output, in our case, 150 times. 

# ### Real Two-Class Data

# <div class="summary">
# <ul>
# <li>Right hand vs left hand, lateralization visible</li>
# </ul>
# </div>

# We now show the same visualization for our running right vs left example.

# In[363]:

#bhno_right_left_model = serial.load('data/models/two-class/raw-square/identity-bp/test/6.pkl').model
in_grad_func = create_input_grad_for_wanted_class_func(bhno_right_left_model)
batches = 3
start_in = create_rand_input(bhno_right_left_model, batches)
ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000)
inputs_class_1 = gradient_descent_input(start_in, in_grad_func, [1,0], **ascent_args)
inputs_class_2 = gradient_descent_input(start_in, in_grad_func, [0,1], **ascent_args)
_ = plot_sensor_signals_two_classes(inputs_class_1[-1,0], inputs_class_2[-1,0], ['C3', 'C4'],
                               'Right Hand', 'Left Hand')
pyplot.xticks([0,150,300,450,600], [0,1000,2000,3000,4000])
pyplot.xlabel('milliseconds')
None


# We see that the left hand shows a higher variance overall, especially for sensor C3. This is consistent with a variance decrease for sensor C3 (on the left side) for the right hand.
# Note that it correctly only shows an increase of varaince after around 500 milliseconds, this is longer than could be explained just by the valid convolution/border effect.
# We also can see that the network seems to focus mostly on the first half of the trial.

# <span class="todecide"> also try with decay + subtract gradient + add (anti-decay)? or just larger learning rate? or include learning rate and decay factor? or just smoothing constraint</span>
