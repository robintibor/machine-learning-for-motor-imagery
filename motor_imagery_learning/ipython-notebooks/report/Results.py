
# coding: utf-8

# In[26]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\n\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n# make sure to use cpu for printing results...\nos.environ[\'THEANO_FLAGS\'] = "floatX=float32,device=cpu,nvcc.fastmath=True"\n%load_ext autoreload\n%autoreload 2\nimport matplotlib\nfrom matplotlib import pyplot\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (12.0, 5.0)\nmatplotlib.rcParams[\'font.size\'] = 7\nfrom analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file\nfrom motor_imagery_learning.analysis.print_results import ResultPrinter\nfrom motor_imagery_learning.csp.print_csp_results import CSPResultPrinter\nfrom pylearn2.utils import serial\nimport numpy as np\nfrom motor_imagery_learning.analysis.plot_util import (plot_nlls_for_file, \n    plot_misclasses_for_result, plot_misclasses_for_file, plot_nlls)\nfrom motor_imagery_learning.analysis.results import ResultPool, DatasetAveragedResults\nfrom glob import glob\nimport datetime\nimport seaborn\nseaborn.set_style(\'darkgrid\')\nseaborn.set_context("notebook", rc={"lines.linewidth": 0.5})\nfrom motor_imagery_learning.analysis.results import compute_confusion_matrix\nfrom motor_imagery_learning.analysis.results import compute_confusion_matrix_csp\nfrom motor_imagery_learning.analysis.plot_util import plot_confusion_matrix\nfrom matplotlib import cm')


# In[3]:

def print_markdown_table(headers, rows):
        headerline = "|".join(headers)
        headerline = "|" + headerline + "|"
        print headerline
        
        # make seperatorline for table
        seperatorline = "|".join(["-" for _ in headers])
        seperatorline = "|" + seperatorline + "|"
        print seperatorline
        for row in rows:
            rowstrings = [str(value) for value in row]
            rowstrings = [rowstr.replace("$", "") for rowstr in rowstrings]
            rowline = "|".join(rowstrings)
            rowline = "|" + rowline + "|"
            print rowline

def create_rows(all_methods_results, cleaner_type, training_type, sensor_names):
    all_rows = []
    for method_name, all_group_results in all_methods_results:
        clean_results = [gres for gres in all_group_results if (
            gres[0]['parameters'].get('train_cleaner', None) == cleaner_type or
            gres[0]['parameters'].get('training_type', None) == training_type)]
        assert len(clean_results) == 4
        all_eeg_results = [gres for gres in clean_results if (
            gres[0]['parameters']['sensor_names'] == sensor_names)]
        assert len(all_eeg_results) == 2
        sample_300_results = [gres for gres in all_eeg_results if (
            gres[0]['parameters'].get('resampling_fs',None) == 300 or
            gres[0]['parameters'].get('resample_fs',None) == 300 or
            gres[0]['parameters'].get('frequency_stop', None) == 144)]
        assert len(sample_300_results) == 1
        training_times = [res['training_time'] for res in sample_300_results[0]]
        train_misclass = [np.atleast_1d(res['misclasses']['train'][0])[-1] for res in sample_300_results[0]]
        test_misclass = [np.atleast_1d(res['misclasses']['test'][0])[-1] for res in sample_300_results[0]]

        all_rows.append([method_name, '300', 
                         str(datetime.timedelta(seconds=round(np.mean(training_times)))),
                         str(datetime.timedelta(seconds=round(np.std(training_times)))),
                         to_perc_str(1 - np.mean(test_misclass)), 
                         to_perc_str(np.std(test_misclass)),
                         to_perc_str(1 - np.max(test_misclass)),
                         to_perc_str(1 - np.min(test_misclass))])#,
                         #to_perc_str(1 - np.mean(train_misclass)), 
                         #to_perc_str(np.std(train_misclass))])
        if len(test_misclass) < 18:
            all_rows[-1].append('Incomplete ({:d} dataset(s) ran)'.format(len(test_misclass)))


        sample_150_results = [gres for gres in all_eeg_results if (
            gres[0]['parameters'].get('resampling_fs',None) == 150 or
            gres[0]['parameters'].get('resample_fs',None) == 150 or
            gres[0]['parameters'].get('frequency_stop', None) == 72)]
        assert len(sample_150_results) == 1
        training_times = [res['training_time'] for res in sample_150_results[0]]
        train_misclass = [np.atleast_1d(res['misclasses']['train'][0])[-1] for res in sample_150_results[0]]
        test_misclass = [np.atleast_1d(res['misclasses']['test'][0])[-1] for res in sample_150_results[0]]
        all_rows.append([method_name, '150', 
                         str(datetime.timedelta(seconds=round(np.mean(training_times)))),
                         str(datetime.timedelta(seconds=round(np.std(training_times)))),
                         to_perc_str(1 - np.mean(test_misclass)), 
                         to_perc_str(np.std(test_misclass)),
                         to_perc_str(1 - np.max(test_misclass)),
                         to_perc_str(1 - np.min(test_misclass))])#,
                         #to_perc_str(1 - np.mean(train_misclass)), 
                         #to_perc_str(np.std(train_misclass))])
        if len(test_misclass) < 18:
            all_rows[-1].append('Incomplete ({:d} dataset(s) ran)'.format(len(test_misclass)))
    return all_rows

def extract_results(all_methods_results, cleaner_type, training_type, sensor_names,
                   resampling_fs):
    wanted_results = dict()
    for method_name, all_group_results in all_methods_results:
        clean_results = [gres for gres in all_group_results if (
            gres[0]['parameters'].get('train_cleaner', None) == cleaner_type or
            gres[0]['parameters'].get('training_type', None) == training_type)]
        assert len(clean_results) == 4
        proper_sensor_results = [gres for gres in clean_results if (
            gres[0]['parameters']['sensor_names'] == sensor_names)]
        assert len(proper_sensor_results) == 2
        
        if resampling_fs == 300:
            proper_sample_results = [gres for gres in proper_sensor_results if (
                gres[0]['parameters'].get('resampling_fs',None) == 300 or
                gres[0]['parameters'].get('resample_fs',None) == 300 or
                gres[0]['parameters'].get('frequency_stop', None) == 144)]
        else:
            assert resampling_fs == 150
            
            proper_sample_results = [gres for gres in proper_sensor_results if (
                gres[0]['parameters'].get('resampling_fs',None) == 150 or
                gres[0]['parameters'].get('resample_fs',None) == 150 or
                gres[0]['parameters'].get('frequency_stop', None) == 72)]
        assert len(proper_sample_results) == 1
        wanted_results[method_name] = proper_sample_results
    return wanted_results
    
def to_perc_str(value):
    return "{:.2f}%".format(100  * value)


# In[4]:

get_ipython().run_cell_magic(u'javascript', u'', u'$(\'body\').append(\'<script src="https://dl.dropboxusercontent.com/u/34637013/work/ipython-notebooks/js-for-table-sorting/jquery.tablesorter.min.js"></script>\')\n// Sort tables every 5 seconds...\nvar makeTablesSortable;\nmakeTablesSortable = function() { \n  $(\'.text_cell_render table\').not(\'.sorted_table\').tablesorter().addClass(\'sorted_table\');\n}\n\nsetInterval(makeTablesSortable, 5000);')


# In[45]:

get_ipython().run_cell_magic(u'html', u'', u'<style>\ndiv.input {\nwidth: 105ex; /* about 80 chars + buffer */\n...\n}\ndiv.text_cell {\nwidth: 105ex /* instead of 100%, */\n...\n}\n\ndiv.text_cell_render:not(.sorted_table) {\n/*font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;*/\nfont-family: "Charis SIL", serif; /* Make non-code text serif. */\nline-height: 145%; /* added for some line spacing of text. */\nwidth: 105ex; /* instead of \'inherit\' for shorter lines */\n...\n}\n\n/* Set the size of the headers */\ndiv.text_cell_render h1 {\nfont-size: 18pt;\n}\n\ndiv.text_cell_render h2 {\nfont-size: 14pt;\n}\n\n\n\n.CodeMirror {\nfont-family: Consolas, monospace;\n}\n\n.todo {\n    background-color: hsla(120, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n.todecide {\n    background-color: hsla(240, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n\n</style>\n\n<style>\ndiv.summary  {\n    background-color: hsla(40,0%,50%,0.2);\n    color:black;\n    border-radius: 5px;\n    padding: 2px;\n    margin-top:0px;\n}\ndiv.summary ul\n{\n    list-style-type: none;\n}\n</style>\n<link rel="stylesheet" href="./motivation.css" type="text/css">\n<script async defer src="//hypothes.is/embed.js"></script>')


# # Results

# We show the results for:
# 
# * cleaned test fold  
# * unclean test fold
# 
# We also separately show results for:
# 
# * sampling rates 150 and 300 Hz
# * using all EEG sensors or only sensors with a C in their name (C sensors)
# 
# Note that the cleaned test fold can also contain trials that the unclean test fold does not contain (e.g., if the entire unclean test fold was rejected by the cleaning). 
# The cleaned test fold is always 10% of the overall data, whereas the uncleaned test fold's relative size can vary, depending on how much trials are rejected in the training fold.
# 
# We show means of the test set misclassification accuracies, the accuracies for the worst and the best dataset, as well as running times. Standard deviations are standard deviations across all 18 datasets.

# ## Cleaned Data

# ### All EEG Sensors

# For all sensors, we see that the raw net works best with around 91% accuracy. The filter bank net has almost the same accuracy, FBCSP is behind by about 3-5% at ~86%, and the fft net shows clearly worse performance at only around 78%. We also see that adding phase does not improve and even degrades performance for the fft net.
# For all networks, except for the filter bank network, a higher sampling rate leads to modest improvements around 1%.
# 
# For training times, we see that the filter bank net has by far the worst training time with 9 hours on the 300 Hz data.
# The FFT net is the fastest with 13-15 minutes, with FBCSP following closely behind. The raw net is about 5 times slower than FBCSP, the filter bank net is about 12-18 times slower than FBCSP. 

# 
# |Methods|Sampling Rate|Time|Std|Test|Std|Worst|Best|
# |-|-|-|-|-|-|-|-|
# |FBCSP|300|0:32:57|0:04:10|86.93%|13.32%|40.62%|100.00%|
# |FBCSP|150|0:15:09|0:01:46|85.47%|14.47%|40.62%|100.00%|
# |Filter Bank Net|300|9:28:29|4:16:50|90.02%|12.86%|40.62%|98.88%|
# |Filter Bank Net|150|3:08:28|1:29:11|90.63%|12.15%|43.75%|100.00%|
# |Raw Net|300|2:25:00|1:01:45|91.52%|8.66%|61.46%|100.00%|
# |Raw Net|150|1:21:35|0:40:20|90.63%|7.92%|64.58%|98.88%|
# |FFT Net|300|0:15:19|0:06:20|78.75%|16.30%|40.62%|98.73%|
# |FFT Net|150|0:13:09|0:06:42|77.86%|12.09%|52.08%|94.38%|
# |FFT Net + Phase|300|0:23:24|0:10:59|78.38%|12.25%|50.00%|94.64%|
# |FFT Net + Phase|150|0:18:14|0:11:17|73.60%|14.39%|40.62%|92.13%|
# 

# ### C Sensors

# Restricting the sensor set the C-sensors improves performance for almost all variants (the only exception is the raw net with 150 Hz sampling rate).
# Raw net and filter bank net are again the best variants, now more closely followed by FBCSP. 
# The time differences are a bit smaller, with FBCSP being about 3 times faster than the raw net and about 6 -12 times faster than the filter bank net.

# 
# |Methods|Sampling Rate|Time|Std|Test|Std|Worst|Best|
# |-|-|-|-|-|-|-|-|
# |FBCSP|300|0:19:25|0:02:16|91.10%|13.37%|40.62%|100.00%|
# |FBCSP|150|0:10:36|0:01:08|88.25%|13.31%|39.58%|100.00%|
# |Filter Bank Net|300|3:47:42|1:52:29|92.74%|8.60%|62.50%|100.00%|
# |Filter Bank Net|150|1:09:09|0:30:47|90.83%|12.82%|40.62%|100.00%|
# |Raw Net|300|0:58:44|0:23:31|92.30%|5.93%|77.08%|100.00%|
# |Raw Net|150|0:33:39|0:14:30|90.45%|6.38%|77.38%|98.88%|
# |FFT Net|300|0:10:45|0:03:30|80.81%|14.68%|44.79%|100.00%|
# |FFT Net|150|0:09:24|0:02:37|80.07%|11.67%|51.19%|95.51%|
# |FFT Net + Phase|300|0:13:23|0:06:18|77.68%|15.99%|38.54%|100.00%|
# |FFT Net + Phase|150|0:11:05|0:04:00|77.48%|12.67%|45.83%|95.51%|
# 

# ## Unclean data

# ### All EEG Sensors

# The results for the unclean test fold are unsurprisingly worse, but only around 3-4%. 
# The raw net seems to deal best with the unclean data, retaining around 90% performance for the 300 Hz data.

# 
# 
# |Methods|Sampling Rate|Time|Std|Test|Std|Worst|Best|
# |-|-|-|-|-|-|-|-|
# |FBCSP|300|0:35:13|0:03:21|83.40%|13.73%|41.87%|98.75%|
# |FBCSP|150|0:15:53|0:01:18|81.25%|13.72%|41.25%|96.88%|
# |Filter Bank Net|300|7:43:14|3:18:21|86.84%|14.23%|39.38%|98.75%|
# |Filter Bank Net|150|2:33:39|1:17:17|86.70%|14.20%|38.13%|100.00%|
# |Raw Net|300|2:13:12|0:51:59|89.65%|8.56%|65.00%|99.38%|
# |Raw Net|150|1:10:00|0:25:14|87.22%|9.10%|61.25%|97.50%|
# |FFT Net|300|0:13:24|0:04:25|74.86%|17.11%|26.88%|96.25%|
# |FFT Net|150|0:12:14|0:05:14|74.65%|15.44%|25.62%|91.88%|
# |FFT Net + Phase|300|0:26:17|0:10:32|73.26%|15.73%|28.75%|95.00%|
# |FFT Net + Phase|150|0:17:22|0:06:41|71.32%|15.47%|26.25%|92.50%|
# 
# 

# ### C Sensors

# Using only the C sensors again improves the decoding accuracies. 
# Again, the raw net profits the least from the restricted sensor set, improving by less than half of a percent.

# 
# 
# 
# |Methods|Sampling Rate|Time|Std|Test|Std|Worst|Best|
# |-|-|-|-|-|-|-|-|
# |FBCSP|300|0:18:01|0:01:36|88.12%|13.86%|45.63%|99.38%|
# |FBCSP|150|0:10:15|0:00:53|86.70%|14.33%|41.87%|98.12%|
# |Filter Bank Net|300|3:06:58|1:14:36|90.52%|10.36%|55.62%|100.00%|
# |Filter Bank Net|150|1:11:06|0:25:24|87.92%|13.87%|39.38%|98.13%|
# |Raw Net|300|1:00:44|0:25:10|89.97%|8.77%|58.13%|100.00%|
# |Raw Net|150|0:33:30|0:17:37|87.60%|9.96%|52.50%|97.50%|
# |FFT Net|300|0:09:27|0:02:15|76.53%|16.22%|30.00%|96.25%|
# |FFT Net|150|0:06:49|0:02:57|73.75%|15.21%|28.75%|91.88%|
# |FFT Net + Phase|300|0:11:42|0:04:24|75.87%|16.30%|29.37%|95.63%|
# |FFT Net + Phase|150|0:09:12|0:03:33|73.30%|16.05%|30.00%|92.50%|
# 

# ## Summary

# Overall, the filter bank net and raw net slightly outperform our FBCSP implementation.
# The FFT Net trails far behind, and adding phase makes it even worse. 
# None of our approaches can improve by using more sensors than the C sensors.
# Increasing the sampling rate and using higher frequency slightly improves the accuracy for all variants in most conditions. 

# ## Details of best results

# We look at the best variant on the clean data in more detail. 
# The best variant is using the C-sensors with 300 Hz.
# We plot the accuracies for all our 18 datasets for all variants.
# You can see that dataset 5 is an outlier with a particularly bad performance. 
# The raw net seems to lose less accuracy than the other variants.  
# You also see that the fft net has a much lower accuracy for dataset 9 than the other variants, maybe indicating some noise which more strongly affects the fourier transformed data. 

# In[17]:

wanted_results = extract_results(all_methods_results, '*one_set_cleaner', 
                '*csp_trainer', '*C_sensors', 300)

fig = pyplot.figure(figsize=(8,2))
for method_name, _ in all_methods_results:
    training_times = [res['training_time'] for res in wanted_results[method_name][0]]
    train_misclass = [np.atleast_1d(res['misclasses']['train'][0])[-1] for res in wanted_results[method_name][0]]
    test_misclass = [np.atleast_1d(res['misclasses']['test'][0])[-1] for res in wanted_results[method_name][0]]
    pyplot.plot(1 - np.array(test_misclass), '-', linewidth=1.5)
method_names = [r[0] for r in all_methods_results]
pyplot.xticks(range(18),range(1,19))
pyplot.xlabel('Dataset')
pyplot.ylabel('Accuracy')

pyplot.legend(method_names, bbox_to_anchor=(1.275,1.0))
pyplot.title('Accuracies for clean data, C-sensors, 300 Hz', fontsize=16, y=1.02)
None


# We can also compare the filter bank net and the raw net more directly to FBCSP. In the following plot, we show the FBCSP accuracy on the x axis and the filter bank or raw net accuracy on the y axis. Any dot above the blue line is a dataset, where the accuracy was better than FBCSP, below the blue line, the accuracy was worse. We see the improvement comes mostly from a few datasets with particularly bad performance.

# In[18]:


csp_test_misclass = [np.atleast_1d(res['misclasses']['test'][0])[-1] for res in wanted_results['FBCSP'][0]]
fb_test_misclass = [np.atleast_1d(res['misclasses']['test'][0])[-1] for res in wanted_results['Filter Bank Net'][0]]
raw_test_misclass = [np.atleast_1d(res['misclasses']['test'][0])[-1] for res in wanted_results['Raw Net'][0]]
fft_test_misclass = [np.atleast_1d(res['misclasses']['test'][0])[-1] for res in wanted_results['FFT Net'][0]]
fftphase_test_misclass = [np.atleast_1d(res['misclasses']['test'][0])[-1] for res in wanted_results['FFT Net + Phase'][0]]

pyplot.figure(figsize=(2.5,2.5))
pyplot.plot([0.4, 1], [0.4,1], linewidth=1.5)
pyplot.plot(1 -np.array(csp_test_misclass), 1 - np.array(fb_test_misclass), '.', 
            markersize=10.)
pyplot.plot(1 -np.array(csp_test_misclass), 1 - np.array(raw_test_misclass), '.',
           markersize=10.)
pyplot.legend(method_names[0:3], bbox_to_anchor=(1.9,1.), fontsize=11)
pyplot.xlabel('FBCSP accuracy')
pyplot.ylabel('Other approach accuracy')
pyplot.xlim(0.38,1.02)
pyplot.ylim(0.38,1.02)
None


# Now let's try to understand which classes are most difficult to tell apart for our approaches. 
# We show confusion matrices for all approaches below. You see that there are a lot of mistakes distinguishing left and right for the FFT net. We can also see this if we compute the confusion matrix over all approaches. 

# In[19]:

method_and_conf_mat = [('FBCSP', compute_confusion_matrix_csp([r['result_objects'] 
                              for r in wanted_results['FBCSP'][0]]))]

for method_name in method_names[1:]:
    conf_mat= compute_confusion_matrix([r['result_objects'] for r in 
                                         wanted_results[method_name][0]])

    method_and_conf_mat.append((method_name, conf_mat))

None


# In[57]:

for method_name, conf_mat in method_and_conf_mat:
    plot_confusion_matrix(conf_mat, figsize=(4,4), colormap=cm.coolwarm)
    pyplot.grid(False)
    pyplot.title(method_name, y=1.02, fontsize=16)
None


# In[56]:

all_conf_mats = [m_c[1] for m_c in method_and_conf_mat]
summed_conf_mat = np.sum(all_conf_mats, axis=0)
plot_confusion_matrix(summed_conf_mat, figsize=(4,4), colormap=cm.coolwarm)
pyplot.grid(False)
pyplot.title('Sum over all variants', y=1.02, fontsize=16)
None


# This confusion matrix over all approaches will be dominated by the FFT nets, since the FFT nets make more mistakes than the other approaches. So we also show the overall confusion matrix excluding the FFT nets. Furthermore, we show the mistakes per classifier-pair (which is the same as the sum in the respective cells of the confusion matrix, e.g., for right vs left, the cell for prediction right and target left plus the cell for prediction left and target right), again without the fft nets. We see that right vs feet is the most easily distinguishable, whereas right vs left is the hardest. 

# In[55]:

all_conf_mats = [m_c[1] for m_c in method_and_conf_mat[:3]]
summed_conf_mat = np.sum(all_conf_mats, axis=0)
plot_confusion_matrix(summed_conf_mat, figsize=(4,4), colormap=cm.coolwarm)
pyplot.grid(False)
pyplot.title('Sum over all variants except FFT', y=1.02, fontsize=16)
None


# In[265]:

sums = []
for i in range(0,4):
    for j in range(i+1,4):
        sums.append(summed_conf_mat[i][j] + summed_conf_mat[j][i])
        

#sums


# |Right vs Left| Right vs Rest| Right vs Feet | Left vs Rest | Left vs Feet | Rest vs Feet|
# |-|-|-|-|-|-|
# |108| 41| 16| 72| 31| 63|

# Finally, we also show how many errors are attributable to the different classes (again excluding the fft nets). An error is attributed to a class, if either the class was wrongly predicted or wrongly *not* predicted. Note that this means any error is attributed to two classes.
# We see that the feet class has the least such attributed errors, whereas the left class has the most. 
# 
# To show that the cleaned dataset is roughly balanced, we also show the overall number of trials per class below.

# In[269]:

single_class_sums = []
for i in range(0,4):
    copied_conf_mat = np.copy(summed_conf_mat)
    copied_conf_mat[i][i] = 0
    single_class_sums.append(np.sum(copied_conf_mat[i]) + np.sum(copied_conf_mat[:,i]))
    
#print single_class_sums


# In[268]:

num_trials_per_class = np.sum(summed_conf_mat, axis=1) / 3
#num_trials_per_class


# 
# ** Errors class attributions** 
# 

# |Right|Left|Rest|Feet|
# |-|-|-|-|
# |165| 211| 176| 110|

# 
# ** Number of trials per class**
# 

# |Right|Left|Rest|Feet|
# |-|-|-|-|
# |317| 354| 339| 344|

# ### Smaller datasets

# In[12]:

raw_result_folder = 'data/models/final-eval/small/raw-net/early-stop/'
raw_resultpool = ResultPool()
raw_resultpool.load_results(raw_result_folder)
raw_dataset_averaged_pool = DatasetAveragedResults()

raw_dataset_averaged_pool.extract_results(raw_resultpool)
raw_small_all_group_results = raw_dataset_averaged_pool.results()


# In[13]:

csp_result_folder = 'data/models/final-eval/small/csp/'
csp_resultpool = ResultPool()
csp_resultpool.load_results(csp_result_folder)
csp_dataset_averaged_pool = DatasetAveragedResults()

csp_dataset_averaged_pool.extract_results(csp_resultpool)
csp_small_all_group_results = csp_dataset_averaged_pool.results()


# In[14]:

fb_result_folder = 'data/models/final-eval/small/csp-net/early-stop/'
fb_resultpool = ResultPool()
fb_resultpool.load_results(fb_result_folder)
fb_dataset_averaged_pool = DatasetAveragedResults()

fb_dataset_averaged_pool.extract_results(fb_resultpool)
fb_small_all_group_results = fb_dataset_averaged_pool.results()


# In[15]:

small_methods_results = (
                      ('FBCSP', csp_small_all_group_results),
                        ('Filter Bank Net', fb_small_all_group_results),
                       ('Raw Net', raw_small_all_group_results))


# In[16]:

small_means = []
for method_name, results  in small_methods_results:
    small_means.append((method_name, []))
    for fraction in [0.25, 0.5, 0.75]:
        this_results = [r for r in results if r[0]['parameters']['restricted_n_trials'] == fraction][0]
        test_misclass = [np.atleast_1d(res['misclasses']['test'][0])[-1] for res in this_results]
        test_acc = np.mean(1 - np.array(test_misclass))
        small_means[-1][1].append(test_acc)

    if method_name == 'FBCSP':
        small_means[-1][1].append(0.9110)
    elif method_name == 'Filter Bank Net':
        small_means[-1][1].append(0.9274)
    elif method_name == 'Raw Net':
        small_means[-1][1].append(0.923)


# We also want to see how the methods compare for different dataset sizes. Concretely, we wanted to answer the question: Restricting the dataset size, how good will the different methods predict the trials immediately following the training fold? 
# Therefore, after cleaning, but before the train/test split, we restricted the dataset size to 25%, 50% and 75% of the original size. Note that we then have different test folds for each dataset size. While this makes the comparisons a little more ambiguous, if we had always used the last 10% of the original data as the test fold, we would mix the effects of less training data and a longer time gap between the training data and the test data. 
# We show the mean accuracies across different subjects below for our best variant (C sensors and 300 Hz), excluding the fft nets. The filterbank net always has the best mean accuracy, especially for smaller datasets. The raw net seems to steadily benefit from more data. We also see that the last fold seems to be especially hard: Despite more training data, FBCSP and filterbank net actually have worse accuracies for the full dataset than for using 75% of the data. This may be because the last fold is intrinsically more difficult, for example due to the subjects being more tired. It might also be because we managed to overfit our model parameters during development. The 75% split will share a part of the test fold with the train/test split we most used during development. 

# In[24]:

pyplot.figure(figsize=(6,2))
for i_method in xrange(len(small_means)):
    means = small_means[i_method][1]
    pyplot.plot([0.25, 0.5, 0.75, 1.], means, marker='o', linestyle='None')
pyplot.xlabel('Fraction of dataset size/Mean train fold size (rounded)')
pyplot.ylabel('Mean accuracy')
pyplot.legend([m[0] for m in small_methods_results], bbox_to_anchor=(1.35,1.0))
pyplot.xlim(0.2,1.05)
pyplot.xticks((0.25,0.5,0.75,1), ('0.25/170', '0.5/340', '0.75/510', '1.0/680'))
None


# In[23]:

train_fold_size = []
for method_name, results  in small_methods_results:
    train_fold_size.append((method_name, []))
    for fraction in [0.25, 0.5, 0.75]:
        this_results = [r for r in results if r[0]['parameters']['restricted_n_trials'] == fraction][0]
        if method_name == 'FBCSP':
            set_sizes = [len(res['result_objects'].multi_class.train_labels[0]) for res in this_results]
        
        else:
            set_sizes = [len(res['result_objects'].targets['train']) for res in this_results]
        
        train_fold_size[-1][1].append(np.mean(set_sizes))
# just in case you want to check them, now they are hardcoded in plot above


# In[3]:

get_ipython().run_cell_magic(u'javascript', u'', u'$(\'body\').append(\'<script src="https://dl.dropboxusercontent.com/u/34637013/work/ipython-notebooks/js-for-table-sorting/jquery.tablesorter.min.js"></script>\')\n// Sort tables every 5 seconds...\n\nvar makeTablesSortable;\nmakeTablesSortable = function() { \n  $(\'.text_cell_render table\').not(\'.sorted_table\').tablesorter().addClass(\'sorted_table\');\n}\n\nsetInterval(makeTablesSortable, 5000);')


# In[273]:

headers = ['Methods', 'Sampling Rate', 'Time', 'Std', 'Test', 'Std', 'Worst', 'Best']#, 'Comment']

all_rows = create_rows(all_methods_results, '*one_set_cleaner', '*csp_trainer', '*all_EEG_sensors')
print_markdown_table(headers, all_rows)
print('\n')

all_rows = create_rows(all_methods_results, '*one_set_cleaner', '*csp_trainer','*C_sensors')
print_markdown_table(headers, all_rows)
print('\n')

all_rows = create_rows(all_methods_results, '*two_set_cleaner', '*csp_two_file_trainer','*all_EEG_sensors')
print_markdown_table(headers, all_rows)
print('\n')

all_rows = create_rows(all_methods_results, '*two_set_cleaner', '*csp_two_file_trainer','*C_sensors')
print_markdown_table(headers, all_rows)
print('\n')


# In[5]:

#ResultPrinter('data/models/two-files-split/csp-net/all_EEG_sensors//early-stop/').print_results(
#    print_individual_datasets=False)


# In[6]:

fft_result_folder = 'data/models/final-eval/fft/early-stop/'
fft_resultpool = ResultPool()
fft_resultpool.load_results(fft_result_folder)
fft_dataset_averaged_pool = DatasetAveragedResults()

fft_dataset_averaged_pool.extract_results(fft_resultpool)
fft_all_group_results = fft_dataset_averaged_pool.results()


# In[7]:

fft_power_all_group_results = [gres for gres in fft_all_group_results if (
    gres[0]['parameters']['transform_function_and_args'] == '*power_func')]
fft_phase_all_group_results = [gres for gres in fft_all_group_results if (
    gres[0]['parameters']['transform_function_and_args'] == '*power_phase_func')]
assert len(fft_phase_all_group_results) == len(fft_power_all_group_results)
assert len(fft_phase_all_group_results) == 8


# In[8]:

raw_result_folder = 'data/models/final-eval/raw-net/early-stop/'
raw_resultpool = ResultPool()
raw_resultpool.load_results(raw_result_folder)
raw_dataset_averaged_pool = DatasetAveragedResults()

raw_dataset_averaged_pool.extract_results(raw_resultpool)
raw_all_group_results = raw_dataset_averaged_pool.results()


# In[9]:

fb_result_folder = 'data/models/final-eval/csp-net/early-stop/'
fb_resultpool = ResultPool()
fb_resultpool.load_results(fb_result_folder)
fb_dataset_averaged_pool = DatasetAveragedResults()

fb_dataset_averaged_pool.extract_results(fb_resultpool)
fb_all_group_results = fb_dataset_averaged_pool.results()


# In[10]:

csp_result_folder = 'data/models/final-eval/csp-standardized/'
csp_resultpool = ResultPool()
csp_resultpool.load_results(csp_result_folder)
csp_dataset_averaged_pool = DatasetAveragedResults()

csp_dataset_averaged_pool.extract_results(csp_resultpool)
csp_all_group_results = csp_dataset_averaged_pool.results()


# In[11]:

all_methods_results = (
                      ('FBCSP', csp_all_group_results),
                        ('Filter Bank Net', fb_all_group_results),
                       ('Raw Net', raw_all_group_results),
                      ('FFT Net', fft_power_all_group_results),
                      ('FFT Net + Phase', fft_phase_all_group_results),)

