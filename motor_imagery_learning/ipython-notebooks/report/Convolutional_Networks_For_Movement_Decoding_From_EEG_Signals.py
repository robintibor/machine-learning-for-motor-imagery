
# coding: utf-8

# In[2]:

get_ipython().run_cell_magic(u'html', u'', u'<style>\ndiv.input {\nwidth: 105ex; /* about 80 chars + buffer */\n...\n}\ndiv.text_cell {\nwidth: 105ex /* instead of 100%, */\n...\n}\n\ndiv.text_cell_render:not(.sorted_table) {\n/*font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;*/\nfont-family: "Charis SIL", serif; /* Make non-code text serif. */\nline-height: 145%; /* added for some line spacing of text. */\nwidth: 105ex; /* instead of \'inherit\' for shorter lines */\n...\n}\n\n/* Set the size of the headers */\ndiv.text_cell_render h1 {\nfont-size: 18pt;\n}\n\ndiv.text_cell_render h2 {\nfont-size: 14pt;\n}\n\n\n\n.CodeMirror {\nfont-family: Consolas, monospace;\n}\n\n.todo {\n    background-color: hsla(120, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n.todecide {\n    background-color: hsla(240, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5);\n}\n\n</style>\n\n<style>\ndiv.summary  {\n    background-color: hsla(40,0%,50%,0.2);\n    color:black;\n    border-radius: 5px;\n    padding: 2px;\n    margin-top:0px;\n}\ndiv.summary ul\n{\n    list-style-type: none;\n}\n</style>\n<link rel="stylesheet" href="./motivation.css" type="text/css">\n<script async defer src="//hypothes.is/embed.js"></script>')


# # Convolutional Networks for Movement Decoding from EEG signals

# 
# <div class="motivation">
# 
# <ul>
# <li>*want to help understand information contained in EEG*
# <li>*for example help paralyzed people control sth with their thoughts*
# <li>*deep learning might help discover new insights or/and improve performance*
# </ul>
# </div>

# ## Abstract

# Inspired by recent successes of convolutional networks, we use them to decode executed movements from EEG signals.
# We hope convolutional networks' ability to learn highly complex features and integrate large amounts of data can make the decoding more reliable and ultimately contribute to more useful brain computer interfaces for patients. Visualizing the networks might allow to discover new or underused features helpful for the decoding even when using standard approaches. Concretely, we investigate whether convolutional networks can match or exceed the accuracy of a state of the art common spatial patterns decoding approach. We also try to create insightful and interpretable visualizations for the networks. 
# 
# We therefore evaluate three convolutional networks for classifiying executed movements from EEG data. They differ in their input: raw data, bandpassed data or fourier transformed data. All other transformations of the input (e.g., spatial filtering) are learned by the networks.
# We collected 1040 trials in which subjects had to either rest, move their left hand, right hand or feet.
# 
# ... We show comparable performance to common spatial patterns?....
# .... Visualizations show....
# ... in the future nets use larger amounts of data, transfer learning, adapt specific layers....

# 
# 
# <div class="motivation">
# (Using deep learning on EEG data may allow several things which are difficult with simpler machine learning models: exploit more complex features, possibly even undiscovered features; transfer knowledge learned on previous subjects to classify new subjects; adapt a learned model to changing conditions. )
# 
# We (therefore) evaluate deep neural networks for classifying motor imagery(?) EEG data. [We find them to be competitive with state of the art multi-class filterbank common spatial patterns and better at blablaadpativetransfer?].
# We collected 1040 trials in which subjects had to either rest, move their left hand, right hand or feet.
# Our deep learning models are convolutional neural networks getting raw data, bandpass-filtered data and complex bandpower data as input. In some layers, we use units that square their input to exploit that a lot of information should be contained in the variance of the signal. 
# 
# We show nothing because our data is kaputt :p
# Therefore there is nothing to conclude.
# So we have added nothing to this research field, so what? :)
# (As brain computer interfaces may get closer to being used by more patients in real life, exploiting recent advances in machine learning to improve their performance seems very important.)
# 
# We hope convolutional networks can exploit several advantages over current state of the art decoding procedures.  Convolutional networks can exploit more complex features, transfer information learned on previos subjects to the next, integrate large amounts of data and be retrained intelligently to adapt to specific changes.
# </div>

# ##[Introduction](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Introduction.html)

# ##[Decoding Methods](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Methods_Algorithms.html)
# 
# ####[Common Spatial Patterns](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Methods_Common_Spatial_Patterns.html)
# ####[Filterbank Net](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Methods_Filterbank_Net.html)
# ####[Raw Net](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Methods_Raw_Square.html)
# ####[FFT Net](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Methods_FFT.html)

# ##[Related Work](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Related_Work.html)
# 
# ##[Network Training Methods](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Network_Training_Methods.html)

# ## [Experiments](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Experiments.html)

# ## [Results](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Results.html)

# ##Visualizations 
# ### [Motivation and Methods](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Visualizations.html)
# 
# #### [Filterbank Net](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/FB_Net_Visualizations.html)
# #### [Raw Net](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Raw_Net_Visualizations.html)
# #### [FFT Net](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/FFT_Net_Visualizations.html)

# ##[Conclusion](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Conclusion.html)

# ##[Discussion](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Discussion.html)

# ##[Future Work](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Future_Work.html)

# In[3]:

from IPython.display import IFrame
IFrame(src="https://rizzoma.com/embedded/b0d735211e7b6af18e60fed3cbeda0ff/", width=800, height=800)


# In[4]:

get_ipython().run_cell_magic(u'html', u'', u'<link rel="stylesheet" href="./motivation.css" type="text/css">\n<script async defer src="//hypothes.is/embed.js"></script>')

