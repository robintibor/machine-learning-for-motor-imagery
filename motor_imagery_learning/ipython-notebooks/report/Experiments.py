
# coding: utf-8

# In[ ]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nimport os\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n#os.environ[\'CUDA_LAUNCH_BLOCKING\'] = \'1\'\n#os.environ[\'THEANO_FLAGS\'] = os.environ[\'THEANO_FLAGS\'] + ",profile=True"\n# switch to cpu\n#os.environ[\'THEANO_FLAGS\'] = \'floatX=float32,device=cpu,nvcc.fastmath=True\'\n%load_ext autoreload\n%autoreload 2\n\nfrom motor_imagery_learning.bbci_pylearn_dataset import (compute_power_spectra)\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset\nimport numpy as np\n\nimport matplotlib\nfrom matplotlib import pyplot\nfrom matplotlib import cm\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (8.0, 2.0)\nmatplotlib.rcParams[\'font.size\'] = 7\n\nfrom pylearn2.costs.mlp import Default\nfrom pylearn2.models.mlp import Softmax, MLP\nfrom pylearn2.termination_criteria import EpochCounter\nimport seaborn\nseaborn.set_style(\'darkgrid\')\nfrom copy import deepcopy\nfrom motor_imagery_learning.train_scripts.train_experiments import ExperimentsRunner\nfrom motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer\nfrom motor_imagery_learning.analysis.data_generation import (create_sine_signal,\n    pipeline, create_shifted_sines,put_noise, convolve_with_weight,\n    max_pool_topo_view, log_sum_pool)\nfrom motor_imagery_learning.analysis.util import create_trainer\nfrom motor_imagery_learning.mypylearn2.conv_squared import ConvSquared\nfrom motor_imagery_learning.analysis.input_reconstruction import (\n    create_rand_input, gradient_descent_input, create_input_grad_for_wanted_class_func,\n    create_input_grad_with_smooth_penalty_for_wanted_class_func, \n    reconstruct_inputs_for_class)\n\nfrom motor_imagery_learning.analysis.plot_util import (plot_misclasses_for_model, \n    plot_nlls_for_model, plot_sensor_signals)\n\nimport h5py\nimport numpy as np\nfrom pylearn2.utils import serial\nfrom pprint import pprint\nfrom  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted\nimport h5py\nfrom motor_imagery_learning.csp.train_csp import CSPTrain\nfrom motor_imagery_learning.mypylearn2.train import TrainEarlyStop\nfrom motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD\nfrom motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset\nfrom motor_imagery_learning.bbci_dataset import BBCIDataset\nfrom pylearn2.costs.mlp import Default\nfrom pylearn2.models.mlp import Softmax, MLP\nfrom pylearn2.termination_criteria import EpochCounter\nfrom sklearn.cross_validation import KFold\nfrom pylearn2.datasets.dense_design_matrix import DenseDesignMatrix\nimport numpy as np\nfrom numpy.random import RandomState\nimport theano.tensor as T\nimport theano\nfrom pylearn2.space import Conv2DSpace\nfrom pylearn2.models.mlp import ConvElemwise\nfrom theano.sandbox.cuda.dnn import dnn_conv\nfrom theano.sandbox.cuda.basic_ops import gpu_alloc_empty\nfrom motor_imagery_learning.analysis.print_results import ResultPrinter\nfrom pylearn2.utils import serial\nfrom motor_imagery_learning.analysis.data_generation import generate_single_sensor_data\nfrom motor_imagery_learning.analysis.plot.raw_signals import (\n    plot_sensor_signals_two_classes)\nfrom motor_imagery_learning.mypylearn2.pool import PoolLayer, log_sum\nfrom motor_imagery_learning.analysis.util import bps_and_freqs\nfrom motor_imagery_learning.analysis.deconv import deconv_model\nfrom motor_imagery_learning.analysis.plot_util import (\n    plot_head_signals_tight_two_signals, plot_head_signals_tight)\nfrom motor_imagery_learning.csp.print_csp_results import CSPResultPrinter\nfrom motor_imagery_learning.analysis.filterbands import (\n    sum_abs_filterband_weights, compute_center_freqs, \n    compute_sum_abs_weights_for_csp_model)\nfrom motor_imagery_learning.analysis.plot.filterbands import (\n    show_freq_importances, plot_filterband_weights_for_train_state)\nfrom motor_imagery_learning.csp.train_csp import generate_filterbank\nfrom motor_imagery_learning.train_scripts.train_with_params import (\n    create_training_object_from_file)\nfrom motor_imagery_learning.mywyrm.plot import ax_scalp, CHANNEL_10_20\nfrom motor_imagery_learning.analysis.util import create_activations_func')


# In[2]:

get_ipython().run_cell_magic(u'javascript', u'', u"IPython.load_extensions('cite2c/main');")


# In[3]:

get_ipython().run_cell_magic(u'html', u'', u'<style>\ndiv.input {\nwidth: 105ex; /* about 80 chars + buffer */\n...\n}\ndiv.text_cell {\nwidth: 105ex /* instead of 100%, */\n...\n}\n\ndiv.text_cell_render {\n/*font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;*/\nfont-family: "Charis SIL", serif; /* Make non-code text serif. */\nline-height: 145%; /* added for some line spacing of text. */\nwidth: 105ex; /* instead of \'inherit\' for shorter lines */\n...\n}\n\n/* Set the size of the headers */\ndiv.text_cell_render h1 {\nfont-size: 18pt;\n}\n\ndiv.text_cell_render h2 {\nfont-size: 14pt;\n}\n\n\n\n.CodeMirror {\nfont-family: Consolas, monospace;\n}\n\n.todo {\n    background-color: hsla(120, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n.todecide {\n    background-color: hsla(240, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n\n</style>\n\n<style>\ndiv.summary  {\n    background-color: hsla(40,0%,50%,0.2);\n    color:black;\n    border-radius: 5px;\n    padding: 2px;\n    margin-top:0px;\n}\ndiv.summary ul\n{\n    list-style-type: none;\n}\n</style>\n<link rel="stylesheet" href="./motivation.css" type="text/css">\n<script async defer src="//hypothes.is/embed.js"></script>')


# # Experiments
# In this chapter, we will describe the experiments, including:
# 
# * datasets
# * cleaning methods
# * preprocessing
# * evaluation method

# ## Datasets
# 
# The datasets are 18 EEG recordings of motor executions. Subjects were instructed to either tap their right hand fingers, tap their left hand fingers, do nothing (resting state) or clench their toes in each trial. Trials lasted 4 seconds, with 3-4 seconds pause inbetween. The sequences of instructions were pseudorandomized.
# 
# There was a total of 1040 trials, divided into 13 runs with 80 trials each. A recording typically lasted around 4 hours. For some datasets, there are less trials, e.g., if the subject got too tired to continue. Subjects were all healthy students or PhD candidates. 
# 

# ## Cleaning
# 
# We cleaned our datasets, trying to remove noisy channels and trials as well as trials with artifacts.
# 
# ### Trial Removal by EOG Artifacts
# For all EOG channels (EOGh and EOGv in our case) and all trials, we compute the difference between the minimum and the maximum value on the EOG channel.
# We remove all trials were the difference between the maximum and the minimum value is greater than 600 Hz.
# 
# ### Channel and Trial Removal by Variance
# 
# We first bandpass our signal to 0.5 Hz to 150 Hz using a 4th-order butterworth filter.
# Then we compute the variance for all trials and channels. Now we have one variance value per trial per channel.
# We then determine a critical threshold for the variance. Above this threshold, we consider a trial/channel combination unclean.
# 
# ```
# Function compute_variance_threshold
# Input: variances, whisker_percentage, whisker_length
# Output: variance_threshold
# 1. low_variance := variance value at percentile whisker_percentage 
# 2. high_variance := variance value at percentile 100 - whisker_percentage
# 3. variance_threshold := high_variance + (high_variance - low_variance) * whisker_length
# ```
# 
# We use whisker percentage 10 and whisker length 3. This means our variance threshold will be at the 90th percentile of the variance plus 3 times the difference between the 90th and the 10th percentile of the variance.
# 
# Then we use the following cleaning algorithm:
# 
# 1. **Excessive Trial Removal**:  Do once:
#     1. Remove all trials where more than 20 percent of the channels exceed the variance threshold
#     2. Recompute variance threshold from remaining trials x channels
# 3. **Channel Removal**: Repeat until no more channels removed:
#     1. If the number of trial x channel variances above the threshold is larger than 5% of the number of remaining trials:
#        1. For all channels, compute number of remaining trials above the variance threshold
#        2. Remove channels where the number of trials above the threshold is larger than 5% of all remaining trials and larger than 10% of all remaining unclean trial x channel combinations
#     2. Recompute variance threshold from remaining trials x channels
# 4. **Trial removal**: Repeat until no more trials removed:
#     1. Remove all trials where at least one channel is above the variance threshold
#     2. Recompute variance threshold from remaining trials x channels
# 5. **Unstable channel removal**: Do once:
#     1. Compute variance of trial variances for each channel
#     2. Compute variance threshold for these trial-variance variances
#     3. Remove all channels with a trial-variance variance above the threshold
#     
# 
# This is largely following the [cleaning procedure of the BBCI Toolbox](https://github.com/bbci/bbci_public/blob/45cd9349dc60cb317e260b10d46de20bf9075afa/processing/utils/reject_varEventsAndChannels.m) with small modifications to prefer removing channels to removing trials. We remove channels first, because we expect that a lot of the noisy channels are of less interest to us for decoding motor execution, e.g., channels in the frontal area. By cleaning channels first, we might be able to retain more trials. The check in 3A might be in the BBCI Toolbox by accident, a more logical check would be to check whether more than 5% of all trial x channel variances are above the threshold (see also this [github issue](https://github.com/bbci/bbci_public/issues/66)). This might lead to less channels being cleaned for some cases, but should have no further negative consequences. The unstable channel removal should delete channels where the variance changes a lot from trial to trial.

# ## Preprocessing
# 
# Our preprocessing pipeline first preprocesses the continous data, and then standardizes the epoched data (i.e.. the trials). 
# 
# ### Continuous data preprocessing
# The continuous preprocessing is done across the entire dataset, ignoring train/test splits, like this:
# 
# 1. Resample to 150 Hz or 300 Hz
# 2. Highpass above 0.5Hz with butterworth filter of order 3 
# 3. For visualizations, subtract the common average of all channels for each sample
# 
# We subtract the common average for visualizations, since the original data is referenced to Cz and therefore the information from Cz will be spread to all sensors, while Cz itself will contain no information. We do not do the same for decoding, as for some very few datasets, this seems to worsen decoding performance. We think this is because some noisy channels were not removed by the cleaning procedure and then strongly affect the common average.
# 
# ### Trial standardization
# After the continuous preprocessing, we cut this data into trials and standardize like this:
# 
# 1. For the train fold, compute means and standard deviations for all channels x frequency combinations
# 2. Subtract the means from the train fold and divide by the standard deviations
# 3. For the test fold, go through the time-ordered trials. For each trial:
#     1. Compute means and standard deviations from the trials of the train fold and the trials of test fold until that trial
#     2. Subtract the means from that trial and divide by the standard deviations
# 
# For the filter bank net and common spatial patterns the frequency refers to the filterband, whereas for the FFT net, the frequency refers to the frequency bin. The raw net does not have a frequency dimension and means and standard deviations are only computed for the channels.
# 
# This standardization could also be applied online as it only uses information up until the current trial for the test fold. It can also be implemented efficiently, since you only need the last means and standard deviations and the next trial to compute the next means and standard deviations (see [this article](https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Online_algorithm), we use the formulas from [here](http://stats.stackexchange.com/a/43183/56289)). Another option would be to do running standardization, i.e., weigh the newer trials exponentially higher when computing the means and standard deviations. This might better account for changes in the variance. However, as we did not see large improvements for the decoding performance and a running standardization would introduce a new parameter (how strongly to favor the newer trials), we stuck with the aforementioned standardization procedure.

# ## Evaluation method
# 
# We decided to use the last two runs (i.e., 160 trials for a complete dataset) for our final evaluation. 
# During the development of our architectures, including the optimization of our hyperparameters, these two runs were never used. This should ensure we are not fitting our architectures or hyperparameters to our test data.
# 
# ### Cleaning evaluation data
# For ease of implementation, during development, we decided to always clean the entire dataset (disrespecting train/test splits). For our final evaluation, we decided to do two cleaning variants. The first one is the same as our development cleaning procedure:
# 
# 1. Clean entire dataset together
# 2. Use last 10% of the dataset as the test fold
# 
# In this case, we only have clean trials. However, we might have some trials of runs before the last two runs in our test set. This happens in case a lot of the trials in the test fold are removed by the cleaning procedure.
# 
# The second cleaning variant does not clean the trials of the test fold at all:
# 
# 1. Split dataset into train fold (first 11 runs) and test fold (last two runs)
# 2. Compute cleaned trials and channels of entire dataset together
# 3. Remove rejected channels from train and test fold
# 4. Remove rejected trials from train fold, but keep all trials in the test fold
# 
# This ensures the trials we are predicting were never used during development. However, we now predict on unclean trials. Unfortunately, it was not possible to clean the entire dataset, and only keep cleaned trials of the last two runs as our test fold. The problem here was, for atleast one dataset, all trials in the test fold were removed by the cleaning procedure.
# 
# Nevertheless, these two variants also have the advantage of showing the decoding accuracies for clean and for unclean trials for our different decoding methods.
# 
# ### Sensors and Sampling Rates
# We also tried using different sensor and sampling rates for our final evaluation. We either used all EEG sensors or only the EEG sensors with a C in their name. This allows us to see if adding more, and probably less informative, sensors to our input improves or degrades the accuracies for the different decoding methods. 
# 
# We also varied the sampling rate, either using 150 Hz or 300 Hz. This should show us how important the frequencies above 75 Hz (the Nyquist frequency for 150 Hz sampling rate) are for the decoding. 
# 
# Also, the variants with 150 Hz and/or only using the C-sensors allow us to see how much time we can save for the training (as our data becomes smaller) and might show a tradeoff between accuracy and training time.

# ### Machine Specifications (CPU, GPU, RAM)

# The networks were trained on [Nvidia Geforce GTX Titan Black](http://www.geforce.com/hardware/desktop-gpus/geforce-gtx-titan-black/specifications) graphic cards, which have 6 GB memory. The CPU was Intel(R) Xeon(R) CPU E5-2650 v2 @ 2.60GHz and the machines had 128 GB main memory. Filter Bank CSP was trained on machines with the same CPU and 64 GB main memory (main memory was not critical for any of the approaches).
