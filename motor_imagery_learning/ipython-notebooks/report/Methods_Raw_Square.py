
# coding: utf-8

# In[1]:

get_ipython().run_cell_magic(u'javascript', u'', u"IPython.load_extensions('cite2c/main');")


# In[7]:

get_ipython().run_cell_magic(u'html', u'', u'<style>\ndiv.input {\nwidth: 105ex; /* about 80 chars + buffer */\n...\n}\ndiv.text_cell {\nwidth: 105ex /* instead of 100%, */\n...\n}\n\ndiv.text_cell_render {\n/*font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;*/\nfont-family: "Charis SIL", serif; /* Make non-code text serif. */\nline-height: 145%; /* added for some line spacing of text. */\nwidth: 105ex; /* instead of \'inherit\' for shorter lines */\n...\n}\n\n/* Set the size of the headers */\ndiv.text_cell_render h1 {\nfont-size: 18pt;\n}\n\ndiv.text_cell_render h2 {\nfont-size: 14pt;\n}\n\n\n\n.CodeMirror {\nfont-family: Consolas, monospace;\n}\n\n.todo {\n    background-color: hsla(120, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\nli.todo {\n    background-color: hsla(120, 40%, 50%, 0.1);\n    border-radius: 0px;\n    padding: 0px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n\n.todecide {\n    background-color: hsla(240, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n\n</style>\n\n<style>\ndiv.summary  {\n    background-color: hsla(40,0%,50%,0.2);\n    color:black;\n    border-radius: 5px;\n    padding: 2px;\n    margin-top:0px;\n}\ndiv.summary ul\n{\n    list-style-type: none;\n}\n</style>\n<link rel="stylesheet" href="./motivation.css" type="text/css">\n<script async defer src="//hypothes.is/embed.js"></script>')


# In[3]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nimport os\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n#os.environ[\'CUDA_LAUNCH_BLOCKING\'] = \'1\'\n#os.environ[\'THEANO_FLAGS\'] = os.environ[\'THEANO_FLAGS\'] + ",profile=True"\n# switch to cpu\n#os.environ[\'THEANO_FLAGS\'] = \'floatX=float32,device=cpu,nvcc.fastmath=True\'\n%load_ext autoreload\n%autoreload 2\n\nfrom motor_imagery_learning.bbci_pylearn_dataset import compute_power_spectra, BBCISetNoCleaner\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset\nimport numpy as np\n\nimport matplotlib\nfrom matplotlib import pyplot\nfrom matplotlib import cm\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (12.0, 5.0)\nmatplotlib.rcParams[\'font.size\'] = 7\n\nimport matplotlib.lines as mlines\nimport seaborn\nseaborn.set_style(\'darkgrid\')')


# ## Raw Net

# ### Overview

# <div class="summary"><ul>
# <li> Raw Net learns temporal filtering, spatial filtering, and classification jointly </li>
# <li> Could possibly learn typically unused features, e.g., motor related potentials</li>
# <li> More prone to overfit </li>
# <li> Learned model hard to interpret </li>
# </ul></div>
# 

# The raw net is a convolutional neural network using the raw data as input. The main difference to the filter bank net is that it doesn't use bandpassed data and learns temporal filters in the first layer. Also, it does not pool the squared outputs over the whole time, but over a number of neighbouring samples.
# 
# We show an overview below, the black boxes are weights and the blueish boxes are outputs/activations of a layer.

# <img src="https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/svgs/architectures/raw_square_explanation.svg?cache=no10"></img>

# Raw net's layers can learn the different steps of a typical motor imagery classification pipeline:
# 
# 1. The first layer convolves over time and could learn a bandpass filter or a matching of a specific frequency.
# 2. The second layer convolves over sensors and the units of the layer before. It could learn a spatial filter together with a weighting of the different filtered/matched frequencies. 
# 3. The second layer also squares the outputs, which transforms to the uncentered variance of the filtered of the signal.
# 4. The pooling layer pools timeblocks together. It computes the logarithm of the sum of the outputs in a timeblock. This creates features which might show the logarithmic bandpower in a certain timeblock, similar to the features CSP learns, but with temporal structure (e.g., the timecourse of the variance increase/decrease in a trial). 
# 5. The softmax layer learns to classify using these features.

# It is not enforced that the raw net learns exactly these features, e.g., instead of learning a bandpass filter in the first layer, it could also learn to match a specific signal shape, e.g., of a motor-related potential. Therefore it is very interesting to visualize what the model has learned, as we will try in the later visualization sections.

# ### Parameters

# We use the following parameter values:
# 
# |Parameter|Value|
# |-|-|
# |First layer units|40|
# |First filter shape|30x1|
# |Second layer units|40|
# |Second filter shape|1x $|$sensors$|$|
# |Second pool shape|100x1|
# |Second pool stride|20x1|
# 
# All filter strides are 1x1. These values are used if we have 300 Hz input (see Experiments section), if we use 150 hz input, the following values are changed:
# 
# |Parameter|Value|
# |-|-|
# |First filter shape|15x1|
# |Second pool shape|50x1|
# |Second pool stride|10x1|
# 
# This way, the filter shape, pool shape and pool stride always cover the same duration in milliseconds (100 ms, 333.33 ms and 66.66 ms).
# 
# Parameters common to all nets (e.g., regularization terms) are explained in the Network Training Methods section.

# <span class="todo"> check if you ever explained motor related potential, maybe remove or rewrite that sentence.</span>
# 
# <span class="todo">explanation of gradient, why it makes sense to square etc.</span>
# 
# <span class="todo">either in image or somewhere else, exackt dimensions of net</span>
# 
