
# coding: utf-8

# # Introduction

# We try to use convolutional networks to improve the decoding part of brain computer interfaces (BCIs). 
# We will first explain what brain computer interfaces are and how improving the decoding could make them more reliable and affordable. We then go on to explain what features a machine learning model has to use to decode movements and how a convolutional network might learn them.

# ## Brain Computer Interfaces and Decoding

# <div class="summary">
# <ul>
# <li>Brain computer interfaces could help people with paralysis</li>
# </ul>
# </div>
# 
# Brain computer interfaces decode signals from the brain and can allow persons to control something using their thoughts.
# A prominent long-term-vision is for people with paralysis to control their own limbs or robotic limbs.
# Ideally, this way they could be able to eat by themselves or do other things that normally require control over your body.
# Many people with paralysis show strong interest in being able to do such activities by themselves   <a data-cite="349468/8GS2SQFZ"target="_blank" href="http://online.liebertpub.com/doi/pdf/10.1089/neu.2004.21.1371">(Anderson, 2004)</a> <a data-cite="349468/KTEN5RG2"target="_blank" href="http://online.liebertpub.com/doi/pdf/10.1089/neu.2004.21.1371">(Zickler et al., 2009)</a>

# <div class="summary">
# <ul>
# <li>Making brain computer interfaces reliable and affordable is a big challenge</li>
# </ul>
# </div>
# 
# Lowering costs and improving reliability are two major challenges towards practically usable brain-computer-interfaces.
# For example, in the last years, BCIs have been developed that allowed people with paralysis to control the movements of a robotic arm and drink by themselves <a data-cite="349468/XFWUST4A"target="_blank" href="http://www.sciencemag.org/content/348/6237/860">(Pruszynski & Diedrichsen, 05/22/2015)</a> <a data-cite="349468/4PWK5KD3"target="_blank" href="http://www.sciencedirect.com/science/article/pii/S0140673612618169">(Collinger et al., February 22, 2013)</a> <a data-cite="349468/86RUBSG4"target="_blank" href="http://www.nature.com/nature/journal/v485/n7398/abs/nature11076.html">(Hochberg et al., May 17, 2012)</a>.
# These systems, including the research on them, are quite expensive <a data-cite="349468/Q9QJAM43"target="_blank" href="http://www.nature.com/nature/journal/v485/n7398/abs/485317a.html">(Jackson, 2012)</a>.
# To allow a lot of people to profit from possible future BCIs, it seems good to investigate cheaper BCIs. 
# At the same time, increasing the reliability of brain computer interfaces matters a lot to make them usable in daily life - the system should always do what the user intends so that it's safe and convenient to use.

# <div class="summary">
# <ul>
# <li>We want to improve the decoding using convolutional neural networks</li>
# </ul>
# </div>
# 
# 
# In this thesis, we try to improve the decoding part of an EEG brain computer interface. Our BCI tries to detect which part of the body a person is moving. We try to improve the decoding by using convolutional neural networks, a machine learning approach which has made great progress in recent years. Progress has been especially fast in fields, where - like in BCI decoding - natural signals are classified, such as object detection in images or speech recognition <a data-cite="349468/6C4P9CZU"target="_blank" href="http://www.nature.com/nature/journal/v521/n7553/abs/nature14539.html">(LeCun et al., 2015)</a> <a data-cite="349468/RFJCRJFI"target="_blank" href="http://arxiv.org/abs/1404.7828">(Schmidhuber, 01/2015)</a>. Improving the decoding could make cheaper BCIs - like EEG-based BCIs- more reliable and might also improve reliability for other types of BCIs. Also, convolutional neural networks could lead to new insights into what parts of the brain signal are helpful for decoding the movement. They are less constrained in what information they can use compared to the learning algorithms that are currently state of the art for decoding EEG signals.

# ## Signals for Motor Decoding 

# <div class="summary">
# <ul>
# <li>Doing or imagining movements causes fairly well-understood changes in the EEG signal</li>
# </ul>
# </div>
# 
# Imagining movements is a well-established way to control brain computer interfaces.
# Whenever you imagine to move or actually move a part of your body you can typically see a characteristic change in the EEG signal. 
# For certain sensors (typically over the motor cortex area), in some frequency bands (for example in the mu-band between 9-13Hz), the variance of the signal will decrease <a data-cite="349468/22HBJ4ZC"target="_blank" href="http://www.sciencedirect.com/science/article/pii/S1053811905025140">(Pfurtscheller et al., 2006)</a>. 
# The sensor which shows the decreased variance is normally opposite to the side of the body the person wants to move. So the decrease would often be visible on a sensor on the left side for a movement of the right hand and vice versa. 
# As an example, we show two trials, one trial where a person rested and another trial, where the same person moved the right hand. We show the signal recorded by the CP3-sensor which is on the left side of the head.
# Note the decrease of variance for the right hand trial.

# In[87]:

low_1_topo, low_3_topo, high_1_topo, high_3_topo = generate_single_sensor_data(
    'data/BBCI-without-last-runs/KaUsMoSc1S001R01_ds10_1-11.BBCI.mat', 'CP3')
fig = plot_single_sensor_low(low_1_topo, low_3_topo,trial_nr=26, figoptions=dict(figsize=(8,2)))

fig.axes[0].set_yticklabels([])
None


# One of the main hypotheses for explaining the decrease of variance states it is caused by a desynchronization of the firing rates of neurons. Concretely, according to this hypothesis, the decrease is caused by groups of neurons in a specific area of the motor cortex (see the figure for a schematic view) desynchronizing their firing rate <a data-cite="349468/TBBJRI4A"target="_blank" href="http://www.sciencedirect.com/science/article/pii/001346949090001Z">(Steriade et al., December 1990)</a> <a data-cite="349468/UCRUHCWC"target="_blank" href="http://www.sciencedirect.com/science/article/pii/S1388245799001418">(Pfurtscheller & Lopes da Silva, November 1, 1999)</a>.
# When you are not moving, the neurons in the motor cortex synchronize their firing rates with each other. Once you plan a movement with a body part, according to the desynchronization hypothesis, the neurons responsible for that body part get new inputs to process and this stops them from firing in the same rhythm.
# 
# <img src="http://ieeexplore.ieee.org/ielx5/79/4404853/4408441/html/img/4408441-fig-3-small.gif"></img>
# 
# <a data-cite="349468/D6IVJ2G2"target="_blank" href="undefined">(Blankertz et al., 2008)</a> © 2008 IEEE
# 
# At the same time, with some persons, you can also observe a higher variance in the gamma frequency bands, from 30Hz up to 150Hz <a data-cite="349468/A8WVHSGB"target="_blank" href="http://www.sciencedirect.com/science/article/pii/S1053811908001717">(Ball et al., June 2008)</a>. This might indicate smaller groups of neurons synchronizing their fast firing rates. This can be seen in the following plots using the same trials as above. Note the increase of variance for the right hand trial.

# In[92]:

fig = plot_single_sensor_high(high_1_topo, high_3_topo, trial_nr=26, figoptions=dict(figsize=(8,2)))
fig.axes[0].axvspan(500, 1500, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
fig.axes[1].axvspan(500, 1500, color=seaborn.color_palette()[2], alpha=0.2, lw=0)

fig.axes[0].set_yticklabels([])
None


# ### Decoding Difficulties

# <div class="summary">
# <ul>
# <li>In reality signals are not clean enough to always easily classify which body part the person wanted to move</li>
# </ul>
# </div>
# 
# In principle, this could allow you to reliably detect which bodypart somebody wants to move. In practice, there are several complications:
# 
# * the signal is "smeared" across several electrodes, especailly if the the source is not very close to the sensors  (volume conduction) <a data-cite="349468/D6IVJ2G2"target="_blank" href="http://ieeexplore.ieee.org/xpls/icp.jsp?arnumber=4408441">(Blankertz et al., 2008)</a> 
# * other unrelated sources also affect the sensor up other brain activity at the same time
# * the sensors also pick up electrical noise from muscle movements from the subject
# * the sensors also pick up electrical noise from the environment
# * the sensor-skin contact can get worse during a recording, leading to  a higher sensor-skin impedance and a lower signal quality
# * the desynchronization might not always happen at exactly the same location with exactly the same delay after movement intention 
#  

# ## Machine Learning

# <div class="summary"><ul>
# <li> With machine learning, we try to automatically learn which brain signal patterns indicate which intention</li>
# </ul></div>
# 
# Most brain computer interfaces try to overcome these complications, atleast in part, by machine learning. You tell the user which body part to move and record the user's brain signals. Then you use this data to automatically learn what brain signal patterns are typical for what class of movement (for that user).
# 
# The machine learning motor decoding approaches typically consist of some or all of these parts, whose order may vary:
# 
# * Frequency filters - filtering the signal to one or several frequency bands, removing some non-task-related activities and noise
# * Spatial filters - creating new virtual channels using weights for each sensor, filters should try to reconstruct a class-discriminative source signal inside the brain
# * Frequency-domain-transformation - tranforming the signal into power or power/phase representations
# * Classification - classifying the filtered transformed signal  
# 
# The spatial filters and frequency filters may be learned from training data or fixed before.
# 

# ## Convolutional neural networks

# <div class="summary"><ul>
# <li> Convolutional neural networks convolve filters smaller than the input over the input</li>
# <li> They can learn patterns at different scales/levels of abstraction</li>
# </ul></div>
# 
# Convolutional neural networks are a type of machine learning algorithm. Their basic idea is to learn useful representations at different levels of abstraction. This can be well explained with the task of classifying numbers in images. An image of a number on the lowest level, when you zoom into it, consists of edges in different directions.  At a slightly higher level, you could see combinations of these edges form shapes such as half-circles and circles. Finally, at the highest level, you can recognize the entire number. Therefore, it would be useful for a machine learning algorithm to simultaneously learn these representations at different levels - edges, circles, etc. - and learn how they are combined to form the different numbers. This is what a convolutional network typically does. At the first layer it shifts, i.e., convolves, a small filter over the entire image. This filter can, for example, learn to detect edges in different directions. The output is then a feature image, where each point tells you how strongly a certain edge is present in that part of the image. The next layer again convolves a small filter over this feature image. Since each point in the feature image already has information from a patch of the image, the filters in the second layer cover a larger region of the image than the ones in the first layer and can learn representations at the next higher level of abstraction. Repeating this for several layers, you eventually cover large enough regions of your image to have features useful for classifying the image. In the common supervised case, the network learns these filters by directly trying to use its own learned features at the last layer to classify the image. This way, a convolutional network:
# 
# * learns features itself, alleviating the burden of feature engineering
# * can learn to fairly robustly detect an object at different locations within the image
# 
# Convolutional networks have been successfully used for a wide range of tasks, including classifying images into 1000 different classes, recognizing speech from audio, detecting cells indicative of breast cancer, etc. <a data-cite="349468/XJH8MURJ"target="_blank" href="http://link.springer.com/chapter/10.1007/978-3-642-40763-5_51">(Cireşan et al., 2013)</a> <a data-cite="349468/RFJCRJFI"target="_blank" href="http://arxiv.org/abs/1404.7828">(Schmidhuber, 2015)</a> <a data-cite="349468/6C4P9CZU"target="_blank" href="http://www.nature.com/nature/journal/v521/n7553/abs/nature14539.html">(LeCun et al., 2015)</a>.
# 
# 
# 
# 
# 
# 
# To illustrate the usefulness of convolutional neural networks for classifying brain signals, we use the same trials as before. First, we show the raw signal and our filter which we will convolve over the input i.e. shift over the input (with the filter flipped from right to left), multiplying the input with the filter at each location. In this example, our filter (which the network would have needed to learn) is a sine signal with 11 Hz frequency.

# In[93]:

raw_1_topo, raw_3_topo = generate_single_sensor_raw_data(
    'data/BBCI-without-last-runs/KaUsMoSc1S001R01_ds10_1-11.BBCI.mat', 'CP3')

trial_raw_1 = np.squeeze(raw_1_topo[26])
trial_raw_3 = np.squeeze(raw_3_topo[26])
sine_signal = np.sin(np.arange(0,250,1) * 2 * np.pi * 11 / (500.0)) * 2
fig = plot_single_sensor_raw(trial_raw_3, trial_raw_1, figoptions=dict(figsize=(8,2)),
                            plotoptions=dict(linewidth=0.8))
fig.suptitle("Raw signal and filter to convolve", fontsize=16, y=1.2)
fig.axes[0].plot(sine_signal, linewidth=1.5, color="green")
fig.axes[1].plot(sine_signal, linewidth=1.5, color="green")
pyplot.legend(('raw', 'filter'), bbox_to_anchor=(1.275, 0.7))
fig.axes[0].set_yticklabels([])
None


# Now we show the output of the convolution: You again see the decrease in the variance for the right hand, just as in the bandpass-filtered version before.

# In[123]:

convolved_1 = np.convolve(trial_raw_1, sine_signal, mode="same")
convolved_3 = np.convolve(trial_raw_3, sine_signal, mode="same")
fig = plot_single_sensor_raw(convolved_1, convolved_3, figoptions=dict(figsize=(8,2)),
                            ylabel=None)
fig.suptitle("Convolved signal", fontsize=16, y=1.1)
fig.axes[0].set_yticklabels([])
None


# To differentiate between rest and right hand trial, we could now compute the variance. Since our signal is roughly zero-mean, we can also just square it and take the sum. Concretely, we take the sum of squares for a certain number of neighbouring samples, in this case for 50 neighbours. This will give us a timecourse of the squared sum (i.e., the uncentered variance) over the trial.
# Pooling several samples together also has the advantage that our results will not change much if the whole signal is shifted by a few milliseconds forward or backward. 
# After squared sum  pooling, we arrive at the following output:

# In[127]:

pooled_1 = np.sum(np.square(convolved_1).reshape(2000/50, -1), axis=1)
pooled_3 = np.sum(np.square(convolved_3).reshape(2000/50, -1), axis=1)
fig = plot_single_sensor_raw_var_samples(pooled_1, pooled_3, 
                                       figoptions=dict(figsize=(8,2)),
                                        ylabel=None)
fig.suptitle("Squared Sum Pooled Output", fontsize=16, y=1.2)

pyplot.xticks(range(0,41,10),range(0,4001,1000))
fig.axes[0].set_yticklabels([])
None


# Now we could either already use the sum of these outputs as a feature to distinguish rest from right hand (the rest trial would have a much higher sum) or we could again convolve with another filter, for example a filter looking for a lasting decrease that would match the decrease from 0 to 2000 milliseconds in the right hand trial. This approach is shown here (we show the filter in flipped form, i.e. the form it will be shifted over the input in):

# In[128]:

weights_final = np.concatenate(([1,1], np.linspace(1,-1,8), [-1] * 10))
fig = pyplot.figure(figsize=(2,2))
fig.suptitle("Final filter", y=1.2, fontsize=16)
pyplot.ylim(-1.5,1.5)
pyplot.plot(weights_final)
pyplot.xticks((0,10,20), (0, 1000,2000))
pyplot.xlabel("Time[ms]")
#fig.axes[0].set_yticklabels(['']* 3 + [0] + ['']* 3)
final_1 = np.convolve(pooled_1, weights_final[::-1], mode="valid")
final_3 = np.convolve(pooled_3, weights_final[::-1], mode="valid")
fig = plot_single_sensor_raw_var_samples(final_1, final_3,
       figoptions=dict(figsize=(8,2)), samples_per_trial=len(pooled_1),
                                        xlabel="Time[ms] (at start of filter region)", 
                                         ylabel=None)
fig.suptitle("Final filtered output", fontsize=16, y=1.2)

fig.axes[0].set_yticklabels([])
fig.axes[0].set_xticks(range(0,41,10))
pyplot.xticks(range(0,41,10),range(0,4001,1000))
None


# These outputs for rest and right hand could now be distinguished by a linear classifier, for example a softmax classifier.
# 
# In practical convolutional networks, we would typically have much more than one filter per layer/step to be able to capture different types of patterns (such as different frequencies in the first step).
# Furthermore, the convolutional filters can not only convolve over time, but also over sensors, or in case of fourier-transformed signals, over frequencies.

# In[7]:

get_ipython().run_cell_magic(u'javascript', u'', u"IPython.load_extensions('cite2c/main');")


# In[3]:

get_ipython().run_cell_magic(u'html', u'', u'<style>\ndiv.input {\nwidth: 105ex; /* about 80 chars + buffer */\n...\n}\ndiv.text_cell {\nwidth: 105ex /* instead of 100%, */\n...\n}\n\ndiv.text_cell_render {\n/*font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;*/\nfont-family: "Charis SIL", serif; /* Make non-code text serif. */\nline-height: 145%; /* added for some line spacing of text. */\nwidth: 105ex; /* instead of \'inherit\' for shorter lines */\n...\n}\n\n/* Set the size of the headers */\ndiv.text_cell_render h1 {\nfont-size: 18pt;\n}\n\ndiv.text_cell_render h2 {\nfont-size: 14pt;\n}\n\n\n\n.CodeMirror {\nfont-family: Consolas, monospace;\n}\n\n.todo {\n    background-color: hsla(120, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n.todecide {\n    background-color: hsla(240, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n\n</style>\n\n<style>\ndiv.summary  {\n    background-color: hsla(40,0%,50%,0.2);\n    color:black;\n    border-radius: 5px;\n    padding: 2px;\n    margin-top:0px;\n}\ndiv.summary ul\n{\n    list-style-type: none;\n}\n</style>\n<link rel="stylesheet" href="./motivation.css" type="text/css">\n<script async defer src="//hypothes.is/embed.js"></script>')


# In[119]:

from motor_imagery_learning.csp.train_csp import generate_filterbank 
from motor_imagery_learning.bbci_pylearn_dataset import (
    BBCIPylearnCleanFilterbankDataset, BBCIPylearnCleanDataset)
from motor_imagery_learning.mywyrm.processing import highpass_cnt

def generate_single_sensor_data(filename, chan, min_freq=75, max_freq=75, low_width=14, last_low_freq=75,high_width=2):
    filterbank_args = dict(min_freq=75, max_freq=75, low_width=6, last_low_freq=75,high_width=2)
    generate_filterbank(**filterbank_args)
    higher_dataset = BBCIPylearnCleanFilterbankDataset(filenames=filename,
                               load_sensor_names=[chan], cleaner=BBCISetNoCleaner(),
                                **filterbank_args)
    higher_dataset.load()
    classes = np.argmax(higher_dataset.y, axis=1)
    higher_topo_view = higher_dataset.get_topological_view()
    higher_class_1_topo_view = higher_topo_view[classes == 0]
    higher_class_3_topo_view = higher_topo_view[classes == 2]
    lower_dataset = BBCIPylearnCleanFilterbankDataset(filenames=filename,
                               load_sensor_names=[chan], cleaner=BBCISetNoCleaner(),
                                min_freq=11, max_freq=11, low_width=2, last_low_freq=11,high_width=2 )

    lower_dataset.load()
    lower_topo_view = lower_dataset.get_topological_view()
    lower_class_1_topo_view = lower_topo_view[classes == 0]
    lower_class_3_topo_view = lower_topo_view[classes == 2]
    return lower_class_1_topo_view, lower_class_3_topo_view, higher_class_1_topo_view, higher_class_3_topo_view

def generate_single_sensor_raw_data(filename, chan):
    raw_dataset = BBCIPylearnCleanDataset(filenames=filename,
                               load_sensor_names=[chan], cleaner=BBCISetNoCleaner(),
                               cnt_preprocessors=[(highpass_cnt, {'low_cut_off_hz': 0.5})]
        )

    raw_dataset.load()
    classes = np.argmax(raw_dataset.y, axis=1)
    raw_topo_view = raw_dataset.get_topological_view()
    raw_class_1_topo_view = raw_topo_view[classes == 0]
    raw_class_3_topo_view = raw_topo_view[classes == 2]
    return raw_class_1_topo_view, raw_class_3_topo_view


#trial nr 26
def plot_single_sensor_low(low_1_topo, low_3_topo,trial_nr,figoptions={}):
    fig, axes = pyplot.subplots(1,2,sharex=True, sharey=True, **figoptions)
    axes[0].plot(np.squeeze(low_3_topo[trial_nr]))
    axes[1].plot(np.squeeze(low_1_topo[trial_nr]))
    axes[0].set_title('Rest')
    axes[1].set_title('Right Hand')
    pyplot.xticks(range(0,2001,500), range(0,4001,1000))
    fig.text(0.5, 0.01, 'Time [ms]', ha='center', va='top', fontsize=12)
    fig.text(0.0, 0.5, 'Voltage', va='center', ha='left', fontsize=12,
             rotation='vertical')
    pyplot.tight_layout()
    fig.suptitle('CP3, 9-13 Hz', fontsize=16, y=1.08)
    return fig
    
def plot_single_sensor_high(high_1_topo, high_3_topo, trial_nr,figoptions={}):
    fig, axes = pyplot.subplots(1,2,sharex=True, sharey=True, **figoptions)
    axes[0].plot(np.squeeze(high_3_topo[trial_nr]))
    axes[1].plot(np.squeeze(high_1_topo[trial_nr]))
    axes[0].set_title('Rest')
    axes[1].set_title('Right Hand')
    pyplot.xticks(range(0,2001,500), range(0,4001,1000))
    fig.text(0.5, 0.01, 'Time [ms]', ha='center', va='top', fontsize=12)
    fig.text(0.0, 0.5, 'Voltage', va='center', ha='left', fontsize=12,
             rotation='vertical')
    pyplot.tight_layout()
    fig.suptitle('CP3, 72-78Hz', fontsize=16, y=1.08)
    return fig

def plot_single_sensor_raw(raw_1_trial, raw_3_trial, figoptions={}, plotoptions={},
                                      xlabel='Time [ms]',
                                      ylabel='Voltage'):
    fig, axes = pyplot.subplots(1,2,sharex=True, sharey=True, **figoptions)
    axes[0].plot(np.squeeze(raw_3_trial), **plotoptions)
    axes[1].plot(np.squeeze(raw_1_trial), **plotoptions)
    axes[0].set_title('Rest')
    axes[1].set_title('Right Hand')
    pyplot.xticks(range(0,2001,500), range(0,4001,1000))
    fig.text(0.5, 0.01, xlabel, ha='center', va='top', fontsize=12)
    if ylabel is not None:
        fig.text(0.0, 0.5, ylabel, va='center', ha='left', fontsize=12,
                 rotation='vertical')
    pyplot.tight_layout()
    fig.suptitle('Raw signal and filter to convolve', fontsize=16, y=1.08)
    return fig

def plot_single_sensor_raw_var_samples(raw_1_trial, raw_3_trial, 
        samples_per_trial=None, figoptions={},
                                      xlabel='Time [ms]',
                                      ylabel='Voltage'):
    
    fig, axes = pyplot.subplots(1,2,sharex=True, sharey=True, **figoptions)
    axes[0].plot(np.squeeze(raw_3_trial))
    axes[1].plot(np.squeeze(raw_1_trial))
    axes[0].set_title('Rest')
    axes[1].set_title('Right Hand')
    #pyplot.xticks(range(0,2001,500), range(0,4001,1000))
    fig.text(0.5, 0.01, xlabel, ha='center', va='top', fontsize=12)
    if ylabel is not None:
        fig.text(0.0, 0.5, ylabel, va='center', ha='left', fontsize=12,
                 rotation='vertical')
    pyplot.tight_layout()
    return fig


# In[1]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nimport os\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n#os.environ[\'CUDA_LAUNCH_BLOCKING\'] = \'1\'\n#os.environ[\'THEANO_FLAGS\'] = os.environ[\'THEANO_FLAGS\'] + ",profile=True"\n# switch to cpu\nos.environ[\'THEANO_FLAGS\'] = \'floatX=float32,device=cpu,nvcc.fastmath=True\'\n%load_ext autoreload\n%autoreload 2\n\nfrom motor_imagery_learning.bbci_pylearn_dataset import compute_power_spectra\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset\nfrom motor_imagery_learning.mywyrm.clean import  BBCISetNoCleaner\nimport numpy as np\n\nimport matplotlib\nfrom matplotlib import pyplot\nfrom matplotlib import cm\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (12.0, 5.0)\nmatplotlib.rcParams[\'font.size\'] = 7\n\nimport matplotlib.lines as mlines\nimport seaborn\nseaborn.set_style(\'darkgrid\')')

