
# coding: utf-8

# # Convolutional Networks for Movement Decoding 

# 
# <div class="motivation">
# 
# <ul>
# <li>*want to help understand information contained in EEG*
# <li>*for example help paralyzed people control sth with their thoughts*
# <li>*deep learning might help discover new insights or/and improve performance*
# </ul>
# </div>

# ## Abstract

# Inspired by recent successes of convolutional networks, we use convolutional networks to decode executed movements from EEG signals.
# We hope convolutional networks can exploit several advantages over current state of the art decoding procedures.  Convolutional networks can exploit more complex features, transfer information learned on previos subjects to the next, integrate large amounts of data and be retrained intelligently to adapt to specific changes. This might help make the decoding more reliable and thereby make brain computer interfaces more useful for patients. Also, visualizing the networks might allow to discover new or underused features helpful for the decoding.
# 
# We therefore evaluate three convolutional networks for classifiying executed movements from EEG data. They get either raw data, bandpassed data or fourier transformed data as input, all other transformations (e.g., spatial filtering) are learned by the networks.
# We collected 1040 trials in which subjects had to either rest, move their left hand, right hand or feet.
# 
# ... We show comparable performance to common spatial patterns?....
# .... Visualizations show....

# 
# 
# <div class="motivation">
# (Using deep learning on EEG data may allow several things which are difficult with simpler machine learning models: exploit more complex features, possibly even undiscovered features; transfer knowledge learned on previous subjects to classify new subjects; adapt a learned model to changing conditions. )
# 
# We (therefore) evaluate deep neural networks for classifying motor imagery(?) EEG data. [We find them to be competitive with state of the art multi-class filterbank common spatial patterns and better at blablaadpativetransfer?].
# We collected 1040 trials in which subjects had to either rest, move their left hand, right hand or feet.
# Our deep learning models are convolutional neural networks getting raw data, bandpass-filtered data and complex bandpower data as input. In some layers, we use units that square their input to exploit that a lot of information should be contained in the variance of the signal. 
# 
# We show nothing because our data is kaputt :p
# Therefore there is nothing to conclude.
# So we have added nothing to this research field, so what? :)
# (As brain computer interfaces may get closer to being used by more patients in real life, exploiting recent advances in machine learning to improve their performance seems very important.)
# </div>

# ##[Introduction](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Introduction.html)

# ##[Decoding Methods](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Methods_Algorithms.html)
# 
# ###[Common Spatial Patterns](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Methods_Common_Spatial_Patterns.html)
# ###[Filterbank Net](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Methods_Bandpassed_Net.html)
# ###[Raw Net](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Methods_Raw_Square.html)
# ###[FFT Net](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Methods_FFT.html)
# 
# TODO: Training methodology networks

# ##[Related Work](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Related_Work.html)

# ## [Visualization Methods](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Visualizations.html)

# ## [Experiments](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Experiments.html)

# ## [Results](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Results.html)
# ##Visualizations 
# ### [Filterbank Net](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/FB_Net_Visualizations.html)
# ### [Raw Net](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/Raw_Net_Visualizations.html)
# ### [FFT Net](https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/notebooks/report/FFT_Net_Visualizations.html)

# ##Conclusion

# ##Discussion

# ##Future Work

# In[14]:

from IPython.display import IFrame
IFrame(src="https://rizzoma.com/embedded/62d7538889b7b2557653dc006f2c6e87/0_b_7q8j_6cbfm/", width=800, height=400)


# 

# In[4]:

get_ipython().run_cell_magic(u'html', u'', u'<link rel="stylesheet" href="./motivation.css" type="text/css">\n<script async defer src="//hypothes.is/embed.js"></script>')

