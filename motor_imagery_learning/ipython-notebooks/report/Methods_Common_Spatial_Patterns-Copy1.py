
# coding: utf-8

# In[1]:

get_ipython().run_cell_magic(u'javascript', u'', u"IPython.load_extensions('cite2c/main');")


# In[1]:

get_ipython().run_cell_magic(u'html', u'', u'<style>\ndiv.input {\nwidth: 105ex; /* about 80 chars + buffer */\n...\n}\ndiv.text_cell {\nwidth: 105ex /* instead of 100%, */\n...\n}\n\ndiv.text_cell_render {\n/*font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;*/\nfont-family: "Charis SIL", serif; /* Make non-code text serif. */\nline-height: 145%; /* added for some line spacing of text. */\nwidth: 105ex; /* instead of \'inherit\' for shorter lines */\n...\n}\n\n/* Set the size of the headers */\ndiv.text_cell_render h1 {\nfont-size: 18pt;\n}\n\ndiv.text_cell_render h2 {\nfont-size: 14pt;\n}\n\n\n\n.CodeMirror {\nfont-family: Consolas, monospace;\n}\n\n.todo {\n    background-color: hsla(120, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\nli.todo {\n    background-color: hsla(120, 40%, 50%, 0.1);\n    border-radius: 0px;\n    padding: 0px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n\n.todecide {\n    background-color: hsla(240, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n\n</style>\n\n<style>\ndiv.summary  {\n    background-color: hsla(40,0%,50%,0.2);\n    color:black;\n    border-radius: 5px;\n    padding: 2px;\n    margin-top:0px;\n}\ndiv.summary ul\n{\n    list-style-type: none;\n}\n</style>\n<link rel="stylesheet" href="./motivation.css" type="text/css">\n<script async defer src="//hypothes.is/embed.js"></script>')


# In[2]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nimport os\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n#os.environ[\'CUDA_LAUNCH_BLOCKING\'] = \'1\'\n#os.environ[\'THEANO_FLAGS\'] = os.environ[\'THEANO_FLAGS\'] + ",profile=True"\n# switch to cpu\n#os.environ[\'THEANO_FLAGS\'] = \'floatX=float32,device=cpu,nvcc.fastmath=True\'\n%load_ext autoreload\n%autoreload 2\n\nfrom motor_imagery_learning.bbci_pylearn_dataset import compute_power_spectra\nfrom motor_imagery_learning.mywyrm.clean import BBCISetNoCleaner\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset\nimport numpy as np\n\nimport matplotlib\nfrom matplotlib import pyplot\nfrom matplotlib import cm\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (12.0, 5.0)\nmatplotlib.rcParams[\'font.size\'] = 7\n\nimport matplotlib.lines as mlines\nimport seaborn\nseaborn.set_style(\'darkgrid\')')


# In[296]:

from motor_imagery_learning.mywyrm.processing import resample_cnt, highpass_cnt, common_average_reference_cnt
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset
from motor_imagery_learning.mypylearn2.preprocessing import RestrictToTwoClasses
from wyrm.processing import apply_csp
from motor_imagery_learning.analysis.plot_util import plot_sensor_signals
from motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
from mywyrm.plot import ax_scalp,get_channelpos, CHANNEL_10_20_exact, CHANNEL_10_20
import matplotlib.cm as cmap

def get_gujo_data():
    gujo_epo = get_gujo_epo()
    restricted_sensor_names = ['Cp1', 'Cz', 'Cp2']
    few_sensor_epo = select_channels(gujo_epo, restricted_sensor_names, chanaxis=-1)
    class_pair = [0,1]#[0,1]
    two_class_epo = select_classes(few_sensor_epo, class_pair) 
    timeval_epo = select_ival(two_class_epo, [500,4000])    
    n_trials =  timeval_epo.data.shape[0]
    train_epo = select_epochs(timeval_epo, range(0, int(np.floor(n_trials * 0.9))))
    test_epo = select_epochs(timeval_epo, range(int(np.floor(n_trials * 0.9)), n_trials))
    return two_class_epo, train_epo, test_epo
    
def get_gujo_epo():
    filename = 'data/BBCI-without-last-runs/GuJoMoSc01S001R01_ds10_1-11.BBCI.mat'

    filterbank_args = dict(min_freq=11, max_freq=11, last_low_freq=11, low_width=4, high_width=4)
    sensor_names = get_C_sensors_sorted()

    dataset = BBCIPylearnCleanFilterbankDataset(filenames=filename,
           load_sensor_names=sensor_names, cleaner=BBCISetNoCleaner(),
          cnt_preprocessors=((resample_cnt, dict(newfs= 300)), 
                             (highpass_cnt, dict(low_cut_off_hz=0.5)),
                            (common_average_reference_cnt, dict())),
                                               **filterbank_args)

    dataset.load_bbci_set()
    epo = dataset.bbci_set.epo
    epo.data = np.squeeze(epo.data)
    epo.class_names = ['Right Hand', 'Left Hand', 'Rest', 'Feet']
    epo.axes = epo.axes[0:3] # remove filterband axis
    return epo


from wyrm.processing import select_classes, select_epochs, calculate_csp, select_channels, select_ival


def get_csp_filters_train_test(train_epo):
    filters, patterns, variances = calculate_csp(train_epo)
    return filters

def get_normal_and_csp_filtered_data(two_class_epo, filters):
    columns =[0,-1]
    n_trials =  two_class_epo.data.shape[0]
    test_epo_full = select_epochs(two_class_epo, range(int(np.floor(n_trials * 0.9)), n_trials))
    csp_filtered_full = apply_csp(test_epo_full, filters, columns)
    return test_epo_full, csp_filtered_full
def plot_combined_example(test_epo_full, csp_filtered_full):
    combined_sensor_trials = np.concatenate((test_epo_full.data[36], test_epo_full.data[37]))
    combined_csp_trials = np.concatenate((csp_filtered_full.data[36], csp_filtered_full.data[37]))
    combined_both = np.concatenate((combined_sensor_trials, combined_csp_trials), axis=1)
    plot_sensor_names =  test_epo_full.axes[2].tolist() + ['CSP1', 'CSP2']
    fig = plot_sensor_signals(combined_both.T, sensor_names=plot_sensor_names, figsize=(8,4))
    for ax in fig.axes:
        ax.axvline(x=1200, color='black', lw=1.25)

    pyplot.plot([0.125, 0.9], [0.43,0.43], color='grey',
        lw=1,transform=fig.transFigure, clip_on=False, linestyle="-")
    pyplot.text(0.25,1.2,"Left Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
    pyplot.text(0.75,1.2,"Right Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
    pyplot.xticks(np.arange(0,2401,300), 
          [0,1000,2000,3000] + ['4000/0'] + [1000,2000,3000,4000])
    pyplot.xlabel("milliseconds")
    fig.axes[1].axvspan(300, 1200, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
    fig.axes[2].axvspan(300, 1200, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
    fig.axes[3].axvspan(300, 1200, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
    fig.axes[4].axvspan(300, 1200, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
    fig.axes[1].axvspan(1500, 2400, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
    fig.axes[2].axvspan(1500, 2400, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
    fig.axes[3].axvspan(1500, 2400, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
    fig.axes[4].axvspan(1500, 2400, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
    
def plot_csp_example(csp_filtered_full):
    combined_csp_trials = np.concatenate((csp_filtered_full.data[36], 
                                          csp_filtered_full.data[37]))
    plot_sensor_names =  ['CSP1', 'CSP2']
    fig = plot_sensor_signals(combined_csp_trials.T, sensor_names=plot_sensor_names,
                              figsize=(8,2))
    for ax in fig.axes:
        ax.axvline(x=1200, color='black', lw=1.25)

    pyplot.text(0.25,1.2,"Left Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
    pyplot.text(0.75,1.2,"Right Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
    pyplot.xticks(np.arange(0,2401,300), 
          [0,1000,2000,3000] + ['4000/0'] + [1000,2000,3000,4000])
    
    pyplot.xlabel("milliseconds")
    
def plot_sensor_example(test_epo_full):
    combined_sensor_trials = np.concatenate((test_epo_full.data[36], 
                                             test_epo_full.data[37]))
    plot_sensor_names =  test_epo_full.axes[2].tolist()
    fig = plot_sensor_signals(combined_sensor_trials.T, sensor_names=plot_sensor_names, 
                              figsize=(8,2.5))
    for ax in fig.axes:
        ax.axvline(x=1200, color='black', lw=1.25)

    pyplot.text(0.25,1.2,"Left Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
    pyplot.text(0.75,1.2,"Right Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
    pyplot.xticks(np.arange(0,2401,300), 
          [0,1000,2000,3000] + ['4000/0'] + [1000,2000,3000,4000])
    pyplot.xlabel("milliseconds")
    
    # highlight decreases and increases
    fig.axes[1].axvspan(300, 1200, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
    fig.axes[2].axvspan(300, 1200, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
    fig.axes[1].axvspan(1500, 2400, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
    fig.axes[2].axvspan(1500, 2400, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
    return None
    
def plot_csp_filters(filters):
    chan_pos_list = CHANNEL_10_20
    figsize=(6,4)
    fig, axes = pyplot.subplots(1,2,figsize=figsize)
    ax_scalp_example(filters[:, 0], test_epo_full.axes[2], ax=axes[0], 
             colormap=cmap.coolwarm, annotate=True, chan_pos_list=chan_pos_list)
    ax_scalp_example(filters[:, -1], test_epo_full.axes[2], ax=axes[1], 
             colormap=cmap.coolwarm, annotate=True, chan_pos_list=chan_pos_list)


from scipy import interpolate
def ax_scalp_example(v, channels, 
    ax=None, annotate=False, vmin=None, vmax=None, colormap=None,
    chan_pos_list=CHANNEL_10_20, fontsize=12):
    """ scalp plot just for example.. wihtout scalp :D
    """
    if ax is None:
        ax = plt.gca()
    assert len(v) == len(channels), "Should be as many values as channels"
    if vmin is None:
        # added by me (robintibor@gmail.com)
        assert vmax is None
        vmin, vmax = -np.max(np.abs(v)), np.max(np.abs(v))
    # what if we have an unknown channel?
    points = [get_channelpos(c, chan_pos_list) for c in channels]
    for c in channels:
        assert get_channelpos(c, chan_pos_list) is not None, ("Expect " + c + " "
            "to exist in positions")
    z = [v[i] for i in range(len(points))]
    # calculate the interpolation
    x = [i[0] for i in points]
    y = [i[1] for i in points]
    # interplolate the in-between values
    xx = np.linspace(min(x), max(x), 500)
    yy = np.linspace(min(y), max(y), 500)
    xx, yy = np.meshgrid(xx, yy)
    f = interpolate.LinearNDInterpolator(list(zip(x, y)), z)
    zz = f(xx, yy)
    # draw the contour map
    ax.contourf(xx, yy, zz, 20, vmin=vmin, vmax=vmax, cmap=colormap)
    ax.set_frame_on(False)
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    # draw the channel names
    if annotate:
        for i in zip(channels, list(zip(x, y))):
            channel = i[0]
            this_x, this_y = i[1]
            if channel == 'CP1':
                horizontalalignment="right"
            elif channel == 'CP2':
                horizontalalignment="left"
            elif channel == 'Cz':
                horizontalalignment="center"
            else:
                assert False
            ax.annotate(" " + channel, (this_x, this_y),
                        horizontalalignment=horizontalalignment,
                       fontsize=fontsize)
    ax.set_aspect(1)
    
    pyplot.subplots_adjust(wspace=0.5)
    return ax
    
from motor_imagery_learning.analysis.data_generation import randomly_shifted_sines
import scipy
from numpy.random import RandomState

def get_noisy_sine_signal():
    rng = RandomState(np.uint64(hash('noise_for_csp_2')))

    sine_signal = np.sin(np.linspace(0, 2*np.pi, 100))
    #class_1_signal = np.outer([0.4,0.6], sine_signal) + rng.randn(100) * 0.1
    #class_2_signal = np.outer([0.6,0.4], sine_signal) + rng.randn(100) * 0.1
    class_1_signal = np.outer([0.6,0.8], sine_signal) + rng.randn(100) * 0.1
    class_2_signal = np.outer([0.8,0.6], sine_signal) + rng.randn(100) * 0.1

    class_1_signal = class_1_signal - np.mean(class_1_signal, axis=1, keepdims=True)
    class_2_signal = class_2_signal - np.mean(class_2_signal, axis=1, keepdims=True)
    #class_1_signal = class_1_signal / np.std(class_1_signal, axis=1, keepdims=True)
    #class_2_signal = class_2_signal / np.std(class_2_signal, axis=1, keepdims=True)
    return class_1_signal, class_2_signal

def compute_csp(class_1_signal, class_2_signal):
    #pyplot.plot(sine_signal)
    c1 = np.cov(class_1_signal)
    c2 = np.cov(class_2_signal)
    #d, v = scipy.linalg.eig(c1-c2, c1+c2)
    #d, v = scipy.linalg.eig(c1-c2, c1 +c2)
    d, v = scipy.linalg.eig(c1, c2)
    d = d.real
    # make sure the eigenvalues and -vectors are correctly sorted
    indx = np.argsort(d)
    # reverse
    indx = indx[::-1]

    d = d.take(indx)
    v = v.take(indx, axis=1)
    a = scipy.linalg.inv(v).transpose()
    
    csp_filtered_class_1 = np.dot(class_1_signal.T, v)
    csp_filtered_class_2 = np.dot(class_2_signal.T, v)
    
    return v, d, csp_filtered_class_1, csp_filtered_class_2

def plot_csp_computation_example(class_1_signal, class_2_signal, eigenvectors, 
                             csp_filtered_class_1, csp_filtered_class_2):
    pyplot.figure(figsize=(2,2))
    pyplot.scatter(class_1_signal[0], class_1_signal[1], color=seaborn.color_palette()[2])
    pyplot.scatter(class_2_signal[0], class_2_signal[1], color=seaborn.color_palette()[3])
    pyplot.xlim(-1.2, 1.2)
    pyplot.ylim(-1.2, 1.2)
    pyplot.xticks((-1.0,-0.5,0,1.0,0.5))
    pyplot.xlabel('Sensor 1')
    pyplot.ylabel('Sensor 2')
    pyplot.legend(('Class 1', 'Class 2'),loc='right', bbox_to_anchor=(1.7, 0.55))
    pyplot.plot([0,eigenvectors[0,0] * 0.5], [0,eigenvectors[1,0] * 0.5], color=seaborn.color_palette()[2])
    pyplot.plot([0,eigenvectors[0,1] * 0.5], [0,eigenvectors[1,1] * 0.5], color=seaborn.color_palette()[3] )
    pyplot.figure(figsize=(2,2))

    pyplot.scatter(csp_filtered_class_1[:,0], csp_filtered_class_1[:,1], color=seaborn.color_palette()[2])
    pyplot.scatter(csp_filtered_class_2[:,0], csp_filtered_class_2[:,1], color=seaborn.color_palette()[3])
    pyplot.xlim(-0.4, 0.4)
    pyplot.ylim(-0.4, 0.4)
    pyplot.xticks((-0.4,-0.2, 0,0.2,0.4))
    pyplot.xlabel('CSP 1')
    pyplot.ylabel('CSP 2')
    pyplot.legend(('Class 1', 'Class 2'),loc='right', bbox_to_anchor=(1.7, 0.55))


# ## Filter Bank Common Spatial Patterns

# ### Overview

# <div class="summary"><ul>
# <li> Common spatial patterns has separate feature extraction, feature selection and classification steps</li>
# <li> Can be extended to multiple filterbands by filter bank common spatial patterns</li>
# 
# </ul></div>

# *Filter bank common spatial patterns* is a decoding approach specifically designed for EEG decoding and very successful for movement decoding <a data-cite="349468/JS66EA4S"target="_blank" href="http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=5332383">(Chin et al., 2009)</a>. 
# 
# Our common spatial patterns (CSP) pipeline broadly works like this:
# 
# 1. **Extract Features:** 
#     1. Find spatial filters (weight vectors for the sensors), that approximately reconstruct important sources in the brain (this is the actual CSP part)
#     2. Apply the spatial filters to all trials
#     3. Compute the variance for each spatially filtered trial
# 2. **Select Features**:
#     1. Select features using a cross validation on the training part of our data
# 3. **Classify:** 
#     1. Classify the features using linear discriminant analysis (LDA)
#     
# 
# These steps would only work for one filterband and two classes. We extend them to multiple filterbands by extracting features from multiple filterbands (that is why it is called filter bank common spatial patterns). We extend to four classes by training all pairwise classifiers and using a voting procedure to determine the winning class.
# 
# Below, we show a graphical overview over our filter bank common spatial patterns classification pipeline to give you a preview of  what we will explain in the following sections. 
# 
# <img src="https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/svgs/architectures/csp_explanation.svg"></img>

# ### Finding Spatial Filters with Common Spatial Patterns

# <div class="summary"><ul>
# <li> Common spatial patterns tries to maximize ratio of variances of two classes</li>
# <li> Common spatial patterns learns optimal spatial filters directly in closed form </li>
# </ul></div>

# Common spatial patterns (CSP) is the current state-of-the-art method for finding spatial filters to extract features for motor imagery decoding from EEG signals <a data-cite="349468/D6IVJ2G2"target="_blank" href="http://ieeexplore.ieee.org/xpls/icp.jsp?arnumber=4408441">(Blankertz et al., 2008)</a>. CSP calculates optimal weights for the sensors so that the variance of the weighted signals is high for one class and low for the other class. Then the classes can be separated by the variances of the weighted signals. 
# 
# We will first explain common spatial patterns for a single filterband and two classes and later explain how to extend it to multiple filterbands and multiple classes. Common spatial patterns, in a BCI-context, seeks to answer this question:
# Given signals from several sensors for two classes (e.g., right hand and left hand), how to weight these sensors, so that the ratio $\frac{variance(\text{weighted signals class 1})}{variance(\text{weighted signals class 2})}$ is maximal (or minimal). The weights are called spatial filters and the weighted signals spatially filtered signals. Now you can learn to separate class 1 from class 2:
# 
# 1. Apply the spatial filters to all signals
# 2. Compute the variance for each spatially filtered signal for each trial
# 3. Learn a classifier (e.g., linear discriminant analysis) on these variances.

# ### Example

# In[35]:

two_class_epo, train_epo, test_epo = get_gujo_data()
filters = get_csp_filters_train_test(train_epo)
test_epo_full, csp_filtered_full = get_normal_and_csp_filtered_data(two_class_epo, filters)


# Let's look at a concrete example. We use 9-13 Hz bandpassed-filtered data for the sensors CP1, Cz and CP2. We look at right hand and left hand trials. We expect that the left side (i.e., CP1) shows a variance decrease for the right hand and the right side (i.e. CP2) shows a variance decrease for the left hand.
# 
# You can indeed see that in the following two trials. Note the parts highlighted in red have a higher variance than the parts highlighted in green. This is more difficult to see for the left hand.

# In[41]:

_ = plot_sensor_example(test_epo_full)
None


# CSP amplifies these variance differences. For two CSP filters computed on this dataset, you can clearly see that they are sensitive to the different sides. Positive weights are shown in red and negative weights are shown in blue, with grey inbetween at zero. Stronger colors indicate larger absolute values, e.g., a more saturated red indicates a larger positive value. In the left plot, the red weight on CP2 has the largest magnitude, in the right plot, the sensor CP1 has the largest magnitude. While plotting the filters for this dataset is informative, you have to be very careful when interpreting them as we will point out in the Visualization chapter.

# In[106]:

plot_csp_filters(filters)


# If we use these filters to weigh the sensors, we see that the variance difference between the right and the left hand trials is larger for the spatially filtered signals than for the raw sensors.

# In[114]:

plot_combined_example(test_epo_full, csp_filtered_full)


# ### Mathematical formulation

# <div class="summary"><ul>
# <li> Common spatial patterns seeks to optimize ratio of variances </li>
# <li> Generalized eigenvalue decomposition can be used to compute spatial filters </li>
# </ul></div>

# Mathematically, CSP aims to optimize the ratio of variances between two classes. 
# We assume we have multivariate signals $X_1$ and $X_2$ for two classes 1 and 2. For EEG decoding, the multivariate signals would be signals from several sensors and the classes could be left hand and right hand.
# We want to find a spatial filter $w$ that maximizes the ratio of the variances:
# 
# $$w = argmax_w \frac{variance(w^{\top} X_1)}{variance(w^{\top} X_2)} = argmax_w \frac{||w^{\top} X_1||^2}{||w^{\top} X_2||^2} = 
# argmax_w \frac{w^{\top} X_1 X_1^{\top}w}{w^{\top} X_2 X_2^{\top}w}$$ (we assume $X_1$ and $X_2$ are mean-subtracted)
# 
# Now we can solve for w using this generalized eigenvalue problem:
# 
# $$\Sigma_1w = \lambda \Sigma_2 w $$
# 
# This will not give us a single spatial filter w, but a matrix of spatial filters W, the generalized eigenvectors.
# The variance ratios of class 1 to class 2 for the signals filtered with the (generalized) eigenvector $w_i$ will be equal to the corresponding eigenvalues of the eigenvectors:
# 
# $$\frac{w_i^{\top} \Sigma_1 w_i}{w_i^{\top} \Sigma_2 w_i} = \lambda_i$$
# 
# 
# The vectors with the highest eigenvalues have a high variance for class 1 compared to class 2 and the vectors with the lowest eigenvalues have a low variance for class 1 compared to class 2. The spatial filter maximizing this ratio is then the $w_i$ with the largest eigenvalue. Conversely, the spatial filter minimizing this ratio is the $w_i$ with the smallest eigenvalue.
# 
# 
# In practice, we use the filters with the 5 largest and 5 smallest eigenvalues.
# 
# See the appendix for further explanations and a computation example.

# We explain one way to compute spatial filters with the common spatial patterns algorithm, and why this is mathematically sound, mostly following <a data-cite="349468/D6IVJ2G2"target="_blank" href="undefined">(Blankertz et al., 2008)</a>.
# 
# We assume we have multivariate signals $X_1$ and $X_2$ for two classes 1 and 2. For EEG decoding, the multivariate signals would be signals from several sensors and the classes could be left hand and right hand.
# We want to find a spatial filter $w$ that maximizes the ratio of the variances:
# 
# $$w = argmax_w \frac{variance(w^{\top} X_1)}{variance(w^{\top} X_2)} = argmax_w \frac{||w^{\top} X_1||^2}{||w^{\top} X_2||^2} = 
# argmax_w \frac{w^{\top} X_1 X_1^{\top}w}{w^{\top} X_2 X_2^{\top}w}$$ (we assume $X_1$ and $X_2$ are mean-subtracted)
# 
# We compute covariance matrices $\Sigma_{1}$ and $\Sigma_{2}$ for classes 1 and 2 by averaging over the covariances for all trials for each class and rewrite our formula as:
# $$w = argmax_w \frac{w^{\top} \Sigma_{1} w}{w^{\top} \Sigma_{2}w}$$

# We put a further restriction on the possible $w$s by setting $w^{\top} \Sigma_2w = 1$. This should not affect the result $w$ as this term is only the denominator of our ratio.
# 
# Now we can solve for w using this generalized eigenvalue problem:
# 
# $$\Sigma_1w = \lambda \Sigma_2 w $$
# 
# This will not give us a single spatial filter w, but a matrix of spatial filters W, the generalized eigenvectors. However, as we will see, this matrix also contains the desired spatial filter w that maximizes our variance ratio. 
# The variance ratios of class 1 to class 2 for the signals filtered with the (generalized) eigenvector $w_i$ will be equal to the corresponding eigenvalues of the eigenvectors:
# 
# $$\frac{w_i^{\top} \Sigma_1 w_i}{w_i^{\top} \Sigma_2 w_i} = \lambda_i$$
# 
# The vectors with the highest eigenvalues have a high variance for class 1 compared to class 2 and the vectors with the lowest eigenvalues have a low variance for class 1 compared to class 2. The spatial filter maximizing this ratio is then the $w_i$ with the largest eigenvalue. Conversely, the spatial filter minimizing this ratio is the $w_i$ with the smallest eigenvalue.
# 
# Furthermore, all the eigenvectors applied to our data lead to uncorrelated signals, i.e.:
# 
# $$W^{\top}\Sigma_1 W=\Lambda_1 \\
# W^{\top}\Sigma_2 W=\Lambda_2$$
# 
# and $$(\Lambda_1 \text{ , } \Lambda_2 \text{ diagonal })$$
# 
# In practice, we use the filters with the 5 largest and 5 smallest eigenvalues.
# 
# <span class="todecide"> put proof somewhere or atleast link to proof when you say the spatial filter maximizing this ratio is then...., using http://www.sciencedirect.com/science/article/pii/001346949190163X http://csl.anthropomatik.kit.edu/downloads/vorlesungsinhalte/MBS12_CommonSpatialPatterns.pdf http://cdn.preterhuman.net/texts/science_and_technology/artificial_intelligence/Pattern_recognition/Introduction%20to%20Statistical%20Pattern%20Recognition%202nd%20Ed%20-%20%20Keinosuke%20Fukunaga.pdf</span>
# 

# ### Computation Example

# We illustrate the computation with an artificial example. We use one source signal, a sine signal $x$ plus some gaussian noise. It is projected to two sensors, differently for each class. For class 1, the signal is: $Sensor_1 = 0.6 x$, $Sensor_2 =0.8x$. For class 2, $Sensor_1 = 0.8 x$, $Sensor_2 =0.6x$. We show the mean-subtracted signals. You can see that the sensor 2 has a slightly larger variance than sensor 1 for class 1 and vice versa for class 2.

# In[289]:

class_1_signal, class_2_signal = get_noisy_sine_signal()
pyplot.figure(figsize=(6,1))
pyplot.plot(class_1_signal.T)
pyplot.legend(('Sensor 1','Sensor 2'), loc='right', bbox_to_anchor=(1.25, 0.55))
pyplot.title("Class 1")
pyplot.figure(figsize=(6,1))
pyplot.plot(class_2_signal.T)
pyplot.legend(('Sensor 1','Sensor 2'), loc='right', bbox_to_anchor=(1.25, 0.55))
_ = pyplot.title("Class 2")


# Now we visualize how CSP spatially filters the sensor signals to make them more separable by their variance. In the first plot, you see a scatterplot of all signal values for class 1 (red) and class 2 (violet). Each dot represents one sample of the signal, and the axes show the value of sensor 1 and the value of sensor 2 for that sample. You again see that sensor 2 has a larger variance than sensor 1 for class 1, since the red dots are more spread in y-direction than in x-direction.
# The spatial filters computed by CSP are shown as two lines in red and violet. The red spatial filter maximizes the variance for class 1 and the violet one for class 2. Note, how the spatial filters are almost orthogonal to the direction where the *opposite* class has the maximum variance. For example, the red spatial filter for class 1 is almost orthogonal to the direction of maximum variance for the violet dots of class 2.
# In the second plot, we show the same scatterplots for the signal values after they are filtered with the CSP-filters. Note that the two axes for the two CSP filters correspond to the directions of the largest variance for the corresponding class.

# In[297]:

eigenvectors, eigenvalues, csp_filtered_class_1, csp_filtered_class_2 = compute_csp(
    class_1_signal, class_2_signal)
plot_csp_computation_example(class_1_signal, class_2_signal, eigenvectors, 
                             csp_filtered_class_1, csp_filtered_class_2)


# You again see the effect of the CSP filters in the plots of the CSP-filtered signals. For class 1, CSP1 has a large variance, whereas for class 2, CSP2 has a large variance.

# In[291]:

pyplot.figure(figsize=(6,1))
pyplot.plot(csp_filtered_class_1)
pyplot.legend(('CSP 1','CSP 2'), loc='right', bbox_to_anchor=(1.25, 0.55))
pyplot.title("Class 1")
pyplot.figure(figsize=(6,1))
pyplot.plot(csp_filtered_class_2)
pyplot.legend(('CSP 1','CSP 2'), loc='right', bbox_to_anchor=(1.25, 0.55))
pyplot.title("Class 2")


# In[302]:

scipy.linalg.inv(eigenvectors).T # => patterns
w1 = [ 1.76998635, 2.32757113],
w2 = [-2.32757113 , -1.74578534]

w1 / np.sqrt(np.sum(np.square(w1)))
w2 / np.sqrt(np.sum(np.square(w2)))
None


# ### Feature Selection

# <div class="summary">
# <ul>
# <li> Feature selection especially important, because spatial filters can be overfitted </li>
# <li> We use a coarse preselection mostly to speed up computation time</li>
# <li> Additionally, we use a wrapper approach evaluating feature subsets with our classifier </li>
# </ul>
# </div>

# #### Motivation
# We can get a large number of features and feature selection can be beneficial, since the spatial filters can be overfitted.
# 
# The spatial filters can overfit since we use the class labels when we compute them. Also, we could have a fairly large number of features if we used all filters.
# For example, if we have 128 sensors and 35 filterbands, we would have 128 filters per filterband and  $128*35=4480$ features. 
# 
# Many ways to reduce the number of features have been tried for motor imagery decoding, e.g. mutual information based methods <a data-cite="349468/JS66EA4S"target="_blank" href="http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=5332383">(Chin et al., Sept 2009)</a>, schemes selecting a subset of CSP filters and reapplying CSP to them  
# <a data-cite="349468/UZS396BS"target="_blank" href="http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=5420462">(Meng et al., December 2009)</a>, etc. No clear consensus is known to us regarding the best feature selection for CSP-based motor imagery decoding, so we felt free to add our own variant.
# 
# We aimed to ensure several things in our feature selection:
# 
# 1. The estimates for the feature importances should be close to what we optimize, i.e., should reflect how much it contributes to the classification accuracy
# 2. Use the information we already have from CSP, i.e., the magnitude of the eigenvalues of the filters
# 3. Reduce computation time by ignoring features that are very unlikely to be important for classification

# #### Pre-Selection
# We aim to achieve points 1 and 2 with the first steps of our feature selection pipeline:
# 
# 1. For each filterband, only keep the spatial filters with the 5 largest and the 5 smallest eigenvalues, and only compute the features for them 
# 2. Only keep the 20 filterbands with the largest mean accuracy over all classifier pairs and all train folds
# 
# Pre-selecting filters by their eigenvalues is a standard procedure for motor imagery decoding. The second step is mostly meant to save computation time. Both steps together reduce the number of features to $10 \cdot 20 = 200$. Besides saving computation time, only keeping the best 20 filterbands modestly improved accuracy in many of our tests.

# #### Main Selection
# 
# We then proceed with the main part of our feature selection. It evaluates feature subsets by training and evaluating our classifier (see next section) on the training fold with a 5-fold cross validation. Selecting features by the accuracies of feature subsets is called using a wrapper approach <a data-cite="349468/CZ29EWZZ"target="_blank" href="http://www.sciencedirect.com/science/article/pii/S000437029700043X">(Kohavi & John, December 1997)</a>. We hope directly using the classifier in the feature selection leads to robust estimates of the feature importances. Using a wrapper approach is also motivated by the fact that the authors of the  winning entry for a motor imagery dataset of the most popular BCI competition mentioned they are mainly not considering wrapper approaches due to their computational costs <a data-cite="349468/IC3IUJ6C"target="_blank" href="http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=4634130">(Ang et al., 2008)</a>.
# 
# Now, the main problem is which feature subsets to consider. Since there are $2^{\text{|Features|}}=2^{200}$ of them, it would be computationally intractable to consider all of them. We instead start with an empty feature set and iteratively add and remove those features from the set that most improve classification accuracy, until we have 80 features. Concretely, this is the addition/forward step and the removal/backward step in our feature selection:
# 
# 
# **Forward Step:**
# (Given current best feature set $F$)
# 
# 1. For each filterband $i$:
#   1. Add the two filters $f_{i,1}$,$f_{i,2}$ with the largest and smallest eigenvalue that are not in F yet
#   2. Run 5-fold cross validation with these features added, i.e. with $F \cup \{f_{i,1},f_{i,2}\}$
# 3. Add the two filters $f_{best1}$,$f_{best2}$ that led to the highest cross validation classification accuracy, $F:=F\cup \{f_{best1},f_{best2}\}$
# 
# The backward step is the same, except that that in step 1.A, the filters are removed:
# 
# 
# **Backward Step:**
# (Given current best feature set $F$)
# 
# 1. For each filterband $i$:
#   1. Remove the two filters $f_{i,1}$,$f_{i,2}$ with the largest and smallest eigenvalue that are in F
#   2. Run 5-fold cross validation with these features removed, i.e. with $F \setminus \{f_{i,1},f_{i,2}\}$
# 3. Remove the two filters $f_{worst1}$,$f_{worst2}$ whose removal led to the highest cross validation classification accuracy, $F:=F\setminus \{f_{worst1},f_{worst2}\}$
# 
# 
# We combine these steps by making 4 forward steps and 2 backward steps until we have 80 features. In our inexhaustive experiments, this combination worked best. Our approach is a variation of the plus l take away r algorithm <a data-cite="349468/2FHG2BS2"target="_blank" href="http://www2.cse.msu.edu/~rossarun/courses/sp15/cse802/papers/FerriFeatureSelection_PR1994.pdf">(Ferri et al., 1994)</a> <a data-cite="349468/2WTJTMHN"target="_blank" href="http://www.sciencedirect.com/science/article/pii/S0031320399000412">(Kudo & Sklansky, January 2000)</a>, making special use of the eigenvalues of the spatial filters.
# 
# Note that since the filters and by implication the features are possibly overfitted on the training data, our feature selection is not guaranteed to remove features unhelpful on the test data.

# ### Linear discriminant analysis

# <div class="summary"><ul>
# <li> Linear discriminant analysis (LDA) tries to find a projection that that separates the classes well<li>
# <li> LDA optimizes the ratio of class mean differences and within class variances </li>
# <li> We use shrinkage to better estimate the covariances</li>
# </ul></div>

# Once we have selected features, we need to classify them. We use linear discriminant analysis (LDA) as our classifier <a data-cite="349468/MSGRPBBX"target="_blank" href="https://en.wikipedia.org/w/index.php?title=Linear_discriminant_analysis&oldid=674793958">(, 2015-08-06T05:07:01Z)</a>. LDA tries to find a weight vector $w$ for the features that maximizes the ratio:
# $$\frac{\text{difference between class means}}{\text{variances within classes}} = \frac{mean(w^{\top} X_2) - mean(w^{\top} X_1)}{variance(w^{\top}X_2) + variance(w^{\top}X_1)}$$
# 
# where $X_1$ and $X_2$ are our features for classes 1 and 2. If you then multiply the weight vector $w$ with the features you should get high values for class 2 and low values for class 1. An artificial example is shown in the plot below, where we randomly generated features for two classes and compute the LDA vector. Each dot represents one sample and the axes represent the features. You can see that the LDA vector nicely separates the classes: If you project the samples to this vector, the red dots will be on the left upper part of the vector and the violet dots on the lower right.

# In[280]:

rng = RandomState(np.uint32(hash('ldasignals')))
cov = [[0.2, 0.1],[0.1,0.3]]
signals_class_1 = rng.multivariate_normal([-1,1], cov, size=30)
signals_class_2 = rng.multivariate_normal([1,-1], cov, size=30)
mean_1 = np.mean(signals_class_1, axis=0)
mean_2 = np.mean(signals_class_2, axis=0)
fig = pyplot.figure(figsize=(2,2))
pyplot.scatter(signals_class_1[:, 0], signals_class_1[:,1], color=seaborn.color_palette()[2])
pyplot.scatter(signals_class_2[:,0], signals_class_2[:,1], color=seaborn.color_palette()[3])


pyplot.xlabel('Feature 1')
pyplot.ylabel('Feature 2')

w = np.dot(np.linalg.pinv(cov), (mean_2 - mean_1))
scaled_w = w/ 5.

length = 0.5 # in data units
dx, dy = scaled_w
dx, dy = length * np.array([dx, dy]) / np.hypot(dx, dy)
arrow = pyplot.quiver(0, 0, dx, dy, scale=1)
pyplot.scatter(mean_1[0], mean_1[1], color='black', marker='+', linewidth=1., s=50)
pyplot.scatter(mean_2[0], mean_2[1], color='black', marker='+', linewidth=1., s=50)

pyplot.xlim(-2.5,2.5)
pyplot.ylim(-2.5,2.5)
pyplot.legend(('Class 1', 'Class 2', 'Class Means'),loc='right', 
              bbox_to_anchor=(1.9, 0.55))
pyplot.quiverkey(arrow, 1.09, 0.345, 0.145, 'LDA vector', coordinates='figure',
                 labelpos='E', fontproperties={'size':10, 'family':'sans-serif'})
pyplot.plot([-scaled_w[0],0], [-scaled_w[1], 0], linewidth=1.,
           color='black', linestyle=":")

None


# You can compute the LDA vector w as:
# 
# $$ w = \Sigma^{-1} (mean(X_2) - mean(X_1))$$
# 
# where $\Sigma$ is the covariance matrix of our features. You can then multiply this weight vector with a sample, sum it and you get large values for class 2 and small values for class 1. To have values above zero for class 2 and below zero for class 1, you still need a bias b:
# 
# $$b = -w^{\top} mean(X)$$
# 
# Linear discriminant analysis assumes that the covariances are equal for both classes, however it often works well even if this assumption is not completely fulfilled <a data-cite="349468/BN9R5JVW"target="_blank" href="http://link.springer.com/article/10.1007/s10115-006-0013-y">(Li et al., 2006/03/24)</a>
# <a data-cite="349468/3FPV8VEH"target="_blank" href="https://books.google.com/books?hl=de&lr=&id=Br33IRC3PkQC&oi=fnd&pg=PR3&dq=Pattern+Classification+Richard+O.+Duda,+Peter+E.+Hart,+David+G.+Stork&ots=2wAOHzd9Kt&sig=y3ZAYtHneVHEY3P0buNWhlh6U7U">(Duda et al., 2012)</a>.
# 
# The main difficulty in the LDA approach is the estimation of the covariance matrix. We use shrinkage regularization as described in <a data-cite="349468/5CASAZRJ"target="_blank" href="http://www.sciencedirect.com/science/article/pii/S0047259X03000964">(Ledoit & Wolf, February 2004)</a>.
# 
# LDA is widely used for classifications in BCI contexts and was therefore also our choice here, although other choices such as support vector machines or logistic regression could be equally sensible.

# ### Multiple Filterbands

# <div class="summary"><ul>
# <li> Compute spatial filters for multiple filterbands</li>
# <li> Use all features from all filterbands together to train classifier </li>
# </ul></div>
# 
# We extend CSP to multiple filterbands by computing spatial filters for multiple filterbands independently. We compute their corresponding features (i.e., the logarithms of their variances) for all spatial filters of all filterbands. Then we train a single classifier with all of these features. Our filterbands might for example be 0-4Hz, 4-8 Hz, ..., 48-52Hz. This approach is called filter bank common spatial pattern <a data-cite="349468/IC3IUJ6C"target="_blank" href="http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=4634130">(Ang et al., June 2008)</a>.

# ### Multiple Classes

# <div class="summary"><ul>
# <li> Compute all pairwise classifiers </li>
# <li> Use weighted voting </li>
# </ul></div>

# We extend our two-class filter bank CSP to multiple classes by training all pairwise classifiers and using weighted voting.
# 
# In our 4-class case, we will train classifiers 1vs2, 1vs3, 1vs4, 2vs3, 2vs4 and 3vs4. When we want to classify a trial, we sum the classifier outputs for each class from all classifiers and select the class with the highest sum. The classifier outputs are not the labels, i.e., class 1 or class 2, but the LDA outputs that could range from negative infinity (strongly predicting class 1) over zero (both classes equally probable) to positive infinity (strongly predicting class 2).
# We have to make sure that all our classifiers have outputs in a similar range. Otherwise, our sum may not be meaningful, a classifier with an output with a larger range would dominate the other classifier outputs in the sum. Therefore, we scale weights w of our trained linear discriminant analysis classifier, so that the classifier outputs for the means of class 1 and 2 are -1 and 1.
# 
# $$w_{i\_scaled} = 2\frac{w_i}{(w_i^{\top}(mean(X_1) - mean(X_2))}$$
# 
# $$\text{($mean$ computes the trial-wise mean)}$$
# 
# The equation for $w_{i\_scaled}$ forces the LDA outputs for the means to have a difference of 2. Since we compute the bias afterwards so that our overall mean is at 0, we will get -1 and 1 as outputs for the means of class 1 and 2.
# 
# Training several binary classifiers is a well-established way to deal with multi-class problems <a data-cite="349468/7MEF3GC8"target="_blank" href="http://www.sciencedirect.com/science/article/pii/S0031320311000458">(Galar et al., August 2011)</a>, also used for motor imagery decoding <a data-cite="349468/JS66EA4S"target="_blank" href="http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=5332383">(Chin et al., 2009)</a>. We preferred one vs one classifiers to one vs rest classifiers, as at least one study found them to be more robust <a data-cite="undefined"target="_blank" href="http://www.sciencedirect.com/science/article/pii/S0031320311000458">(Galar et al., August 2011)</a>. However, one vs rest might work equally well and could also be considered in the future.

# ### Parameters

# As a basic version, we use 35 non-overlapping filterbands, with 2 Hz width between 0 Hz and 30 Hz and 6 Hz width beween 30 Hz and 142 Hz. So our filterbands are: 0.5-1, 1-3, ..., 29-31, 31-37, ..., 139-145. We use all EEG sensors. Preprocessing details and variants are described in the experiments chapter. We use a fixed 500ms-4000ms time-window, as other methods to automatically determine the time window (see for example in <a data-cite="349468/D6IVJ2G2"target="_blank" href="http://ieeexplore.ieee.org/xpls/icp.jsp?arnumber=4408441">(Blankertz et al., 2008)</a>) did not improve accuracies in our tests. 
