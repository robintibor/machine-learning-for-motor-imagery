
# coding: utf-8

# In[1]:

get_ipython().run_cell_magic(u'javascript', u'', u"IPython.load_extensions('cite2c/main');")


# In[1]:

get_ipython().run_cell_magic(u'html', u'', u'<style>\ndiv.input {\nwidth: 105ex; /* about 80 chars + buffer */\n...\n}\ndiv.text_cell {\nwidth: 105ex /* instead of 100%, */\n...\n}\n\ndiv.text_cell_render {\n/*font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;*/\nfont-family: "Charis SIL", serif; /* Make non-code text serif. */\nline-height: 145%; /* added for some line spacing of text. */\nwidth: 105ex; /* instead of \'inherit\' for shorter lines */\n...\n}\n\n/* Set the size of the headers */\ndiv.text_cell_render h1 {\nfont-size: 18pt;\n}\n\ndiv.text_cell_render h2 {\nfont-size: 14pt;\n}\n\n\n\n.CodeMirror {\nfont-family: Consolas, monospace;\n}\n\n.todo {\n    background-color: hsla(120, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\nli.todo {\n    background-color: hsla(120, 40%, 50%, 0.1);\n    border-radius: 0px;\n    padding: 0px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n\n.todecide {\n    background-color: hsla(240, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n\n</style>\n\n<style>\ndiv.summary  {\n    background-color: hsla(40,0%,50%,0.2);\n    color:black;\n    border-radius: 5px;\n    padding: 2px;\n    margin-top:0px;\n}\ndiv.summary ul\n{\n    list-style-type: none;\n}\n</style>\n<link rel="stylesheet" href="./motivation.css" type="text/css">\n<script async defer src="//hypothes.is/embed.js"></script>')


# In[4]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nimport os\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n#os.environ[\'CUDA_LAUNCH_BLOCKING\'] = \'1\'\n#os.environ[\'THEANO_FLAGS\'] = os.environ[\'THEANO_FLAGS\'] + ",profile=True"\n# switch to cpu\n#os.environ[\'THEANO_FLAGS\'] = \'floatX=float32,device=cpu,nvcc.fastmath=True\'\n%load_ext autoreload\n%autoreload 2\n\nfrom motor_imagery_learning.bbci_pylearn_dataset import compute_power_spectra\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset\nimport numpy as np\n\nimport matplotlib\nfrom matplotlib import pyplot\nfrom matplotlib import cm\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (12.0, 5.0)\nmatplotlib.rcParams[\'font.size\'] = 7\n\nimport matplotlib.lines as mlines\nimport seaborn\nseaborn.set_style(\'darkgrid\')')


# ## FFT Net

# <div class="summary"><ul>
# <li> FFT net transforms the input to the frequency domain with a short-time fast Fourier transformation </li>
# <li> Input is then treated as one time-frequency image per sensor, sensors are treated as image channels</li>
# <li> FFT net has no activation functions and is a linear classifier </li>
# 
# </ul></div>
# 

# ### Overview

# The FFT net transforms the signals from the time domain to the frequency domain using a short-time fast Fourier transformation. These transformed inputs could already be reasonably well classified using a linear classifier <a data-cite="349468/KWCMBFFA"target="_blank" href="http://ieeexplore.ieee.org/xpls/icp.jsp?arnumber=1608513">(Townsend et al., April 2006)</a>. 
# In our later experiments, we will try to use only the amplitudes of the frequencies as well as the amplitudes together with the phase differences between two neighbouring time bins. 
# 
# We show our network architecture in this figure:

# <img src="https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/ipython-notebooks/svgs/architectures/fft_net_explanation.svg?cache=no003"></img>

# ### Motivation for no activation functions

# Note that our FFT net does not contain any activation function. We tried using different convolutional networks with rectified linear units <a data-cite="349468/CBHQNM99"target="_blank" href="http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=6639346">(Dahl et al., May 2013)</a>. However, for the architectures that we tested, the rectified units did not improve the performance over a network with linear activations, i.e., without activation functions (which implies our net is a linear classifier). Interestingly, our final network architecture yielded better results than simply using a softmax classifier. This may be due to the fact that the fft net implicitly regularizes the weights to be a combination of sensor, frequency and time weights. Also, it allows to use the regularization technique dropout (see the network training methods section) inbetween the layers.

# ### Short time Fourier transformed input

# We transform our raw input using Fourier transformations as follows:
# 
# 1. Divide the into timeblocks of 500 milliseconds with 250 milliseconds overlap.
# 2. Multiply each timeblock with a blackman-harris window <a data-cite="349468/4CC7PK4T"target="_blank" href="http://ccrma.stanford.edu/~jos/sasp/">(Smith, 2011)</a>, normed to L2-norm 1.
# 3. Compute fourier transform of each timeblock.
# 4. Split into amplitude and phase.
# 5. If using phases, compute differences of phases from one timeblock to the next. Pad with one zero at the start to retain the same time-dimensionality as the amplitudes (i.e. have one phase-difference value per timebin, which is set to zero for the first timebin).
# 6. Above 30 Hz, merge blocks of 3 frequency bins together without overlap to form 6 Hz bins out of the original 2 Hz bins.
# 
# Step 6 is done to have the same input as the filter bank net and common spatial patterns (they both use 2 Hz filterbands until 30 Hz, and 6 Hz filterbands above 30 Hz). Also, it already reduces dimensionality, probably making learning easier without losing a lot of information.

# ### Parameters

# We use 10 units in each layer. As indicated in the diagram, the filters in the first layer have weights for all sensors, in the second layer weights for all frequency bins and in the third layer, weights for all time bins.
# Parameters common to all nets (e.g., regularization terms) are explained in the Network Training Methods section.
