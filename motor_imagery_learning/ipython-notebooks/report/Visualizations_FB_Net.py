
# coding: utf-8

# In[1]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nimport os\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n#os.environ[\'CUDA_LAUNCH_BLOCKING\'] = \'1\'\n#os.environ[\'THEANO_FLAGS\'] = os.environ[\'THEANO_FLAGS\'] + ",profile=True"\n# switch to cpu\n#os.environ[\'THEANO_FLAGS\'] = \'floatX=float32,device=cpu,nvcc.fastmath=True\'\n%load_ext autoreload\n%autoreload 2\n\nfrom motor_imagery_learning.bbci_pylearn_dataset import (compute_power_spectra)\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset\nimport numpy as np\n\nimport matplotlib\nfrom matplotlib import pyplot\nfrom matplotlib import cm\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (8.0, 2.0)\nmatplotlib.rcParams[\'font.size\'] = 7\n\nfrom pylearn2.costs.mlp import Default\nfrom pylearn2.models.mlp import Softmax, MLP\nfrom pylearn2.termination_criteria import EpochCounter\nimport seaborn\nseaborn.set_style(\'darkgrid\')\nfrom copy import deepcopy\nfrom motor_imagery_learning.train_scripts.train_experiments import ExperimentsRunner\nfrom motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer\nfrom motor_imagery_learning.analysis.data_generation import (create_sine_signal,\n    pipeline, create_shifted_sines,put_noise, convolve_with_weight,\n    max_pool_topo_view, log_sum_pool)\nfrom motor_imagery_learning.analysis.util import create_trainer\nfrom motor_imagery_learning.mypylearn2.conv_squared import ConvSquared\nfrom motor_imagery_learning.analysis.input_reconstruction import (\n    create_rand_input, gradient_descent_input, create_input_grad_for_wanted_class_func,\n    create_input_grad_with_smooth_penalty_for_wanted_class_func, \n    reconstruct_inputs_for_class)\n\nfrom motor_imagery_learning.analysis.plot_util import (plot_misclasses_for_model, \n    plot_nlls_for_model, plot_sensor_signals)\n\nimport h5py\nimport numpy as np\nfrom pylearn2.utils import serial\nfrom pprint import pprint\nfrom  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted\nimport h5py\nfrom motor_imagery_learning.csp.train_csp import CSPTrain\nfrom motor_imagery_learning.mypylearn2.train import TrainEarlyStop\nfrom motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD\nfrom motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset\nfrom motor_imagery_learning.bbci_dataset import BBCIDataset\nfrom pylearn2.costs.mlp import Default\nfrom pylearn2.models.mlp import Softmax, MLP\nfrom pylearn2.termination_criteria import EpochCounter\nfrom sklearn.cross_validation import KFold\nfrom pylearn2.datasets.dense_design_matrix import DenseDesignMatrix\nimport numpy as np\nfrom numpy.random import RandomState\nimport theano.tensor as T\nimport theano\nfrom pylearn2.space import Conv2DSpace\nfrom pylearn2.models.mlp import ConvElemwise\nfrom theano.sandbox.cuda.dnn import dnn_conv\nfrom theano.sandbox.cuda.basic_ops import gpu_alloc_empty\nfrom motor_imagery_learning.analysis.print_results import ResultPrinter\nfrom pylearn2.utils import serial\nfrom motor_imagery_learning.analysis.data_generation import generate_single_sensor_data\nfrom motor_imagery_learning.analysis.plot.raw_signals import (\n    plot_sensor_signals_two_classes)\nfrom motor_imagery_learning.mypylearn2.pool import PoolLayer, log_sum\nfrom motor_imagery_learning.analysis.util import bps_and_freqs\nfrom motor_imagery_learning.analysis.deconv import deconv_model\nfrom motor_imagery_learning.analysis.plot_util import (\n    plot_head_signals_tight_two_signals, plot_head_signals_tight)\nfrom motor_imagery_learning.csp.print_csp_results import CSPResultPrinter\nfrom motor_imagery_learning.analysis.filterbands import (\n    sum_abs_filterband_weights, compute_center_freqs, compute_sum_abs_weights_for_csp_model)\nfrom motor_imagery_learning.analysis.plot.filterbands import show_freq_importances\nfrom motor_imagery_learning.csp.train_csp import generate_filterbank\nfrom motor_imagery_learning.train_scripts.train_with_params import (\n    create_training_object_from_file)')


# In[2]:

get_ipython().run_cell_magic(u'javascript', u'', u"IPython.load_extensions('cite2c/main');")


# In[3]:

get_ipython().run_cell_magic(u'html', u'', u'<style>\ndiv.input {\nwidth: 105ex; /* about 80 chars + buffer */\n...\n}\ndiv.text_cell {\nwidth: 105ex /* instead of 100%, */\n...\n}\n\ndiv.text_cell_render {\n/*font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;*/\nfont-family: "Charis SIL", serif; /* Make non-code text serif. */\nline-height: 145%; /* added for some line spacing of text. */\nwidth: 105ex; /* instead of \'inherit\' for shorter lines */\n...\n}\n\n/* Set the size of the headers */\ndiv.text_cell_render h1 {\nfont-size: 18pt;\n}\n\ndiv.text_cell_render h2 {\nfont-size: 14pt;\n}\n\n\n\n.CodeMirror {\nfont-family: Consolas, monospace;\n}\n\n.todo {\n    background-color: hsla(120, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n.todecide {\n    background-color: hsla(240, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n\n</style>\n\n<style>\ndiv.summary  {\n    background-color: hsla(40,0%,50%,0.2);\n    color:black;\n    border-radius: 5px;\n    padding: 2px;\n    margin-top:0px;\n}\ndiv.summary ul\n{\n    list-style-type: none;\n}\n</style>\n<link rel="stylesheet" href="./motivation.css" type="text/css">\n<script async defer src="//hypothes.is/embed.js"></script>')


# ## 

# In[8]:

CSPResultPrinter('data/models/csp/plot/full/').print_results()


# In[317]:

ResultPrinter('data/models/debug/csp-net/car/early-stop/').print_results()


# In[5]:

bhno_fb_net = serial.load(
    'data/models/debug/csp-net/car/early-stop/77.pkl').model


# In[6]:


center_freqs = compute_center_freqs(bhno_fb_net) # shd be same always, calculate just for check..
sum_abs_weights = sum_abs_filterband_weights(bhno_fb_net, center_freqs)
pyplot.figure()
show_freq_importances(center_freqs, sum_abs_weights)
_ = pyplot.xticks(np.array(center_freqs[:16:2].tolist() + 
                           center_freqs[16:].tolist()).astype(np.int))


# In[7]:

bhno_csp = serial.load('data/models/csp/plot/full/2.pkl')
sum_abs_weights = compute_sum_abs_weights_for_csp_model(bhno_csp, center_freqs)
show_freq_importances(center_freqs, sum_abs_weights)
_ = pyplot.xticks(np.array(center_freqs[:16:2].tolist() + center_freqs[16:].tolist()).astype(np.int))


# ## Combined plot

# In[7]:

bhno_fb_net = serial.load(
    'data/models/debug/csp-net/car/early-stop/77.pkl').model
spat_filters  = bhno_fb_net.layers[0].get_weights_topo().squeeze()
soft_weights = bhno_fb_net.layers[1].get_weights_topo()
filt_weights = np.sum(np.abs(soft_weights), axis=(0,1,2))
i_important_filt_weights = np.argsort(filt_weights)[::-1]


# In[389]:

n_rows = 6
n_filts = 18 
n_cols = 2 * (n_filts / n_rows)
assert n_filts % n_rows == 0
class_names = ('Right Hand', 'Left Hand', 'Rest', 'Feet')

fig, axes = pyplot.subplots(n_rows,n_cols,figsize=(12,12))
for i in range(n_filts):
    plot_i = i * 2
    i_filter = i_important_filt_weights[i]
    row, col = plot_i // n_cols, plot_i % n_cols
    ax_scalp(spat_filters[i_filter], 
             sensor_names, ax=axes[row,col], colormap=cm.bwr, annotate=False,
            chan_pos_list=CHANNEL_10_20)
    filt_soft_weight = soft_weights[:,:,:,i_filter].squeeze()
    axes[row, col].set_title('Filter ' + str(i + 1))
    axes[row, col+1].plot(center_freqs, filt_soft_weight.T, linewidth=1)
    axes[row, col+1].set_xticks(np.array(center_freqs[:16:8].tolist() + 
                       center_freqs[16::6].tolist()).astype(np.int))
    axes[row, col+1].set_yticks([0])
    axes[row, col+1].set_yticklabels([])

#axes[0, (n_cols/2)].legend(class_names, bbox_to_anchor=(0,2.))
axes[0, n_cols - 1].legend(class_names, bbox_to_anchor=(2.05,1))
None


# ### Creating patterns

# #### Replace old module paths

# In[398]:

from subprocess import check_output
replacement_pattern = 's/bbci_pylearn_dataset.BBCISetCleaner/mywyrm.clean.BBCISetCleaner/'
filepattern = 'data/models/debug/csp-net/car/early-stop/*.yaml'
command = 'sed -i ' + replacement_pattern + ' ' + filepattern
print check_output(command, shell=True)
replacement_pattern = 's/bbci_pylearn_dataset.BBCISetSecondSetCleaner/mywyrm.clean.BBCISecondSetCleaner/'
command = 'sed -i ' + replacement_pattern + ' ' + filepattern
print check_output(command, shell=True)


# In[8]:

import bbci_pylearn_dataset
bbci_pylearn_dataset.log.setLevel('DEBUG')


# In[60]:

bhno_fb_train = create_training_object_from_file(
    'data/models/debug/csp-net/car/early-stop/77.yaml')


# In[59]:

from motor_imagery_learning.analysis.sensor_positions import get_EEG_sensors_sorted
sensor_names = get_EEG_sensors_sorted()
sensor_names = [s for s in sensor_names 
                if s not in bhno_fb_train.dataset_splitter.dataset.rejected_chans]


# In[61]:

bhno_fb_train.dataset_splitter.dataset.min_freq = 60
bhno_fb_train.dataset_splitter.dataset.low_width = 6 
bhno_fb_train.dataset_splitter.dataset.max_freq = 78
bhno_fb_train.dataset_splitter.dataset.last_low_freq = 78
#bhno_fb_train.dataset_splitter.dataset.bbci_set.sensor_names = ['C3', 'C4']


# In[62]:

bhno_fb_train.dataset_splitter.ensure_dataset_is_loaded()
bhno_sets = bhno_fb_train.get_train_fitted_valid_test()
bhno_fb_train.dataset_splitter.free_memory_if_reloadable()


# In[63]:

train_topo = bhno_sets['train'].get_topological_view()

part_topo = train_topo[:,:,:,:4]

chans = part_topo.shape[1]
covariance = np.cov(part_topo.transpose(1,0,2,3).reshape(chans, -1))
covariance.shape


# In[57]:

covariance_low_freqs = deepcopy(covariance)


# In[ ]:

pyplot.imshow(covariance - covariance_low_freqs, cmap=cm.bwr, 
              vmin=-np.max(np.abs(covariance)),
             vmax=np.max(np.abs(covariance)))
pyplot.xticks([])
pyplot.yticks([])
None


# In[64]:

pyplot.imshow(covariance, cmap=cm.bwr, vmin=-np.max(np.abs(covariance)),
             vmax=np.max(np.abs(covariance)))
pyplot.xticks([])
pyplot.yticks([])
None


# In[41]:

pyplot.imshow(covariance, cmap=cm.bwr, vmin=-np.max(np.abs(covariance)),
             vmax=np.max(np.abs(covariance)))
pyplot.xticks([])
pyplot.yticks([])
None


# In[54]:

pattern = np.dot(spat_filters[13:14], covariance)


# In[56]:

from motor_imagery_learning.mywyrm.plot import ax_scalp, CHANNEL_10_20
ax_scalp(pattern[0], 
         sensor_names, ax=None, colormap=cm.bwr, annotate=False,
        chan_pos_list=CHANNEL_10_20)


# In[53]:

from motor_imagery_learning.mywyrm.plot import ax_scalp, CHANNEL_10_20
ax_scalp(spat_filters[13], 
         sensor_names, ax=None, colormap=cm.bwr, annotate=False,
        chan_pos_list=CHANNEL_10_20)


# ### Laka Potential... move this somewhere else

# In[134]:

from motor_imagery_learning.train_scripts.train_with_params import (
create_training_object_from_file)


# In[159]:

laka_train_obj = create_training_object_from_file(
    'data/models/debug/raw-square/larger-kernel-shape/early-stop/8.yaml')
laka_train_obj.dataset_splitter.dataset.sensor_names = None
laka_train_obj.dataset_splitter.ensure_dataset_is_loaded()
laka_sets = laka_train_obj.get_train_fitted_valid_test()


# In[175]:

left_topo = laka_sets['train'].get_topological_view()[laka_sets['train'].y[:,1] ==1]

right_topo = laka_sets['train'].get_topological_view()[laka_sets['train'].y[:,0] ==1]
not_left_topo = laka_sets['train'].get_topological_view()[laka_sets['train'].y[:,0] != 1]


# In[199]:

#plot_head_signals_tight(np.mean(left_topo, axis=0), get_C_sensors_sorted())
None


# In[198]:

diff_mean = np.mean(left_topo, axis=0) - np.mean(not_left_topo, axis=0)
#plot_head_signals_tight(diff_mean, get_C_sensors_sorted())
None


# In[177]:

sensor_names = get_EEG_sensors_sorted()
sensor_names = [s for s in sensor_names 
                if s not in laka_train_obj.dataset_splitter.dataset.rejected_chans]


# In[192]:

from motor_imagery_learning.analysis.plot_util import  plot_head_signals


# In[197]:

diff_mean = np.mean(left_topo, axis=0) - np.mean(not_left_topo, axis=0)
#_ = plot_head_signals(diff_mean, sensor_names,figsize=(12.5,10))
None


# In[188]:


len(sensor_names)

