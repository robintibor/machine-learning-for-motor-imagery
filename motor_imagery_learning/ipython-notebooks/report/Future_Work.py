
# coding: utf-8

# In[ ]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\n\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n# make sure to use cpu for printing results...\nos.environ[\'THEANO_FLAGS\'] = "floatX=float32,device=cpu,nvcc.fastmath=True"\n%load_ext autoreload\n%autoreload 2\nimport matplotlib\nfrom matplotlib import pyplot\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (12.0, 5.0)\nmatplotlib.rcParams[\'font.size\'] = 7\nfrom analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file\nfrom motor_imagery_learning.analysis.print_results import ResultPrinter\nfrom motor_imagery_learning.csp.print_csp_results import CSPResultPrinter\nfrom pylearn2.utils import serial\nimport numpy as np\nfrom motor_imagery_learning.analysis.plot_util import (plot_nlls_for_file, \n    plot_misclasses_for_result, plot_misclasses_for_file, plot_nlls)\nfrom motor_imagery_learning.analysis.results import ResultPool, DatasetAveragedResults\nfrom glob import glob\nimport datetime\nimport seaborn\nseaborn.set_style(\'darkgrid\')\nseaborn.set_context("notebook", rc={"lines.linewidth": 0.5})\nfrom motor_imagery_learning.analysis.results import compute_confusion_matrix\nfrom motor_imagery_learning.analysis.results import compute_confusion_matrix_csp\nfrom motor_imagery_learning.analysis.plot_util import plot_confusion_matrix')


# In[3]:

get_ipython().run_cell_magic(u'javascript', u'', u'$(\'body\').append(\'<script src="https://dl.dropboxusercontent.com/u/34637013/work/ipython-notebooks/js-for-table-sorting/jquery.tablesorter.min.js"></script>\')\n// Sort tables every 5 seconds...\nvar makeTablesSortable;\nmakeTablesSortable = function() { \n  $(\'.text_cell_render table\').not(\'.sorted_table\').tablesorter().addClass(\'sorted_table\');\n}\n\nsetInterval(makeTablesSortable, 5000);')


# In[4]:

get_ipython().run_cell_magic(u'html', u'', u'<style>\ndiv.input {\nwidth: 105ex; /* about 80 chars + buffer */\n...\n}\ndiv.text_cell {\nwidth: 105ex /* instead of 100%, */\n...\n}\n\ndiv.text_cell_render:not(.sorted_table) {\n/*font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;*/\nfont-family: "Charis SIL", serif; /* Make non-code text serif. */\nline-height: 145%; /* added for some line spacing of text. */\nwidth: 105ex; /* instead of \'inherit\' for shorter lines */\n...\n}\n\n/* Set the size of the headers */\ndiv.text_cell_render h1 {\nfont-size: 18pt;\n}\n\ndiv.text_cell_render h2 {\nfont-size: 14pt;\n}\n\n\n\n.CodeMirror {\nfont-family: Consolas, monospace;\n}\n\n.todo {\n    background-color: hsla(120, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n.todecide {\n    background-color: hsla(240, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n\n</style>\n\n<style>\ndiv.summary  {\n    background-color: hsla(40,0%,50%,0.2);\n    color:black;\n    border-radius: 5px;\n    padding: 2px;\n    margin-top:0px;\n}\ndiv.summary ul\n{\n    list-style-type: none;\n}\n</style>\n<link rel="stylesheet" href="./motivation.css" type="text/css">\n<script async defer src="//hypothes.is/embed.js"></script>')


# In[2]:

get_ipython().run_cell_magic(u'javascript', u'', u"IPython.load_extensions('cite2c/main');")


# # Future Work

# <div class="summary">
# <ul>
# <li>Swap parts of the CSP pipeline and the networks to see differences</li>
# <li>Compare differently predicted trials</li>
# <li>Create more visualizations</li>
# <li>Create nets for different features</li>
# <li>Transfer learning</li>
# <li>Try recurrent networks</li>
# </ul>
# </div>

# We can further improve the understanding of our current networks,create similar networks that might learn different features or use recurrent networks for decoding.

# ## Better Understanding

# There are several ways how we can better understand what our networks are learning.

# ### Replace parts of the pipeline

# It would be interesting to know what differences there are between the filter bank nets and the CSP approach.
# To find out if the filter bank net learns more useful spatial filters, we could let the CSP approach use the spatial filters of the filter bank net. Concretely, the CSP pipelie could not compute the 10 spatial filters with the highest and lowest eigenvalues (per filterband/class pair). Instead, for each filterband/class pair, CSP could choose the 10 spatial filters of our filter bank net that maximize and minimize the ratio of the variances between the classes. This would show if the spatial filters of our filter bank net are better than those computed by CSP. Conversely, we could also use the 80 spatial filters that are eventually picked as features by our CSP approach, fix them for the filter bank net and only train the softmax layer. This would show how much improvement we get by jointly optimizing all classes.
# 
# We could also let our raw net pool over the entire 500ms-4000ms part of the trial as CSP and the filter bank net does. This could show how important the timecourse is for the raw net. First attempts in this direction showed strongly decreased accuracies, however there can be numerous reasons for this other than the importance of the timecourse, e.g., that the gradients get too large or too small if we pool over the entire region.
# 
# 
# 
# 

# ### Compare trials

# We could also compare our trained models trialwise. We could look at trials that are predicted differently by the different approaches. We could try to find out if there is some systematic difference between which trials are easy or difficult for a given approach. We can also use visualizations to see, what part of a trial causes a network to mispredict.

# ### Different Visualizations

# We could also explore different network visualizations. Two approaches seem to be promising to us:
# 
# 1. inverting convolutional networks with convolutional networks <a data-cite="349468/A4WGZFH2"target="_blank" href="http://arxiv.org/abs/1506.02753">(Dosovitskiy & Brox, 2015-06-08)</a>
# 2. deconvolution <a data-cite="349468/U4IX8559"target="_blank" href="http://arxiv.org/abs/1311.2901">(Zeiler & Fergus, 2013-11-12)</a> <a data-cite="349468/GXWCFK46"target="_blank" href="http://arxiv.org/abs/1412.6806">(Springenberg et al., 2014-12-21)</a>
# 
# The first approach flips the training problem. Instead of predicting the class from a given output, you get the activations of a specific layer and try to predict the corresponding input. So you first train your network as before. Then, for a specific layer, you collect the outputs of the trained network in that layer for all training examples. The outputs are then the your input for a new reconstruction network. The targets will be the corresponding inputs. This allows you to supervisedly train your reconstruction network without introducing any additional prior. Once you have trained your network, you can now predict inputs for any given layer output.
# 
# The second approach tries to reconstruct, from given input, what part of that input was responsible for the output of a specific unit. We would need to adapt it to our squared sum pooling, first attempts at this did not produce very clear results, so we didn't pursue it further. However, more systematic attempts at reconstructing with deconvolution might very well still lead to informative visualizations.

# ## Evaluation on BBCI Competition

# We could also evaluate our nets on publicly available datasets such as the BBCI competition datasets. This would allow a comparison against approaches that were implemented by others on these datasets.

# ## Nets for different features

# For now, our nets mostly seem to use standard features for motor execution/imagery decoding. We could think of architectures that could exploit lesser-used features such as phase-locking values. To systematically do this, we could:
# 
# 1. Create artificial data, where a certain feature is informative
# 2. Create a network, that should be able to learn the feature
# 3. Check how to find out if the network has learned that feature on artificial data
# 4. Once the network can learn the feature, train it on real data and see if it still uses the feature
# 

# ## Transfer learning

# We could try to pretrain a network on many subjects and then to finetune it for a specific subject. We could try to find out if that improves accuracy and/or training time. Also, visualizing networks trained on all subjcets might lead to insights what features are subject-independent.

# ## Recurrent networks

# We can try to use recurrent networks for decoding our EEG signal. Recurrent networks have seen an increase in interest and usage recently, using improved training procedures and architectures <a data-cite="349468/KHPSTR8B"target="_blank" href="http://arxiv.org/abs/1312.6026">(Pascanu et al., 2013-12-20)</a> <a data-cite="349468/KBES8KBC"target="_blank" href="http://ieeexplore.ieee.org/xpl/freeabs_all.jsp?arnumber=6795963">(Hochreiter & Schmidhuber, November 1997)</a> <a data-cite="349468/2EEQB8P4"target="_blank" href="http://arxiv.org/abs/1211.5063">(Pascanu et al., 2012-11-21)</a>. Therefore, we think they woudl be worth trying out on our naturally continuous EEG data. Recurrent networks can learn information from the continuous signal that any approach using just the trials without their temporal order could not learn. For example, a recurrent net could learn to ignore a sensor whose variance has increased too much or learn how the location of a spatial pattern shifts over time.
