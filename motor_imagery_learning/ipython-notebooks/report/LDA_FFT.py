
# coding: utf-8

# In[5]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\nimport site\nsite.force_global_eggs_after_local_site_packages()\n#site.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\n#site.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nimport os\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n#os.environ[\'CUDA_LAUNCH_BLOCKING\'] = \'1\'\n#os.environ[\'THEANO_FLAGS\'] = os.environ[\'THEANO_FLAGS\'] + ",profile=True"\n# switch to cpu\n#os.environ[\'THEANO_FLAGS\'] = \'floatX=float32,device=cpu,nvcc.fastmath=True\'\n%load_ext autoreload\n%autoreload 2\n\nfrom motor_imagery_learning.bbci_pylearn_dataset import (compute_power_spectra)\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset\nimport numpy as np\n\nimport matplotlib\nfrom matplotlib import pyplot\nfrom matplotlib import cm\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (8.0, 2.0)\nmatplotlib.rcParams[\'font.size\'] = 7\n\nfrom pylearn2.costs.mlp import Default\nfrom pylearn2.models.mlp import Softmax, MLP\nfrom pylearn2.termination_criteria import EpochCounter\nimport seaborn\nseaborn.set_style(\'darkgrid\')\nfrom copy import deepcopy\nfrom motor_imagery_learning.train_scripts.train_experiments import ExperimentsRunner\nfrom motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer\nfrom motor_imagery_learning.analysis.data_generation import (create_sine_signal,\n    pipeline, create_shifted_sines,put_noise, convolve_with_weight,\n    max_pool_topo_view, log_sum_pool)\nfrom motor_imagery_learning.analysis.util import create_trainer\nfrom motor_imagery_learning.mypylearn2.conv_squared import ConvSquared\nfrom motor_imagery_learning.analysis.input_reconstruction import (\n    create_rand_input, gradient_descent_input, create_input_grad_for_wanted_class_func,\n    create_input_grad_with_smooth_penalty_for_wanted_class_func, \n    reconstruct_inputs_for_class)\n\nfrom motor_imagery_learning.analysis.plot_util import (plot_misclasses_for_model, \n    plot_nlls_for_model, plot_sensor_signals)\n\nimport h5py\nimport numpy as np\nfrom pylearn2.utils import serial\nfrom pprint import pprint\nfrom  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted\nimport h5py\nfrom motor_imagery_learning.csp.train_csp import CSPTrain\nfrom motor_imagery_learning.mypylearn2.train import TrainEarlyStop\nfrom motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD\nfrom motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset\nfrom motor_imagery_learning.bbci_dataset import BBCIDataset\nfrom pylearn2.costs.mlp import Default\nfrom pylearn2.models.mlp import Softmax, MLP\nfrom pylearn2.termination_criteria import EpochCounter\nfrom sklearn.cross_validation import KFold\nfrom pylearn2.datasets.dense_design_matrix import DenseDesignMatrix\nimport numpy as np\nfrom numpy.random import RandomState\nimport theano.tensor as T\nimport theano\nfrom pylearn2.space import Conv2DSpace\nfrom pylearn2.models.mlp import ConvElemwise\nfrom theano.sandbox.cuda.dnn import dnn_conv\nfrom theano.sandbox.cuda.basic_ops import gpu_alloc_empty\nfrom motor_imagery_learning.analysis.print_results import ResultPrinter\nfrom pylearn2.utils import serial\nfrom motor_imagery_learning.analysis.data_generation import generate_single_sensor_data\nfrom motor_imagery_learning.analysis.plot.raw_signals import (\n    plot_sensor_signals_two_classes)\nfrom motor_imagery_learning.mypylearn2.pool import PoolLayer, log_sum\nfrom motor_imagery_learning.analysis.util import bps_and_freqs\nfrom motor_imagery_learning.analysis.deconv import deconv_model\nfrom motor_imagery_learning.analysis.plot_util import plot_head_signals_tight_two_signals\nfrom motor_imagery_learning.train_scripts.train_with_params import  create_training_object_from_file')


# In[6]:


import sklearn
sklearn.__version__


# In[ ]:




# In[ ]:

# sed -i s/bbci_pylearn_dataset.BBCISetCleaner/mywyrm.clean.BBCISetCleaner/ *.yaml
# sed -i s/bbci_pylearn_dataset.BBCISetSecondSetCleaner/mywyrm.clean.BBCISecondSetCleaner/ *.yaml


# In[7]:


train_obj = create_training_object_from_file('data/models/fft/merged-freqs/spatial-freq-time/until-144/debug-fix-import/1.yaml')
train_obj.dataset_splitter.ensure_dataset_is_loaded()
sets = train_obj.get_train_fitted_valid_test()


# In[5]:

ResultPrinter('data/models/fft/merged-freqs/spatial-freq-time/until-144/early-stop//').print_results()


# In[8]:

from motor_imagery_learning.mywyrm.processing import lda_train_scaled

train_topo = sets['train'].get_topological_view()


# In[3]:

import sklearn.lda
clf = sklearn.lda.LDA(solver='lsqr', shrinkage='auto', priors=None, n_components=None, store_covariance=True, tol=0.0001)


# In[9]:

X = np.array([[-1, -1], [-2, -1], [-3, -2], [1, 1], [2, 1], [3, 2]])
y = np.array([1, 1, 1, 2, 2, 2])
clf = sklearn.lda.LDA(solver='lsqr', shrinkage='auto', 
                      priors=None, n_components=None, store_covariance=True, tol=0.0001)
clf.fit(X, y)
print(clf.predict([[-0.8, -1]]))


# In[38]:

get_ipython().run_cell_magic(u'time', u'', u"rng = RandomState(np.uint32(hash('lda_test')))\ntrials = 1000\nfeatures = 128 * 15\nX = rng.randn(trials,features)\ny = rng.binomial(1,0.5, size=trials)\nclf = sklearn.lda.LDA(solver='svd', #shrinkage='auto', \n                      priors=None, n_components=None, store_covariance=True, tol=0.0001)\n\nclf.fit(X, y)")


# In[59]:

np.prod(train_topo.shape[1:])


# In[73]:

X = train_topo.reshape(train_topo.shape[0], -1)
num_trials = np.round(X.shape[0] * 0.9)
X = X[:, 0:2000]
y = np.argmax(sets['train'].y, axis=1)

clf = sklearn.lda.LDA(solver='svd', #shrinkage='auto', 
                      priors=None, n_components=None, store_covariance=True, tol=0.0001)
clf2 = sklearn.lda.LDA(solver='lsqr', shrinkage='auto', 
                      priors=None, n_components=None, store_covariance=True, tol=0.0001)

clf2.fit(X[0:num_trials], y[0:num_trials])

print ("Train", clf2.score(X[:num_trials], y[:num_trials]))

print ("Test", clf2.score(X[num_trials:], y[num_trials:]))


# In[57]:

clf.score(X[0:num_trials], y[0:num_trials])


# In[ ]:

X = np.array([[-1, -1], [-2, -1], [-3, -2], [1, 1], [2, 1], [3, 2]])
y = np.array([1, 1, 1, 2, 2, 2])
clf = sklearn.lda.LDA(solver='lsqr', shrinkage='auto', 
                      priors=None, n_components=None, store_covariance=True, tol=0.0001)
clf.fit(X, y)
print(clf.predict([[-0.8, -1]]))

