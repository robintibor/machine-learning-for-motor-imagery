
# coding: utf-8

# In[ ]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\n\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n# make sure to use cpu for printing results...\nos.environ[\'THEANO_FLAGS\'] = "floatX=float32,device=cpu,nvcc.fastmath=True"\n%load_ext autoreload\n%autoreload 2\nimport matplotlib\nfrom matplotlib import pyplot\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (12.0, 5.0)\nmatplotlib.rcParams[\'font.size\'] = 7\nfrom analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file\nfrom motor_imagery_learning.analysis.print_results import ResultPrinter\nfrom motor_imagery_learning.csp.print_csp_results import CSPResultPrinter\nfrom pylearn2.utils import serial\nimport numpy as np\nfrom motor_imagery_learning.analysis.plot_util import (plot_nlls_for_file, \n    plot_misclasses_for_result, plot_misclasses_for_file, plot_nlls)\nfrom motor_imagery_learning.analysis.results import ResultPool, DatasetAveragedResults\nfrom glob import glob\nimport datetime\nimport seaborn\nseaborn.set_style(\'darkgrid\')\nseaborn.set_context("notebook", rc={"lines.linewidth": 0.5})\nfrom motor_imagery_learning.analysis.results import compute_confusion_matrix\nfrom motor_imagery_learning.analysis.results import compute_confusion_matrix_csp\nfrom motor_imagery_learning.analysis.plot_util import plot_confusion_matrix')


# In[2]:

get_ipython().run_cell_magic(u'javascript', u'', u'$(\'body\').append(\'<script src="https://dl.dropboxusercontent.com/u/34637013/work/ipython-notebooks/js-for-table-sorting/jquery.tablesorter.min.js"></script>\')\n// Sort tables every 5 seconds...\nvar makeTablesSortable;\nmakeTablesSortable = function() { \n  $(\'.text_cell_render table\').not(\'.sorted_table\').tablesorter().addClass(\'sorted_table\');\n}\n\nsetInterval(makeTablesSortable, 5000);')


# In[3]:

get_ipython().run_cell_magic(u'html', u'', u'<style>\ndiv.input {\nwidth: 105ex; /* about 80 chars + buffer */\n...\n}\ndiv.text_cell {\nwidth: 105ex /* instead of 100%, */\n...\n}\n\ndiv.text_cell_render:not(.sorted_table) {\n/*font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;*/\nfont-family: "Charis SIL", serif; /* Make non-code text serif. */\nline-height: 145%; /* added for some line spacing of text. */\nwidth: 105ex; /* instead of \'inherit\' for shorter lines */\n...\n}\n\n/* Set the size of the headers */\ndiv.text_cell_render h1 {\nfont-size: 18pt;\n}\n\ndiv.text_cell_render h2 {\nfont-size: 14pt;\n}\n\n\n\n.CodeMirror {\nfont-family: Consolas, monospace;\n}\n\n.todo {\n    background-color: hsla(120, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n.todecide {\n    background-color: hsla(240, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n\n</style>\n\n<style>\ndiv.summary  {\n    background-color: hsla(40,0%,50%,0.2);\n    color:black;\n    border-radius: 5px;\n    padding: 2px;\n    margin-top:0px;\n}\ndiv.summary ul\n{\n    list-style-type: none;\n}\n</style>\n<link rel="stylesheet" href="./motivation.css" type="text/css">\n<script async defer src="//hypothes.is/embed.js"></script>')


# In[4]:

get_ipython().run_cell_magic(u'javascript', u'', u"IPython.load_extensions('cite2c/main');")


# # Discussion

# <div class="summary">
# <ul>
# <li>First step towards using convolutional nets for EEG decoding </li>
# <li>Accuracy increase over CSP should be interpreted cautiously </li>
# <li>Online performance may be significantly worse for raw net </li>
# <li>Runtimes might prohibit real-life application at the moment </li>
# <li>Visualized features already known </li>
# <li>Nets might benefit from larger amounts of data </li>
# </ul>
# </div>

# All in all, we have made a first step towards using convolutional networks for eeg decoding. While the networks already achieve promising accuracies, one should be aware of several limitations for their accuracies and their visualizations.
# 
# 
# ## More Robust CSP Variants
# 
# First of all, there are more robust variants of Common Spatial Patterns, that we did not compare to. For an overview and a common framework for these robust variants, see <a data-cite="349468/URTHQ4N4"target="_blank" href="http://ieeexplore.ieee.org/xpls/icp.jsp?arnumber=6662468">(Samek et al., 2014)</a>. Since our accuracy improvements mostly come from a few low-performing datasets, using a more robust version of CSP might already close that gap. 
# 
# ## Usage for Online BCI 
# 
# Second of all, we only evaluated on an offline dataset and did not perform an online evaluation. So we cannot make any estimation of the online performance of our approaches. The raw nets might run into several problems if we want to use them for an asynchronous BCI. Typically, in an asynchronous BCI one would shift a window over the signal to cut out trials. So, in our case, the raw net would predict on 4-second windows, which are incrementally shifted over the signal. This leads to two problems:
# 
# * the timecourses over the trials would not be valid anymore
# * the potential at the start can only be used at the start of the motor imagery/execution
# 
# This might strongly hurt the performance of the raw net.
# 
# ## Running times
# 
# Third of all, the running times, atleast for the filterbank net, rule out using the filterbank net for any scenario where you need to quickly train your network. For example, if you want to make a recording, train and then immediately use the BCI, you would need a 3 hour break after the recording.
# 
# ## Features
# 
# In our visualizations, we have only shown features which are already known to be useful for decoding motor execution/motor imagery:
# 
# * event related synchronization/desynchronization
# * (possibly motor related) potentials
# 
# We could not discover a completely new feature. While our raw net uses the timecourse of the trial, which is typically not used, this also limits its usage for online BCIs as discussed before.
# 
# ## FFT Net underoptimized
# 
# One should also be cautious when interpreting the much worse performance of the FFT net. During the course of this thesis, we have spent considerably more time improving the raw net than the FFT net. It is completely possible that a different architecture and/or different training methods might lead to similar accuracies for a net with fourier-transformed input.

# ## Benefit from larger datasets

# Despite all limitations, there are also scenarios in which our nets might strongly improve their accuracy. For much larger datasets, their flexibility might allow them to improve their accuracy more than the standard CSP approach or even the more robust CSP approaches.
