
# coding: utf-8

# In[ ]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\n\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n# make sure to use cpu for printing results...\nos.environ[\'THEANO_FLAGS\'] = "floatX=float32,device=cpu,nvcc.fastmath=True"\n%load_ext autoreload\n%autoreload 2\nimport matplotlib\nfrom matplotlib import pyplot\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (12.0, 5.0)\nmatplotlib.rcParams[\'font.size\'] = 7\nfrom analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file\nfrom motor_imagery_learning.analysis.print_results import ResultPrinter\nfrom motor_imagery_learning.csp.print_csp_results import CSPResultPrinter\nfrom pylearn2.utils import serial\nimport numpy as np\nfrom motor_imagery_learning.analysis.plot_util import (plot_nlls_for_file, \n    plot_misclasses_for_result, plot_misclasses_for_file, plot_nlls)\nfrom motor_imagery_learning.analysis.results import ResultPool, DatasetAveragedResults\nfrom glob import glob\nimport datetime\nimport seaborn\nseaborn.set_style(\'darkgrid\')\nseaborn.set_context("notebook", rc={"lines.linewidth": 0.5})\nfrom motor_imagery_learning.analysis.results import compute_confusion_matrix\nfrom motor_imagery_learning.analysis.results import compute_confusion_matrix_csp\nfrom motor_imagery_learning.analysis.plot_util import plot_confusion_matrix')


# In[ ]:

get_ipython().run_cell_magic(u'javascript', u'', u'$(\'body\').append(\'<script src="https://dl.dropboxusercontent.com/u/34637013/work/ipython-notebooks/js-for-table-sorting/jquery.tablesorter.min.js"></script>\')\n// Sort tables every 5 seconds...\nvar makeTablesSortable;\nmakeTablesSortable = function() { \n  $(\'.text_cell_render table\').not(\'.sorted_table\').tablesorter().addClass(\'sorted_table\');\n}\n\nsetInterval(makeTablesSortable, 5000);')


# In[1]:

get_ipython().run_cell_magic(u'html', u'', u'<style>\ndiv.input {\nwidth: 105ex; /* about 80 chars + buffer */\n...\n}\ndiv.text_cell {\nwidth: 105ex /* instead of 100%, */\n...\n}\n\ndiv.text_cell_render:not(.sorted_table) {\n/*font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;*/\nfont-family: "Charis SIL", serif; /* Make non-code text serif. */\nline-height: 145%; /* added for some line spacing of text. */\nwidth: 105ex; /* instead of \'inherit\' for shorter lines */\n...\n}\n\n/* Set the size of the headers */\ndiv.text_cell_render h1 {\nfont-size: 18pt;\n}\n\ndiv.text_cell_render h2 {\nfont-size: 14pt;\n}\n\n\n\n.CodeMirror {\nfont-family: Consolas, monospace;\n}\n\n.todo {\n    background-color: hsla(120, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n.todecide {\n    background-color: hsla(240, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n\n</style>\n\n<style>\ndiv.summary  {\n    background-color: hsla(40,0%,50%,0.2);\n    color:black;\n    border-radius: 5px;\n    padding: 2px;\n    margin-top:0px;\n}\ndiv.summary ul\n{\n    list-style-type: none;\n}\n</style>\n<link rel="stylesheet" href="./motivation.css" type="text/css">\n<script async defer src="//hypothes.is/embed.js"></script>')


# In[2]:

get_ipython().run_cell_magic(u'javascript', u'', u"IPython.load_extensions('cite2c/main');")


# # Conclusion

# <div class="summary">
# <ul>
# <li>Raw/Filterbank nets slightly exceed CSP's accuracy</li>
# <li>Visualizations look plausible</li>
# <li>Good nets still more than 3 times slower than CSP</li>
# </ul>
# </div>
# 

# We have shown that specialized convolutional networks can be used to decode motor executions with good performance. The nets:
#  
#  * match and even slightly exceed CSP's accuracy
#  * show plausible visualizations
#  
# 
# ## Reasonable Results
# The raw net and the filterbank net have the best accuracies. For our best variant, using only the C sensors and a sampling rate of 300 Hz:
# 
# * raw net and filterbank net are 1-3% better than CSP (regarding accuracy)
# * raw net is 3-4 times slower than CSP
# * filterbank net is 12 times slower than CSP
# 
# A more detailed look shows that most of the accuracy improvement comes from low-performing datasets, especially for the raw net, meaning the raw net is fairly robust. The FFT net is not competitive for any of our variants.
# 
# ## Reasonable Visualizations
# 
# The visualizations match expectations of where the class-discriminative information is, both for the sensor locations and the frequency bands. 
# 
# For the filterbank net, we can show :
# 
# * spatial filters and softmax weights (per frequency band per class)
# * spatial patterns and softmax patterns
# 
# While all are not unambiguously interpretable, the spatial filters and the spatial patterns both show clear spatial structures. The softmax weights and patterns show which spatial filters/patterns contribute positively or negatively for the different frequency bands for the different classes. These visualizations also look plausible.
# 
# For the raw net, we can show:
# 
# * the combined temporal + spatial weights
# * the softmax weights (timecourse per spatial filter per class)
# * inputs reconstructions using the gradient
# 
# The combined weights show that the raw net can learn to use oscillations in certain frequencies at certain locations. The softmax weights show that the net is able to learn a timecourse over the trial time, for example ignoring the start of the trial for some weights matching oscillations. The input reconstructions also show oscillations at certain locations together with a timecourse over the trial. We could also show that the raw net has learned to use a potential at the start of the trial. 
# 
# For the FFT net, we can show:
#  
#  * combined weights
#  * combined patterns
#  
# The combined weights and patterns both match our expectations. We see decreases and increases in those frequency bands and for those sensors where we would expect them.
