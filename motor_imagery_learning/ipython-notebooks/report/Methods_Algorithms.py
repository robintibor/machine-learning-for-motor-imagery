
# coding: utf-8

# In[3]:

get_ipython().run_cell_magic(u'javascript', u'', u"IPython.load_extensions('cite2c/main');")


# In[1]:

get_ipython().run_cell_magic(u'html', u'', u'<style>\ndiv.input {\nwidth: 105ex; /* about 80 chars + buffer */\n...\n}\ndiv.text_cell {\nwidth: 105ex /* instead of 100%, */\n...\n}\n\ndiv.text_cell_render {\n/*font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;*/\nfont-family: "Charis SIL", serif; /* Make non-code text serif. */\nline-height: 145%; /* added for some line spacing of text. */\nwidth: 105ex; /* instead of \'inherit\' for shorter lines */\n...\n}\n\n/* Set the size of the headers */\ndiv.text_cell_render h1 {\nfont-size: 18pt;\n}\n\ndiv.text_cell_render h2 {\nfont-size: 14pt;\n}\n\n\n\n.CodeMirror {\nfont-family: Consolas, monospace;\n}\n\n.todo {\n    background-color: hsla(120, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n.todecide {\n    background-color: hsla(240, 40%, 50%, 0.1);\n    border-radius: 5px;\n    padding: 2px;\n    padding-left: 3px;\n    padding-bottom: 3px;\n    color: hsla(0, 0%, 0%,0.5)\n}\n\n</style>\n\n<style>\ndiv.summary  {\n    background-color: hsla(40,0%,50%,0.2);\n    color:black;\n    border-radius: 5px;\n    padding: 2px;\n    margin-top:0px;\n}\ndiv.summary ul\n{\n    list-style-type: none;\n}\n</style>\n<link rel="stylesheet" href="./motivation.css" type="text/css">\n<script async defer src="//hypothes.is/embed.js"></script>')


# In[2]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nimport os\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n#os.environ[\'CUDA_LAUNCH_BLOCKING\'] = \'1\'\n#os.environ[\'THEANO_FLAGS\'] = os.environ[\'THEANO_FLAGS\'] + ",profile=True"\n# switch to cpu\n#os.environ[\'THEANO_FLAGS\'] = \'floatX=float32,device=cpu,nvcc.fastmath=True\'\n%load_ext autoreload\n%autoreload 2\n\nfrom motor_imagery_learning.bbci_pylearn_dataset import compute_power_spectra, BBCISetNoCleaner\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset\nimport numpy as np\n\nimport matplotlib\nfrom matplotlib import pyplot\nfrom matplotlib import cm\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (12.0, 5.0)\nmatplotlib.rcParams[\'font.size\'] = 7\n\nimport matplotlib.lines as mlines\nimport seaborn\nseaborn.set_style(\'darkgrid\')')


# # Decoding Methods

# <div class="summary"><ul>
# <li> We have four decoding algorithms </li>
# <li> They differ in which steps are learned independently, are learned jointly or are fixed </li>
# </ul></div>

# Most decoding algorithms to classify motor imagery from EEG signals use the known relationship between the brain signal and different (executed or imagined) movements that we mentioned in the introduction:
# A source inside the brain will oscillate weaker or stronger in different frequency bands. In order to classify a signal, the decoding algorithms typically:
# 
# 1. filter the signal to one or several frequency bands
# 2. reconstruct the source(s) by multiplying the EEG sensors with different weight vectors (spatial filters)
# 3. transform the signal to bandpower, e.g., by squaring, or to a bandpower-phase representation, e.g., by fast fourier transformation
# 4. classify these features
# 
# The order of steps 1-3 can be changed in some approaches. 
# 
# We present a broad overview over the approaches presented in this thesis to give a first idea about their differences. We will explain all the steps of the approaches in the following sections.
# 
# **Filter bank common spatial patterns**:
# 
#   * uses several fixed bandpass filters.
#   * independently learns class-discriminative spatial filters for each bandpass filter.
#   * uses the variance to transform to bandpower.
#   * independently learns several classifiers.
# 
# **Filter bank net**: 
# 
# * also uses several fixed bandpass filters.
# * uses squaring to transform to bandpower.
# * jointly learns spatial filters for all bandpass filters together with a classifier.
# 
# **Raw net**: 
# 
# * gets raw data as input
# * also uses squaring to transform to bandpower
# * jointly  learns temporal filters and spatial filters together with a classifier.
# 
# **FFT net**: 
# 
# * uses short-time Fourier transformations on the input.
# * learns a classifier on the resulting bandpower or bandpower-phase features.
