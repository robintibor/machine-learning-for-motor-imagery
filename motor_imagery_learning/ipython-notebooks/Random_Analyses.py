
# coding: utf-8

# In[3]:

import os
os.sys.path.insert(0,'/lokal/schirrmr/motor-imagery/code/') 
os.sys.path.insert(0,'/lokal/schirrmr/motor-imagery/code/motor_imagery_learning/') 
get_ipython().magic(u'cd /lokal/schirrmr/motor-imagery/code/motor_imagery_learning/')
#%cd /home/robintibor/farm04/motor-imagery/code/motor_imagery_learning
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7
from pylearn2.utils import serial
import numpy as np
import h5py
from pylearn2.config import yaml_parse
from analysis.util import reshape_and_get_predictions, load_preprocessed_datasets, check_prediction_correctness
from analysis.sensor_positions import get_C_sensors_sorted
import itertools
from copy import deepcopy
from motor_eeg_dataset import RawMotorEEGDataset


# ## Splitting trials 

# In[5]:

debug_set = RawMotorEEGDataset('data/motor-imagery-tonio/MaGlMoSc2S001R01_ds10_1-12_autoclean_trials_ival_0_4000.mat',
                            'auto_clean_trials_150Hz', 'targets', 0, 67, limits=None, sensor_names=get_C_sensors_sorted(), 
                             time_bin_start=None,time_bin_stop=None)


# In[11]:

debug_topo_view = debug_set.get_topological_view()


# ## Investigating early stop cv bug

# In[465]:

np.mean((np.mean(valid_misclasses), np.mean(test_misclasses)))


# In[473]:

pyplot.figure()
pyplot.plot(1/np.linspace(0,100,100))
pyplot.plot(0.95**np.linspace(0,100,100))


# In[480]:

results_gujo = serial.load('data/gpu-models/raw-data-early-stop-no-reset/cross-validation/early-stop/5.result.pkl')
#results_gujo = serial.load('data/gpu-models/debug/early-stop-learning-rate/cross-validation/early-stop/6.result.pkl')
test_misclasses = []
valid_misclasses = []
for fold_i in xrange(len(results_gujo)):
    result_gujo = results_gujo[fold_i]

    pyplot.figure()
    pyplot.plot(result_gujo.monitor_channels['train_y_misclass'].val_record)
    pyplot.plot(result_gujo.monitor_channels['valid_y_misclass'].val_record)
    pyplot.plot(result_gujo.monitor_channels['test_y_misclass'].val_record)
    test_misclasses.append(result_gujo.monitor_channels['test_y_misclass'].val_record[(len(
        result_gujo.monitor_channels['test_y_misclass'].val_record) / 2) - 1])
    valid_misclasses.append(result_gujo.monitor_channels['valid_y_misclass'].val_record[(len(
        result_gujo.monitor_channels['valid_y_misclass'].val_record) / 2) - 1])
    
    pyplot.legend(('train', 'valid', 'test'))


# ## Checking transfer results

# In[425]:

results_gujo = serial.load('data/gpu-models/conv-nets-transfer/transfer/early-stop/5.result.pkl')


# In[433]:

result_gujo = results_gujo[3]
pyplot.figure()
pyplot.plot(result_gujo.monitor_channels['test_y_misclass'].val_record)
get_available_sensors_topo


# ## Analyzing Raw Filters

# In[399]:

gujomodel = serial.load('data/gpu-models/conv-nets-raw-data-many-convs-learning-rate/early-stop/181.pkl')
mavomodel = serial.load('data/gpu-models/conv-nets-raw-data-many-convs-learning-rate/early-stop/183.pkl')
robinmodel = serial.load('data/gpu-models/conv-nets-raw-data-many-convs-learning-rate/early-stop/185.pkl')
mavomodel2 = serial.load('data/gpu-models/conv-nets-raw-data-many-convs-learning-rate/early-stop/193.pkl')
robinmodel2 = serial.load('data/gpu-models/conv-nets-raw-data-many-convs-learning-rate/early-stop/196.pkl')

print robinmodel.get_weights_topo().shape
#print model2.monitor.channels['test_y_misclass'].val_record[-1]


# In[ ]:

#figure = pyplot.figure()
#f, (ax1, ax2, ax3) = pyplot.subplots(3, sharex=True, sharey=True)
#ax1.plot(np.squeeze(model2.get_weights_topo()[0]))
#ax2.plot(np.squeeze(model2.get_weights_topo()[1]))
#ax3.plot(np.squeeze(model2.get_weights_topo()[2]))


f, axes = pyplot.subplots(50, sharex=True, sharey=True, figsize=(12,75))
for i, ax in enumerate(axes):
    ax.plot(np.squeeze(robinmodel.get_weights_topo()[i, :, 0]))


# In[414]:

def show_power_of_filters(model):
    # this did not make sense i was doing fft along channel axis it seems
    weights = model.layers[0].get_weights_topo() # b 0 1 c(!) -> but 1 are sensors on oinput filters!
    # so we have batch(0) x time(1) x channel(2) x nothing(3)
    weights_fft = np.fft.rfft(weights, axis=1)
    weights_power = np.abs(weights_fft) ** 2
    mean_power = np.squeeze(np.mean(weights_power, axis=(0,2)))
    #meansignal, only for debug/checkups should be close to 0-line
    #figure = pyplot.figure()
    #pyplot.plot(np.squeeze(np.mean(weights, axis=(0,2))))
    figure = pyplot.figure()
    freq_bins = np.fft.rfftfreq(n=weights.shape[1],d=1/150.0)
    pyplot.bar(freq_bins, mean_power)


# In[415]:

#i = 30
#figure = pyplot.figure()
#pyplot.plot(np.squeeze(model2.get_weights_topo()[i]))
#figure = pyplot.figure()
#pyplot.bar(range(len(np.squeeze(weights_power[i]))), np.squeeze(weights_power[i]))
show_power_of_filters(gujomodel)
show_power_of_filters(robinmodel)
show_power_of_filters(robinmodel2)
show_power_of_filters(mavomodel)
show_power_of_filters(mavomodel2)


# In[423]:

from pprint import pprint
robinresult = serial.load('data/gpu-models/conv-nets-raw-data-many-convs-learning-rate/early-stop/185.result.pkl')
pprint(robinresult.parameters)
robinresult2 = serial.load('data/gpu-models/conv-nets-raw-data-many-convs-learning-rate/early-stop/186.result.pkl')
pprint(robinresult2.parameters)


# ## Raw Input data range

# In[238]:

dataset = RawMotorEEGDataset('data/motor-imagery-tonio/GuJoMoSc1S001R01_ds10_1-11_autoclean_trials_ival_0_4000.mat', 
                             'auto_clean_trials_150Hz', 'targets', stop=50, axes=['b', 1, 0, 'c'], 
                             time_bin_start=100, time_bin_stop=250)
#self, filenames, topo_view_channel, target_channel, 
#start=None, stop=None, limits=None, axes=['b', 'c', 0, 1], sensor_names=None, #
#time_bin_start=None, time_bin_stop=None, frequency_bin_start=None, frequency_bin_stop=None, **kwargs)


# In[239]:

print dataset.get_topological_view().shape
print np.mean(dataset.get_topological_view())
print np.var(dataset.get_topological_view())
print np.min(dataset.get_topological_view())
print np.max(dataset.get_topological_view())
print dataset.weights_view_shape() # correctly transformed to #timesx#chansx1 => [150, 128, 1]


# ## Plot confusion matrix

# In[331]:

from analysis.plot_util import plot_confusion_matrix_for_result
plot_confusion_matrix_for_result('data/gpu-models/conv-nets-all-C-sensors/cross-validation/early-stop/', 0)
plot_confusion_matrix_for_result('data/gpu-models/conv-nets-all-C-sensors/cross-validation/early-stop/', 1)


# In[332]:

# have to implement for single models :)
plot_confusion_matrix_for_result('data/gpu-models/conv-nets-many-persons/cross-validation/early-stop/', 0)


# ### Analysis Correctness of model prediction

# In[241]:

model = serial.load('data/gpu-models/conv-nets-all-C-sensors/early-stop/5.pkl')
check_prediction_correctness(model)


# ### Analysis new bandpass filtered signals

# In[2]:

debug_dataset = h5py.File('data/motor-imagery-tonio/LaKaMoSc1S001R01_ds10_1-9_autoclean_trials_ival_0_4000.mat.debug_test')


# In[5]:

first_trial = debug_dataset['auto_clean_trials_150Hz_2_50_filterbank'][0,0,:,:]


# In[8]:

pyplot.figure()
for filter_i in range(first_trial.shape[1]):
    pyplot.figure()
    pyplot.plot(first_trial[:,filter_i])


# ### Analysis valid test big differences

# In[242]:

# First let's see if difference is also shown in cv folds of laka
model_fama_cv = serial.load('data/models/conv-nets-all-C-sensors/cross-validation/early-stop/60.pkl')
model_fama = serial.load('data/models/conv-nets-all-C-sensors/early-stop/3.pkl')
model_fama_smoothed_cv = serial.load('data/models/conv-nets-translated-smoothed/cross-validation/early-stop/7.pkl')


# In[147]:

for model_fama_smoothed_fold in model_fama_smoothed_cv:
    pyplot.figure()
    plot_results(model_fama_smoothed_fold)


# In[139]:

trainset = yaml_parse.load(model_fama.dataset_yaml_src)
validset = yaml_parse.load(model_fama.dataset_yaml_src.replace('start: 0', 'start: 0.8').replace('stop: 0.8', 'stop: 0.9'))
testset = yaml_parse.load(model_fama.dataset_yaml_src.replace('start: 0', 'start: 0.9').replace('stop: 0.8', 'stop: 1.0'))


# In[249]:

datasets = load_preprocessed_datasets(model_fama)
trainset = datasets['train']
validset = datasets['valid']
testset = datasets['test']


# In[246]:

from pylearn2.datasets.preprocessing import Standardize
standard_preproc = Standardize()
# hm lets see if this is the problem wiht preprocessing 
standard_preproc.apply(trainset, can_fit=True)
standard_preproc.apply(validset, can_fit=True)
standard_preproc.apply(testset, can_fit=True)


# In[244]:

train_targets = np.argmax(trainset.get_targets(), axis=1)
valid_targets = np.argmax(validset.get_targets(), axis=1)
test_targets = np.argmax(testset.get_targets(), axis=1)


# ### Fix with online preprocessing?

# In[326]:

from mypylearn2.preprocessing import OnlineStandardize
datasets = load_preprocessed_datasets(model_fama)
trainset = datasets['train']
validset = datasets['valid']
testset = datasets['test']
online_preproc = OnlineStandardize(use_only_new=False, new_factor=20.0)
# hm lets see if this is the problem wiht preprocessing 
online_preproc.apply(trainset, can_fit=True)
online_preproc.apply(validset, can_fit=False)
online_preproc.apply(testset, can_fit=False)


# In[327]:

train_preds = reshape_and_get_predictions(model_fama, np.float32(trainset.get_topological_view()))
train_preds = np.argmax(train_preds, axis=1)
valid_preds = reshape_and_get_predictions(model_fama, np.float32(validset.get_topological_view()))
valid_preds = np.argmax(valid_preds, axis=1)
test_preds = reshape_and_get_predictions(model_fama, np.float32(testset.get_topological_view()))
test_preds = np.argmax(test_preds, axis=1)

print("train", np.sum(np.equal(train_targets, train_preds)) / float(len(train_targets)))
print("valid", np.sum(np.equal(valid_targets, valid_preds)) / float(len(valid_targets)))
print("test", np.sum(np.equal(test_targets, test_preds)) / float(len(test_targets)))
print 1 - model_fama.monitor.channels['train_y_misclass'].val_record[-1]
print 1 - model_fama.monitor.channels['valid_y_misclass'].val_record[-1]
print 1 - model_fama.monitor.channels['test_y_misclass'].val_record[-1]


# In[42]:

pyplot.figure()
pyplot.hist(np.argmax(trainset.get_targets(), axis=1))
pyplot.figure()
pyplot.hist(np.argmax(validset.get_targets(), axis=1))
pyplot.figure()
pyplot.hist(np.argmax(testset.get_targets(), axis=1))


# In[144]:

pyplot.figure()
plot_results(model_fama)


# In[143]:

for model_fama_fold in model_fama_cv:
    pyplot.figure()
    plot_results(model_fama_fold)


# ## Analysis failing datasets

# * Find reason why weights change drastically
# * Print chosen batches
# * Load model before change (10)
# * Replicate quality/miscalss in that epoch
# * Replicate one epoch from pylearn, see if its possible

# In[4]:

#Expecting 
# (73) 46 62 40 31.24.17 36 40 10 11 24 73


# In[26]:

for layer_i in range(2):
    pass

layer_i = 0
layer = model.layers[layer_i]
weights = layer.get_weights_topo()
print weights.shape
print np.sum(weights, axis=(1,2,3))
layer_i = 1
layer = model.layers[layer_i]
weights = layer.get_weights_topo()
print weights.shape
print np.sum(weights, axis=(1,2,3))
layer_i = 2
layer = model.layers[layer_i]
weights = layer.get_weights()
print weights.shape
print np.sum(weights, axis=(0))


# In[38]:

model = serial.load('data/models/conv-nets-translated-examples/early-stop/14.pkl')
models[0].info_parameters
test_error = model.monitor.channels['test_y_misclass'].val_record
train_error = model.monitor.channels['train_y_misclass'].val_record
# 36 -39.pkl are alternating valid - fulltrain values
pyplot.plot(test_error, label='test_error')
pyplot.plot(train_error, label='train_error')
if 'valid_y_misclass' in model.monitor.channels:
    valid_error = model.monitor.channels['valid_y_misclass'].val_record
    pyplot.plot(valid_error, label='valid_error')
pyplot.legend()
last_train_error = model.monitor.channels['train_y_misclass'].val_record[-1]
min_train_error = np.min(model.monitor.channels['train_y_misclass'].val_record)
diff = last_train_error - min_train_error
print diff


# In[5]:

model = serial.load('./debug-model-11.pkl')
model_after = serial.load('./debug-model-12.pkl')


# In[ ]:




# In[6]:

flat_layer_before = model.layers[1].get_weights_topo()
flat_layer_after = model_after.layers[1].get_weights_topo()


# In[8]:

softmax_before = model.layers[2].get_weights()
softmax_after = model_after.layers[2].get_weights()


# In[22]:

diff = softmax_before - softmax_after
maxdiff = np.max(diff)
pyplot.figure()
pyplot.imshow(diff, cmap = matplotlib.cm.bwr, interpolation='nearest')
pyplot.figure()
pyplot.imshow(diff[0:11, :], cmap = matplotlib.cm.bwr, interpolation='nearest', vmin=-maxdiff, vmax=+maxdiff)
print softmax_after[0:4, :]
print diff[0:4, :]


# In[14]:

np.sum(np.abs(flat_layer_before - flat_layer_after))


# In[17]:

np.sum(np.abs(model.layers[0].get_weights_topo() - model_after.layers[0].get_weights_topo()))


# In[19]:

neuron_weight_sums = np.sum(np.abs(model.layers[0].get_weights_topo()), axis=(1,2,3))
pyplot.figure()
pyplot.bar(range(0, len(neuron_weight_sums)), neuron_weight_sums)
pyplot.figure()
neuron_after_weight_sums = np.sum(np.abs(model_after.layers[0].get_weights_topo()), axis=(1,2,3))
pyplot.bar(range(0, len(neuron_after_weight_sums)), neuron_after_weight_sums)


# In[16]:

neuron_weight_sums = np.sum(np.abs(flat_layer_before), axis=(1,2,3))
pyplot.figure()
pyplot.bar(range(0, len(neuron_weight_sums)), neuron_weight_sums)
neuron_after_weight_sums = np.sum(np.abs(flat_layer_after), axis=(1,2,3))
pyplot.bar(range(0, len(neuron_after_weight_sums)), neuron_after_weight_sums)


# In[20]:




# In[22]:

neuron_weight_sums = np.sum(np.abs(model.layers[2].get_weights()), axis=(1))
pyplot.figure()
pyplot.bar(range(0, len(neuron_weight_sums)), neuron_weight_sums)
pyplot.figure()
neuron_after_weight_sums = np.sum(np.abs(model_after.layers[2].get_weights()), axis=(1))
pyplot.bar(range(0, len(neuron_after_weight_sums)), neuron_after_weight_sums)


# ## Trying Replication with softmax -failed

# In[ ]:

for i in np.argsort(diffs)[::-1][0:20] + 1: # biggest diffs, +1 as model ids are 1-based
    model = serial.load('data/models/softmax-adam/early-stop/' + str(i) + '.pkl')
    test_error = model.monitor.channels['test_y_misclass'].val_record
    train_error = model.monitor.channels['train_y_misclass'].val_record

    pyplot.figure()
    # 36 -39.pkl are alternating valid - fulltrain values
    pyplot.plot(test_error, label='test_error')
    pyplot.plot(train_error, label='train_error')
    if 'valid_y_misclass' in model.monitor.channels:
        valid_error = model.monitor.channels['valid_y_misclass'].val_record
        pyplot.plot(valid_error, label='valid_error')
    pyplot.legend()
    pyplot.ylim(0.0, 0.8)


# In[44]:

diffs = []
for i in range(1,271):
    model = serial.load('data/models/softmax-adam/early-stop/' + str(i) + '.pkl')
    test_error = model.monitor.channels['test_y_misclass'].val_record
    train_error = model.monitor.channels['train_y_misclass'].val_record
    """
    pyplot.figure()
    # 36 -39.pkl are alternating valid - fulltrain values
    pyplot.plot(test_error, label='test_error')
    pyplot.plot(train_error, label='train_error')
    if 'valid_y_misclass' in model.monitor.channels:
        valid_error = model.monitor.channels['valid_y_misclass'].val_record
        pyplot.plot(valid_error, label='valid_error')
    pyplot.legend()
    pyplot.ylim(0.0, 0.8)"""
    
    last_train_error = model.monitor.channels['train_y_misclass'].val_record[-1]
    min_train_error = np.min(model.monitor.channels['train_y_misclass'].val_record)
    diff = last_train_error - min_train_error
    diffs.append(diff)
print np.max(diffs)
print np.argmax(diffs)


# In[53]:


print np.max(diffs)
print np.argmax(diffs)
pyplot.figure()
pyplot.plot(diffs)
print np.sort(diffs)[::-1][0:10]
print np.argsort(diffs)[::-1][0:10] + 1


# ## Analysis New Early Stopping

# In[4]:

from pylearn2.utils import serial
models = serial.load('data/models/conv-nets-all-C-sensors-adam/cross-validation/27.pkl')
#models = serial.load('data/models/softmax-better-stopping/cross-validation/9.pkl')


# In[12]:

def plot_results(model):
    test_error = model.monitor.channels['test_y_misclass'].val_record
    train_error = model.monitor.channels['train_y_misclass'].val_record
    pyplot.plot(test_error, label='test_error')
    pyplot.plot(train_error, label='train_error')
    if 'valid_y_misclass' in model.monitor.channels:
        valid_error = model.monitor.channels['valid_y_misclass'].val_record
        pyplot.plot(valid_error, label='valid_error')
    pyplot.legend()


# In[5]:

for i in xrange(0,10):
    pyplot.figure()
    model = models[i]
    pyplot.title("Fold {:d}: {:4.2f}%".format(i, (1 - model.monitor.channels['test_y_misclass'].val_record[-1]) * 100))
    test_error = model.monitor.channels['test_y_misclass'].val_record
    train_error = model.monitor.channels['train_y_misclass'].val_record
    pyplot.plot(test_error, label='test_error')
    pyplot.plot(train_error, label='train_error')
    if 'valid_y_misclass' in model.monitor.channels:
        valid_error = model.monitor.channels['valid_y_misclass'].val_record
        pyplot.plot(valid_error, label='valid_error')
    pyplot.legend()


# ## Compare FFT to preprocessed FFT

# In[137]:

def compute_ps(trials):
    window_length = 250 # 500ms
    window_stride = 125 # 250 ms
    
    start_times = numpy.arange(0, trials.shape[2] - window_length+1, window_stride)
    freq_bins = int(numpy.floor(window_length / 2) + 1)
    power_spectra = numpy.zeros((trials.shape[0], trials.shape[1], len(start_times), freq_bins))
    for time_bin, start_time in enumerate(start_times):        
        w = blackmanharris(window_length)
        w=w/numpy.linalg.norm(w)
        trials_for_fft = trials[:,:,start_time:start_time+window_length] * w
        ps = numpy.abs(numpy.fft.rfft(trials_for_fft, axis=2)) ** 2
        ps = ps / 500 # divide by samplingrate
        power_spectra[:,:,time_bin, :] = ps
    return power_spectra


# In[144]:

window_length = 250 # 500 ms
window_stride = 125 # 250 ms
start_time
ps = compute_ps(trials[0:2])


# In[152]:

import numpy
import scipy
from scipy.signal import blackmanharris
import h5py
import numpy.linalg
h5file = h5py.File('data/motor-imagery-tonio/GuJoMoSc1S001R01_ds10_1-11_autoclean_trials_ival_0_4000.mat', 'r')
trials = h5file['auto_clean_trials_500Hz'][0:10]


# In[108]:

get_ipython().magic(u'timeit ps = compute_ps(trials)')


# In[138]:

ps = compute_ps(trials[0:10])
original_fft = h5file['auto_clean_trials_500Hz_500ms_len_250ms_stride_ps'][0:10]


# In[139]:

numpy.sum(numpy.abs(ps-original_fft)) # => almost identical...


# In[150]:

pyplot.figure()
get_ipython().magic(u'pinfo np.fft.fftfreq')
pyplot.plot(np.fft.rfftfreq(n=250,d=1/500.0)[3:], ps[0,0,0,3:]) 
pyplot.plot(np.fft.rfftfreq(n=250,d=1/500.0)[3:], original_fft[0,0,0,3:])
#  juhu it is the same


# In[53]:

model.get_weights_topo().shape


# In[52]:

good_trials.shape


# In[55]:

print("either {:d} or {:d} as input for next layer".format(45*(15 - 2 + 1) * 32, (15 - 2 + 1)*32 ))


# In[62]:

relu_layer = model.layers[1]
relu_layer.get_weights_topo().shape
32 * 14


# In[45]:

# cehck howf requencies are comptued in eeg dataset
import h5py
filename =  './data/motor-imagery-tonio/LaKaMoSc1S001R01_ds10_1-9_autoclean_trials_ival_0_4000.mat'
topochan = 'auto_clean_trials_500Hz_500ms_len_250ms_stride_ps'
h5file =  h5py.File(filename)
good_trials = h5file[topochan]
good_trials.shape
scale_factor = 126.0 / 251
5 * scale_factor
50* scale_factor


# In[48]:

from motor_eeg_dataset import MotorEEGDataset
motor_dataset = MotorEEGDataset(filename, topochan, 'targets', start=0, stop=50, frequency_bins={'start':5, 'stop':50})


# In[49]:

motor_dataset.get_topological_view().shape


# ### Correct Computation of frequency bins

# In[27]:

np.fft.rfftfreq(n=250,d=1/500.0)


# In[26]:

num_freq_bins = 126

10 * ((num_freq_bins - 1) / 250.0)


# ## Analysis correctness early stopping

# In[63]:

models = serial.load('data/models/softmax-early-stop-test/cross-validation/early-stop/1.pkl')


# In[84]:

model = models[2]
model.validation_model
num_epochs = len(model.monitor.channels['test_y_misclass'].val_record)
num_epochs


# In[114]:

import numpy
validation_misclass = model.validation_model.monitor.channels['valid_y_misclass'].val_record
pyplot.plot(validation_misclass)
lowest_epoch = len(validation_misclass) - 1 - numpy.argmin(validation_misclass[::-1])
print(lowest_epoch)
num_training_epochs = int(round(lowest_epoch * (1.0 + (1.0 /3))))
print (num_training_epochs)


# In[86]:

print validation_misclass


# In[89]:

print numpy.argmin(validation_misclass)


# In[113]:

print validation_misclass[44:50]


# In[110]:

print(numpy.argmin(validation_misclass))
print(numpy.argmin(validation_misclass[::-1]))
print(validation_misclass[-37])
print(len(validation_misclass) - 36 -1)
print(len(validation_misclass) - 1 - numpy.argmin(validation_misclass[::-1]))


# ### Older Models checking test error development

# In[115]:

#40 vs 71
models = serial.load('data/models/conv-nets-all-C-sensors/cross-validation/40.pkl')
newmodels = serial.load('data/models/conv-nets-all-C-sensors/cross-validation/71.pkl')


# In[136]:

fold_nr = 10
model = models[fold_nr]
newmodel = newmodels[fold_nr]
test_old = model.monitor.channels['test_y_misclass'].val_record
test_new = newmodel.monitor.channels['test_y_misclass'].val_record
pyplot.plot(test_old)
pyplot.plot(test_new)
print(test_old[-1])
print(test_new[-1])


# In[27]:

get_ipython().run_cell_magic(u'javascript', u'', u'function makeSVGDownloadable() {\n    $(\'body\').on(\'click\', \'svg\', function(){\n        var svg = this.outerHTML;\n        var b64 = btoa(svg); // or use btoa if supported\n        var linkToSVG = $("<a href-lang=\'image/svg+xml\' href=\'data:image/svg+xml;base64,\\n" + b64 +\n                          "\'title=\'file.svg\' target=\'blank\'>Download</a>");\n        $(this).after(linkToSVG);});\n}\nmakeSVGDownloadable()')

