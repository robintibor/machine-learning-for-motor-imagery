
# coding: utf-8

# # Analysis SvMu Data Variances 

# In[1]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# switch to cpu
os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
from matplotlib import cm
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from motor_imagery_learning.analysis.plot_util import plot_class_probs, plot_chan_matrices, plot_class_reconstruction,     plot_correct_part, plot_incorrect_part, plot_correctness, plot_misclasses, plot_sensor_signals
from  motor_imagery_learning.analysis.util import back_deconv, back_prop, back_guided_deconv, back_linear, bc01toinput,     load_preprocessed_datasets, back_guided_deconv_for_class, reshape_and_get_predictions,     get_trials_for_class, get_summed_activations, get_activations, get_activation,     check_prediction_correctness, load_train_test_datasets
from pylearn2.config import yaml_parse
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted, sort_topologically
from motor_imagery_learning.analysis.print_results import ResultPrinter
from copy import deepcopy
figsize=(13,8)
import psutil
import h5py
from scipy.io import loadmat
from pprint import pprint
from wyrm.types import Data
import wyrm


# In[2]:

from motor_imagery_learning.bbci_dataset import BBCIDataset
from motor_imagery_learning.mywyrm.processing import resample_epo, highpass_cnt

bbci_set = BBCIDataset(filename='data/BBCI-without-last-runs/SvMuMoSc1S001R01_ds10_1-12.BBCI.mat', 
                        sensor_names=None, cnt_preprocessors=[(highpass_cnt, {'low_cut_off_hz': 0.5})],
                        epo_preprocessors=[(resample_epo, {'newfs': 200})])

bbci_set.load()


# ## Trialwise Variances
# 
# Variance for all trials for all channels
# 
# * max values capped for nicer plot => some "spikes" (single trials with very high variance) smaller than they actually are
# * You see a lot of problems for AF/FP channels in first half

# In[3]:

variances = np.var(bbci_set.epo.data, axis=(1))
# cap variances for nicer plotting
variances = np.minimum(variances, 4000)
plot_sensor_signals(variances.T, bbci_set.sensor_names,figsize=(12,32), yticks="onlymax").suptitle(
    'Variances after >0.5Hz-highpass', fontsize=20, y=0.92)


# ## Determine exact trial nr of variance increase
# * Channel seems to be better from 500, even better from 560

# In[19]:

chan_i = bbci_set.sensor_names.index('Fp1')
chan_name = 'Fp1'
variances = np.var(bbci_set.epo.data, axis=(1))
plot_sensor_signals(np.atleast_2d(variances[450:600,chan_i]), 
                    sensor_names=[chan_name],figsize=(11,2), yticks="keep").suptitle(
    'Variances 450-600', fontsize=20, y=1.05)


# ## Problematic Trial/Unproblematic Trial raw signals
# 
# * Not possible for me to see what the problems are for problematic trials

# In[26]:

plot_sensor_signals(bbci_set.epo.data[570,:,chan_i:chan_i+1].T, 
                    [chan_name],figsize=(11,1), yticks="keep").suptitle(
                    'Ok Trial', fontsize=15, y=1.1)
pyplot.xlabel("200 samples=1 second")   
plot_sensor_signals(bbci_set.epo.data[488,:,chan_i:chan_i+1].T, #415
                    [chan_name],figsize=(11,1), yticks="keep").suptitle(
                    'Problematic Trial', fontsize=15, y=1.1)
pyplot.xlabel("200 samples=1 second")   


# ## Determine which frequencies are increased
# * We can make an fft of trials after 570 and before 500 (on that channel) to determine which frequncies are increased
# * We see bandpower is increased only on low frequencies < ~8 Hz
# * shape/peaks are similar => information maybe not destroyed? 

# In[15]:

freq_bins = np.fft.rfftfreq(bbci_set.epo.data.shape[1], d=1./bbci_set.epo.fs)

bandpower_ok_trials = np.mean(np.abs(np.fft.rfft(bbci_set.epo.data[570:,:,chan_i])) ** 2, axis=0) / bbci_set.epo.fs
bandpower_bad_trials = np.mean(np.abs(np.fft.rfft(bbci_set.epo.data[0:500:,:,chan_i])) ** 2, axis=0) / bbci_set.epo.fs
fig = pyplot.figure()

pyplot.plot(freq_bins, bandpower_ok_trials, figure=fig)
pyplot.plot(freq_bins, bandpower_bad_trials, figure=fig)
pyplot.legend(('good trials', 'bad trials'))
_ = pyplot.xticks(np.arange(0,100,5))

