
# coding: utf-8

# # Analysis FaMa Data Variances 

# In[1]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# switch to cpu
os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'

get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
from matplotlib import cm
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from motor_imagery_learning.analysis.plot_util import plot_class_probs, plot_chan_matrices, plot_class_reconstruction,     plot_correct_part, plot_incorrect_part, plot_correctness, plot_misclasses, plot_sensor_signals
from  motor_imagery_learning.analysis.util import back_deconv, back_prop, back_guided_deconv, back_linear, bc01toinput,     load_preprocessed_datasets, back_guided_deconv_for_class, reshape_and_get_predictions,     get_trials_for_class, get_summed_activations, get_activations, get_activation,     check_prediction_correctness, load_train_test_datasets
from pylearn2.config import yaml_parse
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted, sort_topologically
from motor_imagery_learning.analysis.print_results import ResultPrinter
from copy import deepcopy
figsize=(13,8)
import psutil
import h5py
from scipy.io import loadmat
from pprint import pprint
from wyrm.types import Data
import wyrm


# In[2]:

from motor_imagery_learning.bbci_dataset import BBCIDataset
from motor_imagery_learning.mywyrm.processing import resample_epo, highpass_cnt

bbci_set = BBCIDataset(filenames='data/BBCI-without-last-runs/FaMaMoSc1S001R01_ds10_1-14.BBCI.mat', 
                        sensor_names=None, cnt_preprocessors=[(highpass_cnt, {'low_cut_off_hz': 0.5})],
                        epo_preprocessors=[(resample_epo, {'newfs': 200})])

bbci_set.load()


# ## Trialwise Variances
# 
# Variance for all trials for all channels
# 
# * max values capped for nicer plot => some "spikes" (single trials with very high variance) smaller than they actually are
# * For quite a lot of channels like FFT7h, AFF5/6h,... you see from trial ~640 onwards a large increase in the variance

# In[13]:

variances = np.var(bbci_set.epo.data, axis=(1))
# cap variances for nicer plotting
variances = np.minimum(variances, 4000)
plot_sensor_signals(variances.T, bbci_set.sensor_names,figsize=(12,32), yticks="onlymax").suptitle(
    'Variances after >0.5Hz-highpass', fontsize=20, y=0.92)


# ## Determine exact trial nr of variance increase
# * From channel FFT7h we can see the problem starts at trial 637 or 640 

# In[14]:

chan_i = bbci_set.sensor_names.index('FFT7h')
variances = np.var(bbci_set.epo.data, axis=(1))
plot_sensor_signals(np.atleast_2d(variances[630:660,chan_i]), 
                    sensor_names=['FFT7h'],figsize=(11,2), yticks="keep").suptitle(
    'Variances 630-660', fontsize=20, y=1.05)


# ## Problematic Trial/Unproblematic Trial raw signals
# 
# * I checked and different trials from problematic/unproblematic range look similar

# In[40]:

plot_sensor_signals(bbci_set.epo.data[620,:,chan_i:chan_i+1].T, 
                    ['FFT7h'],figsize=(11,1), yticks="keep").suptitle(
                    'Ok Trial', fontsize=15, y=1.1)
plot_sensor_signals(bbci_set.epo.data[660,:,chan_i:chan_i+1].T, 
                    ['FFT7h'],figsize=(11,1), yticks="keep").suptitle(
                    'Problematic Trial', fontsize=15, y=1.1)


# ## Determine which frequncies are increased
# * We can make an fft of trials before 630 and after 650 (on that channel) to determine which frequncies are increased
# * We see bandpower is increased across all frequencies, 
# * shape/peaks are similar => information maybe not destroyed? 
# * Note there are less bad trials so that may be a reason why it is a less smooth curve 

# In[46]:

freq_bins = np.fft.rfftfreq(bbci_set.epo.data.shape[1], d=1./bbci_set.epo.fs)

bandpower_ok_trials = np.mean(np.abs(np.fft.rfft(bbci_set.epo.data[0:630,:,chan_i])) ** 2, axis=0) / bbci_set.epo.fs
bandpower_bad_trials = np.mean(np.abs(np.fft.rfft(bbci_set.epo.data[:655,:,chan_i])) ** 2, axis=0) / bbci_set.epo.fs
fig = pyplot.figure()

pyplot.plot(freq_bins, bandpower_ok_trials, figure=fig)
pyplot.plot(freq_bins, bandpower_bad_trials, figure=fig)
pyplot.legend(('good trials', 'bad trials'))


# ## Single high-variance trial on C-Sensor 
# * From analysis before I saw one trial with a unusually high variance on C2/C4
# * Just showing here in case this is unusual, it would probbably get removed by cleaning

# In[52]:

chan_inds = [bbci_set.sensor_names.index(s) for  s in ['C2', 'C4']]
plot_sensor_signals(bbci_set.epo.data[593,:,chan_inds], ['C2', 'C4'],figsize=(12,4)).suptitle('variance problem trial', 
                                                                                              fontsize=20)

