
# coding: utf-8

# # Analysis Laka Data Variances Before/After Cleaning 

# In[1]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# switch to cpu
os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'

get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
from matplotlib import cm
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from motor_imagery_learning.analysis.plot_util import plot_class_probs, plot_chan_matrices, plot_class_reconstruction,     plot_correct_part, plot_incorrect_part, plot_correctness, plot_misclasses, plot_sensor_signals
from  motor_imagery_learning.analysis.util import back_deconv, back_prop, back_guided_deconv, back_linear, bc01toinput,     load_preprocessed_datasets, back_guided_deconv_for_class, reshape_and_get_predictions,     get_trials_for_class, get_summed_activations, get_activations, get_activation,     check_prediction_correctness, load_train_test_datasets
from pylearn2.config import yaml_parse
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted, sort_topologically
from motor_imagery_learning.analysis.print_results import ResultPrinter
from copy import deepcopy
figsize=(13,8)
import psutil
import h5py
from scipy.io import loadmat
from pprint import pprint
from wyrm.types import Data
import wyrm


# In[3]:

from motor_imagery_learning.bbci_dataset import BBCIDataset
from motor_imagery_learning.mywyrm.processing import resample_epo, highpass_cnt

bbci_set = BBCIDataset(filename='data/BBCI-without-last-runs/LaKaMoSc1S001R01_ds10_1-9.BBCI.mat', 
                        sensor_names=None, cnt_preprocessors=[(highpass_cnt, {'low_cut_off_hz': 0.5})],
                        epo_preprocessors=[(resample_epo, {'newfs': 200})])

bbci_set.load()


# ## Trialwise Variances
# 
# Variance for all trials for all channels
# 
# * max values capped for nicer plot => some "spikes" (single trials with very high variance) smaller than they actually are
# * For quite a lot of channels like FFT7h, AFF5/6h,... you see from trial ~640 onwards a large increase in the variance

# In[4]:

variances = np.var(bbci_set.epo.data, axis=(1))
# cap variances for nicer plotting
variances = np.minimum(variances, 4000)
plot_sensor_signals(variances.T, bbci_set.sensor_names,figsize=(12,32), yticks="onlymax").suptitle(
    'Variances after >0.5Hz-highpass', fontsize=20, y=0.92)


# ## After Cleaning

# In[12]:

from mywyrm.clean import compute_rejected_channels_and_trials
from wyrm.processing import select_channels, select_epochs
rejected_channels, rejected_trials = compute_rejected_channels_and_trials(
    bbci_set, max_min=600, whisker_percent=10, whisker_length=3)
clean_epo = select_channels(bbci_set.epo, rejected_channels, invert=True)
clean_epo = select_epochs(clean_epo, rejected_trials, invert=True)
# cap variances for nicer plotting
variances = np.var(clean_epo.data, axis=1)
variances = np.minimum(variances, 4000)
plot_sensor_signals(variances.T, clean_epo.axes[2],figsize=(12,32), yticks="onlymax").suptitle(
   'Variances after cleaning and >0.5Hz-highpass', fontsize=20, y=0.92)


# # Clean set frequency analysis

# In[35]:

wanted_sensor = 'CPz'
sensor_i = np.flatnonzero(clean_epo.axes[2] == 'CPz')[0]

freq_bins = np.fft.rfftfreq(clean_epo.data.shape[1], d=1./clean_epo.fs)

bandpower_trials = np.mean(np.abs(np.fft.rfft(clean_epo.data, axis=1)) ** 2, axis=(0,2)) / clean_epo.fs
bandpower_Cz = np.mean(np.abs(np.fft.rfft(clean_epo.data[:,:,sensor_i:sensor_i+1], axis=1)) ** 2, 
                       axis=(0,2)) / clean_epo.fs

fig = pyplot.figure()

pyplot.plot(freq_bins, bandpower_trials, figure=fig)
pyplot.plot(freq_bins, bandpower_Cz, figure=fig)
pyplot.legend(('all sensors', wanted_sensor))

