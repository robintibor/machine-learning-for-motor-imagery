
# coding: utf-8

# In[12]:

import os
os.sys.path.insert(0,'/lokal/schirrmr/motor-imagery/code/') 
os.sys.path.insert(0,'/lokal/schirrmr/motor-imagery/code/motor_imagery_learning/') 
get_ipython().magic(u'cd /lokal/schirrmr/motor-imagery/code/motor_imagery_learning/')
#%cd /home/robintibor/farm04/motor-imagery/code/motor_imagery_learning
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7
from motor_imagery_learning.analysis.util import (check_prediction_correctness,
                                                 load_preprocessed_datasets,
                                                 reshape_and_get_predictions)
from motor_imagery_learning.analysis.plot_util import (plot_confusion_matrix_for_averaged_result,
                                plot_confusion_matrix_for_result,
                                plot_misclasses, plot_sensor_signals, plot_chan_matrices)
from motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
                                
from pylearn2.utils import serial
from pylearn2.space import Conv2DSpace
import numpy as np
from motor_eeg_dataset import get_available_sensors_topo
from pylearn2.config import yaml_parse
from scipy.stats import  entropy
from numpy.random import RandomState


# ### New Models spatial first analysis  

# In[34]:

train_state = serial.load('data/gpu-models/debug/raw-data-spatial-time/early-stop/first-5/3.pkl')

spatial_weights = train_state.model.layers[0].get_weights_topo()
time_weights = train_state.model.layers[1].get_weights_topo()


# In[36]:

time_weights.shape


# In[35]:

for weight_i in range(5):
    plot_chan_matrices(spatial_weights.transpose(0,3,1,2)[weight_i], get_C_sensors_sorted())


# In[39]:

_ = plot_sensor_signals(time_weights.transpose(0,3,1,2)[0], map(str, range(50)), figsize=(4,14))


# In[ ]:

load_preprocessed_datasets(train_state, preprocessor=Pi)


# In[2]:

model = serial.load('data/gpu-models/conv-nets-raw-data-many-convs-learning-rate/early-stop/1554.pkl')


# In[3]:

import re
model.dataset_yaml_src
match = re.search('sensor_names:([^\]]+\])', model.dataset_yaml_src)
sensor_names = yaml_parse.load(match.group(1))
match = re.search('filenames: ([^\,]+)', model.dataset_yaml_src)
filename = match.group(1)

get_available_sensors_topo([filename], sensor_names)
print len(sensor_names)


# In[4]:

print model.get_weights_topo().shape

# channels are in second dimension (used as imagerows/height) actually.............
weights_bc01 = Conv2DSpace.convert_numpy(model.get_weights_topo(), ('b', 0, 'c', 1), ('b', 'c', 0, 1))
weights_bc01.shape


# In[ ]:

def get_weights_power(model):
    weights = model.layers[0].get_weights_topo() # b 0 1 c(!) -> but 1 are sensors on oinput filters!
    # so we have batch(0) x time(1) x channel(2) x nothing(3)
    weights_fft = np.fft.rfft(weights, axis=1)
    weights_power = np.abs(weights_fft) ** 2
    freq_bins = np.fft.rfftfreq(n=weights.shape[1],d=1/150.0)
    return weights_power, freq_bins

def show_power_of_filters(model):
    weights_power, freq_bins = get_weights_power(model)
    #weights_power = weights_power[:,:,21,:]# restrict to one chan
    mean_power = np.squeeze(np.mean(weights_power, axis=(0,2)))
    #meansignal, only for debug/checkups should be close to 0-line
    #figure = pyplot.figure()
    #pyplot.plot(np.squeeze(np.mean(weights, axis=(0,2))))
    figure = pyplot.figure()
    pyplot.bar(freq_bins, mean_power)


# In[ ]:

show_power_of_filters(model)


# ## Entropy weights power (FFT) analysis

# In[ ]:

def compute_weight_power_entropies(weight_powers):
    """ assumes weights are in b 0 1 c format,but 1 is acutally sensors,e.g. b 0 c 1 format with 1 as
    an empty dimension """
    weight_entropies = []
    for weight_i in range(weight_powers.shape[0]):
        for chan_i in range(weight_powers.shape[2]):
            this_weight_power = weight_powers[weight_i,3:,chan_i,0] # ignore first 3 bins?
            weight_entropies.append(entropy(this_weight_power))
    return weight_entropies

weights_power, freq_bins = get_weights_power(model)
rng = RandomState(np.uint32(hash('fftanalysis')))
#test for entropy
randomsignal = rng.normal(size=(100))
sinesignal = np.sin(np.linspace(0,7,100))
dummy_freqs = np.linspace(1,51,51)
random_fft = np.abs(np.fft.rfft(randomsignal)) ** 2
sinesignal_fft = np.abs(np.fft.rfft(sinesignal)) ** 2
#pyplot.figure()
#pyplot.bar(dummy_freqs, random_fft)
#pyplot.figure()
#pyplot.bar(dummy_freqs, sinesignal_fft)
weights_entropies = compute_weight_power_entropies(weights_power)

print "entropy weights", np.mean(weights_entropies)
weights_shape = model.layers[0].get_weights_topo().shape
random_weights = rng.normal(size=weights_shape)#rng.uniform(size=weights_shape)#
random_weights_power = np.abs(np.fft.rfft(random_weights, axis=1)) ** 2
random_entropies = compute_weight_power_entropies(random_weights_power)
print "entropy random weights", np.mean(random_entropies)
sine_weights = np.tile(sinesignal[0:50], (weights_shape[0], 1, weights_shape[2], weights_shape[3])).transpose((0,3,2,1))
sine_weights_power = np.abs(np.fft.rfft(sine_weights, axis=1)) ** 2
sine_entropies = compute_weight_power_entropies(sine_weights_power)
print ("sine weights entropies", np.mean(sine_entropies))
print sine_weights.shape
print weights_shape
print random_weights_power.shape
print entropy(random_fft)
print entropy(sinesignal_fft)
pyplot.figure()
pyplot.plot(model.monitor.channels['test_y_misclass'].val_record)


# In[ ]:

pyplot.figure()
weight_i = 0
for chan_i in range(15,25):
    pyplot.figure()
    pyplot.plot(weights_bc01[weight_i, chan_i, :, 0])


# In[ ]:

# Investigate if second derivate could be helpful
weight = weights_bc01[0,0,:,0]
#weight = np.sin(np.arange(0,7,0.1))
pyplot.figure()
pyplot.plot(weight)
first_derivative = weight[1:] - weight[:-1]
pyplot.figure()
pyplot.plot(first_derivative)
pyplot.figure()
second_derivative = first_derivative[1:] - first_derivative[:-1]
pyplot.plot(second_derivative)


# ## Weights affecting input analysis 

# In[5]:

mavomo_softmax_train = serial.load('data/gpu-models/debug/raw-data-explain/early-stop/3.pkl')
plot_confusion_matrix_for_result('data/gpu-models/debug/raw-data-explain/early-stop/', 3)


# In[6]:

preproc = Pipeline(items=[RemoveTrialChannelMean(), OnlineChannelwiseStandardize(new_factor=1)])
train_set, test_set = load_preprocessed_datasets(mavomo_softmax_train, preprocessor=preproc)


# In[ ]:

pyplot.figure()
pypllot


# In[ ]:

trials_class_1 = get_trials_for_class(test_set, 0)
trials_class_3 = get_trials_for_class(test_set, 2)
trials_class_4 = get_trials_for_class(test_set, 3)

plot_sensor_signals(np.squeeze(trials_class_2.transpose(1,2,0,3)), ['C3', 'C4'],figsize=(8,2))
plot_sensor_signals(np.squeeze(trials_class_3.transpose(1,2,0,3)), ['C3', 'C4'],figsize=(8,2))


# In[ ]:

inputs = T.fmatrix()
results =  mavomo_softmax_train.model.fprop(inputs)
mlp_fprop = theano.function([inputs], results)
softmax_layer = mavomo_softmax_train.model.layers[0]


# In[ ]:

trials_class_2 = get_trials_for_class(test_set, 1)
trials_class_2_flat = trials_class_2.reshape(len(trials_class_2), -1)
print trials_class_2[0].shape
single_trial = np.squeeze(np.reshape(trials_class_2[0].astype(np.float32), (1, -1)))
print single_trial.shape
class_2_out = mlp_fprop(trials_class_2_flat.astype(np.float32))


# In[ ]:

weights = softmax_layer.get_weights()
reshaped_weights = weights.reshape(2, -1, 4)


# In[ ]:

softmax_layer.get_biases()


# In[ ]:

class_2_out


# In[ ]:

# works fine!!
linear_out = np.sum(trials_class_2 * reshaped_weights, axis=(1,2)) + softmax_layer.get_biases()
exp_out = np.exp(linear_out)
softmax_out = exp_out / np.expand_dims(np.sum(exp_out, axis=1), 1)


# In[ ]:

reshaped_weights.shape


# In[ ]:

best_trial_ind_2 = np.argmax(class_2_out[:,1])
best_trial = trials_class_2[best_trial_ind_2]

plot_sensor_signals(best_trial, ['C3', 'C4'], figsize=(8,4))
best_trial_linear_out = best_trial * reshaped_weights
plot_sensor_signals(best_trial_linear_out, ['C3', 'C4'], figsize=(8,4))


# In[ ]:

plot_sensor_signals(reshaped_weights[:,:,[1,2]], ['C3', 'C4'], figsize=(8,4))


# In[ ]:

bandpower = np.abs(np.fft.rfft(reshaped_weights[0,:,0])) ** 2
important_bandpower = bandpower[0:len(bandpower) // 2 + 1]
print important_bandpower.shape
pyplot.figure()
pyplot.bar(np.fft.rfftfreq(len(bandpower), d=1/150.0), important_bandpower)


# In[ ]:

np.sum(class_2_out.argmax(axis=1) == 1) / 20.0 # 0.75 all good :)


# In[ ]:

softmax_layer.get_weights().shape

fprop 


# In[ ]:

bhno_train_states = serial.load('data/gpu-models/raw-data-demeaned/cross-validation/early-stop/2.pkl')
bhno_results =  serial.load('data/gpu-models/raw-data-demeaned/cross-validation/early-stop/2.result.pkl')


# In[ ]:

_ = [plot_misclasses(result) for result in bhno_results]


# In[ ]:

plot_confusion_matrix_for_result('data/gpu-models/raw-data-demeaned/cross-validation/early-stop/', 3)


# In[ ]:

bhno_final_model = bhno_train_states[-1].model
check_prediction_correctness(bhno_train_states[-1], 9)
#actually 0.99859


# In[ ]:

from pylearn2.datasets.preprocessing import Pipeline
from motor_imagery_learning.mypylearn2.preprocessing import (
    RemoveTrialChannelMean, OnlineChannelwiseStandardize)
print bhno_final_model.get_weights_topo().shape
preproc = Pipeline(items=[RemoveTrialChannelMean(), OnlineChannelwiseStandardize(new_factor=1)])
datasets = load_preprocessed_datasets(bhno_train_states[9], fold_nr=9, 
        preprocessor=preproc)


# In[ ]:

train_set = datasets['train']
test_set = datasets['test']
test_predictions = reshape_and_get_predictions(bhno_final_model, test_set.get_topological_view())


# In[ ]:

actual_labels = np.argmax(test_set.y, axis=1)
predicted_labels = np.argmax(test_predictions, axis=1)
actual_labels == predicted_labels
# take correct fairly certain prediction
correct_preds = actual_labels == predicted_labels
correct_preds_class_1 = np.logical_and(correct_preds, actual_labels == 0)
best_pred = np.max(test_predictions[correct_preds_class_1, 0])
best_pred_class_1 = np.logical_and(correct_preds_class_1, test_predictions[:,0] == best_pred)
best_pred_class_1_i = np.nonzero(best_pred_class_1)[0][0]

assert np.all(test_set.y[best_pred_class_1_i] == [1,0,0,0]), "should be trial of class 1 of course"
print test_predictions[best_pred_class_1_i]


# In[ ]:

trial_signal = test_set.get_topological_view()[best_pred_class_1_i]
trial_signal = np.squeeze(trial_signal)
sensor_names = get_C_sensors_sorted()
#print trial_signal.shape
plot_sensor_signals(trial_signal, sensor_names=get_C_sensors_sorted())


# In[ ]:

import theano.tensor as T
import theano
first_layer = bhno_final_model.layers[0]
# lets disable pooling
first_layer.pool_type = None
inputs = T.ftensor4()
results = first_layer.fprop(inputs)
fprop_func = theano.function([inputs], results)


# In[ ]:

from motor_imagery_learning.analysis.util import get_trials_for_class
trials_class_1 = get_trials_for_class(test_set, 0)
outputs_class_1 = fprop_func(trials_class_1.astype(np.float32))
trials_class_2 = get_trials_for_class(test_set, 1)
outputs_class_2 = fprop_func(trials_class_2.astype(np.float32))
trials_class_3 = get_trials_for_class(test_set, 2)
outputs_class_3 = fprop_func(trials_class_3.astype(np.float32))
avg_out_1 = np.mean(outputs_class_1, axis=0)
avg_out_2 = np.mean(outputs_class_2, axis=0)
avg_out_3 = np.mean(outputs_class_3, axis=0)


# In[ ]:

# rerun on yourself with only few sensors? or on mavomo with c3 c4
# maybe only with 2 or 3 outgoing chans...check when you can get above >70% or sth
# maybe softmax only first
# then add conv layer, see what happens
# put full inbetween see what happens
# add second conv, see what happens
# make some complete transformations


# In[ ]:

subtracted = avg_out_1 - avg_out_3
plot_sensor_signals(subtracted, map(str, xrange(len(subtracted))))


# In[ ]:


both_avg = np.concatenate((avg_out_1, avg_out_3), axis=2)
plot_sensor_signals(both_avg, map(str, xrange(len(both_avg))))


# In[ ]:




# In[ ]:

from pylearn2.space import Conv2DSpace
# first plot all signals...hope no crash
outputs_for_plot = np.squeeze(Conv2DSpace.convert(outputs, ('b', 'c', 0, 1), ('c', 0, 'b', 1)))
outputs_for_plot = np.mean(outputs_for_plot, axis=2)
print outputs_for_plot.shape

plot_sensor_signals(outputs_for_plot, map(str, xrange(len(outputs_for_plot))))


# In[ ]:

fprop_func([trial_signal.astype(np.float32)])

output = fprop_func([trial_signal.astype(np.float32)])
output.shape

# just use numbers as names
plot_sensor_signals(output[0], map(str, xrange(len(output[0]))))

