
# coding: utf-8

# In[12]:

import os
os.sys.path.insert(0,'/lokal/schirrmr/motor-imagery/code/') 
os.sys.path.insert(0,'/lokal/schirrmr/motor-imagery/code/motor_imagery_learning/') 
get_ipython().magic(u'cd /lokal/schirrmr/motor-imagery/code/motor_imagery_learning/')
#%cd /home/robintibor/farm04/motor-imagery/code/motor_imagery_learning
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7
from motor_imagery_learning.analysis.util import (check_prediction_correctness,
                                                 check_prediction_correctness_for_datasets,
                                                 load_preprocessed_datasets,
                                                 reshape_and_get_predictions,
                                                 get_trials_for_class)
from motor_imagery_learning.analysis.plot_util import (plot_confusion_matrix_for_averaged_result,
                                plot_confusion_matrix_for_result,
                                plot_misclasses, plot_sensor_signals,
                                plot_chan_matrices)
from motor_imagery_learning.analysis.plot_nets import plot_class_weights_and_head_images
from motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
from pylearn2.datasets.preprocessing import Standardize
from pylearn2.utils import serial
from pylearn2.space import Conv2DSpace
import numpy as np
from motor_eeg_dataset import get_available_sensors_topo
from pylearn2.config import yaml_parse
from scipy.stats import  entropy
from numpy.random import RandomState
from IPython.display import HTML


# In[2]:

sensor_names = get_C_sensors_sorted()
bhnotrain_state = serial.load('data/gpu-models/bandpower/early-stop/4.pkl')
datasets = load_preprocessed_datasets(bhnotrain_state, preprocessor=Standardize())


# In[3]:

from motor_eeg_dataset import FFTMotorEEGDataset
bhno_set = FFTMotorEEGDataset('./data/motor-imagery-tonio/BhNoMoSc1S001R01_ds10_1-12_autoclean_trials_ival_0_4000.mat', 
                              'auto_clean_trials_500Hz_500ms_len_250ms_stride_ps',target_channel='targets',
                             frequency_start=2, frequency_stop=130,
                             sensor_names=get_C_sensors_sorted())


# In[4]:

from pylearn2.datasets.preprocessing import Standardize
preproc = Standardize()

preproc.apply(bhno_set, can_fit=True)


# In[5]:

full_mean_class_1 = np.mean(get_trials_for_class(bhno_set, 0), axis=0)
full_mean_class_2 = np.mean(get_trials_for_class(bhno_set, 1), axis=0)
full_mean_class_3 = np.mean(get_trials_for_class(bhno_set, 2), axis=0)
full_mean_class_4 = np.mean(get_trials_for_class(bhno_set, 3), axis=0)


# In[87]:

plot_chan_matrices(full_mean_class_1, sensor_names)
plot_chan_matrices(full_mean_class_2, sensor_names)
plot_chan_matrices(full_mean_class_3, sensor_names)
_ = plot_chan_matrices(full_mean_class_4, sensor_names)


# ## Visualize Nets

# In[7]:

plot_confusion_matrix_for_result('data/gpu-models/bandpower/early-stop/', 4)


# In[8]:

check_prediction_correctness_for_datasets(bhnotrain_state, datasets)


# In[9]:

plot_chan_matrices(all_trials[np.argmax(all_preds[:, 0])], sensor_names)
plot_chan_matrices(all_trials[np.argmax(all_preds[:, 1])], sensor_names)
plot_chan_matrices(all_trials[np.argmax(all_preds[:, 2])], sensor_names)
plot_chan_matrices(all_trials[np.argmax(all_preds[:, 3])], sensor_names)


# In[21]:

all_trials.shape


# In[69]:

mean_outputs = [np.mean(all_trials * all_preds[:, class_i, np.newaxis, np.newaxis, np.newaxis], axis=0)
               for class_i in range(4)]



# In[238]:

softmax_layer = bhnotrain_state.model.layers[2]

class_weights = softmax_layer.get_weights()


# In[137]:

num_neurons = len(activations[1][0])
mean_fullreclin_activations = [np.mean(all_trials * activations[1][ :, neuron_i, np.newaxis, np.newaxis, np.newaxis],
                                       axis=0)
               for neuron_i in range(num_neurons)]

mean_fullreclin_activations = np.array(mean_fullreclin_activations)


# * understand how to detect intentions better for helping ppl with paralysis
# * show first all activations in full layer, together with 
# 

# ### Deconv Neurons Results 

# In[17]:

from motor_imagery_learning.analysis.util import back_deconv
softmax_layer = bhnotrain_state.model.layers[2]

class_weights = softmax_layer.get_weights()
all_wanted_inputs = []
for neuron_i in range(80):
    wanted_linear_output = [0] * 80
    wanted_linear_output[neuron_i] = 1

    wanted_inputs = back_deconv(bhnotrain_state.model.layers[0:2], wanted_linear_output)
    all_wanted_inputs.append(wanted_inputs)


# In[20]:

for neuron_i in np.arange(0,80,8):
    this_inputs = all_wanted_inputs[neuron_i:neuron_i+8]
    this_weights = class_weights[neuron_i:neuron_i+8]
    _ = plot_class_weights_and_head_images(this_inputs, sensor_names, this_weights, figsize=(12,10))


# In[77]:

wanted_linear_output = [0] * 80
wanted_linear_output[2] = 1
wanted_full_inputs = back_deconv(bhnotrain_state.model.layers[1:2], wanted_linear_output)
wanted_full_inputs.shape


# In[73]:

from matplotlib import cm
fig = pyplot.figure(figsize=(12,8))
for conv_neuron_i in xrange(32):
    subplot_ind = (conv_neuron_i + 1) * 3
    ax = fig.add_subplot(32, 3, subplot_ind)
    ax.pcolorfast(wanted_full_inputs[conv_neuron_i].T, cmap=cm.bwr)
    ax.set_yticks([])
    
ax.set_xticklabels([str(i) for i in np.arange(0,4000, 250)[::2]])
fig.subplots_adjust(hspace=0.5)


# In[78]:

from matplotlib import cm
mean_abs_val = np.mean(np.abs(wanted_full_inputs[0:8]))
fig = pyplot.figure(figsize=(12,1))
for conv_neuron_i in xrange(8):
    ax = fig.add_subplot(2, 4, conv_neuron_i + 1)
    ax.pcolorfast(wanted_full_inputs[conv_neuron_i].T, cmap=cm.bwr, vmin=-mean_abs_val*2, vmax=mean_abs_val*2)
    ax.set_xticks(np.arange(0,15,4))
    ax.set_xticklabels([str(i) for i in np.arange(0,4000, 250)[::4]], fontsize=8)
    ax.set_yticks([])
    
fig.subplots_adjust(hspace=0.7)


# In[79]:

wanted_full_inputs.shape
wanted_conv_outputs = wanted_full_inputs * (wanted_full_inputs > 0)


# In[81]:

from matplotlib import cm
mean_abs_val = np.mean(np.abs(wanted_full_inputs[0:8]))
fig = pyplot.figure(figsize=(12,1))
for conv_neuron_i in xrange(8):
    ax = fig.add_subplot(2, 4, conv_neuron_i + 1)
    ax.pcolorfast(wanted_conv_outputs[conv_neuron_i].T, cmap=cm.bwr, vmin=-mean_abs_val*2, vmax=mean_abs_val*2)
    ax.set_xticks(np.arange(0,15,4))
    ax.set_xticklabels([str(i) for i in np.arange(0,4000, 250)[::4]], fontsize=8)
    ax.set_yticks([])
    
fig.subplots_adjust(hspace=0.7)


# In[42]:

conv_weights = bhnotrain_state.model.get_weights_topo()
conv_weights.shape


# In[86]:

_ = plot_class_weights_and_head_images(conv_weights.transpose(0,3,1,2)[0:8], get_C_sensors_sorted(), 
                                   wanted_conv_outputs.transpose(0,2,1)[0:8], figsize=(12,8))


# In[84]:

this_inputs = all_wanted_inputs[2:3]
this_weights = class_weights[2:3]
_ = plot_class_weights_and_head_images(this_inputs, sensor_names, this_weights, figsize=(12,10))


# ###  Stack svgs

# In[79]:


from tempfile import NamedTemporaryFile
import svgutils.transform


#create new SVG figure
svgfig = svgutils.transform.SVGFigure(width="{:.1f}in".format(fig.get_figwidth() * 4),
                                      height="{:.1f}in".format(fig.get_figheight()))
svgsubfig1 = svgutils.transform.fromstring(svg_data)
svgsubfig2 = svgutils.transform.fromstring(svg_data)

# get the plot objects
plot1 = svgsubfig1.getroot()
plot2 = svgsubfig2.getroot()
plot2.moveto(300, 0)


svgfig.append([plot1, plot2])

with NamedTemporaryFile('w') as tempfile:
    svgfig.save(tempfile.name)
    svg_string = open(tempfile.name, 'r').read()
print len(svg_string)
#open("to-delete-svg.svg", "w").write(svg_string)
HTML(svg_string)


# In[53]:

svgsubfig1.getroot()


# In[52]:


import svg_stack

doc = svg_stack.Document()

layout1 = svg_stack.HBoxLayout()
layout1.
#layout1.addSVG('red_ball.svg',alignment=ss.AlignTop|ss.AlignHCenter)
#layout1.addSVG('blue_triangle.svg',alignment=ss.AlignCenter)


# In[50]:

plot_chan_matrices(mean_class_1, sensor_names)


# In[175]:

get_ipython().run_cell_magic(u'javascript', u'', u'function makeSVGDownloadable() {\n    $(\'body\').on(\'click\', \'svg\', function(){\n        var svg = this.outerHTML;\n        var b64 = btoa(svg); // or use btoa if supported\n        var linkToSVG = $("<a href-lang=\'image/svg+xml\' href=\'data:image/svg+xml;base64,\\n" + b64 +\n                          "\'title=\'file.svg\' target=\'blank\'>Download</a>");\n        $(this).after(linkToSVG);});\n}\nmakeSVGDownloadable()')

