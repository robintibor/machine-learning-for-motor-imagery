
# coding: utf-8

# In[7]:


import os
os.environ['FUEL_DATA_PATH'] = '/home/schirrmr/fuel-data/'#%%capture
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
#os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import theano
os.environ['FUEL_DATA_PATH'] = '/home/schirrmr/fuel-data/'
import matplotlib
from matplotlib import pyplot
from matplotlib import cm
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (8.0, 1.0)
matplotlib.rcParams['font.size'] = 7

import matplotlib.lines as mlines
import seaborn
seaborn.set_style('darkgrid')
import numpy as np
from motor_imagery_learning.analysis.data_generation import create_sine_signal


# ### new plan

# In[ ]:

# redo gujo classification with non-recurrent convolution until atleast 60%
# also add temporal pooling without stride
# try once with the data from subject 1
# try adding spatial still linear part as in raw net to convlution
# retry on gujo 
# retry on subject 1 also make train test split...
# try optimize and make some submission with the script provided....
# try adding recurrent layer on top
# maybe try lowpass + subsample to 250


# ### old plan

# In[ ]:

# create fake data with sines and flatline parts
# create squared activation + softmax which could classify this, check if it classifies correctly
# load bhno data singlechan (C3 or CP3, try both) bandpassed right/rest classify this ...
# improve with temporal pooling 
# do same for singlechan raw data, check how good that still is, shd be worse!!
# put linear temporal layer in front, shd improve and match sine
# use several chans, see what happens
# put spatial layer after temporal, shd improve again
# maybe also retry not ignoring unclean data, but using it...

# then try just this on the kaggle data, see how good it is...
# maybe put recurrent in at temporal part or even before .. try recurrent on simple example before:
# has to filter out only one frequency (put 3 freqs together with similar amplitudes, and only middle one is informative)
# maybe first make it regression
# then see if that improves on your and kaggle data over normal temporal layer


# ## Recurrent model

# In[ ]:

# data coming in, multiply with filter lets say 15 length for now
# next layer squares output 
# next layer weights linearly + softmax (later: and also gets its own output again, either after linear or after softmax.... 
# probably after softmax)



# ## Convolution :)

# ## All sensors

# In[1177]:


from blocks.bricks.base import application, Brick
from blocks.bricks import Sequence

from blocks.utils import pack
class SequenceAll(Sequence):
    """Sequence Class that has an optional argument for returning all outputs"""
    @application
    def apply(self, return_all, *args):
        child_input = args
        all_outputs = []
        for application_method in self.application_methods:
            output = application_method(*pack(child_input))
            all_outputs.append(output)
            child_input = output
        if return_all:
            return all_outputs
        else:
            return output
    
    
class ApplyLambda(Brick):
    def __init__(self, apply_func):
        self.apply_func = apply_func
        super(ApplyLambda, self).__init__()
        
    @application(inputs=['input_'], outputs=['output'])
    def apply(self, input_):
        """Apply the linear transformation.
        Parameters
        ----------
        input_ : :class:`~tensor.TensorVariable`
            The input on which to apply the transformation
        Returns
        -------
        output : :class:`~tensor.TensorVariable`
            The transformed input plus optional bias
        """
        output = self.apply_func(input_)
        return output


# In[1376]:

from collections import OrderedDict
from blocks.bricks.cost import CategoricalCrossEntropy
from blocks.roles import WEIGHT, PARAMETER, BIAS
from blocks.graph import ComputationGraph
from blocks.filter import VariableFilter
from blocks.bricks.conv import Convolutional
from motor_imagery_learning.mypylearn2.learning_rule_adam import Adam
from motor_imagery_learning.mypylearn2.pool import  log_sum

n_temporal_units = 2
n_spatial_units = 2
temporal_length = 20
pool_length = 50
n_classes = 7
n_sensors = 32

sequence = SequenceAll([Convolutional(filter_size=(temporal_length,1), num_filters=n_temporal_units,
                           num_channels=1, tied_biases=True,
                          weights_init=initialization.IsotropicGaussian(std=0.05),
       biases_init=initialization.Constant(0)).apply,
          Convolutional(filter_size=(1,n_sensors), num_filters=n_spatial_units,
                               num_channels=n_temporal_units, tied_biases=True,
                              weights_init=initialization.IsotropicGaussian(std=0.05),
           biases_init=initialization.Constant(0)).apply,
          ApplyLambda(lambda inputs: inputs * inputs).apply,
        ApplyLambda(lambda inputs: log_sum(inputs, (pool_length, 1), pool_stride=(1,1), image_shape=None)).apply,
        ApplyLambda(lambda inputs: inputs.dimshuffle(2,1,0,3).reshape((-1,n_spatial_units))).apply,
      Linear(input_dim=n_temporal_units, output_dim=n_classes, 
                weights_init=initialization.IsotropicGaussian(std=0.05),
       biases_init=initialization.Constant(0)).apply,
          Softmax().apply,
    ])

sequence.initialize()

x = tensor.tensor4('x', dtype='floatX')
out = sequence.apply(False, x)

y = tensor.lmatrix('targets')

cost = CategoricalCrossEntropy().apply(y.flatten(), out)
cg = ComputationGraph(cost)

params = VariableFilter(roles=[PARAMETER])(cg.variables)
cost_grad = theano.grad(cost, params)

param_updates = OrderedDict()

param_to_grad = OrderedDict()
for param, grad in zip(params, cost_grad):
    param_to_grad[param] = grad
    
adam_rule = Adam()

adam_updates = adam_rule.get_updates(learning_rate=None, grads=param_to_grad)
fixed_sgd_update = theano.function([x,y], adam_updates.values(), updates=adam_updates)

out_func = theano.function([x], out)

cost_func = theano.function([x,y], cost)


# ### Kaggle Data

# In[1380]:

from motor_imagery_learning.mywyrm.processing import bandpass_cnt
X, y = load_train(1,1)
X_train = np.asarray(X.astype(float))
fs = 500.0
timesteps_in_ms =np.arange(len(X)) * 1000.0 / fs
cnt = wyrm.types.Data(X_train, [timesteps_in_ms, X.keys()], ['time', 'channel'], 
            ['ms', '#'])
cnt.fs = 500.0
#lowpass to remove drifts, highpass for later subsampling all forward filtering
cnt = bandpass_cnt(cnt, 0.5, 100)
X_train = cnt.data
X_train = (X_train - np.mean(X_train, axis=0)) / np.std(X_train, axis=0)
#subsample
X_train = X_train[::2]
y = y[::2]


# In[1381]:

real_targets = np.atleast_2d(y.astype(int))
real_in = X_train
n_border_loss = temporal_length + pool_length - 2
real_in = real_in[np.newaxis,np.newaxis,:,:].astype(theano.config.floatX)
real_targets = real_targets[n_border_loss:, :].T.astype(np.int32)
# add last column for virtual class indicating no other class is active
real_targets = np.concatenate((real_targets, np.zeros((1, real_targets.shape[1]))), axis=0)
real_targets[-1,np.sum(real_targets, axis=0) == 0] = 1
real_targets = np.atleast_2d(real_targets.argmax(axis=0))


# In[1382]:

cost_func(real_in, real_targets)


# In[1266]:

from numpy.random import RandomState
predicted_out = out_func(real_in)
print cost_func(real_in, real_targets)
#print roc_auc_score(real_targets.squeeze(), predicted_out[:,1])
print np.sum(np.argmax(predicted_out, axis=1) == real_targets[0]) / float(len(real_targets[0]))


# train here
rng = RandomState(132098)
batch_size = 5000
for _ in xrange(100):
    start = rng.choice(real_targets.shape[1] - batch_size, size=1 , replace=False)[0]
    fixed_sgd_update(real_in[:,:,start:start+batch_size], real_targets[:,start:start+batch_size-n_border_loss])
    
predicted_out = out_func(real_in)
print cost_func(real_in, real_targets)
#print np.sum((predicted_out[:,1] > 0.5) == (real_targets[0,:] == 1)) / float(len(real_targets[0]))
print np.sum(np.argmax(predicted_out, axis=1) == real_targets[0]) / float(len(real_targets[0]))


# In[1389]:

pyplot.plot(real_targets[:, 2480:2650].T)


# In[1447]:

print np.flatnonzero(real_targets == 0)
print np.diff(np.flatnonzero(real_targets == 0))


# In[1454]:

trial_1_switches = np.flatnonzero(np.diff(np.flatnonzero(real_targets == 0)) != 1)

trial_starts = np.flatnonzero(real_targets == 0)[trial_1_switches]


# In[4]:

from numpy.random import RandomState
predicted_out = out_func(real_in)
print cost_func(real_in, real_targets)
print np.mean([roc_auc_score(np.int32(real_targets == i)[0], predicted_out[:,i]) for i in range(7)])
print np.sum(np.argmax(predicted_out, axis=1) == real_targets[0]) / float(len(real_targets[0]))


# train here
rng = RandomState(132098)
batch_size = 300
for _ in xrange(100):
    start = rng.choice(trial_starts, size=1 , replace=False)[0]
    fixed_sgd_update(real_in[:,:,start:start+batch_size], real_targets[:,start:start+batch_size-n_border_loss])
    
predicted_out = out_func(real_in)
print cost_func(real_in, real_targets)
print np.mean([roc_auc_score(np.int32(real_targets == i)[0], predicted_out[:,i]) for i in range(7)])
print np.sum(np.argmax(predicted_out, axis=1) == real_targets[0]) / float(len(real_targets[0]))


# In[1550]:


from motor_imagery_learning.analysis.sensor_positions import tight_Kaggle_positions
temporal_weights = params[-1].get_value()[:,:,::-1,::-1]
spat_filt_weights = params[-3].get_value()[:,:,::-1,::-1]

combined_weights = np.tensordot(spat_filt_weights, temporal_weights, axes=(1,0))

combined_weights = combined_weights.squeeze()

from motor_imagery_learning.analysis.plot_util import plot_head_signals_tight

fig = plot_head_signals_tight(combined_weights[0], X.keys(), figsize=(8,5), sensor_map=tight_Kaggle_positions)
fig = plot_head_signals_tight(combined_weights[1], X.keys(), figsize=(8,5), sensor_map=tight_Kaggle_positions)


# In[1268]:

pyplot.plot()


# ### Gujo 

# In[1111]:


normalized_data_all_sensors = ((
    dataset.bbci_set.cnt.data - np.mean(dataset.bbci_set.cnt.data, axis=1,keepdims=True)) 
    / np.std(dataset.bbci_set.cnt.data, axis=1,keepdims=True))
n_border_loss = temporal_length + pool_length - 2
inds_samples_to_predict = np.flatnonzero(np.logical_not(np.isnan(all_sample_markers)))
real_in = normalized_data_all_sensors[inds_samples_to_predict].astype(theano.config.floatX)
real_targets = np.atleast_2d(np.int32(all_sample_markers[inds_samples_to_predict] == 3))
real_in = real_in[np.newaxis,np.newaxis,:,:]
real_targets = real_targets[:, n_border_loss:]


# In[1112]:

print real_in.shape
print real_targets.shape


# In[1084]:

from numpy.random import RandomState
predicted_out = out_func(real_in)
print cost_func(real_in, real_targets)
print roc_auc_score(real_targets.squeeze(), predicted_out[:,1])
print np.sum((predicted_out[:,1] > 0.5) == (real_targets[0,:] == 1)) / float(len(real_targets[0]))

# train here
rng = RandomState(132098)
batch_size = 5000
for _ in xrange(100):
    start = rng.choice(real_targets.shape[1] - batch_size, size=1 , replace=False)[0]
    fixed_sgd_update(real_in[:,:,start:start+batch_size], real_targets[:,start:start+batch_size-n_border_loss])
    
predicted_out = out_func(real_in)
print cost_func(real_in, real_targets)
print roc_auc_score(real_targets.squeeze(), predicted_out[:,1])
print np.sum((predicted_out[:,1] > 0.5) == (real_targets[0,:] == 1)) / float(len(real_targets[0]))


# In[1076]:

temporal_weights = params[-1].get_value()[:,:,::-1,::-1]
spat_filt_weights = params[-3].get_value()[:,:,::-1,::-1]

combined_weights = np.tensordot(spat_filt_weights, temporal_weights, axes=(1,0))

combined_weights = combined_weights.squeeze()

from motor_imagery_learning.analysis.plot_util import plot_head_signals_tight

fig = plot_head_signals_tight(combined_weights[0], dataset.bbci_set.cnt.axes[1], figsize=(8,5))

fig = plot_head_signals_tight(combined_weights[1], dataset.bbci_set.cnt.axes[1], figsize=(8,5))


# ### Gujo Data

# In[970]:

n_border_loss = temporal_length + pool_length - 2
inds_samples_to_predict = np.flatnonzero(np.logical_not(np.isnan(all_sample_markers)))
real_in = normalized_data[inds_samples_to_predict].astype(theano.config.floatX)
real_targets = np.atleast_2d(np.int32(all_sample_markers[inds_samples_to_predict] == 3))
real_in = real_in[np.newaxis,np.newaxis,:,:]
real_targets = real_targets[:, n_border_loss:]


# In[983]:

from numpy.random import RandomState
predicted_out = out_func(real_in)
print cost_func(real_in, real_targets)
print roc_auc_score(real_targets.squeeze(), predicted_out[:,1])
print np.sum((predicted_out[:,1] > 0.5) == (real_targets[0,:] == 1)) / float(len(real_targets[0]))

# train here
rng = RandomState(132098)
batch_size = 5000
for _ in xrange(100):
    start = rng.choice(real_targets.shape[1] - batch_size, size=1 , replace=False)[0]
    fixed_sgd_update(real_in[:,:,start:start+batch_size], real_targets[:,start:start+batch_size-n_border_loss])
    
predicted_out = out_func(real_in)
print cost_func(real_in, real_targets)
print roc_auc_score(real_targets.squeeze(), predicted_out[:,1])
print np.sum((predicted_out[:,1] > 0.5) == (real_targets[0,:] == 1)) / float(len(real_targets[0]))


# In[973]:

pyplot.plot(params[-1].get_value().squeeze().T)


# ### Fake Data

# In[694]:

zero_data = np.zeros((20))

#pyplot.plot(zero_data.T)
sine = create_sine_signal(5, freq=1, sampling_freq=5)

#pyplot.plot(sine)
fake_data = np.concatenate((zero_data[:5], sine, zero_data[:5], sine))
pyplot.plot(fake_data)
fake_targets = np.array([0] * 5 + [1] * 5 + [0] * 5 + [1] * 5).astype(theano.config.floatX)


# In[741]:

fake_data_in = fake_data[np.newaxis,np.newaxis,:,np.newaxis].astype(theano.config.floatX)
fake_targets_in = np.atleast_2d(fake_targets).astype(np.int32)


# In[813]:

# print, train, print :D
real_in = fake_data_in
real_targets = fake_targets_in[:,4:]
predicted_out = out_func(real_in)
print cost_func(real_in, real_targets)
print roc_auc_score(real_targets.squeeze(), predicted_out[:,1])
print np.sum((predicted_out[:,1] > 0.5) == (real_targets[0,:] == 1)) / float(len(real_targets[0]))

# train here
for _ in xrange(100):
    fixed_sgd_update(real_in, real_targets)
    
predicted_out = out_func(real_in)
print cost_func(real_in, real_targets)
print roc_auc_score(real_targets.squeeze(), predicted_out[:,1])
print np.sum((predicted_out[:,1] > 0.5) == (real_targets[0,:] == 1)) / float(len(real_targets[0]))


# ## Kaggle Data

# In[531]:

os.path.join(folder, 'train/subj%d_series*_data.csv' % (subject))


# In[530]:

folder


# In[532]:

subject = 1

glob(os.path.join(folder, 'train/subj%d_series*_data.csv' % (subject)))


# In[ ]:

# read in, first subject 1
# make a pure check whether classes are differentiable by variance on C3 or Cz, 
# if two classes well separable, first try on them long linear layer from before


# In[567]:

def load_train(i_subject, i_series):
    train_folder = '../../../grasp-lift/data/train/'
    data_filename = 'subj{:d}_series{:d}_data.csv'.format(
        i_subject, i_series)
    data_file_path = os.path.join(train_folder, data_filename)
    data = pd.read_csv(data_file_path)
    # events file
    events_file_path = data_file_path.replace('_data','_events')
    # read event file
    labels= pd.read_csv(events_file_path)
    clean = data.drop(['id' ], axis=1)#remove id
    labels = labels.drop(['id' ], axis=1)#remove id
    return clean,labels


# In[546]:

import wyrm.types


# In[571]:




# In[577]:

from collections import OrderedDict
from blocks.bricks.cost import CategoricalCrossEntropy
from blocks.roles import WEIGHT, PARAMETER, BIAS
from blocks.graph import ComputationGraph
from blocks.filter import VariableFilter
from motor_imagery_learning.mypylearn2.learning_rule_adam import Adam
n_temporal_units = 2
x = tensor.matrix('x', dtype='floatX')
linear = Linear(input_dim=20, output_dim=n_temporal_units, 
                weights_init=initialization.IsotropicGaussian(std=0.05),
       biases_init=initialization.Constant(0))

linear.initialize()
out_linear = linear.apply(x)
squared = out_linear * out_linear
linear_soft = Linear(input_dim=n_temporal_units, output_dim=2, 
                weights_init=initialization.IsotropicGaussian(std=0.05),
       biases_init=initialization.Constant(0))

linear_soft.initialize()
out_soft_linear = linear_soft.apply(squared)
soft_max = Softmax()
out = soft_max.apply(out_soft_linear)


y = tensor.lmatrix('targets')

cost = CategoricalCrossEntropy().apply(y.flatten(), out)
cg = ComputationGraph(cost)

params = VariableFilter(roles=[PARAMETER])(cg.variables)
cost_grad = theano.grad(cost, params)

param_updates = OrderedDict()

param_to_grad = OrderedDict()
for param, grad in zip(params, cost_grad):
    param_to_grad[param] = grad
    
adam_rule = Adam()

for param, grad in zip(params, cost_grad):
    param_updates[param] = param - 0.1 * grad

adam_updates = adam_rule.get_updates(learning_rate=None, grads=param_to_grad)
#for param, updated_param in zip(params, cost_grad):
#    param_updates[param] = param - 0.1 * grad

#fixed_sgd_update = theano.function([x,y], param_updates.values(), updates=param_updates)
fixed_sgd_update = theano.function([x,y], adam_updates.values(), updates=adam_updates)

out_func = theano.function([x], out)
squared_out_func = theano.function([x], squared)
cost_func = theano.function([x,y], cost)
cost_grad_func = theano.function([x,y], cost_grad)


# In[672]:

X, y = load_train(1,1)
X_train = np.asarray(X.astype(float))
X_train = (X_train - np.mean(X_train, axis=0)) / np.std(X_train, axis=0)
fs = 500.0
timesteps_in_ms =np.arange(len(X)) * 1000.0 / fs
cnt = wyrm.types.Data(X_train, [timesteps_in_ms, X.keys()], ['time', 'channel'], 
            ['ms', '#'])
real_targets = np.atleast_2d(y.astype(int))
chan_ind = np.flatnonzero(X.keys() == 'CP5')[0]
real_in = np.atleast_2d(cnt.data[:, chan_ind]).astype(np.float32)


# In[679]:

np.flatnonzero(real_targets[:,0] == 1)


# In[684]:

pyplot.plot(real_targets[:,0][1050:1500])


# In[597]:

# print, train, print :D
predicted_out = out_func(real_in)
print cost_func(real_in, real_targets)
print roc_auc_score(real_target.squeeze(), predicted_out[:,1])
print np.sum((predicted_out[:,1] > 0.5) == (real_target[0,:] == 1)) / float(len(real_target[0]))

# train here
for _ in xrange(100):
    fixed_sgd_update(real_in, real_target)
    
predicted_out = out_func(real_in)
print cost_func(real_in, real_target)
print roc_auc_score(real_target.squeeze(), predicted_out[:,1])
print np.sum((predicted_out[:,1] > 0.5) == (real_target[0,:] == 1)) / float(len(real_target[0]))


# In[662]:

class_2_inds = np.flatnonzero(real_targets[:,1] == 1)
class_6_inds = np.flatnonzero(real_targets[:,5] == 1)


# In[663]:

convolved_2 = np.convolve(real_in[0, class_2_inds], create_sine_signal(60, freq=18, sampling_freq=500))
convolved_6 = np.convolve(real_in[0, class_6_inds], create_sine_signal(60, freq=18, sampling_freq=500))


# In[656]:

pyplot.plot(convolved_2 * convolved_2, linewidth=0.7)
pyplot.plot(convolved_6 * convolved_6, linewidth=0.7)


# In[664]:

print np.mean(convolved_2 * convolved_2)
print np.mean(convolved_6 * convolved_6)
print np.std(convolved_2 * convolved_2)
print np.std(convolved_6 * convolved_6)


# In[625]:

from motor_imagery_learning.analysis.util import bps_and_freqs


# In[636]:

bp_2, freqs = bps_and_freqs(real_in[0, class_2_inds],axis=0, sampling_rate=500.0)
bp_6, freqs = bps_and_freqs(real_in[0, class_6_inds],axis=0, sampling_rate=500.0)


# In[633]:

freqs[50]


# In[639]:

pyplot.plot(freqs[50:500], bp_2[50:500], linewidth=0.7)
pyplot.plot(freqs[50:500], bp_6[50:500], linewidth=0.7)


# In[ ]:




# In[624]:

fft_class_1 = np.fft.rfft(real_in[0, class_2_inds][0:10000])


# ## GuJo Example

# In[138]:

from motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
from motor_imagery_learning.mywyrm.processing import resample_cnt, highpass_cnt
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset
from motor_imagery_learning.mywyrm.clean import BBCISetNoCleaner
from wyrm.processing import select_channels

filename = 'data/BBCI-without-last-runs/GuJoMoSc01S001R01_ds10_1-11.BBCI.mat'

filterbank_args = dict(min_freq=11, max_freq=11, last_low_freq=11, low_width=4, high_width=4)
sensor_names = get_C_sensors_sorted()

dataset = BBCIPylearnCleanFilterbankDataset(filenames=filename,
       load_sensor_names=sensor_names, cleaner=BBCISetNoCleaner(),
      cnt_preprocessors=((resample_cnt, dict(newfs= 150)), 
                         (highpass_cnt, dict(low_cut_off_hz=0.5))),
                                           **filterbank_args)


# In[142]:


dataset.load_full_set()
dataset.bbci_set.preprocess_continuous_signal()


# In[145]:

right_feet_markers = [m for m in dataset.bbci_set.cnt.markers if m[1] == 1 or m[1] == 3]


# In[155]:

right_feet_samples = [(int(np.round(m[0] * dataset.bbci_set.cnt.fs / 1000.0)), 
                       m[1]) 
                      for m in right_feet_markers]


# In[159]:

one_sensor_cnt = select_channels(dataset.bbci_set.cnt, ['C3'])


# ### Make epo by variance check - 80% auc

# In[164]:

from motor_imagery_learning.mywyrm.processing import segment_dat_fast
from sklearn.metrics import roc_auc_score
one_sensor_epo = segment_dat_fast(one_sensor_cnt,
    { '1 - Right': [1], '3 - Rest': [3]}, ival=[500,4000])

epo_var = np.var(one_sensor_epo.data, axis=(1,2))
roc_auc_score(one_sensor_epo.axes[0], epo_var)


# ### Try to predict every sample

# In[203]:

## Create sample markers for every sample, nan if no class, 1 and 3 for respective classes
# Initialize to nan
all_sample_markers = np.ones(one_sensor_cnt.data.shape[0]) * np.nan
for sample_marker in right_feet_samples:
    all_sample_markers[sample_marker[0] +75: sample_marker[0]+600] = sample_marker[1]
    
normalized_data = (one_sensor_cnt.data - np.mean(one_sensor_cnt.data)) / np.std(one_sensor_cnt.data)


# #### Check AUC on single sample predictions - 0.56

# In[206]:

inds_samples_to_predict = np.logical_not(np.isnan(all_sample_markers))

roc_auc_score(all_sample_markers[inds_samples_to_predict] == 3, 
              normalized_data[inds_samples_to_predict] * normalized_data[inds_samples_to_predict])


# In[279]:

from blocks.bricks.cost import CategoricalCrossEntropy
from blocks.roles import WEIGHT, PARAMETER, BIAS
from blocks.graph import ComputationGraph
from blocks.filter import VariableFilter
x = tensor.matrix('x', dtype='floatX')
squared = x * x
linear = Linear(input_dim=1, output_dim=2, 
                weights_init=initialization.IsotropicGaussian(std=0.05),
       biases_init=initialization.Constant(0))

linear.initialize()
out_linear = linear.apply(squared)
soft_max = Softmax()
out = soft_max.apply(out_linear)

out_func = theano.function([x], out)
squared_out_func = theano.function([x], squared)

y = tensor.lmatrix('targets')

cost = CategoricalCrossEntropy().apply(y.flatten(), out)
cg = ComputationGraph(cost)
W = VariableFilter(roles=[WEIGHT])(cg.variables)[0]
B = VariableFilter(roles=[BIAS])(cg.variables)[0]
cost_grad = theano.grad(cost, [W, B])

cost_func = theano.function([x,y], cost)
cost_grad_func = theano.function([x,y], cost_grad)


# In[285]:

inds_samples_to_predict = np.flatnonzero(np.logical_not(np.isnan(all_sample_markers)))
real_in = normalized_data[inds_samples_to_predict].astype(theano.config.floatX)
real_target = np.atleast_2d(np.int32(all_sample_markers[inds_samples_to_predict] == 3))
#normalized_data[inds_samples_to_predict]


# In[268]:

cost_func(real_in, real_target)


# In[277]:

predicted_out = out_func(real_in)
roc_auc_score(real_target.squeeze(), predicted_out[:,1])


# In[275]:

for _ in xrange(100):
    weight_grad_arr, bias_grad_arr = cost_grad_func(real_in, real_target)
    W.set_value(W.get_value() - np.array(weight_grad_arr) * 0.01)
    B.set_value(B.get_value() - np.array(bias_grad_arr) * 0.01)


# In[280]:

print W.get_value()
print B.get_value()


# ## Try with longer linear layer

# In[417]:

real_in = np.float32([normalized_data[i-10:i+10] for i in inds_samples_to_predict]).squeeze()


# In[494]:

from collections import OrderedDict
from blocks.bricks.cost import CategoricalCrossEntropy
from blocks.roles import WEIGHT, PARAMETER, BIAS
from blocks.graph import ComputationGraph
from blocks.filter import VariableFilter
from motor_imagery_learning.mypylearn2.learning_rule_adam import Adam
n_temporal_units = 2
x = tensor.matrix('x', dtype='floatX')
linear = Linear(input_dim=20, output_dim=n_temporal_units, 
                weights_init=initialization.IsotropicGaussian(std=0.05),
       biases_init=initialization.Constant(0))

linear.initialize()
out_linear = linear.apply(x)
squared = out_linear * out_linear
linear_soft = Linear(input_dim=n_temporal_units, output_dim=2, 
                weights_init=initialization.IsotropicGaussian(std=0.05),
       biases_init=initialization.Constant(0))

linear_soft.initialize()
out_soft_linear = linear_soft.apply(squared)
soft_max = Softmax()
out = soft_max.apply(out_soft_linear)


y = tensor.lmatrix('targets')

cost = CategoricalCrossEntropy().apply(y.flatten(), out)
cg = ComputationGraph(cost)

params = VariableFilter(roles=[PARAMETER])(cg.variables)
cost_grad = theano.grad(cost, params)

param_updates = OrderedDict()

param_to_grad = OrderedDict()
for param, grad in zip(params, cost_grad):
    param_to_grad[param] = grad
    
adam_rule = Adam()

for param, grad in zip(params, cost_grad):
    param_updates[param] = param - 0.1 * grad

adam_updates = adam_rule.get_updates(learning_rate=None, grads=param_to_grad)
#for param, updated_param in zip(params, cost_grad):
#    param_updates[param] = param - 0.1 * grad

#fixed_sgd_update = theano.function([x,y], param_updates.values(), updates=param_updates)
fixed_sgd_update = theano.function([x,y], adam_updates.values(), updates=adam_updates)

out_func = theano.function([x], out)
squared_out_func = theano.function([x], squared)
cost_func = theano.function([x,y], cost)
cost_grad_func = theano.function([x,y], cost_grad)


# In[497]:

param_update_vals = fixed_sgd_update(real_in, real_target)


# In[519]:

for _ in xrange(100):
    fixed_sgd_update(real_in, real_target)


# In[520]:

predicted_out = out_func(real_in)
print cost_func(real_in, real_target)
print roc_auc_score(real_target.squeeze(), predicted_out[:,1])
print np.sum((predicted_out[:,1] > 0.5) == (real_target[0,:] == 1)) / float(len(real_target[0]))


# In[521]:

pyplot.plot(params[-1].get_value())


# ## Simple Artificial Example

# In[52]:

zero_data = np.zeros((20))

#pyplot.plot(zero_data.T)
sine = create_sine_signal(5, freq=1, sampling_freq=5)

#pyplot.plot(sine)
fake_data = np.concatenate((zero_data[:5], sine, zero_data[:5], sine))
pyplot.plot(fake_data)
targets = np.array([0] * 5 + [1] * 5 + [0] * 5 + [1] * 5).astype(theano.config.floatX)


# In[90]:

from blocks.bricks import Softmax, NDimensionalSoftmax
from blocks.bricks import Linear
from blocks import initialization
from theano import tensor
x = tensor.matrix('x', dtype='floatX')
squared = x * x
linear = Linear(input_dim=1, output_dim=2, weights_init=initialization.IsotropicGaussian(std=0.05),
       biases_init=initialization.Constant(0))

linear.initialize()
out_linear = linear.apply(squared)
soft_max = Softmax()
out = soft_max.apply(out_linear)

out_func = theano.function([x], out)
squared_out_func = theano.function([x], squared)


# In[91]:


from blocks.bricks.cost import CategoricalCrossEntropy
y = tensor.lmatrix('targets')

cost = CategoricalCrossEntropy().apply(y.flatten(), out)


# In[92]:

fake_in = np.atleast_2d(fake_data).astype(theano.config.floatX).T
targets_out = np.atleast_2d(targets.astype(np.int32))


# In[119]:

from blocks.roles import WEIGHT, PARAMETER, BIAS
from blocks.graph import ComputationGraph
from blocks.filter import VariableFilter
cg = ComputationGraph(cost)
W = VariableFilter(roles=[WEIGHT])(cg.variables)[0]
B = VariableFilter(roles=[BIAS])(cg.variables)[0]
#B, W = VariableFilter(roles=[PARAMETER])(cg.variables)


# In[123]:

cost_grad = theano.grad(cost, [W, B])

cost_func = theano.function([x,y], cost)
cost_grad_func = theano.function([x,y], cost_grad)
cost_func(fake_in, targets_out)


# In[129]:

for _ in xrange(1000):
    weight_grad_arr, bias_grad_arr = cost_grad_func(fake_in, targets_out)
    W.set_value(W.get_value() - np.array(weight_grad_arr) * 0.01)
    B.set_value(B.get_value() - np.array(bias_grad_arr) * 0.01)


# In[101]:

cost_func(fake_in, targets_out)


# In[130]:

out_func(fake_in)

