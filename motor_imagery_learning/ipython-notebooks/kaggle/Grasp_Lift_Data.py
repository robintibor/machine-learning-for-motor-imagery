
# coding: utf-8

# In[715]:


import os
os.environ['FUEL_DATA_PATH'] = '/home/schirrmr/fuel-data/'#%%capture
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
#os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import theano
os.environ['FUEL_DATA_PATH'] = '/home/schirrmr/fuel-data/'
import matplotlib
from matplotlib import pyplot
from matplotlib import cm
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (8.0, 1.0)
matplotlib.rcParams['font.size'] = 7

import matplotlib.lines as mlines
import seaborn
seaborn.set_style('darkgrid')
import numpy as np
from motor_imagery_learning.analysis.data_generation import create_sine_signal
import pandas as pd
import wyrm.types


# ## Trainer

# ### To check

# * data/models/grasp-lift/only-current-feature-dropout/
# * data/models/grasp-lift/only-current-feature-logistic/ 

# In[474]:

from motor_imagery_learning.grasp_lift.print_grasp_results import GraspResultPrinter


# In[478]:

ls data/models/grasp-lift/first-test/


# In[782]:

GraspResultPrinter('data/models/grasp-lift/look-back-classes/submit/').print_results()


# In[774]:

GraspResultPrinter('data/models/grasp-lift/only-current-feature-logistic/more-time/').print_results()


# In[751]:

GraspResultPrinter('data/models/grasp-lift/only-current-feature-logistic/debug-sub/').print_results()


# In[479]:

GraspResultPrinter('data/models/grasp-lift/first-test/').print_results()


# In[700]:

GraspResultPrinter('data/models/grasp-lift/only-current-feature-dropout/').print_results()


# In[728]:

GraspResultPrinter('data/models/grasp-lift/only-current-feature-logistic/').print_results()


# In[660]:

GraspResultPrinter('data/models/grasp-lift/only-current-feature/').print_results()


# In[661]:

GraspResultPrinter('data/models/grasp-lift/look-back/').print_results()


# In[740]:

pyplot.plot(real_targets[1800:2500,0])
pyplot.plot(real_targets[1800:2500,5])


# In[ ]:

GraspLiftTrain(1, sequence, max_epochs=2, save_path='data/models/grasp-lift/second-test')#dropout=True


# In[ ]:

# saving and seeing results
# adam alpha
# continue after early stop (we can also do this later)


# In[743]:

from motor_imagery_learning.grasp_lift.train import GraspLiftTrain


# In[745]:

grasp_train = GraspLiftTrain(1,sequence,5,2,1e-3, 10,0.1, False)


# In[746]:

grasp_train.load_data()


# In[747]:

grasp_train.train_in.shape


# In[748]:

grasp_train.val_in.shape


# In[749]:

grasp_train.val_targets.shape


# In[750]:

grasp_train.train_targets.shape


# In[ ]:

grasp_train.create_and_compile_theano_graphs()


# In[ ]:

grasp_train.setup_early_stop()


# In[471]:

grasp_train.run_training()


# In[ ]:




# ## Model

# In[731]:

from collections import OrderedDict
from blocks.bricks.cost import CategoricalCrossEntropy
from blocks.roles import WEIGHT, PARAMETER, BIAS
from blocks.graph import ComputationGraph
from blocks.filter import VariableFilter
from blocks.bricks.conv import Convolutional
from motor_imagery_learning.mypylearn2.learning_rule_adam import Adam
from motor_imagery_learning.mypylearn2.pool import  log_sum
from blocks.bricks import Softmax, NDimensionalSoftmax, Logistic
from blocks.bricks import Linear
from blocks import initialization
from theano import tensor
from motor_imagery_learning.grasp_lift.bricks import Prepender

n_temporal_units = 2
n_spatial_units = 2
temporal_length = 20
pool_length = 50
n_classes = 7
n_sensors = 32
n_look_back = 25
n_look_back_classes = 30

n_border_loss = temporal_length + pool_length + n_look_back + n_look_back_classes - 4

sequence = SequenceAll([Convolutional(filter_size=(temporal_length,1), num_filters=n_temporal_units,
                           num_channels=1, tied_biases=True,
                          weights_init=initialization.IsotropicGaussian(std=0.05),
       biases_init=initialization.Constant(0)).apply,
          Convolutional(filter_size=(1,n_sensors), num_filters=n_spatial_units,
                               num_channels=n_temporal_units, tied_biases=True,
                              weights_init=initialization.IsotropicGaussian(std=0.05),
           biases_init=initialization.Constant(0)).apply,
          ApplyLambda(lambda inputs: inputs * inputs).apply,
        ApplyLambda(lambda inputs: log_sum(inputs, (pool_length, 1), pool_stride=(1,1), image_shape=None)).apply,
        Convolutional(filter_size=(n_look_back,1), num_filters=n_classes,
               num_channels=n_spatial_units, tied_biases=True,
              weights_init=initialization.IsotropicGaussian(std=0.05),
           biases_init=initialization.Constant(0)).apply,
          Logistic().apply,
        Convolutional(filter_size=(n_look_back_classes,1), num_filters=n_classes,
               num_channels=n_classes, tied_biases=True,
              weights_init=initialization.IsotropicGaussian(std=0.05),
           biases_init=initialization.Constant(0)).apply,
        ApplyLambda(lambda inputs: inputs.dimshuffle(2,1,0,3).reshape((-1,n_classes))).apply,
          Logistic().apply,
        Prepender(n_border_loss).apply,
    ])

sequence.initialize()


# In[726]:

from motor_imagery_learning.grasp_lift.bricks import SelectFirstInput
from motor_imagery_learning.grasp_lift.bricks import Prepender
from blocks.bricks.recurrent import  GatedRecurrent
lstm_dim = 20
sequence = SequenceAll([Convolutional(filter_size=(temporal_length,1), num_filters=n_temporal_units,
                           num_channels=1, tied_biases=True,
                          weights_init=initialization.IsotropicGaussian(std=0.05),
       biases_init=initialization.Constant(0)).apply,
          Convolutional(filter_size=(1,n_sensors), num_filters=n_spatial_units,
                               num_channels=n_temporal_units, tied_biases=True,
                              weights_init=initialization.IsotropicGaussian(std=0.05),
           biases_init=initialization.Constant(0)).apply,
          ApplyLambda(lambda inputs: inputs * inputs).apply,
        ApplyLambda(lambda inputs: log_sum(inputs, (pool_length, 1), pool_stride=(1,1), image_shape=None)).apply,
        Convolutional(filter_size=(n_look_back,1), num_filters=n_classes,
               num_channels=n_spatial_units, tied_biases=True,
              weights_init=initialization.IsotropicGaussian(std=0.05),
           biases_init=initialization.Constant(0)).apply,
        ApplyLambda(lambda inputs: inputs.dimshuffle(2,1,0,3).reshape((-1,n_classes))).apply,
          Logistic().apply,
         Linear(n_classes, lstm_dim * 4, name='x_to_h',
                weights_init=initialization.IsotropicGaussian(),
                biases_init=initialization.Constant(0.0)).apply,
        GatedRecurrent(lstm_dim, name='lstm',
            weights_init=initialization.IsotropicGaussian(),
            biases_init=initialization.Constant(0.0)).apply,
        Linear(lstm_dim, n_classes, name='h_to_o',
                weights_init=initialization.IsotropicGaussian(),
                biases_init=initialization.Constant(0.0)).apply,
        Logistic().apply,
        Prepender(n_border_loss).apply,
    ])

sequence.initialize()


# In[409]:

from blocks.bricks.conv import Flattener


# In[735]:


from blocks.graph import apply_dropout

from blocks.bricks.cost import CategoricalCrossEntropy, BinaryCrossEntropy

x = tensor.tensor4('x', dtype='floatX')
out = sequence.apply(False, x)

y = tensor.lmatrix('targets')

cost = BinaryCrossEntropy().apply(y, out)
cg = ComputationGraph(cost)

params = VariableFilter(roles=[PARAMETER])(cg.variables)
#cg_dropout = apply_dropout(cg, params, drop_prob=0.5)
cost_grad = theano.grad(cost, params)

param_updates = OrderedDict()

param_to_grad = OrderedDict()
for param, grad in zip(params, cost_grad):
    param_to_grad[param] = grad
    
adam_rule = Adam(2e-2)

adam_updates = adam_rule.get_updates(learning_rate=None, grads=param_to_grad)
fixed_sgd_update = theano.function([x,y], adam_updates.values(), updates=adam_updates)

out_func = theano.function([x], out)

cost_func = theano.function([x,y], cost)

cost_debug_func = theano.function([out,y], cost)


# In[669]:




# ## Try recurrent

# In[698]:

import numpy
import theano
from theano import tensor
from blocks import initialization
from blocks.bricks import Identity
from blocks.bricks.recurrent import SimpleRecurrent
x = tensor.tensor3('x')
rnn = SimpleRecurrent(
    dim=4, activation=Identity(), weights_init=initialization.Identity())
rnn.initialize()
h = rnn.apply(x)
f = theano.function([x], h)

print(f(numpy.ones((3, 1, 4), dtype=theano.config.floatX))) 


# In[703]:

import numpy
import theano
from theano import tensor
from blocks import initialization
from blocks.bricks import Identity
from blocks.bricks.recurrent import LSTM
from blocks.bricks import Linear
in_dim = 7
lstm_dim = 20
x_to_h = Linear(in_dim, lstm_dim * 4, name='x_to_h',
                weights_init=initialization.IsotropicGaussian(),
                biases_init=initialization.Constant(0.0))
lstm = LSTM(lstm_dim, name='lstm',
            weights_init=initialization.IsotropicGaussian(),
            biases_init=initialization.Constant(0.0))
h_to_o = Linear(lstm_dim, in_dim, name='h_to_o',
                weights_init=initialization.IsotropicGaussian(),
                biases_init=initialization.Constant(0.0))


# In[691]:

out_vals[100]


# In[686]:

out_vals = out_func(real_in)


# In[586]:

cost_debug_func(out_vals, real_targets)


# In[587]:

cost_func(real_in, real_targets)


# ### Read Data

# In[588]:

from motor_imagery_learning.mywyrm.processing import bandpass_cnt
from motor_imagery_learning.grasp_lift.train import preprocess_y, preproc_X
real_train = []

X, y = load_train(1,1)
X_train, train_mean, train_std = preproc_X(X, can_fit=True)
y = preprocess_y(y)
real_in = X_train

# target preprocessing
real_targets= y

for i_series in xrange(2,8):
    X, y = load_train(1,i_series)
    X_train,train_mean, train_std  = preproc_X(X, can_fit=True)
    y = preprocess_y(y) 
    real_in = np.concatenate((real_in, X_train), axis=2)
    real_targets = np.concatenate((real_targets, y), axis=0)


# In[594]:

X, y = load_train(1,8)
X_val = preproc_X(X, can_fit=False, old_mean=train_mean, old_std=train_std)
y_val = preprocess_y(y)#n_border_loss) 


# In[616]:

trial_1_inds = np.flatnonzero(real_targets[:, 0] == 1)
trial_1_switches = np.flatnonzero(np.diff(trial_1_inds) != 1)

trial_starts = trial_1_inds[trial_1_switches]


# In[598]:


print "train auc", np.mean([roc_auc_score(np.int32(real_targets[:,i] == 1), predicted_out[:,i]) for i in range(6)])


# In[736]:

from numpy.random import RandomState
from sklearn.metrics import roc_auc_score
predicted_out = out_func(real_in)
predicted_val = out_func(X_val)
print cost_func(real_in, real_targets)
print np.sum(np.argmax(predicted_out, axis=1) == real_targets[0]) / float(len(real_targets[0]))
print "train auc", np.mean([roc_auc_score(np.int32(real_targets[:,i] == 1), predicted_out[:,i]) for i in range(6)])
print "valid auc", np.mean([roc_auc_score(np.int32(y_val[:,i] == 1), predicted_val[:,i]) for i in range(6)])


# train here
rng = RandomState(132098)
batch_size = 400
for _ in xrange(100):
    start = rng.choice(trial_starts, size=1 , replace=False)[0]
    start = start - 50
    fixed_sgd_update(real_in[:,:,start:start+batch_size], real_targets[start:start+batch_size])
    
predicted_out = out_func(real_in)
predicted_val = out_func(X_val)
print cost_func(real_in, real_targets)
print np.sum(np.argmax(predicted_out, axis=1) == real_targets[0]) / float(len(real_targets[0]))
print "train auc", np.mean([roc_auc_score(np.int32(real_targets[:,i] == 1), predicted_out[:,i]) for i in range(6)])
print "valid auc", np.mean([roc_auc_score(np.int32(y_val[:,i] == 1), predicted_val[:,i]) for i in range(6)])


# ## Predict Test set 

# In[436]:

def load_and_preproc_test(i_subject, i_series):
    X_test = load_test(i_subject,i_series)
    X_test = preproc_X(X_test)
    return X_test
        


# In[425]:

def restore_original_prediction(y_out, i_subject, i_series):
    preds_for_csv = y_out[:,:6]
    # double since we halved the input
    preds_for_csv = np.repeat(preds_for_csv,2, axis=0)
    X_orig = load_test(i_subject,i_series)
    assert X_orig.shape[0] == len(preds_for_csv) or X_orig.shape[0] == len(preds_for_csv) - 1
    if X_orig.shape[0] == len(preds_for_csv) - 1:
        preds_for_csv = preds_for_csv[:-1]
    assert X_orig.shape[0] == len(preds_for_csv)
    return preds_for_csv


# In[341]:

subjects_and_preds = []
for i_subject in xrange(1,13):
    print ("Testing on Subject {:d}".format(i_subject))
    subjects_and_preds.append([])
    for i_series in (9,10):
        print ("Testing on Series {:d}".format(i_series))
        X_test = load_and_preproc_test(i_subject,i_series)
        y_out = out_func(X_test)
        #print y_out.shape
        preds_for_csv = restore_original_prediction(y_out, i_subject, i_series)
        subjects_and_preds[-1].append(preds_for_csv)
        


# In[342]:

cols = ['HandStart','FirstDigitTouch',
        'BothStartLoadPhase','LiftOff',
        'Replace','BothReleased']

# collect ids
all_ids = []
all_preds = []
for i_subject in xrange(1,13):
    for i_series in (9,10):
        id_prefix = "subj{:d}_series{:d}_".format(i_subject, i_series)
        this_preds = subjects_and_preds[i_subject-1][i_series-9] # respect offsets
        all_preds.extend(this_preds)
        this_ids = [id_prefix + str(i_sample) for i_sample in range(this_preds.shape[0])]
        all_ids.extend(this_ids)
all_ids = np.array(all_ids)
all_preds = np.array(all_preds)


# In[344]:

submission_file_name = 'data/kaggle-grasp-lift/trained_on_only_one_subject.csv'
submission = pd.DataFrame(index=all_ids,
                          columns=cols,
                          data=all_preds)

submission.to_csv(submission_file_name,index_label='id',float_format='%.3f')

