
# coding: utf-8

# In[120]:


import os
os.environ['FUEL_DATA_PATH'] = '/home/schirrmr/fuel-data/'#%%capture
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
#os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import theano
os.environ['FUEL_DATA_PATH'] = '/home/schirrmr/fuel-data/'
import matplotlib
from matplotlib import pyplot
from matplotlib import cm
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (8.0, 1.0)
matplotlib.rcParams['font.size'] = 7

import matplotlib.lines as mlines
import seaborn
seaborn.set_style('darkgrid')


# In[ ]:

# use blocks
# create a large signal and nothing, check it is predicted correctly
# create a sine signal and nothing check it is predicted corretly at all times
# create sine signals in different frequencies, check they are predicted correctly, maybe use 3 inputs or something
# (do put some noise also)
# check one potential also or alrdy the real kaggle data


# In[ ]:

# square signal
# simple model shd learn


# ## recurrent example

# In[311]:

import numpy
import theano
from theano import tensor
from blocks import initialization
from blocks.bricks import Identity
from blocks.bricks.recurrent import SimpleRecurrent
x = tensor.tensor3('x')
rnn = SimpleRecurrent(
    dim=4, activation=Identity(), weights_init=initialization.Identity())
rnn.initialize()
h = rnn.apply(x)
f = theano.function([x], h)

print(f(numpy.ones((3, 1, 3), dtype=theano.config.floatX))) 


# In[312]:

from blocks.bricks import Linear
doubler = Linear(
    input_dim=3, output_dim=3, weights_init=initialization.Identity(2),
    biases_init=initialization.Constant(0))
doubler.initialize()
h_doubler = rnn.apply(doubler.apply(x))
f = theano.function([x], h_doubler)
print(f(numpy.ones((3, 1, 3), dtype=theano.config.floatX))) 


# In[313]:

h0 = tensor.matrix('h0')
h = rnn.apply(inputs=x, states=h0)
f = theano.function([x, h0], h)
print(f(numpy.ones((3, 1, 3), dtype=theano.config.floatX),
        numpy.ones((1, 3), dtype=theano.config.floatX))) 


# In[316]:

print(f(numpy.ones((4, 1, 3), dtype=theano.config.floatX),
        numpy.ones((1, 3), dtype=theano.config.floatX))) 


# ## Without iterate

# In[319]:

h0 = tensor.matrix('h0')
h = rnn.apply(inputs=x, states=h0, iterate=False)
f = theano.function([x, h0], h)
print(f(numpy.ones((3, 1, 3), dtype=theano.config.floatX),
        numpy.ones((1, 3), dtype=theano.config.floatX))) 


# In[322]:

h0 = tensor.matrix('h0')
h = rnn.apply(inputs=x, states=h0, iterate=False)
f = theano.function([x, h0], h)
print(f(numpy.array([[[0,1,2]], [[3,4,5]], [[6,7,8]]], dtype=theano.config.floatX),
        numpy.ones((1, 3), dtype=theano.config.floatX))) 


# In[323]:

h0 = tensor.matrix('h0')
h = rnn.apply(inputs=x, states=h0, iterate=True)
f = theano.function([x, h0], h)
print(f(numpy.array([[[0,1,2]], [[3,4,5]], [[6,7,8]]], dtype=theano.config.floatX),
        numpy.ones((1, 3), dtype=theano.config.floatX))) 


# ## Mnist simple MLP example

# In[17]:

from theano import tensor
x = tensor.matrix('features')



# In[18]:

from blocks.bricks import Linear, Rectifier, Softmax
input_to_hidden = Linear(name='input_to_hidden', input_dim=784, output_dim=100)
h = Rectifier().apply(input_to_hidden.apply(x))
hidden_to_output = Linear(name='hidden_to_output', input_dim=100, output_dim=10)
y_hat = Softmax().apply(hidden_to_output.apply(h))

y = tensor.lmatrix('targets')
from blocks.bricks.cost import CategoricalCrossEntropy
cost = CategoricalCrossEntropy().apply(y.flatten(), y_hat)


# In[19]:

from blocks.bricks import WEIGHT
from blocks.graph import ComputationGraph
from blocks.filter import VariableFilter
cg = ComputationGraph(cost)
W1, W2 = VariableFilter(roles=[WEIGHT])(cg.variables)
cost = cost + 0.005 * (W1 ** 2).sum() + 0.005 * (W2 ** 2).sum()
cost.name = 'cost_with_regularization'


# In[29]:

from blocks.initialization import IsotropicGaussian, Constant
input_to_hidden.weights_init = hidden_to_output.weights_init = IsotropicGaussian(0.01)
input_to_hidden.biases_init = hidden_to_output.biases_init = Constant(0)
input_to_hidden.initialize()
hidden_to_output.initialize()


# In[21]:

#W1.get_value() 


# In[30]:

from fuel.datasets import MNIST
mnist = MNIST(("train",))


# In[31]:

from fuel.streams import DataStream
from fuel.schemes import SequentialScheme
from fuel.transformers import Flatten
data_stream = Flatten(DataStream.default_stream(
    mnist,
    iteration_scheme=SequentialScheme(mnist.num_examples, batch_size=256)))


# In[45]:

from blocks.algorithms import GradientDescent, Scale
algorithm = GradientDescent(cost=cost, parameters=cg.parameters,
                            step_rule=Scale(learning_rate=0.1))


# In[33]:

mnist_test = MNIST(("test",))
data_stream_test = Flatten(DataStream.default_stream(
    mnist_test,
    iteration_scheme=SequentialScheme(
        mnist_test.num_examples, batch_size=1024)))


# In[34]:

from blocks.extensions.monitoring import DataStreamMonitoring
monitor = DataStreamMonitoring(
    variables=[cost], data_stream=data_stream_test, prefix="test")


# In[79]:

import numpy as np


# In[112]:

extensions = [monitor, FinishAfter(after_n_epochs=1), Printing()]


# In[91]:

mnist_feature_shape = mnist._data_sources[0].shape
#mnist._data_sources[1][0:10].reshape(10, np.prod(mnist_feature_shape[1:]))


# In[96]:

mnist._data_sources[0][0:10].reshape(10, np.prod(mnist_feature_shape[1:])).shape


# In[114]:


change_recursion_limit(config.recursion_limit)
algorithm = GradientDescent(cost=cost, parameters=cg.parameters,
                            step_rule=Scale(learning_rate=0.1))
with Timer('initialization', profile):
    algorithm.initialize()



for start, stop in [(0,10), (100,200), (1000,2000), (6000,10000)]:
    batch = dict(features=mnist._data_sources[0][start:stop].reshape(stop-start, np.prod(mnist_feature_shape[1:])), 
                targets=mnist._data_sources[1][start:stop])
    algorithm.process_batch(batch)
    
    


# In[115]:

out_func = theano.function([x], y_hat)


# In[117]:

cost_func = theano.function([x, y], cost)


# In[128]:

def to_2d(input4d):
    return input4d.reshape(input4d.shape[0], -1)


# In[307]:


for start, stop in [(5,1000)]:
    batch = dict(features=to_2d(mnist._data_sources[0][start:stop]), 
                targets=mnist._data_sources[1][start:stop])
    algorithm.process_batch(batch)


# In[138]:

mnist._data_sources[1][0:3]


# In[309]:

out_func(to_2d(mnist._data_sources[0][0:3]))


# ## Old loop

# In[111]:

from blocks.utils import reraise_as, unpack, change_recursion_limit
from blocks.extensions import CallbackName
from blocks.utils.profile import Profile, Timer
from blocks.config import config
main_loop_dummy = lambda : None
def log_func(args):
    print args
main_loop_dummy.log = lambda args: log_func(args)
main_loop_dummy.log.status = dict()
main_loop_dummy.log.status['training_started'] = False
main_loop_dummy.log.status['epoch_started'] = False
main_loop_dummy.log.status['epoch_interrupt_received'] = False
main_loop_dummy.log.status['batch_interrupt_received'] = False
main_loop_dummy.log.status['epochs_done'] = False
main_loop_dummy.log.status['iterations_done'] = 0
main_loop_dummy.log.current_row = dict()

change_recursion_limit(config.recursion_limit)
for extension in extensions:
    extension.main_loop = main_loop_dummy

args = []
profile = Profile()
from blocks.algorithms import GradientDescent, Scale
algorithm = GradientDescent(cost=cost, parameters=cg.parameters,
                            step_rule=Scale(learning_rate=0.1))

    
method_name = 'before_training'
with Timer(method_name, profile):
    for extension in extensions:
        with Timer(type(extension).__name__, profile):
            extension.dispatch(CallbackName(method_name), *args)
with Timer('initialization', profile):
    algorithm.initialize()

    


for start, stop in [(0,10), (100,200), (1000,2000), (6000,10000)]:
    method_name = 'before_epoch'
    with Timer(method_name, profile):
        for extension in extensions:
            with Timer(type(extension).__name__, profile):
                extension.dispatch(CallbackName(method_name), *args)
    batch = dict(features=mnist._data_sources[0][start:stop].reshape(stop-start, np.prod(mnist_feature_shape[1:])), 
                targets=mnist._data_sources[1][start:stop])
    algorithm.process_batch(batch)


    method_name = 'after_batch'
    with Timer(method_name, profile):
        for extension in extensions:
            with Timer(type(extension).__name__, profile):
                extension.dispatch(CallbackName(method_name), *args)

    main_loop_dummy.log.status['iterations_done'] += 1




training_over = main_loop_dummy.log.current_row.get('training_finish_requested', False)


# In[77]:

epoch_iterator = (data_stream.get_epoch_iterator(as_dict=True))

batch =  next(epoch_iterator)

batch.keys()


# In[35]:

from blocks.main_loop import MainLoop
from blocks.extensions import FinishAfter, Printing
main_loop = MainLoop(data_stream=data_stream, algorithm=algorithm,
                     extensions=extensions)
main_loop.run() 


# In[ ]:


                self.status['training_started'] = True
            # We can not write "else:" here because extensions
            # called "before_training" could have changed the status
            # of the main loop.
            if self.log.status['iterations_done'] > 0:
                self.log.resume()
                self._run_extensions('on_resumption')
                self.status['epoch_interrupt_received'] = False
                self.status['batch_interrupt_received'] = False
            with Timer('training', self.profile):
                while self._run_epoch():
                    pass
        except TrainingFinish:
            self.log.current_row['training_finished'] = True
        except Exception as e:
            self._restore_signal_handlers()
            self.log.current_row['got_exception'] = traceback.format_exc()
            logger.error("Error occured during training." + error_message)
            try:
                self._run_extensions('on_error')
            except Exception:
                logger.error(traceback.format_exc())
                logger.error("Error occured when running extensions." +
                             error_in_error_handling_message)
            reraise_as(e)
        finally:
            self._restore_signal_handlers()
            if self.log.current_row.get('training_finished', False):
                self._run_extensions('after_training')
            if config.profile:
                self.profile.report()

def find_extension(self, name):
    """Find an extension with a given name.
    Parameters
    ----------
    name : str
        The name of the extension looked for.
    Notes
    -----
    Will crash if there no or several extension found.
    """
    return unpack([extension for extension in self.extensions
                   if extension.name == name], singleton=True)

def _run_epoch(self):
    if not self.status.get('epoch_started', False):
        try:
            self.log.status['received_first_batch'] = False
            self.epoch_iterator = (self.data_stream.
                                   get_epoch_iterator(as_dict=True))
        except StopIteration:
            return False
        self.status['epoch_started'] = True
        self._run_extensions('before_epoch')
    with Timer('epoch', self.profile):
        while self._run_iteration():
            pass
    self.status['epoch_started'] = False
    self.status['epochs_done'] += 1
    # Log might not allow mutating objects, so use += instead of append
    self.status['_epoch_ends'] += [self.status['iterations_done']]
    self._run_extensions('after_epoch')
    self._check_finish_training('epoch')
    return True

def _run_iteration(self):
    try:
        with Timer('read_data', self.profile):
            batch = next(self.epoch_iterator)
    except StopIteration:
        if not self.log.status['received_first_batch']:
            reraise_as(ValueError("epoch iterator yielded zero batches"))
        return False
    self.log.status['received_first_batch'] = True
    self._run_extensions('before_batch', batch)
    with Timer('train', self.profile):
        self.algorithm.process_batch(batch)
    self.status['iterations_done'] += 1
    self._run_extensions('after_batch', batch)
    self._check_finish_training('batch')
    return True

def _run_extensions(self, method_name, *args):

def _check_finish_training(self, level):
    """Checks whether the current training should be terminated.
    Parameters
    ----------
    level : {'epoch', 'batch'}
        The level at which this check was performed. In some cases, we
        only want to quit after completing the remained of the epoch.
    """
    # In case when keyboard interrupt is handled right at the end of
    # the iteration the corresponding log record can be found only in
    # the previous row.
    if (self.log.current_row.get('training_finish_requested', False) or
            self.status.get('batch_interrupt_received', False)):
        raise TrainingFinish
    if (level == 'epoch' and
            self.status.get('epoch_interrupt_received', False)):
        raise TrainingFinish

