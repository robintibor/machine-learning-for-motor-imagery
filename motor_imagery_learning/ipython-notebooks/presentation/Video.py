
# coding: utf-8

# In[1]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nimport os\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n#os.environ[\'CUDA_LAUNCH_BLOCKING\'] = \'1\'\n#os.environ[\'THEANO_FLAGS\'] = os.environ[\'THEANO_FLAGS\'] + ",profile=True"\n# switch to cpu\nos.environ[\'THEANO_FLAGS\'] = \'floatX=float32,device=cpu,nvcc.fastmath=True\'\n%load_ext autoreload\n%autoreload 2\n\nfrom motor_imagery_learning.bbci_pylearn_dataset import compute_power_spectra\nfrom motor_imagery_learning.mywyrm.clean import BBCISetNoCleaner\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset\nimport numpy as np\n\nfrom analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file\nfrom motor_imagery_learning.analysis.print_results import ResultPrinter\nfrom motor_imagery_learning.csp.print_csp_results import CSPResultPrinter\nfrom pylearn2.utils import serial\nfrom motor_imagery_learning.analysis.plot_util import (plot_nlls_for_file, \n    plot_misclasses_for_result, plot_misclasses_for_file, plot_nlls, plot_head_signals_tight)\nfrom motor_imagery_learning.analysis.results import ResultPool, DatasetAveragedResults\nfrom glob import glob\nimport datetime\nfrom motor_imagery_learning.analysis.results import compute_confusion_matrix\nfrom motor_imagery_learning.analysis.results import compute_confusion_matrix_csp\nfrom motor_imagery_learning.analysis.plot_util import plot_confusion_matrix\nfrom motor_imagery_learning.analysis.data_generation import (create_sine_signal,\n    pipeline, create_shifted_sines,put_noise, convolve_with_weight,\n    max_pool_topo_view, log_sum_pool)\nfrom motor_imagery_learning.analysis.filterbands import (\n    sum_abs_filterband_weights, compute_center_freqs, \n    compute_sum_abs_weights_for_csp_model)\nimport matplotlib\nfrom matplotlib import pyplot\nfrom matplotlib import cm\nfrom motor_imagery_learning.analysis.input_reconstruction import (\n    create_rand_input, gradient_descent_input, create_input_grad_for_wanted_class_func,\n    create_input_grad_with_smooth_penalty_for_wanted_class_func, \n    reconstruct_inputs_for_class)\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (12.0, 5.0)\nmatplotlib.rcParams[\'font.size\'] = 7\n\n#matplotlib.rcParams[\'figure.autolayout\'] = True\n\nimport matplotlib.lines as mlines\nimport seaborn\nseaborn.set_style(\'darkgrid\')')


# In[68]:

import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.patches as patches
seaborn.set_style('darkgrid')
seaborn.set_style('white')


# In[191]:

get_ipython().run_cell_magic(u'javascript', u'', u'function makeSVGDownloadable() {\n    $(\'body\').on(\'click\', \'svg\', function(){\n        var svg = this.outerHTML;\n        var b64 = btoa(svg); // or use btoa if supported\n        var linkToSVG = $("<a href-lang=\'image/svg+xml\' href=\'data:image/svg+xml;base64,\\n" + b64 +\n                          "\'title=\'file.svg\' target=\'blank\'>Download</a>");\n        $(this).after(linkToSVG);});\n}\nmakeSVGDownloadable()')


# In[215]:

ax = plt.gca()
draw_face(ax)
plt.axis('scaled')
plt.ylim(-4,4)

plt.xticks([])
plt.yticks([])
seaborn.despine(left=True, bottom=True)
draw_neurons(ax, heights=[0.26] * 3, alpha=1)
draw_neurons(ax, heights=[0.15] * 3, alpha=0.75)
draw_neurons(ax, heights=[0] * 3, alpha=0.5)
draw_neurons(ax, heights=[-0.15] * 3, alpha=0.3)
draw_neurons(ax, heights=[-0.3] * 3, alpha=0.2)

last_vals = np.sin(np.linspace(0,7,20)) * 0.5
pyplot.plot(np.linspace(-2,-1,20), last_vals+ 2.5, color=seaborn.color_palette()[0])
pyplot.plot(np.linspace(-0.5,0.5,20), last_vals+ 3, color=seaborn.color_palette()[0])
pyplot.plot(np.linspace(1,2,20), last_vals+ 2.5, color=seaborn.color_palette()[0])
pyplot.xlim(-3,3)


ellipse = Ellipse(xy=(np.sin(np.pi * -40/180.0) * 2, np.cos(np.pi * -40/180.0)* 2), width=0.3, height=0.2, angle=40,
                 facecolor='black')
ax.add_patch(ellipse)
ellipse = Ellipse(xy=(np.sin(np.pi * -0/180.0) * 2, np.cos(np.pi * -0/180.0)* 2), width=0.3, height=0.2, angle=0,
                 facecolor='black')
ax.add_patch(ellipse)
ellipse = Ellipse(xy=(np.sin(np.pi * 40/180.0) * 2, np.cos(np.pi * 40/180.0)* 2), width=0.3, height=0.2, angle=-40,
                 facecolor='black')
ax.add_patch(ellipse)
None


# In[198]:


from matplotlib.patches import Ellipse


# In[221]:

ax = plt.gca()
draw_face(ax)
plt.axis('scaled')
plt.ylim(-4,4)

plt.xticks([])
plt.yticks([])
seaborn.despine(left=True, bottom=True)
draw_neurons(ax, heights=[0.12] + [0.26] * 2, alpha=1)
draw_neurons(ax, heights=[0.06] + [0.12] * 2, alpha=0.75)
draw_neurons(ax, heights=[0] * 3, alpha=0.5)
draw_neurons(ax, heights=[-0.075] + [-0.15] * 2, alpha=0.3)
draw_neurons(ax, heights=[0.15] + [-0.3] * 2, alpha=0.2)

last_vals = np.sin(np.linspace(0,7,20)) * 0.5
high_vals = np.sin(np.linspace(0,30,20)) * 0.5
pyplot.plot(np.linspace(-2,-1,20), (last_vals * 0.25)+ 2.5 + high_vals * 0.15, color=seaborn.color_palette()[0])
pyplot.plot(np.linspace(-0.5,0.5,20), (last_vals * 0.6) + high_vals * 0.1 + 3, color=seaborn.color_palette()[0])
pyplot.plot(np.linspace(1,2,20), last_vals+ 2.5, color=seaborn.color_palette()[0])
# add sensors
#circle = plt.E((-1, 1.2 + heights[0]), radius=0.2, facecolor='black',
#                      edgecolor='None', linewidth=1, alpha=alpha)
#ax.add_patch(circle)
pyplot.xlim(-3,3)

ellipse = Ellipse(xy=(np.sin(np.pi * -40/180.0) * 2, np.cos(np.pi * -40/180.0)* 2), width=0.3, height=0.2, angle=40,
                 facecolor='black')
ax.add_patch(ellipse)
ellipse = Ellipse(xy=(np.sin(np.pi * -0/180.0) * 2, np.cos(np.pi * -0/180.0)* 2), width=0.3, height=0.2, angle=0,
                 facecolor='black')
ax.add_patch(ellipse)
ellipse = Ellipse(xy=(np.sin(np.pi * 40/180.0) * 2, np.cos(np.pi * 40/180.0)* 2), width=0.3, height=0.2, angle=-40,
                 facecolor='black')
ax.add_patch(ellipse)
None


# In[87]:

np.sin(np.linspace(0,3,10))


# In[170]:

def draw_neurons(ax, heights=0, alpha=1):
    circle_args = dict(radius=0.2, facecolor=seaborn.color_palette()[1],
                      edgecolor='None', linewidth=1, alpha=alpha)
    circle = plt.Circle((-1, 1.2 + heights[0]), **circle_args)
    ax.add_patch(circle)
    circle = plt.Circle((0, 1.4 + heights[1]), **circle_args)
    ax.add_patch(circle)
    circle = plt.Circle((1, 1.2 + heights[2]), **circle_args)
    ax.add_patch(circle)


# In[155]:

def draw_face(ax):
    
    circle = plt.Circle((0, 0), radius=2, fc='None', edgecolor='black', linewidth=2)
    ax.add_patch(circle)

    headspace = 0.2
    # draw mouth
    verts = [
        (-1.25, -1 - headspace),  # P0
        (0, -1.75 - headspace), # P1
        (1.25, -1 - headspace), # P2
        ]

    codes = [Path.MOVETO,
             Path.CURVE3,
             Path.CURVE3,
             ]



    path = Path(verts, codes)
    patch = patches.PathPatch(path, facecolor='none', lw=2)
    ax.add_patch(patch)

    # draw nose

    verts = [
        (-0.2, -0.5 - headspace),  # P0
        (0, 0 - headspace), # P1
        (0.2, -0.5 - headspace), # P2
        (0, 0) # will be ignored
        ]

    codes = [Path.MOVETO,
             Path.LINETO,
             Path.LINETO,
             Path.CLOSEPOLY
             ]



    path = Path(verts, codes)

    # add eyes
    circle = plt.Circle((-0.75, 0.5 - headspace), radius=0.25, fc='None', edgecolor='black', linewidth=1.5)
    ax.add_patch(circle)
    circle = plt.Circle((0.75, 0.5 - headspace), radius=0.25, fc='None', edgecolor='black', linewidth=1.5)
    ax.add_patch(circle)



    patch = patches.PathPatch(path, facecolor='none', lw=2)
    ax.add_patch(patch)



# ## Video stuffs

# In[153]:

from pylearn2.space import Conv2DSpace
def create_face_animation(steps=100):
    fig = pyplot.figure()
    ax = fig.gca()
    def face_animate(i_time_step):
        print i_time_step
        draw_face(ax)
        #plt.axis('scaled')
        #ax.ylim(-4,4)
        draw_neurons(ax, height=np.sin(i_time_step / 5.0) * 0.25)

        last_vals = np.sin(np.linspace(0,7,20)) * 0.5
        ax.plot(np.linspace(-2,-1,20), last_vals+ 2.5, color=seaborn.color_palette()[0])
        ax.plot(np.linspace(-0.5,0.5,20), last_vals+ 3, color=seaborn.color_palette()[0])
        ax.plot(np.linspace(1,2,20), last_vals+ 2.5, color=seaborn.color_palette()[0])
    anim = animation.FuncAnimation(fig, face_animate, frames=steps, interval=1, blit=True)
    pyplot.close(fig)
    return anim
    
#weights_anim = create_weights_animation(models, weight_nr=1, epochs=9)

def show_face_animation(fps=1, steps=100):
    face_anim = create_face_animation(steps=steps)
    return display_animation(face_anim, fps=fps)
from tempfile import NamedTemporaryFile
from matplotlib import animation

def anim_to_html(anim, fps):
    VIDEO_TAG = """<video controls>
 <source src="data:video/x-m4v;base64,{0}" type="video/mp4">
 Your browser does not support the video tag.
</video>"""
    if not hasattr(anim, '_encoded_video'):
        with NamedTemporaryFile(suffix='.mp4') as f:
            mywriter = animation.FFMpegWriter(fps=fps, extra_args=['-pix_fmt', 'yuv420p', '-acodec', 'libfaac', 
                                                          '-vcodec', 'libx264'])
            anim.save(f.name, writer=mywriter)
            video = open(f.name, "rb").read()
        anim._encoded_video = video.encode("base64")
    
    return VIDEO_TAG.format(anim._encoded_video)
from IPython.display import HTML

def display_animation(anim, fps):
    pyplot.close(anim._fig)
    return HTML(anim_to_html(anim, fps))


# In[157]:



fig = plt.figure()
fig.set_dpi(100)
fig.set_size_inches(7, 6.5)

ax = plt.axes(xlim=(0, 10), ylim=(0, 10))
patch = plt.Circle((5, -5), 0.75, fc='y')

def init():
    patch.center = (5, 5)
    ax.add_patch(patch)
    return patch,

def animate(i):
    x, y = patch.center
    x = 5 + 3 * np.sin(np.radians(i))
    y = 5 + 3 * np.cos(np.radians(i))
    patch.center = (x, y)
    return patch,

anim = animation.FuncAnimation(fig, animate, 
                               init_func=init, 
                               frames=360, 
                               interval=20,
                               blit=True)


# In[158]:

with open('ballvideo.mp4', 'w') as f:
    mywriter = animation.FFMpegWriter(fps=fps, extra_args=['-pix_fmt', 'yuv420p', '-acodec', 'libfaac', 
                                                  '-vcodec', 'libx264'])
    anim.save(f.name, writer=mywriter)


# In[159]:

anim.save('animation.mp4', fps=30, 
          extra_args=['-vcodec', 'h264', 
                      '-pix_fmt', 'yuv420p'])

