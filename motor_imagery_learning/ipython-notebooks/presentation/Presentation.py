
# coding: utf-8

# In[230]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nimport os\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n#os.environ[\'CUDA_LAUNCH_BLOCKING\'] = \'1\'\n#os.environ[\'THEANO_FLAGS\'] = os.environ[\'THEANO_FLAGS\'] + ",profile=True"\n# switch to cpu\n#os.environ[\'THEANO_FLAGS\'] = \'floatX=float32,device=cpu,nvcc.fastmath=True\'\n%load_ext autoreload\n%autoreload 2\n\nfrom motor_imagery_learning.bbci_pylearn_dataset import compute_power_spectra\nfrom motor_imagery_learning.mywyrm.clean import BBCISetNoCleaner\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset\nimport numpy as np\n\nfrom analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file\nfrom motor_imagery_learning.analysis.print_results import ResultPrinter\nfrom motor_imagery_learning.csp.print_csp_results import CSPResultPrinter\nfrom pylearn2.utils import serial\nfrom motor_imagery_learning.analysis.plot_util import (plot_nlls_for_file, \n    plot_misclasses_for_result, plot_misclasses_for_file, plot_nlls, plot_head_signals_tight)\nfrom motor_imagery_learning.analysis.results import ResultPool, DatasetAveragedResults\nfrom glob import glob\nimport datetime\nfrom motor_imagery_learning.analysis.results import compute_confusion_matrix\nfrom motor_imagery_learning.analysis.results import compute_confusion_matrix_csp\nfrom motor_imagery_learning.analysis.plot_util import plot_confusion_matrix\nfrom motor_imagery_learning.analysis.data_generation import (create_sine_signal,\n    pipeline, create_shifted_sines,put_noise, convolve_with_weight,\n    max_pool_topo_view, log_sum_pool)\nfrom motor_imagery_learning.analysis.filterbands import (\n    sum_abs_filterband_weights, compute_center_freqs, \n    compute_sum_abs_weights_for_csp_model)\nimport matplotlib\nfrom matplotlib import pyplot\nfrom matplotlib import cm\nfrom motor_imagery_learning.analysis.input_reconstruction import (\n    create_rand_input, gradient_descent_input, create_input_grad_for_wanted_class_func,\n    create_input_grad_with_smooth_penalty_for_wanted_class_func, \n    reconstruct_inputs_for_class)\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (12.0, 5.0)\nmatplotlib.rcParams[\'font.size\'] = 7\n\n#matplotlib.rcParams[\'figure.autolayout\'] = True\n\nimport matplotlib.lines as mlines\nimport seaborn\nseaborn.set_style(\'darkgrid\')')


# In[22]:

get_ipython().run_cell_magic(u'javascript', u'', u'function makeSVGDownloadable() {\n    $(\'body\').on(\'click\', \'svg\', function(){\n        var svg = this.outerHTML;\n        var b64 = btoa(svg); // or use btoa if supported\n        var linkToSVG = $("<a href-lang=\'image/svg+xml\' href=\'data:image/svg+xml;base64,\\n" + b64 +\n                          "\'title=\'file.svg\' target=\'blank\'>Download</a>");\n        $(this).after(linkToSVG);});\n}\nmakeSVGDownloadable()')


# In[4]:

from motor_imagery_learning.mywyrm.processing import resample_cnt, highpass_cnt, common_average_reference_cnt
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset
from motor_imagery_learning.mypylearn2.preprocessing import RestrictToTwoClasses
from wyrm.processing import apply_csp
from motor_imagery_learning.analysis.plot_util import plot_sensor_signals
from motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
from mywyrm.plot import ax_scalp,get_channelpos, CHANNEL_10_20_exact, CHANNEL_10_20
import matplotlib.cm as cmap

def get_gujo_data():
    gujo_epo = get_gujo_epo()
    restricted_sensor_names = ['Cp1', 'Cz', 'Cp2']
    few_sensor_epo = select_channels(gujo_epo, restricted_sensor_names, chanaxis=-1)
    class_pair = [0,1]#[0,1]
    two_class_epo = select_classes(few_sensor_epo, class_pair) 
    timeval_epo = select_ival(two_class_epo, [500,4000])    
    n_trials =  timeval_epo.data.shape[0]
    train_epo = select_epochs(timeval_epo, range(0, int(np.floor(n_trials * 0.9))))
    test_epo = select_epochs(timeval_epo, range(int(np.floor(n_trials * 0.9)), n_trials))
    return two_class_epo, train_epo, test_epo
    
def get_gujo_epo():
    """Get gujo, here still all C sensors (for proper common average reference).
    Afterwards """
    filename = 'data/BBCI-without-last-runs/GuJoMoSc01S001R01_ds10_1-11.BBCI.mat'

    filterbank_args = dict(min_freq=11, max_freq=11, last_low_freq=11, low_width=4, high_width=4)
    sensor_names = get_C_sensors_sorted()

    dataset = BBCIPylearnCleanFilterbankDataset(filenames=filename,
           load_sensor_names=sensor_names, cleaner=BBCISetNoCleaner(),
          cnt_preprocessors=((resample_cnt, dict(newfs= 300)), 
                             (highpass_cnt, dict(low_cut_off_hz=0.5)),
                            (common_average_reference_cnt, dict())),
                                               **filterbank_args)

    dataset.load_bbci_set()
    epo = dataset.bbci_set.epo
    epo.data = np.squeeze(epo.data)
    epo.class_names = ['Right Hand', 'Left Hand', 'Rest', 'Feet']
    epo.axes = epo.axes[0:3] # remove filterband axis
    return epo


from wyrm.processing import select_classes, select_epochs, calculate_csp, select_channels, select_ival


def get_csp_filters_train_test(train_epo):
    filters, patterns, variances = calculate_csp(train_epo)
    return filters

def get_normal_and_csp_filtered_data(two_class_epo, filters):
    columns =[0,-1]
    n_trials =  two_class_epo.data.shape[0]
    test_epo_full = select_epochs(two_class_epo, range(int(np.floor(n_trials * 0.9)), n_trials))
    csp_filtered_full = apply_csp(test_epo_full, filters, columns)
    return test_epo_full, csp_filtered_full
def plot_combined_example(test_epo_full, csp_filtered_full):
    combined_sensor_trials = np.concatenate((test_epo_full.data[36], test_epo_full.data[37]))
    combined_csp_trials = np.concatenate((csp_filtered_full.data[36], csp_filtered_full.data[37]))
    combined_both = np.concatenate((combined_sensor_trials, combined_csp_trials), axis=1)
    plot_sensor_names =  test_epo_full.axes[2].tolist() + ['CSP1', 'CSP2']
    fig = plot_sensor_signals(combined_both.T, sensor_names=plot_sensor_names, figsize=(8,4))
    for ax in fig.axes:
        ax.axvline(x=1200, color='black', lw=1.25)

    pyplot.plot([0.125, 0.9], [0.43,0.43], color='grey',
        lw=1,transform=fig.transFigure, clip_on=False, linestyle="-")
    pyplot.text(0.25,1.2,"Left Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
    pyplot.text(0.75,1.2,"Right Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
    pyplot.xticks(np.arange(0,2401,300), 
          [0,1000,2000,3000] + ['4000/0'] + [1000,2000,3000,4000])
    pyplot.xlabel("Time [ms]")
    fig.axes[1].axvspan(300, 1200, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
    fig.axes[2].axvspan(300, 1200, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
    fig.axes[3].axvspan(300, 1200, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
    fig.axes[4].axvspan(300, 1200, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
    fig.axes[1].axvspan(1500, 2400, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
    fig.axes[2].axvspan(1500, 2400, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
    fig.axes[3].axvspan(1500, 2400, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
    fig.axes[4].axvspan(1500, 2400, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
    return fig
    
def plot_csp_example(csp_filtered_full):
    combined_csp_trials = np.concatenate((csp_filtered_full.data[36], 
                                          csp_filtered_full.data[37]))
    plot_sensor_names =  ['CSP1', 'CSP2']
    fig = plot_sensor_signals(combined_csp_trials.T, sensor_names=plot_sensor_names,
                              figsize=(8,2))
    for ax in fig.axes:
        ax.axvline(x=1200, color='black', lw=1.25)

    pyplot.text(0.25,1.2,"Left Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
    pyplot.text(0.75,1.2,"Right Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
    pyplot.xticks(np.arange(0,2401,300), 
          [0,1000,2000,3000] + ['4000/0'] + [1000,2000,3000,4000])
    
    pyplot.xlabel("Time [ms]")
    return fig
    
def plot_sensor_example(test_epo_full):
    combined_sensor_trials = np.concatenate((test_epo_full.data[36], 
                                             test_epo_full.data[37]))
    plot_sensor_names =  test_epo_full.axes[2].tolist()
    fig = plot_sensor_signals(combined_sensor_trials.T, sensor_names=plot_sensor_names, 
                              figsize=(8,2.5))
    for ax in fig.axes:
        ax.axvline(x=1200, color='black', lw=1.25)

    pyplot.text(0.25,1.2,"Left Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
    pyplot.text(0.75,1.2,"Right Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
    pyplot.xticks(np.arange(0,2401,300), 
          [0,1000,2000,3000] + ['4000/0'] + [1000,2000,3000,4000])
    pyplot.xlabel("Time [ms]")
    
    # highlight decreases and increases
    fig.axes[1].axvspan(300, 1200, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
    fig.axes[2].axvspan(300, 1200, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
    fig.axes[1].axvspan(1500, 2400, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
    fig.axes[2].axvspan(1500, 2400, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
    return fig
    
def plot_csp_filters(filters):
    chan_pos_list = CHANNEL_10_20
    figsize=(6,4)
    fig, axes = pyplot.subplots(1,2,figsize=figsize)
    ax_scalp_example(filters[:, 0], test_epo_full.axes[2], ax=axes[0], 
             colormap=cmap.coolwarm, annotate=True, chan_pos_list=chan_pos_list)
    ax_scalp_example(filters[:, -1], test_epo_full.axes[2], ax=axes[1], 
             colormap=cmap.coolwarm, annotate=True, chan_pos_list=chan_pos_list)
    return fig


from scipy import interpolate
def ax_scalp_example(v, channels, 
    ax=None, annotate=False, vmin=None, vmax=None, colormap=None,
    chan_pos_list=CHANNEL_10_20, fontsize=12):
    """ scalp plot just for example.. wihtout scalp :D
    """
    if ax is None:
        ax = plt.gca()
    assert len(v) == len(channels), "Should be as many values as channels"
    if vmin is None:
        # added by me (robintibor@gmail.com)
        assert vmax is None
        vmin, vmax = -np.max(np.abs(v)), np.max(np.abs(v))
    # what if we have an unknown channel?
    points = [get_channelpos(c, chan_pos_list) for c in channels]
    for c in channels:
        assert get_channelpos(c, chan_pos_list) is not None, ("Expect " + c + " "
            "to exist in positions")
    z = [v[i] for i in range(len(points))]
    # calculate the interpolation
    x = [i[0] for i in points]
    y = [i[1] for i in points]
    # interplolate the in-between values
    xx = np.linspace(min(x), max(x), 500)
    yy = np.linspace(min(y), max(y), 500)
    xx, yy = np.meshgrid(xx, yy)
    f = interpolate.LinearNDInterpolator(list(zip(x, y)), z)
    zz = f(xx, yy)
    # draw the contour map
    ax.contourf(xx, yy, zz, 20, vmin=vmin, vmax=vmax, cmap=colormap)
    ax.set_frame_on(False)
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    # draw the channel names
    if annotate:
        for i in zip(channels, list(zip(x, y))):
            channel = i[0]
            this_x, this_y = i[1]
            if channel == 'CP1':
                horizontalalignment="right"
            elif channel == 'CP2':
                horizontalalignment="left"
            elif channel == 'Cz':
                horizontalalignment="center"
            else:
                assert False
            ax.annotate(" " + channel, (this_x, this_y),
                        horizontalalignment=horizontalalignment,
                       fontsize=fontsize)
    ax.set_aspect(1)
    
    pyplot.subplots_adjust(wspace=0.5)
    return ax
    
from motor_imagery_learning.analysis.data_generation import randomly_shifted_sines
import scipy
from numpy.random import RandomState

def get_noisy_sine_signal():
    rng = RandomState(np.uint64(hash('noise_for_csp_2')))

    sine_signal = np.sin(np.linspace(0, 2*np.pi, 100))
    #class_1_signal = np.outer([0.4,0.6], sine_signal) + rng.randn(100) * 0.1
    #class_2_signal = np.outer([0.6,0.4], sine_signal) + rng.randn(100) * 0.1
    class_1_signal = np.outer([0.6,0.8], sine_signal) + rng.randn(100) * 0.1
    class_2_signal = np.outer([0.8,0.6], sine_signal) + rng.randn(100) * 0.1

    class_1_signal = class_1_signal - np.mean(class_1_signal, axis=1, keepdims=True)
    class_2_signal = class_2_signal - np.mean(class_2_signal, axis=1, keepdims=True)
    #class_1_signal = class_1_signal / np.std(class_1_signal, axis=1, keepdims=True)
    #class_2_signal = class_2_signal / np.std(class_2_signal, axis=1, keepdims=True)
    return class_1_signal, class_2_signal

def compute_csp(class_1_signal, class_2_signal):
    #pyplot.plot(sine_signal)
    c1 = np.cov(class_1_signal)
    c2 = np.cov(class_2_signal)
    #d, v = scipy.linalg.eig(c1-c2, c1+c2)
    #d, v = scipy.linalg.eig(c1-c2, c1 +c2)
    d, v = scipy.linalg.eig(c1, c2)
    d = d.real
    # make sure the eigenvalues and -vectors are correctly sorted
    indx = np.argsort(d)
    # reverse
    indx = indx[::-1]

    d = d.take(indx)
    v = v.take(indx, axis=1)
    a = scipy.linalg.inv(v).transpose()
    
    csp_filtered_class_1 = np.dot(class_1_signal.T, v)
    csp_filtered_class_2 = np.dot(class_2_signal.T, v)
    
    return v, d, csp_filtered_class_1, csp_filtered_class_2

def plot_csp_computation_example(class_1_signal, class_2_signal, eigenvectors, 
                             csp_filtered_class_1, csp_filtered_class_2):
    fig = pyplot.figure(figsize=(2,2))
    pyplot.scatter(class_1_signal[0], class_1_signal[1], color=seaborn.color_palette()[2])
    pyplot.scatter(class_2_signal[0], class_2_signal[1], color=seaborn.color_palette()[3])
    pyplot.xlim(-1.2, 1.2)
    pyplot.ylim(-1.2, 1.2)
    pyplot.xticks((-1.0,-0.5,0,1.0,0.5))
    pyplot.xlabel('Sensor 1')
    pyplot.ylabel('Sensor 2')
    pyplot.legend(('Class 1', 'Class 2'),loc='right', bbox_to_anchor=(1.7, 0.55))
    pyplot.plot([0,eigenvectors[0,0] * 0.5], [0,eigenvectors[1,0] * 0.5], color=seaborn.color_palette()[2])
    pyplot.plot([0,eigenvectors[0,1] * 0.5], [0,eigenvectors[1,1] * 0.5], color=seaborn.color_palette()[3] )
    pyplot.figure(figsize=(2,2))

    pyplot.scatter(csp_filtered_class_1[:,0], csp_filtered_class_1[:,1], color=seaborn.color_palette()[2])
    pyplot.scatter(csp_filtered_class_2[:,0], csp_filtered_class_2[:,1], color=seaborn.color_palette()[3])
    pyplot.xlim(-0.4, 0.4)
    pyplot.ylim(-0.4, 0.4)
    pyplot.xticks((-0.4,-0.2, 0,0.2,0.4))
    pyplot.xlabel('CSP 1')
    pyplot.ylabel('CSP 2')
    pyplot.legend(('Class 1', 'Class 2'),loc='right', bbox_to_anchor=(1.7, 0.55))
    return fig


# In[5]:

from motor_imagery_learning.csp.train_csp import generate_filterbank 
from motor_imagery_learning.bbci_pylearn_dataset import (
    BBCIPylearnCleanFilterbankDataset, BBCIPylearnCleanDataset)
from motor_imagery_learning.mywyrm.processing import highpass_cnt

def generate_single_sensor_data(filename, chan, min_freq=75, max_freq=75, low_width=14, last_low_freq=75,high_width=2):
    filterbank_args = dict(min_freq=75, max_freq=75, low_width=6, last_low_freq=75,high_width=2)
    generate_filterbank(**filterbank_args)
    higher_dataset = BBCIPylearnCleanFilterbankDataset(filenames=filename,
                               load_sensor_names=[chan], cleaner=BBCISetNoCleaner(),
                                **filterbank_args)
    higher_dataset.load()
    classes = np.argmax(higher_dataset.y, axis=1)
    higher_topo_view = higher_dataset.get_topological_view()
    higher_class_1_topo_view = higher_topo_view[classes == 0]
    higher_class_3_topo_view = higher_topo_view[classes == 2]
    lower_dataset = BBCIPylearnCleanFilterbankDataset(filenames=filename,
                               load_sensor_names=[chan], cleaner=BBCISetNoCleaner(),
                                min_freq=11, max_freq=11, low_width=2, last_low_freq=11,high_width=2 )

    lower_dataset.load()
    lower_topo_view = lower_dataset.get_topological_view()
    lower_class_1_topo_view = lower_topo_view[classes == 0]
    lower_class_3_topo_view = lower_topo_view[classes == 2]
    return lower_class_1_topo_view, lower_class_3_topo_view, higher_class_1_topo_view, higher_class_3_topo_view

def generate_single_sensor_raw_data(filename, chan):
    raw_dataset = BBCIPylearnCleanDataset(filenames=filename,
                               load_sensor_names=[chan], cleaner=BBCISetNoCleaner(),
                               cnt_preprocessors=[(highpass_cnt, {'low_cut_off_hz': 0.5})]
        )

    raw_dataset.load()
    classes = np.argmax(raw_dataset.y, axis=1)
    raw_topo_view = raw_dataset.get_topological_view()
    raw_class_1_topo_view = raw_topo_view[classes == 0]
    raw_class_3_topo_view = raw_topo_view[classes == 2]
    return raw_class_1_topo_view, raw_class_3_topo_view


#trial nr 26
def plot_single_sensor_low(low_1_topo, low_3_topo,trial_nr,figoptions={}):
    fig, axes = pyplot.subplots(1,2,sharex=True, sharey=True, **figoptions)
    axes[0].plot(np.squeeze(low_3_topo[trial_nr]))
    axes[1].plot(np.squeeze(low_1_topo[trial_nr]))
    axes[0].set_title('Rest')
    axes[1].set_title('Right Hand')
    pyplot.xticks(range(0,2001,500), range(0,4001,1000))
    fig.text(0.5, 0.01, 'Time [ms]', ha='center', va='top', fontsize=12)
    fig.text(0.0, 0.5, 'Voltage', va='center', ha='left', fontsize=12,
             rotation='vertical')
    pyplot.tight_layout()
    fig.suptitle('CP3, 9-13 Hz', fontsize=16, y=1.08)
    return fig
    
def plot_single_sensor_high(high_1_topo, high_3_topo, trial_nr,figoptions={}):
    fig, axes = pyplot.subplots(1,2,sharex=True, sharey=True, **figoptions)
    axes[0].plot(np.squeeze(high_3_topo[trial_nr]))
    axes[1].plot(np.squeeze(high_1_topo[trial_nr]))
    axes[0].set_title('Rest')
    axes[1].set_title('Right Hand')
    pyplot.xticks(range(0,2001,500), range(0,4001,1000))
    fig.text(0.5, 0.01, 'Time [ms]', ha='center', va='top', fontsize=12)
    fig.text(0.0, 0.5, 'Voltage', va='center', ha='left', fontsize=12,
             rotation='vertical')
    pyplot.tight_layout()
    fig.suptitle('CP3, 72-78Hz', fontsize=16, y=1.08)
    return fig

def plot_single_sensor_raw(raw_1_trial, raw_3_trial, figoptions={}, plotoptions={},
                                      xlabel='Time [ms]',
                                      ylabel='Voltage'):
    fig, axes = pyplot.subplots(1,2,sharex=True, sharey=True, **figoptions)
    axes[0].plot(np.squeeze(raw_3_trial), **plotoptions)
    axes[1].plot(np.squeeze(raw_1_trial), **plotoptions)
    axes[0].set_title('Rest')
    axes[1].set_title('Right Hand')
    pyplot.xticks(range(0,2001,500), range(0,4001,1000))
    fig.text(0.5, 0.01, xlabel, ha='center', va='top', fontsize=12)
    if ylabel is not None:
        fig.text(0.0, 0.5, ylabel, va='center', ha='left', fontsize=12,
                 rotation='vertical')
    pyplot.tight_layout()
    fig.suptitle('Raw signal and filter to convolve', fontsize=16, y=1.08)
    return fig

def plot_single_sensor_raw_var_samples(raw_1_trial, raw_3_trial, 
        samples_per_trial=None, figoptions={},
                                      xlabel='Time [ms]',
                                      ylabel='Voltage'):
    
    fig, axes = pyplot.subplots(1,2,sharex=True, sharey=True, **figoptions)
    axes[0].plot(np.squeeze(raw_3_trial))
    axes[1].plot(np.squeeze(raw_1_trial))
    axes[0].set_title('Rest')
    axes[1].set_title('Right Hand')
    #pyplot.xticks(range(0,2001,500), range(0,4001,1000))
    fig.text(0.5, 0.01, xlabel, ha='center', va='top', fontsize=12)
    if ylabel is not None:
        fig.text(0.0, 0.5, ylabel, va='center', ha='left', fontsize=12,
                 rotation='vertical')
    pyplot.tight_layout()
    return fig


# In[6]:

low_1_topo, low_3_topo, high_1_topo, high_3_topo = generate_single_sensor_data(
    'data/BBCI-without-last-runs/KaUsMoSc1S001R01_ds10_1-11.BBCI.mat', 'CP3')
fig = plot_single_sensor_low(low_1_topo, low_3_topo,trial_nr=26, figoptions=dict(figsize=(8,2)))

fig.axes[0].set_yticklabels([])
None


# In[21]:

fig = plot_single_sensor_high(high_1_topo, high_3_topo, trial_nr=26, figoptions=dict(figsize=(8,2)))
fig.axes[0].axvspan(500, 1500, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
fig.axes[1].axvspan(500, 1500, color=seaborn.color_palette()[2], alpha=0.2, lw=0)

fig.axes[0].set_yticklabels([])
#pyplot.tight_layout()
#fig.subplots_adjust(bottom=0.15, top=0.5)
#fig.savefig('introduction-high-freq-change.svg')
None


# In[24]:

two_class_epo, train_epo, test_epo = get_gujo_data()
filters = get_csp_filters_train_test(train_epo)
test_epo_full, csp_filtered_full = get_normal_and_csp_filtered_data(two_class_epo, filters)


# In[25]:


fig = plot_combined_example(test_epo_full, csp_filtered_full)

fig.text(0.04, 0.5, 'Voltage', va='center', rotation='vertical', 
         fontsize=10)
None


# In[284]:

fig = plot_sensor_signals(test_epo_full.data[37].T, 
                          sensor_names=test_epo_full.axes[2].tolist(),
                         figsize=(4,2))
fig.suptitle('Right Hand', fontsize=16, y=1.02)
fig.axes[1].axvspan(300, 1200, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
fig.axes[2].axvspan(300, 1200, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
pyplot.xticks(np.arange(0,1201,300), [0,1000,2000,3000,4000])
pyplot.xlim(0,1200)
pyplot.xlabel("Time [ms]")

fig.text(0.0, 0.5, 'Voltage', va='center', rotation='vertical', 
         fontsize=10)
None

fig = plot_sensor_signals(test_epo_full.data[36].T, 
                          sensor_names=test_epo_full.axes[2].tolist(),
                         figsize=(4,2))
fig.suptitle('Left Hand', fontsize=16, y=1.02)
fig.axes[1].axvspan(300, 1200, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
fig.axes[2].axvspan(300, 1200, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
pyplot.xticks(np.arange(0,1201,300), [0,1000,2000,3000,4000])
pyplot.xlim(0,1200)
pyplot.xlabel("Time [ms]")

fig.text(0.0, 0.5, 'Voltage', va='center', rotation='vertical', 
         fontsize=10)
None

fig = plot_sensor_signals(csp_filtered_full.data[37].T, 
                          sensor_names=['CSP1', 'CSP2'],
                         figsize=(4,4/3.0))
fig.suptitle('Right Hand', fontsize=16, y=1.08)
fig.axes[0].axvspan(300, 1200, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
fig.axes[1].axvspan(300, 1200, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
pyplot.xticks(np.arange(0,1201,300), [0,1000,2000,3000,4000])
pyplot.xlim(0,1200)
pyplot.xlabel("Time [ms]")

#fig.text(-0.03, 0.5, 'Voltage', va='center', rotation='vertical', 
#         fontsize=10)
None

fig = plot_sensor_signals(csp_filtered_full.data[36].T, 
                          sensor_names=['CSP1', 'CSP2'],
                         figsize=(4,4/3.0))
fig.suptitle('Left Hand', fontsize=16, y=1.08)
fig.axes[0].axvspan(300, 1200, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
fig.axes[1].axvspan(300, 1200, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
pyplot.xticks(np.arange(0,1201,300), [0,1000,2000,3000,4000])
pyplot.xlim(0,1200)
pyplot.xlabel("Time [ms]")

#fig.text(-0.03, 0.5, 'Voltage', va='center', rotation='vertical', 
#         fontsize=10)
None


# In[254]:

combined_sensor_trials = np.concatenate((test_epo_full.data[36], test_epo_full.data[37]))
combined_csp_trials = np.concatenate((csp_filtered_full.data[36], csp_filtered_full.data[37]))
combined_both = np.concatenate((combined_sensor_trials, combined_csp_trials), axis=1)
plot_sensor_names =  test_epo_full.axes[2].tolist() + ['CSP1', 'CSP2']
fig = plot_sensor_signals(combined_both.T, sensor_names=plot_sensor_names, figsize=(8,4))
for ax in fig.axes:
    ax.axvline(x=1200, color='black', lw=1.25)

pyplot.plot([0.125, 0.9], [0.43,0.43], color='grey',
    lw=1,transform=fig.transFigure, clip_on=False, linestyle="-")
pyplot.text(0.25,1.2,"Left Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
pyplot.text(0.75,1.2,"Right Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
pyplot.xticks(np.arange(0,2401,300), 
      [0,1000,2000,3000] + ['4000/0'] + [1000,2000,3000,4000])
pyplot.xlabel("Time [ms]")
fig.axes[1].axvspan(300, 1200, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
fig.axes[2].axvspan(300, 1200, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
fig.axes[3].axvspan(300, 1200, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
fig.axes[4].axvspan(300, 1200, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
fig.axes[1].axvspan(1500, 2400, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
fig.axes[2].axvspan(1500, 2400, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
fig.axes[3].axvspan(1500, 2400, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
fig.axes[4].axvspan(1500, 2400, color=seaborn.color_palette()[1], alpha=0.2, lw=0)


# ## Results part

# In[27]:

fft_result_folder = 'data/models/final-eval/fft/early-stop/'
fft_resultpool = ResultPool()
fft_resultpool.load_results(fft_result_folder)
fft_dataset_averaged_pool = DatasetAveragedResults()

fft_dataset_averaged_pool.extract_results(fft_resultpool)
fft_all_group_results = fft_dataset_averaged_pool.results()
fft_power_all_group_results = [gres for gres in fft_all_group_results if (
    gres[0]['parameters']['transform_function_and_args'] == '*power_func')]
fft_phase_all_group_results = [gres for gres in fft_all_group_results if (
    gres[0]['parameters']['transform_function_and_args'] == '*power_phase_func')]
assert len(fft_phase_all_group_results) == len(fft_power_all_group_results)
assert len(fft_phase_all_group_results) == 8

raw_result_folder = 'data/models/final-eval/raw-net/early-stop/'
raw_resultpool = ResultPool()
raw_resultpool.load_results(raw_result_folder)
raw_dataset_averaged_pool = DatasetAveragedResults()

raw_dataset_averaged_pool.extract_results(raw_resultpool)
raw_all_group_results = raw_dataset_averaged_pool.results()

fb_result_folder = 'data/models/final-eval/csp-net/early-stop/'
fb_resultpool = ResultPool()
fb_resultpool.load_results(fb_result_folder)
fb_dataset_averaged_pool = DatasetAveragedResults()

fb_dataset_averaged_pool.extract_results(fb_resultpool)
fb_all_group_results = fb_dataset_averaged_pool.results()

csp_result_folder = 'data/models/final-eval/csp-standardized/'
csp_resultpool = ResultPool()
csp_resultpool.load_results(csp_result_folder)
csp_dataset_averaged_pool = DatasetAveragedResults()

csp_dataset_averaged_pool.extract_results(csp_resultpool)
csp_all_group_results = csp_dataset_averaged_pool.results()


# In[28]:

all_methods_results = (
                      ('FBCSP', csp_all_group_results),
                        ('Filter Bank Net', fb_all_group_results),
                       ('Raw Net', raw_all_group_results),
                      ('FFT Net', fft_power_all_group_results),
                      ('FFT Net + Phase', fft_phase_all_group_results),)


# In[69]:

def extract_results(all_methods_results, all_match_dicts):
    wanted_results = dict()
    for method_name, all_group_results in all_methods_results:
        clean_results = [gres for gres in all_group_results if results_match(
            gres[0], all_match_dicts)]
        wanted_results[method_name] = clean_results
    return wanted_results

def results_match(result, all_match_dicts):
    for match_dict in all_match_dicts:
        dict_matched = False
        for key in match_dict:
            if result['parameters'].get(key, None) == match_dict[key]:
                dict_matched = True
        if not dict_matched:
            return False
    return True


# In[122]:

gres = group_results[1]


# In[126]:

len(gres)


# In[128]:

sorted_gres = sorted(gres, key=lambda r: r['parameters']['dataset_filename'])


# In[131]:




# In[135]:

expected_filenames = ['data/BBCI-all-runs/AnWeMoSc1S001R01_ds10_1-14BBCI.mat',
 'data/BBCI-all-runs/BhNoMoSc1S001R01_ds10_1-14BBCI.mat',
 'data/BBCI-all-runs/FaMaMoSc1S001R01_ds10_1-16BBCI.mat',
 'data/BBCI-all-runs/FrThMoSc1S001R01_ds10_1-13BBCI.mat',
 'data/BBCI-all-runs/GuJoMoSc01S001R01_ds10_1-13BBCI.mat',
 'data/BBCI-all-runs/JoBeMoSc01S001R01_ds10_1-13BBCI.mat',
 'data/BBCI-all-runs/KaUsMoSc1S001R01_ds10_1-13BBCI.mat',
 'data/BBCI-all-runs/LaKaMoSc1S001R01_ds10_1-11BBCI.mat',
 'data/BBCI-all-runs/MaGlMoSc2S001R01_ds10_1-14BBCI.mat',
 'data/BBCI-all-runs/MaJaMoSc1S001R01_ds10_1-13BBCI.mat',
 'data/BBCI-all-runs/MaVoMoSc1S001R01_ds10_1-13BBCI.mat',
 'data/BBCI-all-runs/NaMaMoSc1S001R01_ds10_1-13BBCI.mat',
 'data/BBCI-all-runs/OlIlMoSc01S001R01_ds10_1-13BBCI.mat',
 'data/BBCI-all-runs/PiWiMoSc1S001R01_ds10_1-13BBCI.mat',
 'data/BBCI-all-runs/RoBeMoSc03S001R01_ds10_1-11BBCI.mat',
 'data/BBCI-all-runs/RoScMoSc1S001R01_ds10_1-13BBCI.mat',
 'data/BBCI-all-runs/StHeMoSc01S001R01_ds10_1-12BBCI.mat',
 'data/BBCI-all-runs/SvMuMoSc1S001R01_ds10_1-14BBCI.mat']

def sort_results_by_filename(method_to_group_results):
    sorted_results = dict()
    for method_name, group_results  in method_to_group_results.iteritems():
        all_sorted_group_results = []
        for one_group_result in group_results:
            sorted_group_result = sorted(one_group_result, key=lambda r: r['parameters']['dataset_filename'])
            all_sorted_group_results.append(sorted_group_result)
        sorted_results[method_name] = all_sorted_group_results
    # check correctness:
    for method_name, group_results  in sorted_results.iteritems():
        for one_group_result in group_results:
            assert np.array_equal(expected_filenames, 
                      [r['parameters']['dataset_filename'] for r in one_group_result])
    return sorted_results


# In[132]:

assert np.array_equal(expected_filenames, 
                      [r['parameters']['dataset_filename'] for r in sorted_gres])


# In[153]:

high_fs_dict = dict(resampling_fs=300, resample_fs=300, frequency_stop=144)
clean_train_dict = dict(train_cleaner='*one_set_cleaner', training_type='*csp_trainer')
high_sample_results = sort_results_by_filename(
    extract_results(all_methods_results, [high_fs_dict, clean_train_dict]))
low_fs_dict = dict(resampling_fs=150, resample_fs=150, frequency_stop=72)
low_sample_results = sort_results_by_filename(
    extract_results(all_methods_results, [low_fs_dict, clean_train_dict]))


# In[147]:

def extract_misclasses(method_to_group_results):
    method_to_misclasses = dict()
    for method_name, group_results  in method_to_group_results.iteritems():
        all_misclasses = []
        for one_group_result in group_results:
                misclasses = [np.atleast_1d(r['misclasses']['test'][0])[-1] for r in one_group_result]
                all_misclasses.extend(misclasses)
        method_to_misclasses[method_name] = np.array(all_misclasses)
    return method_to_misclasses


# In[154]:

high_sample_misclasses = extract_misclasses(high_sample_results)
low_sample_misclasses = extract_misclasses(low_sample_results)


# In[171]:

all_method_names = ['FBCSP', 'Filter Bank Net', 'Raw Net', 'FFT Net', 'FFT Net + Phase']
for method_name in all_method_names:
    print method_name
    diff = -np.mean(high_sample_misclasses[method_name] - low_sample_misclasses[method_name])
    std = np.std(high_sample_misclasses[method_name] - low_sample_misclasses[method_name])
    print "{:.2f}%".format((1 - np.mean(high_sample_misclasses[method_name])) * 100)
    print "{:.2f}%".format((1 - np.mean(low_sample_misclasses[method_name])) * 100)
    print "{:.2f}% ({:.2f}%)".format(diff * 100, std* 100)
    


# In[163]:

all_sensors_dict = dict(sensor_names="*all_EEG_sensors")
C_sensors_dict = dict(sensor_names="*C_sensors")
clean_train_dict = dict(train_cleaner='*one_set_cleaner', training_type='*csp_trainer')
all_sensors_results = sort_results_by_filename(
    extract_results(all_methods_results, [all_sensors_dict, clean_train_dict]))
C_sensors_results = sort_results_by_filename(
    extract_results(all_methods_results, [C_sensors_dict, clean_train_dict]))


# In[164]:

all_sensors_misclasses = extract_misclasses(all_sensors_results)
C_sensors_misclasses = extract_misclasses(C_sensors_results)


# In[172]:

all_method_names = ['FBCSP', 'Filter Bank Net', 'Raw Net', 'FFT Net', 'FFT Net + Phase']
for method_name in all_method_names:
    print method_name
    diff = -np.mean(C_sensors_misclasses[method_name] - all_sensors_misclasses[method_name])
    std = np.std(C_sensors_misclasses[method_name] - all_sensors_misclasses[method_name])
    print "{:.2f}%".format((1 - np.mean(C_sensors_misclasses[method_name])) * 100)
    print "{:.2f}%".format((1 - np.mean(all_sensors_misclasses[method_name])) * 100)
    print "{:.2f}% ({:.2f}%)".format(diff * 100, std* 100)
    


# In[174]:

wanted_results = extract_results(all_methods_results, [C_sensors_dict, clean_train_dict, high_fs_dict])

method_names = ['FBCSP', 'Filter Bank Net', 'Raw Net']

csp_test_misclass = [np.atleast_1d(res['misclasses']['test'][0])[-1] for res in wanted_results['FBCSP'][0]]
fb_test_misclass = [np.atleast_1d(res['misclasses']['test'][0])[-1] for res in wanted_results['Filter Bank Net'][0]]
raw_test_misclass = [np.atleast_1d(res['misclasses']['test'][0])[-1] for res in wanted_results['Raw Net'][0]]
fft_test_misclass = [np.atleast_1d(res['misclasses']['test'][0])[-1] for res in wanted_results['FFT Net'][0]]
fftphase_test_misclass = [np.atleast_1d(res['misclasses']['test'][0])[-1] for res in wanted_results['FFT Net + Phase'][0]]

pyplot.figure(figsize=(2.5,2.5))
pyplot.plot([0.4, 1], [0.4,1], linewidth=1.5)
pyplot.plot(1 -np.array(csp_test_misclass), 1 - np.array(fb_test_misclass), '.', 
            markersize=10.)
pyplot.plot(1 -np.array(csp_test_misclass), 1 - np.array(raw_test_misclass), '.',
           markersize=10.)
pyplot.legend(method_names[0:3], bbox_to_anchor=(1.9,1.), fontsize=11)
pyplot.xlabel('FBCSP accuracy')
pyplot.ylabel('Other approach accuracy')
pyplot.xlim(0.38,1.02)
pyplot.ylim(0.38,1.02)
None


# ### Smaller sets

# In[285]:

raw_result_folder = 'data/models/final-eval/small/raw-net/early-stop/'
raw_resultpool = ResultPool()
raw_resultpool.load_results(raw_result_folder)
raw_dataset_averaged_pool = DatasetAveragedResults()

raw_dataset_averaged_pool.extract_results(raw_resultpool)
raw_small_all_group_results = raw_dataset_averaged_pool.results()
csp_result_folder = 'data/models/final-eval/small/csp/'
csp_resultpool = ResultPool()
csp_resultpool.load_results(csp_result_folder)
csp_dataset_averaged_pool = DatasetAveragedResults()

csp_dataset_averaged_pool.extract_results(csp_resultpool)
csp_small_all_group_results = csp_dataset_averaged_pool.results()
fb_result_folder = 'data/models/final-eval/small/csp-net/early-stop/'
fb_resultpool = ResultPool()
fb_resultpool.load_results(fb_result_folder)
fb_dataset_averaged_pool = DatasetAveragedResults()

fb_dataset_averaged_pool.extract_results(fb_resultpool)
fb_small_all_group_results = fb_dataset_averaged_pool.results()


# In[286]:

small_methods_results = (
                      ('FBCSP', csp_small_all_group_results),
                        ('Filter Bank Net', fb_small_all_group_results),
                       ('Raw Net', raw_small_all_group_results))
small_means = []
for method_name, results  in small_methods_results:
    small_means.append((method_name, []))
    for fraction in [0.25, 0.5, 0.75]:
        this_results = [r for r in results if r[0]['parameters']['restricted_n_trials'] == fraction][0]
        test_misclass = [np.atleast_1d(res['misclasses']['test'][0])[-1] for res in this_results]
        test_acc = np.mean(1 - np.array(test_misclass))
        small_means[-1][1].append(test_acc)

    if method_name == 'FBCSP':
        small_means[-1][1].append(0.9110)
    elif method_name == 'Filter Bank Net':
        small_means[-1][1].append(0.9274)
    elif method_name == 'Raw Net':
        small_means[-1][1].append(0.923)


# In[287]:

pyplot.figure(figsize=(6,2))
for i_method in xrange(len(small_means)):
    means = small_means[i_method][1]
    pyplot.plot([0.25, 0.5, 0.75, 1.], means, marker='o', linestyle='None')
pyplot.xlabel('Fraction of dataset size/Mean train fold size (rounded)')
pyplot.ylabel('Mean accuracy')
pyplot.legend([m[0] for m in small_methods_results], bbox_to_anchor=(1.35,1.0))
pyplot.xlim(0.2,1.05)
pyplot.xticks((0.25,0.5,0.75,1), ('0.25/170', '0.5/340', '0.75/510', '1.0/680'))
None


# ## Visualizations Example

# In[189]:

ax = fig.axes[0]
ax.set_ylim


# In[200]:

target = [-1] * 10 + [1] * 10
sensor_1 = [-1] * 10 + [1] * 10
sensor_2 = [0] * 20
fig = plot_sensor_signals(np.array([target, sensor_1, sensor_2]),
                          ('Class', 'Sensor 1', 'Sensor 2'), sharey=False, figsize=(6,2), fontsize=12)

fig.axes[0].grid(False, axis='y')
fig.axes[0].set_yticks((-1,1))
for ax in fig.axes:
    ax.set_ylim((-1.1,1.1))

fig.axes[0].set_yticklabels(('Left','Right'), fontsize=11)
fig.suptitle('Toy example without common distractor', fontsize=16, y=1.02)
None


# In[178]:

noise = create_sine_signal(20,freq=2, sampling_freq=20) * 2.5
noised_sensor_1 = sensor_1 + noise
noised_sensor_2 = sensor_2 + noise


# In[201]:

fig = plot_sensor_signals(np.array([target, noised_sensor_1, noised_sensor_2]), 
                          ('Class', 'Sensor 1', 'Sensor 2'), 
                          sharey=False,
                          figsize=(6,2),
                         fontsize=12)
fig.axes[0].grid(False, axis='y')
fig.axes[0].set_yticks((-0.8,0.8))
fig.axes[0].set_ylim((-1.1,1.1))

fig.axes[0].set_yticklabels(('Left','Right'), fontsize=11)

fig.suptitle('Toy example with common distractor', fontsize=16, y=1.02)
None


# ## FB Net Visualization

# In[202]:

# load to determine rejected sensors
#bhno_fb_train = create_training_object_from_file(
#    'data/models/debug/csp-net/car/chan-freq-standardize-misclass-stop/early-stop/2.yaml')

#bhno_fb_train.dataset_splitter.dataset.max_freq=4
#bhno_fb_train.dataset_splitter.ensure_dataset_is_loaded()
#rejected_chans = bhno_fb_train.dataset_splitter.rejected_chans
#bhno_fb_train.dataset_splitter.rejected_chans
rejected_chans = ['T7']


# In[204]:

from motor_imagery_learning.analysis.sensor_positions import get_EEG_sensors_sorted
sensor_names = get_EEG_sensors_sorted()
sensor_names = [s for s in sensor_names if s not in rejected_chans]


# In[205]:

bhno_fb_net = serial.load(
    'data/models/debug/csp-net/car/chan-freq-standardize-misclass-stop/early-stop/2.pkl').model
spat_filters  = bhno_fb_net.layers[0].get_weights_topo().squeeze()
soft_weights = bhno_fb_net.layers[1].get_weights_topo()
filt_weights = np.sum(np.abs(soft_weights), axis=(0,1,2))
i_important_filt_weights = np.argsort(filt_weights)[::-1]


# In[206]:

from motor_imagery_learning.mywyrm.plot import get_channelpos

def add_circle_around_chan(ax, chan):
    point = get_channelpos(chan, CHANNEL_10_20)
    ax.add_artist(pyplot.Circle(point, 0.2, linestyle='solid', 
                                linewidth=1, fill=False,
                             color=[0.3,0.3,0.3]))
    


# In[211]:



center_freqs = compute_center_freqs(bhno_fb_net)
n_rows = 2
n_filts = 4 
n_cols = 2 * (n_filts / n_rows)
assert n_filts % n_rows == 0
class_names = ('Right Hand', 'Left Hand', 'Rest', 'Feet')

fig, axes = pyplot.subplots(n_rows,n_cols,figsize=(12,6))
for i in range(n_filts):
    plot_i = i * 2
    i_filter = i_important_filt_weights[i]
    row, col = plot_i // n_cols, plot_i % n_cols
    ax_scalp(spat_filters[i_filter], 
             sensor_names, ax=axes[row,col], colormap=cm.coolwarm_r, annotate=False,
            chan_pos_list=CHANNEL_10_20)
    filt_soft_weight = soft_weights[:,:,:,i_filter].squeeze()
    axes[row, col].set_title('Filter ' + str(i + 1))
    if i == 0 or i == 1:
        add_circle_around_chan(axes[row, col], 'Cz')
    if i == 2 or i == 3:
        add_circle_around_chan(axes[row, col], 'FCC4h')
        
    axes[row, col+1].plot(center_freqs, filt_soft_weight.T, linewidth=1)
    axes[row, col+1].set_xticks(range(0,63,20) + [80,110,140])
    axes[row, col+1].set_yticks([0])
    axes[row, col+1].set_yticklabels([])

axes[0, n_cols - 1].legend(class_names, bbox_to_anchor=(2.02,1), fontsize=14)
None


# In[213]:

all_covariances = np.load('./all_covariances.npy')
mean_covariance = np.mean(all_covariances, axis=0)
act_0 = np.load('./activations_0.npy')
act_1 = np.load('./activations_1.npy')
act_0_soft_weight_shape = act_0.transpose(0,2,3,1)
assert np.array_equal(act_0_soft_weight_shape.shape[1:], soft_weights.shape[1:]) 
act_0_for_covar = act_0_soft_weight_shape.reshape(act_0_soft_weight_shape.shape[0], -1).T
features_covar = np.cov(act_0_for_covar)
soft_weights_for_pattern = soft_weights.reshape(soft_weights.shape[0],-1)
assert soft_weights_for_pattern.shape[0] == soft_weights.shape[0]
assert soft_weights_for_pattern.shape[0] == 4
assert soft_weights_for_pattern.shape[1] == np.prod(soft_weights.shape[1:])
soft_weights_pattern = np.dot(features_covar , soft_weights_for_pattern.T)
assert soft_weights_pattern.shape[1] == soft_weights.shape[0]
assert soft_weights_pattern.shape[1] == 4
assert soft_weights_pattern.shape[0] == np.prod(soft_weights.shape[1:])
# have to transpose to again have class dimension as first dimension
soft_weights_pattern = soft_weights_pattern.T.reshape(soft_weights.shape)
assert soft_weights_pattern.shape == soft_weights.shape


# In[214]:

n_rows = 2
n_filts = 4 
n_cols = 2 * (n_filts / n_rows)
assert n_filts % n_rows == 0
class_names = ('Right Hand', 'Left Hand', 'Rest', 'Feet')

fig, axes = pyplot.subplots(n_rows,n_cols,figsize=(12,6))
for i in range(n_filts):
    plot_i = i * 2
    i_filter = i_important_filt_weights[i]
    row, col = plot_i // n_cols, plot_i % n_cols
    pattern = np.dot(spat_filters[i_filter:i_filter+1], mean_covariance)
    pattern = pattern.squeeze()
    ax_scalp(pattern, 
             sensor_names, ax=axes[row,col], colormap=cm.coolwarm, annotate=False,
            chan_pos_list=CHANNEL_10_20)
    filt_soft_weight = soft_weights_pattern[:,:,:,i_filter].squeeze()
    axes[row, col].set_title('Pattern ' + str(i + 1))
    axes[row, col+1].plot(center_freqs, filt_soft_weight.T, linewidth=1)
    axes[row, col+1].set_xticks(range(0,63,20) + [80,110,140])
    axes[row, col+1].set_yticks([0])
    axes[row, col+1].set_yticklabels([])

axes[0, n_cols - 1].legend(class_names, bbox_to_anchor=(2.02,1), fontsize=14)
None


# ## Raw Net Visualizations

# In[215]:

bhno_model = serial.load(
    'data/models/debug/raw-square/car/30-kernel-shape/early-stop/2.pkl').model


# In[216]:

temporal_weights = bhno_model.get_weights_topo()[:,::-1,::-1,:]
spat_filt_weights = bhno_model.layers[1].get_weights_topo()[:,::-1,::-1,:]

combined_weights = np.tensordot(spat_filt_weights, temporal_weights, axes=(3,0))

combined_weights = combined_weights.squeeze()
soft_weights = bhno_model.layers[-1].get_weights_topo().squeeze()
soft_sum =np.sum(np.abs(soft_weights), axis=(0,1))
imp_weights = np.argsort(soft_sum)[::-1]
sensor_names = map(lambda i: "Combined Filter {:d}".format(i), range(1,13))
class_names = ('Right Hand', 'Left Hand', 'Rest', 'Feet')
def argsort_by_class(soft_weights, class_1, class_2, start_time_bin=None, end_time_bin=None):
    
    wanted_vs_other = soft_weights[class_1] - soft_weights[class_2]
    sorted_diffs = np.argsort(np.mean(wanted_vs_other[start_time_bin:end_time_bin], axis=0))
    return sorted_diffs

sorted_diffs = argsort_by_class(soft_weights, 0, 1)

right_best = sorted_diffs[[-1,-2]]
left_best = sorted_diffs[[0,1]]

def highlight_chans(fig, chan_names, color=None, alpha=0.2, xstart=0, xstop=29):
    if color is None:
        color = seaborn.color_palette()[2]
    all_chan_names = np.array(get_C_sensors_sorted())
    highlight_chan_inds = [np.nonzero(all_chan_names == c)[0][0] for c in chan_names]
    for i_chan in highlight_chan_inds:
        fig.axes[i_chan].axvspan(xstart, xstop, color=color, alpha=alpha, lw=0)
        
    

sorted_diffs = argsort_by_class(soft_weights, 2, 3)

rest_best = sorted_diffs[[-1,-2]]
feet_best = sorted_diffs[[0,1]]


# In[227]:


for i_filt in right_best[0:1]:
    fig = plot_sensor_signals(soft_weights[:,:,i_filt:i_filt+1].T, 
                        [''], 
                        figsize=(5,1.5))
    fig.axes[0].set_title("Combined Filter " + str(i_filt + 1) + ": Softmax Weights", fontsize=16)
    fig.axes[0].legend(class_names, bbox_to_anchor=(1.5, 1.1), loc='upper right', fontsize=14)
    pyplot.xticks([0,15,30,45], [0,1000,2000,3000])
    pyplot.xlabel('Time [ms] (at start of pool region)')
    fig = plot_head_signals_tight(combined_weights[i_filt], get_C_sensors_sorted())
    pyplot.xticks([15], [])
    highlight_chans(fig,  ['FCC4h', 'C2'])


# In[228]:



for i_filt in left_best[0:1]:
    fig = plot_sensor_signals(soft_weights[:,:,i_filt:i_filt+1].T, 
                        [''], 
                        figsize=(5,1.5))
    fig.axes[0].set_title("Combined Filter " + str(i_filt + 1) + ": Softmax Weights", fontsize=16)
    fig.axes[0].legend(class_names, bbox_to_anchor=(1.5, 1.1), loc='upper right', fontsize=14)
    pyplot.xticks([0,15,30,45], [0,1000,2000,3000])
    pyplot.xlabel('Time [ms] (at start of pool region)')
    fig = plot_head_signals_tight(combined_weights[i_filt], get_C_sensors_sorted())
    pyplot.xticks([15], [])
    highlight_chans(fig,  ['FC3', 'FCC3h'])


# In[236]:

from copy import deepcopy
from pylearn2.models.mlp import ConvElemwise, IdentityConvNonlinearity
from motor_imagery_learning.mypylearn2.nonlinearities import (
    NonlinearityLayer, square_nonlin)
from motor_imagery_learning.analysis.util import (
create_activations_func)
def transform_raw_net_model(model):
    transformed_model = deepcopy(model)
    oldlayer = transformed_model.layers[1]
    new_layer = ConvElemwise(output_channels=oldlayer.output_channels, 
                             kernel_shape=oldlayer.kernel_shape,
                             layer_name='new_layer',
                             nonlinearity=IdentityConvNonlinearity(),
                             kernel_stride=oldlayer.kernel_stride,
                             tied_b=True,
                             irange=0.01
                            )
    new_layer.mlp = lambda: None
    new_layer.mlp.rng = RandomState(92138)
    new_layer.mlp.batch_size = 2
    new_layer.set_input_space(oldlayer.get_input_space())
    new_layer.set_weights(oldlayer.get_param_values()[0])
    new_layer.set_biases(oldlayer.get_param_values()[1])
    nonlin_layer = NonlinearityLayer(layer_name='square_nonlin', 
                                 nonlinearity_func=square_nonlin)
    nonlin_layer.set_input_space(new_layer.get_output_space())
    transformed_model.layers = [transformed_model.layers[0],
                            new_layer, nonlin_layer, 
                           transformed_model.layers[2],
                           transformed_model.layers[3]]
    return transformed_model

def transform_to_patterns(combined_weights, train_topo, acts_out):
    """Transform raw net combined filters to combined patterns.
    Assumes kernel shape 30"""
    acts_out_for_cov = acts_out[1].transpose(1,0,2,3)
    acts_out_for_cov = acts_out_for_cov.reshape(acts_out_for_cov.shape[0], -1)
    acts_cov = np.cov(acts_out_for_cov)
    new_train_topo = np.empty((train_topo.shape[0], train_topo.shape[1] * 30,
                             train_topo.shape[2] - 30 + 1, 1))


    n_chans = train_topo.shape[1]
    n_samples = 30

    for i_sample in xrange(n_samples):
        end = -30+1+i_sample
        if end == 0:
            end = None
        for i_chan in xrange(n_chans):
            new_train_topo[:, i_chan*n_samples+i_sample, :] =                 train_topo[:,i_chan,i_sample:end]

    new_train_topo_for_cov = new_train_topo.transpose(1,0,2,3)

    new_train_topo_for_cov = new_train_topo_for_cov.reshape(
        new_train_topo_for_cov.shape[0], -1)
    new_train_topo_cov = np.cov(new_train_topo_for_cov)
    combined_vectors = combined_weights.reshape(combined_weights.shape[0],-1)
    transformed_vectors = np.dot(new_train_topo_cov, combined_vectors.T).T
    transformed_vectors = np.dot(transformed_vectors.T, acts_cov).T
    patterns = transformed_vectors.reshape(*combined_weights.shape)
    return patterns


# In[237]:

get_ipython().run_cell_magic(u'capture', u'', u"from motor_imagery_learning.train_scripts.train_with_params import (\n    create_training_object_from_file)\ninputs = create_rand_input(bhno_model, 3)\ntransformed_model = transform_raw_net_model(bhno_model)\n\nact_func_new = create_activations_func(transformed_model)\n\nact_func_old = create_activations_func(bhno_model)\nact_outs = act_func_new(inputs)\nact_outs_old = act_func_old(inputs)\n\nact_outs_restricted = deepcopy(act_outs)\nact_outs_restricted.pop(1) # remove activations not exsting in other model\nassert all([np.allclose(a1,a2) for a1,a2 in zip(act_outs_restricted, act_outs_old)])\n\nbhno_train = create_training_object_from_file(\n    'data/models/debug/raw-square/car/30-kernel-shape/early-stop/2.yaml')\nbhno_train.dataset_splitter.ensure_dataset_is_loaded()\ndatasets = bhno_train.get_train_fitted_valid_test()\ntrain_topo = datasets['train'].get_topological_view()\nacts_out = act_func_new(train_topo)\nacts_out[1].shape\n\npatterns = transform_to_patterns(combined_weights,train_topo, acts_out)")


# In[238]:

act_3 = acts_out[3]
act_3_soft_weight_shape = act_3.transpose(0,2,3,1)
assert np.array_equal(act_3_soft_weight_shape.squeeze().shape[1:], soft_weights.shape[1:]) 
act_3_for_covar = act_3_soft_weight_shape.reshape(act_3_soft_weight_shape.shape[0], -1).T
features_covar = np.cov(act_3_for_covar)


# In[239]:

soft_weights_for_pattern = soft_weights.reshape(soft_weights.shape[0],-1)
assert soft_weights_for_pattern.shape[0] == soft_weights.shape[0]
assert soft_weights_for_pattern.shape[1] == np.prod(soft_weights.shape[1:])
soft_weights_pattern = np.dot(features_covar , soft_weights_for_pattern.T)
assert soft_weights_pattern.shape[1] == soft_weights.shape[0]
assert soft_weights_pattern.shape[0] == np.prod(soft_weights.shape[1:])
# have to transpose to again have class dimension as first dimension
soft_weights_pattern = soft_weights_pattern.T.reshape(soft_weights.shape)
assert soft_weights_pattern.shape == soft_weights.shape


# In[240]:

# 21, 25 and 32 negative for right hand?
# 29 positive
for i_filt in right_best[0:1]:
    fig = plot_sensor_signals(soft_weights_pattern[:,:,i_filt:i_filt+1].T, 
                        [''], 
                        figsize=(5,1.5))
    fig.axes[0].set_title("Combined Pattern " + str(i_filt + 1) + ": Softmax Pattern", fontsize=16)
    fig.axes[0].legend(class_names, bbox_to_anchor=(1.5, 1.1), loc='upper right', fontsize=14)
    pyplot.xticks([0,15,30,45], [0,1000,2000,3000])
    pyplot.xlabel('Time [ms] (at start of pool region)')
    fig = plot_head_signals_tight(patterns[i_filt], get_C_sensors_sorted())
    pyplot.xticks([15], [])
    
    highlight_chans(fig,  ['FCC4h', 'C2'])
    highlight_chans(fig,  ['FCC1h', 'C3'], color=seaborn.color_palette()[1])


# In[241]:

for i_filt in left_best[0:1]:
    fig = plot_sensor_signals(soft_weights_pattern[:,:,i_filt:i_filt+1].T, 
                        [''], 
                        figsize=(5,1.5))
    fig.axes[0].set_title("Combined Pattern " + str(i_filt + 1) + ": Softmax Pattern", fontsize=16)
    fig.axes[0].legend(class_names, bbox_to_anchor=(1.5, 1.1), loc='upper right', fontsize=14)
    pyplot.xticks([0,15,30,45], [0,1000,2000,3000])
    pyplot.xlabel('Time [ms] (at start of pool region)')
    fig = plot_head_signals_tight(patterns[i_filt], get_C_sensors_sorted())
    pyplot.xticks([15], [])
    
    highlight_chans(fig,  ['CCP3h', 'C3'])
    highlight_chans(fig,  ['CCP4h', 'C4'], color=seaborn.color_palette()[1])


# In[242]:

from motor_imagery_learning.train_scripts.train_with_params import (
create_training_object_from_file)
#before data/models/debug/raw-square/larger-kernel-shape/early-stop/8
laka_train_obj = create_training_object_from_file(
    'data/models/debug/raw-square/car/30-kernel-shape/early-stop/8.yaml')
laka_train_obj.dataset_splitter.ensure_dataset_is_loaded()
laka_sets = laka_train_obj.get_train_fitted_valid_test()
laka_model = serial.load(
    'data/models/debug/raw-square/car/30-kernel-shape/early-stop/8.pkl').model


temporal_weights = laka_model.get_weights_topo()[:,::-1,::-1,:]
spat_filt_weights = laka_model.layers[1].get_weights_topo()[:,::-1,::-1,:]

combined_weights = np.tensordot(spat_filt_weights, temporal_weights, axes=(3,0))

combined_weights = combined_weights.squeeze()

soft_weights = laka_model.layers[-1].get_weights_topo()
soft_weights = soft_weights.squeeze()
sorted_diffs = argsort_by_class(soft_weights, 0, 1)

right_best = sorted_diffs[[-1,-2]]
left_best = sorted_diffs[[0,1]]


# In[243]:

i_interesting_filt = 26 # determined by ploitting alll


# In[252]:

for i_filt in [i_interesting_filt]:
    fig = plot_sensor_signals(soft_weights[:,:,i_filt:i_filt+1].T, 
                        [''], 
                        figsize=(5,1.5))
    fig.axes[0].set_title("Combined Filter " + str(i_filt + 1) + ": Softmax Weights", fontsize=16)
    fig.axes[0].legend(class_names, bbox_to_anchor=(1.5, 1.1), loc='upper right', fontsize=14)
    pyplot.xticks([0,15,30,45], [0,1000,2000,3000])
    pyplot.xlabel('Time [ms] (at start of pool region)')
    fig = plot_head_signals_tight(combined_weights[i_filt], get_C_sensors_sorted())
    pyplot.xticks([15], [])
    
    highlight_chans(fig,  ['CCP2h'])
    highlight_chans(fig,  ['CCP4h'], color=seaborn.color_palette()[1])
    


# In[246]:

from motor_imagery_learning.analysis.util import create_activations_func
activations_func = create_activations_func(laka_model)

activations = activations_func(laka_sets['train'].get_topological_view())
copied_model = deepcopy(laka_model)


# In[247]:

get_ipython().run_cell_magic(u'capture', u'', u'spat_layer = copied_model.layers[1]\nspat_layer.kernel_shape = (1,-1)\nspat_layer.output_channels = 1\nspat_layer.set_input_space(copied_model.layers[0].get_output_space())\ncopied_model.layers[-2].set_input_space(copied_model.layers[-3].get_output_space())\ncopied_model.layers[-1].set_input_space(copied_model.layers[-2].get_output_space())')


# In[248]:

W, b = laka_model.layers[-3].get_param_values()

spat_layer.set_biases(np.copy(b[i_interesting_filt:i_interesting_filt+1]))

spat_layer.set_weights(np.copy(W[i_interesting_filt:i_interesting_filt+1]))
soft_layer = copied_model.layers[-1]
b, W = laka_model.layers[-1].get_param_values()
soft_layer.set_biases(np.copy(b))

soft_weights_topo = laka_model.layers[-1].get_weights_topo()
soft_weights_topo = soft_weights_topo[:,:,0,i_interesting_filt]

soft_weights_topo = soft_weights_topo.T
soft_layer.set_weights(np.copy(soft_weights_topo))
copied_model_acts_func = create_activations_func(copied_model)
activations = activations_func(laka_sets['train'].get_topological_view())
other_acts = copied_model_acts_func(laka_sets['train'].get_topological_view())
assert np.allclose(activations[0], other_acts[0])
assert np.allclose(activations[1][:,i_interesting_filt:i_interesting_filt+1,:,:],
           other_acts[1])
assert np.allclose(activations[2][:,i_interesting_filt:i_interesting_filt+1,:,:],
           other_acts[2])
assert not np.allclose(activations[3], other_acts[3])
in_grad_func = create_input_grad_for_wanted_class_func(copied_model)
batches = 6
start_in = create_rand_input(copied_model, batches)
ascent_args = dict(epochs=500, lrate=100, decay=0.98)
inputs_left = gradient_descent_input(start_in, in_grad_func, np.float32([0,1,0,0]), **ascent_args)
inputs_left = inputs_left.squeeze()


# In[249]:

fig = plot_head_signals_tight(inputs_left[-1,:].squeeze().transpose(1,2,0), get_C_sensors_sorted())
highlight_chans(fig,  ['CCP2h'], xstop=90)
highlight_chans(fig,  ['C4'], xstop=90)
None


# In[250]:


left_topo = laka_sets['train'].get_topological_view()[laka_sets['train'].y[:,1] ==1]

right_topo = laka_sets['train'].get_topological_view()[laka_sets['train'].y[:,0] ==1]
not_left_topo = laka_sets['train'].get_topological_view()[laka_sets['train'].y[:,0] != 1]


# In[251]:

diff_mean = np.mean(left_topo, axis=0) - np.mean(not_left_topo, axis=0)
fig = plot_head_signals_tight(diff_mean, get_C_sensors_sorted())
highlight_chans(fig,  ['CCP2h'], xstop=120)
highlight_chans(fig,  ['C4'], xstop=120)
None

