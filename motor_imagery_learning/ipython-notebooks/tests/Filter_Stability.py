
# coding: utf-8

# In[23]:

import os

import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# make sure to use cpu for printing results...
os.environ['THEANO_FLAGS'] = "floatX=float32,device=cpu,nvcc.fastmath=True"
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 2.0)
matplotlib.rcParams['font.size'] = 7
from analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file
from motor_imagery_learning.analysis.print_results import ResultPrinter
from motor_imagery_learning.csp.print_csp_results import CSPResultPrinter
from motor_imagery_learning.analysis.results import ResultPool, DatasetAveragedResults
from pylearn2.utils import serial
import numpy as np
from motor_imagery_learning.analysis.plot_util import (plot_nlls_for_file, 
    plot_misclasses_for_result, plot_misclasses_for_file, plot_nlls)
from glob import glob
import seaborn
seaborn.set_style('darkgrid')
seaborn.set_context("notebook", rc={"lines.linewidth": 0.5})


# In[3]:

from motor_imagery_learning.csp.train_csp import generate_filterbank


# In[6]:

filterbands = generate_filterbank(min_freq=0,max_freq=142, low_width=2, high_width=6, last_low_freq=30)


# In[8]:

filterbands.shape


# In[10]:

import scipy


# ### Check small one

# In[60]:

filt_order = 3
fs = 150
filterbands_small = generate_filterbank(min_freq=0,max_freq=70, low_width=2, high_width=6, last_low_freq=30)
for low_cut_hz, high_cut_hz in filterbands_small:
    nyq_freq = 0.5 * fs
    low = low_cut_hz / nyq_freq
    high = high_cut_hz / nyq_freq
    b, a = scipy.signal.butter(filt_order, [low, high], btype='band')
    assert np.all(np.abs(np.roots(a))<1), "Filter should be stable"


# ### Check larger one

# In[59]:

filt_order = 3
fs = 300

for low_cut_hz, high_cut_hz in filterbands:
    nyq_freq = 0.5 * fs
    low = low_cut_hz / nyq_freq
    high = high_cut_hz / nyq_freq
    b, a = scipy.signal.butter(filt_order, [low, high], btype='band')
    assert np.all(np.abs(np.roots(a))<1), "Filter should be stable"


# ### Plot frequency response for high pass filter

# In[65]:

filt_order = 3
fs = 300#150
low_cut_off_hz = 0.5
b,a = scipy.signal.butter(filt_order, low_cut_off_hz/(fs/2.0),btype='highpass')
w, h = scipy.signal.freqz(b,a)

fig = pyplot.figure(figsize=(5,2))
pyplot.title('Digital filter frequency response')
ax1 = fig.add_subplot(111)
#ax1.set_xticks([0, np.pi/2, np.pi])
#ax1.set_xticklabels([0,0.5,1])
#ax1.set_yticks((0,-100,-200,-300,-400))

ax1.plot(w, 20 * np.log10(abs(h)), 'b')
ax1.set_ylabel('Amplitude [dB]', color='b')
ax1.set_xlabel('Frequency [rad/sample]')


ax2 = ax1.twinx()
angles = np.unwrap(np.angle(h))
ax2.plot(w, angles, 'g')
ax2.set_ylabel('Angle (radians)', color='g')
pyplot.grid()
    
pyplot.axis('tight')


# ### Plot frequency responses for larger one

# In[49]:

fig, axes = pyplot.subplots(6,6, figsize=(11,11))
for i_filt, (low_cut_hz, high_cut_hz) in enumerate(filterbands):
    row, col = i_filt // 6, i_filt % 6
    nyq_freq = 0.5 * fs
    low = low_cut_hz / nyq_freq
    high = high_cut_hz / nyq_freq
    b, a = scipy.signal.butter(filt_order, [low, high], btype='band')
    assert np.all(np.abs(np.roots(a))<1)
    w, h = scipy.signal.freqz(b,a)

    #fig = pyplot.figure(figsize=(5,2))
    #pyplot.title('Digital filter frequency response')
    ax1 = axes[row, col]#fig.add_subplot(111)
    ax1.set_xticks([0, np.pi/2, np.pi])
    ax1.set_xticklabels([0,0.5,1])
    ax1.set_yticks((0,-100,-200,-300,-400))

    ax1.plot(w, 20 * np.log10(abs(h)), 'b')
    #ax1.set_ylabel('Amplitude [dB]', color='b')
    #ax1.set_xlabel('Frequency [rad/sample]')


    ax2 = ax1.twinx()
    angles = np.unwrap(np.angle(h))
    ax2.plot(w, angles, 'g')
    #ax2.set_ylabel('Angle (radians)', color='g')
    #pyplot.grid()
    
pyplot.axis('tight')
pyplot.subplots_adjust(wspace=1, hspace=1)
fig.delaxes(axes[-1,-1])

