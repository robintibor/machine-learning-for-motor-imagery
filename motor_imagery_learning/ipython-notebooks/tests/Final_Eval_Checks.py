
# coding: utf-8

# In[2]:

import os

import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# make sure to use cpu for printing results...
os.environ['THEANO_FLAGS'] = "floatX=float32,device=cpu,nvcc.fastmath=True"
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7
from analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file
from motor_imagery_learning.analysis.print_results import ResultPrinter
from motor_imagery_learning.csp.print_csp_results import CSPResultPrinter
from motor_imagery_learning.analysis.results import ResultPool, DatasetAveragedResults
from pylearn2.utils import serial
import numpy as np
from motor_imagery_learning.analysis.plot_util import (plot_nlls_for_file, 
    plot_misclasses_for_result, plot_misclasses_for_file, plot_nlls)
from glob import glob
import seaborn
seaborn.set_style('darkgrid')
seaborn.set_context("notebook", rc={"lines.linewidth": 0.5})


# ## Check all test/train filenames same

# In[3]:

train_filenames = [
 'data/BBCI-without-last-runs/AnWeMoSc1S001R01_ds10_1-12.BBCI.mat',
 'data/BBCI-without-last-runs/BhNoMoSc1S001R01_ds10_1-12.BBCI.mat',
 'data/BBCI-without-last-runs/FaMaMoSc1S001R01_ds10_1-14.BBCI.mat',
 'data/BBCI-without-last-runs/FrThMoSc1S001R01_ds10_1-11.BBCI.mat',
 'data/BBCI-without-last-runs/GuJoMoSc01S001R01_ds10_1-11.BBCI.mat',
 'data/BBCI-without-last-runs/JoBeMoSc01S001R01_ds10_1-11.BBCI.mat',
 'data/BBCI-without-last-runs/KaUsMoSc1S001R01_ds10_1-11.BBCI.mat',
 'data/BBCI-without-last-runs/LaKaMoSc1S001R01_ds10_1-9.BBCI.mat',
 'data/BBCI-without-last-runs/MaGlMoSc2S001R01_ds10_1-12.BBCI.mat',
 'data/BBCI-without-last-runs/MaJaMoSc1S001R01_ds10_1-11.BBCI.mat',
 'data/BBCI-without-last-runs/MaVoMoSc1S001R01_ds10_1-11.BBCI.mat',
 'data/BBCI-without-last-runs/NaMaMoSc1S001R01_ds10_1-11.BBCI.mat',
 'data/BBCI-without-last-runs/OlIlMoSc01S001R01_ds10_1-11.BBCI.mat',
 'data/BBCI-without-last-runs/PiWiMoSc1S001R01_ds10_1-11.BBCI.mat',
 'data/BBCI-without-last-runs/RoBeMoSc03S001R01_ds10_1-9.BBCI.mat',
 'data/BBCI-without-last-runs/RoScMoSc1S001R01_ds10_1-11.BBCI.mat',
 'data/BBCI-without-last-runs/StHeMoSc01S001R01_ds10_1-10.BBCI.mat',
 'data/BBCI-without-last-runs/SvMuMoSc1S001R01_ds10_1-12.BBCI.mat']


# In[4]:

test_filenames = [
 'data/BBCI-only-last-runs/AnWeMoSc1S001R13_ds10_1-2BBCI.mat',
 'data/BBCI-only-last-runs/BhNoMoSc1S001R13_ds10_1-2BBCI.mat',
 'data/BBCI-only-last-runs/FaMaMoSc1S001R15_ds10_1-2BBCI.mat',
 'data/BBCI-only-last-runs/FrThMoSc1S001R12_ds10_1-2BBCI.mat',
 'data/BBCI-only-last-runs/GuJoMoSc01S001R12_ds10_1-2BBCI.mat',
 'data/BBCI-only-last-runs/JoBeMoSc01S001R12_ds10_1-2BBCI.mat',
 'data/BBCI-only-last-runs/KaUsMoSc1S001R12_ds10_1-2BBCI.mat',
 'data/BBCI-only-last-runs/LaKaMoSc1S001R10_ds10_1-2BBCI.mat',
 'data/BBCI-only-last-runs/MaGlMoSc2S001R13_ds10_1-2BBCI.mat',
 'data/BBCI-only-last-runs/MaJaMoSc1S001R12_ds10_1-2BBCI.mat',
 'data/BBCI-only-last-runs/MaVoMoSc1S001R12_ds10_1-2BBCI.mat',
 'data/BBCI-only-last-runs/NaMaMoSc1S001R12_ds10_1-2BBCI.mat',
 'data/BBCI-only-last-runs/OlIlMoSc01S001R12_ds10_1-2BBCI.mat',
 'data/BBCI-only-last-runs/PiWiMoSc1S001R12_ds10_1-2BBCI.mat',
 'data/BBCI-only-last-runs/RoBeMoSc03S001R10_ds10_1-2BBCI.mat',
 'data/BBCI-only-last-runs/RoScMoSc1S001R12_ds10_1-2BBCI.mat',
 'data/BBCI-only-last-runs/StHeMoSc01S001R11_ds10_1-2BBCI.mat',
 'data/BBCI-only-last-runs/SvMuMoSc1S001R13_ds10_1-2BBCI.mat']


# In[5]:

all_runs_filenames = [
 'data/BBCI-all-runs/AnWeMoSc1S001R01_ds10_1-14BBCI.mat',
 'data/BBCI-all-runs/BhNoMoSc1S001R01_ds10_1-14BBCI.mat',
 'data/BBCI-all-runs/FaMaMoSc1S001R01_ds10_1-16BBCI.mat',
 'data/BBCI-all-runs/FrThMoSc1S001R01_ds10_1-13BBCI.mat',
 'data/BBCI-all-runs/GuJoMoSc01S001R01_ds10_1-13BBCI.mat',
 'data/BBCI-all-runs/JoBeMoSc01S001R01_ds10_1-13BBCI.mat',
 'data/BBCI-all-runs/KaUsMoSc1S001R01_ds10_1-13BBCI.mat',
 'data/BBCI-all-runs/LaKaMoSc1S001R01_ds10_1-11BBCI.mat',
 'data/BBCI-all-runs/MaGlMoSc2S001R01_ds10_1-14BBCI.mat',
 'data/BBCI-all-runs/MaJaMoSc1S001R01_ds10_1-13BBCI.mat',
 'data/BBCI-all-runs/MaVoMoSc1S001R01_ds10_1-13BBCI.mat',
 'data/BBCI-all-runs/NaMaMoSc1S001R01_ds10_1-13BBCI.mat',
 'data/BBCI-all-runs/OlIlMoSc01S001R01_ds10_1-13BBCI.mat',
 'data/BBCI-all-runs/PiWiMoSc1S001R01_ds10_1-13BBCI.mat',
 'data/BBCI-all-runs/RoBeMoSc03S001R01_ds10_1-11BBCI.mat',
 'data/BBCI-all-runs/RoScMoSc1S001R01_ds10_1-13BBCI.mat',
 'data/BBCI-all-runs/StHeMoSc01S001R01_ds10_1-12BBCI.mat',
 'data/BBCI-all-runs/SvMuMoSc1S001R01_ds10_1-14BBCI.mat']


# In[24]:

result_folder = 'data/models/final-eval/csp/'
resultpool = ResultPool()
resultpool.load_results(result_folder)
dataset_averaged_pool = DatasetAveragedResults()
dataset_averaged_pool.extract_results(resultpool)
all_results = dataset_averaged_pool.results()


# In[26]:

for i_group, group_result in enumerate(all_results):
    if (group_result[0]['parameters'].get('dataset_splitter', None) == '*two_file_splitter' or 
            group_result[0]['parameters'].get('training_type', None) == '*csp_two_file_trainer'):
        assert np.array_equal(train_filenames,
                              sorted([res['parameters']['dataset_filename'] for res in group_result]))
        assert np.array_equal(test_filenames,
                              sorted([res['parameters']['test_filename'] for res in group_result]))
        print ("Result {:d} correct".format(i_group))
    else:
        assert (group_result[0]['parameters'].get('dataset_splitter', None) == '*train_test_splitter' or
            group_result[0]['parameters'].get('training_type', None) == '*csp_trainer')
        assert np.array_equal(all_runs_filenames,
                             sorted([res['parameters']['dataset_filename'] for res in group_result]))
        assert sorted([res['parameters']['test_filename'] for res in group_result]) == ['-'] * 18
        print ("Result {:d} correct".format(i_group))


