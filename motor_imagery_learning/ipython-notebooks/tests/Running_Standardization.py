
# coding: utf-8

# # plan:
# * get epoched data, compute variances as you did in variance analysis
# * use exponential running mean and exponential running var to standardize data
# * see variances again
# * fix if problems
# * put into pylearn preprocessor, add it to jobe experiment, run again... see what happens
# * try also for bhno.. hope for best for both play around with factors, block sizes etc...

# In[20]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# switch to cpu
os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
from matplotlib import cm
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from motor_imagery_learning.analysis.plot_util import plot_class_probs, plot_chan_matrices, plot_class_reconstruction,     plot_correct_part, plot_incorrect_part, plot_correctness, plot_misclasses, plot_sensor_signals
from  motor_imagery_learning.analysis.util import back_deconv, back_prop, back_guided_deconv, back_linear, bc01toinput,     load_preprocessed_datasets, back_guided_deconv_for_class, reshape_and_get_predictions,     get_trials_for_class, get_summed_activations, get_activations, get_activation,     check_prediction_correctness, load_train_test_datasets
from pylearn2.config import yaml_parse
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted, sort_topologically
from motor_imagery_learning.analysis.print_results import ResultPrinter
from copy import deepcopy
figsize=(13,8)
import psutil
import h5py
from scipy.io import loadmat
from pprint import pprint
from wyrm.types import Data
import wyrm
from motor_imagery_learning.preprocessing_util import (exponential_running_mean,
                                                       exponential_running_var)


# In[2]:

from motor_imagery_learning.bbci_dataset import BBCIDataset
from motor_imagery_learning.mywyrm.processing import resample_epo, highpass_cnt

bbci_set = BBCIDataset(filename='data/BBCI-without-last-runs/JoBeMoSc01S001R01_ds10_1-11.BBCI.mat', 
                        sensor_names=None, cnt_preprocessors=[(highpass_cnt, {'low_cut_off_hz': 0.5})],
                        epo_preprocessors=[(resample_epo, {'newfs': 200})])

bbci_set.load()


# In[3]:

variances = np.var(bbci_set.epo.data, axis=1)
# cap variances for nicer plotting
variances = np.minimum(variances, 4000)
plot_sensor_signals(variances.T, bbci_set.sensor_names,figsize=(12,32), yticks="onlymax").suptitle(
    'Variances after >0.5Hz-highpass', fontsize=20, y=0.92)


# In[4]:

bbci_set.epo.data.shape


# In[73]:

running_means = exponential_running_mean(bbci_set.epo.data, factor_new=0.1, init_block_size=30, axis=1)
running_vars = exponential_running_var(bbci_set.epo.data, running_means, factor_new=0.1,init_block_size=30, axis=1)
running_means = np.expand_dims(running_means, 1)
running_vars = np.expand_dims(running_vars, 1)
running_std = np.sqrt(running_vars)


# In[64]:

running_vars.shape


# In[74]:

standardize_epo_data = (bbci_set.epo.data - running_means) / running_std


# In[75]:

variances = np.var(standardize_epo_data, axis=1)
# cap variances for nicer plotting
variances = np.minimum(variances, 4000)
plot_sensor_signals(variances.T, bbci_set.sensor_names,figsize=(12,32), yticks="onlymax").suptitle(
    'Variances after >0.5Hz-highpass', fontsize=20, y=0.92)


# ## Problematic Trials analysis  

# In[71]:

chan_i = bbci_set.sensor_names.index('FFT7h')
plot_sensor_signals(bbci_set.epo.data[620,:,chan_i:chan_i+1].T, 
                    ['FFT7h'],figsize=(11,1), yticks="keep").suptitle(
                    'Ok Trial', fontsize=15, y=1.1)
pyplot.xlabel("200 samples=1 second")   
plot_sensor_signals(bbci_set.epo.data[660,:,chan_i:chan_i+1].T, 
                    ['FFT7h'],figsize=(11,1), yticks="keep").suptitle(
                    'Problematic Trial', fontsize=15, y=1.1)
pyplot.xlabel("200 samples=1 second")   


# ### After Running Standardization 

# In[76]:

chan_i = bbci_set.sensor_names.index('FFT7h')
plot_sensor_signals(standardize_epo_data[620,:,chan_i:chan_i+1].T, 
                    ['FFT7h'],figsize=(11,1), yticks="keep").suptitle(
                    'Ok Trial', fontsize=15, y=1.1)
pyplot.xlabel("200 samples=1 second")   
plot_sensor_signals(standardize_epo_data[660,:,chan_i:chan_i+1].T, 
                    ['FFT7h'],figsize=(11,1), yticks="keep").suptitle(
                    'Problematic Trial', fontsize=15, y=1.1)
pyplot.xlabel("200 samples=1 second")   


# ## Determine which frequencies are increased

# In[81]:

freq_bins = np.fft.rfftfreq(bbci_set.epo.data.shape[1], d=1./bbci_set.epo.fs)

bandpower_ok_trials = np.mean(np.abs(np.fft.rfft(bbci_set.epo.data[0:630,:,chan_i])) ** 2, axis=0) / bbci_set.epo.fs
bandpower_bad_trials = np.mean(np.abs(np.fft.rfft(bbci_set.epo.data[655:,:,chan_i])) ** 2, axis=0) / bbci_set.epo.fs
fig = pyplot.figure()

pyplot.plot(freq_bins, bandpower_ok_trials, figure=fig)
pyplot.plot(freq_bins, bandpower_bad_trials, figure=fig)
pyplot.legend(('good trials', 'bad trials'))


# ### After running standardization 

# In[82]:

freq_bins = np.fft.rfftfreq(standardize_epo_data.shape[1], d=1./bbci_set.epo.fs)

bandpower_ok_trials = np.mean(np.abs(np.fft.rfft(standardize_epo_data[0:630,:,chan_i])) ** 2, axis=0) / bbci_set.epo.fs
bandpower_bad_trials = np.mean(np.abs(np.fft.rfft(standardize_epo_data[655:,:,chan_i])) ** 2, axis=0) / bbci_set.epo.fs
fig = pyplot.figure()

pyplot.plot(freq_bins, bandpower_ok_trials, figure=fig)
pyplot.plot(freq_bins, bandpower_bad_trials, figure=fig)
pyplot.legend(('good trials', 'bad trials'))

