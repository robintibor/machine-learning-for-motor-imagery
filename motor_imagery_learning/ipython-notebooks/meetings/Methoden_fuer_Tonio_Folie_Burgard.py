
# coding: utf-8

# In[1]:

get_ipython().run_cell_magic(u'javascript', u'', u'function makeSVGDownloadable() {\n    $(\'body\').on(\'click\', \'svg\', function(){\n        var svg = this.outerHTML;\n        var b64 = btoa(svg); // or use btoa if supported\n        var linkToSVG = $("<a href-lang=\'image/svg+xml\' href=\'data:image/svg+xml;base64,\\n" + b64 +\n                          "\'title=\'file.svg\' target=\'blank\'>Download</a>");\n        $(this).after(linkToSVG);});\n}\nmakeSVGDownloadable()')


# In[17]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
#os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
from matplotlib import cm
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (8.0, 1.0)
matplotlib.rcParams['font.size'] = 7

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from analysis.plot_util import plot_class_probs, plot_chan_matrices, plot_class_reconstruction,     plot_correct_part, plot_incorrect_part, plot_correctness
from analysis.util import back_deconv, back_prop, back_guided_deconv, back_linear, bc01toinput,     load_preprocessed_datasets, back_guided_deconv_for_class, reshape_and_get_predictions,     get_trials_for_class, get_summed_activations, get_activations, get_activation
from motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted 
from pylearn2.config import yaml_parse
from copy import deepcopy


# ## CSP Visualization 

# In[10]:

from motor_imagery_learning.mywyrm.processing import resample_cnt, highpass_cnt, common_average_reference_cnt
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset
from motor_imagery_learning.bbci_pylearn_dataset import BBCISetNoCleaner
from motor_imagery_learning.mypylearn2.preprocessing import RestrictToTwoClasses
from wyrm.processing import apply_csp
from motor_imagery_learning.analysis.plot_util import plot_sensor_signals
from motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
from mywyrm.plot import ax_scalp,get_channelpos, CHANNEL_10_20_exact, CHANNEL_10_20
import matplotlib.cm as cmap

def get_gujo_data():
    gujo_epo = get_gujo_epo()
    restricted_sensor_names = ['Cp1', 'Cz', 'Cp2']
    few_sensor_epo = select_channels(gujo_epo, restricted_sensor_names, chanaxis=-1)
    class_pair = [0,1]#[0,1]
    two_class_epo = select_classes(few_sensor_epo, class_pair) 
    timeval_epo = select_ival(two_class_epo, [500,4000])    
    n_trials =  timeval_epo.data.shape[0]
    train_epo = select_epochs(timeval_epo, range(0, int(np.floor(n_trials * 0.9))))
    test_epo = select_epochs(timeval_epo, range(int(np.floor(n_trials * 0.9)), n_trials))
    return two_class_epo, train_epo, test_epo
    
def get_gujo_epo():
    filename = 'data/BBCI-without-last-runs/GuJoMoSc01S001R01_ds10_1-11.BBCI.mat'

    filterbank_args = dict(min_freq=11, max_freq=11, last_low_freq=11, low_width=4, high_width=4)
    sensor_names = get_C_sensors_sorted()

    dataset = BBCIPylearnCleanFilterbankDataset(filenames=filename,
           load_sensor_names=sensor_names, cleaner=BBCISetNoCleaner(),
          cnt_preprocessors=((resample_cnt, dict(newfs= 300)), 
                             (highpass_cnt, dict(low_cut_off_hz=0.5)),
                            (common_average_reference_cnt, dict())),
                                               **filterbank_args)

    dataset.load_bbci_set()
    epo = dataset.bbci_set.epo
    epo.data = np.squeeze(epo.data)
    epo.class_names = ['Right Hand', 'Left Hand', 'Rest', 'Feet']
    epo.axes = epo.axes[0:3] # remove filterband axis
    return epo


from wyrm.processing import select_classes, select_epochs, calculate_csp, select_channels, select_ival


def get_csp_filters_train_test(train_epo):
    filters, patterns, variances = calculate_csp(train_epo)
    return filters

def get_normal_and_csp_filtered_data(two_class_epo, filters):
    columns =[0,-1]
    n_trials =  two_class_epo.data.shape[0]
    test_epo_full = select_epochs(two_class_epo, range(int(np.floor(n_trials * 0.9)), n_trials))
    csp_filtered_full = apply_csp(test_epo_full, filters, columns)
    return test_epo_full, csp_filtered_full
def plot_combined_example(test_epo_full, csp_filtered_full):
    combined_sensor_trials = np.concatenate((test_epo_full.data[36], test_epo_full.data[37]))
    combined_csp_trials = np.concatenate((csp_filtered_full.data[36], csp_filtered_full.data[37]))
    combined_both = np.concatenate((combined_sensor_trials, combined_csp_trials), axis=1)
    plot_sensor_names =  test_epo_full.axes[2].tolist() + ['CSP1', 'CSP2']
    fig = plot_sensor_signals(combined_both.T, sensor_names=plot_sensor_names, figsize=(8,4))
    for ax in fig.axes:
        ax.axvline(x=1200, color='black', lw=1.25)

    pyplot.plot([0.125, 0.9], [0.425,0.425], color='grey',
        lw=1,transform=fig.transFigure, clip_on=False, linestyle="-")
    pyplot.text(0.25,1.2,"Left Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
    pyplot.text(0.75,1.2,"Right Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
    pyplot.xticks(np.arange(0,2401,300), 
          [0,1000,2000,3000] + ['4000/0'] + [1000,2000,3000,4000])
    pyplot.xlabel("milliseconds")
    
def plot_csp_example(csp_filtered_full):
    combined_csp_trials = np.concatenate((csp_filtered_full.data[36], 
                                          csp_filtered_full.data[37]))
    plot_sensor_names =  ['CSP1', 'CSP2']
    fig = plot_sensor_signals(combined_csp_trials.T, sensor_names=plot_sensor_names,
                              figsize=(8,2))
    for ax in fig.axes:
        ax.axvline(x=1200, color='black', lw=1.25)

    pyplot.text(0.25,1.2,"Left Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
    pyplot.text(0.75,1.2,"Right Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
    pyplot.xticks(np.arange(0,2401,300), 
          [0,1000,2000,3000] + ['4000/0'] + [1000,2000,3000,4000])
    pyplot.xlabel("milliseconds")
    
def plot_sensor_example(test_epo_full):
    combined_sensor_trials = np.concatenate((test_epo_full.data[36], 
                                             test_epo_full.data[37]))
    plot_sensor_names =  test_epo_full.axes[2].tolist()
    fig = plot_sensor_signals(combined_sensor_trials.T, sensor_names=plot_sensor_names, 
                              figsize=(8,2.5))
    for ax in fig.axes:
        ax.axvline(x=1200, color='black', lw=1.25)

    pyplot.text(0.25,1.2,"Left Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
    pyplot.text(0.75,1.2,"Right Hand",transform=fig.axes[0].transAxes, fontsize=12, horizontalalignment='center')
    pyplot.xticks(np.arange(0,2401,300), 
          [0,1000,2000,3000] + ['4000/0'] + [1000,2000,3000,4000])
    pyplot.xlabel("milliseconds")
    return None
    
    
from motor_imagery_learning.analysis.data_generation import randomly_shifted_sines
import scipy
from numpy.random import RandomState


def compute_csp(class_1_signal, class_2_signal):
    #pyplot.plot(sine_signal)
    c1 = np.cov(class_1_signal)
    c2 = np.cov(class_2_signal)
    #d, v = scipy.linalg.eig(c1-c2, c1+c2)
    #d, v = scipy.linalg.eig(c1-c2, c1 +c2)
    d, v = scipy.linalg.eig(c1, c2)
    d = d.real
    # make sure the eigenvalues and -vectors are correctly sorted
    indx = np.argsort(d)
    # reverse
    indx = indx[::-1]

    d = d.take(indx)
    v = v.take(indx, axis=1)
    a = scipy.linalg.inv(v).transpose()
    
    csp_filtered_class_1 = np.dot(class_1_signal.T, v)
    csp_filtered_class_2 = np.dot(class_2_signal.T, v)
    
    return v, d, csp_filtered_class_1, csp_filtered_class_2


# In[11]:


two_class_epo, train_epo, test_epo = get_gujo_data()
filters = get_csp_filters_train_test(train_epo)
test_epo_full, csp_filtered_full = get_normal_and_csp_filtered_data(two_class_epo, filters)


# In[12]:




# In[36]:

pyplot.plot(trials_csp_1_for_plot)


# In[33]:


pyplot.plot(trials_csp_2_for_plot)


# In[47]:

# handpicked trial parts to look good :) thats why -800:-200 for trial 37 csp 1 :))
trials_csp_1_for_plot = np.concatenate((csp_filtered_full.data[36, -600::2, 0],
                                        csp_filtered_full.data[37, -800:-200:2, 0]))

trials_csp_2_for_plot = np.concatenate((csp_filtered_full.data[36, -600::2, -1], 
                                        csp_filtered_full.data[37, -600::2, -1]))
f, (ax1, ax2) = pyplot.subplots(2, sharex=True, sharey=True, figsize=(3,2))
x_line_coord = len(trials_csp_1_for_plot) / 2
yline_coord = (-6,6)
ax1.plot(trials_csp_1_for_plot)
ax1.plot((x_line_coord,x_line_coord), yline_coord, 'k-')
ax1.text(60,8.0, "Class 1", fontsize=16)
ax1.text(360,8.0, "Class 2", fontsize=16)
ax2.plot(trials_csp_2_for_plot)
ax2.plot((x_line_coord,x_line_coord), yline_coord, 'k-')
# Fine-tune figure; make subplots close to each other and hide x ticks for
# all but bottom plot.
f.subplots_adjust(hspace=0)
pyplot.setp([a.get_xticklabels() for a in f.axes], visible=False)
pyplot.xticks([])
pyplot.yticks([])
None


# ### With envelope 

# In[59]:

squared_csp_1 = np.square(trials_csp_1_for_plot)
squared_csp_2 = np.square(trials_csp_2_for_plot)


f, (ax1, ax2) = pyplot.subplots(2, sharex=True, sharey=True, figsize=(3,2))
x_line_coord = len(squared_csp_1) / 2
yline_coord = (-30,30)
ax1.plot(squared_csp_1)
ax1.plot((x_line_coord,x_line_coord), yline_coord, 'k-')
ax1.text(60,40.0, "Class 1", fontsize=16)
ax1.text(360,40.0, "Class 2", fontsize=16)
ax2.plot(squared_csp_2)
ax2.plot((x_line_coord,x_line_coord), yline_coord, 'k-')
# Fine-tune figure; make subplots close to each other and hide x ticks for
# all but bottom plot.
f.subplots_adjust(hspace=0)
pyplot.setp([a.get_xticklabels() for a in f.axes], visible=False)
pyplot.xticks([])
pyplot.yticks([])
None


# ### Show Log Of Sum Of Envelope => LDA Feature 

# In[55]:

a = np.log(np.sum(env_csp_1[:300]))
b = np.log(np.sum(env_csp_1[300:]))
c = np.log(np.sum(env_csp_2[:300]))
d = np.log(np.sum(env_csp_2[300:]))
x_line_coord = 1.9
pyplot.figure(figsize=(3,2))

pyplot.bar(range(4),[a,b,c,d] - np.mean([a,b,c,d])) # meancorrect for visualization
pyplot.plot((x_line_coord,x_line_coord), (-0.7, 0.7), 'k-')
pyplot.text(0.3,0.75, "Class 1", fontsize=16)
pyplot.text(2.3,0.75, "Class 2", fontsize=16)
pyplot.ylim(-0.7,0.7)
#pyplot.setp([a.get_xticklabels() for a in f.axes], visible=False)
pyplot.xticks([])
pyplot.yticks([])


# In[8]:

wanted_sensors = ('C3', 'CPz', 'C4')
sensor_inds = [sensor_names.index(name) for name in wanted_sensors]
trials_sensor_1_for_plt = np.concatenate((trials_1_flat_for_cov[sensor_inds[0], 24300:24600].T, 
                                          trials_2_flat_for_cov[sensor_inds[0], 22400:22700].T))
trials_sensor_2_for_plt = np.concatenate((trials_1_flat_for_cov[sensor_inds[1], 24300:24600].T, 
                                          trials_2_flat_for_cov[sensor_inds[1], 22400:22700].T))
trials_sensor_3_for_plt = np.concatenate((trials_1_flat_for_cov[sensor_inds[2], 24300:24600].T, 
                                          trials_2_flat_for_cov[sensor_inds[2], 22400:22700].T))

f, (ax1, ax2, ax3) = pyplot.subplots(3, sharex=True, sharey=True, figsize=(3,3))
ax1.plot(trials_sensor_1_for_plt)
x_line_coord = len(trials_sensor_1_for_plt) / 2
ax1.plot((x_line_coord, x_line_coord), (-6, 6), 'k-')
ax1.text(60,8, "Class 1", fontsize=16)
ax1.text(360,8, "Class 2", fontsize=16)
ax1.text(-100,0, wanted_sensors[0], fontsize=15)
ax2.plot(trials_sensor_2_for_plt)
ax2.plot((x_line_coord,x_line_coord), (-6, 6), 'k-')
ax2.text(-120,0, wanted_sensors[1], fontsize=15)
ax3.plot(trials_sensor_3_for_plt)
ax3.plot((x_line_coord,x_line_coord), (-6, 6), 'k-')
ax3.text(-100,0, wanted_sensors[2], fontsize=15)
# Fine-tune figure; make subplots close to each other and hide x ticks for
# all but bottom plot.
f.subplots_adjust(hspace=0)
pyplot.setp([a.get_xticklabels() for a in f.axes], visible=False)
pyplot.xticks([])
pyplot.yticks([])


# ### Second signals, different band, for csp  as a net 
# 
# * copy code, do it again for passband 2, maybe even loop?
# * remove class 2 texts
# * put them on top of each other in inkscape
# 

# In[22]:

csp_features = []
for passband in [2,4]:
    trials_1 = get_trials_for_class(mavoset, 0)
    trials_2 = get_trials_for_class(mavoset, 1)
    
    # only take 5th bandpassbin
    trials_1 = trials_1[:,:,:,passband]
    trials_2 = trials_2[:,:,:,passband]
    
    trials_1_flat_for_cov = trials_1.transpose((1,0,2)).reshape(len(sensor_names), -1)
    trials_2_flat_for_cov = trials_2.transpose((1,0,2)).reshape(len(sensor_names), -1)
    
    # compute csp
    trials_1_cov = np.cov(trials_1_flat_for_cov)
    trials_2_cov = np.cov(trials_2_flat_for_cov)
    csp_filters = csp_base(trials_1_cov, trials_2_cov)
    trials_1_filtered = np.dot(csp_filters, trials_1_flat_for_cov)
    trials_2_filtered = np.dot(csp_filters, trials_2_flat_for_cov)
    trials_csp_1_for_plt = (trials_1_filtered[0, 24300:24600].T)
    trials_csp_2_for_plt = (trials_1_filtered[-1, 24300:24600].T)
    
    ## Plot sensors
    
    wanted_sensors = ('C3', 'CPz', 'C4')
    sensor_inds = [sensor_names.index(name) for name in wanted_sensors]
    trials_sensor_1_for_plt = (trials_1_flat_for_cov[sensor_inds[0], 24300:24600].T)
    trials_sensor_2_for_plt = (trials_1_flat_for_cov[sensor_inds[1], 24300:24600].T)
    trials_sensor_3_for_plt = (trials_1_flat_for_cov[sensor_inds[2], 24300:24600].T)

    f, (ax1, ax2, ax3) = pyplot.subplots(3, sharex=True, sharey=True, figsize=(2,3))
    ax1.plot(trials_sensor_1_for_plt)
    x_line_coord = len(trials_sensor_1_for_plt) / 2
    ax1.text(-80,0, wanted_sensors[0], fontsize=15)
    ax2.plot(trials_sensor_2_for_plt)
    ax2.text(-100,0, wanted_sensors[1], fontsize=15)
    ax3.plot(trials_sensor_3_for_plt)
    ax3.text(-80,0, wanted_sensors[2], fontsize=15)
    # Fine-tune figure; make subplots close to each other and hide x ticks for
    # all but bottom plot.
    f.subplots_adjust(hspace=0)
    pyplot.setp([a.get_xticklabels() for a in f.axes], visible=False)
    pyplot.xticks([])
    pyplot.yticks([])
    
    ## Plot CSP-filtered signals

    f, (ax1, ax2) = pyplot.subplots(2, sharex=True, sharey=True, figsize=(2,2))
    x_line_coord = len(trials_csp_1_for_plt) / 2
    ax1.plot(trials_csp_1_for_plt)
    ax2.plot(trials_csp_2_for_plt)
    # Fine-tune figure; make subplots close to each other and hide x ticks for
    # all but bottom plot.
    f.subplots_adjust(hspace=0)
    pyplot.setp([a.get_xticklabels() for a in f.axes], visible=False)
    pyplot.xticks([])
    pyplot.yticks([])
    
    # plot squared csp filtered signals
    f, (ax1, ax2) = pyplot.subplots(2, sharex=True, sharey=True, figsize=(2,2))
    x_line_coord = len(trials_csp_1_for_plt) / 2
    ax1.plot(trials_csp_1_for_plt ** 2)
    ax2.plot(trials_csp_2_for_plt ** 2)
    f.subplots_adjust(hspace=0)
    pyplot.setp([a.get_xticklabels() for a in f.axes], visible=False)
    pyplot.xticks([])
    pyplot.yticks([])
    csp_features.append(np.log(np.sum(trials_csp_1_for_plt ** 2)))
    csp_features.append(np.log(np.sum(trials_csp_2_for_plt ** 2)))


# In[31]:

pyplot.figure(figsize=(3,2))
pyplot.bar(np.array(range(4)) + 0.25, csp_features)
pyplot.xticks([])
pyplot.yticks([])


# ### Bandpower

# In[9]:

from motor_imagery_learning.motor_eeg_dataset import FFTMotorEEGDataset
from pylearn2.datasets.preprocessing import Standardize
mavoset = FFTMotorEEGDataset('data/motor-imagery-tonio/MaVoMoSc1S001R01_ds10_1-11_autoclean_trials_ival_0_4000.mat', 
                             topo_view_channel='auto_clean_trials_500Hz_500ms_len_250ms_stride_ps', 
                             target_channel='targets', start=0, stop=400,
                             sensor_names=sensor_names, frequency_start=6, frequency_stop=20)
preproc = Standardize()
preproc.apply(mavoset, can_fit=True)


# In[10]:

bp_trials_1 = get_trials_for_class(mavoset, 0)
bp_trials_2 = get_trials_for_class(mavoset, 0)
chosen_sensors = ('CP3', 'CP4')
sensor_inds = [sensor_names.index(name) for name in chosen_sensors]#'CPz', 
#plot_chan_matrices(bp_trials_1[0, sensor_inds, : ,:], chosen_sensors, figsize=(10,4))
trial_matrices = bp_trials_1[0, sensor_inds, : ,:]
figure = pyplot.figure(figsize=(5,2))
ax = figure.add_subplot(1, 2, 1)
ax.set_title(chosen_sensors[0],fontsize=20)
ax.imshow(trial_matrices[0].T,
                interpolation='nearest', cmap=cm.bwr, origin='lower', 
                vmin=-1, vmax=1,
                aspect='auto')
ax.set_xticks([])
ax.set_yticks([])

ax = figure.add_subplot(1, 2, 2)
ax.set_title(chosen_sensors[1], fontsize=20)
ax.imshow(trial_matrices[1].T,
                interpolation='nearest', cmap=cm.bwr, origin='lower', 
                vmin=-1, vmax=1,
                aspect='auto')
ax.set_xticks([])
ax.set_yticks([])



# ### Raw Conv Net

# In[11]:

from motor_imagery_learning.motor_eeg_dataset import RawMotorEEGDataset
#sensor_names=['C4', 'CP3', 'CP4', 'CP1', 'CP2']
sensor_names=['CP1', 'CPz', 'CP2', 'C4', 'CP3', 'CP4']
sensor_names = get_C_sensors_sorted()
mavoset = RawMotorEEGDataset('data/motor-imagery-tonio/MaVoMoSc1S001R01_ds10_1-11_autoclean_trials_ival_0_4000.mat', 
                             topo_view_channel='auto_clean_trials_150Hz', target_channel='targets',
                             start=0, stop=400, sensor_names=sensor_names)



# In[12]:

raw_trials_1 = get_trials_for_class(mavoset, 0)
raw_trial = raw_trials_1[50, :, 0:300,:]
wanted_sensors = ('C3', 'CPz', 'C4')
sensor_inds = [sensor_names.index(name) for name in wanted_sensors]
f, (ax1, ax2, ax3) = pyplot.subplots(3, sharex=True, figsize=(3,3))
ax1.plot(raw_trial[sensor_inds[0]])
ax1.set_yticks([])
ax1.text(-50,np.mean(raw_trial[sensor_inds[0]]) - 4, wanted_sensors[0], fontsize=15)
ax2.plot(raw_trial[sensor_inds[1]])
ax2.set_yticks([])
ax2.text(-60,np.mean(raw_trial[sensor_inds[1]]), wanted_sensors[1], fontsize=15)
ax3.plot(raw_trial[sensor_inds[2]])
ax3.set_yticks([])
ax3.text(-50,np.mean(raw_trial[sensor_inds[2]]) -2, wanted_sensors[2], fontsize=15)
# Fine-tune figure; make subplots close to each other and hide x ticks for
# all but bottom plot.
f.subplots_adjust(hspace=0)
pyplot.setp([a.get_xticklabels() for a in f.axes], visible=False)
pyplot.xticks([])
#, sharey=True


# In[5]:



def csp_base(C_a, C_b):
  '''
  Calculate the full common spatial patterns (CSP) transform. 
  The CSP transform finds spatial filters that maximize the variance
  in one condition, and minimize the signal's variance in the other.
  See [1]. Usually, only a subset of the spatial filters is used.
  Parameters
  ----------
  C_a : array-like of shape (n, n)
    Sensor covariance in condition 1.
  C_b : array-like of shape (n, n)
    Sensor covariance in condition 2.
  Returns
  -------
  W : array of shape (m, n)
    A matrix with m spatial filters with decreasing variance in the
    first condition. The rank of (C_a + C_b) determines the number
    of filters m.
  See also
  --------
  csp : common spatial patterns algorithm.
  outer_n : pick indices from both sides.
  References
  ----------
  [1] Zoltan J. Koles. The quantitative extraction and topographic
  mapping of the abnormal components in the clinical EEG.
  Electroencephalography and Clinical Neurophysiology,
  79(6):440--447, December 1991.
  Examples:
  ---------
  In condition 1 the signals are positively correlated, in condition 2
  they are negatively correlated. Their variance stays the same:
  >>> C_1 = np.ones((2, 2))
  >>> C_1
  array([[ 1.,  1.],
         [ 1.,  1.]])
  >>> C_2 = 2 * np.eye(2) - np.ones((2, 2))
  >>> C_2
  array([[ 1., -1.],
         [-1.,  1.]])
  The most differentiating projection is found with the CSP transform:
  >>> csp_base(C_1, C_2).round(2)
  array([[-0.5,  0.5],
         [ 0.5,  0.5]])
  '''
  P = whitener(C_a + C_b)
  P_C_b = reduce(np.dot, [P, C_b, P.T])
  _, _, B = np.linalg.svd((P_C_b))
  return np.dot(B, P.T)


def csp(C_a, C_b, m):
  '''
  Calculate common spatial patterns (CSP) transform. 
  The CSP transform finds spatial filters that maximize the variance
  in one condition, and minimize the signal's variance in the other.
  See [1]. Usually, only a subset of the spatial filters is used.
  Parameters
  ----------
  C_a : array-like of shape (n, n)
    Sensor covariance in condition 1.
  C_b : array-like of shape (n, n)
    Sensor covariance in condition 2.
  m : int
    The number of CSP filters to extract.
  Returns
  -------
  W : array of shape (m, n)
    A matrix with m/2 spatial filters that maximize the variance in
    one condition, and m/2 that maximize the variance in the other.
  See also
  --------
  csp_base : full common spatial patterns transform.
  outer_n : pick indices from both sides.
  References
  ----------
  [1] Zoltan J. Koles. The quantitative extraction and topographic
  mapping of the abnormal components in the clinical EEG.
  Electroencephalography and Clinical Neurophysiology,
  79(6):440--447, December 1991.
  Examples:
  ---------
  We construct two nearly identical covariance matrices for condition
  1 and 2:
  >>> C_1 = np.eye(4)
  >>> C_2 = np.eye(4)
  >>> C_2[1, 3] = 1
  The difference between the conditions is in the 2nd and 4th sensor:
  >>> C_2 - C_1
  array([[ 0.,  0.,  0.,  0.],
         [ 0.,  0.,  0.,  1.],
         [ 0.,  0.,  0.,  0.],
         [ 0.,  0.,  0.,  0.]])
  The two most differentiating projections are found with the CSP transform.
  Indeed, it projects the same sensors:
  >>> csp(C_1, C_2, 2).round(2)
  array([[ 0.  ,  0.37,  0.  ,  0.6 ],
         [ 0.  , -0.6 ,  0.  ,  0.37]])
  '''
  W = csp_base(C_a, C_b)
  assert W.shape[1] >= m
  return W[outer_n(m)]


def whitener(C, rtol=1e-15):
  '''
  Calculate the whitening transform for signals with covariance C.
  The whitening transform is used to remove covariance between
  signals, and can be regarded as principal component analysis with
  rescaling and an optional rotation. The whitening transform is
  calculated as C^{-1/2}, and implemented to work with rank-deficient
  covariance matrices.
  Parameters
  ----------
  C : array-like, shape (p, p)
    Covariance matrix of the signals to be whitened.
  rtol : float, optional
    Cut-off value specified as the fraction of the largest eigenvalue
    of C.
  Returns
  -------
  W : array, shape (p, p)
    Symmetric matrix with spatial filters in the rows.
  See also
  --------
  car : common average reference
  Examples
  --------
  >>> X = np.random.randn(3, 100) 
  >>> W = whitener(np.cov(X))
  >>> X_2 = np.dot(W, X - np.mean(X, axis=1).reshape(-1, 1))
  The covariance of X_2 is now close to the identity matrix:
  >>> np.linalg.norm(np.cov(X_2) - np.eye(3)) < 1e-10
  True
  '''
  e, E = np.linalg.eigh(C)
  return reduce(np.dot, 
    [E, np.diag(np.where(e > np.max(e) * rtol, e, np.inf)**-.5), E.T])


