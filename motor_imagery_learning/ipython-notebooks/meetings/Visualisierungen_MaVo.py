
# coding: utf-8

# # Visualisierungen Mavo

# ## Rekonstruktionen der Klassen aus dem trainierten Netz

# In[20]:

plot_class_reconstruction(mavo_model, classes=[0,1,2,3], figsize=figsize)


# ## Rekonstruktion Neuronen mit größter Aktivierung Klasse 1

# In[21]:

plot_most_activated_neurons(mavo_class_1_activations, mavo_model.layers, 3, plot_reconstruction_and_class_probs, figsize=figsize)


# In[16]:

fold_nr = 0
mavo_model = serial.load('data/gpu-models/bandpower/cross-validation/early-stop/21.pkl')[fold_nr]
mavo_datasets = load_preprocessed_datasets(mavo_model, fold_nr=fold_nr)
print mavo_model.monitor.channels['test_y_misclass'].val_record[-1]
mavo_class_1_trials = get_trials_for_class(mavo_datasets['test'], 1)
mavo_class_1_activations= get_activations(mavo_model.layers, bc01toinput(mavo_model, mavo_class_1_trials).astype(np.float32))


# In[17]:

import os
get_ipython().magic(u'cd /lokal/schirrmr/motor-imagery/code/motor_imagery_learning/')
os.sys.path.insert(0, os.path.abspath('..')) 
os.sys.path.insert(0, os.path.abspath('../..')) 
#%cd /home/robintibor/farm04/motor-imagery/code/motor_imagery_learning
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
from matplotlib import cm
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from analysis.plot_util import (plot_class_probs, plot_chan_matrices, plot_class_reconstruction, 
    plot_correct_part, plot_incorrect_part, plot_correctness, plot_correctly_predicted_trials,
    plot_incorrectly_predicted_trials, plot_only_preds_and_class_probs, plot_reconstruction_and_class_probs)
from analysis.util import back_deconv, back_prop, back_guided_deconv, back_linear, bc01toinput,     load_preprocessed_datasets, back_guided_deconv_for_class, reshape_and_get_predictions,     get_trials_for_class, get_summed_activations, get_activations, get_activation
from motor_imagery_learning.analysis.sensor_positions import sort_topologically, get_C_sensors_sorted
from pylearn2.config import yaml_parse
from copy import deepcopy
figsize=(12,8)


# In[18]:

get_ipython().run_cell_magic(u'javascript', u'', u'function makeSVGDownloadable() {\n    $(\'body\').on(\'click\', \'svg\', function(){\n        var svg = this.outerHTML;\n        var b64 = btoa(svg); // or use btoa if supported\n        var linkToSVG = $("<a href-lang=\'image/svg+xml\' href=\'data:image/svg+xml;base64,\\n" + b64 +\n                          "\'title=\'file.svg\' target=\'blank\'>Download</a>");\n        $(this).after(linkToSVG);});\n}\nmakeSVGDownloadable()')


# In[19]:

def plot_most_activated_neurons(activations, layers, num_neurons, plotfunction, figsize=(13,7)):
    sum_per_neuron = np.sum(np.array(activations), axis=0)
    layer_1_sums = sum_per_neuron[1]
    strongest_neurons = np.argsort(layer_1_sums)[::-1][0:num_neurons]
    plotfunction(strongest_neurons, layers, num_neurons, figsize)
    
def plot_most_variant_neurons(activations, layers, num_neurons, plotfunction, figsize=(13,7)):
    var_per_neuron = np.var(np.array(activations), axis=0)
    layer_1_var = var_per_neuron[1]
    variant_neurons = np.argsort(layer_1_var)[::-1][0:num_neurons]
    plotfunction(variant_neurons, layers, num_neurons, figsize)
    
def plot_reconstruction_and_class_probs(neuron_ids, layers, num_neurons, figsize):
    for neuron_i in neuron_ids:
        wanted_inputs = [0] * 80
        wanted_inputs[neuron_i] = 1
        neuron_deconved = back_deconv(layers[0:2], wanted_inputs)
        neuron_deconved = neuron_deconved[:,::-1,::-1] # flip back as conv filters should b flipped
        plot_chan_matrices(neuron_deconved, get_C_sensors_sorted(), figsize=figsize)
        plot_class_probs(layers[2].get_weights()[neuron_i])
        
def plot_only_class_probs(neuron_ids, layers, num_neurons, figsize):
    for neuron_i in neuron_ids:
         plot_class_probs(layers[2].get_weights()[neuron_i])

