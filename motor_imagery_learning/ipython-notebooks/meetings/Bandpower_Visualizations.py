
# coding: utf-8

# In[50]:

import os
os.sys.path.insert(0,'/lokal/schirrmr/motor-imagery/code/') 
os.sys.path.insert(0,'/lokal/schirrmr/motor-imagery/code/motor_imagery_learning/') 
get_ipython().magic(u'cd /lokal/schirrmr/motor-imagery/code/motor_imagery_learning/')
#%cd /home/robintibor/farm04/motor-imagery/code/motor_imagery_learning
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7
from motor_imagery_learning.analysis.util import (check_prediction_correctness,
                                                 check_prediction_correctness_for_datasets,
                                                 load_preprocessed_datasets,
                                                 reshape_and_get_predictions,
                                                 get_trials_for_class)
from motor_imagery_learning.analysis.plot_util import (plot_confusion_matrix_for_averaged_result,
                                plot_confusion_matrix_for_result,
                                plot_misclasses, plot_sensor_signals,
                                plot_chan_matrices)
from motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
from pylearn2.datasets.preprocessing import Standardize
from pylearn2.utils import serial
from pylearn2.space import Conv2DSpace
import numpy as np
from motor_eeg_dataset import get_available_sensors_topo
from pylearn2.config import yaml_parse
from scipy.stats import  entropy
from numpy.random import RandomState
from IPython.display import HTML
from  motor_imagery_learning.analysis.plot_nets import plot_head_images, plot_class_weights_and_head_images
import theano.tensor as T
import theano


# In[2]:

get_ipython().run_cell_magic(u'javascript', u'', u'function makeSVGDownloadable() {\n    $(\'body\').on(\'click\', \'svg\', function(){\n        var svg = this.outerHTML;\n        var b64 = btoa(svg); // or use btoa if supported\n        var linkToSVG = $("<a href-lang=\'image/svg+xml\' href=\'data:image/svg+xml;base64,\\n" + b64 +\n                          "\'title=\'file.svg\' target=\'blank\'>Download</a>");\n        $(this).after(linkToSVG);});\n}\nmakeSVGDownloadable()')


# In[3]:

sensor_names = get_C_sensors_sorted()
bhnotrain_state = serial.load('data/gpu-models/bandpower/early-stop/4.pkl')
datasets = load_preprocessed_datasets(bhnotrain_state, preprocessor=Standardize())


# ## Class Means (after standardization across all trials)

# **Red always indicates positive, blue negative values, white is zero**

# In[6]:

mean_class_1 = np.mean(get_trials_for_class(datasets['train'], 0), axis=0)
mean_class_2 = np.mean(get_trials_for_class(datasets['train'], 1), axis=0)
mean_class_3 = np.mean(get_trials_for_class(datasets['train'], 2), axis=0)
mean_class_4 = np.mean(get_trials_for_class(datasets['train'], 3), axis=0)


# In[58]:

plot_chan_matrices(mean_class_1, sensor_names, figsize=(11,6.5)).suptitle('Right', fontsize=16)
plot_chan_matrices(mean_class_2, sensor_names, figsize=(11,6.5)).suptitle('Left', fontsize=16)
plot_chan_matrices(mean_class_3, sensor_names, figsize=(11,6.5)).suptitle('Rest', fontsize=16)
plot_chan_matrices(mean_class_4, sensor_names, figsize=(11,6.5)).suptitle('Feet', fontsize=16)


# ## Trials weighted by neuron activations 

# Find out what trials are typical for a neuron.
# 
# 1. For all trials in the dataset, weigh trials by how strongly activated they are for that neuron.
# 2. Compute mean of these weighted trials.
# 
# I also put the class weights of that neuron below.
# 
# Advantage:
# 
# * Fairly clear how plots are created
# 
# Disadvantage:
# 
# * Doesn't have to show what "feature" neuron actually learns

# In[16]:

# First test: just weigh by predictions
all_preds = reshape_and_get_predictions(bhnotrain_state.model, datasets['train'].get_topological_view())
all_trials = datasets['train'].get_topological_view()


# In[17]:


inputs = T.ftensor4()
activations = bhnotrain_state.model.fprop(inputs, return_all=True)
fprop_all_activations = theano.function([inputs], activations, allow_input_downcast=True)


activations = fprop_all_activations(all_trials)


# In[19]:

num_neurons = len(activations[1][0])
mean_fullreclin_activations = [np.mean(all_trials * activations[1][ :, neuron_i, np.newaxis, np.newaxis, np.newaxis],
                                       axis=0)
               for neuron_i in range(num_neurons)]

mean_fullreclin_activations = np.array(mean_fullreclin_activations)


# In[21]:

softmax_layer = bhnotrain_state.model.layers[2]

class_weights = softmax_layer.get_weights()


# In[25]:

for start in np.arange(0,80,8):
    this_activations = mean_fullreclin_activations[start:start+8]
    this_class_weights = class_weights[start:start+8]
    _ = plot_class_weights_and_head_images(this_activations, sensor_names, this_class_weights, figsize=(12,10))


# ## "Deconved" Neurons 

# ### Explanation 

# We want to get an idea what inputs a neuron "likes". So we go backward through our network, intuition is similar to this:
# 
# * Assume neuron wants inputs equal to its weights
# * What outputs do we need from layer below?
# * Given those outputs, what would the inputs for the layer below need to be?
# * Repeat until you arrive at the bottom layer=> wanted inputs
# 

# <img src="https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/meeting-architectures/unconv_explanation.svg" style="height:1500px;"> </img>

# ### 

# In[59]:

from motor_imagery_learning.analysis.util import back_deconv
softmax_layer = bhnotrain_state.model.layers[2]

class_weights = softmax_layer.get_weights()
all_wanted_inputs = []
for neuron_i in range(80):
    wanted_linear_output = [0] * 80
    wanted_linear_output[neuron_i] = 1

    wanted_inputs = back_deconv(bhnotrain_state.model.layers[0:2], wanted_linear_output)
    all_wanted_inputs.append(wanted_inputs)


# In[62]:

for neuron_i in np.arange(0,80,8):
    this_inputs = all_wanted_inputs[neuron_i:neuron_i+8]
    this_weights = class_weights[neuron_i:neuron_i+8]
    _ = plot_class_weights_and_head_images(this_inputs, sensor_names, this_weights, figsize=(12,10))


# ## Trials weighted by prediction/output 

# In[12]:

plot_chan_matrices(np.mean(all_trials * all_preds[:, 0, np.newaxis, np.newaxis, np.newaxis], axis=0), sensor_names).suptitle(
    'Right', fontsize=14, y=1.05)
plot_chan_matrices(np.mean(all_trials * all_preds[:, 1, np.newaxis, np.newaxis, np.newaxis], axis=0), sensor_names).suptitle(
    'Left', fontsize=14, y=1.05)
plot_chan_matrices(np.mean(all_trials * all_preds[:, 2, np.newaxis, np.newaxis, np.newaxis], axis=0), sensor_names).suptitle(
    'Rest', fontsize=14, y=1.05)
plot_chan_matrices(np.mean(all_trials * all_preds[:, 3, np.newaxis, np.newaxis, np.newaxis], axis=0), sensor_names).suptitle(
    'Feet', fontsize=14, y=1.05)

