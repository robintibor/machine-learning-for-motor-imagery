
# coding: utf-8

# In[1]:

import os
os.sys.path.insert(0,'/lokal/schirrmr/motor-imagery/code/') 
os.sys.path.insert(0,'/lokal/schirrmr/motor-imagery/code/motor_imagery_learning/') 
get_ipython().magic(u'cd /lokal/schirrmr/motor-imagery/code/motor_imagery_learning/')
#%cd /home/robintibor/farm04/motor-imagery/code/motor_imagery_learning
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7
from motor_imagery_learning.analysis.util import (check_prediction_correctness,
                                                 check_prediction_correctness_for_datasets,
                                                 load_preprocessed_datasets,
                                                 reshape_and_get_predictions,
                                                 get_trials_for_class)
from motor_imagery_learning.analysis.plot_util import (plot_confusion_matrix_for_averaged_result,
                                plot_confusion_matrix_for_result,
                                plot_misclasses, plot_sensor_signals,
                                plot_chan_matrices)
from motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
from pylearn2.datasets.preprocessing import Standardize, Pipeline
from motor_imagery_learning.mypylearn2.preprocessing import (OnlineChannelwiseStandardize,
                                                             RemoveTrialChannelMean)
from pylearn2.utils import serial
from pylearn2.space import Conv2DSpace
import numpy as np
from motor_eeg_dataset import get_available_sensors_topo
from pylearn2.config import yaml_parse
from scipy.stats import  entropy
from numpy.random import RandomState
from IPython.display import HTML
from  motor_imagery_learning.analysis.plot_nets import plot_head_images, plot_class_weights_and_head_images
import theano.tensor as T
import theano


# In[2]:

get_ipython().run_cell_magic(u'javascript', u'', u'function makeSVGDownloadable() {\n    $(\'body\').on(\'click\', \'svg\', function(){\n        var svg = this.outerHTML;\n        var b64 = btoa(svg); // or use btoa if supported\n        var linkToSVG = $("<a href-lang=\'image/svg+xml\' href=\'data:image/svg+xml;base64,\\n" + b64 +\n                          "\'title=\'file.svg\' target=\'blank\'>Download</a>");\n        $(this).after(linkToSVG);});\n}\nmakeSVGDownloadable()')


# In[3]:

sensor_names = get_C_sensors_sorted()
fft_train_state = serial.load('data/gpu-models/bandpower/early-stop/4.pkl')
fft_datasets = load_preprocessed_datasets(fft_train_state, preprocessor=Standardize())


# In[4]:

raw_preprocessor = Pipeline(items=[RemoveTrialChannelMean(), 
                               OnlineChannelwiseStandardize(use_only_new=False, new_factor=1.0)])
raw_train_state = serial.load('data/gpu-models/raw-data-demeaned/early-stop/2.pkl')
raw_datasets = load_preprocessed_datasets(raw_train_state, preprocessor=raw_preprocessor)


# In[6]:

check_prediction_correctness_for_datasets(raw_train_state, raw_datasets)


# ## Class Means 

# In[7]:

mean_class_1 = np.mean(get_trials_for_class(raw_datasets['train'], 0), axis=0)
mean_class_2 = np.mean(get_trials_for_class(raw_datasets['train'], 1), axis=0)
mean_class_3 = np.mean(get_trials_for_class(raw_datasets['train'], 2), axis=0)
mean_class_4 = np.mean(get_trials_for_class(raw_datasets['train'], 3), axis=0)


# In[8]:

from motor_imagery_learning.analysis.plot_util import plot_head_signals
plot_head_signals(mean_class_1,sensor_names).suptitle('Right', fontsize=16)
plot_head_signals(mean_class_2,sensor_names).suptitle('Left', fontsize=16)
plot_head_signals(mean_class_3,sensor_names).suptitle('Rest', fontsize=16)
plot_head_signals(mean_class_4,sensor_names).suptitle('Feet', fontsize=16)


# ## Class Means, with rest class subtracted 

# In[9]:

from motor_imagery_learning.analysis.plot_util import plot_head_signals
plot_head_signals(mean_class_1 - mean_class_3,sensor_names ).suptitle('Right', fontsize=16)
plot_head_signals(mean_class_2 - mean_class_3,sensor_names).suptitle('Left', fontsize=16)
plot_head_signals(mean_class_3 - mean_class_3,sensor_names).suptitle('Rest', fontsize=16)
plot_head_signals(mean_class_4 - mean_class_3,sensor_names).suptitle('Feet', fontsize=16)


# Question: 
# 
# How to interpret this? Especially Feet
# 

# ## Filters first layer

# In[10]:

from motor_imagery_learning.analysis.plot_util import plot_head_signals_tight
raw_model = raw_train_state.model
_ = [plot_head_signals_tight(raw_model.get_weights_topo().transpose(0,3,1,2)[i], sensor_names) for i in range(4)]


# ## Show Single Trial class 3 and output 

# In[11]:

train_preds = reshape_and_get_predictions(raw_train_state.model, raw_datasets['train'].get_topological_view())
train_trials = raw_datasets['train'].get_topological_view()
test_preds = reshape_and_get_predictions(raw_train_state.model, raw_datasets['test'].get_topological_view())
test_trials = raw_datasets['test'].get_topological_view()


# In[12]:

correct_test = np.argmax(test_preds, axis=1) == np.argmax(raw_datasets['test'].y, axis=1)


# In[13]:

correct_trials_3_inds = np.nonzero(np.logical_and(correct_test, np.argmax(raw_datasets['test'].y, axis=1) == 2))[0]


# In[14]:

correct_trials_for_class_3 = test_trials[correct_trials_3_inds]
correct_trial_for_class_3 = test_trials[correct_trials_3_inds[0]]


# In[15]:

raw_datasets['test'].y[correct_trials_3_inds[0]]


# In[16]:

test_preds[correct_trials_3_inds[0]][2]


# In[17]:

_ = plot_sensor_signals(correct_trial_for_class_3, sensor_names)


# ## Outputs for that trial without pooling

# In[18]:

from copy import deepcopy
first_layer_no_pool = deepcopy(raw_model.layers[0])
# turn off pooling
first_layer_no_pool.pool_type = None

inputs = T.ftensor4()
results = first_layer_no_pool.fprop(inputs)

fprop_first_layer_no_pool = theano.function([inputs], results, allow_input_downcast=True)


# In[19]:

out_signals = fprop_first_layer_no_pool([correct_trial_for_class_3])

_ = plot_sensor_signals(out_signals[0], map(str, range(50)))


# ## Mean outputs for class 3 after pooling 

# In[20]:

first_layer = deepcopy(raw_model.layers[0])
inputs = T.ftensor4()
results = first_layer.fprop(inputs)

fprop_first_layer = theano.function([inputs], results, allow_input_downcast=True)


# In[21]:

out_class_3 = fprop_first_layer(correct_trials_for_class_3)


# In[22]:

_ = plot_sensor_signals(np.mean(out_class_3, axis=0), map(str, range(50)))


# In[23]:

# 29,46,49 for possibly frequency things
#18, 47, 48 for non-frequency things


# ### Filters more highly activated towards end of trial 

# In[24]:

for filt_i in [29,46,49]:
    plot_head_signals_tight(raw_model.get_weights_topo().transpose(0,3,1,2)[filt_i], sensor_names).suptitle(
        str(filt_i), fontsize= 14) 


# ### Filters more active at start of trial 

# In[25]:

for filt_i in [18, 39]:
    plot_head_signals_tight(raw_model.get_weights_topo().transpose(0,3,1,2)[filt_i], sensor_names).suptitle(
        str(filt_i), fontsize= 14) 


# ### Investigate for class 1 

# In[30]:

trials_class_1 = get_trials_for_class(raw_datasets['test'], 0)


# In[31]:

out_class_1 = fprop_first_layer(trials_class_1)


# In[32]:

# kaputt with mean, some values explode: investigate...
_ = plot_sensor_signals(np.mean(out_class_1, axis=0), map(str, range(50)))


# In[ ]:

# 18,23 have increases ? 46 stays same/small decrease?
#47 decreases, also 46

