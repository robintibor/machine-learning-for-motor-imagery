
# coding: utf-8

# # Meeting overview 

# * Update on Current State: Explain architectures, show performances
#  * Decide how to proceed evaluating architectures? As now? Or try also other datasets? 
#  * Try improving baseline CSP further?
# * Hyperparameter discussion 
# * Show some visualizations for bandpower and raw data net
#   * See if there is anything interesting in these yet
#   * Decide what questions visualizations could answer, what insights they could show and how to do that
#   * Discuss raw data (problems?)
# * Possibly discuss transfer learning (missing sensors problem)

# ## Result Overview

# * 10-fold blockwise cross validation results
# * Standard deviations, max, min always refer to datasets, e.g. max is the best dataset result (already averaged over 10 folds)

# |Architecture|time|std|test|std|train|std|maxtest|mintest|
# |-|-|-|-|-|-|-|-|-|-|-|
# |Common Spatial Patterns|0:19:55|0:02:45|87.16%|11.51%|99.64%|0.99%|99.37%|51.00%|
# |CSP as a net|~9:00:00|~2:00:00|~90%|~9%|~97%|~4%|98.61%|65.71%|
# |Convnet Bandpower|0:37:36|0:06:02|81.39%|10.11%|99.99%|0.03%|96.31%|55.56%|
# |Convnet Raw Data|1:20:02|0:22:18|83.30%|7.90%|98.60%|1.79%|92.67%|65.87%|
# 
# * Common Spatial Patterns possibly performs better when using more frequencies above 50 Hz.
# * CSP as a net not yet finished running the training with new ival (500-4000ms)...

# ## Explanations Architectures

# ## Common Spatial Patterns 

# <img src="https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/meeting-architectures/csp_explanation.svg?bla=blub6" style="float:left; height:450px"></img>

# ## CSP as a net 

# 
# <img src="https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/meeting-architectures/csp_as_a_net_explanation.svg?bla=blub4" style="float:left; height:450px"></img>
# 

# ## Convolutional Net Bandpower 

# <img src="https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/meeting-architectures/bandpower_explanation.svg?bla=blub15" style="float:left; height:450px"></img>

# Question: Putting one purely spatial layer with 100 filters infront reduces performance >5%, why?

# ## Convolutional Net Raw Data 

# <img src="https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/meeting-architectures/raw_data_explanation.svg?bla=blub15" style="float:left; height:400px"></img>

# Question:
# 
# Why did we make second layer like that actually?
# 
# Why did 1x1 kernel in first layer (so only spatial filters + pooling) also work(>70%)? Does that make sense?

# ## Confusion Matrices 

# <img src="https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/meeting-architectures/csp_confusion_matrix.svg?bla=blub2" style="float:left"></img>

# In[31]:

plot_confusion_matrix_for_averaged_result('data/gpu-models/bandpower-translated-fixed/cross-validation/early-stop/', 0)
pyplot.title('Convolutional Bandpower', fontsize=20, y=1.05)


# In[29]:

plot_confusion_matrix_for_averaged_result('data/gpu-models/raw-data-demeaned/cross-validation/early-stop/', 0)
pyplot.title('Convolutional Net Raw Data', fontsize=20, y=1.05)


# In[30]:

plot_confusion_matrix_for_averaged_result('data/gpu-models/csp-remake-from-500-ms/cross-validation/early-stop/', 0)
pyplot.title('CSP as a net (unfinished run)', fontsize=20, y=1.05)


# In[24]:

import os
os.sys.path.insert(0,'/lokal/schirrmr/motor-imagery/code/') 
os.sys.path.insert(0,'/lokal/schirrmr/motor-imagery/code/motor_imagery_learning/') 
get_ipython().magic(u'cd /lokal/schirrmr/motor-imagery/code/motor_imagery_learning/')
#%cd /home/robintibor/farm04/motor-imagery/code/motor_imagery_learning
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7
from pylearn2.utils import serial
import numpy as np
import h5py
from pylearn2.config import yaml_parse
from analysis.util import reshape_and_get_predictions, load_preprocessed_datasets, check_prediction_correctness
from motor_imagery_learning.analysis.plot_util import plot_confusion_matrix_for_averaged_result
from analysis.sensor_positions import get_C_sensors_sorted
import itertools
from copy import deepcopy
from motor_eeg_dataset import RawMotorEEGDataset

