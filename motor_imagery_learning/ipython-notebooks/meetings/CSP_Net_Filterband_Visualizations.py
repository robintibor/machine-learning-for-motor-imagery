
# coding: utf-8

# In[4]:

import os

import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# make sure to use cpu for printing results...
os.environ['THEANO_FLAGS'] = "floatX=float32,device=cpu,nvcc.fastmath=True"
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7
from motor_imagery_learning.analysis.print_results import ResultPrinter
from motor_imagery_learning.csp.print_csp_results import CSPResultPrinter
from pylearn2.utils import serial
import numpy as np


# # Zipfile mit visualisierungen
# 
# https://dl.dropboxusercontent.com/u/34637013/work/machine-learning-for-motor-imagery/filterbands_csp_net.zip

# In[70]:

get_ipython().run_cell_magic(u'javascript', u'', u'function makeSVGDownloadable() {\n    $(\'body\').on(\'click\', \'svg\', function(){\n        var svg = this.outerHTML;\n        var b64 = btoa(svg); // or use btoa if supported\n        var linkToSVG = $("<a href-lang=\'image/svg+xml\' href=\'data:image/svg+xml;base64,\\n" + b64 +\n                          "\'title=\'file.svg\' target=\'blank\'>Download</a>");\n        $(this).after(linkToSVG);});\n}\nmakeSVGDownloadable()')


# In[140]:

from motor_imagery_learning.analysis.plot.filterbands import show_freq_importances
from motor_imagery_learning.analysis.filterbands import (
    sum_abs_filterband_weights, compute_center_freqs, compute_sum_abs_weights_for_csp_model)


# In[97]:

import seaborn


# In[23]:

from glob import glob
folder = 'data/models/debug/csp-net-online-standardize-keep-lrule/early-stop/'
models = []
for filenr in range(2,37,2):
    filename = folder + str(filenr) + ".pkl"
    model = serial.load(filename)
    models.append(model)


# ## Visualisungen
# 
# * Zeigt Summe aller absoluten Gewichte (über alle Spatial Filter) aller 4 Klassifikationsneuronen für das jeweilige Filterband => evlt interpretiebar als "Wichtigkeit" des Filterbands für die Klassifikation

# In[110]:

#mkdir filterband-weights-visualizations/
folder_name = 'filterband-weights-visualizations'


# In[101]:

seaborn.set(font_scale=0.8)


# In[141]:

all_sum_abs_weights = []
for model in models:
    filename = model.info_parameters['dataset_filename']
    nice_filename = filename.split("/")[-1][:4]
    center_freqs = compute_center_freqs(model.model) # shd be same always, calculate just for check..
    sum_abs_weights = sum_abs_filterband_weights(model.model, center_freqs)
    all_sum_abs_weights.append(sum_abs_weights)
    pyplot.figure()
    show_freq_importances(center_freqs, sum_abs_weights)
    pyplot.title(nice_filename, fontsize=18)
    pyplot.savefig(folder_name + "/" + nice_filename + str('.pdf'))


# In[103]:

mean_fb = np.mean(all_sum_abs_weights, axis=0)
std_fb = np.std(all_sum_abs_weights, axis=0)
median_fb = np.median(all_sum_abs_weights, axis=0)
mad_fb = mad(all_sum_abs_weights, axis=0) 


# In[113]:

pyplot.plot(center_freqs, mean_fb, marker='o', linestyle='None')
_ = pyplot.xticks(np.array(center_freqs[:16:2].tolist() + center_freqs[16:].tolist()).astype(np.int))
eb = pyplot.errorbar(center_freqs, mean_fb, yerr=std_fb, fmt=None, ecolor='k')
pyplot.xlim(-0.5,145)
pyplot.savefig(folder_name + "/" + 'mean_std' + str('.pdf'))


# In[112]:

pyplot.plot(center_freqs, median_fb, marker='o', linestyle='None')
_ = pyplot.xticks(np.array(center_freqs[:16:2].tolist() + center_freqs[16:].tolist()).astype(np.int))
eb = pyplot.errorbar(center_freqs, median_fb, yerr=mad_fb, fmt=None, ecolor='k')
pyplot.xlim(-0.5,145)
pyplot.savefig(folder_name + "/" + 'median_mad' + str('.pdf'))


# In[142]:

freq_strs = ["{:.1f} - {:.1f}".format(f[0], f[1]) for f in filterbank]
freq_line = "\t".join(freq_strs)
median_strs = ["{:.2f} (+-{:.2f})".format(median_fb[i], mad_fb[i]) for i, val in enumerate(mad_fb)]
median_line = "\t".join(median_strs)
mean_strs = ["{:.2f} (+-{:.2f})".format(mean_fb[i], std_fb[i]) for i, val in enumerate(mean_fb)]
mean_line = "\t".join(mean_strs)


# In[152]:

csv_file_content = ("Filterband" + "\t" + freq_line + "\n" + "Median (Mad)\t" + 
    median_line + "\n" + "Mean (Std)\t" + mean_line + "\n")
with  open(folder_name + "/" + "median_mean.csv", "w") as csv_file:
    csv_file.write(csv_file_content)


# In[117]:


filterbank = generate_filterbank(min_freq=0, max_freq=142, last_low_freq=30, low_width=2, high_width=6)


# ## CSP Visualisierungen

# In[214]:

csp_folder = 'data/models/csp/full/'
from glob import glob
filename_abs_weights = []
for filenr in range(19, 33)+ range(47,51):
    filename = csp_folder + str(filenr) + ".pkl"
    csp_model = serial.load(filename)
    mean_abs_weights = compute_sum_abs_weights_for_csp_model(csp_model)
    filename_abs_weights.append((csp_model.filename, mean_abs_weights))
all_csp_abs_weights = np.array(zip(*filename_abs_weights)[1])


# In[215]:

for filename_weights in filename_abs_weights:
    filename = filename_weights[0]
    nice_filename = filename.split("/")[-1][:4]
    sum_abs_weights = filename_weights[1]
    pyplot.figure()
    show_freq_importances(center_freqs, sum_abs_weights)
    pyplot.title(nice_filename, fontsize=18)
    pyplot.savefig(folder_name + "/csp/" + nice_filename + str('.pdf'))


# In[219]:

mean_fb = np.mean(all_csp_abs_weights, axis=0)
std_fb = np.std(all_csp_abs_weights, axis=0)
median_fb = np.median(all_csp_abs_weights, axis=0)
mad_fb = mad(all_csp_abs_weights, axis=0)


# In[220]:

pyplot.plot(center_freqs, mean_fb, marker='o', linestyle='None')
_ = pyplot.xticks(np.array(center_freqs[:16:2].tolist() + center_freqs[16:].tolist()).astype(np.int))
eb = pyplot.errorbar(center_freqs, mean_fb, yerr=std_fb, fmt=None, ecolor='k')
pyplot.xlim(-0.5,145)
pyplot.savefig(folder_name + "/csp/" + 'mean_std' + str('.pdf'))


# In[221]:

pyplot.plot(center_freqs, median_fb, marker='o', linestyle='None')
_ = pyplot.xticks(np.array(center_freqs[:16:2].tolist() + center_freqs[16:].tolist()).astype(np.int))
eb = pyplot.errorbar(center_freqs, median_fb, yerr=mad_fb, fmt=None, ecolor='k')
pyplot.xlim(-0.5,145)
pyplot.savefig(folder_name + "/csp/" + 'median_mad' + str('.pdf'))


# In[222]:

filterbank = generate_filterbank(min_freq=0, max_freq=142, last_low_freq=30, low_width=2, high_width=6)
freq_strs = ["{:.1f} - {:.1f}".format(f[0], f[1]) for f in filterbank]
freq_line = "\t".join(freq_strs)
median_strs = ["{:.2f} (+-{:.2f})".format(median_fb[i], mad_fb[i]) for i, val in enumerate(mad_fb)]
median_line = "\t".join(median_strs)
mean_strs = ["{:.2f} (+-{:.2f})".format(mean_fb[i], std_fb[i]) for i, val in enumerate(mean_fb)]
mean_line = "\t".join(mean_strs)
csv_file_content = ("Filterband" + "\t" + freq_line + "\n" + "Median (Mad)\t" + 
    median_line + "\n" + "Mean (Std)\t" + mean_line + "\n")
with  open(folder_name + "/csp/" + "median_mean.csv", "w") as csv_file:
    csv_file.write(csv_file_content)


# In[2]:

from motor_imagery_learning.analysis.util import mad

