
# coding: utf-8

# In[1]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# switch to cpu
#os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
from motor_imagery_learning.mypylearn2.conv_squared import ConvSquared
from motor_imagery_learning.analysis.print_results import ResultPrinter


# In[2]:

os.environ['THEANO_FLAGS']


# ## Do not predict all at once fix prediction crash 

# In[4]:

from motor_imagery_learning.train_scripts.train_with_params import create_training_object_from_file
train_obj = create_training_object_from_file('data/models/debug/csp-net-crash/test-models/early-stop/1.yaml')

train_obj 


# ## Performance Test when changing to dot product in layer 

# In[2]:

from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from motor_imagery_learning.mypylearn2.conv_squared import ConvSquaredDot


# In[3]:

input_shape = (11,3)
num_chans = 5
real_input = np.random.randn(100000, num_chans, *input_shape).astype('float32')
b01c_input = real_input.transpose(0,2,3,1)
input_space = Conv2DSpace(shape=input_shape, num_channels=num_chans, axes=('b', 'c', 0, 1))


# In[4]:

conv_sq_layer = ConvSquared(layer_name='conv_sq', output_channels=20, irange=0.5, kernel_shape=[1,1],
                           pool_shape=[1,1], pool_stride=[1,1], pool_type='sumlog', 
                            max_kernel_norm=2., tied_b=True)

conv_sq_layer_fast = ConvSquaredDot(layer_name='conv_sq', output_channels=20, irange=0.5, kernel_shape=[1,1],
                           pool_shape=[1,1], pool_stride=[1,1], pool_type='sumlog', 
                            max_kernel_norm=2., tied_b=True)


# In[5]:

for layer in [conv_sq_layer, conv_sq_layer_fast]:
    layer.mlp = lambda: None
    layer.mlp.rng = RandomState(seed=np.uint32(hash("tobithepuma")))
    layer.mlp.batch_size = 5
    layer.set_input_space(input_space)


# In[70]:

#from pylearn2.utils import sharedX
#b = conv_sq_layer.transformer._filters
#b = np.array(b.eval())[:,:,0,0].transpose(1,0)
#conv_sq_layer_fast.transformer._filters = sharedX(np.array(b))


# In[23]:

a = np.array(conv_sq_layer_fast.transformer.get_params()[0].eval())
b = np.array(conv_sq_layer.transformer.get_params()[0].eval())#[:,:,0,0]
#a = a.transpose(1,0)
assert np.array_equal(a,b)


# For N dimensions it is a sum product over the last axis of `a` and
# the second-to-last of `b`::
# 
#     dot(a, b)[i,j,k,m] = sum(a[i,j,:] * b[k,:,m])

# In[6]:

inputs = T.ftensor4()
result = conv_sq_layer.fprop(inputs)
fprop_func = theano.function([inputs], result)


# In[7]:

inputs = T.ftensor4()
result = conv_sq_layer_fast.fprop(inputs)
fprop_func_fast = theano.function([inputs], result)


# In[8]:

get_ipython().run_cell_magic(u'time', u'', u'old_result = fprop_func(real_input)')


# In[9]:

get_ipython().run_cell_magic(u'time', u'', u'new_result = fprop_func_fast(real_input)')


# In[10]:

np.array_equal(new_result, old_result)


# In[10]:

np.array_equal(new_result.shape, old_result.shape)


# In[ ]:

# now for real set
# before: 450 MB
# after dataset creation: 601 MB
# after creation of inner trainer: 605 MB
# after main loop: 645 MB
# after adapt; 764 MB?!


# In[33]:

del trainer
del dataset
del conv_sq_layer
del softmax_layer
del model
del sgd_algo


# In[35]:

del inner_trainer


# In[38]:

del sizes


# In[4]:

from motor_imagery_learning.util import memory_usage
import gc
gc.collect()
memory_usage()


# ## Real csp net now :) 

# In[8]:

import numpy as np
len(np.linspace(500, 4000, float(4000 - 500) / 1000 * 300, endpoint=False))


# In[1]:

3500  * 300 / 1000.0


# In[3]:

from pylearn2.format.target_format import OneHotFormatter
y = OneHotFormatter(4).format(np.random.random_integers(0,3,size=766))
topo_view = np.empty((766,128,1200,2), dtype=np.float32)
for i in xrange(len(topo_view)):
    topo_view[i] =  np.float32(np.random.rand( *(topo_view.shape[1:])))

dataset = DenseDesignMatrix(topo_view = topo_view, y=y, axes=('b', 'c', 0, 1))
dataset.reload = True
del topo_view


# In[2]:

from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanDataset, BBCIPylearnCleanFilterbankDataset

dataset = BBCIPylearnCleanFilterbankDataset('data/BBCI-without-last-runs/LaKaMoSc1S001R01_ds10_1-9.BBCI.mat',
                                 load_sensor_names=['C2', 'C4', 'CP1', 'C3', 'C5'], 
                                           min_freq=2,max_freq=14,last_low_freq=20,low_width=2,high_width=4)
dataset.load()


# In[5]:

memory_usage()


# In[8]:

conv_sq_layer = ConvSquared(layer_name='conv_sq', output_channels=80, irange=0.5, kernel_shape=[1,1],
                           pool_shape=[1,1], pool_stride=[1,1], pool_type='sumlog', 
                            max_kernel_norm=2., tied_b=True)

softmax_layer = Softmax(n_classes=4, layer_name='y', irange=0.0001, max_col_norm=2.)
model = MLP([conv_sq_layer, softmax_layer])
sgd_algo = SimilarBatchSizeSGD(batch_size=50,cost=Default(),termination_criterion=EpochCounter(max_epochs=2),
               learning_rate=0.1, seed=[2012, 10, 16])
trainer = TrainEarlyStop(dataset, model, algorithm=sgd_algo, preprocessor=OnlineStandardize())


# In[6]:

memory_usage()


# In[9]:

get_ipython().run_cell_magic(u'capture', u'', u'#%%prun\ninner_trainer = trainer._construct_train_object()')


# In[15]:

len(trainer.dataset.clean_trials)


# In[32]:

inner_trainer.dataset is trainer.algorithm.monitoring_dataset['train']


# In[29]:

sum(map(lambda d: d.X.shape[0], trainer.algorithm.monitoring_dataset.values()))


# In[43]:

for attr in trainer.model.monitor.__dict__:
    print attr, getsize(trainer.model.monitor.__dict__[attr]) / (1024.0 ** 2)


# In[45]:

trainer.model.monitor.accum


# In[47]:

getsize(trainer) / (1024.0 ** 2)


# In[44]:

getsize(trainer.model.monitor) / (1024.0 ** 2)


# In[46]:

memory_usage()


# In[22]:

get_ipython().run_cell_magic(u'capture', u'', u'inner_trainer.main_loop()')


# In[33]:

memory_usage()


# In[10]:

topo_view = np.empty((766,128,1200,35), dtype=np.float32)
for i in xrange(len(topo_view)):
    topo_view[i] =  np.float16(np.random.rand( *(topo_view.shape[1:])))

trainer.dataset = DenseDesignMatrix(topo_view = topo_view, y=y, axes=('b', 'c', 0, 1))
del topo_view


# In[27]:

#%%capture
trainer._adapt_training_after_early_stop(inner_trainer)


# In[13]:

memory_usage()


# In[25]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer._run_training_until_validation_nll_as_training(inner_trainer)')


# In[17]:

import sys
import numbers
import collections

def getsize(obj):
    # recursive function to dig out sizes of member objects:               
    def inner(obj, _seen_ids = set()):
        obj_id = id(obj)
        if obj_id in _seen_ids:
            return 0
        _seen_ids.add(obj_id)
        size = sys.getsizeof(obj)
        assert len(_seen_ids) < 1000000
        if isinstance(obj, (basestring, numbers.Number, xrange)):
            pass # bypass remaining control flow and return                
        elif isinstance(obj, (tuple, list, set, frozenset)):
            size += sum(inner(i) for i in obj)
        elif isinstance(obj, collections.Mapping) or hasattr(obj, 'iteritems'):
            size += sum(inner(k) + inner(v) for k, v in obj.iteritems())
        elif isinstance(obj, np.ndarray):
            size += obj.nbytes
        else:
            attr = getattr(obj, '__dict__', None)
            if attr is not None:
                size += inner(attr)
        return size
    return inner(obj)

