
# coding: utf-8

# In[1]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
#os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import numpy as np
from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import ConvElemwise
from theano.sandbox.cuda.dnn import dnn_conv
from theano.sandbox.cuda.basic_ops import gpu_alloc_empty
from motor_imagery_learning.analysis.print_results import ResultPrinter
from pylearn2.utils import serial

import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7

from motor_imagery_learning.analysis.plot_util import plot_misclasses_for_model
import seaborn


# In[4]:

ResultPrinter('data/models/two-class/raw-square/early-stop/').print_results()


# In[3]:

ResultPrinter('data/models/two-class/raw-square-smooth/early-stop/').print_results()


# In[5]:

ResultPrinter('data/models/two-class/csp-net/early-stop/').print_results()


# In[43]:

from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanDataset, BBCISetCleaner
from motor_imagery_learning.mywyrm.processing import highpass_cnt, resample_cnt
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.preprocessing import RestrictToTwoClasses

anwe_set = BBCIPylearnCleanDataset(filenames="data/BBCI-without-last-runs/AnWeMoSc1S001R01_ds10_1-12.BBCI.mat", 
                                   cleaner=BBCISetCleaner(),
                                   cnt_preprocessors= [
                                   (resample_cnt, dict(newfs=150)),
                                   (highpass_cnt, dict(low_cut_off_hz=0.5))
                                    ],
                                   sensor_names=get_C_sensors_sorted(),
                                   unsupervised_preprocessor= RestrictToTwoClasses(classes=[0,2]),
        )

anwe_set.load()


# 
# ### Fake dataset

# In[682]:

from motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer
from motor_imagery_learning.analysis.data_generation import (create_sine_signal,
    SingleChanDataset, pipeline, create_shifted_sines,put_noise_on_set, convolve_with_weight,
    max_pool_topo_view, log_sum_pool)

input_shape=(300,1,600,1)
start_topo_view, y = create_shifted_sines(input_shape, RandomState(np.uint64(hash("pooled_pipeline"))),
                                         sampling_freq=150.0)

topo_view_noised, y = pipeline(start_topo_view, y, 
    lambda x,y: put_noise_on_set(x,weight_old_data=0.05))


# In[877]:

from motor_imagery_learning.mypylearn2.conv_squared import ConvSquaredSwitchAxes,ConvSquared
from motor_imagery_learning.mypylearn2.conv_switch_axes import ConvSwitchAxes
from pylearn2.models.mlp import RectifierConvNonlinearity, ConvRectifiedLinear
from motor_imagery_learning.analysis.util import create_trainer
from pylearn2.datasets.preprocessing import Pipeline
from motor_imagery_learning.mypylearn2.preprocessing import OnlineChannelwiseStandardize
oldlayers = [ConvSwitchAxes(layer_name="bp", 
                       nonlinearity= RectifierConvNonlinearity(),
                       output_channels=15,
                       irange=0.05,
                       kernel_shape=[50,1],
                       kernel_stride=[1,1],
                       pool_type=None,
                       switch_axes=(['b', 'c', 0, 1], ['b', 1, 0, 'c']),
                       max_kernel_norm=2.0
            ),
          ConvSquaredSwitchAxes(layer_name="spatsquare",
                      output_channels=20,
                     irange=0.05,
                     kernel_shape=[1,1],
                     kernel_stride=[1,1],
                     pool_type="max",
                     pool_shape=[50, 1],
                     pool_stride=[50, 1],
                    switch_axes=(['b', 'c', 0, 1], ['b', 1, 0, 'c']),
                     max_kernel_norm=2.0),
          ConvRectifiedLinear(layer_name="bp_conv",
                      output_channels=10,
                     irange=0.05,
                     kernel_shape=[1,15],
                     kernel_stride=[1,1],
                     pool_type=None,
                     pool_shape=[None, None],
                     pool_stride=[None, None],
                     max_kernel_norm=2.0,
                             tied_b=True),
          Softmax(n_classes=2, irange=0.05, layer_name="y")
]
"""
ConvSwitchAxes(layer_name="bp", 
                       nonlinearity= RectifierConvNonlinearity(),
                       output_channels=1,
                       irange=0.05,
                       kernel_shape=[50,1],
                       kernel_stride=[1,1],
                       pool_type=None,
                       switch_axes=(['b', 'c', 0, 1], ['b', 1, 0, 'c']),
                       max_kernel_norm=2.0
            ),
"""
layers = [
          ConvSquared(layer_name="bpconv_square",
                     output_channels=10,
                     irange=0.05,
                     kernel_shape=[50,1],
                     kernel_stride=[1,1],
                     pool_type="sumlogall",
                     pool_shape=[50, 1],
                     pool_stride=[50, 1],
                     max_kernel_norm=2.0),
          Softmax(n_classes=2, irange=0.05, layer_name="y")
]

trainer = create_trainer(topo_view=topo_view_noised, y=y, layers=layers,
               cost=None, max_epochs=100, preprocessor=OnlineChannelwiseStandardize())


# In[878]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer._create_early_stop_trainer()\ntrainer.early_stop_trainer.setup()')


# In[271]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer.run_until_earlystop()')


# In[267]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer.run_until_second_stop()')


# ## Get datasets

# In[879]:

train_set = trainer.algorithm.monitoring_dataset['train']
train_topo = train_set.get_topological_view().astype(np.float32)
train_y = train_set.y.astype(np.float32)
test_set = trainer.algorithm.monitoring_dataset['test']
test_topo = test_set.get_topological_view().astype(np.float32)
test_y = test_set.y.astype(np.float32)


# ### Create cost, gradient and activation functions

# In[880]:

topo_in = T.ftensor4()
y_in = T.fmatrix()
cost = trainer.algorithm.cost.expr(trainer.model, (topo_in, y_in))

grads, updates = trainer.algorithm.cost.get_gradients(trainer.model, (topo_in, y_in))

cost_func = theano.function([topo_in, y_in], cost)

outputs = []


for grad in grads:
    outputs.append(grads[grad])

cost_grad_func = theano.function([topo_in, y_in], outputs)
activations = trainer.model.fprop(topo_in, return_all=True)
fprop_func = theano.function([topo_in], activations)
grad_keys = grads.keys()
grad_param_and_vals = lambda topo, y: zip(grad_keys, cost_grad_func(topo, y))


# In[881]:

#(bp_w, bp_b), (bp_conv_w, bp_conv_b), (softmax_b, softmax_w) = [l.get_params() for l in trainer.model.layers]
(bp_w, bp_b), (softmax_b, softmax_w) = [l.get_params() for l in trainer.model.layers]


# In[774]:

def plot_w_and_grad(weights, grads):
    w = np.squeeze(weights)
    g = np.squeeze(grads)
    print g.shape
    print w.shape
    assert w.ndim == 2
    assert g.shape == w.shape
    w_and_g = np.array([w,g]).transpose(1,2,0)
    _ = plot_sensor_signals(w_and_g, yticks="onlymax")


# In[882]:

params_during_training = []
grads_during_training = []


# In[883]:

get_ipython().run_cell_magic(u'capture', u'', u"lrate = 0.01\nbatch_rand = RandomState(np.uint64(hash('batch_inds')))\nfor i in xrange(10000):\n    start = 0\n    batch_size=50\n    batch_inds = batch_rand.choice(range(len(train_topo)), size=batch_size, replace=False)\n    grads = grad_param_and_vals(train_topo[batch_inds], train_y[batch_inds])\n    #bp_w_grad,bp_b_grad, bp_conv_w_grad, bp_conv_b_grad, softmax_b_grad, softmax_w_grad = [np.array(g[1]) for g in grads]\n    bp_w_grad,bp_b_grad, softmax_b_grad, softmax_w_grad = [np.array(g[1]) for g in grads]\n\n    \n    params_during_training.append(deepcopy(trainer.model.get_params()))\n    grads_during_training.append(deepcopy(grads))\n    bp_w.set_value(bp_w.get_value() - lrate * bp_w_grad)\n    bp_b.set_value(bp_b.get_value() - lrate * bp_b_grad)\n    #bp_conv_w.set_value(bp_conv_w.get_value() - lrate * bp_conv_w_grad)\n    #bp_conv_b.set_value(bp_conv_b.get_value() - lrate * bp_conv_b_grad)\n    softmax_w.set_value(softmax_w.get_value() - lrate * softmax_w_grad)\n    softmax_b.set_value(softmax_b.get_value() - lrate * softmax_b_grad)\n    #trainer.early_stop_trainer.run_callbacks_and_monitoring()")


# In[778]:

softmax_ws.shape


# In[845]:

softmax_ws = np.squeeze([W.get_value() for W in np.array(params_during_training)[:, 3]])
_ = plot_sensor_signals(softmax_ws[::1000].transpose(1,2,0))


# In[884]:

bp_ws  = np.squeeze([W.get_value() for W in np.array(params_during_training)[:, 0]])
bp_ws = np.squeeze(bp_ws)
if (bp_ws.ndim == 2):
    bp_ws = bp_ws[:,np.newaxis,:]
if (bp_ws.ndim == 1):
    bp_ws = bp_ws[:,np.newaxis,np.newaxis]


# In[863]:

pyplot.plot(bp_ws[-1,0])


# In[886]:

_ = plot_sensor_signals(bp_ws[::1000].transpose(1,2,0), figsize=(3,6), yticks="onlymax")


# In[861]:

trainer.early_stop_trainer.run_callbacks_and_monitoring()


# In[475]:

np.array(weight_during_training).shape


# In[434]:

plot_w_and_grad(bp_conv_w.get_value(), bpconv_w_grad)


# In[413]:

plot_w_and_grad(bp_w.get_value(), bp_w_grad)


# In[398]:

weight_and_grad = np.concatenate((bp_w.get_value(), bp_w_grad), axis=3)


# In[330]:

plot_sensor_signals(np.squeeze(bp_w_grad), yticks="onlymax")


# In[236]:

plot_misclasses_for_model(trainer.model)
trainer.model.monitor.channels['test_y_misclass'].val_record[-5:]


# In[237]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer.run_until_second_stop()')


# In[238]:

plot_misclasses_for_model(trainer.model)
trainer.model.monitor.channels['test_y_misclass'].val_record[-5:]


# ### Old with bp merged instead of channels 

# In[172]:

plot_misclasses_for_model(trainer.model)
trainer.model.monitor.channels['test_y_misclass'].val_record[-5:]


# In[ ]:

# 


# In[106]:

from motor_imagery_learning.analysis.plot_util import plot_sensor_signals

_ = plot_sensor_signals(np.squeeze(trainer.model.get_weights_topo()))


# In[891]:




# ## Experiments with bandpass filter coefficients

# In[903]:

#Lowpass filter
filters = []
for start in np.arange(0.01, 0.9, 0.1):
    a = firwin(N, cutoff = start, window = 'blackmanharris')
    #Highpass filter with spectral inversion
    b = - firwin(N, cutoff = start+0.1, window = 'blackmanharris'); 
    b[N/2] = b[N/2] + 1

    #Combine into a bandpass filter
    d = - (a+b); d[N/2] = d[N/2] + 1
    filters.append(d)


# In[921]:

numtaps = N
# Build up the coefficients.
alpha = 0.5 * (numtaps - 1)
m = np.arange(0, numtaps) - alpha
h = 0 
right = 0.31
left = 0.21
h += right * np.sinc(right * m)
h -= left * np.sinc(left * m)
win = scipy.signal.get_window('blackmanharris', numtaps, fftbins=False)
#h *= win
pyplot.plot(h)


# In[922]:

taps=h
figure(2)
clf()
w, h = freqz(taps, worN=8000)
plot((w/pi)*nyq_rate, absolute(h), linewidth=2)
xlabel('Frequency (Hz)')
ylabel('Gain')
title('Frequency Response')
ylim(-0.05, 1.05)
grid(True)


# In[908]:


pyplot.plot(firwin(N, cutoff = [0.01, 0.11], window = 'blackmanharris', pass_zero=False))
pyplot.plot(filters[0])


# In[902]:


pyplot.plot(np.array(filters[0:3]).T)


# In[887]:

import scipy.signal

from numpy import cos, sin, pi, absolute, arange
from scipy.signal import kaiserord, lfilter, firwin, freqz
from pylab import figure, clf, plot, xlabel, ylabel, xlim, ylim, title, grid, axes, show


#------------------------------------------------
# Create a signal for demonstration.
#------------------------------------------------

sample_rate = 100.0
nsamples = 400
t = arange(nsamples) / sample_rate
x = cos(2*pi*0.5*t) + 0.2*sin(2*pi*2.5*t+0.1) +         0.2*sin(2*pi*15.3*t) + 0.1*sin(2*pi*16.7*t + 0.1) +             0.1*sin(2*pi*23.45*t+.8)


#------------------------------------------------
# Create a FIR filter and apply it to x.
#------------------------------------------------

# The Nyquist rate of the signal.
nyq_rate = sample_rate / 2.0

# The desired width of the transition from pass to stop,
# relative to the Nyquist rate.  We'll design the filter
# with a 5 Hz transition width.
width = 5.0/nyq_rate

# The desired attenuation in the stop band, in dB.
ripple_db = 60.0

# Compute the order and Kaiser parameter for the FIR filter.
N, beta = kaiserord(ripple_db, width)

# The cutoff frequency of the filter.
cutoff_hz = 10.0

# Use firwin with a Kaiser window to create a lowpass FIR filter.
taps = firwin(N, cutoff_hz/nyq_rate, window=('kaiser', beta))
#http://mpastell.com/2010/01/18/fir-with-scipy/
#Lowpass filter
a = firwin(N, cutoff = 0.3, window = 'blackmanharris')
#Highpass filter with spectral inversion
b = - firwin(N, cutoff = 0.4, window = 'blackmanharris'); 
b[N/2] = b[N/2] + 1

#Combine into a bandpass filter
d = - (a+b); d[N/2] = d[N/2] + 1
def create_sine_signal(samples, freq, sampling_freq, shift=0):
    x = (np.arange(0,samples,1) * 2 * np.pi * freq / float(sampling_freq)) + shift
    return np.sin(x)
# fake taps
#taps = create_sine_signal(74, freq=9, sampling_freq=sample_rate) / 10.0
#taps=-taps
#bandpass taps
taps = d * 1
# fake smoothed taps
#taps = final_abs_taps[0] / 2.0

# Use lfilter to filter x with the FIR filter.
filtered_x = lfilter(taps, 1.0, x)

#------------------------------------------------
# Plot the FIR filter coefficients.
#------------------------------------------------

figure(1)
plot(taps, 'bo-', linewidth=2)
title('Filter Coefficients (%d taps)' % N)
grid(True)

#------------------------------------------------
# Plot the magnitude response of the filter.
#------------------------------------------------

figure(2)
clf()
w, h = freqz(taps, worN=8000)
plot((w/pi)*nyq_rate, absolute(h), linewidth=2)
xlabel('Frequency (Hz)')
ylabel('Gain')
title('Frequency Response')
ylim(-0.05, 1.05)
grid(True)

# Upper inset plot.
ax1 = axes([0.42, 0.6, .45, .25])
plot((w/pi)*nyq_rate, absolute(h), linewidth=2)
xlim(0,8.0)
ylim(0.9985, 1.001)
grid(True)

# Lower inset plot
ax2 = axes([0.42, 0.25, .45, .25])
plot((w/pi)*nyq_rate, absolute(h), linewidth=2)
xlim(12.0, 20.0)
ylim(0.0, 0.0025)
grid(True)

#------------------------------------------------
# Plot the original and filtered signals.
#------------------------------------------------

# The phase delay of the filtered signal.
delay = 0.5 * (N-1) / sample_rate

figure(3)
# Plot the original signal.
plot(t, x)
# Plot the filtered signal, shifted to compensate for the phase delay.
plot(t-delay, filtered_x, 'r-')
# Plot just the "good" part of the filtered signal.  The first N-1
# samples are "corrupted" by the initial conditions.
plot(t[N-1:]-delay, filtered_x[N-1:], 'g', linewidth=4)

xlabel('t')
grid(True)

show()

