
# coding: utf-8

# In[312]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
#os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import numpy as np
from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import ConvElemwise
from theano.sandbox.cuda.dnn import dnn_conv
from theano.sandbox.cuda.basic_ops import gpu_alloc_empty
from motor_imagery_learning.analysis.print_results import ResultPrinter
from pylearn2.utils import serial

from copy import deepcopy
from motor_imagery_learning.train_scripts.train_experiments import ExperimentsRunner
from motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer
from motor_imagery_learning.analysis.data_generation import (create_sine_signal,
    pipeline, create_shifted_sines,put_noise, convolve_with_weight,
    max_pool_topo_view, log_sum_pool,generate_single_sensor_data, 
    add_timecourse, spread_to_sensors, SpreadedSinesDataset)
from motor_imagery_learning.analysis.util import create_trainer
from motor_imagery_learning.mypylearn2.conv_squared import ConvSquared, ConvSquaredFullHeight
from motor_imagery_learning.analysis.input_reconstruction import (
    create_rand_input, gradient_descent_input, create_input_grad_for_wanted_class_func,
    create_input_grad_with_smooth_penalty_for_wanted_class_func, reconstruct_inputs_for_class,
    power_smoothness_cost_moving_avg_scaled)
from pylearn2.config import yaml_parse
from motor_imagery_learning.analysis.util import bps_and_freqs
from motor_imagery_learning.analysis.deconv import deconv_model
from motor_imagery_learning.analysis.plot.raw_signals import plot_sensor_signals_two_classes

import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (8.0, 1.0)
matplotlib.rcParams['font.size'] = 7

from motor_imagery_learning.analysis.plot_util import plot_misclasses_for_model, plot_nlls_for_model, plot_sensor_signals
import scipy
import seaborn
seaborn.set_style('darkgrid')
from motor_imagery_learning.analysis.util import create_activations_func
from motor_imagery_learning.train_scripts.train_with_params import create_training_object_from_file
from motor_imagery_learning.analysis.util import create_prediction_func
from motor_imagery_learning.analysis.plot_util import plot_head_signals_tight_multiple_signals
from motor_imagery_learning.analysis.plot_util import plot_head_signals, plot_head_signals_tight
from motor_imagery_learning.analysis.plot_util import plot_head_signals_tight_two_signals


# In[207]:

from motor_imagery_learning.mypylearn2.pool import PoolLayer, log_sum
from scipy.interpolate import interp1d

def get_reconstructions_artificial_data():
    input_shape=(600,1,600,1)
    start_topo_view, y = create_shifted_sines(input_shape, RandomState(np.uint64(hash("pooled_pipeline"))), 
                                              sampling_freq=150.0)
    topo_view_noised, y = pipeline(start_topo_view, y,
                             lambda x,y: put_noise(x, weight_old_data=0.05))
    # convolve + square + maxpool
    layers = [ConvSquared(output_channels=1, kernel_shape=[150,1], pool_shape=[-1,-1], pool_stride=[-1,-1],
                         pool_type=None,layer_name="conv_squared", irange=0.05, max_kernel_norm=2.0, tied_b=True),
                  PoolLayer(pool_func=log_sum, layer_name="pool_squared", pool_shape=[50,1], pool_stride=[50,1]),
              
              Softmax(n_classes=2, layer_name='y', irange=0.001, max_col_norm=0.5)]
    trainer = create_trainer(topo_view_noised, y,max_epochs=100, layers=layers, cost=None)
    
    trainer.run_until_earlystop()
    in_grad_func = create_input_grad_for_wanted_class_func(trainer.model)
    batches = 3
    start_in = create_rand_input(trainer.model, batches)
    ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000)
    inputs_class_1 = gradient_descent_input(start_in, in_grad_func, [1,0], **ascent_args)
    inputs_class_2 = gradient_descent_input(start_in, in_grad_func, [0,1], **ascent_args)
    return inputs_class_1, inputs_class_2

def get_reconstructions_kaus_data():
    kaus_topo, kaus_y = generate_single_sensor_data(newfs=150)
    # convolve + square + maxpool
    layers = [ConvSquared(output_channels=1, kernel_shape=[150,1], pool_shape=[-1,-1], pool_stride=[-1,-1],
                         pool_type=None,layer_name="conv_squared", irange=0.05, max_kernel_norm=2.0, tied_b=True),
              PoolLayer(pool_func=log_sum, layer_name="pool_squared", pool_shape=[50,1], pool_stride=[50,1]),
              Softmax(n_classes=2, layer_name='y', irange=0.001, max_col_norm=0.5)]
    trainer = create_trainer(kaus_topo, kaus_y,max_epochs=100, layers=layers, cost=None)
    
    trainer.run_until_earlystop()
    in_grad_func = create_input_grad_for_wanted_class_func(trainer.model)
    batches = 3
    start_in = create_rand_input(trainer.model, batches)
    ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000)
    inputs_class_1 = gradient_descent_input(start_in, in_grad_func, [1,0], **ascent_args)
    inputs_class_2 = gradient_descent_input(start_in, in_grad_func, [0,1], **ascent_args)
    return inputs_class_1, inputs_class_2
    
def get_data_with_timecourse():
    input_shape=(600,1,600,1)
    start_topo_view, y = create_shifted_sines(input_shape, RandomState(np.uint64(hash("pooled_pipeline"))), 
                                              sampling_freq=150.0)
    topo_view_noised, y = pipeline(start_topo_view, y,
                             lambda x,y: put_noise(x, weight_old_data=0.05))
    # convolve + square + maxpool
    layers = [ConvSquared(output_channels=1, kernel_shape=[150,1], pool_shape=[-1,-1], pool_stride=[-1,-1],
                         pool_type=None,layer_name="conv_squared", irange=0.05, max_kernel_norm=2.0, tied_b=True),
                  PoolLayer(pool_func=log_sum, layer_name="pool_squared", pool_shape=[50,1], pool_stride=[50,1]),
              
              Softmax(n_classes=2, layer_name='y', irange=0.001, max_col_norm=0.5)]
    trainer = create_trainer(topo_view_noised, y,max_epochs=100, layers=layers, cost=None)
    
    trainer.run_until_earlystop()
    in_grad_func = create_input_grad_for_wanted_class_func(trainer.model)
    batches = 3
    start_in = create_rand_input(trainer.model, batches)
    ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000)
    inputs_class_1 = gradient_descent_input(start_in, in_grad_func, [1,0], **ascent_args)
    inputs_class_2 = gradient_descent_input(start_in, in_grad_func, [0,1], **ascent_args)
    return inputs_class_1, inputs_class_2

def reconstruct_ignore_activation(model, wanted_soft_out=[-1,1]):
    class_weights = np.array(wanted_soft_out)[:,np.newaxis,np.newaxis,np.newaxis]

    wanted_softmax_in = np.sum(model.layers[-1].get_weights_topo()  * class_weights, axis=0)

    wanted_softmax_in = np.sum(wanted_softmax_in, axis=1)
    wanted_softmax_in =  wanted_softmax_in * np.float32(wanted_softmax_in > 0)
    input_length = model.input_space.shape[0]
    weight_length = model.layers[0].kernel_shape[0]
    weight_appearances = np.int(input_length / weight_length)
    time_weights = np.squeeze(model.layers[0].get_weights_topo())

    repeated_weights = np.tile(time_weights[::-1], weight_appearances)
    pool_length = model.layers[1].pool_shape[0]
    pool_stride = model.layers[1].pool_stride[0]

    centers = range(pool_length/2,(input_length - weight_length + 1) - pool_length + 1 + pool_stride,pool_stride)
    
    wanted_softmax_in = np.squeeze(wanted_softmax_in)
    interp_pool = interp1d(centers, wanted_softmax_in)
    interp_values = [interp_pool(sample_i) for sample_i in range(centers[0],centers[-1] + 1)]
    in_reconstructed = repeated_weights[centers[0]:centers[-1] + 1] * interp_values
    return in_reconstructed

def add_increasing_timecourse(topo_view, start=0.3, stop=1.5):
    return topo_view * np.linspace(start,stop, topo_view.shape[2])[np.newaxis, np.newaxis,:,np.newaxis]
def get_artificial_model():
    input_shape=(600,1,600,1)
    start_topo_view, y = create_shifted_sines(input_shape, RandomState(np.uint64(hash("pooled_pipeline"))), 
                                              sampling_freq=150.0)
    topo_view_noised, y = pipeline(start_topo_view, y,
                             lambda x,y: put_noise(x, weight_old_data=0.05))
    # convolve + square + maxpool
    layers = [ConvSquared(output_channels=1, kernel_shape=[150,1], pool_shape=[-1,1], pool_stride=[-1,1],
                         pool_type=None,layer_name="conv_squared", irange=0.05, max_kernel_norm=2.0, tied_b=True),
              PoolLayer(pool_func=log_sum, layer_name="pool_squared", pool_shape=[50,1], pool_stride=[50,1]),
              Softmax(n_classes=2, layer_name='y', irange=0.001, max_col_norm=0.5)]
    trainer = create_trainer(topo_view_noised, y,max_epochs=100, layers=layers, cost=None)

    trainer.run_until_earlystop()
    return trainer.model


def get_artificial_dataset():
    input_shape=(600,1,600,1)
    start_topo_view, y = create_shifted_sines(input_shape, RandomState(np.uint64(hash("pooled_pipeline"))), 
                                              sampling_freq=150.0)
    topo_view_noised, y = pipeline(start_topo_view, y,
                             lambda x,y: put_noise(x, weight_old_data=0.05))
    return topo_view_noised, y


# ## BhNo Deconv All Sensors

# In[6]:

ResultPrinter('data/models/two-class/raw-square/identity-bp/early-stop/').print_results()


# In[345]:

ResultPrinter('data/models/two-class/csp-net-keep-lrule/test/').print_results()


# In[344]:

ResultPrinter('data/models/two-class/raw-square/identity-bp/test/').print_results()


# ## Artificial data ascend actual trials

# In[206]:

get_ipython().run_cell_magic(u'capture', u'', u'artificial_model = get_artificial_model()\n    ')


# In[213]:

artificial_topo, artificial_y = get_artificial_dataset()


# In[212]:

in_grad_func = create_input_grad_for_wanted_class_func(artificial_model)
batches = 3
start_in = artificial_topo[0:1]
ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000)
inputs_class_1 = gradient_descent_input(start_in, in_grad_func, [1,0], **ascent_args)
inputs_class_2 = gradient_descent_input(start_in, in_grad_func, [0,1], **ascent_args)


# In[240]:

_ = pyplot.plot(inputs_class_2[::100].squeeze().T)


# ## 2 sensors ascend actual trials

# In[222]:

bhno_right_rest_two_sensors_model = serial.load('data/models/two-class/raw-square/identity-bp/test/2.pkl').model
bhno_right_rest_two_sensors_model.monitor.channels['test_y_misclass'].val_record[-1]


# In[246]:

get_ipython().run_cell_magic(u'capture', u'', u"trainer = create_training_object_from_file('data/models/two-class/raw-square/identity-bp/test/2.yaml')\ntrainer.dataset_splitter.ensure_dataset_is_loaded()\ndatasets = bhno_right_rest_two_sensors_trainer.get_train_fitted_valid_test()")


# In[248]:

topo_view = datasets['test'].get_topological_view()


# In[234]:

preds_correct = (np.argmax(bhno_right_rest_two_sensors_model.info_predictions['test'], axis=1) == 
 np.argmax(bhno_right_rest_two_sensors_model.info_targets['test'],axis=1))

false_trial_i = np.flatnonzero(preds_correct == False)[0]


# In[254]:

print ("actual", np.argmax(bhno_right_rest_two_sensors_model.info_targets['test'],axis=1)[false_trial_i])
print ("predicted", np.argmax(bhno_right_rest_two_sensors_model.info_predictions['test'],axis=1)[false_trial_i])


# In[251]:

_ = plot_sensor_signals(topo_view[false_trial_i], ['C3', 'C4'])


# In[289]:

in_grad_func = create_input_grad_for_wanted_class_func(bhno_right_rest_two_sensors_model)
batches = 3
start_in = np.float32(topo_view[false_trial_i:false_trial_i+1])
ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000)
inputs_class_1 = gradient_descent_input(start_in, in_grad_func, [1,0], **ascent_args)
inputs_class_2 = gradient_descent_input(start_in, in_grad_func, [0,1], **ascent_args)


# In[290]:

_ = plot_sensor_signals(inputs_class_1.squeeze()[::100].transpose(1,2,0))
_ = plot_sensor_signals(inputs_class_2.squeeze()[::100].transpose(1,2,0))


# In[300]:


class_1_norm = np.sqrt(np.sum(np.square(inputs_class_1), axis=(1,2,3,4), keepdims=True))
class_2_norm = np.sqrt(np.sum(np.square(inputs_class_2), axis=(1,2,3,4), keepdims=True))
mean_norm = np.mean([class_1_norm, class_2_norm], axis=0) 
normed_class_1 = inputs_class_1  / mean_norm
normed_class_2 = inputs_class_2  / mean_norm


# In[301]:

_ = plot_sensor_signals(normed_class_1.squeeze()[::100].transpose(1,2,0))


# In[302]:

_ = plot_sensor_signals(normed_class_2.squeeze()[[0,-1]].transpose(1,2,0))


# ## Highlighting important parts

# In[306]:

start_in.shape


# In[313]:

pyplot.plot(start_in.squeeze()[0])
pyplot.figure()
pyplot.plot(start_in.squeeze()[1])


# In[315]:

activation_func = create_activations_func(bhno_right_rest_two_sensors_model)

activations = activation_func(start_in)


# In[317]:

activations[0].shape


# In[320]:

_ = plot_sensor_signals(activations[0][0,:,:,0])


# In[322]:


_ = plot_sensor_signals(activations[1][0,:,:,0])


# In[338]:

mean_activations = np.mean(np.exp(activations[2].squeeze()), axis=0)
pyplot.plot(mean_activations)


# In[341]:

soft_layer_weights_topo = bhno_right_rest_two_sensors_model.layers[-1].get_weights_topo()
soft_weights = np.abs(soft_layer_weights_topo).squeeze().transpose(0,2,1)

_  = plot_sensor_signals(soft_weights[0])


# In[342]:


_  = plot_sensor_signals(soft_weights[1])


# In[336]:

activations[2].shape


# In[326]:

_ = plot_sensor_signals(activations[2].squeeze())


# ## 45 sensors deconv

# In[118]:

bhno_right_rest_model = serial.load('data/models/two-class/raw-square/identity-bp/early-stop/3.pkl').model
_ = plot_misclasses_for_model(bhno_right_rest_model)


# In[204]:


inputs_class_1 = deconv_model([1,-1], bhno_right_rest_model)
inputs_class_2 = deconv_model([-1,1], bhno_right_rest_model)
fig = plot_head_signals_tight_two_signals(inputs_class_1, inputs_class_2, get_C_sensors_sorted(),
    plot_args=dict(linewidth=0.4), figsize=(12.5,8))
fig.suptitle('Right Hand and Rest Deconv', fontsize=14)


# In[346]:


inputs_class_1 = deconv_model([1,-1], bhno_right_left_model)
inputs_class_2 = deconv_model([-1,1], bhno_right_left_model)
fig = plot_head_signals_tight_two_signals(inputs_class_1, inputs_class_2, get_C_sensors_sorted(),
    plot_args=dict(linewidth=0.4), figsize=(12.5,8))
fig.suptitle('Right Hand and Left Hand Deconv', fontsize=14)


# ## 45 sensors ascend bhno

# In[113]:

in_grad_func = create_input_grad_for_wanted_class_func(bhno_right_rest_model)
batches = 3
start_in = create_rand_input(bhno_right_rest_model, batches)
ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000)
inputs_class_1 = gradient_descent_input(start_in, in_grad_func, [1,0], **ascent_args)
inputs_class_2 = gradient_descent_input(start_in, in_grad_func, [0,1], **ascent_args)
fig = plot_head_signals_tight_two_signals(inputs_class_1[-1,0], inputs_class_2[-1,0], get_C_sensors_sorted(),
    plot_args=dict(linewidth=0.4), figsize=(12.5,8))
fig.suptitle('Right Hand and Rest Ascend', fontsize=14)


# #### Right Hand vs Left Hand

# In[117]:

bhno_right_left_model = serial.load('data/models/two-class/raw-square/identity-bp/early-stop/7.pkl').model
_ = plot_misclasses_for_model(bhno_right_left_model)


# In[119]:


inputs_class_1 = deconv_model([1,-1], bhno_right_left_model)
inputs_class_2 = deconv_model([-1,1], bhno_right_left_model)
fig = plot_head_signals_tight_two_signals(inputs_class_1, inputs_class_2, get_C_sensors_sorted(),
    plot_args=dict(linewidth=0.4), figsize=(12.5,8))
fig.suptitle('Right Hand and Left Hand Deconv', fontsize=14)


# In[120]:

in_grad_func = create_input_grad_for_wanted_class_func(bhno_right_left_model)
batches = 3
start_in = create_rand_input(bhno_right_left_model, batches)
ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000)
inputs_class_1 = gradient_descent_input(start_in, in_grad_func, [1,0], **ascent_args)
inputs_class_2 = gradient_descent_input(start_in, in_grad_func, [0,1], **ascent_args)
fig = plot_head_signals_tight_two_signals(inputs_class_1[-1,0], inputs_class_2[-1,0], get_C_sensors_sorted(),
    plot_args=dict(linewidth=0.4), figsize=(12.5,8))
fig.suptitle('Right Hand and Left Hand Ascend', fontsize=14)


# ## Ascending on actual trials

# In[200]:

get_ipython().run_cell_magic(u'capture', u'', u"trainer = create_training_object_from_file('data/models/two-class/raw-square/identity-bp/early-stop/7.yaml')\n\ntrainer.dataset_splitter.ensure_dataset_is_loaded()\ndatasets = trainer.get_train_fitted_valid_test()")


# In[141]:

predict_func = create_prediction_func(bhno_right_left_model)
# first trial is for class 0 (right)


# In[201]:

start_in_right = datasets['test'].get_topological_view()[0:1].astype(np.float32)
in_grad_func = create_input_grad_for_wanted_class_func(bhno_right_left_model)
batches = 3
ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000)
inputs_right_class_1 = gradient_descent_input(start_in_right, in_grad_func, [1,0], **ascent_args)
inputs_right_class_2 = gradient_descent_input(start_in_right, in_grad_func, [0,1], **ascent_args)


# In[202]:

start_in_left = datasets['test'].get_topological_view()[1:2].astype(np.float32)
in_grad_func_left = create_input_grad_for_wanted_class_func(bhno_right_left_model)
batches = 3
ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000)
inputs_left_class_1 = gradient_descent_input(start_in_left, in_grad_func_left, [1,0], **ascent_args)
inputs_left_class_2 = gradient_descent_input(start_in_left, in_grad_func_left, [0,1], **ascent_args)


# In[203]:

mean_var = np.sqrt(np.mean([np.var(inputs_right_class_1[650,0]), np.var(inputs_right_class_2[650,0])]))
all_signals = np.array([start_in[0] * mean_var, inputs_right_class_1[650,0], inputs_right_class_2[650,0],
                       start_in_left[0] * mean_var, inputs_left_class_1[650,0], inputs_left_class_2[650,0]])
fig = plot_head_signals_tight_multiple_signals(all_signals, get_C_sensors_sorted(),
    plot_args=dict(linewidth=0.4), figsize=(13,24))
fig.suptitle('Ascend: Original Right, Right Hand, Left Hand, Original Left, Right Hand, Left Hand', fontsize=14)


# ###?? other stuff :)

# In[380]:

bps, freqs = bps_and_freqs(inputs_class_2)

fig = plot_sensor_signals(bps.squeeze(), ['C3', 'C4'], xvals=freqs, figsize=(8,2))

pyplot.xticks([0,5,10,15] + range(20,81,10))
pyplot.xlabel('Hz')
fig.suptitle('Rest', fontsize=12)
None


# In[381]:

inputs_class_1 = deconv_model([1,-1], bhno_right_left_model)
inputs_class_2 = deconv_model([-1,1], bhno_right_left_model)
_ = plot_sensor_signals_two_classes(inputs_class_1, inputs_class_2, ['C3', 'C4'],
                               'Right Hand', 'Left Hand')
pyplot.xticks(range(0,601,75), range(0,4001,500))
pyplot.xlabel('milliseconds')


# In[382]:

bps, freqs = bps_and_freqs(inputs_class_1)

fig = plot_sensor_signals(bps.squeeze(), ['C3', 'C4'], xvals=freqs, figsize=(8,2))

pyplot.xticks([0,5,10,15] + range(20,81,10))
pyplot.xlabel('Hz')
fig.suptitle('Right Hand', fontsize=12)
None


# In[388]:

bps, freqs = bps_and_freqs(inputs_class_1[:, 75:225])

fig = plot_sensor_signals(bps.squeeze(), ['C3', 'C4'], xvals=freqs, figsize=(8,2))

pyplot.xticks([0,5,10,15] + range(20,81,10))
pyplot.xlabel('Hz')
fig.suptitle('Right Hand 500ms - 1500ms', fontsize=12)
None


# ## Artificial data deconv

# In[343]:

get_ipython().run_cell_magic(u'capture', u'', u'artificial_model = get_artificial_model()')


# In[368]:

inputs_class_1 = deconv_model([1,-1], artificial_model)
inputs_class_2 = deconv_model([-1,1], artificial_model)
fig, axes = pyplot.subplots(2,1)
axes[0].set_title("Class 1")
axes[0].plot(inputs_class_1)
axes[1].set_title("Class 2")
axes[1].plot(inputs_class_2)
fig.subplots_adjust(hspace=2)


_ = pyplot.setp(axes, xticks=range(0,601,75), xticklabels=range(0,4001,500), xlabel='milliseconds', 
                ylim=(-0.03, 0.03), yticks=(-0.02, 0, 0.02))


# ## BhNo Ascend

# ### Right vs Left

# In[310]:

bhno_right_left_model = serial.load('data/models/two-class/raw-square/identity-bp/test/6.pkl').model
in_grad_func = create_input_grad_for_wanted_class_func(bhno_right_left_model)
batches = 3
start_in = create_rand_input(bhno_right_left_model, batches)
ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000)
inputs_class_1 = gradient_descent_input(start_in, in_grad_func, [1,0], **ascent_args)
inputs_class_2 = gradient_descent_input(start_in, in_grad_func, [0,1], **ascent_args)
_ = plot_sensor_signals_two_classes(inputs_class_1[-1,0], inputs_class_2[-1,0], ['C3', 'C4'],
                               'Right Hand', 'Left Hand')
pyplot.xticks(range(0,601,75), range(0,4001,500))
pyplot.xlabel('milliseconds')


# In[305]:

bps, freqs = bps_and_freqs(inputs_class_1[-1,0])

fig = plot_sensor_signals(bps.squeeze(), ['C3', 'C4'], xvals=freqs, figsize=(8,2))

pyplot.xticks([0,4,8,12,16] + range(20,81,10))
pyplot.xlabel('Hz')
fig.suptitle('Right Hand', fontsize=12)
None


# In[311]:

bps, freqs = bps_and_freqs(inputs_class_2[-1,0])

fig = plot_sensor_signals(bps.squeeze(), ['C3', 'C4'], xvals=freqs, figsize=(8,2))

pyplot.xticks([0,4,8,12,16] + range(20,81,10))
pyplot.xlabel('Hz')
fig.suptitle('Left Hand', fontsize=12)
None


# ### Right vs Rest

# In[313]:

bhno_right_rest_model = serial.load('data/models/two-class/raw-square/identity-bp/test/2.pkl').model


# In[322]:

in_grad_func = create_input_grad_for_wanted_class_func(bhno_right_rest_model)
batches = 3
start_in = create_rand_input(bhno_right_rest_model, batches)
ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000)
inputs_class_1 = gradient_descent_input(start_in, in_grad_func, [1,0], **ascent_args)
inputs_class_2 = gradient_descent_input(start_in, in_grad_func, [0,1], **ascent_args)


# In[323]:

_ = plot_sensor_signals_two_classes(inputs_class_1[-1,0], inputs_class_2[-1,0], ['C3', 'C4'],
                               'Right Hand', 'Rest')
pyplot.xticks(range(0,601,75), range(0,4001,500))
pyplot.xlabel('milliseconds')
None


# In[324]:

bps, freqs = bps_and_freqs(inputs_class_2[-1,0])

fig = plot_sensor_signals(bps.squeeze(), ['C3', 'C4'], xvals=freqs, figsize=(8,2))

pyplot.xticks([0,4,8,12,16] + range(20,81,10))
pyplot.xlabel('Hz')
fig.suptitle('Rest', fontsize=12)
None


# ## Two sines reconstruction

# In[142]:

input_shape=(600,1,600,1)
sine_1_view, y = create_shifted_sines(input_shape, RandomState(np.uint64(hash("pooled_pipeline"))), 
                                          sampling_freq=150.0, freq=13)
sine_2_view, y2 = create_shifted_sines(input_shape, RandomState(np.uint64(hash("pooled_pipeline"))), 
                                          sampling_freq=150.0, freq=22)
rng = RandomState(np.uint64(hash("switch_sines")))
mask = rng.binomial(n=1, p=0.5, size=sine_1_view.shape[0])
mask = mask[:,np.newaxis, np.newaxis,np.newaxis]
start_topo_view = sine_1_view * mask + sine_2_view *  (1 - mask)

assert np.array_equal(y,y2)
topo_view_noised, y = pipeline(start_topo_view, y,
                         lambda x,y: add_increasing_timecourse(x, start=0.3, stop=1.5),
                         lambda x,y: put_noise(x, weight_old_data=0.05))


# In[143]:

get_ipython().run_cell_magic(u'capture', u'', u'# failing reconstruction with 15 output channels\n# convolve + square + maxpool\nlayers = [ConvSquaredFullHeight(output_channels=3, kernel_shape=[150,-1], pool_shape=[-1,-1], pool_stride=[-1,-1],\n                     pool_type=None,layer_name="conv_squared", irange=0.05, max_kernel_norm=2.0, tied_b=True),\n              PoolLayer(pool_func=log_sum, layer_name="pool_squared", pool_shape=[50,1], pool_stride=[50,1]),\n          Softmax(n_classes=2, layer_name=\'y\', irange=0.001, max_col_norm=0.5)] # irange=0.001\ntrainer = create_trainer(topo_view_noised, y,max_epochs=80, layers=layers, cost=None)\n\ntrainer.run_until_earlystop()')


# In[146]:

_ = plot_misclasses_for_model(trainer.model)


# In[147]:

_ = pyplot.plot(trainer.model.get_weights_topo().squeeze().T)


# In[148]:

bps, freqs = bps_and_freqs(trainer.model.get_weights_topo().squeeze(), axis=1)

pyplot.plot(freqs, bps.T)
pyplot.figure()

pyplot.plot(freqs, np.mean(bps, axis=0))


# In[157]:

wanted_conv_in = deconv_model([-1,1], real_data_model)
_ = plot_sensor_signals(wanted_conv_in)


# In[155]:

wanted_out_before_softmax = [-1,+1]
wanted_conv_in = deconv_model(wanted_out_before_softmax, trainer.model)
_ = pyplot.plot(wanted_conv_in)


# In[153]:

wanted_out_before_softmax = [1,-1]
wanted_conv_in = deconv_model(wanted_out_before_softmax, trainer.model)
pyplot.plot(wanted_conv_in)


# In[166]:

real_data_model = serial.load('data/models/two-class/raw-square/identity-bp/test/2.pkl').model
_ = plot_misclasses_for_model(real_data_model)


# In[167]:

wanted_out_before_softmax = [-1,+1]
model = real_data_model
wanted_conv_in = deconv_model(wanted_out_before_softmax, real_data_model)
_ = plot_sensor_signals(wanted_conv_in, ['C3', 'C4'], yticks="onlymax")
rest_class_in = wanted_conv_in


# In[168]:

wanted_out_before_softmax = [+1,-1]
model = real_data_model
wanted_conv_in = deconv_model(wanted_out_before_softmax, real_data_model)
_ = plot_sensor_signals(wanted_conv_in, ['C3', 'C4'], yticks="onlymax")
right_class_in = wanted_conv_in


# In[184]:

both_classes = np.array([right_class_in, rest_class_in]).transpose(1,2,0)
_ = plot_sensor_signals(both_classes, ['C3', 'C4'], figsize=(8,2))


# In[214]:

both_classes = np.concatenate((right_class_in, rest_class_in))
fig = plot_sensor_signals(both_classes, ['C3', 'C4', 'C3', 'C4'], figsize=(8,2))
pyplot.text(0.02,0.7,"Right Hand",transform=fig.transFigure, fontsize=12, horizontalalignment='right')
pyplot.text(0.02,0.3,"Rest",transform=fig.transFigure, fontsize=12, horizontalalignment='right')

_ = pyplot.plot([0.125, 0.9], [0.51,0.51], color='grey',
    lw=1,transform=fig.transFigure, clip_on=False, linestyle="-")


# In[216]:

in_grad_func = create_input_grad_for_wanted_class_func(real_data_model)
batches = 3
start_in = create_rand_input(real_data_model, batches)
ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000)
inputs_class_1 = gradient_descent_input(start_in, in_grad_func, [1,0], **ascent_args)
inputs_class_2 = gradient_descent_input(start_in, in_grad_func, [0,1], **ascent_args)
fig, axes = pyplot.subplots(2,1)
axes[0].set_title("Class 1")
axes[0].plot(np.squeeze(inputs_class_1[-1,2]).T)
axes[0].set_yticks([-0.0001,0,0.0001])
axes[1].set_title("Class 2")
axes[1].set_yticks([-0.05,0,0.05])
axes[1].plot(np.squeeze(inputs_class_2[-1,2]).T)
fig.subplots_adjust(hspace=1)


# In[223]:

real_data_model = serial.load('data/models/two-class/raw-square/identity-bp/test/6.pkl').model

in_grad_func = create_input_grad_for_wanted_class_func(real_data_model)
batches = 3
start_in = create_rand_input(real_data_model, batches)
ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000)
inputs_class_1 = gradient_descent_input(start_in, in_grad_func, [1,0], **ascent_args)
inputs_class_2 = gradient_descent_input(start_in, in_grad_func, [0,1], **ascent_args)
fig, axes = pyplot.subplots(2,1)
axes[0].set_title("Class 1")
axes[0].plot(np.squeeze(inputs_class_1[-1,2]).T)
axes[0].set_yticks([-0.0001,0,0.0001])
axes[1].set_title("Class 2")
axes[1].set_yticks([-0.05,0,0.05])
axes[1].plot(np.squeeze(inputs_class_2[-1,2]).T)
fig.subplots_adjust(hspace=1)


# In[105]:

bps, freqs = bps_and_freqs(right_class_in)
pyplot.plot(freqs, bps.T)


# In[103]:

bps, freqs = bps_and_freqs(model.get_weights_topo().squeeze())

pyplot.plot(freqs, np.mean(bps, axis=0))


# In[528]:

in_grad_func = create_input_grad_for_wanted_class_func(trainer.model)
batches = 3
start_in = create_rand_input(trainer.model, batches)
ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000)
inputs_class_1 = gradient_descent_input(start_in, in_grad_func, [1,0], **ascent_args)
inputs_class_2 = gradient_descent_input(start_in, in_grad_func, [0,1], **ascent_args)
fig, axes = pyplot.subplots(2,1)
axes[0].set_title("Class 1")
axes[0].plot(np.squeeze(inputs_class_1[-1,2]).T)
#axes[0].set_yticks([-0.0001,0,0.0001])
axes[1].set_title("Class 2")
#axes[1].set_yticks([-0.05,0,0.05])
axes[1].plot(np.squeeze(inputs_class_2[-1,2]).T)
fig.subplots_adjust(hspace=1)


# In[517]:

bps, freqs = bps_and_freqs(inputs_class_2[-1,1].squeeze(), axis=0)
pyplot.plot(freqs, bps)


# ### Try ascending from a particular input

# In[540]:

sine_input = trainer.dataset_splitter.dataset.get_topological_view()[0]
nosine_input = trainer.dataset_splitter.dataset.get_topological_view()[1]

pyplot.plot(sine_input.squeeze())
pyplot.plot(nosine_input.squeeze())


# In[543]:

in_grad_func = create_input_grad_for_wanted_class_func(trainer.model)
batches = 3
start_in = np.float32([sine_input, nosine_input])
ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000)
inputs_class_1 = gradient_descent_input(start_in, in_grad_func, [1,0], **ascent_args)
inputs_class_2 = gradient_descent_input(start_in, in_grad_func, [0,1], **ascent_args)
fig, axes = pyplot.subplots(2,1)
axes[0].set_title("Class 1")
axes[0].plot(np.squeeze(inputs_class_1[-1,0]).T)
#axes[0].set_yticks([-0.0001,0,0.0001])
axes[1].set_title("Class 2")
#axes[1].set_yticks([-0.05,0,0.05])
axes[1].plot(np.squeeze(inputs_class_2[-1,0]).T)
fig.subplots_adjust(hspace=1)


# ## Single sine reconstruction

# In[6]:

get_ipython().run_cell_magic(u'capture', u'', u'inputs_class_1, inputs_class_2 = get_reconstructions_artificial_data()')


# In[7]:

fig, axes = pyplot.subplots(2,1)
axes[0].set_title("Class 1")
axes[0].plot(np.squeeze(inputs_class_1[-1,2]).T)
axes[0].set_yticks([-0.0001,0,0.0001])
axes[1].set_title("Class 2")
axes[1].set_yticks([-0.05,0,0.05])
axes[1].plot(np.squeeze(inputs_class_2[-1,2]).T)
fig.subplots_adjust(hspace=1)


# ## Single Sine with timecourse reconstruction

# In[23]:


input_shape=(600,1,600,1)
start_topo_view, y = create_shifted_sines(input_shape, RandomState(np.uint64(hash("pooled_pipeline"))), 
                                          sampling_freq=150.0)
topo_view_noised, y = pipeline(start_topo_view, y,
                         lambda x,y: add_increasing_timecourse(x, start=0.3, stop=1.5),
                         lambda x,y: put_noise(x, weight_old_data=0.05))


# In[32]:

get_ipython().run_cell_magic(u'capture', u'', u'# convolve + square + maxpool\nlayers = [ConvSquared(output_channels=1, kernel_shape=[150,1], pool_shape=[-1,-1], pool_stride=[-1,-1],\n                     pool_type=None,layer_name="conv_squared", irange=0.05, max_kernel_norm=2.0, tied_b=True),\n              PoolLayer(pool_func=log_sum, layer_name="pool_squared", pool_shape=[50,1], pool_stride=[50,1]),\n\n          Softmax(n_classes=2, layer_name=\'y\', irange=0.001, max_col_norm=0.5)]\ntrainer = create_trainer(topo_view_noised, y,max_epochs=100, layers=layers, cost=None)\n\ntrainer.run_until_earlystop()')


# In[37]:

_ = plot_misclasses_for_model(trainer.model)


# In[35]:

in_grad_func = create_input_grad_for_wanted_class_func(trainer.model)
batches = 3
start_in = create_rand_input(trainer.model, batches)
ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000)
inputs_class_1 = gradient_descent_input(start_in, in_grad_func, [1,0], **ascent_args)
inputs_class_2 = gradient_descent_input(start_in, in_grad_func, [0,1], **ascent_args)
fig, axes = pyplot.subplots(2,1)
axes[0].set_title("Class 1")
axes[0].plot(np.squeeze(inputs_class_1[-1,2]).T)
axes[0].set_yticks([-0.0001,0,0.0001])
axes[1].set_title("Class 2")
axes[1].set_yticks([-0.05,0,0.05])
axes[1].plot(np.squeeze(inputs_class_2[-1,2]).T)
fig.subplots_adjust(hspace=1)


# In[158]:




# In[159]:

pyplot.plot(reconstruct_ignore_activation(trainer.model))


# In[160]:

pyplot.plot(reconstruct_ignore_activation(trainer.model, wanted_soft_out=[1,-1]))


# In[112]:

class_weights = np.array([-1,1])[:,np.newaxis,np.newaxis,np.newaxis]

weighted_weights = np.sum(trainer.model.layers[2].get_weights_topo()  * class_weights, axis=0)

wanted_in = np.sum(weighted_weights, axis=1)


# In[133]:


input_length = trainer.model.input_space.shape[0]
weight_length = trainer.model.layers[0].kernel_shape[0]
weight_appearances = np.int(input_length / weight_length)
time_weights = np.squeeze(trainer.model.layers[0].get_weights_topo())

repeated_weights = np.tile(time_weights[::-1], weight_appearances)# -1 since we are doing convolution


# In[124]:


pool_length = 50
pool_stride = 50

centers = range(pool_length/2,(input_length - weight_length + 1) - pool_length + 1 + pool_stride,pool_stride)


# In[130]:

centers


# In[126]:

from scipy.interpolate import interp1d

wanted_in = np.squeeze(wanted_in)
interp_pool = interp1d(centers, wanted_in)


# In[127]:

interp_values = [interp_pool(sample_i) for sample_i in range(25,426)]


# In[128]:

in_reconstructed = repeated_weights[25:426] * interp_values


# In[129]:

pyplot.plot(in_reconstructed)

