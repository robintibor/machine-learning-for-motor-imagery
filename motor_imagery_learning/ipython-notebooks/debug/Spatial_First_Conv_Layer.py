
# coding: utf-8

# In[200]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
#os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import numpy as np
from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import ConvElemwise
from theano.sandbox.cuda.dnn import dnn_conv
from theano.sandbox.cuda.basic_ops import gpu_alloc_empty
from motor_imagery_learning.analysis.print_results import ResultPrinter
from pylearn2.utils import serial

import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7


# In[4]:

def convolve_keeping_chans_one_conv(inputs, filters, numfilters):
    # inputs shape should be #batches #virtualchans x #0 x #1
    outshape = compute_out_shape(inputs.shape, filters.shape)
    # reshape so that each channel is an own "input image"
    # will make unnecessary convolutions since it convolves any filter with any channel,
    # but we only want filter 1 convolved with channel 1(for all images),
    # filter 2 with channel 2 etc.
    # so we only need #batches x #filters convolutions
    # but we will make #batches x #filters x#chans (or sth like this, maybe not correct :))
    rval = gpu_alloc_empty(inputs.shape[0], numfilters, outshape[2], outshape[3])
    inputs = inputs.reshape((inputs.shape[0] * inputs.shape[1], 1, inputs.shape[2],
                           inputs.shape[3]))
    conved = dnn_conv(inputs, filters)
    
    # Take only the convolutions we actually want:
    # we want (batch-filter), always matching channel 0 with filter 0, channel 1 with filter 1 etc.:
    # 0-0, 7-0, 14-0,...70-0,
    # 1-1, 8-1, 15-1 ..
    # ..
    # 6-6, 13-6, ..., 76-6
    # we don't want:
    # 0-1, 0-2,0-3, ..., 0-6
    # 1-0, 1-2, 1-3, .. 1-6
    # ...
    # 
    for filter_i in range(numfilters):
         rval = T.set_subtensor(rval[:,filter_i:filter_i+1,:,:], 
                                conved[filter_i::num_filters, filter_i:filter_i+1, :, :])
    return rval


# In[5]:

def convolve_keeping_chans(inputs, filters, numfilters):
    # inputs shape should be #batches #virtualchans x #0 x #1
    # loop through filters, make convolutions for 1-dimensional chans
    outshape = compute_out_shape(inputs.shape, filters.shape)
    rval = gpu_alloc_empty(inputs.shape[0], numfilters, outshape[2], outshape[3])
    for filter_i in range(numfilters):
        conved_by_filter_i = dnn_conv(inputs[:,filter_i:filter_i+1,:,:], filters[filter_i:filter_i+1,:,:,:])
        rval = T.set_subtensor(rval[:,filter_i:filter_i+1,:,:], conved_by_filter_i)
    return rval
    # we want output for next layer:  #batches x #channels x #(convolved 0) x #(convolved 1)
    # => output dim channels is same as input dim channels
    # reshape input to: inputs.reshape(inputs.shape[0] * inputs.shape[1], inputs.shape[2], inputs.shape[3])
    # inputs = inputs.dimshuffle(0, 'x', 1, 2) (#batches*#channels) x 1 x  #(convolved 0) x #(convolved 1)
    # conv mit filters der größe (#batches*#channels) x 1 x #(filter 0) x #(filter 1)
    # result.reshape(inputs.shape[0], inputs.shape[1], inputs.shape[2], inputs.shape[3])
    
def loop_conv(X, W):
    """ Gold standard convolution for test, looping over all dimensions.
    Actually performing cross correlation, not convolution. 
    Assumes bc012 input"""
    # Go over all five dimensions 
    # (#batches x #channels x #height x #width x #dur/length )
    # with filter that has
    # #filters x #channels x #height x #width x #dur/length 
    num_filters = W.shape[0]
    filt_channels = W.shape[1]
    filt_height = W.shape[2]
    filt_width = W.shape[3]
    num_batches = X.shape[0]
    input_channels = X.shape[1]
    assert(filt_channels == input_channels)
    out_shape = compute_out_shape(X.shape, W.shape)
    out_height = out_shape[2]
    out_width = out_shape[3]
    
    # The output is H :)
    H = np.zeros((out_shape))
    for batch_i in xrange(0, num_batches):
        for filt_i in xrange(0, num_filters):
            for out_x in xrange(0, out_height):
                for out_y in xrange(0, out_width):
                    for chan_i in xrange(0, filt_channels):
                        for filt_x in xrange(0, filt_height):
                            for filt_y in xrange(0, filt_width):
                                weight = W[filt_i, chan_i, filt_x, filt_y]
                                input_val =  X[batch_i, chan_i,                                     out_x + filt_x, out_y + filt_y]
                                H[batch_i, filt_i, out_x, out_y] +=                                      weight * input_val
    return H


def compute_out_shape(inputs_shape, filters_shape):
    num_batches = inputs_shape[0]
    out_height = inputs_shape[2] - filters_shape[2] + 1;
    out_width = inputs_shape[3] - filters_shape[3] + 1;
    num_filters = filters_shape[0]
    return (num_batches, num_filters, out_height, out_width)


# In[7]:

input_shape = (600,3)
num_chans = 45
num_filters = 11
kernel_shape=(10,2)
num_batches = 100


# In[8]:

inputs = T.ftensor4()
filters = T.ftensor4() # filter x 1, inputshape2, inputshape3
result = convolve_keeping_chans(inputs, filters, num_filters)
convolve_func = theano.function([inputs, filters], result)
sumresult = T.sum(result)
convolve_sum_func = theano.function([inputs, filters], sumresult)


# In[9]:

inputs = T.ftensor4()
filters = T.ftensor4() # filter x 1, inputshape2, inputshape3
result = dnn_conv(inputs, filters)
dnn_func = theano.function([inputs, filters], result)
sumresult = T.sum(result)
dnn_sum_func = theano.function([inputs, filters], sumresult)


# In[10]:

inputs = T.ftensor4()
filters = T.ftensor4() # filter x 1, inputshape2, inputshape3
result = convolve_keeping_chans_one_conv(inputs, filters, num_filters)
one_conv_func = theano.function([inputs, filters], result)
sumresult = T.sum(result)
one_conv_sum_func = theano.function([inputs, filters], sumresult)


# In[11]:


real_input = np.random.randn(num_batches, num_chans, *input_shape).astype('float32')
input_space = Conv2DSpace(shape=input_shape, num_channels=num_chans, axes=('b', 'c', 0, 1))
spatial_filters = np.random.randn(num_filters, num_chans).astype('float32')
kernel_weights = np.random.randn(num_filters, 1, *kernel_shape).astype('float32')
out_shape = compute_out_shape(real_input.shape, kernel_weights.shape)


# In[15]:

filtered_result = np.dot(spatial_filters, real_input.T)
# => c10b
filtered_result = filtered_result.transpose(3,0,2,1)

conv_result = np.empty((num_batches,num_filters,out_shape[2],out_shape[3]), dtype='float32')
for i in range(num_filters):
    conv_result[:,i:i+1,:,:] = dnn_func(filtered_result[:,i:i+1,:,:], kernel_weights[i:i+1,:,:,:])


# In[13]:

old_conv_result = np.copy(conv_result)


# In[16]:

np.allclose(old_conv_result, conv_result)


# ### Test creating weight matrix from vector + matrix 

# In[106]:

spatial_filters.shape


# In[105]:

kernel_weights.shape


# #### make theano unfolding and theano forward pass complete

# In[101]:

spatial_filters_theano = T.fmatrix()
kernel_weights_theano = T.ftensor4()

unfolded_weights_theano = T.batched_tensordot(spatial_filters_theano.dimshuffle(0,1,'x'),
                                              kernel_weights_theano,
                                              axes=((1), (0)))
unfold_func = theano.function([spatial_filters_theano, kernel_weights_theano], unfolded_weights_theano)
inputs = T.ftensor4()
unfolded_result_theano = dnn_conv(inputs, unfolded_weights_theano)
unfold_conv_func = theano.function([inputs, spatial_filters_theano, kernel_weights_theano], unfolded_result_theano)


# In[89]:

unfolded_by_theano = unfold_func(spatial_filters, kernel_weights)


# In[109]:

get_ipython().run_cell_magic(u'time', u'', u'unfold_result = unfold_conv_func(real_input, spatial_filters, kernel_weights)')


# In[104]:

np.sum(np.square(unfold_result - conv_result))


# In[99]:

np.sum(np.square(unfolded_by_theano - unfolded_weights_tdot))


# ## Test layer

# In[194]:

from motor_imagery_learning.mypylearn2.conv_channel_first import ConvChanKernelDot
from pylearn2.models.mlp import IdentityConvNonlinearity

fakemlp = lambda: None
fakemlp.rng = np.random
fakemlp.batch_size = 2
chan_kern_layer = ConvChanKernelDot(output_channels=num_filters, kernel_shape=kernel_weights.shape[2:],
    layer_name='conv_chan_kernel', nonlinearity=IdentityConvNonlinearity(), irange=0.05, tied_b=True)

chan_kern_layer.set_mlp(fakemlp)
chan_kern_layer.set_input_space(Conv2DSpace(real_input.shape[2:], channels=real_input.shape[1], axes=('b', 'c', 0, 1)))


# add weights
folded_weights = np.concatenate((spatial_filters, kernel_weights.reshape(num_filters, -1)), axis=1)
chan_kern_layer.set_weights(folded_weights[:,:,np.newaxis, np.newaxis])
chan_kern_layer.set_biases(np.zeros((num_filters)).astype('float32'))


# In[195]:

inputs = T.ftensor4()
unfolded_layer_result_theano = chan_kern_layer.fprop(inputs)
unfolded_layer_function = theano.function([inputs], unfolded_layer_result_theano)


# In[196]:

unfolded_layer_result = unfolded_layer_function(real_input)


# In[197]:

np.sum(np.square(unfolded_layer_result - conv_result))


# In[17]:

convolve_theano_result = convolve_func(filtered_result, kernel_weights)


# In[18]:

np.allclose(convolve_theano_result, conv_result)


# In[107]:

get_ipython().run_cell_magic(u'time', u'', u'convolve_keep_sum_result = convolve_sum_func(filtered_result, kernel_weights)')


# In[107]:

repeated_kernel = np.repeat(kernel_weights.transpose(1,0,2,3), 80, axis=0)


# In[108]:

get_ipython().run_cell_magic(u'time', u'', u'\ndnn_sum_result = dnn_sum_func(filtered_result, repeated_kernel)')


# In[109]:

get_ipython().run_cell_magic(u'time', u'', u'\none_conv_sum_result = one_conv_sum_func(filtered_result, kernel_weights)')


# In[87]:

np.allclose(one_conv_sum_result, convolve_keep_sum_result)


# In[66]:

result_one_conv = one_conv_func(filtered_result, kernel_weights)


# In[201]:

spatial_model = serial.load('data/models/debug/raw-data-spatial/early-stop/23.pkl')
spatial_model = spatial_model.model

spat_layer = spatial_model.layers[0]
time_layer = spatial_model.layers[1]

spatial_weights = spat_layer.get_weights_topo()
time_weights = time_layer.get_weights_topo()
print "spatial weights", spatial_weights.shape
print "time weights", time_weights.shape


from motor_imagery_learning.analysis.plot_util import plot_sensor_signals
_ = plot_sensor_signals(np.squeeze(time_weights), map(str, range(len(time_weights))), figsize=(4,15),yticks=[])


# ### Unsmoothness weight penalty

# In[208]:

bad_weights = np.squeeze(time_weights[0:2])
good_weights = np.squeeze(time_weights[2:4])
pyplot.plot(good_weights.T)
pyplot.plot(bad_weights.T)


# In[219]:

#pyplot.plot(moving_average(weight, 3))
pyplot.plot(good_weights[0][1:-1])
pyplot.plot(np.convolve(good_weights[0], [0.5, 0, 0.5], mode='valid'))


# In[220]:


pyplot.plot(bad_weights[0][1:-1])
pyplot.plot(np.convolve(bad_weights[0], [0.5, 0, 0.5], mode='valid'))


# In[225]:

pyplot.plot(good_weights[0][1:-1] - np.convolve(good_weights[0], [0.5, 0, 0.5], mode='valid'))
pyplot.plot(bad_weights[0][1:-1] - np.convolve(bad_weights[0], [0.5, 0, 0.5], mode='valid'))


# In[232]:

def weight_unsmoothness_moving_avg(weight):
    average = np.convolve(weight, [0.5, 0, 0.5], mode='valid')
    return np.sum(np.square(weight[1:-1] - average))

def weight_unsmoothness_moving_avg_scaled(weight):
    average = np.convolve(weight, [0.5, 0, 0.5], mode='valid')
    return np.sum(np.square(weight[1:-1] - average)) / np.sum(np.square(weight[1:-1]))

def weight_unsmoothness_diff_square(weight):
    return np.sum(np.square(weight[1:] - weight[:-1])) 

def weight_unsmoothness_diff_square_scaled(weight):
    return np.sum(np.square(weight[1:] - weight[:-1]))  / np.sum(np.square(weight))


# In[237]:

print "moving avg good ", weight_unsmoothness_moving_avg(good_weights[1])
print "diff good ", weight_unsmoothness_diff_square(good_weights[1])
print "moving avg bad ", weight_unsmoothness_moving_avg(bad_weights[1])
print "diff bad ", weight_unsmoothness_diff_square(bad_weights[1])

print "moving avg good scaled ", weight_unsmoothness_moving_avg_scaled(good_weights[1])
print "diff good scaled ", weight_unsmoothness_diff_square_scaled(good_weights[1])
print "moving avg bad scaled ", weight_unsmoothness_moving_avg_scaled(bad_weights[1])
print "diff bad scaled ", weight_unsmoothness_diff_square_scaled(bad_weights[1])


# In[238]:

1.61 / 0.04


# In[240]:

2.06 / 0.28


# In[234]:

weight_unsmoothness_diff_square(bad_weights[0])


# In[223]:

weight_unsmoothness_moving_avg(bad_weights[0])


# In[226]:

weight_unsmoothness_moving_avg(good_weights[1])


# In[227]:

#pyplot.plot(moving_average(weight, 3))
pyplot.plot(good_weights[1][1:-1])
pyplot.plot(np.convolve(good_weights[1], [0.5, 0, 0.5], mode='valid'))


# In[42]:

time_weights[3].shape


# In[65]:

def plot_fft_amplitudes(signal, sampling_rate, figargs={}):
    pyplot.figure(**figargs)
    fft_signal = np.fft.rfft(signal)
    amplitude_signal = np.abs(fft_signal)

    bins = np.fft.rfftfreq(len(signal), d=1/float(sampling_rate))
    pyplot.plot(bins, amplitude_signal)


# In[72]:

plot_fft_amplitudes(np.squeeze(time_weights[3]), sampling_rate=150, figargs={'figsize': (6,4)})


# In[15]:

spatial_model = serial.load('data/models/debug/raw-data-spatial/early-stop/2.pkl')
spatial_model = spatial_model.model

spat_layer = spatial_model.layers[0]
time_layer = spatial_model.layers[1]

spatial_weights = spat_layer.get_weights_topo()
time_weights = time_layer.get_weights_topo()
print "spatial weights", spatial_weights.shape
print "time weights", time_weights.shape


from motor_imagery_learning.analysis.plot_util import plot_sensor_signals
_ = plot_sensor_signals(np.squeeze(time_weights), map(str, range(len(time_weights))), figsize=(4,15))


# In[19]:

from motor_imagery_learning.analysis.plot_util import plot_chan_matrices
from motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
plot_chan_matrices(spatial_weights.swapaxes(1,3)[16], get_C_sensors_sorted())

