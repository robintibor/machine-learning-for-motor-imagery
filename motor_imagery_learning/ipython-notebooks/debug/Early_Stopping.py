
# coding: utf-8

# In[4]:

import os
# make sure to use cpu for printing results...
os.environ['THEANO_FLAGS'] = "floatX=float32,device=cpu,nvcc.fastmath=True"

import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7
from analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file
from motor_imagery_learning.analysis.print_results import ResultPrinter
from pylearn2.utils import serial
import numpy as np
from motor_imagery_learning.preprocessing_util import exponential_running_mean, exponential_running_var


# In[15]:

a = [0.12, 0.19, 0.14, 0.17, 0.17,0.12,0.19, 0.17,0.12, 0.09, 0.07, 0.14, 0.09, 0.12] * 3
exp_means = exponential_running_mean(a, factor_new=0.02, start_mean=0.1)
exp_vars = exponential_running_var(a, exp_means, factor_new=0.02, start_var=0.01)
print(exp_means)
print(exp_vars)
print(0.1 - exp_means + exp_vars )


# In[ ]:

# Idea:
# Use mean or mean of diffs and variance of valid nll
# (use train also somehow?)


# In[151]:

from motor_imagery_learning.analysis.results import ResultPool


# In[2]:

gujo_result = serial.load('data/models/debug/raw-data-new-best/early-stop/7.result.pkl')


# In[11]:

valid_nll = np.array(gujo_result.monitor_channels['valid_y_nll'].val_record)


# In[12]:

diff_valid_nll = valid_nll[1:] - valid_nll[:-1]


# In[8]:

pyplot.plot(valid_nll)


# In[15]:

pyplot.plot(diff_valid_nll)


# In[3]:

def runningMeanFast(x, N):
    return np.convolve(x, np.ones((N,))/N, mode='same')[(N-1):]


# In[35]:

running_mean_valid_nll = runningMeanFast(valid_nll, 200)


# In[56]:

train_nll = np.array(gujo_result.monitor_channels['train_y_nll'].val_record)
test_nll = np.array(gujo_result.monitor_channels['test_y_nll'].val_record)


# In[57]:

running_mean_train_nll = runningMeanFast(train_nll, 200)
running_mean_test_nll = runningMeanFast(test_nll, 200)


# In[71]:

# test on entire raw data new best: test the valid misclass and valid nll from current
# and test the valid misclass and nll by using technique:
# compute variance diff running mean and variance diff running var if diff going up is bigger sqrt of var, stop


# In[136]:

running_mean_valid_nll = runningMeanFast(valid_nll, 3)


# In[333]:

res_pool = ResultPool()
results = res_pool.load_result_objects_for_folder('data/models/debug/raw-data-new-best/early-stop/')
results = res_pool.load_result_objects_for_folder('data/models/debug/bandpower-new-termination/early-stop/first-5/')


# In[367]:


results = res_pool.load_result_objects_for_folder('data/models/debug/bandpower-new-best//early-stop//')
results = res_pool.load_result_objects_for_folder('data/models/debug/raw-data-new-best/early-stop/')


# In[387]:

result = results[10]

test_nll = np.array(result.monitor_channels['test_y_nll'].val_record)
test_misclass = np.array(result.monitor_channels['test_y_misclass'].val_record)
valid_nll = np.array(result.monitor_channels['valid_y_nll'].val_record)
valid_misclass = np.array(result.monitor_channels['valid_y_misclass'].val_record)
exp_mean_valid_nll = exponential_running_mean(valid_nll, factor_new=0.05, init_block_size=20) # 0.02
exp_var_valid_nll = exponential_running_var(valid_nll,exp_mean_valid_nll,factor_new=0.05, init_block_size=20)
exp_std_valid_nll = np.sqrt(exp_var_valid_nll)
running_min = np.minimum.accumulate(exp_mean_valid_nll)
min_exp_valid_nll = np.min(exp_mean_valid_nll[:220])
pyplot.plot(exp_mean_valid_nll)
#pyplot.plot(exp_mean_valid_nll - exp_var_valid_nll)
#pyplot.plot(exp_mean_valid_nll + exp_var_valid_nll)
#pyplot.plot(exp_var_valid_nll + min_exp_valid_nll)
threshold_line = running_min + exp_std_valid_nll
pyplot.plot(threshold_line)
pyplot.plot(valid_nll)
above_threshold = np.flatnonzero(exp_mean_valid_nll > threshold_line)
if len(above_threshold) > 0:
    break_point = above_threshold[0]
    pyplot.axvline(x=break_point, color='black')
    pyplot.axhline(y=np.min(valid_nll[:1420]), color='black', linestyle=":")
    pyplot.plot(valid_misclass[:1420])
    pyplot.plot(test_misclass[:1420])
    pyplot.axhline(y=np.min(valid_misclass[:1420]), color='black', linestyle="-")
    print break_point


# In[383]:

np.min(valid_misclass[:1420])


# In[386]:

np.min(test_misclass[:1420])


# In[388]:

np.min(test_misclass[:break_point])


# In[391]:

test_nll[np.argmin(valid_misclass[:1420])]


# In[392]:

test_nll[break_point]


# ### With mean of diffs 

# In[287]:

for result in results[30:50]:
    print result.parameters['dataset_filename']
    train_nll = np.array(result.monitor_channels['train_y_nll'].val_record)
    valid_nll = np.array(result.monitor_channels['valid_y_nll'].val_record)
    valid_misclass = np.array(result.monitor_channels['valid_y_misclass'].val_record)
    diff_valid_train = valid_nll - train_nll
    after_valid_stop = np.flatnonzero(diff_valid_train > np.mean(diff_valid_train))[-1]
    print("Stop around: {:d}".format(after_valid_stop))
    compare_point = int(0.75 * after_valid_stop)
    best_valid_accuracy = 1 - np.min(valid_misclass[:compare_point])
    best_valid_nll= 1 - np.min(valid_nll[:compare_point])
    print("Max valid accuracy: {:5.2f}%".format(best_valid_accuracy * 100))
    diff_valid_nll = np.diff(valid_nll[:compare_point])#after_valid_stop])
    exp_mean_diff_valid_nll = exponential_running_mean(diff_valid_nll, factor_new=0.02, init_block_size=20) # 0.02
    exp_var_diff_valid_nll = exponential_running_var(diff_valid_nll, exp_mean_diff_valid_nll,
                                                     factor_new=0.01, init_block_size=20)
    var_adjusted = exp_mean_diff_valid_nll / np.sqrt(exp_var_diff_valid_nll)
    if (len(var_adjusted)) < 1000:
        pyplot.figure()
        pyplot.plot(var_adjusted)
        pyplot.axhline(y=0.0)
    stop_trials = np.flatnonzero(var_adjusted[100:] > 0.0 ) # > 0.05
    if len(stop_trials > 0):
        new_stop = stop_trials[0] + 100
        print ("New stop at {:d}".format(new_stop))
        best_new_valid_accuracy =1 - np.min(valid_misclass[:new_stop])
        best_new_valid_nll= 1 - np.min(valid_nll[:new_stop])
        print ("Diff in accuracy (change for new stop) {:5.2f}".format(
            (best_new_valid_accuracy - best_valid_accuracy) * 100))
        print ("Diff in valid nll (change for new stop) {:f}".format(
            (best_new_valid_nll - best_valid_nll)*10000))
    else:
        print("No stop in new version after {:d} trials".format(compare_point))
    #pyplot.plot(diff_valid_train[::])
    #pyplot.axhline(y=np.mean(diff_valid_train), linestyle=":")


# In[180]:

train_nll = np.array(gujo_result.monitor_channels['train_y_nll'].val_record)
valid_nll = np.array(gujo_result.monitor_channels['valid_y_nll'].val_record)

#diff_valid_train = np.diff(valid_nll - train_nll)
diff_valid_train = valid_nll - train_nll

pyplot.plot(diff_valid_train[::20][::-1])
#after_valid_stop = np.flatnonzero(diff_valid_train[200:] < 0.01)[0]
#compare_point = int(0.9 * after_valid_stop)
#print np.argmin(gujo_result.monitor_channels['valid_y_nll'].val_record[:compare_point])
#print gujo_result.monitor_channels['valid_y_misclass'].val_record[4965]
#print np.min(gujo_result.monitor_channels['valid_y_misclass'].val_record[:compare_point])

pyplot.axhline(y=np.mean(diff_valid_train), linestyle=":")


# In[214]:

from motor_imagery_learning.preprocessing_util import exponential_running_mean, exponential_running_var

exp_mean_diff_valid_nll = exponential_running_mean(diff_valid_nll, factor_new=0.01, init_block_size=20)
exp_var_diff_valid_nll = exponential_running_var(diff_valid_nll, exp_mean_diff_valid_nll,
                                                 factor_new=0.01, init_block_size=20)
pyplot.plot(exp_mean_diff_valid_nll[::20])
pyplot.axhline(color='k', linestyle=":")


# In[82]:

pyplot.plot(exp_var_diff_valid_nll)


# In[215]:

var_adjusted = exp_mean_diff_valid_nll / np.sqrt(exp_var_diff_valid_nll)


# In[216]:

pyplot.plot(var_adjusted[::20])
pyplot.axhline(color='k', linestyle=":")
pyplot.axhline(y=0.05, color='red', linestyle=":")


# In[104]:

pyplot.plot(var_adjusted[700:1200])
pyplot.axhline(y=0.05, color='red', linestyle=":")


# In[217]:

np.flatnonzero(var_adjusted > 0.05)[0]


# In[126]:

pyplot.plot(gujo_result.monitor_channels['valid_y_misclass'].val_record[450:550])


# In[135]:

pyplot.plot(gujo_result.monitor_channels['valid_y_misclass'].val_record[3900:4000])


# In[115]:

gujo_result.monitor_channels['valid_y_misclass'].val_record[-1]


# In[93]:

pyplot.plot(var_adjusted[::20][250:300])
pyplot.axhline(color='k', linestyle=":")

