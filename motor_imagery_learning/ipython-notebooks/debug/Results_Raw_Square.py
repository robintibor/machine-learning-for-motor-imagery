
# coding: utf-8

# In[1]:

import os

import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# make sure to use cpu for printing results...
os.environ['THEANO_FLAGS'] = "floatX=float32,device=cpu,nvcc.fastmath=True"
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7
from analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file
from motor_imagery_learning.analysis.print_results import ResultPrinter
from motor_imagery_learning.csp.print_csp_results import CSPResultPrinter
from pylearn2.utils import serial
import numpy as np
from motor_imagery_learning.analysis.plot_util import (plot_nlls_for_file, 
    plot_misclasses_for_result, plot_misclasses_for_file, plot_nlls)
from glob import glob
import seaborn
seaborn.set_style('darkgrid')
seaborn.set_context("notebook", rc={"lines.linewidth": 0.5})


# In[2]:

get_ipython().run_cell_magic(u'javascript', u'', u'$(\'body\').append(\'<script src="https://dl.dropboxusercontent.com/u/34637013/work/ipython-notebooks/js-for-table-sorting/jquery.tablesorter.min.js"></script>\')\n// Sort tables every 5 seconds...\nvar makeTablesSortable;\nmakeTablesSortable = function() { \n  $(\'.text_cell_render table\').not(\'.sorted_table\').tablesorter().addClass(\'sorted_table\');\n}\n\nsetInterval(makeTablesSortable, 5000);')


# ## Open Comparisons

# 
# * data/models/debug/raw-square/misclass_stop_no_relu/
# * data/models/raw-square/300-fs/large-kernel/larger-pools-chan-freq-standardize/

# ### TODO 

# * power 4 instead of square?
# * like csp net with chan height dot? 
# 

# ## Analysis

# ### 300 fs
# * without demeaning large pool 100 also 93.2%
# * 30 kernel: same as before 89.7%, 2nd layer with 2nd pool shape 10 88.6%, with large pool 100 93.2%
# * 15 kernel: same as before 90.5%, 2nd layer with 2nd pool shape 10 90.9%, with large pool 100 93.5%
# 
# ### 150 fs stuffs
# * spatial before ~87%
# * nonlin first layer: with abs 81.15% after 6, with abs and then log 26% after 2
# * data augment with var 0.0001 93.55, with var 0.01 93.25%, with var 0.04 92.44%
# * mean log pool 92.3%
# * kernel shape 30 93%, 45 90.5%
# * stop on misclass ~92.2% 30 minutes (2 times faster than 1 hour before)
# * softmax irange ~91.5%
# * square plus abs nonlin ~91.5%
# * mean pool 83 %
# * identity no relu 40 spatial filters 93.5%
# * mean pool only 83% after 16
# * like csp mean pool ~72% after 2... stopped cause very slow
# * identity bp faster stop no switch 93.0% for unknown reasons
# * identity bp faster stop 92.5% (in about half the time)
# * identity bp 92.9%
# * sine init with relu 90.8%
# * sine init no relu 88%
# * raw square faster stop 89%
# * like csp without full height 70% after 18, like csp 71% after 7 with sine init 70% after 6
# * no relu chan only freq conv (with sine init) not good (~75% after 10)
# * no relu chan only (with sine init) not good (~76% after 18)
# * max and other pool does not work so well (best only 83% on first5)
# * root mean large pool similar, 85% for 9 files, then cancelled
# * larger pool (shape 150, stride 75) slightly worse (~86%)
# * common average reference almost exactly same (~90.1%)
# * max pool is worse (~81%)
# * without relu is almost as good as best (~-1%, 89.3%)
# * chan only no relu does not work properly (~73%)
# * chan only with relu completely bad (~30%)
# * ~1% percent ahead of smaller csp net for 150fs.. however smaller csp net alrdy cut at 50hz(!) and 150 fs implies up to 75 Hz are usable (nyquist freq)
# * 40 bp chans seems best (around 89%)...

# ## Results 

# In[5]:

ResultPrinter('data/models/final-eval/small/raw-net/early-stop/').print_results(print_individual_datasets=False)


# In[5]:

ResultPrinter('data/models/final-eval/raw-net/early-stop/').print_results(print_individual_datasets=False)


# In[210]:

ResultPrinter('data/models/final-eval/raw-net-150-fs-with-large-kernel//early-stop/').print_results(
print_individual_datasets=False)


# In[186]:

ResultPrinter('data/models/raw-square/300-fs/large-kernel/larger-pools-chan-freq-standardize/early-stop/').print_results(
)


# In[183]:

ResultPrinter('data/models/raw-square/300-fs/large-kernel/larger-pools/early-stop/').print_results()


# In[164]:

ResultPrinter('data/models/debug/raw-square/car/identity-bp-faster-stop-no-relu/early-stop/').print_results()


# In[163]:

ResultPrinter('data/models/raw-square/300-fs/large-kernel/larger-pools-no-demean/early-stop/').print_results()


# In[159]:

ResultPrinter('data/models/raw-square/300-fs/large-kernel/larger-pools/early-stop/').print_results(
    print_individual_datasets=True)


# In[160]:

ResultPrinter('data/models/raw-square/300-fs/larger-pools/early-stop/').print_results(
    print_individual_datasets=False)

