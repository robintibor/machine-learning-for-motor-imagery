
# coding: utf-8

# In[1]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# switch to cpu
os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
from matplotlib import cm
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from motor_imagery_learning.analysis.plot_util import plot_class_probs, plot_chan_matrices, plot_class_reconstruction,     plot_correct_part, plot_incorrect_part, plot_correctness, plot_misclasses_for_file, plot_sensor_signals
from  motor_imagery_learning.analysis.util import back_deconv, back_prop, back_guided_deconv, back_linear, bc01toinput,     load_preprocessed_datasets, back_guided_deconv_for_class, reshape_and_get_predictions,     get_trials_for_class, get_summed_activations, get_activations, get_activation,     check_prediction_correctness, load_train_test_datasets
from pylearn2.config import yaml_parse
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted, sort_topologically
from motor_imagery_learning.analysis.print_results import ResultPrinter
from copy import deepcopy
figsize=(13,8)
import psutil
import h5py
from scipy.io import loadmat
from pprint import pprint
from wyrm.types import Data
import wyrm
from motor_imagery_learning.preprocessing_util import (exponential_running_mean,
                                                       exponential_running_var)
import scipy.io
from motor_imagery_learning.bbci_dataset import BBCIDataset
from wyrm.processing import  select_ival
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.csp.print_csp_results import CSPResultPrinter


# In[4]:

get_ipython().run_cell_magic(u'javascript', u'', u'$(\'body\').append(\'<script src="https://dl.dropboxusercontent.com/u/34637013/work/ipython-notebooks/js-for-table-sorting/jquery.tablesorter.min.js"></script>\')\n// Sort tables every 5 seconds...\nvar makeTablesSortable;\nmakeTablesSortable = function() { \n  $(\'.text_cell_render table\').not(\'.sorted_table\').tablesorter().addClass(\'sorted_table\');\n}\n\nsetInterval(makeTablesSortable, 5000);')


# ##Open Comparison

# * ...

# ## TODO

# * with car
# * plus 2 features minus 1 feature logic, and/or going until num features but then taking first or last feature combination that maximized internal crossval test accuracy

# ## Analysis

# * 
# * going always until num features minimally better 88.60% with 20 selected filterbands or 88.46% with all filterbands plus 80 features (however this is around 7 times, 4 minutes to 28 minutes, slower)
# * (all filterbands + 10 or 20 or 40 features(doesnt matter)) almost as good (-0.41%)
# * selecting 20 filterbands and selecting 20 features is best (88.34%) when only adding features until no improvement during feature selection 

# In[148]:

CSPResultPrinter('data/models/final-eval/small/csp/', only_last_fold=True).print_results(
    print_templates_and_constants=False, print_individual_datasets=False)


# In[147]:

CSPResultPrinter('data/models/final-eval/csp-standardized/', only_last_fold=True).print_results(
    print_templates_and_constants=False, print_individual_datasets=False)


# In[137]:

CSPResultPrinter('data/models/final-eval/csp/', only_last_fold=True).print_results(
    print_templates_and_constants=False, print_individual_datasets=False)


# In[116]:

CSPResultPrinter('data/models/final-eval/csp-slow/', only_last_fold=True).print_results(
    print_templates_and_constants=False, print_individual_datasets=False)


# In[74]:

CSPResultPrinter('data/models/final-eval/csp/', only_last_fold=True).print_results(
    print_templates_and_constants=False, print_individual_datasets=False)


# In[92]:

CSPResultPrinter('data/models/csp/corr-ival/', only_last_fold=True).print_results(
    print_templates_and_constants=False, print_individual_datasets=False)


# In[93]:

CSPResultPrinter('data/models/csp/auc-ival/', only_last_fold=True).print_results(
    print_templates_and_constants=False, print_individual_datasets=False)


# In[3]:

CSPResultPrinter('data/models/csp/retrain-full-new/', only_last_fold=True).print_results(
    print_templates_and_constants=False, print_individual_datasets=False)


# 
# Markdown Table
# 
# |id|files|sel_feat|for|back|stop_improve|sel_filt|time|std|test|std|train|std|
# |-|-|-|-|-|-|-|-|-|-|-|-|-|
# |0|18|10|1|0|False|None|0:01:26|0:00:06|85.21%|9.64%|99.59%|0.70%|
# |1|18|10|2|1|False|None|0:02:52|0:00:10|85.17%|9.77%|99.60%|0.66%|
# |2|18|10|4|2|False|None|0:03:39|0:00:14|86.47%|9.39%|99.70%|0.51%|
# |3|18|10|1|0|False|10|0:00:33|0:00:03|83.64%|10.83%|99.51%|0.75%|
# |4|18|10|2|1|False|10|0:01:00|0:00:05|84.60%|11.75%|99.55%|0.75%|
# |5|18|10|4|2|False|10|0:01:15|0:00:07|84.55%|12.37%|99.71%|0.47%|
# |6|18|10|1|0|False|20|0:00:54|0:00:04|87.28%|8.14%|99.58%|0.62%|
# |7|18|10|2|1|False|20|0:01:44|0:00:07|87.95%|9.34%|99.66%|0.60%|
# |8|18|10|4|2|False|20|0:02:11|0:00:09|88.28%|9.36%|99.75%|0.43%|
# |9|18|10|1|0|True|None|0:01:08|0:00:13|86.89%|9.27%|99.53%|0.68%|
# |10|18|10|2|1|True|None|0:02:12|0:00:26|86.58%|10.74%|99.56%|0.64%|
# |11|18|10|4|2|True|None|0:03:09|0:00:28|86.38%|9.46%|99.68%|0.51%|
# |12|18|10|1|0|True|10|0:00:28|0:00:04|84.70%|10.80%|99.43%|0.77%|
# |13|18|10|2|1|True|10|0:00:49|0:00:08|83.99%|11.06%|99.47%|0.73%|
# |14|18|10|4|2|True|10|0:01:08|0:00:10|84.54%|11.92%|99.70%|0.46%|
# |15|18|10|1|0|True|20|0:00:43|0:00:08|88.18%|8.21%|99.49%|0.60%|
# |16|18|10|2|1|True|20|0:01:22|0:00:16|87.41%|8.91%|99.52%|0.59%|
# |17|18|10|4|2|True|20|0:01:55|0:00:18|88.19%|8.95%|99.70%|0.41%|
# |18|18|20|1|0|False|None|0:03:07|0:00:12|84.78%|9.97%|99.86%|0.34%|
# |19|18|20|2|1|False|None|0:06:31|0:00:25|83.13%|10.90%|99.88%|0.22%|
# |20|18|20|4|2|False|None|0:06:45|0:00:27|85.59%|9.03%|99.94%|0.12%|
# |21|18|20|1|0|False|10|0:01:00|0:00:05|84.34%|11.30%|99.93%|0.17%|
# |22|18|20|2|1|False|10|0:02:07|0:00:10|84.03%|12.77%|99.91%|0.16%|
# |23|18|20|4|2|False|10|0:02:12|0:00:11|83.82%|12.87%|99.92%|0.16%|
# |24|18|20|1|0|False|20|0:01:50|0:00:08|85.72%|10.54%|99.86%|0.30%|
# |25|18|20|2|1|False|20|0:03:52|0:00:16|86.94%|10.29%|99.90%|0.21%|
# |26|18|20|4|2|False|20|0:04:00|0:00:17|88.21%|8.98%|99.93%|0.13%|
# |27|18|20|1|0|True|None|0:01:15|0:00:20|86.71%|9.94%|99.63%|0.41%|
# |28|18|20|2|1|True|None|0:02:27|0:00:44|86.43%|11.08%|99.67%|0.45%|
# |29|18|20|4|2|True|None|0:03:40|0:01:01|85.86%|9.53%|99.85%|0.17%|
# |30|18|20|1|0|True|10|0:00:30|0:00:06|84.81%|11.02%|99.55%|0.56%|
# |31|18|20|2|1|True|10|0:00:54|0:00:14|85.02%|10.94%|99.61%|0.53%|
# |32|18|20|4|2|True|10|0:01:19|0:00:21|85.03%|11.13%|99.84%|0.16%|
# |33|18|20|1|0|True|20|0:00:47|0:00:12|87.86%|9.17%|99.51%|0.55%|
# |34|18|20|2|1|True|20|0:01:30|0:00:27|88.14%|8.52%|99.59%|0.45%|
# |35|18|20|4|2|True|20|0:02:15|0:00:40|87.84%|9.23%|99.81%|0.22%|
# |36|18|40|1|0|False|None|0:08:13|0:00:34|86.90%|9.01%|99.98%|0.07%|
# |37|18|40|2|1|False|None|0:17:54|0:01:17|86.79%|10.17%|99.98%|0.05%|
# |38|18|40|4|2|False|None|0:18:30|0:01:21|87.31%|10.08%|99.98%|0.07%|
# |39|18|40|1|0|False|10|0:02:13|0:00:10|85.81%|12.67%|99.98%|0.05%|
# |40|18|40|2|1|False|10|0:05:26|0:00:27|85.90%|12.06%|99.98%|0.06%|
# |41|18|40|4|2|False|10|0:05:36|0:00:28|85.23%|12.84%|99.96%|0.09%|
# |42|18|40|1|0|False|20|0:04:37|0:00:20|88.60%|10.00%|99.98%|0.06%|
# |43|18|40|2|1|False|20|0:10:24|0:00:47|88.94%|9.52%|99.98%|0.06%|
# |44|18|40|4|2|False|20|0:10:44|0:00:48|88.39%|9.74%|99.98%|0.06%|
# |45|18|40|1|0|True|None|0:01:15|0:00:21|86.71%|9.94%|99.63%|0.41%|
# |46|18|40|2|1|True|None|0:02:27|0:00:44|86.43%|11.08%|99.67%|0.45%|
# |47|18|40|4|2|True|None|0:03:45|0:01:12|85.94%|9.35%|99.86%|0.15%|
# |48|18|40|1|0|True|10|0:00:30|0:00:06|84.81%|11.02%|99.57%|0.53%|
# |49|18|40|2|1|True|10|0:00:54|0:00:15|85.02%|10.94%|99.61%|0.53%|
# |50|18|40|4|2|True|10|0:01:21|0:00:24|84.71%|11.82%|99.84%|0.16%|
# |51|18|40|1|0|True|20|0:00:47|0:00:12|87.86%|9.17%|99.51%|0.55%|
# |52|18|40|2|1|True|20|0:01:29|0:00:26|88.14%|8.52%|99.59%|0.45%|
# |53|18|40|4|2|True|20|0:02:17|0:00:44|87.86%|9.33%|99.80%|0.24%|
# |54|18|80|1|0|False|None|0:27:16|0:01:46|88.46%|9.53%|100.00%|0.00%|
# |55|18|80|2|1|False|None|1:02:05|0:04:16|87.93%|10.98%|100.00%|0.00%|
# |56|18|80|4|2|False|None|1:03:32|0:04:17|88.87%|9.32%|100.00%|0.00%|
# |57|18|80|1|0|False|10|0:05:07|0:00:25|87.24%|10.31%|99.99%|0.03%|
# |58|18|80|2|1|False|10|0:16:22|0:01:15|87.33%|11.40%|100.00%|0.00%|
# |59|18|80|4|2|False|10|0:16:43|0:01:17|88.06%|10.39%|100.00%|0.00%|
# |60|18|80|1|0|False|20|0:13:57|0:00:57|88.64%|10.55%|100.00%|0.00%|
# |61|18|80|2|1|False|20|0:34:40|0:02:30|89.34%|10.57%|100.00%|0.00%|
# |62|18|80|4|2|False|20|0:35:27|0:02:31|89.60%|9.49%|100.00%|0.00%|
# |63|18|80|1|0|True|None|0:01:15|0:00:21|86.71%|9.94%|99.63%|0.41%|
# |64|18|80|2|1|True|None|0:02:28|0:00:44|86.43%|11.08%|99.67%|0.45%|
# |65|18|80|4|2|True|None|0:03:46|0:01:13|85.94%|9.35%|99.86%|0.15%|
# |66|18|80|1|0|True|10|0:00:30|0:00:07|84.81%|11.02%|99.57%|0.53%|
# |67|18|80|2|1|True|10|0:00:55|0:00:15|85.02%|10.94%|99.61%|0.53%|
# |68|18|80|4|2|True|10|0:01:21|0:00:24|84.71%|11.82%|99.84%|0.16%|
# |69|18|80|1|0|True|20|0:00:47|0:00:12|87.86%|9.17%|99.51%|0.55%|
# |70|18|80|2|1|True|20|0:01:30|0:00:27|88.14%|8.52%|99.59%|0.45%|
# |71|18|80|4|2|True|20|0:02:17|0:00:45|87.86%|9.33%|99.80%|0.24%|

# In[2]:

CSPResultPrinter('data/models/csp/retrain-full-new/').print_results(
    print_templates_and_constants=False, print_individual_datasets=False)


# In[9]:

CSPResultPrinter('data/models/csp/retrain-full-until-num-features/old/', only_last_fold=True).print_results(print_templates_and_constants=False,
                                                                                     print_individual_datasets=False)


# In[63]:

CSPResultPrinter('data/models/csp/retrain-full/', only_last_fold=True).print_results(print_templates_and_constants=False,
                                                                                     print_individual_datasets=False)


# In[55]:

CSPResultPrinter('data/models/csp/full/', only_last_fold=True).print_results()


# In[14]:

CSPResultPrinter('data/models/csp/retrain-short-bands/').print_results()


# In[12]:

CSPResultPrinter('data/models/csp/short-bands/', only_last_fold=True).print_results()

