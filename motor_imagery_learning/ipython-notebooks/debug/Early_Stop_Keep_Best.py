
# coding: utf-8

# In[1]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# switch to cpu
os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from pylearn2.training_algorithms.sgd import SGD 
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
from motor_imagery_learning.mypylearn2.learning_rule_adam import Adam
from pylearn2.format.target_format import OneHotFormatter
from copy import deepcopy
from numpy.random import RandomState


# In[2]:

rng = RandomState(np.uint64(hash('testearlystop')))
num_trials = 300
y = OneHotFormatter(4).format(rng.random_integers(0,3,size=num_trials))
topo_view = np.ones((num_trials,10,2,3), dtype=np.float32) #10,2,3
topo_view[y[:,0] == 1] = 0 # make it atleast a little learnable :)

dataset = DenseDesignMatrix(topo_view = topo_view, y=y, axes=('b', 'c', 0, 1))


# In[3]:

softmax_layer = Softmax(n_classes=4, layer_name='y', irange=0.0001, max_col_norm=2.)
model = MLP([softmax_layer])
sgd_algo = SGD(batch_size=5,cost=Default(),termination_criterion=EpochCounter(max_epochs=5),
               learning_rate=0.1, seed=[2012, 10, 16], learning_rule=Adam())
trainer = TrainEarlyStop(dataset, model, algorithm=sgd_algo, preprocessor=OnlineStandardize())


# In[5]:

#trainer.main_loop()


# In[4]:

trainer._remember_original_algorithm()
trainer._create_early_stop_trainer()


# In[5]:

trainer.early_stop_trainer.main_loop()


# In[6]:

trainer._remember_best_params()
trainer._remove_early_stop_trainer()


# In[7]:

old_grad = deepcopy(trainer.best_learning_rule.grad)
old_sq_grad = deepcopy(trainer.best_learning_rule.sq_grad)


# In[8]:

old_grad.values()[0].get_value()


# In[9]:

trainer.best_learning_rule.time.get_value() # 97, from currentmodel is 289


# In[10]:

trainer._create_and_setup_after_stop_trainer()


# In[11]:

old_grad.values()[0].get_value()


# In[12]:

trainer.algorithm.learning_rule.grad.values()[0].get_value()


# In[13]:

trainer.algorithm.learning_rule.sq_grad.values()[0].get_value()


# In[87]:

trainer.algorithm.learning_rule.time.get_value()


# In[15]:

trainer.after_stop_trainer.run_training()


# In[31]:

trainer.after_stop_trainer.model.monitor.channels['valid_y_misclass'].val_record


# In[12]:

inner_trainer = trainer._construct_train_object()


# In[13]:

inner_trainer.setup()


# In[45]:

from copy import deepcopy
old_learn_rule = deepcopy(inner_trainer.algorithm.learning_rule)


# In[37]:

old_learn_rule.grad.values()[0].get_value()


# In[40]:

inner_trainer.algorithm.learning_rule.grad.values()[0].get_value()


# In[57]:

inner_trainer.algorithm.learning_rule.time.get_value()


# In[58]:

trainer._adapt_training_after_early_stop(inner_trainer)


# In[51]:

inner_trainer.algorithm.learning_rule = old_learn_rule


# In[62]:

inner_trainer.algorithm.learning_rule.time.get_value()


# In[63]:

inner_trainer.setup()


# In[61]:

inner_trainer.run_one_epoch()


# In[20]:


inner_trainer.setup()


# In[5]:

inner_trainer.main_loop()

