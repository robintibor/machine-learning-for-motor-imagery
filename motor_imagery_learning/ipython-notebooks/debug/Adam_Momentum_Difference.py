
# coding: utf-8

# In[1]:

import os

import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# make sure to use cpu for printing results...
os.environ['THEANO_FLAGS'] = "floatX=float32,device=cpu,nvcc.fastmath=True"
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7
from analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file
from motor_imagery_learning.analysis.print_results import ResultPrinter
from pylearn2.utils import serial
import numpy as np
from motor_imagery_learning.analysis.plot_util import (plot_nlls_for_file, 
    plot_misclasses, plot_misclasses_for_file, plot_nlls)


# In[2]:

get_ipython().run_cell_magic(u'javascript', u'', u'$(\'body\').append(\'<script src="https://dl.dropboxusercontent.com/u/34637013/work/ipython-notebooks/js-for-table-sorting/jquery.tablesorter.min.js"></script>\')\n// Sort tables every 5 seconds...\nvar makeTablesSortable;\nmakeTablesSortable = function() { \n  $(\'.text_cell_render table\').not(\'.sorted_table\').tablesorter().addClass(\'sorted_table\');\n}\n\nsetInterval(makeTablesSortable, 5000);')


# In[28]:

result_momentum = serial.load('data/models/debug/bandpower-adam//early-stop/first-5/2.result.pkl')
result_adam = serial.load('data/models/debug/bandpower-adam//early-stop/first-5/7.result.pkl')
test_misclass_diff = np.array(result_momentum.get_misclasses()['test'][:202]) - np.array(result_adam.get_misclasses()['test'])
valid_misclass_diff = np.array(result_momentum.get_misclasses()['valid'][:202]) - np.array(result_adam.get_misclasses()['valid'])
train_misclass_diff = np.array(result_momentum.get_misclasses()['train'][:202]) - np.array(result_adam.get_misclasses()['train'])


# In[3]:

ResultPrinter('data/models/debug/raw-data-best-stop-late//early-stop/').print_results(print_templates_and_constants=False)


# In[4]:

ResultPrinter('data/models/debug/raw-data-best-stop-late-keep-adam/early-stop/').print_results(print_templates_and_constants=False)


# In[126]:

ResultPrinter('data/models/best/raw-data/early-stop/').print_results(print_templates_and_constants=False)


# In[78]:

ResultPrinter('data/models/debug/raw-data-running-early-stop/early-stop/').print_results(print_templates_and_constants=False)


# ## Bandpower results 

# In[70]:

ResultPrinter('data/models/debug/bandpower-1-learning-rate//early-stop/').print_results(print_templates_and_constants=False)


# In[81]:

ResultPrinter('data/models/best//bandpower/early-stop/').print_results(print_templates_and_constants=False)


# In[84]:

ResultPrinter('data/models/debug/bandpower-running-min//early-stop/').print_results(print_templates_and_constants=False)


# In[59]:

ResultPrinter('data/models/debug/bandpower-new-best//early-stop/').print_results(print_templates_and_constants=False)


# In[60]:

ResultPrinter('data/models/debug/bandpower-best-stop/early-stop/').print_results(print_templates_and_constants=False)


# In[6]:

ResultPrinter('data/models/debug/bandpower-full-vs-2-50/early-stop/').print_results(print_templates_and_constants=True)


# In[ ]:

plot_misclasses


# In[7]:

ResultPrinter('data/models/best/bandpower/early-stop/').print_results(print_templates_and_constants=False)


# In[62]:

ResultPrinter('data/models/debug/bandpower-best-stop-late/early-stop/first-5/').print_results(print_templates_and_constants=False)

