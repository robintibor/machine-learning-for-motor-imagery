
# coding: utf-8

# In[1]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# switch to cpu
#os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
from pylearn2.format.target_format import OneHotFormatter
from motor_imagery_learning.mypylearn2.conv_squared import ConvSquared
from motor_imagery_learning.analysis.print_results import ResultPrinter
from motor_imagery_learning.train_scripts.train_with_params import create_training_object_from_file
import theano.misc.pycuda_init 
import pycuda.driver


# ## Investigate graphic card memory

# In[2]:

print "Total", pycuda.driver.mem_get_info()[1] / (1024 ** 2.0)


# With batch size 100
# total: 3071
# 
# * at start:        2982
# * after train obj: 2982
# * after dataset:   2982
# * after remember alg: 2982
# * after create earlystop trainer:  2960
# * after earlystop trainer setup:  2950/2972
# * after earlystop run training: 2972
# * after remember best params: 2972
# * after removing early stop trainer: 2972
# * after create and setup after stop trainer: 2970
# * after crash after stop trainer: 974

# In[3]:

print "Free", pycuda.driver.mem_get_info()[0] / (1024 ** 2.0)


# In[4]:

train_obj = create_training_object_from_file('data/models/debug/csp-net-crash/early-stop/1.yaml')


# In[6]:

train_obj.algorithm.batch_size = 60


# In[7]:

print "Free", pycuda.driver.mem_get_info()[0] / (1024 ** 2.0)


# In[8]:


input_shape = (766,45,1040,35)
y = OneHotFormatter(4).format(np.random.random_integers(0,3,size=input_shape[0]))
# important that topo view not created here as variable as we want to free its memory later
# by deleting dataset.X!
dataset = DenseDesignMatrix(topo_view = np.ones(input_shape, dtype=np.float32), y=y, axes=('b', 'c', 0, 1))


# In[9]:

print "Free", pycuda.driver.mem_get_info()[0] / (1024 ** 2.0)


# In[10]:


# Replace dataset in train_obj with fake dataset same dims
# 766 x 45 x 1050 x 35
train_obj.dataset = dataset


# In[11]:

train_obj._remember_original_algorithm()


# In[12]:

print "Free", pycuda.driver.mem_get_info()[0] / (1024 ** 2.0)


# In[13]:

train_obj._create_early_stop_trainer()


# In[14]:

print "Free", pycuda.driver.mem_get_info()[0] / (1024 ** 2.0)


# In[15]:

get_ipython().run_cell_magic(u'capture', u'', u'train_obj.early_stop_trainer.setup()')


# In[16]:

print "Free", pycuda.driver.mem_get_info()[0] / (1024 ** 2.0)


# In[17]:

get_ipython().run_cell_magic(u'capture', u'', u'train_obj.early_stop_trainer.run_training()')


# In[18]:

print "Free", pycuda.driver.mem_get_info()[0] / (1024 ** 2.0)


# In[19]:

train_obj._remember_best_params()


# In[20]:

print "Free", pycuda.driver.mem_get_info()[0] / (1024 ** 2.0)


# In[21]:

train_obj._remove_early_stop_trainer()


# In[22]:

print "Free", pycuda.driver.mem_get_info()[0] / (1024 ** 2.0)


# In[23]:

get_ipython().run_cell_magic(u'capture', u'', u'train_obj._create_and_setup_after_stop_trainer()')


# In[24]:

print "Free", pycuda.driver.mem_get_info()[0] / (1024 ** 2.0)


# In[25]:

train_obj.after_stop_trainer.algorithm.batch_size


# In[36]:

train_obj.after_stop_trainer.run_training()


# ## SGD reconstruction to see where failure happens
# -> happens because square is not inplace... computations make sense that it is having problems

# In[ ]:

### SGD train reconstruction
dataset= train_obj.after_stop_trainer.dataset
self = train_obj.after_stop_trainer.algorithm
from motor_imagery_learning.mypylearn2.sgd import *


# In[31]:

print "Free", pycuda.driver.mem_get_info()[0] / (1024 ** 2.0)


# In[26]:

# Make sure none of the parameters have bad values
for param in self.params:
    value = param.get_value(borrow=True)
    if not isfinite(value):
        raise Exception("NaN in " + param.name)


# In[28]:

print "Free", pycuda.driver.mem_get_info()[0] / (1024 ** 2.0)


# In[32]:

self.first = False
rng = self.rng
if not is_stochastic(self.train_iteration_mode):
    rng = None

data_specs = self.cost.get_data_specs(self.model)


# In[33]:

print "Free", pycuda.driver.mem_get_info()[0] / (1024 ** 2.0)


# In[34]:

# The iterator should be built from flat data specs, so it returns
# flat, non-redundent tuples of data.
mapping = DataSpecsMapping(data_specs)
space_tuple = mapping.flatten(data_specs[0], return_tuple=True)
source_tuple = mapping.flatten(data_specs[1], return_tuple=True)
if len(space_tuple) == 0:
    # No data will be returned by the iterator, and it is impossible
    # to know the size of the actual batch.
    # It is not decided yet what the right thing to do should be.
    raise NotImplementedError(
        "Unable to train with SGD, because "
        "the cost does not actually use data from the data set. "
        "data_specs: %s" % str(data_specs))
flat_data_specs = (CompositeSpace(space_tuple), source_tuple)


# In[35]:

print "Free", pycuda.driver.mem_get_info()[0] / (1024 ** 2.0)


# In[36]:

assert self.batches_per_iter is None,             "Expecting batchees per iter to be none, " +             "since we ignore the parameter"

iterator = create_iterator(dataset,
    batch_size=self.batch_size, data_specs=flat_data_specs,
    return_tuple=True, rng=rng)


# In[39]:

print "Free", pycuda.driver.mem_get_info()[0] / (1024 ** 2.0)


# In[38]:

on_load_batch = self.on_load_batch


# In[40]:

batchlist = list(iterator)


# In[41]:

print "Free", pycuda.driver.mem_get_info()[0] / (1024 ** 2.0)


# In[43]:

batch = batchlist[0]


# In[44]:

for callback in on_load_batch:
    callback(*batch)


# In[45]:

print "Free", pycuda.driver.mem_get_info()[0] / (1024 ** 2.0)


# In[46]:

self.sgd_update(*batch)


# In[47]:

pycuda.driver.mem_get_info()[0] / (1024 ** 2.0)


# In[35]:

batch_size = 100.0
dataset_size = 614
print batch_size + np.ceil(batch_size / (dataset_size // batch_size)) # worst case for similar size, not actually happening
print np.floor(dataset_size / (dataset_size // batch_size)) + 1 # actual case


# In[103]:

batch_size = 103#77
sensors = 45#128
print(4 * batch_size * sensors * 1050 * 35 / 1024.0 ** 2 + 4 * batch_size * 80 * 1050 * 35 / 1024.0 ** 2 + 
      4 * batch_size * 80 * 1050 * 35 / 1024.0 ** 2)


# In[73]:

train_obj.after_stop_trainer.model.layers


# In[31]:

print "Free", pycuda.driver.mem_get_info()[0] / (1024 ** 2.0)


# In[ ]:

del train_obj
del dataset
del y


# In[32]:

import gc
gc.collect()


# In[41]:

print "Free", pycuda.driver.mem_get_info()[0] / (1024 ** 2.0)


# In[40]:

whos

