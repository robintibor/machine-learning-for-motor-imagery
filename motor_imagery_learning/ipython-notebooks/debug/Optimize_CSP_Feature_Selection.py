
# coding: utf-8

# In[ ]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\n\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n#site.force_global_eggs_after_local_site_packages()\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n# make sure to use cpu for csp...\nos.environ[\'THEANO_FLAGS\'] = "floatX=float32,device=cpu,nvcc.fastmath=True"\n%load_ext autoreload\n%autoreload 2\nimport matplotlib\nfrom matplotlib import pyplot\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \n#matplotlib.rcParams[\'figure.figsize\'] = (12.0, 2.0)\nmatplotlib.rcParams[\'font.size\'] = 7\nfrom analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file\nfrom motor_imagery_learning.analysis.print_results import ResultPrinter\nfrom motor_imagery_learning.csp.print_csp_results import CSPResultPrinter\nfrom pylearn2.utils import serial\nimport numpy as np\nfrom motor_imagery_learning.analysis.plot_util import (plot_nlls_for_file, \n    plot_misclasses_for_result, plot_misclasses_for_file, plot_nlls)\nfrom motor_imagery_learning.analysis.results import ResultPool, DatasetAveragedResults\nfrom glob import glob\nimport datetime\nimport seaborn\nseaborn.set_style(\'darkgrid\')\nseaborn.set_context("notebook", rc={"lines.linewidth": 0.5})\nmatplotlib.rcParams[\'figure.figsize\'] = (8.0, 2.0)')


# In[153]:

get_ipython().run_cell_magic(u'javascript', u'', u'$(\'body\').append(\'<script src="https://dl.dropboxusercontent.com/u/34637013/work/ipython-notebooks/js-for-table-sorting/jquery.tablesorter.min.js"></script>\')\n// Sort tables every 5 seconds...\nvar makeTablesSortable;\nmakeTablesSortable = function() { \n  $(\'.text_cell_render table\').not(\'.sorted_table\').tablesorter().addClass(\'sorted_table\');\n}\n\nsetInterval(makeTablesSortable, 5000);')


# In[86]:

from motor_imagery_learning.csp.pipeline import FilterbankCSP, BinaryCSP


# In[20]:

from wyrm.types import Data


# In[53]:

np.array(features).shape


# In[94]:

# some useful features on filt 1 and not useful features on filt2
from numpy.random import RandomState
y = np.array([0] * 10 + [1] * 10)
features = RandomState(30984938).randn(2,1,1,20,8)
#features= [[[[[0.7,-0.7],[0.8,-0.8],[0.2,-0.2],[-1,1],[-0.4,0.4],[0.5,-0.5]]]], #filt1
#           [[[[0.7,-0.7],[0.8,-0.8],[0.2,-0.2],[-1,1],[-0.4,0.4],[0.5,-0.5]]]]] #filt2
features[1,0,0][y==0] +=0.3
fake_folds = [{'train': range(0,8) + range(10,18), 'test':[8,9,18,19]}]
bin_csp = BinaryCSP(None, filterbands=[[0,2],[2,4]], filt_order=-1, folds=fake_folds, class_pairs=[[0,1]],
                    segment_ival=None, num_filters=None, ival_optimizer=None, marker_def=None)

bin_csp.train_feature = np.empty((2,1,1), dtype=object)
bin_csp.train_feature[0,0,0] = Data(data=np.array(features[0][0][0]), axes=[y,range(0, features.shape[-1])], 
                                    names=['class', 'feature'],
                                    units=['.','.'])
bin_csp.train_feature[1,0,0] = Data(data=np.array(features[1][0][0]), axes=[y,range(0, features.shape[-1])], 
                                    names=['class', 'feature'],
                                    units=['.','.'])

bin_csp.train_feature_full_fold = bin_csp.train_feature.copy()
bin_csp.test_feature = bin_csp.train_feature.copy()
bin_csp.test_feature_full_fold = bin_csp.train_feature.copy()
fb_csp = FilterbankCSP(bin_csp,num_features=10, forward_steps=2, backward_steps=1, stop_when_no_improvement=True)


# In[ ]:

FilterbankCSP()


# In[95]:

fb_csp.run()


# In[96]:

fb_csp.selected_filters_per_filterband


# In[105]:

CSPResultPrinter('data/models/csp/retrain-full-until-num-features/old/', only_last_fold=True).print_results(
    print_templates_and_constants=False, print_individual_datasets=False)


# In[110]:

CSPResultPrinter('data/models/csp/retrain-full-until-num-features/', only_last_fold=True).print_results(
    print_templates_and_constants=False, print_individual_datasets=False)


# In[111]:

CSPResultPrinter('data/models/csp/retrain-full-until-num-features/old/').print_results(
    print_templates_and_constants=False, print_individual_datasets=False)


# In[164]:

CSPResultPrinter('data/models/csp/retrain-full-new/').print_results(
    print_templates_and_constants=False, print_individual_datasets=False)


# 
# |id|files|sel_feat|for|back|stop_improve|sel_filt|time|std|test|std|train|std|
# |-|-|-|-|-|-|-|-|-|-|-|-|-|
# |0|18|10|1|0|False|None|0:01:26|0:00:06|86.16%|9.60%|99.54%|0.59%|
# |1|18|10|2|1|False|None|0:02:52|0:00:10|85.84%|9.59%|99.60%|0.53%|
# |2|18|10|4|2|False|None|0:03:39|0:00:14|86.28%|9.39%|99.71%|0.40%|
# |3|18|10|1|0|False|10|0:00:33|0:00:03|85.77%|10.68%|99.51%|0.63%|
# |4|18|10|2|1|False|10|0:01:00|0:00:05|85.74%|10.35%|99.58%|0.54%|
# |5|18|10|4|2|False|10|0:01:15|0:00:07|86.41%|10.02%|99.74%|0.37%|
# |6|18|10|1|0|False|20|0:00:54|0:00:04|86.94%|9.69%|99.58%|0.56%|
# |7|18|10|2|1|False|20|0:01:44|0:00:07|86.74%|9.88%|99.63%|0.50%|
# |8|18|10|4|2|False|20|0:02:11|0:00:09|87.70%|9.04%|99.76%|0.36%|
# |9|18|10|1|0|True|None|0:01:08|0:00:13|86.32%|9.62%|99.43%|0.59%|
# |10|18|10|2|1|True|None|0:02:12|0:00:26|86.22%|9.66%|99.49%|0.55%|
# |11|18|10|4|2|True|None|0:03:09|0:00:28|86.36%|9.23%|99.70%|0.41%|
# |12|18|10|1|0|True|10|0:00:28|0:00:04|85.63%|10.76%|99.35%|0.66%|
# |13|18|10|2|1|True|10|0:00:49|0:00:08|85.61%|10.61%|99.44%|0.55%|
# |14|18|10|4|2|True|10|0:01:08|0:00:10|86.70%|9.65%|99.70%|0.36%|
# |15|18|10|1|0|True|20|0:00:43|0:00:08|86.83%|9.71%|99.43%|0.55%|
# |16|18|10|2|1|True|20|0:01:22|0:00:16|86.75%|9.70%|99.48%|0.50%|
# |17|18|10|4|2|True|20|0:01:55|0:00:18|87.57%|8.87%|99.73%|0.35%|
# |18|18|20|1|0|False|None|0:03:07|0:00:12|85.99%|9.06%|99.88%|0.21%|
# |19|18|20|2|1|False|None|0:06:31|0:00:25|86.23%|8.68%|99.89%|0.20%|
# |20|18|20|4|2|False|None|0:06:45|0:00:27|86.82%|8.62%|99.90%|0.19%|
# |21|18|20|1|0|False|10|0:01:00|0:00:05|86.64%|9.89%|99.90%|0.17%|
# |22|18|20|2|1|False|10|0:02:07|0:00:10|86.96%|10.10%|99.91%|0.15%|
# |23|18|20|4|2|False|10|0:02:12|0:00:11|87.22%|9.61%|99.91%|0.15%|
# |24|18|20|1|0|False|20|0:01:50|0:00:08|87.70%|9.02%|99.90%|0.18%|
# |25|18|20|2|1|False|20|0:03:52|0:00:16|88.18%|9.04%|99.91%|0.16%|
# |26|18|20|4|2|False|20|0:04:00|0:00:17|88.69%|8.82%|99.90%|0.17%|
# |27|18|20|1|0|True|None|0:01:15|0:00:20|86.56%|9.31%|99.55%|0.41%|
# |28|18|20|2|1|True|None|0:02:27|0:00:44|86.37%|9.54%|99.59%|0.38%|
# |29|18|20|4|2|True|None|0:03:40|0:01:01|86.66%|8.90%|99.79%|0.22%|
# |30|18|20|1|0|True|10|0:00:30|0:00:06|86.00%|10.47%|99.49%|0.46%|
# |31|18|20|2|1|True|10|0:00:54|0:00:14|86.33%|9.92%|99.58%|0.33%|
# |32|18|20|4|2|True|10|0:01:19|0:00:21|87.16%|9.44%|99.81%|0.19%|
# |33|18|20|1|0|True|20|0:00:47|0:00:12|86.96%|9.68%|99.54%|0.36%|
# |34|18|20|2|1|True|20|0:01:30|0:00:27|87.08%|9.34%|99.59%|0.32%|
# |35|18|20|4|2|True|20|0:02:15|0:00:40|87.94%|8.84%|99.81%|0.20%|
# |36|18|40|1|0|False|None|0:08:13|0:00:34|87.52%|8.65%|99.98%|0.07%|
# |37|18|40|2|1|False|None|0:17:54|0:01:17|87.70%|8.29%|99.98%|0.06%|
# |38|18|40|4|2|False|None|0:18:30|0:01:21|88.33%|8.17%|99.97%|0.06%|
# |39|18|40|1|0|False|10|0:02:13|0:00:10|87.52%|9.35%|99.97%|0.05%|
# |40|18|40|2|1|False|10|0:05:26|0:00:27|87.56%|9.36%|99.98%|0.05%|
# |41|18|40|4|2|False|10|0:05:36|0:00:28|87.95%|9.12%|99.97%|0.05%|
# |42|18|40|1|0|False|20|0:04:37|0:00:20|88.73%|8.53%|99.98%|0.05%|
# |43|18|40|2|1|False|20|0:10:24|0:00:47|88.84%|8.92%|99.97%|0.05%|
# |44|18|40|4|2|False|20|0:10:44|0:00:48|88.90%|8.60%|99.97%|0.05%|
# |45|18|40|1|0|True|None|0:01:15|0:00:21|86.55%|9.32%|99.55%|0.42%|
# |46|18|40|2|1|True|None|0:02:27|0:00:44|86.36%|9.55%|99.59%|0.38%|
# |47|18|40|4|2|True|None|0:03:45|0:01:12|86.67%|8.88%|99.81%|0.20%|
# |48|18|40|1|0|True|10|0:00:30|0:00:06|86.00%|10.47%|99.49%|0.45%|
# |49|18|40|2|1|True|10|0:00:54|0:00:15|86.32%|9.93%|99.58%|0.33%|
# |50|18|40|4|2|True|10|0:01:21|0:00:24|87.14%|9.50%|99.82%|0.18%|
# |51|18|40|1|0|True|20|0:00:47|0:00:12|86.97%|9.66%|99.54%|0.36%|
# |52|18|40|2|1|True|20|0:01:29|0:00:26|87.08%|9.34%|99.59%|0.32%|
# |53|18|40|4|2|True|20|0:02:17|0:00:44|87.96%|8.81%|99.81%|0.20%|
# |54|18|80|1|0|False|None|0:27:16|0:01:46|88.27%|8.64%|100.00%|0.00%|
# |55|18|80|2|1|False|None|1:02:05|0:04:16|88.32%|8.84%|100.00%|0.00%|
# |56|18|80|4|2|False|None|1:03:32|0:04:17|88.36%|8.89%|100.00%|0.00%|
# |57|18|80|1|0|False|10|0:05:07|0:00:25|88.07%|9.59%|100.00%|0.02%|
# |58|18|80|2|1|False|10|0:16:22|0:01:15|88.49%|9.23%|100.00%|0.01%|
# |59|18|80|4|2|False|10|0:16:43|0:01:17|88.53%|9.18%|100.00%|0.01%|
# |60|18|80|1|0|False|20|0:13:57|0:00:57|88.96%|9.36%|100.00%|0.01%|
# |61|18|80|2|1|False|20|0:34:40|0:02:30|89.26%|8.95%|100.00%|0.01%|
# |62|18|80|4|2|False|20|0:35:27|0:02:31|89.60%|8.74%|100.00%|0.01%|
# |63|18|80|1|0|True|None|0:01:15|0:00:21|86.55%|9.32%|99.55%|0.42%|
# |64|18|80|2|1|True|None|0:02:28|0:00:44|86.36%|9.55%|99.59%|0.38%|
# |65|18|80|4|2|True|None|0:03:46|0:01:13|86.67%|8.88%|99.81%|0.20%|
# |66|18|80|1|0|True|10|0:00:30|0:00:07|86.00%|10.47%|99.49%|0.45%|
# |67|18|80|2|1|True|10|0:00:55|0:00:15|86.32%|9.93%|99.58%|0.33%|
# |68|18|80|4|2|True|10|0:01:21|0:00:24|87.14%|9.50%|99.82%|0.18%|
# |69|18|80|1|0|True|20|0:00:47|0:00:12|86.97%|9.66%|99.54%|0.36%|
# |70|18|80|2|1|True|20|0:01:30|0:00:27|87.08%|9.34%|99.59%|0.32%|
# |71|18|80|4|2|True|20|0:02:17|0:00:45|87.96%|8.81%|99.81%|0.20%|

# In[134]:

CSPResultPrinter('data/models/csp/retrain-full-until-num-features/').print_results(
    print_templates_and_constants=False, print_individual_datasets=False)


# In[135]:

CSPResultPrinter('data/models/csp/retrain-full-repl/').print_results(
    print_templates_and_constants=False, print_individual_datasets=False)

