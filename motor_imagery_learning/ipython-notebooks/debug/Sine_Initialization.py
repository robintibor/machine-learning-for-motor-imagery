
# coding: utf-8

# In[760]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
#os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import numpy as np
from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import ConvElemwise
from theano.sandbox.cuda.dnn import dnn_conv
from theano.sandbox.cuda.basic_ops import gpu_alloc_empty
from motor_imagery_learning.analysis.print_results import ResultPrinter
from pylearn2.utils import serial

import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (8.0, 1.0)
matplotlib.rcParams['font.size'] = 7

from motor_imagery_learning.analysis.plot_util import (plot_misclasses_for_model, 
   plot_nlls_for_model, plot_sensor_signals, plot_misclasses_for_file)
import scipy
import seaborn
seaborn.set_style('darkgrid')
from copy import deepcopy
from motor_imagery_learning.train_scripts.train_experiments import ExperimentsRunner
from motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer
from motor_imagery_learning.analysis.data_generation import (create_sine_signal,
    pipeline, create_shifted_sines,put_noise, convolve_with_weight,
    max_pool_topo_view, log_sum_pool,generate_single_sensor_data, 
    add_timecourse, spread_to_sensors, SpreadedSinesDataset, randomly_shifted_sines)
from motor_imagery_learning.analysis.util import create_trainer
from motor_imagery_learning.mypylearn2.conv_squared import ConvSquared
from motor_imagery_learning.analysis.input_reconstruction import (
    create_rand_input, gradient_descent_input, create_input_grad_for_wanted_class_func,
    create_input_grad_with_smooth_penalty_for_wanted_class_func, reconstruct_inputs_for_class,
    power_smoothness_cost_moving_avg_scaled)
from pylearn2.config import yaml_parse
from  motor_imagery_learning.train_scripts.train_with_params import create_training_object_from_file
from pylearn2.costs.mlp import Default


# # Try out new initializer

# In[763]:

_ = plot_misclasses_for_file('data/models/debug/csp-net-online-standardize-keep-lrule/early-stop/1.result.pkl')


# In[764]:

_ = plot_misclasses_for_file('data/models/debug/raw-square-smaller-repl/early-stop/2.result.pkl')


# In[734]:

model = serial.load('data/models/debug/raw-square-smaller-repl/early-stop/2.pkl').model
_ = plot_sensor_signals(np.squeeze(model.get_weights_topo()), figsize=(5,6))


# In[ ]:




# In[739]:

initializer = SineWeightInitializer(layer_names='bandpass', sampling_freq=150)


# In[740]:

initializer.initialize(model)


# In[741]:

_ = plot_sensor_signals(np.squeeze(model.get_weights_topo()), figsize=(5,6))


# #Anwe real dataset

# In[652]:

trainer = create_training_object_from_file('data/models/debug/raw-square-smaller-repl/early-stop/2.yaml')


# In[653]:

trainer.dataset_splitter.ensure_dataset_is_loaded()


# In[663]:

model = serial.load('data/models/debug/raw-square-smaller-repl/early-stop/2.pkl').model
trainer.model = model
del trainer.model.monitor


# In[664]:

trainer._create_early_stop_trainer()


# In[ ]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer.early_stop_trainer.setup()')


# In[667]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer.early_stop_trainer.run_callbacks_and_monitoring()')


# In[668]:

plot_misclasses_for_model(trainer.model)


# In[682]:

_ = plot_sensor_signals(np.squeeze(trainer.model.get_weights_topo()), figsize=(2,6))


# In[691]:

bps = np.abs(np.fft.rfft(np.squeeze(trainer.model.get_weights_topo()), axis=1))
bp_freqs = np.fft.rfftfreq(15, d=1/150.0)
fig = plot_sensor_signals(bps, figsize=(5,6))
_ = pyplot.xticks(range(8), bp_freqs)


# ### Now replace bpfilters with sines

# In[669]:

trainer.model.layers[0].get_param_values()[0].shape


# In[678]:

sine_signals = [create_sine_signal(samples=15, freq=freq,sampling_freq=150) for freq in np.linspace(1,40,40)]
sine_signals = np.array(sine_signals).astype(np.float32)
sine_signals = sine_signals.reshape((sine_signals.shape[0],1,sine_signals.shape[1],1))


# In[692]:

# first copy
orig_params = deepcopy(trainer.model.get_param_values())


# In[693]:

trainer.model.layers[0].set_weights(sine_signals)


# In[695]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer.early_stop_trainer.run_callbacks_and_monitoring()')


# In[696]:

plot_misclasses_for_model(trainer.model)


# In[702]:

all_params = []


# In[711]:

get_ipython().run_cell_magic(u'capture', u'', u'for i in xrange(50):\n    trainer.early_stop_trainer.run_one_epoch()\n    all_params.append(deepcopy(trainer.model.get_param_values()))')


# In[712]:

plot_nlls_for_model(trainer.model)


# In[714]:

plot_misclasses_for_model(trainer.model)


# In[722]:

time_weights = np.array([np.squeeze(p[0]) for p in all_params], dtype=np.float32)
time_weights = time_weights.transpose(1,2,0)


# In[ ]:

SineWeightInitializer(layer_names=['bandpass'], sampling_freq=150)


# In[731]:

trainer.model.layers[0].layer_name


# In[737]:

class SineWeightInitializer():
    def __init__(self, layer_names, sampling_freq):
        self.layer_names = layer_names
        self.sampling_freq = sampling_freq
        
    def initialize(self, model):
        num_samples = model
        for layer in model.layers:
            if layer.layer_name in self.layer_names:
                self.initialize_layer(layer)
                
                
    def initialize_layer(self, layer):
        num_weights, num_samples = np.squeeze(layer.get_weights_topo()).shape
        sine_weights = [create_sine_signal(samples=num_samples, freq=freq,
                    sampling_freq=150) for freq in np.linspace(1,num_weights,num_weights)]
        sine_weights = np.array(sine_weights).astype(np.float32)
        sine_weights = sine_weights.reshape((sine_weights.shape[0],1,sine_weights.shape[1],1))
        layer.set_weights(sine_weights)


# In[728]:

plot_sensor_signals(time_weights[:,:,-1] - time_weights[:,:,0])


# In[725]:

_ = plot_sensor_signals(time_weights[:,:,::10])


# In[713]:

_ = plot_sensor_signals(np.squeeze(trainer.model.get_weights_topo()), figsize=(2,6))


# ## Try slightly easier dataset, without mixing to sensors 

# In[642]:

input_shape=(1000,1,600,1)    
lower_freq_topo_view, y = create_shifted_sines(input_shape, 
            RandomState(np.uint64(hash("pooled_pipeline"))), 
            sampling_freq=150.0)
        
topo_view, y = pipeline(lower_freq_topo_view, y,
    lambda x,y: put_noise(x, weight_old_data=0.04, 
        same_for_chans=True)
)


# In[643]:

from motor_imagery_learning.analysis.data_generation import SpreadedSinesDataset
clf = LDA()



topo_view_feature, y = pipeline(topo_view, y,
                         lambda x,y: convolve_with_weight(x, create_sine_signal(150, 11.0, 150.0)),
                         lambda x,y: x *x,
                         lambda x,y: log_sum_pool(x, 50))

train_acc, test_acc = clf.fit_and_score_train_test_topo(topo_view_feature, y)
print("train/test acc", train_acc, test_acc)


# In[644]:

get_ipython().run_cell_magic(u'capture', u'', u"trainer = create_training_object_from_file('data/models/fake-data/raw-square/early-stop/13.yaml')\ntrainer.dataset_splitter.dataset = DenseDesignMatrix(topo_view=topo_view, y=y, axes=('b', 'c', 0, 1))\ntrainer._create_early_stop_trainer()")


# In[645]:

sine_weight = create_sine_signal(samples=150, freq=11,sampling_freq=150)
#sine_weight = create_sine_signal(samples=150, freq=13,sampling_freq=150)
sine_weight = sine_weight.reshape((1,1,sine_weight.shape[0], 1))
trainer.model.layers[0].set_weights(sine_weight.astype(np.float32))


# ## Stepwise training

# In[646]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer.early_stop_trainer.algorithm.cost = Default()\ntrainer.early_stop_trainer.algorithm.termination_criterion._max_epochs = 200\ntrainer.early_stop_trainer.setup()\nall_params = []')


# In[647]:

get_ipython().run_cell_magic(u'capture', u'', u'for i in xrange(200):\n    trainer.early_stop_trainer.run_one_epoch()\n    all_params.append(deepcopy(trainer.model.get_param_values()))')


# In[648]:

_ = plot_misclasses_for_model(trainer.model)


# In[649]:

all_chan_weights = [p[2][0,0,0,0] for p in all_params]
all_chan_biases = [p[3][0] for p in all_params]
pyplot.plot(all_chan_weights, label="chan_weight")
# => basically does not change which makes sense since it doesnt matter much, only scales
pyplot.plot(all_chan_biases, label="chan_bias")
# moves up eventually.. 
pyplot.axhline(y=0, color='black', linestyle='--')
pyplot.legend()


# In[650]:

all_softmax_bias_diffs = [p[4][1] - p[4][0] for p in all_params]
all_softmax_weight_diffs = [p[5][0,1] - p[5][0,0] for p in all_params]

pyplot.plot(all_softmax_weight_diffs, label="softmax_weight_1-0")
pyplot.plot(all_softmax_bias_diffs, label="softmax_bias1-0")

pyplot.axhline(y=0, color='black', linestyle='--')
pyplot.legend()


# In[632]:

all_time_biases = [p[1][0] for p in all_params]

pyplot.plot(all_time_biases, label="time_biases")

pyplot.axhline(y=0, color='black', linestyle='--')
pyplot.legend()


# In[651]:

all_time_weights = np.array([p[0][0,0,:,0] for p in all_params])
pyplot.plot(all_time_weights[::10].T)


# In[634]:

pyplot.plot(np.squeeze(trainer.model.get_weights_topo()))


# ## Full training

# In[ ]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer.early_stop_trainer.run_training()')


# In[105]:

_ = plot_misclasses_for_model(trainer.model).suptitle('with freq 13')
_ = plot_nlls_for_model(trainer.model)


# In[111]:

_ = plot_misclasses_for_model(trainer.model).suptitle('with freq 13, more noise')
_ = plot_nlls_for_model(trainer.model)


# In[112]:

pyplot.plot(np.squeeze(trainer.model.get_weights_topo()))


# # Two-sensor dataset 

# In[71]:

from motor_imagery_learning.analysis.data_generation import SpreadedSinesDataset
clf = LDA()
dataset = SpreadedSinesDataset(weight_old_data=0.08)
dataset.load()



topo_view_feature, y = pipeline(dataset.get_topological_view(), dataset.y,
                         lambda x,y: spread_to_sensors(x, [[0.8/1.3, 0.5/1.3]]), # unmix
                         lambda x,y: convolve_with_weight(x, create_sine_signal(150, 11.0, 150.0)),
                         lambda x,y: x *x,
                         lambda x,y: log_sum_pool(x, 50))

train_acc, test_acc = clf.fit_and_score_train_test_topo(topo_view_feature, y)
print("train/test acc", train_acc, test_acc)


# In[78]:

get_ipython().run_cell_magic(u'capture', u'', u"trainer = create_training_object_from_file('data/models/fake-data/raw-square/early-stop/13.yaml')\ntrainer.dataset_splitter.ensure_dataset_is_loaded()\ntrainer._create_early_stop_trainer()")


# In[79]:

sine_weight = create_sine_signal(samples=150, freq=11,sampling_freq=150)
sine_weight = create_sine_signal(samples=150, freq=13,sampling_freq=150)
sine_weight = sine_weight.reshape((1,1,sine_weight.shape[0], 1))
trainer.model.layers[0].set_weights(sine_weight.astype(np.float32))


# In[80]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer.early_stop_trainer.algorithm.cost = Default()\ntrainer.early_stop_trainer.algorithm.termination_criterion._max_epochs = 200\ntrainer.early_stop_trainer.setup()\ntrainer.early_stop_trainer.run_training()')


# In[ ]:

# investigate drop and back up at 15 for random weights without sine initialization?


# In[81]:

_ = plot_misclasses_for_model(trainer.model)
_ = plot_nlls_for_model(trainer.model)


# In[69]:

_ = plot_misclasses_for_model(trainer.model)
_ = plot_nlls_for_model(trainer.model)


# In[82]:

pyplot.plot(np.squeeze(trainer.model.get_weights_topo()))

