
# coding: utf-8

# In[298]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\n\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n#site.force_global_eggs_after_local_site_packages()\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n# make sure to use cpu for csp...\nos.environ[\'THEANO_FLAGS\'] = "floatX=float32,device=cpu,nvcc.fastmath=True"\n%load_ext autoreload\n%autoreload 2\nimport matplotlib\nfrom matplotlib import pyplot\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \n#matplotlib.rcParams[\'figure.figsize\'] = (12.0, 2.0)\nmatplotlib.rcParams[\'font.size\'] = 7\nfrom analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file\nfrom motor_imagery_learning.analysis.print_results import ResultPrinter\nfrom motor_imagery_learning.csp.print_csp_results import CSPResultPrinter\nfrom pylearn2.utils import serial\nimport numpy as np\nfrom motor_imagery_learning.analysis.plot_util import (plot_nlls_for_file, \n    plot_misclasses_for_result, plot_misclasses_for_file, plot_nlls)\nfrom motor_imagery_learning.analysis.results import ResultPool, DatasetAveragedResults\nfrom glob import glob\nimport datetime\nimport seaborn\nseaborn.set_style(\'darkgrid\')\nseaborn.set_context("notebook", rc={"lines.linewidth": 0.5})\nmatplotlib.rcParams[\'figure.figsize\'] = (8.0, 2.0)')


# # Evaluation

# In[591]:

CSPResultPrinter('data/models/csp/auc-ival/').print_results()


# ## First baseline

# In[560]:

import motor_imagery_learning.csp.train_csp
import motor_imagery_learning.csp.pipeline
motor_imagery_learning.csp.train_csp.log.setLevel('DEBUG')
motor_imagery_learning.csp.pipeline.log.setLevel('DEBUG')
from motor_imagery_learning.mywyrm.processing import bandpass_cnt, segment_dat_fast
from wyrm.processing import select_classes


# In[561]:

from motor_imagery_learning.csp.train_csp import CSPTrain
import motor_imagery_learning.csp.ival_optimizers
from motor_imagery_learning.csp.ival_optimizers import AucIntervalOptimizer
from motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
csp_debug_args = dict(load_sensor_names=get_C_sensors_sorted(),#['CP3', 'CP4', 'C3', 'C4'], 
         only_last_fold=True, segment_ival=[0,4000], ival_optimizer=AucIntervalOptimizer(max_score_fraction=0.95,
                                                                                        use_abs_for_threshold=False))
anwe_csp_args = dict(csp_debug_args, min_freq=10, max_freq=10, last_low_freq=10, low_width=8, high_width=8)

bhno_csp_args = dict(csp_debug_args, min_freq=11, max_freq=11, last_low_freq=11, low_width=4, high_width=4)
big_band_csp_args = dict(csp_debug_args, min_freq=16, max_freq=16, last_low_freq=16, low_width=28, high_width=28)
middle_band_csp_args = dict(csp_debug_args, min_freq=16, max_freq=16, last_low_freq=16, low_width=14, high_width=14)


anwe_train = CSPTrain('data/BBCI-without-last-runs/AnWeMoSc1S001R01_ds10_1-12.BBCI.mat', **middle_band_csp_args)
                      
         
bhno_train = CSPTrain('data/BBCI-without-last-runs/BhNoMoSc1S001R01_ds10_1-12.BBCI.mat', **middle_band_csp_args)


# In[ ]:

hacky_meaned_remember = np.copy(motor_imagery_learning.csp.ival_optimizers.hacky_remember)


# In[573]:

hacky_meaned_remember_bhno = np.copy(motor_imagery_learning.csp.ival_optimizers.hacky_remember[6:])


# In[577]:

pyplot.plot(hacky_meaned_remember.T)


# In[575]:

pyplot.plot(hacky_meaned_remember_bhno.T)


# In[569]:

# anwe 44.87%
# bhno 61.04%
# now for some reason changed:
# anwe 43.59%
# bhno 68.83%
# for c sensors 0.95 and no abs treshold
# anwe:  42.31%
# bhno: 93.51%
# for c sensors no ival optimization
# anwe: 43.59%
# bhno: 85.71%
# for c sensors no ival optimization, ival 500-4000
# anwe: 47.44%
# bhno: 93.51%
#anwe_train.run()
bhno_train.run()


# ## Some stuffs for ival

# In[8]:

bandpassed_cnt = bandpass_cnt(bhno_train.cnt, 9,23,3)

epo = segment_dat_fast(bandpassed_cnt, 
    marker_def={'1 - Right Hand': [1], '2 - Left Hand': [2], 
        '3 - Rest': [3], '4 - Feet': [4]}, 
    ival=[0,4000])

epo_pair = select_classes(epo, [0,3])


# In[15]:

from scipy.ndimage.filters import gaussian_filter


# In[105]:

from scipy.signal import hilbert
epo_data = epo_pair.data



epo_envelope = np.abs(hilbert(epo_data, axis=1))
epo_smoothed = gaussian_filter(epo_envelope, (0,15,0), order=0, mode='reflect')


pyplot.plot(epo_data[0,:,1])
pyplot.plot(epo_envelope[0,:,1])
pyplot.plot(epo_smoothed[0,:,1])


# ### Test Binary CSP

# In[151]:

## test:
# create simple cnt set that has two classes, just 3 trials
# put sine set on first class and zeroline on second class
# (put same noise on both)

# check weights 
# ...

# implement:

# first try with data already loaded, just repeat binary csp
# then replace parts with artificial signal
# repeat 
# create artificial signal and repeat


# In[495]:

from motor_imagery_learning.csp.pipeline import BinaryCSP


# ### Original

# In[500]:

binary_csp = BinaryCSP(bhno_train.cnt, bhno_train.filterbands, 
             bhno_train.filt_order, bhno_train.folds, 
             bhno_train.class_pairs,
          bhno_train.segment_ival, bhno_train.num_filters, 
             ival_optimizer=None)


# In[501]:

binary_csp.run()


# In[195]:

bhno_train.cnt.markers[:20]


# #### Replacing folds

# In[175]:

from motor_imagery_learning.analysis.data_generation import create_sine_signal


# In[209]:

new_folds = [{'train': [0,1,4,5], 'test':[10,12]}]
new_class_pairs = [(0,3)]
new_cnt_data = np.copy(bhno_train.cnt.data)
new_cnt_data[3000:4200,0:3] = np.atleast_2d(create_sine_signal(1200, 15, 300.0)).T
new_cnt_data[5209:6409,0:3] = 0#np.random.randn(1200,4) * 0.01
new_cnt_data[11947:11947+1200,0:3] = np.atleast_2d(create_sine_signal(1200, 15, 300.0)).T
new_cnt_data[14221:14221+1200,0:3] = 0#np.random.randn(1200,4) * 0.01
new_cnt_data[25219:25219+1200,0:3] = np.atleast_2d(create_sine_signal(1200, 15, 300.0)).T
new_cnt_data[29728:29728+1200,0:3] = 0#np.random.randn(1200,4) * 0.01
new_cnt = bhno_train.cnt.copy(data=new_cnt_data)


# In[210]:

binary_csp = BinaryCSP(new_cnt, bhno_train.filterbands,
             bhno_train.filt_order, new_folds,
             new_class_pairs,
          bhno_train.segment_ival, bhno_train.num_filters,
             bhno_train.optimize_segment_ival)
binary_csp.run()


# In[213]:

print binary_csp.filters[0,0,0]


# In[207]:

binary_csp.clf


# In[203]:

99092 * 300 / 1000.0


# ### Artificial data

# In[218]:

from wyrm.types import Data

artificial_cnt = Data(new_cnt_data, axes=new_cnt.axes, names=new_cnt.names, 
                      units=new_cnt.units)

artificial_cnt.fs = 300
artificial_cnt.markers = new_cnt.markers

binary_csp = BinaryCSP(artificial_cnt, bhno_train.filterbands,
             bhno_train.filt_order, new_folds,
             new_class_pairs,
          bhno_train.segment_ival, bhno_train.num_filters,
             bhno_train.optimize_segment_ival)
binary_csp.run()


# ### Completely artificial data

# In[475]:

from numpy.random import  RandomState
fs = 100
samples_per_trial = 100

fake_cnt_data = np.ones((6 * samples_per_trial, 2)) * np.nan

sine_signal = create_sine_signal(samples=samples_per_trial, freq=5, sampling_freq=fs)

# put sine signal on last three trials on first chan
fake_cnt_data[3*samples_per_trial:4*samples_per_trial:,0] = sine_signal
fake_cnt_data[4*samples_per_trial:5*samples_per_trial,0] = sine_signal
fake_cnt_data[5*samples_per_trial:6*samples_per_trial:,0] = sine_signal

# put zeros on first three  trials on first chan
fake_cnt_data[0:1*samples_per_trial:,0] = 0
fake_cnt_data[1*samples_per_trial:2*samples_per_trial:,0] = 0
fake_cnt_data[2*samples_per_trial:3*samples_per_trial:,0] = 0

# put zeros on second chan for all trials
fake_cnt_data[:,1] = 0

# put some noise on everything
noise = RandomState(np.uint32(hash('fake_cnt'))).randn(fake_cnt_data.shape[0], 1)

fake_cnt_data += noise * 0.2

assert not np.any(np.isnan(fake_cnt_data))

axes = [np.arange(0,6*1000,10), ['Sensor1', 'Sensor2']]


artificial_cnt = Data(fake_cnt_data, axes=axes, names=['time', 'channel'], 
                      units=['ms', '#'])
markers = [(0.0, 0), (1000.0, 0), (2000.0, 0), (3000.0, 1), (4000.0, 1), (5000.0, 1)]

# make sure to have both classes in train and test
fake_folds = [{'train': [0,1,3,4], 'test':[2,5]}]

artificial_cnt.markers = markers
artificial_cnt.fs = fs

fake_class_pairs = [[0,1]]

fake_ival = [0,1000.0]
num_filters = 1
fake_optimize_ival = 0

binary_csp = BinaryCSP(
            artificial_cnt,
            [[3, 6]],
            3,
            fake_folds,
            fake_class_pairs,
            fake_ival,
            num_filters,
            fake_optimize_ival,
            marker_def={'0':[0], '1':[1]})
binary_csp.run()


# In[484]:

assert np.array_equal([1,1,1], binary_csp.test_accuracy.shape)
assert binary_csp.test_accuracy[0,0,0] == 1.0
assert np.array_equal([1,1,1], binary_csp.train_accuracy.shape)
assert binary_csp.train_accuracy[0,0,0] == 1.0
# regression test
assert np.allclose([[0.01285997, -1.],
                   [1. ,  1.]],
                   binary_csp.filters[0,0,0]), (
   "regression test, should stay exactly the same unless some parts of computation changed")
assert np.allclose([0.03325832, -1.],binary_csp.variances[0,0,0]), (
   "regression test, should stay exactly the same unless some parts of computation changed")


# In[489]:

epo.fs * 100 / 1000.0


# In[490]:

from motor_imagery_learning.csp.pipeline import optimize_segment_ival
optimize_segment_ival(train_epo)


# ### Comparing csp filter apply functions runtimes

# In[388]:

from mywyrm.processing import apply_csp_fast, apply_csp_faster
from wyrm.processing import apply_csp


# In[424]:

filtered = np.dot(train_epo.data, binary_csp.filters[0,0,0])
#np.dot(binary_csp.filters[0,0,0].T, train_epo.data.transpose(0,2,1)).transpose(1,2,0)
filtered_epo = apply_csp_fast(train_epo, binary_csp.filters[0,0,0])
filtered_epo_wyrm = apply_csp(train_epo, binary_csp.filters[0,0,0])
filtered_epo_faster= apply_csp_faster(train_epo, binary_csp.filters[0,0,0])
filtered.shape
filtered_epo.data.shape
assert np.array_equal(filtered, filtered_epo.data)
assert np.array_equal(filtered, filtered_epo_wyrm.data)
assert np.array_equal(filtered, filtered_epo_faster.data)


# In[374]:

from wyrm.processing import select_channels
epo.axes[2]


# In[433]:

rng = RandomState(30487)
fake_epo = epo.copy(data=rng.randn(1000,1200,128))
fakefilters = rng.randn(128,10)


# In[434]:

get_ipython().run_cell_magic(u'timeit', u'', u'apply_csp_fast(fake_epo, fakefilters)')


# In[435]:

get_ipython().run_cell_magic(u'timeit', u'', u'apply_csp_faster(fake_epo, fakefilters)')


# In[301]:

from wyrm.processing import lda_apply


# In[300]:

pyplot.plot(fake_cnt_data)


# In[265]:




# In[25]:


from sklearn.metrics import roc_auc_score


# In[123]:


epo_pair = select_classes(epo, [2,3])
epo_data = epo_pair.data

epo_envelope = np.abs(hilbert(epo_data, axis=1))
epo_smoothed = gaussian_filter(epo_envelope, (0,15,0), order=0, mode='reflect')


# In[133]:

from wyrm.processing import select_ival
epo_cut = select_ival(epo_pair, [start_ms, end_ms])

epo_cut.data.shape


# In[148]:

100.0 * epo_pair.fs / 1000.0


# In[141]:

max_score_fraction = 0.80
labels = epo_pair.axes[0]
binary_labels = np.int32(labels == np.max(labels))
n_samples = len(epo_pair.axes[1])
n_chans = len(epo_pair.axes[2])
assert n_samples % 40 == 0
n_samples_per_block = n_samples // 40
n_time_blocks = n_samples // n_samples_per_block


auc_scores = np.ones((n_time_blocks, n_chans)) * np.nan

for i_time_block in range(n_time_blocks):
    for i_chan in range(n_chans):
        start_sample = i_time_block *n_samples_per_block
        epo_part = epo_smoothed[:,start_sample: start_sample + n_samples_per_block,i_chan]
        score = roc_auc_score(binary_labels, np.sum(epo_part, axis=(1)))
        auc_scores[i_time_block, i_chan] = score
        
# auc values indicate good separability if they are close to 0 or close to 1
# subtracting 0.5 transforms them to mean better separability more far away from 0
# this makes later computations easier
auc_scores = auc_scores - 0.5 

auc_score_chan = np.sum(np.abs(auc_scores), axis=1)

# sort time ivals so that best ival across chans is first
time_blocks_sorted = np.argsort(auc_score_chan)[::-1]
best_block = time_blocks_sorted[0]


chan_above_zero = auc_scores[best_block, :] > 0
chan_sign = 1 * chan_above_zero + -1 * np.logical_not(chan_above_zero)

sign_adapted_scores = auc_scores * chan_sign

chan_meaned_scores = np.sum(sign_adapted_scores, axis=1)
best_meaned_block = np.argsort(chan_meaned_scores)[::-1][0]
threshold = np.sum(chan_meaned_scores[chan_meaned_scores > 0]) * max_score_fraction


t0 = best_meaned_block
t1 = best_meaned_block
while (np.sum(chan_meaned_scores[t0:t1+1]) < threshold):
    if np.sum(chan_meaned_scores[:t0]) > np.sum(chan_meaned_scores[t1+1:]):
        t0 = t0 - 1
    else:
        t1 = t1 + 1

start_sample = t0 * n_samples_per_block
end_sample = (t1 + 1) * n_samples_per_block
start_ms = start_sample * 1000.0 / epo_pair.fs 
end_ms = end_sample * 1000.0 / epo_pair.fs
start_ms, end_ms


# In[140]:


pyplot.plot(auc_scores[:,:])
pyplot.plot(auc_score_chan)
pyplot.plot(chan_meaned_scores)


# In[ ]:



