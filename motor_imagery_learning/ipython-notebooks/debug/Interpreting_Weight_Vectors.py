
# coding: utf-8

# In[2]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nimport os\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n#os.environ[\'CUDA_LAUNCH_BLOCKING\'] = \'1\'\n#os.environ[\'THEANO_FLAGS\'] = os.environ[\'THEANO_FLAGS\'] + ",profile=True"\n# switch to cpu\n#os.environ[\'THEANO_FLAGS\'] = \'floatX=float32,device=cpu,nvcc.fastmath=True\'\n%load_ext autoreload\n%autoreload 2\n\nfrom motor_imagery_learning.bbci_pylearn_dataset import (compute_power_spectra)\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset\nimport numpy as np\n\nimport matplotlib\nfrom matplotlib import pyplot\nfrom matplotlib import cm\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (8.0, 2.0)\nmatplotlib.rcParams[\'font.size\'] = 7\n\nfrom pylearn2.costs.mlp import Default\nfrom pylearn2.models.mlp import Softmax, MLP\nfrom pylearn2.termination_criteria import EpochCounter\nimport seaborn\nseaborn.set_style(\'darkgrid\')\nfrom copy import deepcopy\nfrom motor_imagery_learning.train_scripts.train_experiments import ExperimentsRunner\nfrom motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer\nfrom motor_imagery_learning.analysis.data_generation import (create_sine_signal,\n    pipeline, create_shifted_sines,put_noise, convolve_with_weight,\n    max_pool_topo_view, log_sum_pool)\nfrom motor_imagery_learning.analysis.util import create_trainer\nfrom motor_imagery_learning.mypylearn2.conv_squared import ConvSquared\nfrom motor_imagery_learning.analysis.input_reconstruction import (\n    create_rand_input, gradient_descent_input, create_input_grad_for_wanted_class_func,\n    create_input_grad_with_smooth_penalty_for_wanted_class_func, \n    reconstruct_inputs_for_class)\n\nfrom motor_imagery_learning.analysis.plot_util import (plot_misclasses_for_model, \n    plot_nlls_for_model, plot_sensor_signals)\n\nimport h5py\nimport numpy as np\nfrom pylearn2.utils import serial\nfrom pprint import pprint\nfrom  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted\nimport h5py\nfrom motor_imagery_learning.csp.train_csp import CSPTrain\nfrom motor_imagery_learning.mypylearn2.train import TrainEarlyStop\nfrom motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD\nfrom motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize\nfrom motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset\nfrom motor_imagery_learning.bbci_dataset import BBCIDataset\nfrom pylearn2.costs.mlp import Default\nfrom pylearn2.models.mlp import Softmax, MLP\nfrom pylearn2.termination_criteria import EpochCounter\nfrom sklearn.cross_validation import KFold\nfrom pylearn2.datasets.dense_design_matrix import DenseDesignMatrix\nimport numpy as np\nfrom numpy.random import RandomState\nimport theano.tensor as T\nimport theano\nfrom pylearn2.space import Conv2DSpace\nfrom pylearn2.models.mlp import ConvElemwise\nfrom theano.sandbox.cuda.dnn import dnn_conv\nfrom theano.sandbox.cuda.basic_ops import gpu_alloc_empty\nfrom motor_imagery_learning.analysis.print_results import ResultPrinter\nfrom pylearn2.utils import serial\nfrom motor_imagery_learning.analysis.data_generation import generate_single_sensor_data\nfrom motor_imagery_learning.analysis.plot.raw_signals import (\n    plot_sensor_signals_two_classes)\nfrom motor_imagery_learning.mypylearn2.pool import PoolLayer, log_sum\nfrom motor_imagery_learning.analysis.util import bps_and_freqs\nfrom motor_imagery_learning.analysis.deconv import deconv_model\nfrom motor_imagery_learning.analysis.plot_util import plot_head_signals_tight_two_signals')


# In[33]:

rng = RandomState(np.uint64(hash('weightinterpretation')))
n_trials = 1000
noise = rng.randn(n_trials)
negative_examples = np.sort(rng.choice(len(x1),len(x1)/2, replace=False))
x1 = np.ones(n_trials)
x1[negative_examples] = -1

y = np.ones(n_trials)
y[negative_examples] = 0
x2 = np.zeros(n_trials)
x1 = x1 + noise
x2 = x2 + noise
X = np.vstack((x1,x2)).T



# In[41]:

from sklearn.covariance import LedoitWolf
def lda(x,y):
    shrink =True
    labels = np.sort(np.unique(y))
    mu1 = np.mean(x[y == labels[0]], axis=0)
    mu2 = np.mean(x[y == labels[1]], axis=0)
    # x' = x - m
    m = np.empty(x.shape)
    m[y == labels[0]] = mu1
    m[y == labels[1]] = mu2
    x2 = x - m
    # w = cov(x)^-1(mu2 - mu1)
    if shrink:
        estimator = LedoitWolf()
        covm = estimator.fit(x2).covariance_
    else:
        covm = np.cov(x2.T)
    w = np.dot(np.linalg.pinv(covm), (mu2 - mu1))

    #  From matlab bbci toolbox:
    # https://github.com/bbci/bbci_public/blob/fe6caeb549fdc864a5accf76ce71dd2a926ff12b/classification/train_RLDAshrink.m#L133-L134
    #C.w= C.w/(C.w'*diff(C_mean, 1, 2))*2;
    #C.b= -C.w' * mean(C_mean,2);
    w = (w / np.dot(w.T, (mu2 - mu1))) * 2
    b = np.dot(-w.T, np.mean((mu1, mu2), axis=0))
    return w,b


# In[43]:

w, b = lda(X,y)
w


# In[36]:

Xcov = np.cov(X.T)


# In[38]:

Xcov


# In[37]:

np.dot(Xcov, w)


# In[44]:

rng = RandomState(np.uint64(hash('weightinterpretation')))
n_trials = 1000
noise = rng.randn(n_trials)
negative_examples = np.sort(rng.choice(len(x1),len(x1)/2, replace=False))
x1 = np.ones(n_trials)
x1[negative_examples] = -1

x2 = np.ones(n_trials) * 0.1
x2[negative_examples] = -0.1
x1 = x1 + noise
x2 = x2 + noise
y = np.ones(n_trials)
y[negative_examples] = 0
X = np.vstack((x1,x2)).T



# In[45]:

w, b = lda(X,y)
w


# In[63]:

np.cov(X.T)


# In[46]:

np.dot(np.cov(X.T), w)


# In[52]:

y.shape


# In[53]:

X.shape


# In[62]:

np.cov(X.T)


# In[60]:

np.cov(X.T,y)


# In[ ]:



