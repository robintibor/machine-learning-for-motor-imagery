
# coding: utf-8

# In[73]:

import os

import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# switch to cpu...
os.environ['THEANO_FLAGS'] = "floatX=float32,device=cpu,nvcc.fastmath=True"
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import theano
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from pylearn2.models.mlp import MLP, Softmax
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
from numpy.random import RandomState
import numpy as np
from pylearn2.format.target_format import OneHotFormatter
from pylearn2.termination_criteria import EpochCounter
from motor_imagery_learning.mypylearn2.dataset_splitters import DatasetSingleFoldSplitter, DatasetTwoFileSingleFoldSplitter


# In[9]:

input_shape = (13,5,2,3)

max_epochs_crit = EpochCounter(max_epochs=1)
algorithm = SimilarBatchSizeSGD(learning_rate=0.1, batch_size=10,seed=np.uint64(hash('test_train_sgd')),
                                termination_criterion=max_epochs_crit)

softmax = Softmax(n_classes=2, layer_name='y', irange=0.01)
mlp = MLP(layers=[softmax], seed= np.uint64(hash('test_train_mlp')))

rng = RandomState(np.uint64(hash('test_dataset')))
topo_view = rng.randn(*input_shape)

y = OneHotFormatter(2).format(rng.random_integers(0,1,size=input_shape[0]))
dataset = DenseDesignMatrix(topo_view=topo_view, y=y, rng=rng)
dataset_splitter = DatasetSingleFoldSplitter(dataset, num_folds=10, test_fold_nr=-1)
trainer = TrainEarlyStop(dataset_splitter, mlp, algorithm=algorithm, save_path=None, preprocessor=None, 
                         keep_best_lrule_params=False, 
                         best_model_channel='valid_y_misclass', fraction_epochs_after_stop=1.0)


# In[10]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer.main_loop()')


# In[11]:

train_misclass_expected = [0.36363637, 0.0, 0.0, 0.083333336, 0.083333336, 0.083333336]
valid_misclass_expected = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
test_misclass_expected = [1.0, 0.0, 0.0, 0.0, 0.0, 0.0]
assert np.allclose(train_misclass_expected, trainer.model.monitor.channels['train_y_misclass'].val_record)
assert np.array_equal(valid_misclass_expected, trainer.model.monitor.channels['valid_y_misclass'].val_record)
assert np.array_equal(test_misclass_expected, trainer.model.monitor.channels['test_y_misclass'].val_record)


# In[86]:

input_shape = (100,5,8,3)

max_epochs_crit = EpochCounter(max_epochs=20)
algorithm = SimilarBatchSizeSGD(learning_rate=0.1, batch_size=10,seed=np.uint64(hash('test_train_sgd')),
                                termination_criterion=max_epochs_crit)

softmax = Softmax(n_classes=2, layer_name='y', irange=0.01)
mlp = MLP(layers=[softmax], seed= np.uint64(hash('test_train_mlp')))

rng = RandomState(np.uint64(hash('test_dataset_2')))
topo_view = rng.randn(*input_shape)

y = OneHotFormatter(2).format(rng.random_integers(0,1,size=input_shape[0]))
topo_view[np.argmax(y, axis=1) == 1] +=0000.1
topo_view[np.argmax(y, axis=1) == 1][:10] -=0.1
dataset = DenseDesignMatrix(topo_view=topo_view, y=y, rng=rng)
dataset_splitter = DatasetSingleFoldSplitter(dataset, num_folds=10, test_fold_nr=-1)
trainer = TrainEarlyStop(dataset_splitter, mlp, algorithm=algorithm, save_path=None, preprocessor=None, 
                         keep_best_lrule_params=False, 
                         best_model_channel='valid_y_misclass', fraction_epochs_after_stop=1.0)


# In[87]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer.main_loop()')


# In[88]:

trainer.best_epoch


# In[89]:

trainer.model.monitor.channels['train_y_misclass'].val_record


# In[90]:

train_misclass_expected = [0.53750002, 0.125, 0.055555556]
valid_misclass_expected = [0.60000002, 0.2, 0.0]
test_misclass_expected = [0.30000001, 0.30000001, 0.30000001]
assert np.allclose(train_misclass_expected, trainer.model.monitor.channels['train_y_misclass'].val_record)
assert np.allclose(valid_misclass_expected, trainer.model.monitor.channels['valid_y_misclass'].val_record)
assert np.allclose(test_misclass_expected, trainer.model.monitor.channels['test_y_misclass'].val_record)

