
# coding: utf-8

# In[28]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
#os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import numpy as np
from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import ConvElemwise
from theano.sandbox.cuda.dnn import dnn_conv
from theano.sandbox.cuda.basic_ops import gpu_alloc_empty
from motor_imagery_learning.analysis.print_results import ResultPrinter
from pylearn2.utils import serial

import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7

from motor_imagery_learning.analysis.plot_util import plot_misclasses_for_model


# In[2]:

def weight_unsmoothness_moving_avg(weight):
    average = np.convolve(weight, [0.5, 0, 0.5], mode='valid')
    return np.sum(np.square(weight[1:-1] - average))

def weight_unsmoothness_moving_avg_scaled(weight):
    average = np.convolve(weight, [0.5, 0, 0.5], mode='valid')
    return np.sum(np.square(weight[1:-1] - average)) / np.sum(np.square(weight[1:-1]))

def weight_unsmoothness_diff_square(weight):
    return np.sum(np.square(weight[1:] - weight[:-1])) 

def weight_unsmoothness_diff_square_scaled(weight):
    return np.sum(np.square(weight[1:] - weight[:-1]))  / np.sum(np.square(weight))


# In[3]:

spatial_model = serial.load('data/models/debug/raw-data-spatial/early-stop/23.pkl')
spatial_model = spatial_model.model

spat_layer = spatial_model.layers[0]
time_layer = spatial_model.layers[1]

spatial_weights = spat_layer.get_weights_topo()
time_weights = time_layer.get_weights_topo()
print "spatial weights", spatial_weights.shape
print "time weights", time_weights.shape


from motor_imagery_learning.analysis.plot_util import plot_sensor_signals
_ = plot_sensor_signals(np.squeeze(time_weights), map(str, range(len(time_weights))), figsize=(4,15),yticks=[])


# In[4]:

bad_weights = np.squeeze(time_weights[0:2])
good_weights = np.squeeze(time_weights[2:4])
pyplot.plot(good_weights.T)
pyplot.plot(bad_weights.T)
pyplot.legend(('good0', 'good1', 'bad0', 'bad1'))
pyplot.title("Good weights(bad weights)")


# In[5]:

print "moving avg good ", weight_unsmoothness_moving_avg(good_weights[1])
print "diff good ", weight_unsmoothness_diff_square(good_weights[1])
print "moving avg bad ", weight_unsmoothness_moving_avg(bad_weights[1])
print "diff bad ", weight_unsmoothness_diff_square(bad_weights[1])

print "moving avg good scaled ", weight_unsmoothness_moving_avg_scaled(good_weights[1])
print "diff good scaled ", weight_unsmoothness_diff_square_scaled(good_weights[1])
print "moving avg bad scaled ", weight_unsmoothness_moving_avg_scaled(bad_weights[1])
print "diff bad scaled ", weight_unsmoothness_diff_square_scaled(bad_weights[1])


# In[8]:

from theano.sandbox.cuda import cuda_enabled
from theano.sandbox.cuda.dnn import dnn_available
if cuda_enabled and dnn_available():
    from theano.sandbox.cuda.dnn import dnn_conv 
else:
    import theano.tensor.nnet.conv
def conv2d(inputs, filters, filter_stride):
    if cuda_enabled and dnn_available():
        return dnn_conv(img=inputs, kerns=filters, subsample=filter_stride, border_mode='valid')
    else:
        return theano.tensor.nnet.conv.conv2d(inputs, filters, 
           image_shape=None, filter_shape=None,
            subsample=filter_stride, border_mode='valid')


# In[9]:

from theano.sandbox.cuda.dnn import dnn_available, dnn_pool
from theano.sandbox.cuda import cuda_enabled

# all functions here assume weights are matrices i.e. #filters x #time
def moving_avg_theano(weights):
    weights_expanded = weights.dimshuffle(0,'x',1,'x')
    filters = T.constant([0.5,0,0.5], dtype=theano.config.floatX).dimshuffle('x', 'x', 0, 'x')
    average = conv2d(weights_expanded, filters, filter_stride=(1,1))
    return average[:,0,:,0] # assumes average has only two dimensions: #weights x #time
def weight_unsmoothness_moving_avg_theano(weights):
    average = moving_avg_theano(weights)
    return T.sum(T.square(weights[:,1:-1] - average), axis=(1))


def weight_unsmoothness_moving_avg_scaled_theano(weights):
    unsmoothness = weight_unsmoothness_moving_avg_theano(weights)
    return unsmoothness / T.sum(T.square(weights), axis=(1))#T.sum(T.square(weights[:,1:-1]), axis=(1))

def weight_unsmoothness_moving_avg_no_square_theano(weights):
    average = moving_avg_theano(weights)
    return T.sum(T.abs_(weights[:,1:-1] - average), axis=(1))

def weight_unsmoothness_moving_avg_no_square_scaled_theano(weights):
    unsmoothness = weight_unsmoothness_moving_avg_no_square_theano(weights)
    return unsmoothness / T.sum(T.abs_(weights), axis=(1))#T.sum(T.square(weights[:,1:-1]), axis=(1))

def weight_unsmoothness_diff_square_theano(weights):
    return T.sum(T.square(weights[:, 1:] - weights[:, :-1]), axis=(1)) 

def weight_unsmoothness_diff_square_scaled_theano(weights):
    return T.sum(T.square(weights[:, 1:] - weights[:, :-1]), axis=(1)) / T.sum(T.square(weights), axis=(1))#T.sum(T.square(weights[:,1:-1]), axis=(1))


# In[10]:

theano_weight = T.fmatrix()
penalty = weight_unsmoothness_moving_avg_theano(theano_weight)

unsmooth_movavg_theano_func = theano.function([theano_weight], penalty)


# In[11]:

# [ 0.04128213,  0.07440607] for bad _weights expected
unsmooth_movavg_theano_func(bad_weights)


# In[12]:

theano_weight = T.fmatrix()
penalty = weight_unsmoothness_moving_avg_scaled_theano(theano_weight)

unsmooth_movavg_scaled_theano_func = theano.function([theano_weight], penalty)


# In[13]:

# [ 0.93735653,  1.61124740604] for bad _weights expected
unsmooth_movavg_scaled_theano_func(bad_weights)


# In[14]:

unsmooth_movavg_scaled_theano_func(good_weights)


# In[15]:

def make_penalty_grad_func(penalty_func):
    theano_weights = T.fmatrix()
    penalty = penalty_func(theano_weights)

    cost = T.sum(penalty)
    grad = T.grad(cost, theano_weights)
    penalty_grad_func = theano.function([theano_weights], grad)
    return penalty_grad_func


# In[16]:

unsmooth_movavg_scaled_grad_func = make_penalty_grad_func(weight_unsmoothness_moving_avg_scaled_theano)
unsmooth_movavg_grad_func = make_penalty_grad_func(weight_unsmoothness_moving_avg_theano)
unsmooth_diff_scaled_grad_func = make_penalty_grad_func(weight_unsmoothness_diff_square_scaled_theano)
unsmooth_diff_grad_func = make_penalty_grad_func(weight_unsmoothness_diff_square_theano)
unsmooth_movavg_nosq_scaled_grad_func = make_penalty_grad_func(weight_unsmoothness_moving_avg_no_square_scaled_theano)
unsmooth_movavg_nosq_grad_func = make_penalty_grad_func(weight_unsmoothness_moving_avg_no_square_theano)


# In[17]:

def show_weight_changes(grad_func, weights, epochs=2000):
    cur_weights = weights
    pyplot.plot(cur_weights.T)
    for i in range(epochs):
        cur_grad = grad_func(cur_weights)
        cur_weights = (cur_weights - cur_grad * 0.1)
        if (i % (epochs // 8) == 0):
            pyplot.plot(cur_weights.T)

    pyplot.plot(cur_weights.T)
    pyplot.figure()
    pyplot.plot(cur_weights.T)
    return cur_weights


# In[18]:

final_weights = show_weight_changes(unsmooth_movavg_scaled_grad_func, bad_weights[1:2], epochs=500)


# In[19]:

final_good_weights = show_weight_changes(unsmooth_movavg_scaled_grad_func, good_weights[1:2], epochs=500)


# ## Try solve classifier problems with smoothness

# ### First replicate problems

# In[20]:

from motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer
from motor_imagery_learning.analysis.data_generation import (create_sine_signal,
    SingleChanDataset, pipeline, create_shifted_sines,put_noise_on_set, convolve_with_weight,
    max_pool_topo_view, log_sum_pool)


# In[96]:

from motor_imagery_learning.mypylearn2.learning_rule_adam import Adam
from pylearn2.costs.mlp.dropout import Dropout
from motor_imagery_learning.mypylearn2.dataset_splitters import DatasetSingleFoldSplitter
from motor_imagery_learning.mywyrm.processing import resample_cnt, highpass_cnt

def generate_single_sensor_data(filename, chan, min_freq=11, max_freq=11, low_width=2, last_low_freq=11,high_width=2):

    lower_dataset = BBCIPylearnCleanFilterbankDataset(filenames=filename,
                               load_sensor_names=[chan], cleaner=BBCISetNoCleaner(),
                                min_freq=min_freq, max_freq=max_freq, low_width=low_width, 
                               last_low_freq=last_low_freq,high_width=high_width,
                               cnt_preprocessors = [(resample_cnt, dict(newfs=300)),
                                                    (highpass_cnt, dict(low_cut_off_hz=0.5))])

    lower_dataset.load()
    
    classes = np.argmax(lower_dataset.y, axis=1)
    wanted_trials = np.logical_or(classes == 0, classes == 2)
    lower_topo_view = lower_dataset.get_topological_view()
    lower_class_1_3_topo_view = lower_topo_view[wanted_trials]
    y_1_3 = lower_dataset.y[wanted_trials][:,[0,2]] # only keep the colums for classes 1/3
    return lower_class_1_3_topo_view, y_1_3

def create_trainer(topo_view, y, layers=None, cost="dropout", max_epochs=1000, preprocessor=None):
    if layers is None:
        layers = [Softmax(n_classes=2, layer_name='y', irange=0.001)]
    max_epochs_crit = EpochCounter(max_epochs=max_epochs)
    if cost == "dropout":
        cost = Dropout(input_include_probs={layers[0].layer_name: 0.8}, 
                       input_scales={layers[0].layer_name: 1/0.8})
    algorithm = SimilarBatchSizeSGD(learning_rate=0.1, batch_size=60,seed=np.uint64(hash('test_train_sgd')),
                                    termination_criterion=max_epochs_crit, learning_rule=Adam(),
                                   cost=cost)
    mlp = MLP(layers=layers, seed= np.uint64(hash('test_train_mlp')))
    dataset = DenseDesignMatrix(topo_view=topo_view, y=y, 
                                rng=RandomState(np.uint64(hash('batchhash?'))),
                                                         axes=('b', 'c', 0, 1))
    
    
    dataset_splitter = DatasetSingleFoldSplitter(dataset, num_folds=10, test_fold_nr=-1)
    trainer = TrainEarlyStop(dataset_splitter, mlp, algorithm=algorithm, save_path=None, preprocessor=None, 
                             keep_best_lrule_params=False, 
                             best_model_channel='valid_y_misclass', fraction_epochs_after_stop=1.0)
    return trainer


# In[22]:

def sum_all(topo_view):
    return np.sum(topo_view, axis=(1,2,3))


# In[23]:

clf = LDA()
input_shape=(100,1,1200,1)
start_topo_view, y = create_shifted_sines(input_shape, RandomState(np.uint64(hash("pooled_pipeline"))))


# In[24]:

topo_view_pooled, y_pooled = pipeline(start_topo_view, y, 
    lambda x,y: put_noise_on_set(x,weight_old_data=0.05),
    lambda x,y: convolve_with_weight(x, create_sine_signal(samples=150,freq=11, sampling_freq=300.0)),
    lambda x,y: x*x,
    lambda x,y: sum_all(x)) #lambda x,y: max_pool_topo_view(x,pool_shape=50))
    #lambda x,y: np.log(x)) # unnecessary atleast for lda

print ("topo view pooled", clf.fit_and_score_train_test_topo(topo_view_pooled, y_pooled))


# In[25]:

from motor_imagery_learning.mypylearn2.conv_squared import ConvSquared

topo_view_noised, y = pipeline(start_topo_view, y, 
    lambda x,y: put_noise_on_set(x,weight_old_data=0.05))
# convolve + square + maxpool
layers = [ConvSquared(output_channels=1, kernel_shape=[150,1], pool_shape=[50,1], pool_stride=[50,1],
                     pool_type="sumlogall",layer_name="conv_squared", irange=0.05, max_kernel_norm=2.0,
                     tied_b=True),
          Softmax(n_classes=2, layer_name='y', irange=0.001, max_col_norm=0.5)]
trainer = create_trainer(topo_view_noised, y,max_epochs=1000, layers=layers, cost=None)


# In[26]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer.run_until_earlystop()')


# In[29]:

plot_misclasses_for_model(trainer.model)


# In[30]:

pyplot.plot(np.squeeze(trainer.model.get_weights_topo()))


# In[31]:

trainer.model.monitor.channels['test_y_misclass'].val_record[-5:]


# In[44]:

#mache modell: => artificial data => sqrtsumsqr => softmax => say 1 or 0 
# first just make function whcih accepts weights as argument
# 

def create_conv_square_model(unsmooth_weight = 0.01):
    inputs = T.ftensor4()
    weight = T.fvector()
    weights_expanded = weight.dimshuffle('x', 'x',0,'x')
    convolved = conv2d(inputs, weights_expanded, filter_stride=(1,1))
    squared = T.sqr(convolved)
    summed = T.sum(squared, axis=(1,2,3))

    forward_func = theano.function([inputs, weight], summed, allow_input_downcast=True)

    logged = T.log(summed)
    softmax_weights = T.fvector()
    softmax_biases = T.fvector()
    affine_out = T.dot(logged.dimshuffle(0, 'x'), softmax_weights.dimshuffle('x', 0)) + softmax_biases.dimshuffle('x', 0)
    softmax_out = T.nnet.softmax(affine_out)
    class_out_func = theano.function([inputs, weight, softmax_weights, softmax_biases], softmax_out,
                                    allow_input_downcast=True)
    Y = T.fmatrix()
    z = affine_out
    # code copied from mlp.py softmax class pylearn
    z = z - z.max(axis=1).dimshuffle(0, 'x')
    log_prob = z - T.log(T.exp(z).sum(axis=1).dimshuffle(0, 'x'))
    log_prob_of = (Y * log_prob)
    summed_log_prob_of = log_prob_of.sum(axis=1)
    cost = -log_prob_of.mean()

    weight_grad, softmax_weight_grad, softmax_bias_grad = T.grad(cost, [weight, softmax_weights, softmax_biases])

    weights_grad_func = theano.function([inputs, weight, softmax_weights, softmax_biases, Y], 
                                        [weight_grad, softmax_weight_grad, softmax_bias_grad])

    # now lets add our weight cost unsmoothness
    unsmooth_cost =  T.sqrt(T.sum(weight_unsmoothness_moving_avg_scaled_theano(weight.dimshuffle('x', 0)))) * unsmooth_weight
    total_cost = unsmooth_cost + cost

    weight_grad_total, softmax_weight_grad_total, softmax_bias_grad_total = T.grad(total_cost, [weight, softmax_weights,
                                                                                               softmax_biases])
    weights_grad_total_func = theano.function([inputs, weight, softmax_weights, softmax_biases, Y], 
                                        [weight_grad_total, softmax_weight_grad_total, softmax_bias_grad_total])


    weight_grad_unsmooth = T.grad(unsmooth_cost, [weight])
    unsmooth_grad_func = theano.function([weight], weight_grad_unsmooth)
    costs_func = theano.function([inputs, weight, softmax_weights, softmax_biases, Y], [cost, unsmooth_cost])
    return weights_grad_total_func, unsmooth_grad_func, costs_func, forward_func, class_out_func


# In[66]:

def train_model(inputs, y, cur_weight, cur_soft_weight, cur_biases,
                weights_grad_total_func, unsmooth_grad_func, costs_func, lrate,batch_size):
    data_costs = []
    weight_costs = []
    total_grad_sum = []
    data_grad_sum = []
    unsmooth_grad_sum = []
    weights_during_train = []
    for i in range(epochs):
        batch_inds = batch_rng.choice(range(inputs.shape[0]), size=batch_size, replace=False) 
        if i % (epochs // 10) == 0:
            weights_during_train.append(cur_weight)
        weight_grad, softmax_grad,bias_grad = weights_grad_total_func(inputs[batch_inds], cur_weight, cur_soft_weight, 
                                                                      cur_biases, y[batch_inds])
        unsmooth_grad = unsmooth_grad_func(cur_weight)
        cur_weight = cur_weight  - (lrate * weight_grad) 
        cur_soft_weight = cur_soft_weight - (lrate * softmax_grad)
        cur_biases = cur_biases - (lrate * bias_grad)
        data_cost, weight_cost = costs_func(inputs, cur_weight, cur_soft_weight, [0,0], y)
        data_costs.append(data_cost)
        weight_costs.append(weight_cost)
        total_grad_sum.append(np.sum(np.abs(weight_grad)))
        data_grad_sum.append(np.sum(np.abs(weight_grad)) - np.sum(np.abs(unsmooth_grad)))
        unsmooth_grad_sum.append(np.sum(np.abs(unsmooth_grad)))
    # show final weight also
    weights_during_train.append(cur_weight)
        
    return (cur_weight, cur_soft_weight, cur_biases, weights_during_train, data_costs, weight_costs, 
            data_grad_sum, unsmooth_grad_sum)


# In[67]:

(weights_grad_total_func, unsmooth_grad_func, 
 costs_func,forward_func, class_out_func) = create_conv_square_model(unsmooth_weight=0.001)
weight_length = 150

batch_rng =  RandomState(np.uint64(hash('batch')))
cur_soft_weight = np.array([1,-1]).astype(np.float32)
cur_weight = RandomState(np.uint64(hash('huhu'))).randn(weight_length).astype(np.float32) * 0.01
cur_biases = np.array([0,0]).astype(np.float32)
assert len(topo_view_noised) == 100, "or else do proper sklearn fold splitting"
assert len(y) == 100, "or else do proper sklearn fold splitting"
inputs = topo_view_noised[:90]
test_y = y[:90]
epochs = 1500
batch_size = 50
lrate = 0.1
(cur_weight, cur_soft_weight, cur_biases, weights_during_train, data_costs, weight_costs, 
            data_grad_sum, unsmooth_grad_sum) = train_model(inputs, test_y, cur_weight, cur_soft_weight, cur_biases,
                                                            weights_grad_total_func, unsmooth_grad_func, 
                                                            costs_func, lrate=lrate, batch_size=batch_size)

pyplot.figure()
pyplot.plot(cur_weight)
pyplot.figure()
pyplot.plot(np.array(weights_during_train).T)
pyplot.figure()
pyplot.plot(data_costs)
pyplot.plot(weight_costs)
pyplot.legend(('data costs', 'weight costs'))
pyplot.figure()
pyplot.plot(data_grad_sum)
pyplot.plot(unsmooth_grad_sum)
pyplot.legend(('data grad', 'unsmooth grad'))


# ### Train on real kaus data

# In[133]:

from motor_imagery_learning.bbci_pylearn_dataset import (BBCIPylearnCleanFilterbankDataset,
    BBCISetNoCleaner)
clf = LDA()
chan='CP3'
filename = 'data/BBCI-without-last-runs/KaUsMoSc1S001R01_ds10_1-11.BBCI.mat'
kaus_topo, kaus_y = generate_single_sensor_data(filename, chan)
kaus_pooled, kaus_pooled_y = pipeline(kaus_topo, kaus_y, 
    lambda x,y: convolve_with_weight(x, create_sine_signal(samples=150,freq=11, sampling_freq=300.0)),
    lambda x,y: x*x,
    lambda x,y: max_pool_topo_view(x,pool_shape=50))
assert np.array_equal(kaus_y, kaus_pooled_y)
kaus_single_feature = np.sum(kaus_pooled, axis=2, keepdims=True)
print ("topo view pooled", clf.fit_and_score_train_test_topo(kaus_pooled, kaus_y, sklearn_test_fold=True))
print("one feature accuracy", OneFeatureScorer().fit_and_score_train_test_topo(kaus_single_feature, kaus_pooled_y))


# In[147]:

(weights_grad_total_func, unsmooth_grad_func, 
 costs_func,forward_func, class_out_func) = create_conv_square_model(unsmooth_weight=0.001)
weight_length = 150

batch_rng =  RandomState(np.uint64(hash('batch')))
cur_soft_weight = np.array([1,-1]).astype(np.float32)
cur_weight = RandomState(np.uint64(hash('huhu'))).randn(weight_length).astype(np.float32) * 0.01
cur_biases = np.array([0,0]).astype(np.float32)
split_i = 220
kaus_train_topo = kaus_topo[:split_i]
kaus_train_y = kaus_y[:split_i]
kaus_test_topo = kaus_topo[split_i:]
kaus_test_y = kaus_y[split_i:]
epochs = 100
batch_size = 50
lrate = 0.1
(cur_weight, cur_soft_weight, cur_biases, weights_during_train, data_costs, weight_costs, 
            data_grad_sum, unsmooth_grad_sum) = train_model(kaus_train_topo, kaus_train_y, cur_weight, 
                                                            cur_soft_weight, cur_biases,
                                                            weights_grad_total_func, unsmooth_grad_func, 
                                                            costs_func, lrate=lrate, batch_size=batch_size)

pyplot.figure()
pyplot.plot(cur_weight)
pyplot.figure()
pyplot.plot(np.array(weights_during_train).T)
pyplot.figure()
pyplot.plot(data_costs)
pyplot.plot(weight_costs)
pyplot.legend(('data costs', 'weight costs'))
pyplot.figure()
pyplot.plot(data_grad_sum)
pyplot.plot(unsmooth_grad_sum)
pyplot.legend(('data grad', 'unsmooth grad'))


# ## Run pylearn trainer with costs

# In[ ]:

#create model copy from handmade models
# compute costs of model for all ys one
# write own costs
# 


# In[269]:

cost_str = """!obj:pylearn2.costs.cost.SumOfCosts { 
                costs: [
                    !obj:pylearn2.costs.mlp.dropout.Dropout {
                        input_include_probs: { 'bandpass' : $input_first_probs },
                        input_scales: { 'bandpass': $input_first_scales },
                    },
                    !obj:motor_imagery_learning.mypylearn2.unsmooth_cost.UnsmoothCost {
                        coeffs: { 'bandpass' : $unsmooth_coeff },
                    },
                ]
            }"""


# In[270]:

from pylearn2.config import yaml_parse
unsmooth_str = """!obj:motor_imagery_learning.mypylearn2.unsmooth_cost.UnsmoothCost {
                        coeffs: { 'bandpass' : $unsmooth_coeff },
                    }"""
dropout_str = """!obj:pylearn2.costs.mlp.dropout.Dropout {
                        input_include_probs: { 'bandpass' : $input_first_probs },
                        input_scales: { 'bandpass': $input_first_scales },
                    }"""
yaml_parse.load(cost_str)


# In[252]:

from pylearn2.costs.cost import SumOfCosts

unsmooth_cost = UnsmoothCost(dict(conv_squared=0.01))
dropout_cost = Dropout(input_include_probs=dict(conv_squared=0.8), input_scales=dict(conv_squared=1.0/0.8))
summed_costs = SumOfCosts([dropout_cost, unsmooth_cost])
cost = summed_costs
kaus_trainer = create_trainer(topo_view=kaus_topo, y=kaus_y, cost=cost,
                              layers=[ConvSquared(output_channels=30, 
                                 kernel_shape=(150,1), pool_shape=(0,0), pool_type="sumlogall",
                                  layer_name="conv_squared", pool_stride=(0,0), irange=0.01,
                                tied_b=True),
                       Softmax(layer_name="y",n_classes=2, irange=0.01)], max_epochs=100)


# In[253]:

get_ipython().run_cell_magic(u'capture', u'', u'kaus_trainer.run_until_earlystop()')


# In[254]:

kaus_trainer.model.monitor.channels['test_y_misclass'].val_record[-5:]


# In[255]:

plot_sensor_signals(np.squeeze(kaus_trainer.model.get_weights_topo()),
                    sensor_names=map(str, range(len(kaus_trainer.model.get_weights_topo()))))
None


# In[241]:

plot_sensor_signals(np.squeeze(kaus_trainer.model.get_weights_topo()),
                    sensor_names=map(str, range(len(kaus_trainer.model.get_weights_topo()))))
None


# In[256]:

plot_misclasses_for_model(kaus_trainer.model)


# In[171]:

plot_misclasses_for_model(kaus_trainer.model)


# In[282]:




# In[283]:

anwe_model_less_smoothed = serial.load('data/models/two-class/raw-square-smooth//early-stop/1.pkl').model
anwe_model_more_smoothed = serial.load('data/models/two-class/raw-square-smooth//early-stop/5.pkl').model
anwe_model_longer_shape = serial.load('data/models/two-class/raw-square-smooth//early-stop/10.pkl').model


# In[297]:


pyplot.plot(np.fft.rfftfreq(15, d=1/150.0), 
            np.mean((np.abs(np.squeeze(
                np.fft.rfft(anwe_model_less_smoothed.get_weights_topo(), axis=1))) ** 2.0), axis=0))


# In[295]:


pyplot.plot(np.fft.rfftfreq(60, d=1/150.0), 
            np.mean((np.abs(np.squeeze(
                np.fft.rfft(anwe_model_longer_shape.get_weights_topo(), axis=1))) ** 2.0), axis=0))


# In[288]:

fft_weights = 


# In[284]:

_ = plot_sensor_signals(np.squeeze(anwe_model_less_smoothed.get_weights_topo()))
_ = plot_sensor_signals(np.squeeze(anwe_model_more_smoothed.get_weights_topo()))
_ = plot_sensor_signals(np.squeeze(anwe_model_longer_shape.get_weights_topo()))


# ### Some results analysis, maybe not correct code anymore :D

# In[140]:

topo_view_pooled, y_pooled = pipeline(kaus_topo, kaus_y, 
    lambda x,y: forward_func(x, cur_weight)) #lambda x,y: max_pool_topo_view(x,pool_shape=50))
    #lambda x,y: np.log(x)) # unnecessary atleast for lda

print ("topo view pooled", clf.fit_and_score_train_test_topo(topo_view_pooled, y_pooled))
print ("kaus topo lda", clf.fit_and_score_train_test_topo(topo_view_pooled, y_pooled))


# In[141]:

#train ..sth kaputt... check agina...preds dont seem correct
preds = class_out_func(kaus_train_topo, cur_weight, 
                       cur_soft_weight, cur_biases)
sum(np.argmax(kaus_train_y, axis=1) == preds.argmax(axis=1)) / float(len(kaus_train_y))


# In[145]:

#test
preds = class_out_func(kaus_test_topo, cur_weight, 
                       cur_soft_weight, cur_biases)
sum(np.argmax(kaus_test_y, axis=1) == preds.argmax(axis=1)) / float(len(kaus_test_y))


# In[179]:

weight_from_model = np.squeeze(trainer.model.get_weights_topo())
softmax_weights_from_model = trainer.model.layers[1].get_weights_topo()
print softmax_weights_from_model
softmax_biases_from_model = trainer.model.layers[1].get_biases()
print softmax_biases_from_model


# In[352]:

topo_view_pooled, y_pooled = pipeline(start_topo_view, y, 
    lambda x,y: put_noise_on_set(x,weight_old_data=0.05),
    lambda x,y: forward_func(x, weight_from_model)) #lambda x,y: max_pool_topo_view(x,pool_shape=50))
    #lambda x,y: np.log(x)) # unnecessary atleast for lda

print ("topo view pooled", clf.fit_and_score_train_test_topo(topo_view_pooled, y_pooled))


# ## Looking at actual bandpass filters what they look like

# In[423]:

import scipy.signal

from numpy import cos, sin, pi, absolute, arange
from scipy.signal import kaiserord, lfilter, firwin, freqz
from pylab import figure, clf, plot, xlabel, ylabel, xlim, ylim, title, grid, axes, show


#------------------------------------------------
# Create a signal for demonstration.
#------------------------------------------------

sample_rate = 100.0
nsamples = 400
t = arange(nsamples) / sample_rate
x = cos(2*pi*0.5*t) + 0.2*sin(2*pi*2.5*t+0.1) +         0.2*sin(2*pi*15.3*t) + 0.1*sin(2*pi*16.7*t + 0.1) +             0.1*sin(2*pi*23.45*t+.8)


#------------------------------------------------
# Create a FIR filter and apply it to x.
#------------------------------------------------

# The Nyquist rate of the signal.
nyq_rate = sample_rate / 2.0

# The desired width of the transition from pass to stop,
# relative to the Nyquist rate.  We'll design the filter
# with a 5 Hz transition width.
width = 5.0/nyq_rate

# The desired attenuation in the stop band, in dB.
ripple_db = 60.0

# Compute the order and Kaiser parameter for the FIR filter.
N, beta = kaiserord(ripple_db, width)

# The cutoff frequency of the filter.
cutoff_hz = 10.0

# Use firwin with a Kaiser window to create a lowpass FIR filter.
taps = firwin(N, cutoff_hz/nyq_rate, window=('kaiser', beta))
#http://mpastell.com/2010/01/18/fir-with-scipy/
#Lowpass filter
a = firwin(N, cutoff = 0.3, window = 'blackmanharris')
#Highpass filter with spectral inversion
b = - firwin(N, cutoff = 0.4, window = 'blackmanharris'); 
b[N/2] = b[N/2] + 1

#Combine into a bandpass filter
d = - (a+b); d[N/2] = d[N/2] + 1
def create_sine_signal(samples, freq, sampling_freq, shift=0):
    x = (np.arange(0,samples,1) * 2 * np.pi * freq / float(sampling_freq)) + shift
    return np.sin(x)
# fake taps
#taps = create_sine_signal(74, freq=9, sampling_freq=sample_rate) / 10.0
#taps=-taps
#bandpass taps
taps = d * 1
# fake smoothed taps
#taps = final_abs_taps[0] / 2.0

# Use lfilter to filter x with the FIR filter.
filtered_x = lfilter(taps, 1.0, x)

#------------------------------------------------
# Plot the FIR filter coefficients.
#------------------------------------------------

figure(1)
plot(taps, 'bo-', linewidth=2)
title('Filter Coefficients (%d taps)' % N)
grid(True)

#------------------------------------------------
# Plot the magnitude response of the filter.
#------------------------------------------------

figure(2)
clf()
w, h = freqz(taps, worN=8000)
plot((w/pi)*nyq_rate, absolute(h), linewidth=2)
xlabel('Frequency (Hz)')
ylabel('Gain')
title('Frequency Response')
ylim(-0.05, 1.05)
grid(True)

# Upper inset plot.
ax1 = axes([0.42, 0.6, .45, .25])
plot((w/pi)*nyq_rate, absolute(h), linewidth=2)
xlim(0,8.0)
ylim(0.9985, 1.001)
grid(True)

# Lower inset plot
ax2 = axes([0.42, 0.25, .45, .25])
plot((w/pi)*nyq_rate, absolute(h), linewidth=2)
xlim(12.0, 20.0)
ylim(0.0, 0.0025)
grid(True)

#------------------------------------------------
# Plot the original and filtered signals.
#------------------------------------------------

# The phase delay of the filtered signal.
delay = 0.5 * (N-1) / sample_rate

figure(3)
# Plot the original signal.
plot(t, x)
# Plot the filtered signal, shifted to compensate for the phase delay.
plot(t-delay, filtered_x, 'r-')
# Plot just the "good" part of the filtered signal.  The first N-1
# samples are "corrupted" by the initial conditions.
plot(t[N-1:]-delay, filtered_x[N-1:], 'g', linewidth=4)

xlabel('t')
grid(True)

show()


# In[219]:

# Lets try smoothing tas, see if freq filter gets destroyed
final_taps = show_weight_changes(unsmooth_movavg_scaled_grad_func, np.array([taps]).astype(np.float32), 
                                                                                              epochs=500)

