
# coding: utf-8

# In[1]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\n\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n# make sure to use cpu for printing results...\nos.environ[\'THEANO_FLAGS\'] = "floatX=float32,device=cpu,nvcc.fastmath=True"\n%load_ext autoreload\n%autoreload 2\nimport matplotlib\nfrom matplotlib import pyplot\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (12.0, 5.0)\nmatplotlib.rcParams[\'font.size\'] = 7\nfrom analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file\nfrom motor_imagery_learning.analysis.print_results import ResultPrinter\nfrom motor_imagery_learning.csp.print_csp_results import CSPResultPrinter\nfrom pylearn2.utils import serial\nimport numpy as np\nfrom motor_imagery_learning.analysis.plot_util import (plot_nlls_for_file, \n    plot_misclasses_for_result, plot_misclasses_for_file, plot_nlls)\nfrom motor_imagery_learning.analysis.results import ResultPool, DatasetAveragedResults\nfrom glob import glob\nimport datetime\nimport seaborn\nseaborn.set_style(\'darkgrid\')\nseaborn.set_context("notebook", rc={"lines.linewidth": 0.5})\nfrom motor_imagery_learning.analysis.results import compute_confusion_matrix\nfrom motor_imagery_learning.analysis.results import compute_confusion_matrix_csp\nfrom motor_imagery_learning.analysis.plot_util import plot_confusion_matrix\nfrom matplotlib import cm')


# # Load features! :)

# -> 91-108 are the important experiments

# In[4]:

CSPResultPrinter('data/models/final-eval/csp-standardized/').print_results()


# In[6]:

from pylearn2.utils import serial


# In[7]:

filename = 'data/models/final-eval/csp-standardized/91.pkl'
csp_trainer = serial.load(filename)


# In[8]:

csp_trainer.binary_csp.train_feature_full_fold.shape


# In[12]:

csp_trainer.binary_csp.train_feature_full_fold[0,0,0].data.shape


# In[65]:

X_train, y_train = load_features_labels(csp_trainer.binary_csp.train_feature_full_fold,
                                       csp_trainer.binary_csp.train_labels_full_fold)


# In[64]:

X_test, y_test = load_features_labels(csp_trainer.binary_csp.test_feature_full_fold,
                                       csp_trainer.binary_csp.test_labels_full_fold)


# In[59]:

def load_features_labels(features, labels):
    """ Given full fold features labels are still separated by filterband, classifier pair etc."""
    assert np.array_equal([35,1,6], features.shape), "Should have 35 filtbands, single fold, 6 classpairs."
    assert np.array_equal([1], labels.shape), "Should have just a single fold."
    y = labels[0]
    
    # Check all have features same shape
    for i_filt_band in range(35):
        for i_class_pair in range(6):
            assert np.array_equal(features[0,0,0].data.shape,
                features[i_filt_band,0,i_class_pair].data.shape)

    # initialize with nans
    X = np.nan * np.ones((features[0,0,0].data.shape[0],
                 features[0,0,0].data.shape[1] * np.prod(features.shape)),
                     dtype=np.float32)
    for i_filt_band in range(35):
        for i_class_pair in range(6):
            start_ind = i_filt_band * 6 * 10 + i_class_pair * 10
            X[:, start_ind:start_ind+10] = features[i_filt_band,0,i_class_pair].data
    # check for no nans
    assert not (np.any(np.isnan(X)))
    return X,y


# ## Load all

# In[94]:

clf = LogisticRegression()
all_old_accs = []
all_new_accs = []
for i_file in xrange(91,109):
    filename = 'data/models/final-eval/csp-standardized/' + str(i_file) + '.pkl'
    csp_trainer = serial.load(filename)
    print "Training {:20s}".format(shorten_dataset_name(csp_trainer.filename))
    X_train, y_train = load_features_labels(csp_trainer.binary_csp.train_feature_full_fold,
                                       csp_trainer.binary_csp.train_labels_full_fold)
    X_test, y_test = load_features_labels(csp_trainer.binary_csp.test_feature_full_fold,
                                       csp_trainer.binary_csp.test_labels_full_fold)
    clf.fit(X_train, y_train)
    old_acc = csp_trainer.multi_class.test_accuracy[0]
    new_acc = clf.score(X_test, y_test)
    print ("Master Thesis Accuracy: {:5.2f}%".format(old_acc* 100))
    print ("New Accuracy:           {:5.2f}%".format(new_acc* 100))
    all_old_accs.append(old_acc)
    all_new_accs.append(new_acc)
    
    
print("Master Thesis average(std): {:5.2f}% ({:5.2f}%)".format(
        np.mean(all_old_accs) * 100, np.std(all_old_accs) * 100))
print("New           average(std): {:5.2f}% ({:5.2f}%)".format(
        np.mean(all_new_accs) * 100, np.std(all_new_accs) * 100))


# In[78]:

import re
dataset_name = csp_trainer.filename
def shorten_dataset_name(dataset_name):
    dataset_name = re.sub(r"(./)?data/[^/]*/", '', str(dataset_name))
    dataset_name = re.sub(r"MoSc[0-9]*S[0-9]*R[0-9]*_ds10_", '',
        dataset_name)
    dataset_name = re.sub("BBCI.mat", '', dataset_name)

    return dataset_name


# In[ ]:

csp_trainer.


# In[46]:

import sklearn
from sklearn.linear_model import LogisticRegression
clf = LogisticRegression()


# In[56]:

clf.fit(X_train[:, 0:1000], y_train)



# In[57]:

clf.score(X_test[:, 0:1000], y_test)

