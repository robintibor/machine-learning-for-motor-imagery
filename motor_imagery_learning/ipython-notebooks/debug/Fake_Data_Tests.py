
# coding: utf-8

# In[5]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import numpy as np
from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import ConvElemwise
from theano.sandbox.cuda.dnn import dnn_conv
from theano.sandbox.cuda.basic_ops import gpu_alloc_empty
from motor_imagery_learning.analysis.print_results import ResultPrinter
from pylearn2.utils import serial

import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7


# In[ ]:

# Load trainer raw data and raw data spatial
# Create fake data:
# First simplest dataset low values class 1, high values class 2
# run with softmax, see if everything works fine
# Second, slightly more advanced set:
#   first just two "sources" and two filters
#   certain frequency is on for source 1 for class 1, source 2 for class 2
#   (, no source for class 3, source 1 and 2 for class 4)

# run it with softmax, hopefully see no clear separation
# run it with something like raw data net
# if perfectly predicting test, increase noise levels
# noise can be uniform gaussian, only applied to certain times, add to or completely replace signal
# be appplied in same way to all sensors or differently to different sensors
# try to duplicate difference by adding more sensors, more frequencies (i.e. more frequncy-sensor combinations)
# more complicated relationships, more noise, maybe adding sth like a motor related potential also indicative of class
# maybe also permute some trials, so some trials for class 1 get class 2 label

# also, if u can recreate problem or not, try to make visible the training phase what happens, what can make it fail?


# In[25]:

import numpy as np
import scipy

freq = 10
samplerate = 150
period = samplerate/float(freq)

source1 = np.sin(np.linspace(0,150,150) * 2 * np.pi / period)
source2 = np.sin(np.linspace(0,150,150) * 2 * np.pi / (period/2.0))


# In[26]:

pyplot.plot(source1 + source2)

