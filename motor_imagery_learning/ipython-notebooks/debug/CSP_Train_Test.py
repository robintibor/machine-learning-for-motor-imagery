
# coding: utf-8

# In[2]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
#os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import numpy as np
from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import ConvElemwise
from theano.sandbox.cuda.dnn import dnn_conv
from theano.sandbox.cuda.basic_ops import gpu_alloc_empty
from motor_imagery_learning.analysis.print_results import ResultPrinter
from pylearn2.utils import serial

from copy import deepcopy
from motor_imagery_learning.train_scripts.train_experiments import ExperimentsRunner
from motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer
from motor_imagery_learning.analysis.data_generation import (create_sine_signal,
    pipeline, create_shifted_sines,put_noise, convolve_with_weight,
    max_pool_topo_view, log_sum_pool,generate_single_sensor_data, 
    add_timecourse, spread_to_sensors, SpreadedSinesDataset)
from motor_imagery_learning.analysis.util import create_trainer
from motor_imagery_learning.mypylearn2.conv_squared import ConvSquared, ConvSquaredFullHeight
from motor_imagery_learning.analysis.input_reconstruction import (
    create_rand_input, gradient_descent_input, create_input_grad_for_wanted_class_func,
    create_input_grad_with_smooth_penalty_for_wanted_class_func, reconstruct_inputs_for_class,
    power_smoothness_cost_moving_avg_scaled)
from pylearn2.config import yaml_parse
from motor_imagery_learning.analysis.util import bps_and_freqs
from motor_imagery_learning.analysis.deconv import deconv_model
from motor_imagery_learning.analysis.plot.raw_signals import plot_sensor_signals_two_classes

import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (8.0, 1.0)
matplotlib.rcParams['font.size'] = 7

from motor_imagery_learning.analysis.plot_util import plot_misclasses_for_model, plot_nlls_for_model, plot_sensor_signals
import scipy
import seaborn
seaborn.set_style('darkgrid')
from motor_imagery_learning.analysis.util import create_activations_func
from motor_imagery_learning.train_scripts.train_with_params import create_training_object_from_file
from motor_imagery_learning.analysis.util import create_prediction_func
from motor_imagery_learning.analysis.plot_util import plot_head_signals_tight_multiple_signals
from motor_imagery_learning.analysis.plot_util import plot_head_signals, plot_head_signals_tight
from motor_imagery_learning.analysis.plot_util import plot_head_signals_tight_two_signals


# In[3]:

bbci_set = BBCIDataset("data/BBCI-without-last-runs/AnWeMoSc1S001R01_ds10_1-12.BBCI.mat",
                       sensor_names=['C2', 'C4'])

bbci_set.load_continuous_signal()
bbci_set.add_markers()


# In[4]:

bbci_set2 = BBCIDataset("data/BBCI-without-last-runs/GuJoMoSc01S001R01_ds10_1-11.BBCI.mat",
                       sensor_names=['C2', 'C4'])

bbci_set2.load_continuous_signal()
bbci_set2.add_markers()


# In[5]:

from wyrm.processing import append_cnt


# In[7]:

merged_cnt = append_cnt(bbci_set2.cnt, bbci_set.cnt)


# In[13]:

from mywyrm.processing import segment_dat_fast

first_epo = segment_dat_fast(bbci_set.cnt, 
            marker_def={'1 - Right Hand': [1], '2 - Left Hand': [2], 
                '3 - Rest': [3], '4 - Feet': [4]}, 
            ival=[0,4000])
second_epo = segment_dat_fast(bbci_set2.cnt, 
            marker_def={'1 - Right Hand': [1], '2 - Left Hand': [2], 
                '3 - Rest': [3], '4 - Feet': [4]}, 
            ival=[0,4000])
merged_epo = segment_dat_fast(merged_cnt, 
            marker_def={'1 - Right Hand': [1], '2 - Left Hand': [2], 
                '3 - Rest': [3], '4 - Feet': [4]}, 
            ival=[0,4000])


# In[50]:

from motor_imagery_learning.csp.train_csp import CSPTrain, generate_filterbank

from wyrm.processing import select_channels
from motor_imagery_learning.mywyrm.processing import resample_cnt, highpass_cnt
import itertools
import logging
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


# ## Compare trials of csp and fft net

# In[11]:

#
#data/models/two-files-split/csp-net/C-sensors/early-stop/


# In[13]:

ls data/models/two-files-split/fft/early-stop/


# In[17]:

for i_file in xrange(1,19):
    csp_result = serial.load('data/models/csp/two_file_split/full/' + str(i_file) + '.result.pkl')

    assert len(csp_result.multi_class.test_labels[0]) == len(csp_result.multi_class.test_predicted_labels[0])
    assert len(csp_result.multi_class.train_labels[0]) == len(csp_result.multi_class.train_predicted_labels[0])
    test_csp = csp_result.multi_class.test_labels[0]
    train_csp = csp_result.multi_class.train_labels[0]
    
    fft_result = serial.load('data/models/two-files-split/csp-net/all_EEG_sensors/early-stop/' + str(i_file) + '.result.pkl')
    test_fft = np.argmax(fft_result.targets['test'], axis=1)
    train_fft = np.argmax(fft_result.targets['train'], axis=1)
    assert len(fft_result.targets['train']) == len(fft_result.predictions['train'])
    assert len(fft_result.targets['test']) == len(fft_result.predictions['test'])
    assert np.array_equal(test_fft, test_csp)
    assert np.array_equal(train_fft, train_csp)


# ## Fix Cleaning

# In[ ]:

## load the two jobe sets 


# In[136]:

a = [1,2,3,67,67,68,9]
a = np.array(a)
a[a > 8]


# In[166]:

from motor_imagery_learning.mywyrm.clean import BBCITwoSetsCleaner


# In[163]:

jobe_first_filename = 'data/BBCI-without-last-runs/AnWeMoSc1S001R01_ds10_1-12.BBCI.mat'
jobe_second_filename = 'data/BBCI-only-last-runs/AnWeMoSc1S001R13_ds10_1-2BBCI.mat'


jobe_set_1 = BBCIDataset(jobe_first_filename)
jobe_set_1.load_continuous_signal()
jobe_set_1.add_markers()
jobe_set_1.segment_into_trials()


# In[164]:


jobe_set_2 = BBCIDataset(jobe_second_filename)
jobe_set_2.load_continuous_signal()
jobe_set_2.add_markers()
jobe_set_2.segment_into_trials()


# In[174]:

cleaner = BBCITwoSetsCleaner(second_filename=jobe_second_filename, load_sensor_names=None)

clean_result = cleaner.clean(jobe_set_1.cnt, jobe_first_filename)


# In[175]:

cleaner.rejected_trials_second


# In[176]:

cleaner.clean_trials_second


# In[153]:

assert np.intersect1d(clean_result[1], clean_result[2]).size == 0


# In[156]:




# # Investigate Jobe Crash

# In[199]:

import motor_imagery_learning.csp.train_csp
motor_imagery_learning.csp.train_csp.log.setLevel('DEBUG')


# In[200]:

train_string = open('data/models/csp/two_file_split/short-bands/6.yaml').read()
train_dict = yaml_parse.load(train_string)
train_obj = train_dict['train_obj']


# In[201]:

train_obj.run()


# In[191]:


epo = segment_dat_fast(train_obj.cnt, 
           marker_def={'1 - Right Hand': [1], '2 - Left Hand': [2], 
               '3 - Rest': [3], '4 - Feet': [4]}, 
           ival=[0,4000])


# In[194]:

variances = np.var(epo.data, axis=1)


# In[198]:

train_obj.rejected_trials


# In[197]:

pyplot.plot(variances)


# In[ ]:




# In[185]:

train_obj.clean_trials

