
# coding: utf-8

# In[1]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\n\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n# make sure to use cpu for printing results...\nos.environ[\'THEANO_FLAGS\'] = "floatX=float32,device=cpu,nvcc.fastmath=True"\n%load_ext autoreload\n%autoreload 2\nimport matplotlib\nfrom matplotlib import pyplot\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (12.0, 5.0)\nmatplotlib.rcParams[\'font.size\'] = 7\nfrom analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file\nfrom motor_imagery_learning.analysis.print_results import ResultPrinter\nfrom motor_imagery_learning.csp.print_csp_results import CSPResultPrinter\nfrom pylearn2.utils import serial\nimport numpy as np\nfrom motor_imagery_learning.analysis.plot_util import (plot_nlls_for_file, \n    plot_misclasses_for_result, plot_misclasses_for_file, plot_nlls)\nfrom motor_imagery_learning.analysis.results import ResultPool, DatasetAveragedResults\nfrom glob import glob\nimport datetime\nimport seaborn\nseaborn.set_style(\'darkgrid\')\nseaborn.set_context("notebook", rc={"lines.linewidth": 0.5})\nfrom motor_imagery_learning.analysis.results import compute_confusion_matrix\nfrom motor_imagery_learning.analysis.results import compute_confusion_matrix_csp\nfrom motor_imagery_learning.analysis.plot_util import plot_confusion_matrix\nfrom matplotlib import cm')


# In[25]:

ResultPrinter('data/models/hyperopt-debug/raw-net-hypopt-params/early-stop/').print_results()


# In[35]:

ResultPrinter('data/models/hyperopt-debug/raw-net-hypopt-cv/').print_results()


# In[16]:

ResultPrinter('data/models/hyperopt-debug/raw-net/early-stop/').print_results()


# In[24]:

ResultPrinter('data/models/hyperopt-debug/raw-net-2-hypopt/early-stop/').print_results()


# In[37]:

ResultPrinter('data/models/hyperopt-debug/fb-net-old/').print_results()


# In[38]:

ResultPrinter('data/models/hyperopt-debug/fb-net/').print_results()

