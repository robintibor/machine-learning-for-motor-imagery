
# coding: utf-8

# In[56]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import numpy as np
from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import ConvElemwise
from theano.sandbox.cuda.dnn import dnn_conv
from theano.sandbox.cuda.basic_ops import gpu_alloc_empty
from motor_imagery_learning.analysis.print_results import ResultPrinter
from pylearn2.utils import serial

import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (8.0, 2.0)
matplotlib.rcParams['font.size'] = 7

from motor_imagery_learning.analysis.plot_util import plot_misclasses_for_model, plot_nlls_for_model
import scipy
import seaborn
seaborn.set_style('darkgrid')
from copy import deepcopy


# ## Understanding gradients of bandpasscreated weight

# In[55]:

# First lets look at gradient for straight line created by parameter

curve = T.fvector()
freq = T.scalar()
x = T.arange(-(curve.shape[0] // 2), (curve.shape[0] // 2) + 1)
reconstructed_curve = T.sin(freq* x)

diff = T.square(curve - reconstructed_curve)
cost = T.sum(diff)

grad = T.grad(cost, freq)

grad_func = theano.function([curve, freq], grad, allow_input_downcast=True)
"""
diff = abs(line - reconstructed_line)
cost = T.sum(diff)

grad = T.grad(cost, slope)

grad_abs_func = theano.function([line, slope], grad, allow_input_downcast=True)"""


# In[106]:

grads = [grad_func(line, i) for i in np.linspace(0,1,100)]
pyplot.plot(np.linspace(0,1,100), grads)


# In[103]:

x = np.arange(-200,201)
line = np.sin(0.5*x)

pyplot.plot(np.sin(0.5 * x))
pyplot.plot(np.sin(0.7 * x))


# ## with straight line

# In[108]:

# First lets look at gradient for straight line created by parameter

line = T.fvector()
slope = T.scalar()
x = T.arange(-(line.shape[0] // 2), (line.shape[0] // 2) + 1)
reconstructed_line = slope * x

diff = T.square(line - reconstructed_line)
cost = T.sum(diff)

grad = T.grad(cost, slope)

grad_func = theano.function([line, slope], grad, allow_input_downcast=True)

diff = abs(line - reconstructed_line)
cost = T.sum(diff)

grad = T.grad(cost, slope)

grad_abs_func = theano.function([line, slope], grad, allow_input_downcast=True)


# In[109]:

x = np.arange(-3,4)

line = 4 * x
grad_func(line, 3)

# 4x is -12,-8,-4,0,4,8,12
# 3x is  -9,-6,-3,0,3,6,9
# diff    3, 2, 1,0,1,2,3 => sum is 12
# diff*2  6, 4, 2,0,2,4,6 => sum is 24
# diff**2 9, 4, 1,0,1,4,9 => sum is 28 => *2 => 56


# In[110]:


grad_abs_func(line, 3)


# ## Looking at trained models

# In[136]:

model = serial.load('data/models/two-class/raw-square-bp-no-full//early-stop/48.pkl').model
bp_ws = np.squeeze(model.get_weights_topo())
bp_ws = np.squeeze(bp_ws)
start_len = bp_ws
nyq = 150 / 2.0
for i, (start, length) in enumerate(start_len):
    pyplot.plot([i,i], [nyq * start, nyq *  (start+length)])
weights = np.sum(np.abs(model.layers[1].get_weights_topo()), axis=(0,1,2))
pyplot.plot(weights)


# In[3]:

model = serial.load('data/models/two-class/raw-square-bp-no-full/test/63.pkl').model
bp_ws = np.squeeze(model.get_weights_topo())
bp_ws = np.squeeze(bp_ws)
start_len = bp_ws
nyq = 150 / 2.0
for i, (start, length) in enumerate(start_len):
    pyplot.plot([i,i], [nyq * start, nyq *  (start+length)])


# ## Train on Fakedata

# In[116]:

from motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer
from motor_imagery_learning.analysis.data_generation import (create_sine_signal,
    SingleChanDataset, pipeline, create_shifted_sines,put_noise_on_set, convolve_with_weight,
    max_pool_topo_view, log_sum_pool)

input_shape=(300,1,600,1)
sampling_freq = 150.0
start_topo_view, y = create_shifted_sines(input_shape, RandomState(np.uint64(hash("pooled_pipeline"))),
                                         sampling_freq=sampling_freq)

nyq_rate = sampling_freq / 2.0
topo_view_noised, y = pipeline(start_topo_view, y, 
    lambda x,y: put_noise_on_set(x,weight_old_data=0.05))
topo_view_bp_pooled, y = pipeline(start_topo_view, y, 
    lambda x,y: put_noise_on_set(x,weight_old_data=0.05),
    lambda x,y: convolve_with_weight(x, fir_taps_func(10.5 / nyq_rate, 11.5/nyq_rate)),
    lambda x,y: x*x,
    lambda x,y: log_sum_pool(x, 50))

topo_view_sin_pooled, y = pipeline(start_topo_view, y, 
    lambda x,y: put_noise_on_set(x,weight_old_data=0.05),
    lambda x,y: convolve_with_weight(x, create_sine_signal(50, freq=11.0, sampling_freq=150.0)),
    lambda x,y: x*x,
    lambda x,y: log_sum_pool(x, 50))
topo_view_noised.shape


# In[117]:

clf = LDA()
print clf.fit_and_score_train_test_topo(topo_view_noised, y)
print clf.fit_and_score_train_test_topo(topo_view_sin_pooled, y)
print clf.fit_and_score_train_test_topo(topo_view_bp_pooled, y)


# In[308]:

from motor_imagery_learning.mypylearn2.conv_squared import ConvSquaredSwitchAxes,ConvSquared
from motor_imagery_learning.mypylearn2.conv_bandpass import ConvBandpassSwitchAxes, init_bp_transformer
from motor_imagery_learning.mypylearn2.conv_switch_axes import ConvSwitchAxes
from pylearn2.models.mlp import RectifierConvNonlinearity, ConvRectifiedLinear
from motor_imagery_learning.analysis.util import create_trainer
from pylearn2.datasets.preprocessing import Pipeline
from motor_imagery_learning.mypylearn2.preprocessing import OnlineChannelwiseStandardize
layers = [
          ConvSquaredSwitchAxes(layer_name="bpconv_square",
                     output_channels=2,
                     irange=0.5,
                     kernel_shape=[50,1],
                     kernel_stride=[1,1],
                     pool_type="sumlogall",
                     pool_shape=[50, 1],
                     pool_stride=[50, 1],
                     max_kernel_norm=2.0,
                     switch_axes=[['b', 'c', 0, 1], ['b', 1, 0, 'c']],
                     transformer_func=init_bp_transformer),
          Softmax(n_classes=2, irange=0.05, layer_name="y")
]
trainer = create_trainer(topo_view=topo_view_noised, y=y, layers=layers,
               cost=None, max_epochs=500, preprocessor=OnlineChannelwiseStandardize())


# In[309]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer._create_early_stop_trainer()\ntrainer.early_stop_trainer.setup()')


# In[302]:

trainer.early_stop_trainer.algorithm.termination_criterion._max_epochs = 200


# In[337]:

# fix weights to good values (first one good, second irrelevant)
#trainer.model.layers[0].get_params()[0].set_value(np.float32([[[[0.135]], [[0.02]]], [[[0.285]], [[0.01]]]]))
# fix a bit too far up.. does it move down=?
trainer.model.layers[0].get_params()[0].set_value(np.float32([[[[0.17]], [[0.02]]], [[[0.285]], [[0.01]]]]))
# fix a bit too far down.. does it move up?
trainer.model.layers[0].get_params()[0].set_value(np.float32([[[[0.09]], [[0.02]]], [[[0.285]], [[0.01]]]]))


# In[303]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer.early_stop_trainer.run_training()')


# In[289]:

trainer.early_stop_trainer.run_callbacks_and_monitoring()


# In[304]:

plot_misclasses_for_model(trainer.model)


# ## Stepwise analysis

# In[315]:

train_set = trainer.algorithm.monitoring_dataset['train']
train_topo = train_set.get_topological_view().astype(np.float32)
train_y = train_set.y.astype(np.float32)
test_set = trainer.algorithm.monitoring_dataset['test']
test_topo = test_set.get_topological_view().astype(np.float32)
test_y = test_set.y.astype(np.float32)


# In[311]:

topo_in = T.ftensor4()
y_in = T.fmatrix()
cost = trainer.algorithm.cost.expr(trainer.model, (topo_in, y_in))

grads, updates = trainer.algorithm.cost.get_gradients(trainer.model, (topo_in, y_in))

cost_func = theano.function([topo_in, y_in], cost)

outputs = []


for grad in grads:
    outputs.append(grads[grad])

cost_grad_func = theano.function([topo_in, y_in], outputs)
activations = trainer.model.fprop(topo_in, return_all=True)
fprop_func = theano.function([topo_in], activations)
grad_keys = grads.keys()
grad_param_and_vals = lambda topo, y: zip(grad_keys, cost_grad_func(topo, y))


# In[312]:

#(bp_w, bp_b), (bp_conv_w, bp_conv_b), (softmax_b, softmax_w) = [l.get_params() for l in trainer.model.layers]
(bp_w, bp_b), (softmax_b, softmax_w) = [l.get_params() for l in trainer.model.layers]


# In[306]:

def plot_w_and_grad(weights, grads):
    w = np.squeeze(weights)
    g = np.squeeze(grads)
    print g.shape
    print w.shape
    assert w.ndim == 2
    assert g.shape == w.shape
    w_and_g = np.array([w,g]).transpose(1,2,0)
    _ = plot_sensor_signals(w_and_g, yticks="onlymax")


# In[313]:

params_during_training = []
grads_during_training = []


# In[374]:

get_ipython().run_cell_magic(u'capture', u'', u"lrate = 0.1\nbatch_rand = RandomState(np.uint64(hash('batch_inds')))\nfor i in xrange(5000):\n    start = 0\n    batch_size=50\n    batch_inds = batch_rand.choice(range(len(train_topo)), size=batch_size, replace=False)\n    grads = grad_param_and_vals(train_topo[batch_inds], train_y[batch_inds])\n    #bp_w_grad,bp_b_grad, bp_conv_w_grad, bp_conv_b_grad, softmax_b_grad, softmax_w_grad = [np.array(g[1]) for g in grads]\n    bp_w_grad,bp_b_grad, softmax_b_grad, softmax_w_grad = [np.array(g[1]) for g in grads]\n\n    \n    params_during_training.append(deepcopy(trainer.model.get_params()))\n    grads_during_training.append(deepcopy(grads))\n    bp_w.set_value(bp_w.get_value() - lrate * bp_w_grad)\n    bp_b.set_value(bp_b.get_value() - lrate * bp_b_grad)\n    #bp_conv_w.set_value(bp_conv_w.get_value() - lrate * bp_conv_w_grad)\n    #bp_conv_b.set_value(bp_conv_b.get_value() - lrate * bp_conv_b_grad)\n    softmax_w.set_value(softmax_w.get_value() - lrate * softmax_w_grad)\n    softmax_b.set_value(softmax_b.get_value() - lrate * softmax_b_grad)\n    #trainer.early_stop_trainer.run_callbacks_and_monitoring()")


# In[376]:

get_ipython().run_cell_magic(u'capture', u'', u"trainer.early_stop_trainer.run_callbacks_and_monitoring()\ntrainer.model.monitor.channels['train_y_misclass'].val_record[-1]")


# In[375]:

bp_ws  = np.squeeze([W.get_value() for W in np.array(params_during_training)[:, 0]])
bp_ws = np.squeeze(bp_ws)
for epoch in range(500, len(bp_ws), 1000):
    start_len = bp_ws[epoch]
    nyq = 150 / 2.0
    for i, (start, length) in enumerate(start_len):
        pyplot.plot([epoch,epoch], [nyq * start, nyq *  (start+length)])


# 
# ## Tests with bandpass filters in theano

# In[148]:

numtaps = 50
# Build up the coefficients.
alpha = 0.5 * (numtaps - 1)
m = np.arange(0, numtaps) - alpha
h = 0 
left = 0.21
right = 0.31
h += right * np.sinc(right * m)
h -= left * np.sinc(left * m)
win = scipy.signal.get_window('blackmanharris', numtaps, fftbins=False)
#h *= win
pyplot.plot(h)


# In[ ]:

theano.scan()


# In[ ]:

start_stops = T.fmatrix


# In[186]:

def create_all_filters_all_bp_coeffs_start_len(start_len_per_filter, numtaps):
    # Generate the components of the polynomial
    start_len_per_filter = T.minimum(T.maximum(start_len_per_filter, 0), 1)
    start_stops_per_filter = T.set_subtensor(start_len_per_filter[:,:,1],
                                             start_len_per_filter[:,:,0] + start_len_per_filter[:,:,1])
    start_stops_per_filter = T.minimum(start_stops_per_filter, 1)
    components = create_all_filters_all_bp_coeffs(start_stops_per_filter, numtaps)
    return components

def create_all_bp_coeffs_start_len(start_len_per_filter, numtaps):
    # Generate the components of the polynomial
    start_len_per_filter = T.minimum(T.maximum(start_len_per_filter, 0), 1)
    start_stops_per_filter = T.set_subtensor(start_len_per_filter[:,1],
        start_len_per_filter[:,0] + start_len_per_filter[:,1])
    start_stops_per_filter = T.minimum(start_stops_per_filter, 1)
    components = create_all_bp_coeffs(start_stops_per_filter, numtaps)
    return components

def create_all_filters_all_bp_coeffs(start_stops_per_filter, numtaps):
    # Generate the components of the polynomial
    components, updates = theano.scan(fn=lambda start_stops: create_all_bp_coeffs(start_stops, numtaps),
                                      outputs_info=None,
                                      sequences=[start_stops_per_filter])
    return components

def create_all_bp_coeffs(start_stops, numtaps):
    # Generate the components of the polynomial
    components, updates = theano.scan(fn=lambda start_stop: create_bp_coeffs(start_stop[0], start_stop[1], numtaps),
                                      outputs_info=None,
                                      sequences=[start_stops])
    return components

def create_bp_coeffs(start, stop, numtaps):
    alpha = 0.5 * (numtaps - 1)
    m = np.arange(0, numtaps) - alpha
    coeffs = 0 

    coeffs -= start * T.sin(start * m * np.pi) / (start * m * np.pi) 
    coeffs += stop * T.sin(stop * m * np.pi) / (stop * m * np.pi)
    return coeffs

start = T.fscalar()
stop = T.fscalar()
coeffs = create_bp_coeffs(start, stop, numtaps)
fir_taps_func = theano.function([start, stop], coeffs)
start_stops = T.fmatrix()
all_coeffs = create_all_bp_coeffs(start_stops, numtaps)
all_fir_taps_func = theano.function([start_stops], all_coeffs)

start_stops_per_filter = T.ftensor3()
all_filter_coeffs = create_all_filters_all_bp_coeffs(start_stops_per_filter, numtaps)

all_filters_fir_taps_func = theano.function([start_stops_per_filter], all_filter_coeffs)


start_len_per_filter = T.fmatrix()
all_start_len_coeffs = create_all_bp_coeffs_start_len(start_len_per_filter, numtaps)

all_filters_start_len_fir_taps_func = theano.function([start_len_per_filter], all_start_len_coeffs)

cost = T.sum(all_start_len_coeffs)
grad_start_len = T.grad(cost, start_len_per_filter)
grad_start_len_func = theano.function([start_len_per_filter], grad_start_len)


# In[194]:

all_filts.shape


# In[193]:

all_filts = all_filters_start_len_fir_taps_func(np.float32([[0.21, 0.1], [0.61, 0.2]]))

pyplot.plot(all_filts.T)


# In[189]:

all_grads = grad_start_len_func(np.float32([[0.21, 0.1], [0.61, 0.2]]))


pyplot.plot(all_grads.T)


# In[184]:

all_filts.shape


# In[183]:

all_filts = all_filters_start_len_fir_taps_func(np.float32([[[0.21, 0.1], [0.61, 0.2]], 
                                                  [[0.001, 0.309], [0.61, 0.2]],
                                                 [[0.21, 0.1], [0.61, 0.2]]]))

pyplot.plot(all_filts[1].T)


# In[172]:

all_filts = all_filters_fir_taps_func(np.float32([[[0.21, 0.31], [0.61, 0.81]], 
                                                  [[0.001, 0.31], [0.61, 0.81]],
                                                 [[0.21, 0.31], [0.61, 0.81]]]))

pyplot.plot(all_filts[1].T)


# In[156]:

pyplot.plot(all_fir_taps_func(np.float32([[0.21, 0.31], [0.61, 0.81]])).T)


# In[136]:

coefficients = theano.tensor.vector("coefficients")
x = T.scalar("x")

max_coefficients_supported = 10000

# Generate the components of the polynomial
components, updates = theano.scan(fn=lambda coefficient, power, free_variable: coefficient * (free_variable ** power),
                                  outputs_info=None,
                                  sequences=[coefficients, theano.tensor.arange(max_coefficients_supported)],
                                  non_sequences=x)
# Sum them up
polynomial = components.sum()

# Compile a function
calculate_polynomial = theano.function(inputs=[coefficients, x], outputs=polynomial)

# Test
test_coefficients = np.asarray([1, 0, 2], dtype=np.float32)
test_value = 3
print calculate_polynomial(test_coefficients, test_value)
print 1.0 * (3 ** 0) + 0.0 * (3 ** 1) + 2.0 * (3 ** 2)


# In[149]:

bp_start_stops = T.fmatrix()
# Generate the components of the polynomial
components, updates = theano.scan(fn=lambda bp_start_stop: create_bp_coeffs(bp_start_stop[0], bp_start_stop[1], numtaps),
                                  outputs_info=None,
                                  sequences=[bp_start_stops])


# Compile a function
scan_bp_coeffs = theano.function(inputs=[bp_start_stops], outputs=components)


# In[150]:

pyplot.plot(scan_bp_coeffs(np.float32([[0.21, 0.31], [0.61, 0.81]])).T)


# In[22]:

cost = T.sum(h)
grad_cost = T.grad(cost, [start, stop])
grad_taps_func = theano.function([start, stop], grad_cost)


# In[113]:

pyplot.plot(fir_taps_func(0.21, 0.31))


# In[107]:

taps=h
pyplot.figure(2)
sample_rate = 150.0
# The Nyquist rate of the signal.
nyq_rate = sample_rate / 2.0
w, h = scipy.signal.freqz(taps, worN=8000)
pyplot.plot((w/np.pi)*nyq_rate, np.absolute(h), linewidth=2)
pyplot.xlabel('Frequency (Hz)')
pyplot.ylabel('Gain')
pyplot.title('Frequency Response')
pyplot.ylim(-0.05, 1.05)
pyplot.grid(True)

