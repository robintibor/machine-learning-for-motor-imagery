
# coding: utf-8

# In[1]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import numpy as np
from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import ConvElemwise
from theano.sandbox.cuda.dnn import dnn_conv
from theano.sandbox.cuda.basic_ops import gpu_alloc_empty
from motor_imagery_learning.analysis.print_results import ResultPrinter
from motor_imagery_learning.analysis.plot_util import plot_chan_matrices
from motor_imagery_learning.mypylearn2.preprocessing import Standardize
from motor_imagery_learning.bbci_pylearn_dataset import compute_power_spectra, BBCISetNoCleaner
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset

from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from pylearn2.models.mlp import MLP, Softmax
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
from numpy.random import RandomState
import numpy as np
from pylearn2.format.target_format import OneHotFormatter
from pylearn2.termination_criteria import EpochCounter
from motor_imagery_learning.mypylearn2.dataset_splitters import DatasetSingleFoldSplitter, DatasetTwoFileSingleFoldSplitter
from copy import deepcopy
from motor_imagery_learning.analysis.util import  get_dataset_predictions

import matplotlib
from matplotlib import pyplot
from matplotlib import cm
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (8.0, 1.0)
matplotlib.rcParams['font.size'] = 7
from motor_imagery_learning.analysis.plot_util import plot_misclasses_for_model


# ## Things to try on real data

# * Max Pooling instead of sumlog?

# ## Problems at start of testset kaus

# In[2]:

# for kaus trials 1vs3 at start seemed suspiciously badly predicted (trials 0-4, all predicted as right hand class,
# even for rest trials 1,2 and 4)
# however when looking at them, they do rly not seem to show a lot of mu waves

from motor_imagery_learning.train_scripts.train_with_params import create_training_object_from_file
kaus_train = create_training_object_from_file('data/models/two-class/raw-square/early-stop/3.yaml')

kaus_train.dataset_splitter.ensure_dataset_is_loaded()
kaus_datasets = kaus_train.get_train_fitted_valid_test()
#kaus_model = serial.load('data/models/two-class/raw-square/early-stop/3.pkl').model
kaus_result = serial.load('data/models/two-class/raw-square/early-stop/3.result.pkl')


# In[11]:

kaus_test_topo = kaus_datasets['test'].get_topological_view()
from motor_imagery_learning.analysis.plot_util import  plot_sensor_signals
_ = plot_sensor_signals(kaus_test_topo[1], sensor_names=kaus_train.dataset_splitter.dataset.sensor_names,
                       yticks="onlymax").suptitle("rest trial, incorrectly predicted", y=0.95, fontsize=20)
_ = plot_sensor_signals(kaus_test_topo[2], sensor_names=kaus_train.dataset_splitter.dataset.sensor_names,
                       yticks="onlymax").suptitle("rest trial, incorrectly predicted", y=0.95, fontsize=20)

_ = plot_sensor_signals(kaus_test_topo[10], sensor_names=kaus_train.dataset_splitter.dataset.sensor_names,
                       yticks="onlymax").suptitle("rest trial, correctly predicted", y=0.95, fontsize=20)
_ = plot_sensor_signals(kaus_test_topo[11], sensor_names=kaus_train.dataset_splitter.dataset.sensor_names,
                       yticks="onlymax").suptitle("right hand trial, correctly predicted", y=0.95, fontsize=20)

