
# coding: utf-8

# In[1]:

import os

import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# make sure to use cpu for printing results...
os.environ['THEANO_FLAGS'] = "floatX=float32,device=cpu,nvcc.fastmath=True"
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7
from analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file
from motor_imagery_learning.analysis.print_results import ResultPrinter
from motor_imagery_learning.csp.print_csp_results import CSPResultPrinter
from pylearn2.utils import serial
import numpy as np
from motor_imagery_learning.analysis.plot_util import (plot_nlls_for_file, 
    plot_misclasses_for_model, plot_misclasses_for_file, plot_nlls)


# In[2]:

get_ipython().run_cell_magic(u'javascript', u'', u'$(\'body\').append(\'<script src="https://dl.dropboxusercontent.com/u/34637013/work/ipython-notebooks/js-for-table-sorting/jquery.tablesorter.min.js"></script>\')\n// Sort tables every 5 seconds...\nvar makeTablesSortable;\nmakeTablesSortable = function() { \n  $(\'.text_cell_render table\').not(\'.sorted_table\').tablesorter().addClass(\'sorted_table\');\n}\n\nsetInterval(makeTablesSortable, 5000);')


# ## Open Comparisons 

# * data/models/fft/all-freqs/spatial-freq-time/rectified/
# * data/models/fft/merged-freqs/spatial-freq-time/until-144/rectified/
# * data/models/fft/merged-freqs/car/softmax/low-col-norm/

# ### TODO 

# * chan freq standadization
# * other dropout probs? less dropout even on later layers? or more dropout on input? or mix of both?
# * exponential running standardization
# * batch size?
# * NaMa difference more freqs from lowest bin or from higher bins? try 0-50 for comparison... (maybe also try doing the class reconstruction to see what is happening :)) (data/models/debug/bandpower-full-vs-2-50/early-stop/)

# ## Analysis 

# * merged freqs spatial freq time adam 10 fitlers 82.23%
# * spatial freq time all freqs 60 momentum /10 adam 80%/79,5%
# * merged freqs softmax adam no dropout 75.6%
# * freq conv all freqs momentum 60 filters 75.5% 
# * identtity  30 filters momentum 76.22%
# * freq spatial time ~71%
# * spatial freq time ~75.8% (momentum 60 filters, adam 10 filters ~75.3%)
# * freq conv momentum 60 filters 76%
# * freq conv identity + softmax (10 filters) 75.5%
# * freq conv 10 filters + softmax (no relu/fully connected layer) 74%, 60 filters 74.5% (and faster)
# * softmax 70.5%
# * more layers stride even more filters,  no change, still 61%
# * more layers stride correct pool 59%
# * two layers, stride, 100 output filters, 50 pool filters 67%
# * more layers correct pools 61%
# * two layers and stride 63%
# * more layers stride  61% (57% first try with less filters)
# * more layers pool more filters ~62.5%
# * more layers mean pool ~ 62%
# * more layers pool 63%
# * larger model mit pool ~63%
# * very large model ~58% 
# * adam with standard parameters 73%
# ### old attempts
# * standardizing over channels/freq only same  0% 75%
# * adam with squaring amplitude -3%, 72.8%
# * running min mean stopping is fine, +0.5% better than best replicated 75.42%
# * adam with alpha 0.2 very bad -10%
# * adam with keep lrule and alpha 0.02 1% better, ~1% behind best (momentum)
# * full better on some datasets, worse on others
# * best replication almost as good as best (-0.5% only)

# In[4]:

ResultPrinter('data/models/final-eval/small/fft/early-stop/').print_results(
    print_individual_datasets=False)


# In[7]:

ResultPrinter('data/models/fft/merged-freqs/car/softmax/low-col-norm/early-stop/').print_results()


# In[6]:

ResultPrinter('data/models/fft/merged-freqs/car/softmax/until-144/early-stop/').print_results()


# In[3]:

ResultPrinter('data/models/final-eval/fft/early-stop/').print_results(print_individual_datasets=False)


# |id|files|transform|sensor_names|freq_stop|splitter|cleaner|k_shape|time|std|valid_test|std|test|std|train|std|valid|std|
# |-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
# |0|18|*power|*C_sensors|144|two_file|*two_set|[1, 35]|0:09:27|0:02:15|87.52%|8.90%|76.53%|16.22%|99.54%|0.99%|98.51%|2.70%|
# |1|18|*power|*C_sensors|144|train_test|*one_set|[1, 35]|0:10:45|0:03:30|90.03%|7.76%|80.81%|14.68%|99.72%|0.79%|99.25%|1.45%|
# |2|18|*power|*C_sensors|72|two_file|*two_set|[1, 23]|0:06:49|0:02:57|84.89%|9.65%|73.75%|15.21%|96.92%|4.67%|96.03%|4.75%|
# |3|18|*power|*C_sensors|72|train_test|*one_set|[1, 23]|0:09:24|0:02:37|88.46%|8.04%|80.07%|11.67%|98.26%|4.07%|96.85%|7.01%|
# |4|18|*power|*all_EEG|144|two_file|*two_set|[1, 35]|0:13:24|0:04:25|86.00%|10.99%|74.86%|17.11%|98.07%|6.20%|97.14%|8.74%|
# |5|18|*power|*all_EEG|144|train_test|*one_set|[1, 35]|0:15:19|0:06:20|87.77%|11.49%|78.75%|16.30%|98.07%|6.35%|96.80%|9.40%|
# |6|18|*power|*all_EEG|72|two_file|*two_set|[1, 23]|0:12:14|0:05:14|86.22%|8.90%|74.65%|15.44%|98.83%|2.53%|97.78%|4.46%|
# |7|18|*power|*all_EEG|72|train_test|*one_set|[1, 23]|0:13:09|0:06:42|88.21%|6.57%|77.86%|12.09%|99.25%|1.57%|98.56%|3.11%|
# |8|18|*power_phase|*C_sensors|144|two_file|*two_set|[1, 35]|0:11:42|0:04:24|87.33%|9.15%|75.87%|16.30%|99.56%|1.12%|98.79%|3.06%|
# |9|18|*power_phase|*C_sensors|144|train_test|*one_set|[1, 35]|0:13:23|0:06:18|88.15%|9.04%|77.68%|15.99%|99.42%|1.89%|98.62%|4.19%|
# |10|18|*power_phase|*C_sensors|72|two_file|*two_set|[1, 23]|0:09:12|0:03:33|85.26%|9.52%|73.30%|16.05%|98.89%|2.62%|97.23%|4.71%|
# |11|18|*power_phase|*C_sensors|72|train_test|*one_set|[1, 23]|0:11:05|0:04:00|88.35%|6.68%|77.48%|12.67%|99.68%|0.74%|99.22%|1.80%|
# |12|18|*power_phase|*all_EEG|144|two_file|*two_set|[1, 35]|0:26:17|0:10:32|85.98%|8.23%|73.26%|15.73%|99.33%|2.51%|98.70%|4.12%|
# |13|18|*power_phase|*all_EEG|144|train_test|*one_set|[1, 35]|0:23:24|0:10:59|89.04%|6.29%|78.38%|12.25%|99.95%|0.17%|99.69%|1.02%|
# |14|18|*power_phase|*all_EEG|72|two_file|*two_set|[1, 23]|0:17:22|0:06:41|85.11%|8.04%|71.32%|15.47%|99.29%|2.13%|98.90%|2.75%|
# 

# In[ ]:




# ## Results 

# In[319]:

ResultPrinter('data/models/fft/merged-freqs/spatial-freq-time/until-144/rectified/early-stop/').print_results(print_individual_datasets=False)


# In[318]:

ResultPrinter('data/models/fft/merged-freqs/spatial-freq-time/until-144/early-stop/').print_results(print_individual_datasets=False)


# In[287]:

ResultPrinter('data/models/fft/all-freqs/spatial-freq-time/rectified/early-stop/').print_results(print_individual_datasets=False)


# In[65]:

ResultPrinter('data/models/best/bandpower/early-stop/').print_results(print_templates_and_constants=False)

