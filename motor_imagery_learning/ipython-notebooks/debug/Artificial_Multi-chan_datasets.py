
# coding: utf-8

# In[26]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import numpy as np
from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import ConvElemwise
from theano.sandbox.cuda.dnn import dnn_conv
from theano.sandbox.cuda.basic_ops import gpu_alloc_empty
from motor_imagery_learning.analysis.print_results import ResultPrinter
from pylearn2.utils import serial

import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (8.0, 2.0)
matplotlib.rcParams['font.size'] = 7

from motor_imagery_learning.analysis.plot_util import plot_misclasses_for_model, plot_nlls_for_model, plot_sensor_signals
import scipy
import seaborn
seaborn.set_style('darkgrid')
from copy import deepcopy
from motor_imagery_learning.train_scripts.train_experiments import ExperimentsRunner
from motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer
from motor_imagery_learning.analysis.data_generation import (create_sine_signal,
    pipeline, create_shifted_sines,put_noise, convolve_with_weight,
    max_pool_topo_view, log_sum_pool,generate_single_sensor_data, 
    add_timecourse, spread_to_sensors, SpreadedSinesDataset)
from motor_imagery_learning.analysis.util import create_trainer
from motor_imagery_learning.mypylearn2.conv_squared import ConvSquared
from motor_imagery_learning.analysis.input_reconstruction import (
    create_rand_input, gradient_descent_input, create_input_grad_for_wanted_class_func,
    create_input_grad_with_smooth_penalty_for_wanted_class_func, reconstruct_inputs_for_class,
    power_smoothness_cost_moving_avg_scaled)
from pylearn2.config import yaml_parse


# # Multi Chan dataset

# ## Replicate with single chan data

# In[46]:

clf = LDA()
input_shape=(1000,1,1200,1)
start_topo_view, y = create_shifted_sines(input_shape, RandomState(np.uint64(hash("pooled_pipeline"))))
topo_view_pooled, y_pooled = pipeline(start_topo_view, y, 
    lambda x,y: put_noise(x,weight_old_data=0.05),
    lambda x,y: convolve_with_weight(x, create_sine_signal(samples=150,freq=11, sampling_freq=300.0)),
    lambda x,y: x*x,
    lambda x,y: log_sum_pool(x,pool_shape=50))

topo_view_noised, _ = pipeline(start_topo_view, y, 
    lambda x,y: put_noise(x,weight_old_data=0.05))

print ("topo view pooled", clf.fit_and_score_train_test_topo(topo_view_pooled, y_pooled))


# In[ ]:

a = x + 0.5y
b = 0.5x + y

x = (2/3a - 1/3b)
y = (-1/3a + 2/3b)


# In[23]:

clf = LDA()
input_shape=(1000,1,600,1)
lower_freq_topo_view, y1 = create_shifted_sines(input_shape, RandomState(np.uint64(hash("pooled_pipeline"))), 
                                          sampling_freq=150.0)

higher_freq_topo_view, y2 = create_shifted_sines(input_shape, RandomState(np.uint64(hash("pooled_pipeline"))), 
                                          sampling_freq=150.0, freq=40)
assert np.array_equal(y1,y2)
# since we use same RandomState.... so couold also add results of topo view instaed of concatenating in chan dimension

combined_topo_view = np.concatenate((lower_freq_topo_view, higher_freq_topo_view), axis=1)
y = y1

topo_view_feature, y = pipeline(combined_topo_view, y,
                         lambda x,y: spread_to_sensors(x, [[1,0.5], [0.5,1]]), #mix
                         lambda x,y: put_noise(x, weight_old_data=0.015, same_for_chans=True),
                         lambda x,y: spread_to_sensors(x, [[2/3.0,-1/3.0], [-1/3.0,2/3.0]]), # unmix
                         lambda x,y: convolve_with_weight(x, create_sine_signal(150, 11.0, 150.0)),
                         lambda x,y: x *x,
                         lambda x,y: log_sum_pool(x, 50))

train_acc, test_acc = clf.fit_and_score_train_test_topo(topo_view_feature, y)
print("train/test acc", train_acc, test_acc)


# In[ ]:

a = 0.8 * x
b = 0.5 * x

x= 0.8/1.3 *a + 0.5/1.3 * b


# In[28]:

dataset.get_topological_view().shape


# In[37]:

from motor_imagery_learning.analysis.data_generation import SpreadedSinesDataset
clf = LDA()
dataset = SpreadedSinesDataset(weight_old_data=0.08)
dataset.load()



topo_view_feature, y = pipeline(dataset.get_topological_view(), dataset.y,
                         lambda x,y: spread_to_sensors(x, [[0.8/1.3, 0.5/1.3]]), # unmix
                         lambda x,y: convolve_with_weight(x, create_sine_signal(150, 11.0, 150.0)),
                         lambda x,y: x *x,
                         lambda x,y: log_sum_pool(x, 50))

train_acc, test_acc = clf.fit_and_score_train_test_topo(topo_view_feature, y)
print("train/test acc", train_acc, test_acc)


# In[ ]:



