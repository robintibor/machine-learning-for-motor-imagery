
# coding: utf-8

# In[3]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
#os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import numpy as np
from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import ConvElemwise
from theano.sandbox.cuda.dnn import dnn_conv
from theano.sandbox.cuda.basic_ops import gpu_alloc_empty
from motor_imagery_learning.analysis.print_results import ResultPrinter
from pylearn2.utils import serial

import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (8.0, 2.0)
matplotlib.rcParams['font.size'] = 7

from motor_imagery_learning.analysis.plot_util import plot_misclasses_for_model, plot_nlls_for_model, plot_sensor_signals
import scipy
import seaborn
seaborn.set_style('darkgrid')
from copy import deepcopy
from motor_imagery_learning.train_scripts.train_experiments import ExperimentsRunner
from motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer
from motor_imagery_learning.analysis.data_generation import (create_sine_signal,
    SingleChanDataset, pipeline, create_shifted_sines,put_noise, convolve_with_weight,
    max_pool_topo_view, log_sum_pool,generate_single_sensor_data, 
    add_timecourse)
from motor_imagery_learning.analysis.util import create_trainer
from motor_imagery_learning.mypylearn2.conv_squared import ConvSquared
from motor_imagery_learning.analysis.input_reconstruction import (
    create_rand_input, gradient_descent_input, create_input_grad_for_wanted_class_func,
    create_input_grad_with_smooth_penalty_for_wanted_class_func, reconstruct_inputs_for_class,
    power_smoothness_cost_moving_avg_scaled)
from pylearn2.config import yaml_parse


# In[2]:

model = serial.load('data/models/two-class/raw-square/early-stop/3.pkl').model


# In[4]:

grad_args = dict(epochs=1000, lrate=0.5, input_cost_func=power_smoothness_cost_moving_avg_scaled)


# In[5]:

inputs_class_2 = reconstruct_inputs_for_class(model, [0,1], **grad_args)
_ = plot_sensor_signals(np.squeeze(inputs_class_2[-1,0]), get_C_sensors_sorted(), yticks="onlymax").suptitle(
    'Rest', y=0.93, fontsize=20)


# In[6]:

inputs_class_1 = reconstruct_inputs_for_class(model, [1,0], **grad_args)
_ = plot_sensor_signals(np.squeeze(inputs_class_1[-1,0]), get_C_sensors_sorted(), yticks="onlymax").suptitle(
    'Right Hand', y=0.93, fontsize=20)


# In[14]:

inputs_class_2[-1, 0].shape


# In[18]:

bps = np.mean(np.abs(np.fft.rfft(inputs_class_2[-1, 0], axis=1)), axis=(0,2))
freq_bins = np.fft.rfftfreq(inputs_class_2[-1,0].shape[1], d=1/150.0)

pyplot.plot(freq_bins, bps)


# In[20]:

bps = np.mean(np.abs(np.fft.rfft(inputs_class_1[-1, 0], axis=1)), axis=(0,2))
freq_bins = np.fft.rfftfreq(inputs_class_2[-1,0].shape[1], d=1/150.0)

pyplot.plot(freq_bins, bps)


# ## 

# In[ ]:



