
# coding: utf-8

# In[2]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# switch to cpu
os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
from matplotlib import cm
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from motor_imagery_learning.analysis.plot_util import plot_class_probs, plot_chan_matrices, plot_class_reconstruction,     plot_correct_part, plot_incorrect_part, plot_correctness, plot_misclasses, plot_sensor_signals
from  motor_imagery_learning.analysis.util import back_deconv, back_prop, back_guided_deconv, back_linear, bc01toinput,     load_preprocessed_datasets, back_guided_deconv_for_class, reshape_and_get_predictions,     get_trials_for_class, get_summed_activations, get_activations, get_activation,     check_prediction_correctness, load_train_test_datasets
from pylearn2.config import yaml_parse
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted, sort_topologically
from motor_imagery_learning.analysis.print_results import ResultPrinter
from copy import deepcopy
figsize=(13,8)
import psutil
import h5py
from scipy.io import loadmat
from pprint import pprint
from wyrm.types import Data
import wyrm
from motor_imagery_learning.preprocessing_util import (exponential_running_mean,
                                                       exponential_running_var)
import scipy.io
from motor_imagery_learning.bbci_dataset import BBCIDataset
from wyrm.processing import  select_ival
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.csp.print_csp_results import CSPResultPrinter


# ## Feature Selection 

# Write existing tests into test file with number comparison, maybeplausibility comparison
# turn on feature selection, put into csp, retrain here only filterbank csp, first run it before, note down numbers
# then run with feature selection and num_features= allfeatures for correctness check
# then take 4 features and see accuracy, also try changing invcov and see which is better

# In[2]:

CSPResultPrinter('data/models/csp/short-bands//').print_results()


# ## Lets try running it properly as experiment

# In[3]:

from glob import glob
trainer_filenames = glob('data/models/csp/full/*.pkl')
trainer_filenames = [f for f in trainer_filenames if 'result' not in f]
trainer_filenames.sort(key=lambda s: int(s.split('/')[-1].split('.')[0]))
trainer_filenames


# In[4]:

len(trainer_filenames)


# In[13]:

from motor_imagery_learning.csp.train_csp_experiment import CSPExperimentsRunner

exp_runner = CSPExperimentsRunner(experiments_file_name='configs/experiments/csp/retrain.yaml')

exp_runner.run()


# ## Checks for feature selection accuracies

# In[26]:

full_train_csp =serial.load('data/models/csp/full/19.pkl')


# In[27]:

from motor_imagery_learning.csp.pipeline import MultiClassWeightedVoting

def recreate_multi_class(train_csp_obj):
    train_csp_obj.multi_class = MultiClassWeightedVoting(
                                    train_csp_obj.binary_csp.train_labels_full_fold, 
                                    train_csp_obj.binary_csp.test_labels_full_fold,
                                    train_csp_obj.filterbank_csp.train_pred_full_fold,
                                    train_csp_obj.filterbank_csp.test_pred_full_fold,
                                    train_csp_obj.class_pairs)


# In[34]:

get_ipython().run_cell_magic(u'capture', u'', u'from motor_imagery_learning.csp.pipeline import FilterbankCSP\n\nfull_train_csp.filterbank_csp.num_features = 20\nfull_train_csp.filterbank_csp.num_filterbands = None\nfull_train_csp.filterbank_csp.run()\nrecreate_multi_class(full_train_csp)\nfull_train_csp.multi_class.run()')


# In[35]:

np.mean(full_train_csp.multi_class.test_accuracy)


# In[ ]:

# 0.97248803827751185 
# 0.97510252904989747 (with 10 features)
# 0.97510252904989747 (with 20 features)


# In[ ]:

# 0.80263157894736836 (with all features)
# 0.70639097744360901 (with 20 features)
# 0.71165413533834598 (with 30 features)


# In[282]:

np.mean(full_train_csp.multi_class.train_accuracy)


# In[210]:

def collect_features(features, filters_for_filterband):
    num_filters_per_fb = features[0].data.shape[1] / 2
    num_filterbands = len(features)
    first_features = deepcopy(features[0])
    first_num_filters = filters_for_filterband[0]
    first_features.data = first_features.data[:, range(first_num_filters) + range(-first_num_filters,0)]

    all_features = first_features
    for i in range(1, num_filterbands):
        this_num_filters = min(num_filters_per_fb, filters_for_filterband[i])
        if (this_num_filters > 0):
            next_features = deepcopy(features[i])
            next_features.data = next_features.data[:, range(this_num_filters) + range(-this_num_filters,0)]
            all_features = append_epo(all_features, next_features, classaxis=1)
    return all_features


# In[204]:

this_features[0].data.shape


# In[222]:

bincsp.train_feature[:, fold_i, class_i][0].data.shape[1] / 2


# In[219]:

#%%timeit
## First just select all features

class_i = 4
fold_i = 0
max_features = 20
selected_filters_per_filterband = [0 ] * num_filterbands
best_accuracy = -1
best_selected_filters_per_filterband = None
while np.sum(selected_filters_per_filterband) < max_features and best_accuracy < 1:
    for filt_i in range(num_filterbands):
        this_filt_per_fb = deepcopy(selected_filters_per_filterband)
        this_filt_per_fb[filt_i] += 1

        bincsp = full_train_csp.filterbank_csp.binary_csp # just to make code shorter
        this_features = deepcopy(bincsp.train_feature[:, fold_i, class_i])
        all_features = collect_features(this_features, this_filt_per_fb)
        test_start = int(np.ceil(all_features.data.shape[0] * 0.8))
        train_features = all_features.copy(data=all_features.data[:test_start], axes=[all_features.axes[0][:test_start]])
        test_features = all_features.copy(data=all_features.data[test_start:], axes=[all_features.axes[0][test_start:]])
        clf = lda_train_scaled(train_features, shrink=True)
        class_pair = full_train_csp.filterbank_csp.binary_csp.class_pairs[class_i]
        test_out = lda_apply(test_features, clf)
        correct_test= test_features.axes[0] == class_pair[1]
        predicted_test = test_out >= 0
        test_accuracy = (sum(correct_test == predicted_test) / 
            float(len(predicted_test)))
        if (test_accuracy > best_accuracy):
            best_accuracy = test_accuracy
            best_selected_filters_per_filterband = this_filt_per_fb
            best_features = deepcopy(all_features)
        print test_accuracy
    selected_filters_per_filterband = best_selected_filters_per_filterband


# In[213]:

selected_filters_per_filterband


# In[184]:

test_features.data.shape


# In[185]:




# In[154]:

(correct_test - 0.5) *  2 * test_out


# ## Visualizations

# In[402]:

from wyrm import plot
plot.beautify()
import motor_imagery_learning.mywyrm.plot as mywyrmplot


# In[399]:

sensor_names = csp_trainer.cnt.axes[1]


# In[434]:

pyplot.figure()
patterns = csp_trainer.binary_csp.patterns[-1,-1,0]
mywyrmplot.ax_scalp(patterns[:,-2], sensor_names, colormap=cm.bwr)

