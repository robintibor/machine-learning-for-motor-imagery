
# coding: utf-8

# In[1]:

import os
# make sure to use cpu for printing results...
os.environ['THEANO_FLAGS'] = "floatX=float32,device=cpu,nvcc.fastmath=True"

import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7
from analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file
from motor_imagery_learning.analysis.print_results import ResultPrinter
from pylearn2.utils import serial 
import h5py
import numpy as np


# In[7]:

get_ipython().run_cell_magic(u'javascript', u'', u'function makeSVGDownloadable() {\n    $(\'body\').on(\'click\', \'svg\', function(){\n        var svg = this.outerHTML;\n        var b64 = btoa(svg); // or use btoa if supported\n        var linkToSVG = $("<a href-lang=\'image/svg+xml\' href=\'data:image/svg+xml;base64,\\n" + b64 +\n                          "\'title=\'file.svg\' target=\'blank\'>Download</a>");\n        $(this).after(linkToSVG);});\n}\nmakeSVGDownloadable()')


# In[2]:

from glob import glob
result_file_nrs = range(1,15) + range(29,33)
results = []
for result_nr in result_file_nrs:
    filename = 'data/models/debug/csp-net/early-stop/' + str(result_nr) + '.result.pkl'
    result = serial.load(filename)
    results.append(result)


# In[3]:


filenames = [r.parameters['dataset_filename'].split('/')[-1][:4] for r in results]
train_nums = [len(r.targets['train']) for r in results]
test_nums = [len(r.targets['test'])for r in results]
trial_nums = np.array(train_nums) + np.array(test_nums)
fig = pyplot.figure()
pyplot.bar(range(18), trial_nums)
pyplot.xticks(np.array(range(18)) +0.5, filenames)
None


# In[6]:

print("Train")
print np.mean(train_nums)
print np.std(train_nums)
print np.max(train_nums)
print np.min(train_nums)
print("Test")
print np.mean(test_nums)
print np.std(test_nums)
print np.max(test_nums)
print np.min(test_nums)


# In[ ]:



