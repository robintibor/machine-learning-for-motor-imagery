
# coding: utf-8

# In[5]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
#os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import numpy as np
from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import ConvElemwise
from theano.sandbox.cuda.dnn import dnn_conv
from theano.sandbox.cuda.basic_ops import gpu_alloc_empty
from motor_imagery_learning.analysis.print_results import ResultPrinter
from pylearn2.utils import serial

import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (8.0, 2.0)
matplotlib.rcParams['font.size'] = 7

from motor_imagery_learning.analysis.plot_util import plot_misclasses_for_model, plot_nlls_for_model, plot_sensor_signals
import scipy
import seaborn
seaborn.set_style('darkgrid')
from copy import deepcopy
from motor_imagery_learning.train_scripts.train_experiments import ExperimentsRunner
from motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer
from motor_imagery_learning.analysis.data_generation import (create_sine_signal,
    pipeline, create_shifted_sines,put_noise, convolve_with_weight,
    max_pool_topo_view, log_sum_pool,generate_single_sensor_data, 
    add_timecourse, spread_to_sensors, SpreadedSinesDataset, randomly_shifted_sines)
from motor_imagery_learning.analysis.util import create_trainer
from motor_imagery_learning.mypylearn2.conv_squared import ConvSquared
from motor_imagery_learning.analysis.input_reconstruction import (
    create_rand_input, gradient_descent_input, create_input_grad_for_wanted_class_func,
    create_input_grad_with_smooth_penalty_for_wanted_class_func, reconstruct_inputs_for_class,
    power_smoothness_cost_moving_avg_scaled)
from pylearn2.config import yaml_parse
from  motor_imagery_learning.train_scripts.train_with_params import create_training_object_from_file

from theano.tensor.nnet.conv import conv2d


# ## Gradient behavior for small artificial signal

# Gradient is smaller where signal is not matched at all, larger where it is either matched or matched "reverse", i.e. convolution has a negative output.
# 
# Gradient gets smaller when output gets bigger due to log fuction(!)

# In[217]:

grad_weight_func, grad_conv_out_func, conv_out_func = create_conv_square_sum_log_model()
sine, weights = create_input_and_weights()
pyplot.plot(sine)
pyplot.plot(weights)
pyplot.legend(('input', 'weights'))
pyplot.title('start')
weights, weight_grads, conv_grads = run_training(sine, weights, grad_weight_func, grad_conv_out_func, epochs=4)
plot_training(sine, weights, weight_grads, conv_grads)


# In[221]:

def create_conv_square_sum_log_model():
    return create_conv_pool_model(pool_square_sum_log)

def create_conv_square_sum_model():
    return create_conv_pool_model(pool_square_sum)

def create_conv_pool_model(nonlin_and_pool_func): 
    inputs = T.fvector()
    weights = T.fvector()
    conved_output = conv2d(inputs.dimshuffle('x', 'x',0,'x'), weights.dimshuffle('x', 'x', 0, 'x'))
    logsum = nonlin_and_pool_func(conved_output)


    grad_simple_model = T.grad(logsum, weights)

    grad_weight_func = theano.function([inputs, weights], grad_simple_model, allow_input_downcast=True)
    grad_conved_output = T.grad(logsum, conved_output)
    grad_conv_out_func = theano.function([inputs, weights], grad_conved_output, allow_input_downcast=True)

    conv_out_func = theano.function([inputs, weights], conved_output, allow_input_downcast=True)
    return grad_weight_func, grad_conv_out_func, conv_out_func
    


def pool_square_sum_log(conved_output):
    return T.log(T.sum(T.sqr(conved_output)))

def pool_square_sum(conved_output):
    return T.sum(T.sqr(conved_output))

def pool_square_plus_abs_sum_log_mean(conved_output):
    return T.log(T.sum(T.sqr(conved_output))) + T.mean(T.abs_(conved_output))
    

def create_input_and_weights(samples=18, weight_length=10):
    sine = create_sine_signal(samples=samples, freq=11, sampling_freq=100)
    rng = RandomState(np.uint64(hash('randomweights')))
    weights = rng.randn(weight_length)
    return sine, weights

def run_training(sine, weights, grad_weight_func, grad_conv_out_func, epochs=10):
    cur_weights = deepcopy(weights)
    weights, weight_grads, conv_grads = [cur_weights], [], []
    for i in xrange(epochs):
        grad = grad_weight_func(sine, cur_weights)
        grad_conv = np.squeeze(grad_conv_out_func(sine, cur_weights))
        cur_weights = cur_weights+grad# * 0.1
        weights.append(deepcopy(cur_weights))
        weight_grads.append(deepcopy(grad))
        conv_grads.append(deepcopy(grad_conv))
    return weights, weight_grads, conv_grads

def plot_training(inputs, weights, weight_grads, conv_grads):
    weight_color = seaborn.color_palette()[0]
    grad_color = seaborn.color_palette()[1]

    color_weight = 13
    for i in xrange(len(weight_grads)):
        cur_weights = weights[i]
        grad = weight_grads[i]
        conv_grad = conv_grads[i]
        weight_grad_fig, _ = pyplot.subplots(1,1)
        weight_grad_fig.suptitle('weight, grad')
        input_grad_conv_fig, _ = pyplot.subplots(1,1)
        input_grad_conv_fig.suptitle('input, grad_conv, conv_out')
        cur_weight_color = list(weight_color) + [1]#[0.2 + 0.8* float(i + 1) / epochs]
        cur_grad_color =  list(grad_color) + [1] # [0.2 + 0.8 * float(i + 1) / epochs]
        weight_grad_fig.axes[0].plot(cur_weights, color=cur_weight_color)
        weight_grad_fig.axes[0].plot(grad, color=cur_grad_color)
        input_grad_conv_fig.axes[0].plot(inputs * 0.1, color=cur_weight_color)
        grad_inds = range(len(cur_weights) / 2, (len(cur_weights) / 2) + len(conv_grad) )
        input_grad_conv_fig.axes[0].plot(grad_inds, conv_grad, color=cur_grad_color)
        cur_weights = cur_weights+grad# * 0.1
    


# ## Two-class artificial example

# In[235]:

first_sines = randomly_shifted_sines(number=3, samples=60, freq=sine_freq_1, sampling_freq=100)
second_sines = randomly_shifted_sines(number=3, samples=60, freq=sine_freq_2, sampling_freq=100)

pyplot.plot(first_sines[0])
pyplot.plot(second_sines[0])

pyplot.plot(first_sines[0]+ second_sines[0])


# In[238]:

conved_signal = np.convolve(first_sines[0]+ second_sines[0], 
            create_sine_signal(samples=15, freq=sine_freq_1, sampling_freq=100),
            mode='valid')

pyplot.plot(conved_signal)


# In[240]:

grad_weight_func, grad_conv_out_func, conv_out_func = create_conv_square_sum_log_model()


# In[242]:

np.ones(15).shape


# In[254]:

pyplot.plot(grad_weight_func(first_sines[0]+ second_sines[0], np.ones(20)))


# In[239]:

conved_signal = np.convolve(first_sines[0]+ second_sines[0], 
            create_sine_signal(samples=15, freq=sine_freq_2, sampling_freq=100),
            mode='valid')

pyplot.plot(conved_signal)


# In[229]:

from motor_imagery_learning.mypylearn2.pool import PoolLayer, log_sum_all

sine_freq_1 = 9
sine_freq_2 = 14

layers = ((ConvSquared, dict(kernel_shape=(15,1), num_channels=2)),
          (PoolLayer, dict(pool_func=log_sum_all)), 
          Softmax)

dataset = create_two_added_sines_dataset(sine_freq_1, sine_freq_2)
trainer = SGDTrainer(dataset, layers)

trainer.train_epochs(epochs=3, batch_size=30)


# In[259]:

first_sines = randomly_shifted_sines(number=3, samples=60, freq=sine_freq_1, sampling_freq=100)
second_sines = randomly_shifted_sines(number=3, samples=60, freq=sine_freq_2, sampling_freq=100)
merged_sines = first_sines * np.array([1,0,1])[:,np.newaxis] + second_sines * np.array([0,1,1])[:,np.newaxis]



# In[260]:

anwe_model = serial.load('data/models/debug/raw-square/like-csp/mean-pool/early-stop/1.pkl').model


# In[265]:

anwe_model_raw = serial.load('data/models/debug/raw-square/identity-bp-faster-stop-no-relu/early-stop/1.pkl').model


# In[277]:

bhno_model = serial.load('data/models/debug/raw-square/like-csp/early-stop/3.pkl').model


# In[274]:

bhno_model_raw = serial.load('data/models/debug/raw-square/identity-bp-faster-stop-no-relu/early-stop/2.pkl').model


# In[289]:

misclass_test = bhno_model_raw.monitor.channels['test_y_misclass'].val_record

pyplot.plot(misclass_test[:100])


# In[271]:

from motor_imagery_learning.analysis.util import bps_and_freqs


# In[272]:

bps, freq_bins = bps_and_freqs(np.squeeze(anwe_model_raw.get_weights_topo()))
pyplot.plot(freq_bins, np.mean(bps, axis=0))


# In[273]:

bps, freq_bins = bps_and_freqs(np.squeeze(anwe_model.get_weights_topo()))
pyplot.plot(freq_bins, np.mean(bps, axis=0))


# In[266]:

plot_sensor_signals(np.squeeze(anwe_model_raw.get_weights_topo()))
None


# In[264]:

plot_sensor_signals(np.squeeze(anwe_model.get_weights_topo()))
None


# ## GuJo Data

# In[201]:

from motor_imagery_learning.bbci_pylearn_dataset import BBCISetNoCleaner, BBCIPylearnCleanDataset
from motor_imagery_learning.mywyrm.processing import resample_cnt, highpass_cnt
from motor_imagery_learning.mypylearn2.preprocessing import OnlineChannelwiseStandardize

gujo_set = BBCIPylearnCleanDataset(filenames='data/BBCI-without-last-runs/GuJoMoSc01S001R01_ds10_1-11.BBCI.mat', 
                        cleaner=BBCISetNoCleaner(), load_sensor_names=['CP2'], 
                        cnt_preprocessors=[(resample_cnt, dict(newfs=100)),
                                          (highpass_cnt, dict(low_cut_off_hz=0.5))] 
                        )


gujo_set.load()
preproc = OnlineChannelwiseStandardize()
preproc.apply(gujo_set, can_fit=True)
rest_class_inds = np.flatnonzero(gujo_set.y.argmax(axis=1) == 2)


# In[202]:

topo_view = np.squeeze(gujo_set.get_topological_view())
pyplot.plot(topo_view[rest_class_inds[0]])


# In[203]:


pyplot.plot(topo_view[rest_class_inds[0]][238:256])


# In[207]:

_, weights = create_input_and_weights()
inputs = topo_view[rest_class_inds[0]][238:256]

pyplot.plot(inputs)
pyplot.plot(weights)
pyplot.legend(('input', 'weights'))
weights, weight_grads, conv_grads = run_training(inputs, weights, grad_weight_func, grad_conv_out_func, epochs=3)


# In[208]:

plot_training(inputs, weights, weight_grads, conv_grads)


# ### More data

# In[ ]:

grad_weight_func, grad_conv_out_func, conv_out_func = create_conv_pool_model(pool_square_plus_abs_sum_log_mean)
sine, weights = create_input_and_weights(samples=108)
pyplot.plot(sine)
pyplot.plot(weights)
pyplot.legend(('input', 'weights'))
pyplot.title('start')
weights, weight_grads, conv_grads = run_training(sine, weights, grad_weight_func, grad_conv_out_func)
plot_training(sine, weights, weight_grads, conv_grads)


# In[212]:

grad_weight_func, grad_conv_out_func, conv_out_func = create_conv_square_logsum_model()
sine, weights = create_input_and_weights(samples=108)
pyplot.plot(sine)
pyplot.plot(weights)
pyplot.legend(('input', 'weights'))
pyplot.title('start')
weights, weight_grads, conv_grads = run_training(sine, weights, grad_weight_func, grad_conv_out_func)
plot_training(sine, weights, weight_grads, conv_grads)


# In[218]:

grad_weight_func, grad_conv_out_func, conv_out_func = create_conv_square_sum_model()
sine, weights = create_input_and_weights(samples=108)
pyplot.plot(sine)
pyplot.plot(weights)
pyplot.legend(('input', 'weights'))
pyplot.title('start')
weights, weight_grads, conv_grads = run_training(sine, weights, grad_weight_func, grad_conv_out_func)
plot_training(sine, weights, weight_grads, conv_grads)


# ## LDA Comparison with known weights

# In[159]:

from motor_imagery_learning.analysis.data_generation import SpreadedSinesDataset
clf = LDA()
dataset = SpreadedSinesDataset(weight_old_data=0.08)
dataset.load()



topo_view_feature, y = pipeline(dataset.get_topological_view(), dataset.y,
                         lambda x,y: spread_to_sensors(x, [[0.8/1.3, 0.5/1.3]]), # unmix
                         lambda x,y: convolve_with_weight(x, create_sine_signal(150, 11.0, 150.0)),
                         lambda x,y: x *x,
                         lambda x,y: log_sum_pool(x, 50))

train_acc, test_acc = clf.fit_and_score_train_test_topo(topo_view_feature, y)
print("train/test acc", train_acc, test_acc)


# In[207]:

model = serial.load('data/models/fake-data/raw-square/early-stop/13.pkl').model


# In[208]:

_ = plot_misclasses_for_model(model)
_ = plot_nlls_for_model(model)


# In[209]:

pyplot.plot(np.squeeze(model.get_weights_topo()))
pyplot.plot(create_sine_signal(samples=150,freq=11,sampling_freq=150.0) * np.mean(np.abs(model.get_weights_topo())))


# In[210]:

train_obj = create_training_object_from_file('data/models/fake-data/raw-square/early-stop/13.yaml')
train_obj.dataset_splitter.ensure_dataset_is_loaded()
train_obj._create_early_stop_trainer()
#train_obj.dataset_splitter.dataset.load()
train_set, valid_set, test_set = [train_obj.algorithm.monitoring_dataset[key] for key in ['train', 'valid', 'test']]


# In[203]:

topo_view_feature, y = pipeline(train_obj.dataset_splitter.dataset.get_topological_view(), 
                                train_obj.dataset_splitter.dataset.y,
                         lambda x,y: spread_to_sensors(x, [[0.8/1.3, 0.5/1.3]]), # unmix
                         lambda x,y: convolve_with_weight(x, create_sine_signal(150, 11.0, 150.0)),
                         lambda x,y: x *x,
                         lambda x,y: log_sum_pool(x, 50))

train_acc, test_acc = clf.fit_and_score_train_test_topo(topo_view_feature, y)
print("train/test acc", train_acc, test_acc)


# In[211]:

inputs = T.ftensor4()
outputs = model.fprop(inputs, return_all=True)
fprop_func = theano.function([inputs], outputs)


# In[205]:

model.layers[0].set_weights(orig_weights)
orig_weights = deepcopy(model.get_weights_topo()).transpose(0,3,1,2)


# ## Investigate gradient

# In[222]:

inputs = T.ftensor4()
outputs = model.fprop(inputs)
labels = T.fmatrix()

cost = model.layers[3].cost(labels, outputs)
all_params = []
for layer in model.layers:
    for param in layer.get_params():
        all_params.append(param)
        
grads = T.grad(cost, all_params)

grad_func = theano.function([inputs, labels], grads)


# In[396]:

class_1_inds =  np.argmax(train_set.y, axis=1) == 1

grads = grad_func(train_set.get_topological_view()[class_1_inds].astype(np.float32), train_set.y[class_1_inds])


# In[398]:

pyplot.plot(np.squeeze(model.layers[0].get_weights_topo()))
pyplot.plot(np.squeeze(grads[0]) * np.mean(model.layers[0].get_weights_topo()) / np.mean(grads[0]), '--')
pyplot.title('gradient entire training set')


# In[395]:

test_class_1_inds =  np.argmax(test_set.y, axis=1) == 1

test_grads = grad_func(test_set.get_topological_view()[test_class_1_inds].astype(np.float32), 
                       test_set.y[test_class_1_inds])


# In[399]:

pyplot.plot(np.squeeze(model.layers[0].get_weights_topo()))
pyplot.plot(np.squeeze(test_grads[0]) * np.mean(model.layers[0].get_weights_topo()) / np.mean(test_grads[0]), '--')
pyplot.title('gradient entire test set')


# ## Investigate fake data

# In[342]:

fake_signals = np.float32([create_sine_signal(samples=600,freq=11,sampling_freq=150.0, shift=s) for s in 
                           [1, 0.5, 0,-1,-0.25,+0.25,-0.33,+0.33, -2,+2,-3,-3]])

#fake_signals = randomly_shifted_sines(500, samples=600, freq=11,sampling_freq=150.0)
fake_signals = np.float32([fake_signals, fake_signals])
fake_signals = fake_signals.swapaxes(0,1)
fake_signals = fake_signals[:, :, :, np.newaxis]
fake_signals = fake_signals.astype(np.float32)
fake_y = np.repeat([[0,1]], fake_signals.shape[0], axis=0).astype(np.float32)
grad_fake_data = grad_func(fake_signals, fake_y)
individual_grads = [grad_func([fake_signal], fake_y[0:1]) for fake_signal in fake_signals]
individual_lowest_layer = [np.array(ind_grad[0]) for ind_grad in individual_grads]

individual_lowest_layer = np.array(individual_lowest_layer)


# In[343]:

mean_lowest = np.mean(individual_lowest_layer, axis=(0,1,2,4))
pyplot.plot(mean_lowest)


# In[345]:

pyplot.plot(np.squeeze(individual_lowest_layer[0:2]).T)


# In[318]:

pyplot.plot(np.squeeze(grad_fake_data[0]))


# In[313]:

pyplot.plot(np.squeeze(grad_fake_data[0]))


# ## Simple model for understanding gradient

# In[358]:

from theano.tensor.nnet.conv import conv2d


# In[363]:

inputs = T.fvector()
weights = T.fvector()
conved_output = conv2d(inputs.dimshuffle('x', 'x',0,'x'), weights.dimshuffle('x', 'x', 0, 'x'))
logsum = T.log(T.sum(T.sqr(conved_output * (conved_output > 0))))

cost = 1.0/logsum

grad_simple_model = T.grad(cost, weights)

grad_simple_func = theano.function([inputs, weights], grad_simple_model)


# In[392]:

rng = RandomState(np.uint64(hash('simplegrad')))
weight = rng.rand(150)
#weight = weight - np.mean(weight)
weight_2 = rng.rand(150)
weight_3 = rng.rand(150)
sine_signal = create_sine_signal(samples=600,freq=11,sampling_freq=150.0, shift=0)
sine_signal_2 = create_sine_signal(samples=600,freq=11,sampling_freq=150.0, shift=1)
simple_grad = grad_simple_func(sine_signal.astype(np.float32), weight.astype(np.float32))
simple_grad_2 = grad_simple_func(sine_signal_2.astype(np.float32), weight.astype(np.float32))
simple_grad_weight_2 = grad_simple_func(sine_signal.astype(np.float32), weight_2.astype(np.float32))
simple_grad_weight_3 = grad_simple_func(sine_signal.astype(np.float32), weight_3.astype(np.float32))
#pyplot.plot(simple_grad)
#pyplot.plot(simple_grad_2)
#pyplot.plot(weight * np.mean(np.abs(simple_grad_2)) / np.mean(np.abs(weight)))
pyplot.plot(simple_grad_weight_2)
pyplot.plot(simple_grad_weight_3)
#pyplot.plot((weight_2 - np.mean(weight_2)) / 20)
pyplot.plot((weight_3 - np.mean(weight_3)) / 20)


# ## Hand-made weights

# In[393]:

sine_signal = create_sine_signal(samples=150,freq=11,sampling_freq=150.0).astype(np.float32)
#model.layers[0].set_weights(np.ones((1,1,15,1), dtype=np.float32))
model.layers[0].set_weights(sine_signal[np.newaxis, np.newaxis, :, np.newaxis])
model.layers[3].set_weights(np.float32([[-1.25, 1.25]]))
model.layers[3].set_biases(np.float32([+7,-7]))


# In[394]:

preds = fprop_func(train_set.get_topological_view().astype(np.float32))[-1]
print "train", np.sum(np.argmax(preds, axis=1) == np.argmax(train_set.y, axis=1)) / float(len(preds))
preds = fprop_func(valid_set.get_topological_view().astype(np.float32))[-1]
print "valid", np.sum(np.argmax(preds, axis=1) == np.argmax(valid_set.y, axis=1)) / float(len(preds))
preds = fprop_func(test_set.get_topological_view().astype(np.float32))[-1]
print "test", np.sum(np.argmax(preds, axis=1) == np.argmax(test_set.y, axis=1)) / float(len(preds))


# In[181]:

train_feature = fprop_func(train_set.get_topological_view().astype(np.float32))[-2]

train_acc, test_acc = clf.fit_and_score_train_test_topo(train_feature, train_set.y)
print("train/test acc", train_acc, test_acc)
clf
print clf.w,clf.b

clf.w = clf.w * 0 + 2.5
clf.b = clf.b * 0 - 14
print clf.score(train_feature.reshape(train_feature.shape[0], -1), train_set.y)

print clf.w,clf.b


# ## Redo training, look at gradients, play around with batch sizes etc

# In[ ]:

from motor_imagery_learning.mypylearn2.conv_squared
layers = [
    
]

