
# coding: utf-8

# In[2]:

import os

import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# make sure to use cpu for printing results...
os.environ['THEANO_FLAGS'] = "floatX=float32,device=cpu,nvcc.fastmath=True"
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7
from analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file
from motor_imagery_learning.analysis.print_results import ResultPrinter
from motor_imagery_learning.csp.print_csp_results import CSPResultPrinter
from pylearn2.utils import serial
import numpy as np
from motor_imagery_learning.analysis.plot_util import (plot_nlls_for_file, 
    plot_misclasses, plot_misclasses_for_file, plot_nlls)
from glob import glob


# In[3]:

get_ipython().run_cell_magic(u'javascript', u'', u'$(\'body\').append(\'<script src="https://dl.dropboxusercontent.com/u/34637013/work/ipython-notebooks/js-for-table-sorting/jquery.tablesorter.min.js"></script>\')\n// Sort tables every 5 seconds...\nvar makeTablesSortable;\nmakeTablesSortable = function() { \n  $(\'.text_cell_render table\').not(\'.sorted_table\').tablesorter().addClass(\'sorted_table\');\n}\n\nsetInterval(makeTablesSortable, 5000);')


# ## Open Comparisons

# * higher fs (data/models/debug/raw-square-300-fs/)
# * no fully connected layer (data/models/debug/raw-square-no-fully-connected/)
# * chan only (weights do not also span filterbank) and no fully connected (data/models/debug/raw-square-chan-only-no-fully-connected/)
# * chan only with fully connected (data/models/debug/raw-square-chan-only-weight/)
# * max pool (data/models/debug/raw-square-max-pool/)
# 

# ### TODO 

# * try different early stop
# * without relu after bp?
# * max pooling instead of sumlog pooling
# * bigger pool stride
# * raw square 300 fs run again (right now unclear it seems it never ran through all 18, also now maybe new params), increase kernel shape appropriately(!)
# * look at learning curves
# * higher resampling, more dropout?

# ## Analysis

# * ~1% percent ahead of smaller csp net for 150fs.. however smaller csp net alrdy cut at 50hz(!) and 150 fs implies up to 75 Hz are usable (nyquist freq)
# * 40 bp chans seems best (around 89%)...

# ## Results 

# In[23]:

ResultPrinter('data/models/debug/raw-square-max-pool/early-stop/').print_results(
    print_templates_and_constants=False, 
    print_individual_datasets=True)


# In[9]:

ResultPrinter('data/models/debug/raw-square-chan-only-weight/early-stop/').print_results(
    print_templates_and_constants=False, 
    print_individual_datasets=False)


# In[10]:

ResultPrinter('data/models/debug/raw-square-chan-only-no-fully-connected/early-stop/').print_results(
    print_templates_and_constants=False,
    print_individual_datasets=False)


# In[14]:

ResultPrinter('data/models/debug/raw-square-no-fully-connected/early-stop/').print_results(
    print_templates_and_constants=False,
    print_individual_datasets=False)


# In[100]:

ResultPrinter('data/models/two-class/raw-square/early-stop/').print_results(print_templates_and_constants=False)


# In[104]:

ResultPrinter('data/models/two-class/raw-square-smooth//early-stop/').print_results(print_templates_and_constants=False)


# In[102]:

ResultPrinter('data/models/debug/raw-square-smooth/early-stop/').print_results(print_templates_and_constants=False)


# In[99]:

ResultPrinter('data/models/debug/raw-square-300-fs/early-stop/').print_results(print_templates_and_constants=False)


# In[13]:

ResultPrinter('data/models/debug/raw-square-smaller/early-stop/').print_results(print_templates_and_constants=False)


# In[32]:

ResultPrinter('data/models/debug/raw-square-smaller/early-stop/first-5/').print_results(print_templates_and_constants=False,
    print_individual_datasets=False)


# 
# |id|files|spat_chans|bp_chans|bp_kernel_norm|in_first_probs|in_first_scales|time|std|valid_test|std|test|std|train|std|valid|std|
# |-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
# |0|5|30|20|1.0|1.0|1.0|1:55:48|0:56:29|93.61%|6.08%|87.21%|12.15%|100.00%|0.00%|100.00%|0.00%|
# |1|5|30|20|1.0|0.8|1.25|2:03:53|1:11:50|92.64%|5.66%|85.29%|11.31%|100.00%|0.00%|100.00%|0.00%|
# |2|5|30|40|1.0|1.0|1.0|1:43:47|1:01:44|94.53%|5.92%|89.06%|11.85%|100.00%|0.00%|100.00%|0.00%|
# |3|5|30|40|1.0|0.8|1.25|3:07:06|2:00:42|93.99%|4.36%|87.97%|8.72%|99.97%|0.06%|100.00%|0.00%|
# |4|5|50|20|1.0|1.0|1.0|1:13:00|0:32:06|94.15%|5.96%|88.31%|11.93%|100.00%|0.00%|100.00%|0.00%|
# |5|5|50|20|1.0|0.8|1.25|2:19:43|0:49:58|93.78%|5.53%|87.57%|11.05%|100.00%|0.00%|100.00%|0.00%|
# |6|5|50|40|1.0|1.0|1.0|2:17:49|0:49:52|94.97%|5.78%|89.93%|11.56%|100.00%|0.00%|100.00%|0.00%|
# |7|5|50|40|1.0|0.8|1.25|2:44:21|1:20:07|93.19%|5.40%|86.39%|10.79%|100.00%|0.00%|100.00%|0.00%|
# |8|5|30|20|2.0|1.0|1.0|1:42:11|0:44:15|93.61%|6.27%|87.22%|12.54%|100.00%|0.00%|100.00%|0.00%|
# |9|5|30|20|2.0|0.8|1.25|1:50:12|0:58:04|92.40%|4.12%|84.80%|8.23%|100.00%|0.00%|100.00%|0.00%|
# |10|5|30|40|2.0|1.0|1.0|1:42:01|0:59:33|95.06%|5.53%|90.12%|11.06%|100.00%|0.00%|100.00%|0.00%|
# |11|5|30|40|2.0|0.8|1.25|3:06:52|1:56:08|94.07%|4.56%|88.14%|9.11%|100.00%|0.00%|100.00%|0.00%|
# |12|5|50|20|2.0|1.0|1.0|1:23:45|0:43:42|93.70%|6.53%|87.40%|13.07%|100.00%|0.00%|100.00%|0.00%|
# |13|5|50|20|2.0|0.8|1.25|2:37:22|1:05:29|92.32%|5.36%|84.65%|10.72%|100.00%|0.00%|100.00%|0.00%|
# |14|5|50|40|2.0|1.0|1.0|2:29:10|0:58:36|94.93%|5.47%|89.86%|10.94%|100.00%|0.00%|100.00%|0.00%|
# |15|5|50|40|2.0|0.8|1.25|2:53:17|1:15:17|93.34%|4.83%|86.68%|9.66%|100.00%|0.00%|100.00%|0.00%|
# 

# In[7]:

# this is with 300 fs
ResultPrinter('data/models/debug/raw-square-smaller//early-stop//').print_results(print_templates_and_constants=False)


# In[22]:

plot_nlls_for_file('data/models/debug/raw-square-smaller/early-stop/first-5/2.result.pkl')

