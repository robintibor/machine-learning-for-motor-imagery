
# coding: utf-8

# In[29]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import numpy as np
from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import ConvElemwise
from theano.sandbox.cuda.dnn import dnn_conv
from theano.sandbox.cuda.basic_ops import gpu_alloc_empty
from motor_imagery_learning.analysis.print_results import ResultPrinter

import matplotlib
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7
from analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file
from motor_imagery_learning.analysis.print_results import ResultPrinter
from motor_imagery_learning.csp.print_csp_results import CSPResultPrinter
from pylearn2.utils import serial
import numpy as np
from motor_imagery_learning.analysis.plot_util import (plot_nlls_for_file, 
    plot_misclasses, plot_misclasses_for_file, plot_nlls)
from glob import glob


# ### TODELAY: Transform the later tests, looking at input/detector/output spaces into tests?

# In[31]:

from motor_imagery_learning.mypylearn2.conv_squared import ConvSquared
from pylearn2.space import Conv2DSpace


# In[32]:

real_input = np.ones((13,2,30,40)).astype('float32')


# In[33]:


fakemlp = lambda: None
fakemlp.rng = np.random
fakemlp.batch_size = 2
squared_layer = ConvSquared(output_channels=3, kernel_shape=[5,4], pool_shape=[2,3], 
                            pool_type='sumlog', irange=0.05, tied_b=True,
           pool_stride=[1,1], layer_name='papalapap')

squared_layer.set_mlp(fakemlp)
squared_layer.set_input_space(Conv2DSpace(real_input.shape[2:], channels=real_input.shape[1], axes=('b', 'c', 0, 1)))



# In[22]:

#expected output: 
# nach conv:
# 13,3, 26,37
# nach pool:
# 13,3,25,35


# In[34]:

inputs = T.ftensor4()
fprop_result = squared_layer.fprop(inputs)
fprop_func = theano.function([inputs], fprop_result)


# In[70]:

real_input.shape


# In[35]:

fprop_func(real_input).shape


# ### Switch Axes at start layer

# In[80]:

get_ipython().magic(u'reload_ext autoreload')
from motor_imagery_learning.mypylearn2.conv_switch_axes import convert_conv_space, ConvSwitchAxes
from pylearn2.models.mlp import IdentityConvNonlinearity

fakemlp = lambda: None
fakemlp.rng = np.random
fakemlp.batch_size = 2
switch_layer = ConvSwitchAxes(output_channels=3, kernel_shape=[5,1], pool_shape=[2,1], 
                            pool_type='mean', irange=0.05, tied_b=True,
           pool_stride=[1,1], layer_name='papalapap', nonlinearity=IdentityConvNonlinearity(),
                             switch_axes=[('b', 'c', 0, 1), ('b', 1, 0, 'c')])

switch_layer.set_mlp(fakemlp)
switch_layer.set_input_space(Conv2DSpace(real_input.shape[2:], channels=real_input.shape[1], axes=('b', 'c', 0, 1)))


# In[85]:

switch_layer.input_space


# In[86]:

switch_layer.detector_space


# In[84]:

switch_layer.output_space


# In[82]:

inputs = T.ftensor4()
fprop_result_switch = switch_layer.fprop(inputs)
fprop_switch_func = theano.function([inputs], fprop_result_switch)


# In[83]:

fprop_switch_func(real_input).shape


# In[57]:

convert_conv_space(oldspace, ('b', 'c',0,1), ('b', 0,1,'c'))


# In[106]:

from motor_imagery_learning.train_scripts.train_experiments import ExperimentsRunner

exp_runner = ExperimentsRunner('configs/experiments/debug/raw_square.yaml', cross_validation=False)



# In[107]:

exp_runner.create_all_experiments()


# In[108]:

exp_runner._run_all_experiments()


# In[109]:

exp_runner._run_experiment(1)

