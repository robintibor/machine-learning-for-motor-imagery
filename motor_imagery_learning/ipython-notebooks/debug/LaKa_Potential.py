
# coding: utf-8

# In[ ]:

# repeat with car!! data/models/debug/raw-square/car/30-kernel-shape/early-stop/8.yaml
from motor_imagery_learning.train_scripts.train_with_params import (
create_training_object_from_file)
laka_train_obj = create_training_object_from_file(
    'data/models/debug/raw-square/larger-kernel-shape/early-stop/8.yaml')
laka_train_obj.dataset_splitter.dataset.sensor_names = None
laka_train_obj.dataset_splitter.ensure_dataset_is_loaded()
laka_sets = laka_train_obj.get_train_fitted_valid_test()
left_topo = laka_sets['train'].get_topological_view()[laka_sets['train'].y[:,1] ==1]

right_topo = laka_sets['train'].get_topological_view()[laka_sets['train'].y[:,0] ==1]
not_left_topo = laka_sets['train'].get_topological_view()[laka_sets['train'].y[:,0] != 1]
#plot_head_signals_tight(np.mean(left_topo, axis=0), get_C_sensors_sorted())
None
diff_mean = np.mean(left_topo, axis=0) - np.mean(not_left_topo, axis=0)
#plot_head_signals_tight(diff_mean, get_C_sensors_sorted())
None
sensor_names = get_EEG_sensors_sorted()
sensor_names = [s for s in sensor_names 
                if s not in laka_train_obj.dataset_splitter.dataset.rejected_chans]
from motor_imagery_learning.analysis.plot_util import  plot_head_signals
diff_mean = np.mean(left_topo, axis=0) - np.mean(not_left_topo, axis=0)
#_ = plot_head_signals(diff_mean, sensor_names,figsize=(12.5,10))
None

len(sensor_names)

