
# coding: utf-8

# In[2]:

import os

import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# make sure to use cpu for printing results...
os.environ['THEANO_FLAGS'] = "floatX=float32,device=cpu,nvcc.fastmath=True"
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7
from analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file
from motor_imagery_learning.analysis.print_results import ResultPrinter
from motor_imagery_learning.csp.print_csp_results import CSPResultPrinter
from pylearn2.utils import serial
import numpy as np
from motor_imagery_learning.analysis.plot_util import (plot_nlls_for_file, 
    plot_misclasses, plot_misclasses_for_file, plot_nlls)
from glob import glob


# In[3]:

get_ipython().run_cell_magic(u'javascript', u'', u'$(\'body\').append(\'<script src="https://dl.dropboxusercontent.com/u/34637013/work/ipython-notebooks/js-for-table-sorting/jquery.tablesorter.min.js"></script>\')\n// Sort tables every 5 seconds...\nvar makeTablesSortable;\nmakeTablesSortable = function() { \n  $(\'.text_cell_render table\').not(\'.sorted_table\').tablesorter().addClass(\'sorted_table\');\n}\n\nsetInterval(makeTablesSortable, 5000);')


# ## Open Comparisons

# * Spatial net (raw-data-chan-kernel-dot/)

# ### TODO 

# * make beststop late keep adam new standard
# * mean trial channel removal true vs false
# * 300 fs with doubled kernel/pool sizes in first layer
# * exponential running standardization

# ## Analysis

# * kernel dot without rect 12% better on Anwe (60% vs 47.5%, could still be strongly influenced by luck, best accuracy and test accuracy same for without rect(60%) version), ~3% worse on next 3
# * 300 fs with same model worse ~-3% 82.7%
# * replication best 1.5% worse :( 85.6%
# * keep best lrule params a little better ~1% 86.5%

# ## Results 

# In[4]:

ResultPrinter('data/models/debug/raw-data-chan-kernel-dot-rect/early-stop/first-5/').print_results(print_templates_and_constants=False)


# In[122]:

ResultPrinter('data/models/best//raw-data/early-stop//').print_results(print_templates_and_constants=False)


# In[5]:

ResultPrinter('data/models/debug/raw-data-150-300-fs-best-stop/early-stop//').print_results(print_templates_and_constants=False)


# In[23]:

ResultPrinter('data/models/debug/raw-data-best-stop-late//early-stop/').print_results(print_templates_and_constants=False)


# In[4]:

ResultPrinter('data/models/debug/raw-data-best-stop-late-keep-adam/early-stop/').print_results(print_templates_and_constants=False)


# In[126]:

ResultPrinter('data/models/best/raw-data/early-stop/').print_results(print_templates_and_constants=False)


# In[78]:

ResultPrinter('data/models/debug/raw-data-running-early-stop/early-stop/').print_results(print_templates_and_constants=False)

