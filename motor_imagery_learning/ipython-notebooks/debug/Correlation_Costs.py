
# coding: utf-8

# In[4]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
#os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import numpy as np
from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import ConvElemwise
from theano.sandbox.cuda.dnn import dnn_conv
from theano.sandbox.cuda.basic_ops import gpu_alloc_empty
from motor_imagery_learning.analysis.print_results import ResultPrinter
from pylearn2.utils import serial

from copy import deepcopy
from motor_imagery_learning.train_scripts.train_experiments import ExperimentsRunner
from motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer
from motor_imagery_learning.analysis.data_generation import (create_sine_signal,
    pipeline, create_shifted_sines,put_noise, convolve_with_weight,
    max_pool_topo_view, log_sum_pool,generate_single_sensor_data, 
    add_timecourse, spread_to_sensors, SpreadedSinesDataset)
from motor_imagery_learning.analysis.util import create_trainer
from motor_imagery_learning.mypylearn2.conv_squared import ConvSquared, ConvSquaredFullHeight
from motor_imagery_learning.analysis.input_reconstruction import (
    create_rand_input, gradient_descent_input, create_input_grad_for_wanted_class_func,
    create_input_grad_with_smooth_penalty_for_wanted_class_func, reconstruct_inputs_for_class,
    power_smoothness_cost_moving_avg_scaled)
from pylearn2.config import yaml_parse
from motor_imagery_learning.analysis.util import bps_and_freqs
from motor_imagery_learning.analysis.deconv import deconv_model
from motor_imagery_learning.analysis.plot.raw_signals import plot_sensor_signals_two_classes

import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (8.0, 1.0)
matplotlib.rcParams['font.size'] = 7

from motor_imagery_learning.analysis.plot_util import plot_misclasses_for_model, plot_nlls_for_model, plot_sensor_signals
import scipy
import seaborn
seaborn.set_style('darkgrid')
from motor_imagery_learning.analysis.util import create_activations_func
from motor_imagery_learning.train_scripts.train_with_params import create_training_object_from_file
from motor_imagery_learning.analysis.util import create_prediction_func
from motor_imagery_learning.analysis.plot_util import plot_head_signals_tight_multiple_signals
from motor_imagery_learning.analysis.plot_util import plot_head_signals, plot_head_signals_tight
from motor_imagery_learning.analysis.plot_util import plot_head_signals_tight_two_signals


# In[2]:

ResultPrinter('data/models/two-class/raw-square/increasing-filters//test/').print_results()


# ## Decorrelate simple model softmax activations

# Lets look at a working model (100% on test) with two filters, lets see if we can decorrelate them? and keep close to 100% also maybe check increasing to 8 chans... b/c of dropout(?).. maybe not necessary....

# In[5]:

bhno_right_rest_model = serial.load('data/models/two-class/raw-square/increasing-filters/test/11.pkl').model
_ = plot_misclasses_for_model(bhno_right_rest_model)


# In[6]:

activation_func = create_activations_func(bhno_right_rest_model)


# In[7]:

bhno_right_rest_trainer = create_training_object_from_file(
    'data/models/two-class/raw-square/increasing-filters/test/11.yaml')
bhno_right_rest_trainer.dataset_splitter.ensure_dataset_is_loaded()
bhno_right_rest_sets = datasets = bhno_right_rest_trainer.get_train_fitted_valid_test()


# In[9]:

test_set = bhno_right_rest_sets['test']


# In[10]:

activations = activation_func(test_set.get_topological_view()[0:2])


# In[11]:

_ = plot_sensor_signals_two_classes(activations[0][0].transpose(2,0,1).reshape(8,-1),
                       activations[0][1].transpose(2,0,1).reshape(8,-1),             
                       ['C3'] * 4 + ['C4'] * 4,
    class_1_name='Rest', class_2_name='Right',figsize=(8,6))


# In[12]:

_ = plot_sensor_signals_two_classes(activations[2][0],
                       activations[2][1],    
    class_1_name='Rest', class_2_name='Right',figsize=(8,3))


# In[14]:

_ = plot_sensor_signals(bhno_right_rest_model.get_weights_topo().squeeze(), figsize=(8,4))


# In[17]:

np.dot(activations[2].squeeze(), activations[2].squeeze())

