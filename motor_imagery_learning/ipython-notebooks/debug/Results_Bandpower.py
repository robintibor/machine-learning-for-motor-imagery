
# coding: utf-8

# In[1]:

import os

import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# make sure to use cpu for printing results...
os.environ['THEANO_FLAGS'] = "floatX=float32,device=cpu,nvcc.fastmath=True"
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7
from analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file
from motor_imagery_learning.analysis.print_results import ResultPrinter
from motor_imagery_learning.csp.print_csp_results import CSPResultPrinter
from pylearn2.utils import serial
import numpy as np
from motor_imagery_learning.analysis.plot_util import (plot_nlls_for_file, 
    plot_misclasses, plot_misclasses_for_file, plot_nlls)


# In[2]:

get_ipython().run_cell_magic(u'javascript', u'', u'$(\'body\').append(\'<script src="https://dl.dropboxusercontent.com/u/34637013/work/ipython-notebooks/js-for-table-sorting/jquery.tablesorter.min.js"></script>\')\n// Sort tables every 5 seconds...\nvar makeTablesSortable;\nmakeTablesSortable = function() { \n  $(\'.text_cell_render table\').not(\'.sorted_table\').tablesorter().addClass(\'sorted_table\');\n}\n\nsetInterval(makeTablesSortable, 5000);')


# ## Open Comparisons 

# ...

# ### TODO 

# * exponential running standardization
# * batch size?
# * NaMa difference more freqs from lowest bin or from higher bins? try 0-50 for comparison... (maybe also try doing the class reconstruction to see what is happening :)) (data/models/debug/bandpower-full-vs-2-50/early-stop/)

# ## Analysis 

# * standardizing over channels/freq only same  0% 75%
# * adam with squaring amplitude -3%, 72.8%
# * running min mean stopping is fine, +0.5% better than best replicated 75.42%
# * adam with alpha 0.2 very bad -10%
# * adam with keep lrule and alpha 0.02 1% better, ~1% behind best (momentum)
# * full better on some datasets, worse on others
# * best replication almost as good as best (-0.5% only)

# ## Results 

# In[72]:

ResultPrinter('data/models/debug/bandpower-preproc-keep-time/early-stop/').print_results(print_templates_and_constants=False)


# In[71]:

ResultPrinter('data/models/debug/bandpower-adam-squared/early-stop/').print_results(print_templates_and_constants=False)


# In[3]:

ResultPrinter('data/models/full/bandpower/early-stop//').print_results(print_templates_and_constants=False)


# In[65]:

ResultPrinter('data/models/best/bandpower/early-stop/').print_results(print_templates_and_constants=False)

