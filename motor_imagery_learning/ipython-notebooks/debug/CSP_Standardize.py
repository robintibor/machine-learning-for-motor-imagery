
# coding: utf-8

# In[3]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# switch to cpu
os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
from matplotlib import cm
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from motor_imagery_learning.analysis.plot_util import plot_class_probs, plot_chan_matrices, plot_class_reconstruction,     plot_correct_part, plot_incorrect_part, plot_correctness, plot_misclasses_for_file, plot_sensor_signals
from  motor_imagery_learning.analysis.util import back_deconv, back_prop, back_guided_deconv, back_linear, bc01toinput,     load_preprocessed_datasets, back_guided_deconv_for_class, reshape_and_get_predictions,     get_trials_for_class, get_summed_activations, get_activations, get_activation,     check_prediction_correctness, load_train_test_datasets
from pylearn2.config import yaml_parse
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted, sort_topologically
from motor_imagery_learning.analysis.print_results import ResultPrinter
from copy import deepcopy
figsize=(13,8)
import psutil
import h5py
from scipy.io import loadmat
from pprint import pprint
from wyrm.types import Data
import wyrm
from motor_imagery_learning.preprocessing_util import (exponential_running_mean,
                                                       exponential_running_var)
import scipy.io
from motor_imagery_learning.bbci_dataset import BBCIDataset
from wyrm.processing import  select_ival
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.csp.print_csp_results import CSPResultPrinter


# In[6]:

import motor_imagery_learning.csp.train_csp
import motor_imagery_learning.csp.pipeline
motor_imagery_learning.csp.train_csp.log.setLevel('DEBUG')
motor_imagery_learning.csp.pipeline.log.setLevel('DEBUG')
from motor_imagery_learning.mywyrm.processing import bandpass_cnt, segment_dat_fast
from wyrm.processing import select_classes


# In[4]:

from motor_imagery_learning.csp.train_csp import CSPTrain
import motor_imagery_learning.csp.ival_optimizers
from motor_imagery_learning.csp.ival_optimizers import AucIntervalOptimizer
from motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
csp_debug_args = dict(load_sensor_names=get_C_sensors_sorted(),#['CP3', 'CP4', 'C3', 'C4'], 
         only_last_fold=True, segment_ival=[0,4000], ival_optimizer=AucIntervalOptimizer(max_score_fraction=0.95,
                                                                                        use_abs_for_threshold=False))
anwe_csp_args = dict(csp_debug_args, min_freq=10, max_freq=10, last_low_freq=10, low_width=8, high_width=8)

bhno_csp_args = dict(csp_debug_args, min_freq=11, max_freq=11, last_low_freq=11, low_width=4, high_width=4)
big_band_csp_args = dict(csp_debug_args, min_freq=16, max_freq=16, last_low_freq=16, low_width=28, high_width=28)
middle_band_csp_args = dict(csp_debug_args, min_freq=16, max_freq=16, last_low_freq=16, low_width=14, high_width=14)


anwe_train = CSPTrain('data/BBCI-without-last-runs/AnWeMoSc1S001R01_ds10_1-12.BBCI.mat', **middle_band_csp_args)
                      
         
bhno_train = CSPTrain('data/BBCI-without-last-runs/BhNoMoSc1S001R01_ds10_1-12.BBCI.mat', **middle_band_csp_args)


# In[30]:


anwe_train.run()


# In[7]:

bandpassed_cnt = bandpass_cnt(bhno_train.cnt, 9,23,3)

epo = segment_dat_fast(bandpassed_cnt, 
    marker_def={'1 - Right Hand': [1], '2 - Left Hand': [2], 
        '3 - Rest': [3], '4 - Feet': [4]}, 
    ival=[0,4000])


# In[9]:

from wyrm.processing import select_epochs


# In[10]:

len(epo.axes[0])


# In[12]:

epo_train = select_epochs(epo, range(600))


# In[15]:

epo_test = select_epochs(epo, range(600,897))


# In[16]:




# In[27]:




# In[28]:

online_standardize_epo(epo_train, epo_test)

