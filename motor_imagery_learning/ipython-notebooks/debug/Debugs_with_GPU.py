
# coding: utf-8

# In[1]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
#os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import numpy as np
from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import ConvElemwise
from theano.sandbox.cuda.dnn import dnn_conv
from theano.sandbox.cuda.basic_ops import gpu_alloc_empty
from motor_imagery_learning.analysis.print_results import ResultPrinter
from pylearn2.utils import serial

import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (8.0, 2.0)
matplotlib.rcParams['font.size'] = 7

from motor_imagery_learning.analysis.plot_util import (plot_misclasses_for_model, 
                                                       plot_nlls_for_model, plot_sensor_signals,
                                                       plot_misclasses_for_file)
import scipy
import seaborn
seaborn.set_style('darkgrid')
from copy import deepcopy
from motor_imagery_learning.train_scripts.train_experiments import ExperimentsRunner
from motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer
from motor_imagery_learning.analysis.data_generation import (create_sine_signal,
    pipeline, create_shifted_sines,put_noise, convolve_with_weight,
    max_pool_topo_view, log_sum_pool,generate_single_sensor_data, 
    add_timecourse, spread_to_sensors)
from motor_imagery_learning.analysis.util import create_trainer
from motor_imagery_learning.mypylearn2.conv_squared import ConvSquared
from motor_imagery_learning.analysis.input_reconstruction import (
    create_rand_input, gradient_descent_input, create_input_grad_for_wanted_class_func,
    create_input_grad_with_smooth_penalty_for_wanted_class_func, reconstruct_inputs_for_class,
    power_smoothness_cost_moving_avg_scaled)
from pylearn2.config import yaml_parse


# ## See if sine init models change weights at all

# In[2]:

model = serial.load('data/models/debug/raw-square/sine-init/early-stop/14.pkl').model


# In[20]:

mean_freq_weights = np.mean(np.abs(model.layers[1].get_weights_topo()), axis=(0,1,2))

pyplot.plot(mean_freq_weights)


# In[3]:

_ = plot_sensor_signals(np.squeeze(model.get_weights_topo()), figsize=(5,8), yticks="onlymax")


# In[14]:

_  = plot_sensor_signals(all_bps) # 


# In[11]:

all_bps = np.abs(np.fft.rfft(np.squeeze(model.get_weights_topo())))
bps = np.mean(np.abs(np.fft.rfft(np.squeeze(model.get_weights_topo()), axis=1)), axis=0)
freq_bins = np.fft.rfftfreq(model.get_weights_topo().shape[1], d=1/150.0)
pyplot.plot(freq_bins, bps)


# In[4]:

import motor_imagery_learning.analysis.util as anautil


# ## large pool model

# In[9]:

model = serial.load('data/models/debug/raw-square-large-pool/early-stop/1.pkl').model


# In[13]:

_ = plot_sensor_signals(np.squeeze(model.get_weights_topo()), figsize=(5,8), yticks="onlymax")


# In[19]:

mean_bps.shape


# In[20]:

weights = np.squeeze(model.get_weights_topo())

bps, freq_bins = get_bps_and_bins(weights, axis=1, sampling_freq=150.0)

mean_bps = np.mean(bps, axis=0)
pyplot.plot(freq_bins, mean_bps)


# In[16]:

def get_bps_and_bins(signal, axis, sampling_freq=150.0):
    bps = np.abs(np.fft.rfft(signal, axis=axis))
    freq_bins = np.fft.rfftfreq(signal.shape[axis], 1.0/sampling_freq)
    return bps, freq_bins

