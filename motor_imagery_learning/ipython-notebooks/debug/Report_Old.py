
# coding: utf-8

# ## Raw net seperate first two layer weights

# The weights in the first layer can show us what kind of signal shapes or frequencies the network is sensitive to. We show them here for our right hand vs left hand network. We would expect that the temporal filters match some frequency around 9-13 Hz.
# 
# bhno_right_left_model = serial.load(
#     'data/models/two-class/raw-square/increasing-filters/test/36.pkl').model
# temporal_weights = bhno_right_left_model.get_weights_topo()[:,::-1,::-1,:]
# fig = plot_sensor_signals(temporal_weights.squeeze(), 
#                           ['Filter 1', 'Filter 2'], figsize=(5,2.5))
# fig.suptitle('Temporal Filters', fontsize=16, y=1.02)
# pyplot.xticks(range(0,31,5), map(lambda ms: "{:.1f}".format(ms),
#                                  np.arange(0,31,5) * 1000 / 150.0))
# pyplot.xlabel("milliseconds")
# None
# 
# We see some sine-like shapes, especially well visible in filter 2. Now we might want to know what frequencies this net is sensitive to. To find out, we can transform the weights to the frequency domain using a fast Fourier transformation. Plotting the amplitudes, we can see that the weights show a peak at 10 Hz.
# 
# bps, freqs = bps_and_freqs(temporal_weights.squeeze())
# 
# fig = plot_sensor_signals(bps, ['Filter 1', 'Filter 2'], figsize=(5,2.5))
# fig.suptitle('Temporal Filters Frequency Amplitudes', fontsize=16, y=1.02)
# 
# pyplot.xticks(range(len(freqs)), map(lambda freq: "{:.0f}".format(freq), freqs))
# pyplot.xlabel('Hz')
# None
# 
# Now we can look how the output of the first layer is weighted in the second layer. Blue indicates a negative weight, red a positive weight, white corresponds to zero. All colormaps are on the same scale.
# 
# You can nicely see the lateralization for filter 2 by looking at the strength of the color, typically it will be much higher for one sensor than for the other one, i.e., higher for C3 for the first and last images and higher for C4 for the two middle images.
# Note that only the color differences matter, since the output of the second layer will be squared. For example, the first and the last filters should be roughly equivalent after squaring.
# 
# spat_filt_weights = bhno_right_left_model.layers[1].get_weights_topo()[:,::-1,::-1,:]
# fig, axes = pyplot.subplots(1, 4, sharex=True, sharey=True)
# for i_weight in range(4):
#     axes[i_weight].imshow(spat_filt_weights[i_weight,0].T, cm.bwr, 
#                           interpolation='nearest', origin='upper', 
#                           vmin=-np.max(np.abs(spat_filt_weights)),
#                          vmax=+np.max(np.abs(spat_filt_weights)))
#     axes[i_weight].grid(False)
#     axes[i_weight].set_adjustable('box-forced')
# pyplot.xticks((0,1), ('C3', 'C4'))
# pyplot.yticks((0,1), ('Temporal Filter 1', 'Temporal Filter 2'))
# None

# ## Spatial patterns
# 
# <div class="summary"><ul>
# <li> A spatial filter shows how to weigh sensors to reconstruct a source</li>
# <li> A spatial pattern shows with which weights a source is projected onto the sensors </li>
# </ul></div>
# 
# One should be aware that the spatial filters are not so easily interpretable. They do not directly reveal where in the brain the location of a source may be located. The filters only transform our sensors signals so that the filtered signals' variance can tell us which class we are looking at. This can be interpreted as weighing the sensor signals to reconstruct underlying sources in the brain (*backward-model*).
# The filters are influenced by things other than the location of the source, namely by the covariances between the sensors and the sources. We might be more interested in getting an idea about the location of a reconstructed source, for example by seeing how the sources is projected to the sensors (*forward-model*, this would be the $Sensor1=0.6x, Sensor2=0.8x$,  $Sensor1=0.8x, Sensor2=0.6x$ parts of our artificial example). This is possible by inverting and transposing the filters to get *spatial patterns* $A=(W^{-1})^{\top}$.
# In our case, this would give us the coefficients $Sensor1=1.77x, Sensor2=2.33x$,  $Sensor1=-2.33x, Sensor2=-1.75x$. These roughly are just scaled versions of our original coefficients. If we norm them so that the squared sum is 1, we approximately get  $Sensor1=0.6x, Sensor2=0.8x$,  $Sensor1=-0.8x, Sensor2=-0.6x$. These are our original coefficients, except for the sign flip for the second coefficients. So these patterns tell us what we wanted - how strongly the signal is projected to the different sensors.

# ### Activations
# 
# <div class="summary">
# <ul>
# <li>Activations can show you how network transforms the data to get useful features</li>
# </ul>
# </div>
# 
# We can also look at the activations for specific trials to see how the signals get transformed for the classification.
# We will look at a trial which the net confidently correctly classifies as left hand.
# We would expect a decrease of variance in a certain frequency band on sensor C4.
# In the trial, the lower variance of C4 copmared to C3 is mostly visible between around 500 to 1000 milliseconds and after 2500 milliseconds.

# In[ ]:

from motor_imagery_learning.train_scripts.train_with_params import (
    create_training_object_from_file)
bhno_left_right_train = create_training_object_from_file(
    'data/models/two-class/raw-square/increasing-filters/test/36.yaml')
bhno_left_right_train.dataset_splitter.ensure_dataset_is_loaded()
datasets= bhno_left_right_train.get_train_fitted_valid_test()

test_preds = bhno_right_left_model.info_predictions['test']

test_topo = datasets['test'].get_topological_view()
test_y = datasets['test'].y

pred_correct = np.argmax(test_preds, axis=1) == np.argmax(test_y, axis=1)
np.sum(pred_correct) / float(len(test_preds))
pred_correct_inds = np.flatnonzero(pred_correct)

#print test_preds[pred_correct_inds][0]
#print test_preds[pred_correct_inds][8]
#print pred_correct_inds[8]

fig = plot_sensor_signals(test_topo[9].squeeze(), ['C3', 'C4'], figsize=(8,2))
fig.suptitle('Raw Signals', fontsize=16, y=1.03)
pyplot.xticks(range(0,601,150), [0,1000,2000,3000,4000])
pyplot.xlabel('milliseconds')
fig.axes[0].axvspan(75, 170, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
fig.axes[1].axvspan(75, 170, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
fig.axes[0].axvspan(375, 600, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
fig.axes[1].axvspan(375, 600, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
None


# Now we look at the activations after layer 1, the temporally filtered signals. We see that the filter 2 seems to resemble a bandpass, it mostly retains an oscillation in a certain frequency band.

# In[ ]:

from motor_imagery_learning.analysis.util import create_activations_func

activations_func = create_activations_func(bhno_right_left_model)
activations = activations_func(test_topo[9:10])
_ = plot_sensor_signals_two_classes(activations[0][0,0].T, activations[0][0,1].T, 
                                    sensor_names=['C3', 'C4'], 
                                    class_1_name='Temporal Filter 1',
                                    class_2_name='Temporal Filter 2',
                                   figsize=(8,4)).suptitle(
    'Temporally Filtered Signals (outputs of first layer)', fontsize=16)
pyplot.xticks([0,150,300,450], [0,1000,2000,3000])
pyplot.xlabel('milliseconds (at start of convolution region)')
None


# When we look at the squared outputs of the second layer, we see that the net manages to transform the signal to signals useful for classification. The combined filter 1 and 4 now show higher variances than 2 and 3.

# In[ ]:

sensor_names = map(lambda i: "Combined Filter {:d}".format(i), range(1,5))
fig = plot_sensor_signals(activations[1].squeeze(), sensor_names, figsize=(8,4))
fig.suptitle('Squared output of the second layer', fontsize=16)
for i in (0,3):
    fig.axes[i].axvspan(50, 110, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
    fig.axes[i].axvspan(400, 571, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
for i in (1,2):
    fig.axes[i].axvspan(50, 110, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
    fig.axes[i].axvspan(400, 571, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
#fig.axes[1].axvspan(75, 130, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
#fig.axes[0].axvspan(375, 600, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
pyplot.xticks([0,150,300,450], [0,1000,2000,3000])
pyplot.xlabel('milliseconds (at start of convolution region)')
None


# Finally, the network pools together the outputs by a sliding window with 50 samples length and 10 samples stride.
# We use the logarithm of the sum of the window as a feature for our softmax classifier. 
# The output after pooling is shown below and you again can nicely see the difference between combined filters 1/4 and 2/3.

# In[ ]:

sensor_names = map(lambda i: "Combined Filter {:d}".format(i), range(1,5))
fig = plot_sensor_signals(activations[2].squeeze(), sensor_names, figsize=(8,4))
fig.suptitle('Output after pooling', fontsize=16)
for i in (0,3):
    fig.axes[i].axvspan(3, 8, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
    fig.axes[i].axvspan(40, 52, color=seaborn.color_palette()[2], alpha=0.2, lw=0)
for i in (1,2):
    fig.axes[i].axvspan(3, 8, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
    fig.axes[i].axvspan(40, 52, color=seaborn.color_palette()[1], alpha=0.2, lw=0)
pyplot.xticks([0,15,30,45], [0,1000,2000,3000])
pyplot.xlabel('milliseconds (at start of pool region)')
None


# If we look at the pooled output weighted with the softmax weights and summed across the combined filters, we get a timecourse for both classes. We can see that the classifier is mostly able to seperate the trial by the time until around 1300 milliseconds and by the time between around 1550 and 1900 milliseconds.

# In[ ]:

softmax_in = (bhno_right_left_model.layers[-1].get_weights_topo().transpose(0,3,1,2) *
              activations[2])
pyplot.plot(np.sum(softmax_in, axis=1).squeeze().T)

pyplot.xticks([0,15,19.5,23.25,30,45], [0,1000,1300,1550,2000,3000])
pyplot.legend(("Right Hand", "Left Hand"), loc='right', bbox_to_anchor=(1.225, 0.55))

pyplot.xlabel('milliseconds (at start of pool region)')
pyplot.title(
    'Weighted Softmax inputs, summed across units of second layer', fontsize=16)
None


# The network correctly classifies this with 98.9% as a left hand trial.
