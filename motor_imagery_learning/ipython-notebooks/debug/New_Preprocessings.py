
# coding: utf-8

# In[33]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# make sure to use cpu for printing results...
os.environ['THEANO_FLAGS'] = "floatX=float32,device=cpu,nvcc.fastmath=True"
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7
from analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file
from motor_imagery_learning.analysis.print_results import ResultPrinter
from motor_imagery_learning.csp.print_csp_results import CSPResultPrinter
from pylearn2.utils import serial
import numpy as np
from motor_imagery_learning.analysis.plot_util import (plot_nlls_for_file, 
    plot_misclasses, plot_misclasses_for_file, plot_nlls)
from motor_imagery_learning.mypylearn2.preprocessing import OnlineAxiswiseStandardize
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
from pylearn2.format.target_format import OneHotFormatter


# In[61]:

from numpy.random import RandomState
rng = RandomState(np.uint64(hash('asdasdas')))
input_shape = (10,12,3,5)
y = OneHotFormatter(4).format(np.random.random_integers(0,3,size=input_shape[0]))
# important that topo view not created here as variable as we want to free its memory later
# by deleting dataset.X!
train_set = DenseDesignMatrix(topo_view = rng.rand(*input_shape).astype(np.float32), y=y, axes=('b', 'c', 0, 1))
test_set = DenseDesignMatrix(topo_view = rng.rand(*input_shape).astype(np.float32), y=y, axes=('b', 'c', 0, 1))


# In[67]:

print np.mean(train_set.get_topological_view())
print np.mean(test_set.get_topological_view())
print np.var(train_set.get_topological_view())
print np.var(test_set.get_topological_view())


# In[68]:

preproc = OnlineAxiswiseStandardize(axis=['c',0])

preproc.determine_standardization_dims(dataset.view_converter.axes)


# In[69]:

train_set.view_converter.axes


# In[70]:

preproc.apply(train_set, can_fit=True)
preproc.apply(test_set, can_fit=False)


# In[77]:

preproc._mean.shape


# In[72]:

print np.mean(train_set.get_topological_view())
print np.mean(test_set.get_topological_view())
print np.var(train_set.get_topological_view())
print np.var(test_set.get_topological_view())


# In[74]:


print np.mean(train_set.get_topological_view(), axis=(0,3))
print np.mean(test_set.get_topological_view(), axis=(0,3))
print np.var(train_set.get_topological_view(), axis=(0,3))
print np.var(test_set.get_topological_view(), axis=(0,3))


# In[76]:

print np.mean(train_set.get_topological_view(), axis=(1,2))
print np.mean(test_set.get_topological_view(), axis=(1,2))
print np.var(train_set.get_topological_view(), axis=(1,2))
print np.var(test_set.get_topological_view(), axis=(1,2))

