
# coding: utf-8

# In[1]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# switch to cpu
#os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
from motor_imagery_learning.mypylearn2.learning_rule_adam import Adam


# Plan:
# 
# have fast experiment bandpower just load four good sensors, note down result, print start and stop
# 
# remove memoize, bring it to crash here
# make it work without memoize, just load directly do not check
# make split inside train test early stop, replicate results from here
# remove validation start etc. form motor eeg.yaml, remove validationdataset etc etc from motor eeg.yaml
# (also check they are set both for crossval and for early stop)
# try to replicate results from bandpower experiment
# 
# repeat experiments form before with and without dividing by win length etc.
# 
# start from train early stop only getting one dataset and then splitting it according to folds
# remove validation start stop test start stop parameters from motor eeg etc
# 
# 
# del algorithm monitoring datasets before rereading them

# In[2]:

import sys
import numbers
import collections

def getsize(obj):
    # recursive function to dig out sizes of member objects:               
    def inner(obj, _seen_ids = set()):
        obj_id = id(obj)
        if obj_id in _seen_ids:
            return 0
        _seen_ids.add(obj_id)
        size = sys.getsizeof(obj)
        assert len(_seen_ids) < 1000000
        if isinstance(obj, (basestring, numbers.Number, xrange)):
            pass # bypass remaining control flow and return                
        elif isinstance(obj, (tuple, list, set, frozenset)):
            size += sum(inner(i) for i in obj)
        elif isinstance(obj, collections.Mapping) or hasattr(obj, 'iteritems'):
            size += sum(inner(k) + inner(v) for k, v in obj.iteritems())
        elif isinstance(obj, np.ndarray):
            size += obj.nbytes
        else:
            attr = getattr(obj, '__dict__', None)
            if attr is not None:
                size += inner(attr)
        return size
    return inner(obj)


# ### Memory investigation

# ### after new early stop: for even smaller artificial data float32 np.ones((766,128,300,2),dtype=np.float32)
# * before loading: 658
# * after loading: 883
# * after creation of inner trainer: 1111
# * after deletion datasetX: 887
# * after early stop trainer main loop: 941
# * after remember best params and remove early stop trainer: 715
# * after recreation dataset: 938
# * after creation after stop trainer: 938
# * after after stop trainer main loop: 941

# In[3]:

from motor_imagery_learning.util import memory_usage
import gc
gc.collect()
memory_usage()


# In[4]:

#from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
#dataset = BBCIPylearnCleanFFTDataset('data/BBCI-without-last-runs/BhNoMoSc1S001R01_ds10_1-12.BBCI.mat',
#                                  load_sensor_names=get_C_sensors_sorted(), 
#                                     frequency_start=2, frequency_stop=50)
#dataset.load()


# In[5]:

from pylearn2.format.target_format import OneHotFormatter
input_shape = (766,128,300,2)
y = OneHotFormatter(4).format(np.random.random_integers(0,3,size=input_shape[0]))
# important that topo view not created here as variable as we want to free its memory later
# by deleting dataset.X!
dataset = DenseDesignMatrix(topo_view = np.ones(input_shape, dtype=np.float32), y=y, axes=('b', 'c', 0, 1))


# In[6]:

gc.collect()
memory_usage()


# In[7]:

softmax_layer = Softmax(n_classes=4, layer_name='y', irange=0.0001, max_col_norm=2.)
model = MLP([softmax_layer])
sgd_algo = SimilarBatchSizeSGD(batch_size=5,cost=Default(),termination_criterion=EpochCounter(max_epochs=2),
               learning_rate=0.1, seed=[2012, 10, 16], learning_rule=Adam())
trainer = TrainEarlyStop(dataset, model, algorithm=sgd_algo, preprocessor=OnlineStandardize())
del dataset
del y
del model
del sgd_algo
del softmax_layer


# In[8]:

gc.collect()
memory_usage()


# In[9]:

#trainer._ensure_dataset_is_loaded()
trainer._remember_original_algorithm()
trainer._create_early_stop_trainer()


# In[10]:

gc.collect()
memory_usage()


# In[11]:

del trainer.dataset.X


# In[12]:

gc.collect()
memory_usage()


# In[13]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer.early_stop_trainer.main_loop()')


# In[14]:

gc.collect()
memory_usage()


# In[15]:

trainer._remember_best_params()
trainer._remove_early_stop_trainer()


# In[16]:

gc.collect()
memory_usage()


# In[17]:

y = OneHotFormatter(4).format(np.random.random_integers(0,3,size=input_shape[0]))
# important that topo view not created here as variable as we want to free its memory later
# by deleting dataset.X!
dataset = DenseDesignMatrix(topo_view = np.ones(input_shape, dtype=np.float32), y=y, axes=('b', 'c', 0, 1))
trainer.dataset = dataset
del dataset
del y


# In[18]:

gc.collect()
memory_usage()


# In[19]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer._create_and_setup_after_stop_trainer()')


# In[20]:

gc.collect()
memory_usage()


# In[21]:

trainer.after_stop_trainer.run_training()


# In[22]:

gc.collect()
memory_usage()


# 
# ## Unknown stuff below, maybe just delete :)

# In[26]:

from pylearn2.utils import serial
from motor_imagery_learning.train_scripts.train_with_params import create_training_object_from_file
compare_result = serial.load('data/models/debug/bandpower_new_early_stop/early-stop/15.result.pkl')
print np.array(compare_result.monitor_channels['test_y_misclass'].val_record)


# In[28]:

print np.array(trainer.model.monitor.channels['test_y_misclass'].val_record)


# In[14]:

softmax_layer = Softmax(n_classes=4, layer_name='y', irange=0.0001, max_col_norm=2.)
model = MLP([softmax_layer])
bgd_algo = BGD(batch_size=776,cost=Default(),termination_criterion=EpochCounter(max_epochs=5),conjugate=True, 
               line_search_mode='exhaustive', updates_per_batch=5, seed=[2012, 10, 16])
trainer = TrainEarlyStop(dataset, model, algorithm=bgd_algo, preprocessor=OnlineStandardize())


# In[101]:

#cpu result
print trainer.model.monitor.channels['test_y_misclass'].val_record #0.16883117 at end.... 


# In[15]:

trainer.main_loop()


# In[ ]:

#comparisoN:
	test_y_misclass: 0.168831
	test_y_nll: 0.639768
	test_y_row_norms_max: 0.235622
	test_y_row_norms_mean: 0.0661587
	test_y_row_norms_min: 0.0035369
	total_seconds_last_epoch: 0.134273
	train_objective: 0.000588383
	train_y_col_norms_max: 1.93286
	train_y_col_norms_mean: 1.76129
	train_y_col_norms_min: 1.59316
	train_y_max_max_class: 1.0
	train_y_mean_max_class: 0.999412
	train_y_min_max_class: 0.994518
	train_y_misclass: 0.0
	train_y_nll: 0.000588383
	train_y_row_norms_max: 0.235622
	train_y_row_norms_mean: 0.0661587
	train_y_row_norms_min: 0.0035369
	training_seconds_this_epoch: 0.115949
	valid_objective: 0.000708176
	valid_y_col_norms_max: 1.93286
	valid_y_col_norms_mean: 1.76129
	valid_y_col_norms_min: 1.59316
	valid_y_max_max_class: 1.0
	valid_y_mean_max_class: 0.999292
	valid_y_min_max_class: 0.996164
	valid_y_misclass: 0.0
	valid_y_nll: 0.000708176
	valid_y_row_norms_max: 0.235622
	valid_y_row_norms_mean: 0.0661587
	valid_y_row_norms_min: 0.0035369

