
# coding: utf-8

# In[1]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import numpy as np
from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import ConvElemwise
from theano.sandbox.cuda.dnn import dnn_conv
from theano.sandbox.cuda.basic_ops import gpu_alloc_empty
from motor_imagery_learning.analysis.print_results import ResultPrinter


# In[3]:

get_ipython().run_cell_magic(u'javascript', u'', u'$(\'body\').append(\'<script src="https://dl.dropboxusercontent.com/u/34637013/work/ipython-notebooks/js-for-table-sorting/jquery.tablesorter.min.js"></script>\')\n// Sort tables every 5 seconds...\nvar makeTablesSortable;\nmakeTablesSortable = function() { \n  $(\'.text_cell_render table\').not(\'.sorted_table\').tablesorter().addClass(\'sorted_table\');\n}\n\nsetInterval(makeTablesSortable, 5000);')


# * best seems to be to use 50 chans and no dropout?!  87% -> still 10% below combined first conv
# * already 4 chans with dropout very good 
# * maybe try 50 chans with dropout?

# 
# |id|spat_kernel_norm|first_chans|spat_drop_probs|spat_drop_scales|time|valid_test|test|best|epoch|train|valid|
# |-|-|-|-|-|-|-|-|-|-|-|-|
# |2|2.0|50|-|-|0:17:17|82.89%|78.95%|82.89%|603|91.27%|86.84%|
# |3|0.5|50|-|-|0:13:25|85.53%|84.21%|86.84%|540|90.39%|86.84%|
# |4|0.5|80|-|-|0:14:53|67.76%|60.53%|63.16%|243|74.53%|75.00%|
# |5|0.5|160|-|-|0:40:57|80.26%|73.68%|76.32%|581|86.32%|86.84%|
# |6|1.0|50|-|-|0:16:04|83.55%|80.26%|82.89%|644|90.39%|86.84%|
# |7|1.0|80|-|-|0:12:11|62.50%|59.21%|61.84%|224|69.29%|65.79%|
# |8|1.0|160|-|-|0:51:34|82.24%|77.63%|78.95%|832|87.34%|86.84%|
# |9|2.0|50|-|-|0:18:26|87.50%|86.84%|86.84%|787|91.41%|88.16%|
# |10|2.0|80|-|-|0:11:39|65.79%|61.84%|61.84%|224|71.32%|69.74%|
# |11|2.0|160|-|-|0:58:31|83.55%|77.63%|78.95%|1020|89.08%|89.47%|
# |12|0.5|4|-|-|0:04:39|70.39%|68.42%|68.42%|327|74.09%|72.37%|
# |13|0.5|4|0.5|2.0|0:06:49|80.92%|77.63%|78.95%|229|87.48%|84.21%|
# |14|0.5|4|1.0|1.0|0:04:37|70.39%|68.42%|68.42%|327|74.38%|72.37%|
# |15|0.5|8|0.5|2.0|0:07:31|71.05%|61.84%|65.79%|489|79.48%|80.26%|
# |16|0.5|8|1.0|1.0|0:07:19|76.97%|73.68%|75.00%|405|80.93%|80.26%|
# |17|0.5|16|0.5|2.0|0:07:35|61.84%|55.26%|63.16%|234|73.65%|68.42%|
# |18|0.5|16|1.0|1.0|0:10:52|72.37%|67.11%|69.74%|592|82.68%|77.63%|
# |19|1.0|4|0.5|2.0|0:08:21|82.89%|80.26%|81.58%|599|88.79%|85.53%|
# |20|1.0|4|1.0|1.0|0:05:04|68.42%|64.47%|67.11%|357|74.38%|72.37%|
# |21|1.0|8|0.5|2.0|0:06:10|64.47%|61.84%|64.47%|124|75.55%|67.11%|
# |22|1.0|8|1.0|1.0|0:18:32|72.37%|67.11%|69.74%|292|75.69%|77.63%|
# |23|1.0|16|0.5|2.0|0:14:57|79.61%|77.63%|77.63%|891|85.01%|81.58%|
# |24|1.0|16|1.0|1.0|0:08:56|73.03%|67.11%|69.74%|509|81.51%|78.95%|
# |25|2.0|4|0.5|2.0|0:06:51|82.24%|80.26%|81.58%|490|87.63%|84.21%|
# |26|2.0|4|1.0|1.0|0:05:04|68.42%|63.16%|65.79%|348|76.13%|73.68%|
# |27|2.0|8|0.5|2.0|0:06:12|64.47%|61.84%|64.47%|124|75.25%|67.11%|
# |28|2.0|8|1.0|1.0|0:05:21|72.37%|67.11%|69.74%|288|76.13%|77.63%|
# |29|2.0|16|0.5|2.0|0:15:00|78.95%|76.32%|77.63%|877|85.30%|81.58%|
# |30|2.0|16|1.0|1.0|0:09:27|73.68%|69.74%|69.74%|559|81.22%|77.63%|

# In[2]:

ResultPrinter('data/models/debug/raw-data-spatial/early-stop/').print_results(
    print_templates_and_constants=False)

