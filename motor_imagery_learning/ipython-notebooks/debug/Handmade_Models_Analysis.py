
# coding: utf-8

# In[1]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
#os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import numpy as np
from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import ConvElemwise
from theano.sandbox.cuda.dnn import dnn_conv
from theano.sandbox.cuda.basic_ops import gpu_alloc_empty
from motor_imagery_learning.analysis.print_results import ResultPrinter
from motor_imagery_learning.analysis.plot_util import plot_chan_matrices
from motor_imagery_learning.mypylearn2.preprocessing import Standardize
from motor_imagery_learning.bbci_pylearn_dataset import compute_power_spectra, BBCISetNoCleaner
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset

from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from pylearn2.models.mlp import MLP, Softmax
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
from numpy.random import RandomState
import numpy as np
from pylearn2.format.target_format import OneHotFormatter
from pylearn2.termination_criteria import EpochCounter
from motor_imagery_learning.mypylearn2.dataset_splitters import (DatasetSingleFoldSplitter, 
                                                                 DatasetTwoFileSingleFoldSplitter)
from copy import deepcopy
from motor_imagery_learning.analysis.util import  get_dataset_predictions

import matplotlib
from matplotlib import pyplot
from matplotlib import cm
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (8.0, 1.0)
matplotlib.rcParams['font.size'] = 7
from motor_imagery_learning.analysis.plot_util import plot_misclasses_for_model
from motor_imagery_learning.train_scripts.train_with_params import create_training_object_from_file


# In[2]:

from motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer
from motor_imagery_learning.analysis.data_generation import (create_sine_signal,
    SingleChanDataset, pipeline, create_shifted_sines,put_noise_on_set, convolve_with_weight,
    max_pool_topo_view, log_sum_pool)


# In[3]:

from motor_imagery_learning.mypylearn2.learning_rule_adam import Adam
from pylearn2.costs.mlp.dropout import Dropout
from motor_imagery_learning.mypylearn2.dataset_splitters import DatasetSingleFoldSplitter

def generate_single_sensor_data(filename, chan, min_freq=11, max_freq=11, low_width=2, last_low_freq=11,high_width=2):

    lower_dataset = BBCIPylearnCleanFilterbankDataset(filenames=filename,
                               load_sensor_names=[chan], cleaner=BBCISetNoCleaner(),
                                min_freq=min_freq, max_freq=max_freq, low_width=low_width, 
                               last_low_freq=last_low_freq,high_width=high_width)

    lower_dataset.load()
    
    classes = np.argmax(lower_dataset.y, axis=1)
    wanted_trials = np.logical_or(classes == 0, classes == 2)
    lower_topo_view = lower_dataset.get_topological_view()
    lower_class_1_3_topo_view = lower_topo_view[wanted_trials]
    y_1_3 = lower_dataset.y[wanted_trials][:,[0,2]] # only keep the colums for classes 1/3
    return lower_class_1_3_topo_view, y_1_3

def create_trainer(topo_view, y, layers=None, cost="dropout", max_epochs=1000, preprocessor=None):
    if layers is None:
        layers = [Softmax(n_classes=2, layer_name='y', irange=0.001)]
    max_epochs_crit = EpochCounter(max_epochs=max_epochs)
    if cost == "dropout":
        cost = Dropout(input_include_probs={'y': 0.8}, input_scales={'y': 1/0.8})
    algorithm = SimilarBatchSizeSGD(learning_rate=0.1, batch_size=60,seed=np.uint64(hash('test_train_sgd')),
                                    termination_criterion=max_epochs_crit, learning_rule=Adam(),
                                   cost=cost)
    mlp = MLP(layers=layers, seed= np.uint64(hash('test_train_mlp')))
    dataset = DenseDesignMatrix(topo_view=topo_view, y=y, 
                                rng=RandomState(np.uint64(hash('batchhash?'))),
                                                         axes=('b', 'c', 0, 1))
    
    
    dataset_splitter = DatasetSingleFoldSplitter(dataset, num_folds=10, test_fold_nr=-1)
    trainer = TrainEarlyStop(dataset_splitter, mlp, algorithm=algorithm, save_path=None, preprocessor=None, 
                             keep_best_lrule_params=False, 
                             best_model_channel='valid_y_misclass', fraction_epochs_after_stop=1.0)
    return trainer


# In[5]:

# in the end understand how net would need to be constructed to pick up all possibilities you are thinking of:
# - csp-like: one source , spread to several sensors, in certain frequency, make several frequencies
# - motor related potentials
# - possibly: only increase/decrease of source important
# - kaputt sensors
# - noise on all
# - possibly: noisy csp-like-source overlayed?
#

# improvement goals:

# first try to improve on kaus raw 1v3 90.91% and anwe raw 78.26%
# data/models/two-class/raw-square/early-stop/1
# data/models/two-class/raw-square/early-stop/3
# later try if you can improve kaus csp 1v3 97.73% and answe csp 89.13%
#... and maybe also other results from 1vs2

# when you can make one step work on artificial data, also try if you can make it work on real data (kaus/anwe)...


# make conv model, just timeconv 150 + square + sumlogpool or
#                                    + square + max_pool
# first on fakedata, see if it learns properly, also see for raw+highpassed(!) data kaus
# also compare to lda on 500-4000ms interval just variance + log ("csp-pipeline" second step)
# see what is better
# make results understandable here, push them down, so reproducibly understandable


# go on towards model: (conv over chans+ conv over time + squaring?
# then occasionally destroy some chans for some trials to destory model, maybe also only in test
# then make to conv over time + conv over chans + conv over time + squaring or sth else which might be able to kick 
# out destroyed chans (think how its possible to "kick out" chans)

# make second phase: fakedata which is split up according two sensors 
# (there try out same noise on both sensors and different noise on different sensors)



# In[6]:

# try problem at startsets kaus/answe:
# load dataset + model properly, verify results
# show failing trials at start + some successful trials for wanted class
# -friday
# try extracting features for successful trials on chan, extract for like trials 6-10, see what they look like
# do same for earlier trials see why there is a problem...
# see if ther eis some obvious difference
# in any case try different preprocessings, for example running standardization and see if it improves prediction


# ## Single Chan Experiments

# In[4]:

clf = LDA()
input_shape=(1000,1,1200,1)
start_topo_view, y = create_shifted_sines(input_shape, RandomState(np.uint64(hash("pooled_pipeline"))))
topo_view_pooled, y_pooled = pipeline(start_topo_view, y, 
    lambda x,y: put_noise_on_set(x,weight_old_data=0.05),
    lambda x,y: convolve_with_weight(x, create_sine_signal(samples=150,freq=11, sampling_freq=300.0)),
    lambda x,y: max_pool_topo_view(x,pool_shape=50))
topo_view_noised, _ = pipeline(start_topo_view, y, 
    lambda x,y: put_noise_on_set(x,weight_old_data=0.05))

print ("topo view pooled", clf.fit_and_score_train_test_topo(topo_view_pooled, y_pooled))


# In[6]:

from motor_imagery_learning.mypylearn2.conv_squared import ConvSquared
# convolve + square + maxpool
layers = [ConvSquared(output_channels=1, kernel_shape=[150,1], pool_shape=[50,1], pool_stride=[50,1],
                     pool_type="max",layer_name="conv_squared", irange=0.05, max_kernel_norm=2.0, tied_b=True),
          Softmax(n_classes=2, layer_name='y', irange=0.001, max_col_norm=0.5)]
trainer = create_trainer(topo_view_noised, y,max_epochs=400, layers=layers, cost=None)


# In[ ]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer.run_until_earlystop()')


# In[81]:

pyplot.plot(np.squeeze(trainer.model.get_weights_topo()))


# In[68]:

pyplot.plot(np.squeeze(trainer.model.layers[1].get_weights_topo().T))


# In[82]:

trainer.model.monitor.channels['train_y_misclass'].val_record[-10:]


# In[83]:

plot_misclasses_for_model(trainer.model)


# In[8]:

kaus_train = create_training_object_from_file('data/models/two-class/raw-square/early-stop/3.yaml')

kaus_train.dataset_splitter.ensure_dataset_is_loaded()
kaus_datasets = kaus_train.get_train_fitted_valid_test()
kaus_model = serial.load('data/models/two-class/raw-square/early-stop/3.pkl').model
kaus_result = serial.load('data/models/two-class/raw-square/early-stop/3.result.pkl')


# In[25]:

inputs = T.ftensor4()
fprop_result = kaus_model.fprop(inputs)
fprop = theano.function([inputs], fprop_result)
kaus_out = fprop(kaus_datasets['test'].get_topological_view().astype(np.float32))
assert np.allclose(kaus_result.predictions['test'], kaus_out)


# In[42]:

clf = LDA()
chan='CP3'
filename = 'data/BBCI-without-last-runs/KaUsMoSc1S001R01_ds10_1-11.BBCI.mat'
kaus_topo, kaus_y = generate_single_sensor_data(filename, chan)
kaus_pooled, kaus_pooled_y = pipeline(kaus_topo, kaus_y, 
    lambda x,y: convolve_with_weight(x, create_sine_signal(samples=150,freq=11, sampling_freq=300.0)),
    lambda x,y: x*x,
    lambda x,y: max_pool_topo_view(x,pool_shape=50))
assert np.array_equal(kaus_y, kaus_pooled_y)
kaus_single_feature = np.sum(kaus_pooled, axis=2, keepdims=True)
print ("topo view pooled", clf.fit_and_score_train_test_topo(kaus_pooled, kaus_y, sklearn_test_fold=True))
print("one feature accuracy", OneFeatureScorer().fit_and_score_train_test_topo(kaus_single_feature, kaus_pooled_y))


# In[ ]:

ge


# ## Compare if they fail on similar trials atleast

# In[1182]:

# make plot of correct_incorrect trials for both
test_pred = kaus_result.predictions['test']
test_targets = kaus_result.targets['test']
preds = clf.predict_prob(np.squeeze(kaus_pooled[-44:]))

pyplot.plot(preds / 10.0)
pyplot.plot(test_pred[:,1] - 0.5)
pyplot.plot(2*test_targets[:,1] - 1)
pyplot.axhline(y=0, color='black')


# In[1058]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer = create_trainer(kaus_pooled[:,:,:,:], kaus_pooled_y, max_epochs=200)\ntrainer.run_until_earlystop()')


# In[1064]:

plot_misclasses_for_model(trainer.model)


# In[1062]:

print trainer.model.monitor.channels['test_y_misclass'].val_record[-20:]


# In[1060]:

pyplot.plot(np.squeeze(trainer.model.layers[0].get_weights()))


# In[1034]:

def to_class_nr(y):
    return np.argmax(y, axis=1)


# In[1061]:

pyplot.plot(np.mean(np.squeeze(kaus_pooled[to_class_nr(kaus_pooled_y) == 0]), axis=0))
pyplot.plot(np.mean(np.squeeze(kaus_pooled[to_class_nr(kaus_pooled_y) == 1]), axis=0))

#kaus_pooled[to_class_nr(kaus_pooled_y) == 1]


# In[874]:

single_chan_set = SingleChanDataset()
single_chan_set.load()

# topo_view, y= pipeline(*create_shifted_sines(input__shape, RandomState..), 
#    lambda x,y: put_noise_on_set(x,weight_old_data),
#    lambda x,y: convolve_with_weight(x, create_sine_signal(samples=150,freq=11, sampling_freq=300.0),
#    lambda x,y: pool_topo_view(x,pool_shape=50)))


# In[974]:




# In[883]:




max_epochs_crit = EpochCounter(max_epochs=1000)
cost = Dropout(input_include_probs={'y': 0.8}, input_scales={'y': 1/0.8})
#cost = None
algorithm = SimilarBatchSizeSGD(learning_rate=0.1, batch_size=60,seed=np.uint64(hash('test_train_sgd')),
                                termination_criterion=max_epochs_crit, learning_rule=Adam(),
                               cost=cost)

softmax = Softmax(n_classes=2, layer_name='y', irange=0.001)
mlp = MLP(layers=[softmax], seed= np.uint64(hash('test_train_mlp')))



dataset = DenseDesignMatrix(topo_view=single_chan_set.topo_view_pooled, y=single_chan_set.y, 
                            rng=RandomState(np.uint64(hash('batchhash?'))))
dataset_splitter = DatasetSingleFoldSplitter(dataset, num_folds=10, test_fold_nr=-1)
trainer = TrainEarlyStop(dataset_splitter, mlp, algorithm=algorithm, save_path=None, preprocessor=None, 
                         keep_best_lrule_params=False, 
                         best_model_channel='valid_y_misclass', fraction_epochs_after_stop=1.0)


# In[884]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer.run_until_earlystop()')


# In[885]:

print("train", 1-trainer.model.monitor.channels['train_y_misclass'].val_record[-1])
print("test", 1-trainer.model.monitor.channels['test_y_misclass'].val_record[-1])
# old before dataset class
# without dropout
#('train', 0.90124999731779099)
#('test', 0.90999999642372131)
# with dropout
#('train', 0.89750000089406967)
#('test', 0.92000000178813934)



# In[886]:

pyplot.plot(trainer.model.monitor.channels['train_y_misclass'].val_record)
pyplot.plot(trainer.model.monitor.channels['valid_y_misclass'].val_record)
pyplot.plot(trainer.model.monitor.channels['test_y_misclass'].val_record)


# In[887]:

pyplot.plot(trainer.model.get_weights())


# In[888]:

np.mean(trainer.model.layers[0].get_weights()[:,0])


# In[889]:

np.mean(trainer.model.layers[0].get_weights()[:,1])


# In[890]:

trainer.model.layers[0].get_biases()


# In[934]:




# In[933]:

from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset

