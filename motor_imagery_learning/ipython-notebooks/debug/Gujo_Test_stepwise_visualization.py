
# coding: utf-8

# In[1]:

import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/') 
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ


# In[2]:

get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
from matplotlib import cm
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from analysis.plot_util import plot_class_probs, plot_chan_matrices, plot_class_reconstruction,     plot_correct_part, plot_incorrect_part, plot_correctness
from analysis.util import back_deconv, back_prop, back_guided_deconv, back_linear, bc01toinput,     load_preprocessed_datasets, back_guided_deconv_for_class, reshape_and_get_predictions,     get_trials_for_class, get_summed_activations, get_activations, get_activation
from pylearn2.config import yaml_parse
from analysis.sensor_positions import get_C_sensors, sort_topologically
from copy import deepcopy
figsize=(13,8)


# In[3]:

# Want to know how algorithm can really help understand brain waves
# want to have algorithm that quickly (cheaply) recognizes intentions

from train_scripts.train_experiments import ExperimentsRunner
# Get train object for specific filename and nr
runner = ExperimentsRunner('configs/experiments/conv_nets_all_C_sensors.yaml', cross_validation=False)
# train one step 
# show train/test/valid performance
# for class 1
# visualize most active filter class 1
runner.create_all_experiments()
i = 4
gujo_experiment = runner._all_experiments[i]
gujo_experiment = runner._process_experiment_parameters(gujo_experiment, i)


# In[4]:

gujo_experiment['save_path'] = None
gujo_experiment['save_freq'] = 0
gujo_experiment


# In[6]:

from train_scripts.train_with_params import process_yaml_file, create_training_object
train_string = process_yaml_file(runner._template_file_name,
            command_line_train_parameters=gujo_experiment,
            parameter_list_name='default', 
            cross_validation=runner._cross_validation,
            early_stop=runner._early_stop,
            transfer_learning=runner._transfer_learning)
train_obj = create_training_object(train_string)


# In[7]:

train_obj.setup()


# In[8]:

get_ipython().run_cell_magic(u'capture', u'', u'train_obj.run_callbacks_and_monitoring()')


# In[9]:

print "{:3.1f}%".format((np.round(np.array(train_obj.model.monitor.channels['train_y_misclass'].val_record, 
                                   dtype=np.float64), decimals=3) * 100)[0])


# In[10]:

from copy import deepcopy
models =[deepcopy(train_obj.model)]


# In[36]:

get_ipython().run_cell_magic(u'capture', u'', u'#for epoch_nr in xrange(13)\n# run this cell repeatedly...\ntrain_obj.algorithm.train(train_obj.dataset)\ntrain_obj.model.monitor.report_epoch()\ntrain_obj.run_callbacks_and_monitoring()\n\nmodels.append(deepcopy(train_obj.model))')


# In[39]:

print "Epoch {:d}: {:3.1f}%".format(train_obj.model.monitor.get_epochs_seen(), 
                                    (np.round(np.array(train_obj.model.monitor.channels['train_y_misclass'].val_record, 
                               dtype=np.float64), decimals=3) * 100)[-1])


# In[205]:

from pylearn2.space import Conv2DSpace
for model_i in xrange(6):
    weights = Conv2DSpace.convert_numpy(models[model_i].get_weights_topo(), ('b', 0, 1, 'c'), ('b', 'c', 0, 1))
    plot_chan_matrices(weights[5], sort_topologically(get_C_sensors()), figsize=(6,3))


# In[46]:

from pylearn2.space import Conv2DSpace
def create_weights_animation(models, weight_nr=0, epochs=2, figsize=figsize):
    
    fig = pyplot.figure(figsize=figsize)
    def weight_animate(model_i):
        weights = Conv2DSpace.convert_numpy(models[model_i].get_weights_topo(),
                                            ('b', 0, 1, 'c'), ('b', 'c', 0, 1))
        plot_chan_matrices(weights[weight_nr], sort_topologically(get_C_sensors()), figure=fig)

    anim = animation.FuncAnimation(fig, weight_animate, frames=epochs, interval=1, blit=True)
    pyplot.close(fig)
    return anim
    
#weights_anim = create_weights_animation(models, weight_nr=1, epochs=9)

def show_weights_animation(models, fps=1, weight_nr=0, epochs=9,figsize=figsize):
    weights_anim = create_weights_animation(models, weight_nr=weight_nr, epochs=epochs, figsize=figsize)
    return display_animation(weights_anim, fps=fps)


# In[54]:

weights_anim = create_weights_animation(models, weight_nr=0, epochs=13, figsize=(11,8))


# In[55]:

display_animation(weights_anim, fps=1)


# In[204]:

np.mean(np.abs(models[5].get_weights_topo()[5] - models[0].get_weights_topo()[5]) 
        / np.abs(models[0].get_weights_topo()[5]))


# ## Video making 

# In[43]:

from tempfile import NamedTemporaryFile
from matplotlib import animation

def anim_to_html(anim, fps):
    VIDEO_TAG = """<video controls>
 <source src="data:video/x-m4v;base64,{0}" type="video/mp4">
 Your browser does not support the video tag.
</video>"""
    if not hasattr(anim, '_encoded_video'):
        with NamedTemporaryFile(suffix='.mp4') as f:
            mywriter = animation.FFMpegWriter(fps=fps, extra_args=['-pix_fmt', 'yuv420p', '-acodec', 'libfaac', 
                                                          '-vcodec', 'libx264'])
            anim.save(f.name, writer=mywriter)
            video = open(f.name, "rb").read()
        anim._encoded_video = video.encode("base64")
    
    return VIDEO_TAG.format(anim._encoded_video)


# In[44]:

from IPython.display import HTML

def display_animation(anim, fps):
    pyplot.close(anim._fig)
    return HTML(anim_to_html(anim, fps))


# ## Old Video 

# In[176]:

import numpy as np
from matplotlib import pyplot
from matplotlib import animation

# First set up the figure, the axis, and the plot element we want to animate
fig = pyplot.figure(figsize=(8,3))
ax = pyplot.axes(xlim=(0, 2), ylim=(-2, 2))
line, = ax.plot([], [], lw=2)

# initialization function: plot the background of each frame
def init():
    line.set_data([], [])
    return line,

noise = np.random.normal(size=(1000)) 

# animation function.  This is called sequentially
def animate(i):
    x = np.linspace(0, 2, 1000)
    y = np.sin(2 * np.pi * (x - 0.01 * i)) + noise
    line.set_data(x, y)
    return line,

# call the animator.  blit=True means only re-draw the parts that have changed.
anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=100, interval=20, blit=True)
pyplot.close(fig)


# In[177]:

display_animation(anim, fps=20)


# In[ ]:

# if you dont want to call explicit function: animation.Animation._repr_html_ = anim_to_html

