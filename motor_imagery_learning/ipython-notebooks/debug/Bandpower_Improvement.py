
# coding: utf-8

# In[ ]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import numpy as np
from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import ConvElemwise
from theano.sandbox.cuda.dnn import dnn_conv
from theano.sandbox.cuda.basic_ops import gpu_alloc_empty
from motor_imagery_learning.analysis.print_results import ResultPrinter


# * Bandpower phase not working
# * Results worse for both using phase and using phase diffs, normal pase even seems better?!

# In[38]:

get_ipython().run_cell_magic(u'javascript', u'', u'$(\'body\').append(\'<script src="https://dl.dropboxusercontent.com/u/34637013/work/ipython-notebooks/js-for-table-sorting/jquery.tablesorter.min.js"></script>\')\n// Sort tables every 5 seconds...\nvar makeTablesSortable;\nmakeTablesSortable = function() { \n  $(\'.text_cell_render table\').not(\'.sorted_table\').tablesorter().addClass(\'sorted_table\');\n}\n\nsetInterval(makeTablesSortable, 5000);')


# 
# 
# |id|files|phases_diff|div_win_len|transform_func|amplitude^2|time|std|valid_test|std|test|std|train|std|valid|std|
# |-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
# |0|5|-|False|power_func|False|0:02:06|0:00:21|89.17%|9.06%|78.34%|18.11%|100.00%|0.00%|100.00%|0.00%|
# |1|5|True|False|power_phase_func|False|0:02:33|0:00:38|85.37%|11.86%|70.74%|23.72%|100.00%|0.00%|100.00%|0.00%|
# |2|5|False|False|power_phase_func|False|0:02:31|0:00:25|87.22%|8.85%|74.44%|17.69%|100.00%|0.00%|100.00%|0.00%|
# |3|5|-|True|power_func|False|0:02:04|0:00:23|89.26%|8.53%|78.52%|17.06%|100.00%|0.00%|100.00%|0.00%|
# |4|5|True|True|power_phase_func|False|0:02:34|0:00:37|85.52%|11.11%|71.03%|22.22%|100.00%|0.00%|100.00%|0.00%|
# |5|5|False|True|power_phase_func|False|0:02:30|0:00:29|87.28%|8.39%|74.57%|16.78%|100.00%|0.00%|100.00%|0.00%|
# |6|5|-|False|power_func|True|0:02:03|0:00:26|89.75%|8.06%|79.50%|16.13%|100.00%|0.00%|100.00%|0.00%|
# |7|5|True|False|power_phase_func|True|0:02:34|0:00:48|86.38%|9.89%|72.75%|19.77%|100.00%|0.00%|100.00%|0.00%|
# |8|5|False|False|power_phase_func|True|0:02:25|0:00:25|85.46%|8.96%|70.93%|17.92%|100.00%|0.00%|100.00%|0.00%|
# |9|5|-|True|power_func|True|0:02:01|0:00:22|89.14%|7.25%|78.28%|14.50%|100.00%|0.00%|100.00%|0.00%|
# |10|5|True|True|power_phase_func|True|0:02:31|0:00:41|85.49%|10.24%|70.99%|20.48%|100.00%|0.00%|100.00%|0.00%|
# |11|5|False|True|power_phase_func|True|0:02:25|0:00:24|84.88%|10.07%|69.76%|20.13%|100.00%|0.00%|100.00%|0.00%|

# In[42]:

ResultPrinter('data/models/debug/bandpower-phase-diff/early-stop/first-5/').print_results(
    print_templates_and_constants=False)


# In[36]:

ResultPrinter('data/models/debug/bandpower/early-stop/').print_results(
    print_templates_and_constants=False)

