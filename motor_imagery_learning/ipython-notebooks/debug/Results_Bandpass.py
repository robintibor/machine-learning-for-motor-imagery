
# coding: utf-8

# In[2]:

import os

import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# make sure to use cpu for printing results...
os.environ['THEANO_FLAGS'] = "floatX=float32,device=cpu,nvcc.fastmath=True"
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7
from analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file
from motor_imagery_learning.analysis.print_results import ResultPrinter
from motor_imagery_learning.csp.print_csp_results import CSPResultPrinter
from pylearn2.utils import serial
import numpy as np
from motor_imagery_learning.analysis.plot_util import (plot_nlls_for_file, plot_misclasses_for_file, plot_nlls)
from glob import glob


# In[3]:

get_ipython().run_cell_magic(u'javascript', u'', u'$(\'body\').append(\'<script src="https://dl.dropboxusercontent.com/u/34637013/work/ipython-notebooks/js-for-table-sorting/jquery.tablesorter.min.js"></script>\')\n// Sort tables every 5 seconds...\nvar makeTablesSortable;\nmakeTablesSortable = function() { \n  $(\'.text_cell_render table\').not(\'.sorted_table\').tablesorter().addClass(\'sorted_table\');\n}\n\nsetInterval(makeTablesSortable, 5000);')


# ## Open Comparisons

# * first attempts (data/models/two-class/raw-square-bp/)
# * without fully connected ((data/models/two-class/raw-square-bp-no-full/)

# ### TODO 

# * longer bp shape like 75 or even 100?
# * more bp chans, 30 or 40?

# ## Analysis

# * ...

# ## Results 

# In[42]:

ResultPrinter('data/models/two-class/csp-net//early-stop//').print_results(print_templates_and_constants=False)


# In[43]:

ResultPrinter('data/models/two-class/raw-square/early-stop//').print_results(print_templates_and_constants=False)


# In[45]:

ResultPrinter('data/models/two-class/raw-square-bp/early-stop//').print_results(print_templates_and_constants=False, start=7)


# In[47]:

ResultPrinter('data/models/two-class/raw-square-bp-no-full/early-stop/').print_results(print_templates_and_constants=False)

