
# coding: utf-8

# In[1]:

get_ipython().run_cell_magic(u'capture', u'', u'import os\n\nimport site\nsite.addsitedir(\'/home/schirrmr/.local/lib/python2.7/site-packages/\')\nsite.addsitedir(\'/usr/lib/pymodules/python2.7/\')\nos.sys.path.insert(0, \'/home/schirrmr/motor-imagery/code/\')\n#site.force_global_eggs_after_local_site_packages()\n%cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/\nassert \'THEANO_FLAGS\' in os.environ\n# make sure to use cpu for csp...\nos.environ[\'THEANO_FLAGS\'] = "floatX=float32,device=cpu,nvcc.fastmath=True"\n%load_ext autoreload\n%autoreload 2\nimport matplotlib\nfrom matplotlib import pyplot\n%matplotlib inline\n%config InlineBackend.figure_format = \'svg\' \nmatplotlib.rcParams[\'figure.figsize\'] = (12.0, 5.0)\nmatplotlib.rcParams[\'font.size\'] = 7\nfrom analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file\nfrom motor_imagery_learning.analysis.print_results import ResultPrinter\nfrom motor_imagery_learning.csp.print_csp_results import CSPResultPrinter\nfrom pylearn2.utils import serial\nimport numpy as np\nfrom motor_imagery_learning.analysis.plot_util import (plot_nlls_for_file, \n    plot_misclasses_for_result, plot_misclasses_for_file, plot_nlls)\nfrom motor_imagery_learning.analysis.results import ResultPool, DatasetAveragedResults\nfrom glob import glob\nimport datetime\nimport seaborn\nseaborn.set_style(\'darkgrid\')\nseaborn.set_context("notebook", rc={"lines.linewidth": 0.5})')


# ## First baseline

# In[3]:

import motor_imagery_learning.csp.train_csp
import motor_imagery_learning.csp.pipeline
motor_imagery_learning.csp.train_csp.log.setLevel('DEBUG')
motor_imagery_learning.csp.pipeline.log.setLevel('DEBUG')
from motor_imagery_learning.mywyrm.processing import bandpass_cnt, segment_dat_fast
from wyrm.processing import select_classes


# In[6]:

from motor_imagery_learning.csp.train_csp import CSPTrain
csp_debug_args = dict(load_sensor_names=['CP3', 'CP4', 'C3', 'C4'], 
         only_last_fold=True, segment_ival=[0,4000])
anwe_csp_args = dict(csp_debug_args, min_freq=10, max_freq=10, last_low_freq=10, low_width=8, high_width=8)

bhno_csp_args = dict(csp_debug_args, min_freq=11, max_freq=11, last_low_freq=11, low_width=4, high_width=4)
big_band_csp_args = dict(csp_debug_args, min_freq=16, max_freq=16, last_low_freq=16, low_width=28, high_width=28)
middle_band_csp_args = dict(csp_debug_args, min_freq=16, max_freq=16, last_low_freq=16, low_width=14, high_width=14)


anwe_train = CSPTrain('data/BBCI-without-last-runs/AnWeMoSc1S001R01_ds10_1-12.BBCI.mat', **middle_band_csp_args)
                      
         
bhno_train = CSPTrain('data/BBCI-without-last-runs/BhNoMoSc1S001R01_ds10_1-12.BBCI.mat', **middle_band_csp_args)


# In[8]:

bandpassed_cnt = bandpass_cnt(bhno_train.cnt, 9,23,3)

epo = segment_dat_fast(bandpassed_cnt, 
    marker_def={'1 - Right Hand': [1], '2 - Left Hand': [2], 
        '3 - Rest': [3], '4 - Feet': [4]}, 
    ival=[0,4000])

epo_pair = select_classes(epo, [0,3])


# In[15]:

from scipy.ndimage.filters import gaussian_filter


# In[105]:

from scipy.signal import hilbert
epo_data = epo_pair.data



epo_envelope = np.abs(hilbert(epo_data, axis=1))
epo_smoothed = gaussian_filter(epo_envelope, (0,15,0), order=0, mode='reflect')


pyplot.plot(epo_data[0,:,1])
pyplot.plot(epo_envelope[0,:,1])
pyplot.plot(epo_smoothed[0,:,1])


# In[25]:

from sklearn.metrics import roc_auc_score


# In[123]:


epo_pair = select_classes(epo, [2,3])
epo_data = epo_pair.data

epo_envelope = np.abs(hilbert(epo_data, axis=1))
epo_smoothed = gaussian_filter(epo_envelope, (0,15,0), order=0, mode='reflect')


# In[133]:

from wyrm.processing import select_ival
epo_cut = select_ival(epo_pair, [start_ms, end_ms])

epo_cut.data.shape


# In[148]:

100.0 * epo_pair.fs / 1000.0


# In[141]:

max_score_fraction = 0.80
labels = epo_pair.axes[0]
binary_labels = np.int32(labels == np.max(labels))
n_samples = len(epo_pair.axes[1])
n_chans = len(epo_pair.axes[2])
assert n_samples % 40 == 0
n_samples_per_block = n_samples // 40
n_time_blocks = n_samples // n_samples_per_block


auc_scores = np.ones((n_time_blocks, n_chans)) * np.nan

for i_time_block in range(n_time_blocks):
    for i_chan in range(n_chans):
        start_sample = i_time_block *n_samples_per_block
        epo_part = epo_smoothed[:,start_sample: start_sample + n_samples_per_block,i_chan]
        score = roc_auc_score(binary_labels, np.sum(epo_part, axis=(1)))
        auc_scores[i_time_block, i_chan] = score
        
# auc values indicate good separability if they are close to 0 or close to 1
# subtracting 0.5 transforms them to mean better separability more far away from 0
# this makes later computations easier
auc_scores = auc_scores - 0.5 

auc_score_chan = np.sum(np.abs(auc_scores), axis=1)

# sort time ivals so that best ival across chans is first
time_blocks_sorted = np.argsort(auc_score_chan)[::-1]
best_block = time_blocks_sorted[0]


chan_above_zero = auc_scores[best_block, :] > 0
chan_sign = 1 * chan_above_zero + -1 * np.logical_not(chan_above_zero)

sign_adapted_scores = auc_scores * chan_sign

chan_meaned_scores = np.sum(sign_adapted_scores, axis=1)
best_meaned_block = np.argsort(chan_meaned_scores)[::-1][0]
threshold = np.sum(chan_meaned_scores[chan_meaned_scores > 0]) * max_score_fraction


t0 = best_meaned_block
t1 = best_meaned_block
while (np.sum(chan_meaned_scores[t0:t1+1]) < threshold):
    if np.sum(chan_meaned_scores[:t0]) > np.sum(chan_meaned_scores[t1+1:]):
        t0 = t0 - 1
    else:
        t1 = t1 + 1

start_sample = t0 * n_samples_per_block
end_sample = (t1 + 1) * n_samples_per_block
start_ms = start_sample * 1000.0 / epo_pair.fs 
end_ms = end_sample * 1000.0 / epo_pair.fs
start_ms, end_ms


# In[140]:


pyplot.plot(auc_scores[:,:])
pyplot.plot(auc_score_chan)
pyplot.plot(chan_meaned_scores)


# In[ ]:

# anwe 44.87%
# bhno 61.04%


# In[7]:

anwe_train.run()
bhno_train.run()

