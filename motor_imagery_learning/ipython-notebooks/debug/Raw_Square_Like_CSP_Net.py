
# coding: utf-8

# In[7]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
#os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import numpy as np
from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import ConvElemwise
from theano.sandbox.cuda.dnn import dnn_conv
from theano.sandbox.cuda.basic_ops import gpu_alloc_empty
from motor_imagery_learning.analysis.print_results import ResultPrinter
from pylearn2.utils import serial
from motor_imagery_learning.mypylearn2.conv_squared import ConvSquaredSwitchAxes
from motor_imagery_learning.mypylearn2.conv_switch_axes import ConvSwitchAxes

import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7

from motor_imagery_learning.analysis.plot_util import (plot_misclasses_for_model, 
                                                       plot_misclasses_for_file,
                                                       plot_sensor_signals)
from motor_imagery_learning.train_scripts.train_with_params import create_training_object_from_file
from pylearn2.models.mlp import MLP, IdentityConvNonlinearity
from motor_imagery_learning.mypylearn2.train_util import ensure_model_has_input_space
from motor_imagery_learning.analysis.classifiers import  LDA
import seaborn
seaborn.set_style('darkgrid')


# # Investigate "like csp"  model

# In[17]:

model = serial.load('data/models/debug/raw-square/like-csp/early-stop/2.pkl').model


# In[19]:

_ = plot_sensor_signals(np.squeeze(model.get_weights_topo()))


# In[20]:

bps = np.mean(np.abs(np.fft.rfft(np.squeeze(model.get_weights_topo()), axis=1)), axis=0)
freq_bins = np.fft.rfftfreq(model.get_weights_topo().shape[1], d=1/150.0)

pyplot.plot(freq_bins, bps)


# ## Older stuff

# In[94]:

ResultPrinter('data/models/two-class/csp-net/early-stop/').print_results(print_templates_and_constants=False)


# In[7]:

ResultPrinter('data/models/two-class/raw-square/early-stop/').print_results(print_templates_and_constants=False, stop=2)


# In[88]:

csp_net_model = serial.load('data/models/two-class/csp-net//early-stop/1.pkl').model

layers = [ConvSwitchAxes(layer_name='bandpass', 
                         output_channels=13, 
                         irange=0.05,
                         kernel_shape=[75,1],
                         kernel_stride=[1,1],
                         pool_type=None,
                         max_kernel_norm=3.0, 
                         tied_b=True,
                         switch_axes=[['b','c',0,1],['b',1,0,'c']],
                         nonlinearity= IdentityConvNonlinearity()),
         ConvSquaredSwitchAxes(layer_name='spatial_filt', 
                         output_channels=80, 
                         irange=0.05,
                         kernel_shape=[75,1],
                         kernel_stride=[1,1], 
                         max_kernel_norm=3.0, 
                         tied_b=True,
                         switch_axes=[['b','c',0,1],['b',1,0,'c']],
                         pool_type="sumlogall",
                         pool_shape=(None,None),
                         pool_stride=(None,None)),
          Softmax(
                layer_name="y",
                n_classes=2,
                istdev=0.05,
                max_col_norm=0.5,
            ),
         ]
None



# In[89]:

mlp = MLP(layers=layers, seed= np.uint64(hash('test_train_mlp_2')))


# In[90]:

ensure_model_has_input_space(mlp, raw_trainer.dataset_splitter.dataset)


# In[91]:

inputs = T.ftensor4()
activations = mlp.fprop(inputs, return_all=True)
fprop_func = theano.function([inputs], activations)
activations = fprop_func(raw_trainer.dataset_splitter.dataset.get_topological_view().astype('float32'))


# In[62]:

print activations[0].shape
print activations[1].shape


# In[41]:

raw_trainer = create_training_object_from_file('data/models/two-class/raw-square/early-stop/1.yaml')
raw_trainer.dataset_splitter.ensure_dataset_is_loaded()


# In[92]:

clf = LDA()

clf.fit_and_score_train_test_topo(activations[1],raw_trainer.dataset_splitter.dataset.y)


# In[93]:

preds = clf.predict(activations[1][421:].reshape(len(activations[1][421:]), -1))
print np.mean(raw_trainer.dataset_splitter.dataset.y[421:, 1] == preds)
preds = clf.predict(activations[1][421:].reshape(len(activations[1][421:]), -1))
print np.mean(raw_trainer.dataset_splitter.dataset.y[421:, 1] == preds)

