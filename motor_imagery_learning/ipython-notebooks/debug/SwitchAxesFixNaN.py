
# coding: utf-8

# In[1]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
#os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import numpy as np
from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import ConvElemwise
from theano.sandbox.cuda.dnn import dnn_conv
from theano.sandbox.cuda.basic_ops import gpu_alloc_empty
from motor_imagery_learning.analysis.print_results import ResultPrinter
from motor_imagery_learning.analysis.plot_util import plot_chan_matrices
from motor_imagery_learning.mypylearn2.preprocessing import Standardize
from motor_imagery_learning.bbci_pylearn_dataset import compute_power_spectra, BBCISetNoCleaner
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset

from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from pylearn2.models.mlp import MLP, Softmax
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
from numpy.random import RandomState
import numpy as np
from pylearn2.format.target_format import OneHotFormatter
from pylearn2.termination_criteria import EpochCounter
from motor_imagery_learning.mypylearn2.dataset_splitters import (DatasetSingleFoldSplitter, 
                                                                 DatasetTwoFileSingleFoldSplitter)
from copy import deepcopy
from motor_imagery_learning.analysis.util import  get_dataset_predictions

import matplotlib
from matplotlib import pyplot
from matplotlib import cm
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (8.0, 1.0)
matplotlib.rcParams['font.size'] = 7
from motor_imagery_learning.analysis.plot_util import plot_misclasses_for_model
from motor_imagery_learning.train_scripts.train_with_params import create_training_object_from_file


# In[36]:

train = create_training_object_from_file('data/models/debug/raw-square-chan-only-no-fully-connected/test/1.yaml')


# In[35]:

# execute this OR cell below with run until early stop
train.dataset_splitter.ensure_dataset_is_loaded()
train._create_early_stop_trainer()


# In[37]:

train.run_until_earlystop()


# In[27]:

np.any(np.isnan(train.model.layers[2].get_weights_topo()))


# In[4]:


topo_view = train.early_stop_trainer.dataset.get_topological_view()

inputs = T.ftensor4()
out = train.model.fprop(inputs)

fprop_func = theano.function([inputs], out)


# In[6]:

first_layer = train.model.layers[0]

inputs = T.ftensor4()
out = first_layer.fprop(inputs)
first_layer_fprop_func = theano.function([inputs], out)


# In[7]:

second_layer = train.model.layers[1]

inputs = T.ftensor4()
out = second_layer.fprop(inputs)
second_layer_fprop_func = theano.function([inputs], out)


# In[8]:

softmax_layer = train.model.layers[2]

inputs = T.ftensor4()
out = softmax_layer.fprop(inputs)
softmax_layer_fprop_func = theano.function([inputs], out)


# In[9]:

first_out = first_layer_fprop_func(topo_view.astype('float32'))
print "nans", np.sum(np.isnan(first_out))
print "sum", np.sum(first_out)
print "mean", np.mean(first_out)


# In[10]:

second_out = second_layer_fprop_func(first_out.astype('float32'))

print "nans", np.sum(np.isnan(second_out))
print "sum", np.sum(second_out)
print "mean", np.mean(second_out)


# In[33]:

conv_squared_layer = train.model.layers[1]
self = conv_squared_layer
state_below = T.ftensor4()
self.input_space.validate(state_below)

z = self.transformer.lmul(state_below)
z_val = np.array(z.eval({state_below: first_out}))

print ("sum after conv",np.sum(z_val))

b = self.b.dimshuffle('x', 0, 'x', 'x')

z = z + b
z_val = np.array(z.eval({state_below: first_out}))
print ("sum after bias",np.sum(z_val))
d = self.nonlin.apply(z)
d_val = np.array(d.eval({state_below: first_out}))
print ("sum after nonlin",np.sum(d_val))
from motor_imagery_learning.mypylearn2.conv_squared import pool_log_sum
p = pool_log_sum(d, (50,1), (10,1), self.detector_space.shape)


p_val = np.array(p.eval({state_below: first_out}))
print ("sum after pool",np.sum(p_val))


# In[28]:

self.pool_stride


# In[31]:

np.log(np.float32(1e-8))


# In[27]:




# In[152]:

final_out = softmax_layer_fprop_func(second_out.astype('float32'))
np.sum(np.isnan(final_out))


# In[144]:

second_out.shape


# In[148]:

np.prod((30, 54, 40))


# In[151]:

np.sum(W.eval())


# In[150]:

np.array(b.eval())


# In[136]:

b, W =softmax_layer.get_params()
softmax_layer.set_weights(W.eval() * np.float32(0))


# In[109]:

np.sum(np.isnan(fprop_func(topo_view.astype('float32'))))


# In[101]:

np.sum(softmax_layer.get_weights_topo() ** 2.0, axis=(1,2,3))


# In[ ]:

softmax_layer.get_we


# In[51]:

np.prod(train.model.layers[2].get_weights_topo().shape)


# In[95]:

np.sum(train.model.layers[2].get_weights_topo())


# In[52]:

np.mean(train.model.layers[2].get_weights_topo())


# In[7]:

train.model.layers[0].get_weights_topo().shape


# In[11]:

train.model.layers[0].input_space


# In[12]:

train.model.layers[0].output_space


# In[8]:

train.model.layers[1].get_weights_topo().shape


# In[9]:

train.model.layers[1].input_space


# In[19]:

np.any(np.isnan(train.model.layers[0].get_weights_topo()))


# In[17]:

np.any(np.isnan(train.model.layers[1].get_weights_topo()))

