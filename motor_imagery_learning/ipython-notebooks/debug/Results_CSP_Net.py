
# coding: utf-8

# In[3]:

import os

import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# make sure to use cpu for printing results...
os.environ['THEANO_FLAGS'] = "floatX=float32,device=cpu,nvcc.fastmath=True"
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 2.0)
matplotlib.rcParams['font.size'] = 7
from analysis.plot_util import plot_confusion_matrix_for_averaged_result, plot_misclasses_for_file
from motor_imagery_learning.analysis.print_results import ResultPrinter
from motor_imagery_learning.csp.print_csp_results import CSPResultPrinter
from pylearn2.utils import serial
import numpy as np
from motor_imagery_learning.analysis.plot_util import (plot_nlls_for_file, 
    plot_misclasses_for_model, plot_misclasses_for_file, plot_nlls)


# In[2]:

get_ipython().run_cell_magic(u'javascript', u'', u'$(\'body\').append(\'<script src="https://dl.dropboxusercontent.com/u/34637013/work/ipython-notebooks/js-for-table-sorting/jquery.tablesorter.min.js"></script>\')\n// Sort tables every 5 seconds...\nvar makeTablesSortable;\nmakeTablesSortable = function() { \n  $(\'.text_cell_render table\').not(\'.sorted_table\').tablesorter().addClass(\'sorted_table\');\n}\n\nsetInterval(makeTablesSortable, 5000);')


# ## Open Comparisons 

# * with car (data/models/debug/csp-net/car/)
# * chan freq standardize (data/models/debug/csp-net/chan-freq-standardize/)
# * chan freq standardize and car (data/models/debug/csp-net/car/chan-freq-standardize/)
# * data/models/debug/csp-net/chan-freq-standardize-misclass-stop/
# * data/models/debug/csp-net/car/chan-freq-standardize-misclass-stop/
# * with time pool (data/models/debug/csp-net/time-pool/, data/models/debug/csp-net/time-pool-full-height/, *data/models/debug/csp-net/time-pool-full-height-no-car/* **crashed, repeat**)

# ### TODO 

# * maybe: with kernel going over full freqband to get freq+chan combinations ... probably then lower number of output chans?
# * maybe: different early stop

# ## Analysis 

# * just time pooling significantly worse 72/75% after 3/4
# * with all sensors almost exactly same performance 95.23% about 1 hour slower, 3 instead of 2 hours
# * online standardize with adam keep params achieves best performance 95.15%
# * higher frequencies very important for some
# * adam almost exactly same accuracy(+0.02%)
# * running min sometimes does not stop

# ## Results 

# In[21]:

from motor_imagery_learning.analysis.results import ResultPool


# In[38]:

ResultPrinter('data/models/final-eval/small/csp-net/early-stop/').print_results(print_individual_datasets=False)


# In[4]:

ResultPrinter('data/models/final-eval/csp-net/early-stop/').print_results(print_individual_datasets=False)


# In[6]:

ResultPrinter('data/models/debug/csp-net-online-standardize-keep-lrule/early-stop//').print_results()


# In[93]:

ResultPrinter('data/models/debug/csp-net/chan-freq-standardize-misclass-stop//early-stop/').print_results()


# In[3]:

ResultPrinter('data/models/debug/csp-net/chan-freq-standardize/early-stop/').print_results()


# In[4]:

ResultPrinter('data/models/debug/csp-net/car/chan-freq-standardize-misclass-stop/early-stop/').print_results()


# In[4]:

ResultPrinter('data/models/two-files-split/csp-net/all_EEG_sensors//early-stop/').print_results()


# In[8]:

ResultPrinter('data/models/debug/csp-net/car/early-stop//').print_results()


# In[4]:

ResultPrinter('data/models/full/csp-net-best/early-stop/').print_results(
    print_templates_and_constants=False)


# In[11]:

ResultPrinter('data/models/debug/csp-net-online-standardize-keep-lrule//early-stop/').print_results(
    print_templates_and_constants=False)


# In[13]:

ResultPrinter('data/models/full/csp-net/early-stop/').print_results(print_templates_and_constants=False)


# In[12]:

ResultPrinter('data/models/full/csp-net-best/early-stop/').print_results(print_templates_and_constants=False)

