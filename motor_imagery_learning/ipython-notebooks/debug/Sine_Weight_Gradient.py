
# coding: utf-8

# In[29]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
#os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import numpy as np
from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import ConvElemwise
from theano.sandbox.cuda.dnn import dnn_conv
from theano.sandbox.cuda.basic_ops import gpu_alloc_empty
from motor_imagery_learning.analysis.print_results import ResultPrinter
from pylearn2.utils import serial

import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (8.0, 1.0)
matplotlib.rcParams['font.size'] = 7

from motor_imagery_learning.analysis.plot_util import plot_misclasses_for_model, plot_nlls_for_model, plot_sensor_signals
import scipy
import seaborn
seaborn.set_style('darkgrid')
from copy import deepcopy
from motor_imagery_learning.train_scripts.train_experiments import ExperimentsRunner
from motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer
from motor_imagery_learning.analysis.data_generation import (create_sine_signal,
    pipeline, create_shifted_sines,put_noise, convolve_with_weight,
    max_pool_topo_view, log_sum_pool,generate_single_sensor_data, 
    add_timecourse, spread_to_sensors, SpreadedSinesDataset, randomly_shifted_sines)
from motor_imagery_learning.analysis.util import create_trainer
from motor_imagery_learning.mypylearn2.conv_squared import ConvSquared
from motor_imagery_learning.analysis.input_reconstruction import (
    create_rand_input, gradient_descent_input, create_input_grad_for_wanted_class_func,
    create_input_grad_with_smooth_penalty_for_wanted_class_func, reconstruct_inputs_for_class,
    power_smoothness_cost_moving_avg_scaled)
from pylearn2.config import yaml_parse
from  motor_imagery_learning.train_scripts.train_with_params import create_training_object_from_file
from theano.tensor.nnet.conv import conv2d
from pylearn2.models.mlp import dnn_pool


# ## Simpler grad without relu etc

# In[107]:

def make_batch_grad_and_out_func(model_func):
    inputs = T.fmatrix()
    weights = T.fvector()

    out = model_func(inputs, weights)
    batch_grad = T.grad(out, weights)
    batch_grad_and_out_func = theano.function([inputs, weights], [batch_grad, out])
    return batch_grad_and_out_func

def conv_squared_sum(inputs, weights):
    conved_output = conv2d(inputs.dimshuffle(0, 'x',1,'x'), weights.dimshuffle('x', 'x', 0, 'x'))
    squared_sum = T.mean(T.sum(T.sqr(conved_output), axis=(1,2,3)))
    return squared_sum

def conv_abs_sum(inputs, weights):
    conved_output = conv2d(inputs.dimshuffle(0, 'x',1,'x'), weights.dimshuffle('x', 'x', 0, 'x'))
    abs_sum = T.mean(T.sum(T.abs_(conved_output), axis=(1,2,3)))
    return abs_sum

def conv_sum(inputs, weights):
    conved_output = conv2d(inputs.dimshuffle(0, 'x',1,'x'), weights.dimshuffle('x', 'x', 0, 'x'))
    mean_sum = T.mean(T.sum(conved_output, axis=(1,2,3)))
    return mean_sum

def conv_relu_sum(inputs, weights):
    conved_output = conv2d(inputs.dimshuffle(0, 'x',1,'x'), weights.dimshuffle('x', 'x', 0, 'x'))
    abs_sum = T.mean(T.sum(conved_output * conved_output > 0, axis=(1,2,3)))
    return abs_sum

def conv_maxpool_abs_sum(inputs, weights):
    conved_output = conv2d(inputs.dimshuffle(0, 'x',1,'x'), weights.dimshuffle('x', 'x', 0, 'x'))
    pooled_output = dnn_pool(conved_output, ws=(8,1), stride=(1,1), mode='max')
    abs_sum = T.mean(T.sum(T.abs_(pooled_output), axis=(1,2,3)))
    return abs_sum

def conv_maxpool_squared_sum(inputs, weights):
    conved_output = conv2d(inputs.dimshuffle(0, 'x',1,'x'), weights.dimshuffle('x', 'x', 0, 'x'))
    pooled_output = dnn_pool(conved_output, ws=(8,1), stride=(1,1), mode='max')
    squared_sum = T.mean(T.sum(T.sqr(pooled_output), axis=(1,2,3)))
    return squared_sum

def conv_squared_sum_decay(inputs, weights):
    squared_sum = conv_squared_sum(inputs, weights)
    return squared_sum - T.sum(T.sqr(weights))

def nonorm(weight):
    return 1

def l1norm(weight):
    return np.sum(np.abs(weight))

def l2norm(weight):
    return np.sqrt(np.sum(np.square(weight)))
    
def eval_weight(batch_grad_and_out_func, signals, test_weight, reference_weight, lrate=0.1, epochs=50, norm=nonorm):
    weights, grads, wanted_sums = gradient_ascent(batch_grad_and_out_func, signals, 
        test_weight, lrate=lrate, epochs=epochs, norm=norm)
    reference_weight = reference_weight / norm(reference_weight)
    best_sum = batch_grad_and_out_func(signals, reference_weight[::-1])[1]
    return weights, grads, wanted_sums, best_sum
    

def plot_ascent_result(weights, grads, wanted_sums, best_sum):
    pyplot.figure()
    pyplot.plot(weights[-1])
    pyplot.figure()
    pyplot.plot(wanted_sums)
    pyplot.axhline(y=best_sum, color='black', linestyle='--')
    
def eval_and_plot(batch_grad_and_sqsum_func, 
        signals, weight, reference_weight, lrate=0.1, epochs=50):
    weights, grads, wanted_sums, best_sum = eval_weight(batch_grad_and_sqsum_func, 
            signals, weight, reference_weight, lrate=0.1, epochs=50)
    plot_ascent_result(weights, grads, wanted_sums, best_sum)
    return weights, grads, wanted_sums, best_sum

def generate_data(noise_factor, seed=np.uint64(hash('rand_data')), num_periods=3):
    signals = randomly_shifted_sines(2000, samples=np.round(num_periods * 150.0/11.0), 
                                     freq=11,sampling_freq=150.0).astype(np.float32)
    noise = RandomState(seed).rand(*signals.shape).astype(np.float32)
    noise = 2 * (noise - 0.5) # change interval from  [0,1] to [-1,1]
    noisy_signals = (signals + noise_factor * noise) / (noise_factor + 1)
    return noisy_signals
    
def generate_weights(norm, seed=np.uint64(hash('rand_weights'))):
    sine_weight = create_sine_signal(samples=15, freq=11, sampling_freq=150).astype(np.float32)
    sine_weight = sine_weight / norm(sine_weight)
    rand_weight = 2 * (RandomState(seed).rand(15).astype(np.float32) - 0.5)
    rand_weight = rand_weight / norm(rand_weight)
    return sine_weight, rand_weight
def gradient_ascent(batch_grad_and_sum_func, signals, cur_weight, lrate=0.1, epochs=50, norm=nonorm):
    all_grads = []
    cur_weight = cur_weight / norm(cur_weight) # clip to norm
    all_weights = [cur_weight]
    all_wanted_sums = []

    for _ in xrange(epochs):
        grad, wanted_sum = batch_grad_and_sum_func(signals, cur_weight[::-1]) # ::-1 to make cross correlation
        cur_weight = cur_weight + lrate * grad
        cur_weight = cur_weight / norm(cur_weight) # clip to norm
        all_grads.append(grad)
        all_weights.append(cur_weight)
        all_wanted_sums.append(wanted_sum)
        
    return np.array(all_weights), np.array(all_grads), np.array(all_wanted_sums)

def run_ascent_and_plot(model_func, data_params, weight_seed, ascent_args):
    
    batch_grad_and_out_func = make_batch_grad_and_out_func(model_func)


    noisy_signals = generate_data(**data_params)
    sine_weight, rand_weight = generate_weights(ascent_args['norm'], seed=weight_seed)
    weights_rand, grads_rand, wanted_sums_rand, best_sum = eval_weight(batch_grad_and_out_func, 
            noisy_signals, rand_weight, reference_weight=sine_weight, **ascent_args)
    #plot_ascent_result(weights, grads_rand, wanted_sums, best_sum)
    weights_sine, grads_sine, wanted_sums_sine, best_sum = eval_weight(batch_grad_and_out_func, 
            noisy_signals, sine_weight, reference_weight=sine_weight, **ascent_args)

    pyplot.figure()
    pyplot.plot(wanted_sums_rand)
    pyplot.plot(wanted_sums_sine, label="sine_weight")
    pyplot.axhline(y=best_sum, color='black', linestyle='--')
    pyplot.legend()
    pyplot.figure()
    pyplot.title("rand weight ")
    pyplot.plot(weights_rand[-1])
    pyplot.figure()
    pyplot.title("sine weight")
    pyplot.plot(weights_sine[-1])


# In[20]:

def merge_dicts(*dict_args):
    '''
    Given any number of dicts, shallow copy and merge into a new dict,
    precedence goes to key value pairs in latter dicts.
    '''
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result


# ## Gradients fine for low noise
# 
# conv squared seems fine for low noise

# In[30]:

data_params = dict(noise_factor=0.1)
ascent_args = dict(lrate=0.01, epochs=1000, norm=l2norm)
model_func = conv_squared_sum
run_ascent_and_plot(model_func, data_params, ascent_args)


# In[44]:

data_params = dict(noise_factor=100, seed=np.uint64(hash('rand_data')))
weight_seed= np.uint64(hash('rand_weight'))
ascent_args = dict(lrate=0.01, epochs=1000, norm=l2norm)
model_func = conv_squared_sum
run_ascent_and_plot(model_func, data_params, weight_seed, ascent_args)


# In[60]:

data_params = dict(noise_factor=100, seed=np.uint64(hash('rand_data')))
weight_seed= np.uint64(hash('rand_weight_2'))
ascent_args = dict(lrate=0.01, epochs=5000, norm=l2norm)
model_func = conv_squared_sum
run_ascent_and_plot(model_func, data_params, weight_seed, ascent_args)


# # Development

# Find critical noise value

# In[112]:

data_params = dict(noise_factor=20, seed=np.uint64(hash('rand_data'), num_periods=10))
weight_seed= np.uint64(hash('rand_weights'))
ascent_args = dict(lrate=0.01, epochs=2000, norm=l2norm)
model_func = conv_squared_sum
run_ascent_and_plot(model_func, data_params, weight_seed, ascent_args)


# In[63]:

data_params = dict(noise_factor=20, seed=np.uint64(hash('rand_data')))
weight_seed= np.uint64(hash('rand_weight'))
ascent_args = dict(lrate=0.01, epochs=2000, norm=l2norm)
model_func = conv_maxpool_squared_sum
run_ascent_and_plot(model_func, data_params, weight_seed, ascent_args)


# In[67]:

data_params = dict(noise_factor=20, seed=np.uint64(hash('rand_datas')))
weight_seed= np.uint64(hash('rand_weight'))
ascent_args = dict(lrate=0.01, epochs=2000, norm=l2norm)
model_func = conv_maxpool_squared_sum
run_ascent_and_plot(model_func, data_params, weight_seed, ascent_args)


# In[ ]:

pyplot.plot(np.mean(np.abs(np.fft.rfft(noisy_signals, axis=1)), axis=0))


# In[498]:

ascent_args = dict(lrate=0.01, epochs=1000, norm=l2norm)

batch_grad_and_sqsum_func = make_batch_grad_and_out_func(conv_maxpool_sum)
short_sine = create_sine_signal(samples=150, freq=11, sampling_freq=150).astype(np.float32)
noise_factor = 5

rand_weight = 2 * (RandomState(np.uint64(hash('rand_weights'))).rand(15).astype(np.float32) - 0.5)
sine_weight = create_sine_signal(samples=15, freq=11, sampling_freq=150).astype(np.float32)
sine_weight = sine_weight / ascent_args['norm'](sine_weight)

weights, grads_rand, wanted_sums, best_sum = eval_weight(batch_grad_and_sqsum_func, 
        noisy_signals, rand_weight, sine_weight, **ascent_args)
plot_ascent_result(weights, grads_rand, wanted_sums, best_sum)
weights, grads, wanted_sums, best_sum = eval_weight(batch_grad_and_sqsum_func, 
        noisy_signals, sine_weight, sine_weight, **ascent_args)
pyplot.plot(wanted_sums, label="sine_weight")
pyplot.legend()
pyplot.figure()
pyplot.plot(weights[-1])


# In[ ]:

pyplot


# In[439]:




# # More complicated stuff ;)

# In[156]:

inputs = T.fmatrix()
weights = T.fvector()
conved_output = conv2d(inputs.dimshuffle(0, 'x',1,'x'), weights.dimshuffle('x', 'x', 0, 'x'))
logsum = T.log(T.maximum(1e-6,T.sum(T.sqr(conved_output * (conved_output > 0)))))

cost = 1.0/logsum + T.sum(T.sqr(weights))

batch_grad = T.grad(cost, weights)

batch_grad_func = theano.function([inputs, weights], batch_grad)
batch_grad_cost_func = theano.function([inputs, weights], [batch_grad, cost])


# In[139]:

inputs = T.fvector()
weights = T.fvector()
conved_output = conv2d(inputs.dimshuffle('x', 'x',0,'x'), weights.dimshuffle('x', 'x', 0, 'x'))
logsum = T.log(T.sum(T.sqr(conved_output * (conved_output > 0))))

cost = 1.0/logsum + T.sum(T.sqr(weights))


grad_simple_model = T.grad(cost, weights)

grad_simple_func = theano.function([inputs, weights], grad_simple_model)
grad_cost_simple_func = theano.function([inputs, weights], [grad_simple_model, cost])


# In[12]:

rng = RandomState(np.uint64(hash('simplegrad')))
weight = rng.rand(150)
#weight = weight - np.mean(weight)
weight_2 = rng.rand(150).astype(np.float32)
weight_3 = rng.rand(150).astype(np.float32)
sine_signal = create_sine_signal(samples=600,freq=11,sampling_freq=150.0, shift=0)
sine_signal_2 = create_sine_signal(samples=600,freq=11,sampling_freq=150.0, shift=1)
simple_grad = grad_simple_func(sine_signal.astype(np.float32), weight.astype(np.float32))
simple_grad_2 = grad_simple_func(sine_signal_2.astype(np.float32), weight.astype(np.float32))
simple_grad_weight_2 = grad_simple_func(sine_signal.astype(np.float32), weight_2.astype(np.float32))
simple_grad_weight_3 = grad_simple_func(sine_signal.astype(np.float32), weight_3.astype(np.float32))
#pyplot.plot(simple_grad)
#pyplot.plot(simple_grad_2)
#pyplot.plot(weight * np.mean(np.abs(simple_grad_2)) / np.mean(np.abs(weight)))
pyplot.plot(simple_grad_weight_2)
pyplot.plot(simple_grad_weight_3)
#pyplot.plot((weight_2 - np.mean(weight_2)) / 20)
pyplot.plot((weight_3 - np.mean(weight_3)) / 20)


# In[27]:

fake_signals = np.float32([create_sine_signal(samples=600,freq=11,sampling_freq=150.0, shift=s) for s in 
                           [1, 0.5, 0,-1,-0.25,+0.25,-0.33,+0.33, -2,+2,-3,-3]])



#fake_signals = randomly_shifted_sines(500, samples=600, freq=11,sampling_freq=150.0)

individual_grads = np.arrrbay([grad_simple_func(fake_signal, weight_3) for fake_signal in fake_signals])
mean_grad = np.mean(individual_grads, axis=0)
pyplot.plot(mean_grad)
pyplot.plot(individual_grads[1], '--')


# In[59]:

def compute_grads(grad_func, signals, weight):
    individual_grads = np.array([grad_func(signal, weight) for signal in signals])
    return individual_grads


# In[114]:

fake_signals = randomly_shifted_sines(200, samples=600, freq=11,sampling_freq=150.0).astype(np.float32)
rng = RandomState(np.uint64(hash('signals_with_noise')))
signal_factor = 0.01
demeaned_weight = weight_3 - np.mean(weight_3)
signals_with_noise =  signal_factor * fake_signals + rng.randn(*fake_signals.shape).astype(np.float32)
individual_grads = compute_grads(grad_simple_func, signals_with_noise, demeaned_weight)
mean_grad = np.mean(individual_grads, axis=0)
pyplot.plot(mean_grad)
pyplot.plot(demeaned_weight  * np.mean(np.abs(mean_grad)))


# In[151]:

np.mean(np.abs(sine_weight))


# In[158]:

sine_weight = create_sine_signal(samples=150,freq=11,sampling_freq=150.0).astype(np.float32)
individual_grads = compute_grads(grad_simple_func, signals_with_noise, cur_weight)
mean_grad = np.mean(individual_grads, axis=0)
batch_grad, batch_cost = batch_grad_cost_func(signals_with_noise, cur_weight)
batch_grad, batch_cost = batch_grad_cost_func(signals_with_noise, cur_weight)
pyplot.plot(mean_grad)
pyplot.plot(batch_grad)
batch_grad_sine, batch_cost_sine = batch_grad_cost_func(signals_with_noise, sine_weight*0.1)
print batch_cost
print batch_cost_sine


# ## Gradient descent stuffs

# In[143]:

cur_weight = demeaned_weight
all_weights = [cur_weight]
all_grads = []
lrate = 1
for _ in range(50):
    individual_grads = compute_grads(grad_simple_func, signals_with_noise, cur_weight)
    mean_grad = np.mean(individual_grads, axis=0)
    mean_grad = mean_grad #/ np.mean(mean_grad) # hackyhacky dont allow gradient to go down :)
    all_grads.append(mean_grad)
    cur_weight = cur_weight - lrate * mean_grad
    all_weights.append(cur_weight)


# In[144]:

mean_all_grads = np.mean(all_grads, axis=0)
pyplot.plot(mean_all_grads)


# In[145]:

_ = pyplot.plot(np.array(all_grads[0::10]).T)


# In[146]:

#('grad size', 0.00059942057)
pyplot.plot(cur_weight)
pyplot.plot(mean_grad * np.mean(np.abs(cur_weight)) / np.mean(np.abs(mean_grad)), '--')
print ("grad size", np.mean(np.abs(mean_grad)))

