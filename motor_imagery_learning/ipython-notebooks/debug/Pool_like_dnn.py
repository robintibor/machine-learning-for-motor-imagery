
# coding: utf-8

# In[1]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
#os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import numpy as np
from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import ConvElemwise
from theano.sandbox.cuda.dnn import dnn_conv
from theano.sandbox.cuda.basic_ops import gpu_alloc_empty
from motor_imagery_learning.analysis.print_results import ResultPrinter
from motor_imagery_learning.analysis.plot_util import plot_chan_matrices
from motor_imagery_learning.mypylearn2.preprocessing import Standardize
from motor_imagery_learning.bbci_pylearn_dataset import compute_power_spectra, BBCISetNoCleaner
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFilterbankDataset

from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from pylearn2.models.mlp import MLP, Softmax
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
from numpy.random import RandomState
import numpy as np
from pylearn2.format.target_format import OneHotFormatter
from pylearn2.termination_criteria import EpochCounter
from motor_imagery_learning.mypylearn2.dataset_splitters import DatasetSingleFoldSplitter, DatasetTwoFileSingleFoldSplitter
from copy import deepcopy
from theano.sandbox.cuda.dnn import dnn_pool
import theano.tensor as T

import matplotlib
from matplotlib import pyplot
from matplotlib import cm
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (8.0, 1.0)
matplotlib.rcParams['font.size'] = 7
from motor_imagery_learning.analysis.plot_util import plot_misclasses_for_model


# In[120]:

input_shape = (3,2,5,7)
pool_shape = (2,3)
pool_stride = (2,3)


# In[121]:

input_shape = (3,2,586,1)
pool_shape = (50,1)
pool_stride = (10,1)


# In[122]:

rng = RandomState(np.uint64(hash('pooltests')))
inputs = rng.randn(*input_shape).astype(np.float32)


# In[123]:

theano_in = T.ftensor4()
theano_res = dnn_pool(theano_in, ws=pool_shape, mode="average_exc_pad",stride=pool_stride)
dnn_pool_func = theano.function([theano_in], theano_res)


# In[124]:

from motor_imagery_learning.mypylearn2.pool import mean_pool
theano_in = T.ftensor4()
theano_res = mean_pool(theano_in, pool_shape=pool_shape, image_shape=input_shape[2:],pool_stride=pool_stride)
pylearn_pool_func = theano.function([theano_in], theano_res)


# In[125]:

theano_in = T.ftensor4()
theano_res = dnn_pool(theano_in, ws=pool_shape, mode="max",stride=pool_stride)
dnn_max_pool_func = theano.function([theano_in], theano_res)


# In[126]:

from motor_imagery_learning.mypylearn2.pool import max_pool
theano_in = T.ftensor4()
theano_res = max_pool(theano_in, pool_shape=pool_shape, image_shape=input_shape[2:], pool_stride=pool_stride,
                      try_dnn=False)
pylearn_max_pool_func = theano.function([theano_in], theano_res)


# In[127]:

dnn_result = dnn_pool_func(inputs)
pylearn_result = pylearn_pool_func(inputs)
np.allclose(dnn_result, pylearn_result)


# In[128]:

dnn_result = dnn_max_pool_func(inputs)
pylearn_result = pylearn_max_pool_func(inputs)
np.allclose(dnn_result, pylearn_result)

