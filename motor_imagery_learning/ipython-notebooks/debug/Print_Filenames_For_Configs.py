
# coding: utf-8

# ## Print filenames for yaml configs

# In[3]:

cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/


# In[8]:

from glob import glob

filenames = glob('data/BBCI-all-runs/*')

filenames = sorted(filenames)
filenames



# In[ ]:



