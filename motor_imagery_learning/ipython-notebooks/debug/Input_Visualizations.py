
# coding: utf-8

# In[680]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
#os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import numpy as np
from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import ConvElemwise
from theano.sandbox.cuda.dnn import dnn_conv
from theano.sandbox.cuda.basic_ops import gpu_alloc_empty
from motor_imagery_learning.analysis.print_results import ResultPrinter
from pylearn2.utils import serial

import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (8.0, 2.0)
matplotlib.rcParams['font.size'] = 7

from motor_imagery_learning.analysis.plot_util import plot_misclasses_for_model, plot_nlls_for_model, plot_sensor_signals
import scipy
import seaborn
seaborn.set_style('darkgrid')
from copy import deepcopy
from motor_imagery_learning.train_scripts.train_experiments import ExperimentsRunner
from motor_imagery_learning.analysis.classifiers import LDA, OneFeatureScorer
from motor_imagery_learning.analysis.data_generation import (create_sine_signal,
    SingleChanDataset, pipeline, create_shifted_sines,put_noise, convolve_with_weight,
    max_pool_topo_view, log_sum_pool,generate_single_sensor_data, 
    add_timecourse)
from motor_imagery_learning.analysis.util import create_trainer
from motor_imagery_learning.mypylearn2.conv_squared import ConvSquared
from motor_imagery_learning.analysis.input_reconstruction import (
    create_rand_input, gradient_descent_input, create_input_grad_for_wanted_class_func,
    create_input_grad_with_smooth_penalty_for_wanted_class_func, reconstruct_inputs_for_class)
from pylearn2.config import yaml_parse


# ## Input visualization time switches + power smoothness

# In[681]:

clf = LDA()
input_shape=(600,1,600,1)
start_topo_view, y = create_shifted_sines(input_shape, RandomState(np.uint64(hash("pooled_pipeline"))), 
                                          sampling_freq=150.0)
topo_view_noised, y = pipeline(start_topo_view, y,
                         lambda x,y: add_timecourse(x,y),     
                         lambda x,y: put_noise(x, weight_old_data=0.05))

train_acc, test_acc = clf.fit_and_score_train_test_topo(topo_view_noised, y)
print("train/test acc", train_acc, test_acc)
# convolve + square + maxpool
layers = [ConvSquared(output_channels=1, kernel_shape=[150,1], pool_shape=[50,1], pool_stride=[50,1],
                     pool_type="sumlog",layer_name="conv_squared", irange=0.05, max_kernel_norm=2.0, tied_b=True),
          Softmax(n_classes=2, layer_name='y', irange=0.001, max_col_norm=0.5)]
trainer = create_trainer(topo_view_noised, y,max_epochs=200, layers=layers, cost=None)


# In[682]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer.run_until_earlystop()')


# In[683]:

plot_misclasses_for_model(trainer.model)


# In[919]:

orig_weights = deepcopy(trainer.model.get_weights_topo()).swapaxes(1,2)


# In[929]:

trainer.model.layers[0].set_weights(orig_weights)


# In[931]:

pyplot.plot(trainer.model.get_weights_topo().squeeze())


# In[930]:

grad_args = dict(epochs=1000, lrate=3)
inputs_class_2 = reconstruct_inputs_for_class(trainer.model, [0,1], **grad_args)
pyplot.plot(np.squeeze(inputs_class_2[-1,0]))


# In[928]:

trainer.model.layers


# In[783]:

input_to_test = np.squeeze(inputs_class_2[-1,0])
inputs_square = np.square(input_to_test)
pyplot.plot(inputs_square)

avg_square = np.convolve(inputs_square, np.ones(200) / 200, mode='valid')
pyplot.plot(avg_square)


# In[801]:

pyplot.plot(np.convolve(inputs_square, np.ones(200) / 200, mode='valid')[::50])


# In[819]:

pyplot.plot(-env + np.mean(env) + 1)
pyplot.plot(env * (-env + np.mean(env) + 1))


# In[822]:

changed_input =input_to_test * np.mean(env)/env
env = np.abs(scipy.signal.hilbert(input_to_test))
pyplot.plot(changed_input)


# In[883]:

inputs = T.ftensor4()
block_mov_avg = power_block_avg(inputs)
cost_movavg = power_smoothness_cost_moving_avg(inputs)
cost_movavg_scaled = power_smoothness_cost_moving_avg_scaled(inputs)

cost_func_movavg = theano.function([inputs], cost_movavg)
cost_func_mov_avg_scaled = theano.function([inputs], cost_movavg_scaled)
block_mov_avg_func = theano.function([inputs], block_mov_avg)


# In[884]:

print cost_func_mov_avg_scaled(input_to_test[np.newaxis, np.newaxis, :,np.newaxis].astype(np.float32))
print cost_func_mov_avg_scaled(changed_input[np.newaxis, np.newaxis, :,np.newaxis].astype(np.float32))


# In[871]:

pyplot.plot(np.squeeze(block_mov_avg_func(input_to_test[np.newaxis, np.newaxis,:,np.newaxis])))


# In[893]:

grad_args = dict(epochs=2000, lrate=0.1, input_cost_func=power_smoothness_cost_moving_avg_scaled)
inputs_class_2 = reconstruct_inputs_for_class(trainer.model, [0,1], **grad_args)
pyplot.plot(np.squeeze(inputs_class_2[-1,0]))


# In[709]:




# In[892]:

def power_smoothness_cost(inputs):
    inputs_squared = T.sqr(inputs)
    return T.sum(T.sqr(inputs_squared[:,:,1:,:] - inputs_squared[:,:,:-1,:])) 

def power_smoothness_cost_scaled(inputs):
    inputs_squared = T.sqr(inputs)
    return T.sum(T.sqr(inputs_squared[:,:,1:,:] - inputs_squared[:,:,:-1,:])) / T.sum(T.sqr(inputs_squared[:,:,1:-1,:]))


def power_smoothness_cost_moving_avg(inputs, avg_num=200, subsample=50):
    average = power_mov_avg(inputs, avg_num=avg_num, subsample=subsample)
    return subsample * T.sum(T.sqr(average[:,:,1:,:] - average[:,:,:-1,:]))# * 0.01

def power_mov_avg(inputs, avg_num=200, subsample=50):
    inputs_squared = T.sqr(inputs)
    filters = T.constant(np.ones(avg_num) / avg_num, dtype='floatX').dimshuffle('x', 'x', 0, 'x')
    average = conv2d(inputs_squared, filters, filter_stride=(1,1))[:,:,::subsample,:]
    return average

def power_block_avg(inputs, avg_num=50):
    inputs_squared = T.sqr(inputs)
    # expect last dimension to be empty anyways..
    inputs_cut = inputs_squared[:,:,:inputs_squared.shape[2] - (inputs_squared.shape[2] % avg_num),:]
    inputs_reshaped = inputs_cut.reshape((inputs_cut.shape[0], inputs_cut.shape[1], avg_num, -1))
    average = T.mean(inputs_reshaped, axis=3, keepdims=True)
    return average
    
def power_smoothness_cost_moving_avg_scaled(inputs, avg_num=200, subsample=50):
    average = power_block_avg(inputs, avg_num=subsample)#, subsample=subsample)
    return T.sum(T.sqr(average[:,:,1:,:] - average[:,:,:-1,:])) / T.sum(T.sqr(average))#[:,:,1:-1,:])


# In[839]:

inputs_square.shape
inputs_square[:len(inputs_square) - (len(inputs_square) % 50)].shape


# In[847]:

inputs_reshaped = np.reshape(inputs_square[:len(inputs_square) - (len(inputs_square) % 50)], (50, -1))

pyplot.plot(np.mean(inputs_reshaped, axis=1, keepdims=True))


# ## Input visualizations two class

# In[621]:

ResultPrinter('data/models/two-class/raw-square/early-stop/').print_results()


# In[898]:

model = serial.load('data/models/two-class/raw-square/early-stop/3.pkl').model


# In[897]:

grad_args = dict(epochs=1000, lrate=3, input_cost_func=power_smoothness_cost_moving_avg_scaled)
grad_args_adam = dict(epochs=1000, lrate=3, learning_rule=AdamNumpy(alpha=1., beta2=0.001, beta1=0.1))


# In[899]:

inputs_class_2 = reconstruct_inputs_for_class(model, [0,1], **grad_args)
_ = plot_sensor_signals(np.squeeze(inputs_class_2[-1,0]), get_C_sensors_sorted(), yticks="onlymax").suptitle(
    'Rest', y=0.93, fontsize=20)


# In[900]:

inputs_class_1 = reconstruct_inputs_for_class(model, [1,0], **grad_args)
_ = plot_sensor_signals(np.squeeze(inputs_class_1[-1,0]), get_C_sensors_sorted(), yticks="onlymax").suptitle(
    'Right Hand', y=0.93, fontsize=20)


# ## Input visualizations full model

# In[903]:

# 19 gujo or 23 kaus are possible

model = serial.load('data/models/debug/raw-square-smaller/early-stop/23.pkl').model


# In[902]:

grad_args = dict(epochs=1000, lrate=3, input_cost_func=power_smoothness_cost_moving_avg_scaled)


# In[904]:

inputs_class_4 = reconstruct_inputs_for_class(model, [0,0,0,1], **grad_args)
_ = plot_sensor_signals(np.squeeze(inputs_class_4[-1,1]), get_C_sensors_sorted(), yticks="onlymax").suptitle(
    'Feet', y=0.93, fontsize=20)


# In[698]:

bps = np.abs(np.fft.rfft(inputs_class_3[-1,0], axis=1)).mean(axis=(0,2))

freq_bins = np.fft.rfftfreq(inputs_class_3[-1,0].shape[1], d=1/150.0)

pyplot.plot(freq_bins, bps)


# In[905]:

inputs_class_3 = reconstruct_inputs_for_class(model, [0,0,1,0], **grad_args)
_ = plot_sensor_signals(np.squeeze(inputs_class_3[-1,0]), get_C_sensors_sorted(), yticks="onlymax").suptitle(
    'Rest', y=0.93, fontsize=20)


# In[906]:

inputs_class_2 = reconstruct_inputs_for_class(model, [0,1,0,0], **grad_args)
_ = plot_sensor_signals(np.squeeze(inputs_class_2[-1,0]), get_C_sensors_sorted(), yticks="onlymax").suptitle(
    'Left Hand', y=0.93, fontsize=20)


# In[907]:

inputs_class_1 = reconstruct_inputs_for_class(model, [1,0,0,0], **grad_args)
_ = plot_sensor_signals(np.squeeze(inputs_class_1[-1,0]), get_C_sensors_sorted(), yticks="onlymax").suptitle(
    'Right Hand', y=0.93, fontsize=20)


# # Input visualizations kauswe singlechan data

# In[446]:

kaus_topo, kaus_y = generate_single_sensor_data()


# In[586]:

# convolve + square + maxpool
layers = [ConvSquared(output_channels=1, kernel_shape=[150,1], pool_shape=[50,1], pool_stride=[50,1],
                     pool_type="sumlog",layer_name="conv_squared", irange=0.05, max_kernel_norm=2.0, tied_b=True),
          Softmax(n_classes=2, layer_name='y', irange=0.001, max_col_norm=0.5)]
trainer = create_trainer(kaus_topo, kaus_y,max_epochs=200, layers=layers, cost=None)


# In[587]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer.run_until_earlystop()')


# In[588]:

_ = plot_misclasses_for_model(trainer.model)


# In[589]:

_ = pyplot.plot(np.squeeze(trainer.model.get_weights_topo()))


# In[590]:

in_grad_func = create_input_grad_for_wanted_class_func(trainer.model)
batches = 3
start_in = create_rand_input(trainer.model, batches)
ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000, learning_rule=AdamNumpy(alpha=0.1))
inputs_class_1 = gradient_descent_input(start_in, in_grad_func, [1,0], **ascent_args)
inputs_class_2 = gradient_descent_input(start_in, in_grad_func, [0,1], **ascent_args)


# In[591]:

#pyplot.plot(np.squeeze(inputs_class_1[::100])[:,2].T)
#pyplot.figure()
pyplot.plot(np.squeeze(inputs_class_1[-1,2]).T)


# In[592]:

#pyplot.plot(np.squeeze(inputs_class_2[0::100])[:,1].T)
#pyplot.figure()
pyplot.plot(np.squeeze(inputs_class_2[-1,2]).T)


# # Input visualizations artificial data

# Know how to find out how to make programs become more reliable for users who depend on BCI
# Method for visualization, results checked for pool all and pool part nets for single chan

# In[546]:

clf = LDA()
input_shape=(600,1,600,1)
start_topo_view, y = create_shifted_sines(input_shape, RandomState(np.uint64(hash("pooled_pipeline"))), 
                                          sampling_freq=150.0)
topo_view_noised, y = pipeline(start_topo_view, y,
                         lambda x,y: put_noise(x, weight_old_data=0.05))

train_acc, test_acc = clf.fit_and_score_train_test_topo(topo_view_noised, y)
print("train/test acc", train_acc, test_acc)
# convolve + square + maxpool
layers = [ConvSquared(output_channels=1, kernel_shape=[150,1], pool_shape=[50,1], pool_stride=[50,1],
                     pool_type="sumlog",layer_name="conv_squared", irange=0.05, max_kernel_norm=2.0, tied_b=True),
          Softmax(n_classes=2, layer_name='y', irange=0.001, max_col_norm=0.5)]
trainer = create_trainer(topo_view_noised, y,max_epochs=200, layers=layers, cost=None)


# In[548]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer.run_until_earlystop()')


# In[549]:

_ = plot_misclasses_for_model(trainer.model)


# In[550]:

_ = pyplot.plot(np.squeeze(trainer.model.get_weights_topo()))


# In[556]:

from motor_imagery_learning.analysis.input_reconstruction import AdamNumpy


# In[583]:

in_grad_func = create_input_grad_for_wanted_class_func(trainer.model)
batches = 3
start_in = create_rand_input(trainer.model, batches)
ascent_args = dict(lrate=0.1, decay=0.99, epochs=1000, learning_rule=AdamNumpy(alpha=0.1))
inputs_class_1 = gradient_descent_input(start_in, in_grad_func, [1,0], **ascent_args)
inputs_class_2 = gradient_descent_input(start_in, in_grad_func, [0,1], **ascent_args)

#in_grad_func_smooth = create_input_grad_with_smooth_penalty_for_wanted_class_func(trainer.model,
#                                                                                 smooth_weight=0.01)
#inputs_class_1_smooth = gradient_descent_input(start_in, in_grad_func_smooth, [1,0], learning_rule=None, **ascent_args)
#inputs_class_2_smooth = gradient_descent_input(start_in, in_grad_func_smooth, [0,1], learning_rule=AdamNumpy(), 
                                               #**ascent_args)


# In[584]:

pyplot.plot(np.squeeze(inputs_class_1[::100])[:,2].T)
pyplot.figure()
pyplot.plot(np.squeeze(inputs_class_1[-1,2]).T)


# In[585]:

pyplot.plot(np.squeeze(inputs_class_2[0::100])[:,1].T)
pyplot.figure()
pyplot.plot(np.squeeze(inputs_class_2[-1,1]).T)


# In[439]:




# # Models on real data

# In[162]:

#ResultPrinter('data/models/debug/raw-square-smaller/early-stop/').print_results()


# In[23]:

model = serial.load('data/models/debug/raw-square-smaller/early-stop/13.pkl').model


# In[25]:

model.input_space


# In[104]:

inputs = T.ftensor4()
class_out = model.fprop(inputs)
fprop_func = theano.function([inputs], class_out)
wanted_output = T.fvector()
cost_right_class = model.layers[3].cost(wanted_output.dimshuffle('x', 0), class_out)
grad_right_class = T.grad(cost_right_class, inputs)

grad_func = theano.function([inputs, wanted_output], grad_right_class)


# In[70]:

rng = RandomState(np.uint64(hash("fprop")))
batches = 10
out = fprop_func(rng.randn(batches, model.input_space.num_channels, *model.input_space.shape).astype(np.float32))


# In[157]:

rng = RandomState(np.uint64(hash("grad")))
start_input = rng.randn(batches, model.input_space.num_channels, *model.input_space.shape).astype(np.float32)
grad = grad_func(start_input, np.float32([0,1,0,0]))


# In[158]:

cur_input = deepcopy(start_input)
all_inputs = []


# In[160]:

for epoch in range(1000):
    grad = grad_func(cur_input, np.float32([1,0,0,0]))
    cur_input = cur_input + 10.0 * grad
    cur_input = cur_input * 0.99
    
    all_inputs.append(deepcopy(cur_input))


# In[161]:

_ = plot_sensor_signals(cur_input[2], yticks="onlymax", sensor_names=get_C_sensors_sorted())


# In[144]:

_ =plot_sensor_signals(cur_input[0], yticks="onlymax", sensor_names=get_C_sensors_sorted())


# In[92]:

_ =plot_sensor_signals(cur_input[1], yticks="onlymax", sensor_names=get_C_sensors_sorted())


# In[89]:

plot_sensor_signals(grad[0], yticks="onlymax", sensor_names=get_C_sensors_sorted())
None

