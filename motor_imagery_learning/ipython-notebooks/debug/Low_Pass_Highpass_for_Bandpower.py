
# coding: utf-8

# In[22]:

import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# switch to cpu
os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import numpy as np
from pylearn2.utils import serial
from motor_imagery_learning.analysis.print_results import ResultPrinter
from motor_imagery_learning.train_scripts.train_with_params import create_training_object_from_file

import matplotlib
from matplotlib import pyplot
get_ipython().magic(u'matplotlib inline')
get_ipython().magic(u"config InlineBackend.figure_format = 'svg'")
matplotlib.rcParams['figure.figsize'] = (12.0, 5.0)
matplotlib.rcParams['font.size'] = 7


# * 

# In[2]:

train_highpass_fama = create_training_object_from_file('data/models/debug/bandpower/early-stop/10.yaml') 


# In[3]:

train_no_highpass_fama = create_training_object_from_file('data/models/debug/bandpower/early-stop/42.yaml')


# In[4]:

result_with_highpass = serial.load('data/models/debug/bandpower/early-stop/10.result.pkl')
result_no_highpass = serial.load('data/models/debug/bandpower/early-stop/42.result.pkl')


# In[5]:

assert np.array_equal(result_no_highpass.targets['test'], result_with_highpass.targets['test'])
assert np.array_equal(result_no_highpass.targets['train'], result_with_highpass.targets['train'])


# In[6]:

predicted_test_no_highpass = np.argmax(result_no_highpass.predictions['test'], axis=1)
predicted_test_highpass = np.argmax(result_with_highpass.predictions['test'], axis=1)

true_test = np.argmax(result_no_highpass.targets['test'], axis=1)


# In[7]:

correct_no_highpass = true_test == predicted_test_no_highpass


# In[8]:

correct_highpass = true_test == predicted_test_highpass


# In[9]:

print("test accuracy according to result predictions", sum(correct_no_highpass) / float(len(true_test)))
print ("test accuracy according to result channels", 1 - result_no_highpass.get_misclasses()['test'][-1])


# In[10]:

highpass_better = np.logical_and(correct_highpass, np.logical_not(correct_no_highpass))
no_highpass_better = np.logical_and(correct_no_highpass, np.logical_not(correct_highpass))


# In[11]:

# Lets load the datasets
train_no_highpass_fama.dataset.clean_and_load_set()
train_highpass_fama.dataset.clean_and_load_set()


# In[23]:

np.array_equal(train_highpass_fama.dataset.clean_trials, train_no_highpass_fama.dataset.clean_trials)


# In[17]:

trials_no_highpass = train_no_highpass_fama.dataset.bbci_set.epo.data[-56:].transpose(0,2,1)
trials_highpass = train_highpass_fama.dataset.bbci_set.epo.data[-56:].transpose(0,2,1)


# In[13]:

train_no_highpass_fama.dataset.rejected_chans


# In[19]:

no_highpass_better_trials_no_highpass = trials_no_highpass[no_highpass_better]
no_highpass_better_trials_highpass = trials_highpass[no_highpass_better]


# In[39]:

from motor_imagery_learning.analysis.plot_util import plot_sensor_signals

for i in xrange(len(no_highpass_better_trials_highpass)):
    _ = plot_sensor_signals(no_highpass_better_trials_no_highpass[i], train_highpass_fama.dataset.sensor_names,
                       sharey=False, highlight_zero_line=False)

    _ = plot_sensor_signals(no_highpass_better_trials_highpass[i], train_highpass_fama.dataset.sensor_names,
                       sharey=False, highlight_zero_line=False)


# In[35]:

from copy import deepcopy
old_bbci_set = deepcopy(train_no_highpass_fama.dataset.bbci_set)


# In[11]:

get_ipython().run_cell_magic(u'capture', u'', u'train_no_highpass_fama.main_loop()')


# In[38]:

sum(correct_highpass)


# In[39]:

sum(correct_no_highpass)


# In[40]:

sum(np.logical_or(correct_highpass, correct_no_highpass))


# In[41]:

len(true_test)


# In[28]:

ResultPrinter('data/models/debug/bandpower/early-stop/').print_results()

