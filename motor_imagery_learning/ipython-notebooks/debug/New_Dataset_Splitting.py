
# coding: utf-8

# In[1]:

import os

import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
get_ipython().magic(u'cd /home/schirrmr/motor-imagery/code/motor_imagery_learning/')
assert 'THEANO_FLAGS' in os.environ
# switch to cpu...
os.environ['THEANO_FLAGS'] = "floatX=float32,device=cpu,nvcc.fastmath=True"
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
import theano


# In[2]:

from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from pylearn2.models.mlp import MLP, Softmax
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
from numpy.random import RandomState
import numpy as np
from pylearn2.format.target_format import OneHotFormatter
from pylearn2.termination_criteria import EpochCounter
from motor_imagery_learning.mypylearn2.dataset_splitters import DatasetSingleFoldSplitter, DatasetTwoFileSingleFoldSplitter


# In[3]:

input_shape = (13,5,2,3)

max_epochs_crit = EpochCounter(max_epochs=1)
algorithm = SimilarBatchSizeSGD(learning_rate=0.1, batch_size=10,seed=np.uint64(hash('test_train_sgd')),
                                termination_criterion=max_epochs_crit)

softmax = Softmax(n_classes=2, layer_name='y', irange=0.01)
mlp = MLP(layers=[softmax], seed= np.uint64(hash('test_train_mlp')))

rng = RandomState(np.uint64(hash('test_dataset')))
topo_view = rng.randn(*input_shape)

y = OneHotFormatter(2).format(rng.random_integers(0,1,size=input_shape[0]))
dataset = DenseDesignMatrix(topo_view=topo_view, y=y, rng=rng)
dataset_splitter = DatasetSingleFoldSplitter(dataset, num_folds=10, test_fold_nr=-1)


# In[4]:

trainer = TrainEarlyStop(dataset_splitter, mlp, algorithm=algorithm, save_path=None, preprocessor=None, 
                         keep_best_lrule_params=False, 
                         best_model_channel='valid_y_misclass', fraction_epochs_after_stop=1.0)


# In[5]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer.main_loop()')


# In[6]:

train_misclass_expected = [0.36363637, 0.0, 0.0, 0.083333336, 0.083333336, 0.083333336]
valid_misclass_expected = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
test_misclass_expected = [1.0, 0.0, 0.0, 0.0, 0.0, 0.0]
assert np.allclose(train_misclass_expected, trainer.model.monitor.channels['train_y_misclass'].val_record)
assert np.array_equal(valid_misclass_expected, trainer.model.monitor.channels['valid_y_misclass'].val_record)
assert np.array_equal(test_misclass_expected, trainer.model.monitor.channels['test_y_misclass'].val_record)


# ## Two File Splitter

# In[7]:

input_shape = (13,5,2,3)

max_epochs_crit = EpochCounter(max_epochs=1)
algorithm = SimilarBatchSizeSGD(learning_rate=0.1, batch_size=10,seed=np.uint64(hash('test_train_sgd')),
                                termination_criterion=max_epochs_crit)

softmax = Softmax(n_classes=2, layer_name='y', irange=0.01)
mlp = MLP(layers=[softmax], seed= np.uint64(hash('test_train_mlp')))

rng = RandomState(np.uint64(hash('test_dataset')))
topo_view = rng.randn(*input_shape)

y = OneHotFormatter(2).format(rng.random_integers(0,1,size=input_shape[0]))
train_set = DenseDesignMatrix(topo_view=topo_view, y=y, rng=rng)

test_topo_view = rng.randn(*input_shape)
test_y = OneHotFormatter(2).format(rng.random_integers(0,1,size=input_shape[0]))
test_set = DenseDesignMatrix(topo_view=test_topo_view, y=test_y, rng=rng)

dataset_splitter = DatasetTwoFileSingleFoldSplitter(train_set = train_set, test_set=test_set, num_folds=10)


# In[8]:

trainer = TrainEarlyStop(dataset_splitter, mlp, algorithm=algorithm, save_path=None, preprocessor=None, 
                         keep_best_lrule_params=False, 
                         best_model_channel='valid_y_misclass', fraction_epochs_after_stop=1.0)


# In[9]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer.main_loop()')


# In[10]:

train_misclass_expected = [0.41666669, 0.083333336, 0.0, 0.0]
valid_misclass_expected = [1.0, 0.0, 0.0, 0.0]
test_misclass_expected = [0.46153846, 0.23076925, 0.23076925, 0.23076925]
assert np.allclose(train_misclass_expected, trainer.model.monitor.channels['train_y_misclass'].val_record)
assert np.array_equal(valid_misclass_expected, trainer.model.monitor.channels['valid_y_misclass'].val_record)
assert np.allclose(test_misclass_expected, trainer.model.monitor.channels['test_y_misclass'].val_record)


# ## Test having two datasets with different cleaning etc.

# In[11]:

from motor_imagery_learning.bbci_pylearn_dataset import (
    BBCIPylearnCleanFFTDataset, compute_power_spectra, BBCISetCleaner)
from motor_imagery_learning.mywyrm.processing import resample_cnt, highpass_cnt
from motor_imagery_learning.mypylearn2.learning_rule_adam import Adam

train_set = BBCIPylearnCleanFFTDataset(filenames='data/BBCI-without-last-runs/BhNoMoSc1S001R01_ds10_1-12.BBCI.mat',
                                       cleaner = BBCISetCleaner(),
                                       transform_function_and_args=(compute_power_spectra, 
                                        {'divide_win_length': False, 'square_amplitude': False}),
                                   cnt_preprocessors=[(resample_cnt, {'newfs': 100})],
                                                      load_sensor_names=['C3', 'C4'])

max_epochs_crit = EpochCounter(max_epochs=2)
algorithm = SimilarBatchSizeSGD(learning_rule=Adam(), learning_rate=1, 
                                batch_size=10,seed=np.uint64(hash('test_train_sgd')),
                                termination_criterion=max_epochs_crit)

softmax = Softmax(n_classes=4, layer_name='y', irange=0.01)
mlp = MLP(layers=[softmax], seed= np.uint64(hash('test_train_mlp')))

dataset_splitter = DatasetSingleFoldSplitter(dataset=train_set, num_folds=10, test_fold_nr=9)
trainer = TrainEarlyStop(dataset_splitter, mlp, algorithm=algorithm, save_path=None, preprocessor=None, 
                         keep_best_lrule_params=False, 
                         best_model_channel='valid_y_misclass', fraction_epochs_after_stop=1.0)


# In[12]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer.main_loop()')


# In[13]:

train_misclass_expected = [0.76205754, 0.57073933, 0.72025692, 0.54180044, 0.40200272]
valid_misclass_expected = [0.74025971, 0.59740263, 0.74025971, 0.59740263, 0.45454544]
test_misclass_expected = [0.71428573, 0.58441561, 0.67532468, 0.5454545, 0.33766234]
assert np.allclose(train_misclass_expected, trainer.model.monitor.channels['train_y_misclass'].val_record)
assert np.allclose(valid_misclass_expected, trainer.model.monitor.channels['valid_y_misclass'].val_record)
assert np.allclose(test_misclass_expected, trainer.model.monitor.channels['test_y_misclass'].val_record)


# In[17]:

from motor_imagery_learning.bbci_pylearn_dataset import (
    BBCIPylearnCleanFFTDataset, compute_power_spectra, BBCISetCleaner, BBCISetSecondSetCleaner)
from motor_imagery_learning.mywyrm.processing import resample_cnt, highpass_cnt
from motor_imagery_learning.mypylearn2.learning_rule_adam import Adam
#highpass_cnt)
first_set_cleaner = BBCISetCleaner()
train_set = BBCIPylearnCleanFFTDataset(filenames='data/BBCI-without-last-runs/BhNoMoSc1S001R01_ds10_1-12.BBCI.mat',
                                       cleaner = first_set_cleaner,
                                       transform_function_and_args=(compute_power_spectra, 
                                        {'divide_win_length': False, 'square_amplitude': False}),
                                   cnt_preprocessors=[(resample_cnt, {'newfs': 100})],
                                                      load_sensor_names=['CP3', 'CP4', 'C3', 'C4','C1','C2','CP1', 'CP2',
                                                                        'CPP6h'])

test_set = BBCIPylearnCleanFFTDataset(filenames='data/BBCI-without-last-runs/JoBeMoSc01S001R01_ds10_1-11.BBCI.mat',
                                      cleaner=BBCISetSecondSetCleaner(first_set_cleaner=first_set_cleaner),
                                       transform_function_and_args=(compute_power_spectra, 
                                        {'divide_win_length': False, 'square_amplitude': False}),
                                   cnt_preprocessors=[(resample_cnt, {'newfs': 100})],
                                                      load_sensor_names=['CP3', 'CP4', 'C3', 'C4','C1','C2','CP1', 'CP2',
                                                                        'CPP6h'])
max_epochs_crit = EpochCounter(max_epochs=3)
algorithm = SimilarBatchSizeSGD(learning_rule=Adam(), learning_rate=1, 
                                batch_size=10,seed=np.uint64(hash('test_train_sgd')),
                                termination_criterion=max_epochs_crit)

softmax = Softmax(n_classes=4, layer_name='y', irange=0.01)
mlp = MLP(layers=[softmax], seed= np.uint64(hash('test_train_mlp')))

dataset_splitter = DatasetTwoFileSingleFoldSplitter(train_set = train_set, test_set=test_set, num_folds=10)
trainer = TrainEarlyStop(dataset_splitter, mlp, algorithm=algorithm, save_path=None, preprocessor=None, 
                         keep_best_lrule_params=False, 
                         best_model_channel='valid_y_misclass', fraction_epochs_after_stop=1.0)


# In[18]:

get_ipython().run_cell_magic(u'capture', u'', u'trainer.main_loop()')


# In[16]:

train_misclass_expected = [0.73390543, 0.68812579, 0.49213159, 0.46494988, 0.41917023, 0.44974226]
valid_misclass_expected = [0.74025977, 0.6493507, 0.46753246, 0.50649351, 0.41558444, 0.41558444]
test_misclass_expected = [0.72521234, 0.68271953, 0.75779033, 0.71246451, 0.73796016, 0.69546765]
assert np.allclose(train_misclass_expected, trainer.model.monitor.channels['train_y_misclass'].val_record)
assert np.allclose(valid_misclass_expected, trainer.model.monitor.channels['valid_y_misclass'].val_record)
assert np.allclose(test_misclass_expected, trainer.model.monitor.channels['test_y_misclass'].val_record)

