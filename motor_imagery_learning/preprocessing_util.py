import numpy as np
def exponential_running_mean(data, factor_new, start_mean=None,
    init_block_size=None, axis=None):
    """ Compute the running mean across axis 0.
    For each datapoint in axis 0 its "running exponential mean" is computed as:
    Its mean * factor_new + so far computed mean * (1-factor_new).
    You can either specify a start mean or an init_block_size to 
    compute the start mean of. 
    In any case one mean per datapoint in axis 0 is returned."""
    assert not (start_mean is None and init_block_size is None), (""
        "Need either an init block or a start mean")
    assert start_mean is None or init_block_size is None, ("Can only use start mean"
                                                           "or init block size")
    assert factor_new <= 1.0    
    assert factor_new >= 0.0
    if axis is None:
        axis = tuple(range(1,np.array(data).ndim))
    if isinstance(axis, int):
        axis = (axis,)
    #running_means = np.ones
    factor_old = 1 - factor_new
    if start_mean is None:
        start_data = data[0:init_block_size]
        axes_for_start_mean = (0,) + axis # also average across init trials
        current_mean = np.mean(start_data, axis=axes_for_start_mean)
        data = data[init_block_size:]
        # repeat mean for running means
        running_means = [current_mean.copy() for _ in range(init_block_size)]
        running_means = np.array(running_means)
    else:
        # do first iteraiton of loop to init running means with correct shape
        # TODELAY: how to do this without this else clause? as it is basically
        # copied code from loop for first iteration
        first_mean = factor_new * np.mean(data[0:1], axis=axis) + factor_old * start_mean
        data = data[1:]
        running_means = np.array(first_mean) # wrap in array with one more dimension
        current_mean = first_mean
        
    for i in range(len(data)):
        next_mean = factor_new * np.mean(data[i:i+1], axis=axis) + factor_old * current_mean
        running_means = np.concatenate((running_means, next_mean), axis=0)
        current_mean = next_mean
    return running_means

def exponential_running_var(data, running_means, factor_new, start_var=None,
    init_block_size=None,   axis=None):
    """ Compute the running var across axis 0.
    For each datapoint in axis 0 its "running exponential var" is computed as:
    Its (datapoint - its running mean)**2 * factor_new + so far computed var * (1-factor_new).
    You can either specify a start mean or an initial block size to 
    compute the start mean of. 
    In any case one mean per datapoint in axis 0 is returned."""
    assert not (start_var is None and init_block_size is None), (""
        "Need either an init block or a start mean")
    assert start_var is None or init_block_size is None, ("Can only use start mean"
        "or init block size, not both")
    assert factor_new <= 1.0    
    assert factor_new >= 0.0
    data = np.array(data)
    if axis is None:
        axis = tuple(range(1,np.array(data).ndim))
    if isinstance(axis, int):
        axis = (axis,)
    # add missing dimensions to running means to prevent incorrect broadcasts
    for i in range(1,np.array(data).ndim):
        if i in axis:
            running_means = np.expand_dims(running_means, i)
    demeaned_data = data - running_means
    return exponential_running_var_from_demeaned(demeaned_data,
        factor_new, start_var, init_block_size, axis)

def exponential_running_var_from_demeaned(demeaned_data, factor_new, start_var=None,
    init_block_size=None,   axis=None):
    # TODELAY: split out if and else case into different functions
    # i.e. split apart a common function hjaving a start value (basically the loop)
    # and then split if and else into different functions
    factor_old = 1 - factor_new
    # prealloc running vars for performance (otherwise much slower)
    running_vars_shape = [len(demeaned_data)]
    for i in range(1, demeaned_data.ndim):
        if i not in axis:
            running_vars_shape.append(demeaned_data.shape[i])
    running_vars = np.ones(running_vars_shape) * np.nan
    if start_var is None:
        start_running_var = np.mean(np.square(demeaned_data[0:init_block_size]),
             axis=axis)
        start_running_var = np.mean(start_running_var, axis=0)
        running_vars[0:init_block_size] = start_running_var
        start_i = init_block_size
    else:
        first_start_var = np.mean(np.square(demeaned_data[0:1]), axis=axis)
        first_start_var = first_start_var[0] # unwrap
        first_var = factor_new * first_start_var + factor_old * start_var 
        running_vars[0] = first_var
        start_i = 1
    current_var = running_vars[start_i-1]
    for i in range(start_i, len(demeaned_data)):
        this_var = np.mean(np.square(demeaned_data[i:i+1]),axis=axis)
        next_var = factor_new * this_var + factor_old * current_var
        running_vars[i] = next_var
        current_var = next_var
    return running_vars


def compute_combined_mean(num_old, num_new, old_mean, new_mean):
    # formula according to http://stats.stackexchange.com/a/43183
    return (num_old*old_mean + num_new*new_mean) / \
                (num_old + num_new)

def compute_combined_std(num_old, num_new, old_mean, new_mean, 
    combined_mean, old_std, new_std):
    # formulas according to http://stats.stackexchange.com/a/43183
    combined_var = ((num_old * (old_std**2 + old_mean**2) + \
                    num_new * (new_std**2 + new_mean**2)) / \
                    (num_old + num_new)) - \
                    combined_mean ** 2
    combined_std = np.sqrt(combined_var)
    return combined_std
