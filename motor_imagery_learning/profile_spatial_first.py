#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
assert 'THEANO_FLAGS' in os.environ
#os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
#os.environ['THEANO_FLAGS'] = os.environ['THEANO_FLAGS'] + ",profile=True"
# switch to cpu
#os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'

import h5py
import numpy as np
from pylearn2.utils import serial
from pprint import pprint
from  motor_imagery_learning.analysis.sensor_positions import get_C_sensors_sorted
import h5py
from motor_imagery_learning.csp.train_csp import CSPTrain
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from motor_imagery_learning.mypylearn2.preprocessing import OnlineStandardize
from motor_imagery_learning.bbci_pylearn_dataset import BBCIPylearnCleanFFTDataset
from motor_imagery_learning.bbci_dataset import BBCIDataset
from pylearn2.costs.mlp import Default
from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
import numpy as np
from numpy.random import RandomState
import theano.tensor as T
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import ConvElemwise
from theano.sandbox.cuda.dnn import dnn_conv
from theano.sandbox.cuda.basic_ops import gpu_alloc_empty
def convolve_keeping_chans(inputs, filters, numfilters):
    # inputs shape should be #batches #virtualchans x #0 x #1
    # loop through filters, make convolutions for 1-dimensional chans
    outshape = compute_out_shape(inputs.shape, filters.shape)
    rval = gpu_alloc_empty(inputs.shape[0], numfilters, outshape[2], outshape[3])
    for filter_i in range(numfilters):
        conved_by_filter_i = dnn_conv(inputs[:,filter_i:filter_i+1,:,:], filters[filter_i:filter_i+1,:,:,:])
        rval = T.set_subtensor(rval[:,filter_i:filter_i+1,:,:], conved_by_filter_i)
    return rval
    # we want output for next layer:  #batches x #channels x #(convolved 0) x #(convolved 1)
    # => output dim channels is same as input dim channels
    # reshape input to: inputs.reshape(inputs.shape[0] * inputs.shape[1], inputs.shape[2], inputs.shape[3])
    # inputs = inputs.dimshuffle(0, 'x', 1, 2) (#batches*#channels) x 1 x  #(convolved 0) x #(convolved 1)
    # conv mit filters der größe (#batches*#channels) x 1 x #(filter 0) x #(filter 1)
    # result.reshape(inputs.shape[0], inputs.shape[1], inputs.shape[2], inputs.shape[3])

def convolve_keeping_chans_one_conv(inputs, filters, numfilters):
    # inputs shape should be #batches #virtualchans x #0 x #1
    outshape = compute_out_shape(inputs.shape, filters.shape)
    # reshape so that each channel is an own "input image"
    # will make unnecessary convolutions since it convolves any filter with any channel,
    # but we only want filter 1 convolved with channel 1(for all images),
    # filter 2 with channel 2 etc.
    # so we only need #batches x #filters convolutions
    # but we will make #batches x #filters x#chans (or sth like this, maybe not correct :))
    rval = gpu_alloc_empty(inputs.shape[0], numfilters, outshape[2], outshape[3])
    inputs = inputs.reshape((inputs.shape[0] * inputs.shape[1], 1, inputs.shape[2],
                           inputs.shape[3]))
    conved = dnn_conv(inputs, filters)
    
    # Take only the convolutions we actually want:
    # we want (batch-filter), always matching channel 0 with filter 0, channel 1 with filter 1 etc.:
    # 0-0, 7-0, 14-0,...70-0,
    # 1-1, 8-1, 15-1 ..
    # ..
    # 6-6, 13-6, ..., 76-6
    # we don't want:
    # 0-1, 0-2,0-3, ..., 0-6
    # 1-0, 1-2, 1-3, .. 1-6
    # ...
    # 
    for filter_i in range(numfilters):
         rval = T.set_subtensor(rval[:,filter_i:filter_i+1,:,:], 
                                conved[filter_i::num_filters, filter_i:filter_i+1, :, :])
    return rval


def loop_conv(X, W):
    """ Gold standard convolution for test, looping over all dimensions.
    Actually performing cross correlation, not convolution. 
    Assumes bc012 input"""
    # Go over all five dimensions 
    # (#batches x #channels x #height x #width x #dur/length )
    # with filter that has
    # #filters x #channels x #height x #width x #dur/length 
    num_filters = W.shape[0]
    filt_channels = W.shape[1]
    filt_height = W.shape[2]
    filt_width = W.shape[3]
    num_batches = X.shape[0]
    input_channels = X.shape[1]
    assert(filt_channels == input_channels)
    out_shape = compute_out_shape(X.shape, W.shape)
    out_height = out_shape[2]
    out_width = out_shape[3]
    
    # The output is H :)
    H = np.zeros((out_shape))
    for batch_i in xrange(0, num_batches):
        for filt_i in xrange(0, num_filters):
            for out_x in xrange(0, out_height):
                for out_y in xrange(0, out_width):
                    for chan_i in xrange(0, filt_channels):
                        for filt_x in xrange(0, filt_height):
                            for filt_y in xrange(0, filt_width):
                                weight = W[filt_i, chan_i, filt_x, filt_y]
                                input_val =  X[batch_i, chan_i, \
                                    out_x + filt_x, out_y + filt_y]
                                H[batch_i, filt_i, out_x, out_y] += \
                                     weight * input_val
    return H


def compute_out_shape(inputs_shape, filters_shape):
    num_batches = inputs_shape[0]
    out_height = inputs_shape[2] - filters_shape[2] + 1;
    out_width = inputs_shape[3] - filters_shape[3] + 1;
    num_filters = filters_shape[0]
    return (num_batches, num_filters, out_height, out_width)

    
input_shape = (600,3)
num_chans = 45
num_filters = 80
kernel_shape=(10,2)
num_batches = 100
real_input = np.random.randn(num_batches, num_chans, *input_shape).astype('float32')
input_space = Conv2DSpace(shape=input_shape, num_channels=num_chans, axes=('b', 'c', 0, 1))
spatial_filters = np.random.randn(num_filters, num_chans).astype('float32')
kernel_weights = np.random.randn(num_filters, 1, *kernel_shape).astype('float32')
out_shape = compute_out_shape(real_input.shape, kernel_weights.shape)

filtered_result = np.dot(spatial_filters, real_input.T)
# => c10b
filtered_result = filtered_result.transpose(3,0,2,1)
"""
conv_result = np.empty((num_batches,num_filters,out_shape[2],out_shape[3]), dtype='float32')
for i in range(num_filters):
    conv_result[:,i:i+1,:,:] = dnn_func(filtered_result[:,i:i+1,:,:], kernel_weights[i:i+1,:,:,:])
"""

repeated_kernel = np.repeat(kernel_weights.transpose(1,0,2,3), 80, axis=0)

def perf_dnn(filtered_result, repeated_kernel, iterations=3):
    inputs = T.ftensor4()
    filters = T.ftensor4() # filter x 1, inputshape2, inputshape3
    result = dnn_conv(inputs, filters)
    sumresult = T.sum(result)
    #dnn_sum_func = theano.function([inputs, filters], sumresult)
    
    grad = T.grad(sumresult, inputs)
    
    sumgrad = T.sum(grad)
    dnn_sum_grad_func = theano.function([inputs, filters], sumgrad)
    
    
    for _ in range(iterations):
        dnn_sum_grad = dnn_sum_grad_func(filtered_result, repeated_kernel)
    print dnn_sum_grad
    
def perf_conv_keep_chans(num_filters, filtered_result, kernel_weights, 
        iterations=3):
    inputs = T.ftensor4()
    filters = T.ftensor4() # filter x 1, inputshape2, inputshape3
    result = convolve_keeping_chans(inputs, filters, num_filters)
    convolve_func = theano.function([inputs, filters], result)
    sumresult = T.sum(result)
    grad = T.grad(sumresult, inputs)
    sumgrad = T.sum(grad)
    
    keep_chans_sum_grad_func = theano.function([inputs, filters], sumgrad)
    
    for _ in range(iterations):
        keep_chans_sum_grad = keep_chans_sum_grad_func(filtered_result, kernel_weights)
    print keep_chans_sum_grad
    
def perf_conv_one_conv_keep_chans(num_filters, filtered_result, kernel_weights, 
        iterations=3):
    inputs = T.ftensor4()
    filters = T.ftensor4() # filter x 1, inputshape2, inputshape3
    result = convolve_keeping_chans_one_conv(inputs, filters, num_filters)
    #convolve_func = theano.function([inputs, filters], result)
    sumresult = T.sum(result)
    grad = T.grad(sumresult, inputs)
    sumgrad = T.sum(grad)
    
    keep_chans_sum_grad_func = theano.function([inputs, filters], sumgrad)
    
    for _ in range(iterations):
        keep_chans_sum_grad = keep_chans_sum_grad_func(filtered_result, kernel_weights)
    print keep_chans_sum_grad
#perf_dnn(filtered_result, repeated_kernel, iterations=3)
#perf_conv_keep_chans(num_filters, filtered_result, kernel_weights, iterations=3)
perf_conv_one_conv_keep_chans(num_filters, filtered_result, kernel_weights, iterations=3)
    