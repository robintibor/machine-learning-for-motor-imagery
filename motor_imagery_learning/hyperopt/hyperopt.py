#!/usr/bin/env python

"""
This is a script to make hyperparameter optimization with motor imagery data.
Call the train_hyperopt function with a dictionary of parameters
and get a 10-fold average misclassification back.
Example:
    train_hyperopt({"hidden_neurons": "10", "algorithm": "*sgd",
        "layers":"*flat", "max_epochs": "4",
        "learning_rule": "*adam", "costs": "dropout",
        "adam_alpha": "0.0002", "adam_beta1": "0.1", "adam_beta2": "0.001",
        "dataset_args": "*bbci_dataset_fft_args",
        'dataset_filename': filename, 'sensor_names': "['CP3', 'CP4']"
        }, debug=True)
Scroll down to the hyperopt_example_calls function for running this example

Parameters are explained in parameters.txt in the configs/searchspaces/ folder

Parameters that a training will be run come from two sources:
1. the dictionary given to the train_hyperopt
2. the default values in 'configs/train/motor_eeg.yaml'

Parameters from 1 will override parameters from 2.
So if you specify algorithm:'*sgd' in your dictionary
it will override the default algorithm '*bgd' in configs/train/motor_eeg.yaml
"""
import logging
import numpy as np
import os
from pprint import pprint

import motor_imagery_learning.train_scripts.train_with_params as train_with_params

def train_hyperopt(params, debug=False):
    """ Runs 10-fold cross validation with the given parameters
        and returns the average test misclass rate."""
    if debug == True: 
        params = _adjust_params_for_debug(params)
    params  = _adjust_params_for_hyperopt(params)
    # go to directoy above this source-file
    config_filename = os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
    # then complete path to config
    config_filename = os.path.join(config_filename, "configs", "train", "motor_eeg.yaml")
    if not os.path.exists(config_filename):
        raise ValueError("Could not find %s" % config_filename)
    train_string = train_with_params.process_yaml_file(filename=config_filename,
        command_line_train_parameters=params,
        parameter_list_name='default',
        cross_validation=False,
        early_stop=False,
        transfer_learning=False)
    train_obj = train_with_params.create_training_object(train_string)
    # turn off pylearn logging
    logging.getLogger("pylearn2").setLevel(logging.WARN)
    logging.getLogger("motor_imagery_learning").setLevel(logging.WARN)
    train_obj.main_loop()
    result = _get_final_misclass(train_obj)
    print("Result for")
    pprint(params)
    print result
    print("Average Test misclass: {:5.4f}".format(float(result)))
    return result
    
def _adjust_params_for_debug(params):
    debug_params = dict({
        'cross_validation_start': 0,
        'cross_validation_stop': 100,
        'cross_val_folds': 3,
        'load_sensor_names': ['CP3', 'CP4'],
        'max_epochs': 10,
    })
    # overwrite debug params by provided params if both exist for one key
    # (i.e. given params have preference over debug params)
    debug_params.update(params)
    return debug_params

def _adjust_params_for_hyperopt(params):
    # get default parameters for hyperparameter optimization
    hyper_params = dict({
        'save_path': None,
        'save_freq': 0
    })
    # you can overwrite default parameters for hyperparameter optimization with the
    # parameter dict (should probably never be necessary, normally this should just
    # add the values specified in the call to the default values?),e.g.
    # default: {save_freq: "0"}, call_param_dict: {algorithm: "*sgd", hidden_neurons: "8"}
    # -> {save_freq: "0", algorithm: "*sgd", hidden_neurons: "8"}

    hyper_params.update(params)

    # Adjust dropbox input scales to be 1/input_probs
    for key in hyper_params.keys():
        if 'include_probs' in key:
            prob_val = float(hyper_params[key])
            scales_val = 1.0/prob_val
            scales_key = key.replace("include_probs", "scales")
            hyper_params[scales_key] = str(scales_val)
        # sometimes i call it probs instead of include probs, oh well
        elif 'probs' in key:
            prob_val = float(hyper_params[key])
            scales_val = 1.0/prob_val
            scales_key = key.replace("probs", "scales")
            hyper_params[scales_key] = str(scales_val)
            
    if 'axes' in hyper_params.keys():
        assert isinstance(hyper_params['axes'], str)
        # Transform 'b01c' to '[b,0,1,c]'
        hyper_params['axes'] = '['+ ','.join(hyper_params['axes']) + ']'
    if 'segment_ival' in hyper_params.keys():
        # Transform 'from500to4000' -> [500,4000]
        segment_ival_str = hyper_params['segment_ival']
        assert 'from' in segment_ival_str
        assert 'to' in segment_ival_str
        segment_ival_str = segment_ival_str.replace('from', '')
        segment_ival_str = segment_ival_str.replace('to', ',')
        segment_ival_str = '[' + segment_ival_str + ']'
        hyper_params['segment_ival'] = segment_ival_str
    return hyper_params

def _compute_cross_validation_average_misclass(train_saver):
    """ Compute cross validation result given a list of trainers.
    Returns test misclass average over all trainers (i.e. all folds)"""
    models = [t.model for t in train_saver._train_states] 
    test_chans = [model.monitor.channels['test_y_misclass'] for model in models]
    # take the result of the last epoch(-1) as the rate
    test_misclass_rates = [chan.val_record[-1] for chan in test_chans]
    test_misclass_average = np.average(test_misclass_rates)
    return test_misclass_average

def _get_final_misclass(early_stop_trainer):
    monitor = early_stop_trainer.after_stop_trainer.model.monitor
    return monitor.channels['test_y_misclass'].val_record[-1]

def hyperopt_example_calls():
    ''' Some example calls only using sensors CP3 and CP4 '''
    filename = './data/BBCI-without-last-runs/LaKaMoSc1S001R01_ds10_1-9.BBCI.mat'

    sensor_names = "['CP3', 'CP4']"
    
    # expected Average Test misclass: 0.6595
    # Bandpower Flat
    train_hyperopt({"hidden_neurons": "10", "algorithm": "*sgd",
        "layers":"*flat", "max_epochs": "4",
        "learning_rule": "*adam", "costs": "dropout",
        "adam_alpha": "0.0002", "adam_beta1": "0.1", "adam_beta2": "0.001",
        "dataset_args": "*bbci_dataset_fft_args",
        'dataset_filename': filename, 'sensor_names': sensor_names
        }, debug=True)
    # expected Average Test misclass: 0.6996

    # Bandpower Conv
    train_hyperopt({"hidden_neurons": "20", "algorithm": "*sgd",
        "layers":"*conv", "max_epochs": "20",
        "output_channels_conv": "10", "batch_size": "50",
        "frequency_start": "0", "frequency_stop": "50",
        "learning_rule": "*adam", "costs": "dropout",
        "adam_alpha": "0.0002", "adam_beta1": "0.1", "adam_beta2": "0.001",
        "dataset_args": "*bbci_dataset_fft_args",
        'dataset_filename': filename, 'sensor_names': sensor_names
        }, debug=True)
    # expected Average Test misclass: 0.6595

    # Bandpower Conv with dropout include probs
    train_hyperopt({"hidden_neurons": "20", "algorithm": "*sgd",
        "layers":"*conv", "max_epochs": "20",
        "output_channels_conv": "10", "batch_size": "50",
        "frequency_start": "0", "frequency_stop": "50",
        "learning_rule": "*adam", "costs": "dropout",
        "adam_alpha": "0.0002", "adam_beta1": "0.1", "adam_beta2": "0.001",
        "dataset_args": "*bbci_dataset_fft_args",
        'dataset_filename': filename, 'sensor_names': sensor_names,
        'full_second_layer_input_include_probs': 0.322344,
        'first_layer_input_include_probs': 0.4567,
        'final_layer_input_include_probs': 0.4679,
        'pool_shape_time': 3,
        'pool_shape_freq': 5,
        }, debug=True)
    # expected Average Test misclass: 0.7911
if __name__ == "__main__":
    hyperopt_example_calls()
