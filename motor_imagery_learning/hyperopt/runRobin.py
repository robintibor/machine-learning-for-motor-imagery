import os
import sys
import time

import motor_imagery_learning.hyperopt.hyperopt as hyperopt

__authors__ = ["Katharina Eggensperger"]
__contact__ = "automl.org"

import logging

def parse_cli():
    """
    Provide a generic command line interface for benchmarks. It will just parse
    the command line according to simple rules and return two dictionaries, one
    containing all arguments for the benchmark algorithm like dataset,
    crossvalidation metadata etc. and the containing all learning algorithm
    hyperparameters.

    Parsing rules:
    - Arguments with two minus signs are treated as benchmark arguments, Xalues
     are not allowed to start with a minus. The last argument must --params,
     starting the hyperparameter arguments.
    - All arguments after --params are treated as hyperparameters to the
     learning algorithm. Every parameter name must start with one minus and must
     have exactly one value which has to be given in single quotes.
    - Arguments with no value before --params are treated as boolean arguments

    Example:
    python neural_network.py --folds 10 --fold 1 --dataset convex  --params
        -depth '3' -n_hid_0 '1024' -n_hid_1 '1024' -n_hid_2 '1024' -lr '0.01'
    """
    args = {}
    arg_name = None
    arg_values = None
    parameters = {}

    cli_args = sys.argv
    found_params = False
    skip = True
    iterator = enumerate(cli_args)

    for idx, arg in iterator:
        if skip:
            skip = False
            continue
        else:
            skip = True

        if arg == "--params":
            if arg_name:
                args[arg_name] = " ".join(arg_values)
            found_params = True
            skip = False

        elif arg[0:2] == "--" and not found_params:
            if arg_name:
                args[arg_name] = " ".join(arg_values)
            arg_name = arg[2:]
            arg_values = []
            skip = False

        elif arg[0:2] == "--" and found_params:
            raise ValueError("You are trying to specify an argument after the "
                             "--params argument. Please change the order.")

        elif arg[0] == "-" and arg[0:2] != "--" and found_params:
            parameters[cli_args[idx][1:]] = cli_args[idx+1]

        elif arg[0] == "-" and arg[0:2] != "--" and not found_params:
            raise ValueError("You either try to use arguments with only one lea"
                             "ding minus or try to specify a hyperparameter bef"
                             "ore the --params argument. %s" %
                             " ".join(cli_args))
        elif arg[0:2] != "--" and not found_params:
            arg_values.append(arg)
            skip = False

        elif not found_params:
            raise ValueError("Illegal command line string, expected an argument"
                             " starting with -- but found %s" % (arg,))

        else:
            raise ValueError("Illegal command line string, expected a hyperpara"
                             "meter starting with - but found %s" % (arg,))

    return args, parameters


def main(params, **kwargs):
    #print 'Params: ', params,
    print "kwargs", kwargs
    if "debug" in kwargs and kwargs['debug'] == '1':
        print "#" * 80
        print "# DEBUGGING "
        print "#" * 80
        logging.basicConfig(level=logging.DEBUG)
    y = hyperopt.train_hyperopt(params)
    print 'Result: ', y
    return y

if __name__ == "__main__":
    starttime = time.time()
    args, params = parse_cli()
    print params

    dataset_dir = args['dataset_dir']
    fold = int(float(args['fold']))

    if "dataset" in args:
        dataset_filename = args['dataset']
        inner_fold_nr = fold
    else:
        dataset_list = ["AnWeMoSc1S001R01_ds10_1-12.BBCI.mat",
                        "BhNoMoSc1S001R01_ds10_1-12.BBCI.mat",
                        "FaMaMoSc1S001R01_ds10_1-14.BBCI.mat",
                        "FrThMoSc1S001R01_ds10_1-11.BBCI.mat",
                        "GuJoMoSc01S001R01_ds10_1-11.BBCI.mat",
                        "JoBeMoSc01S001R01_ds10_1-11.BBCI.mat",
                        "KaUsMoSc1S001R01_ds10_1-11.BBCI.mat",
                        "LaKaMoSc1S001R01_ds10_1-9.BBCI.mat",
                        #"LuFiMoSc3S001R01_ds10_1-11.BBCI.mat",
                        "MaGlMoSc2S001R01_ds10_1-12.BBCI.mat",
                        "MaJaMoSc1S001R01_ds10_1-11.BBCI.mat",
                        #"MaKiMoSC01S001R01_ds10_1-4.BBCI.mat",
                        "MaVoMoSc1S001R01_ds10_1-11.BBCI.mat",
                        "NaMaMoSc1S001R01_ds10_1-11.BBCI.mat",
                        "OlIlMoSc01S001R01_ds10_1-11.BBCI.mat",
                        "PiWiMoSc1S001R01_ds10_1-11.BBCI.mat",
                        "RoBeMoSc03S001R01_ds10_1-9.BBCI.mat",
                        "RoScMoSc1S001R01_ds10_1-11.BBCI.mat",
                        "StHeMoSc01S001R01_ds10_1-10.BBCI.mat",
                        "SvMuMoSc1S001R01_ds10_1-12.BBCI.mat"
                       ]
        # We assume we split our datasets into 10 folds
        # the given fold variable is referring to all folds of all datasets
        # concatenated, so e.g. fold 37 is
        # dataset 3, fold 7 in 0-based indexing

        assert len(dataset_list) == (int(float(args['folds'])) / 10)
        dataset_nr = fold / 10
        inner_fold_nr = fold % 10
        dataset_filename = dataset_list[dataset_nr]

    # DEBUG (both batch size 50: should be 0.454545 for debug raw_net_params,
    # 0.571429 for fbnet debug params):
    #dataset_nr = 1
    #inner_fold_nr = 9

    params['dataset_filename'] = os.path.join(dataset_dir, dataset_filename)
    params['test_fold_nr'] = inner_fold_nr
    assert os.path.isfile(params['dataset_filename'])
    print "Using dataset %s" % params['dataset_filename']

    result = main(params, **args)
    duration = time.time() - starttime
    print "Result for ParamILS: %s, %f, 1, %f, %d, %s" % \
        ("SAT", abs(duration), result, -1, str(__file__))