import numpy as np

def print_cross_validation_results(results):
    '''Print last model for each fold and average of last models'''
    train_misclass_rates = []
    valid_misclass_rates = []
    test_misclass_rates = []
    for index, result in enumerate(results):
        this_model_channels = result.monitor_channels
        if (this_model_channels.has_key('valid_y_misclass')):
            valid_chan = this_model_channels['valid_y_misclass']
            valid_misclass_final = valid_chan.val_record[-1]
            valid_misclass_rates.append(valid_misclass_final)
        test_chan = this_model_channels['test_y_misclass']
        test_misclass_final = test_chan.val_record[-1]
        test_misclass_rates.append(test_misclass_final)
        train_chan = this_model_channels['train_y_misclass']
        train_misclass_final = train_chan.val_record[-1]
        train_misclass_rates.append(train_misclass_final)
        print("train_correct_{:d} : {:5.2f}%".format(index,
            (1 - train_misclass_final) * 100))
        if (this_model_channels.has_key('valid_y_misclass')):
            print("valid_correct_{:d} : {:5.2f}%".format(index,
                (1 - valid_misclass_final) * 100))
        print("test_correct_{:d}  : {:5.2f}%".format(index,
            (1 - test_misclass_final) * 100))
    
    train_misclass_average = np.average(train_misclass_rates)
    test_misclass_average = np.average(test_misclass_rates)
    # print average correct rate in % and with standard deviation
    print("train_correct_avg: {:5.2f}% (+-{:3.2f}%)".format(
        (1 - train_misclass_average) * 100,
        np.std(train_misclass_rates) * 100))

    if (this_model_channels.has_key('valid_y_misclass')):
        valid_misclass_average = np.average(valid_misclass_rates)
        print("valid_correct_avg: {:5.2f}% (+-{:3.2f}%)".format(
            (1 - valid_misclass_average) * 100,
            np.std(valid_misclass_rates) * 100))

    print("test_correct_avg : {:5.2f}% (+-{:3.2f}%)".format(
        (1 - test_misclass_average) * 100,
        np.std(test_misclass_rates) * 100))
    
def print_train_test_result(result):
    ''' Print results for this model'''
    channels = result.monitor_channels
    for channel in ['train_y_misclass', 'valid_y_misclass', 'test_y_misclass']:
        misclass_rate = channels[channel].val_record[-1]
        print("{:20s}: {:5.2f}%".format(
            channel.replace('misclass', 'correct'),
            (1 - misclass_rate) * 100))
    print("")
    print("{:20s}: {:5.2f}%".format("Average valid/test",
           (1 - ((channels['valid_y_misclass'].val_record[-1] + 
           channels['test_y_misclass'].val_record[-1]) 
          / 2)) * 100))
