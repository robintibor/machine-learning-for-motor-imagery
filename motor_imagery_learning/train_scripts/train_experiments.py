#!/usr/bin/env python
from time import sleep
import argparse
from motor_imagery_learning.train_scripts.train_with_params import (
    process_yaml_string, create_training_object_from_file)
from motor_imagery_learning.train_scripts.print_results_after_training import (
    print_cross_validation_results, print_train_test_result)
import os
from pylearn2.config import yaml_parse
import itertools
from pprint import pprint
from copy import copy, deepcopy
from string import Template
from multiprocessing import Process
from pylearn2.utils import serial
import time
from glob import glob
import logging
from pylearn2.utils.logger import (
    CustomStreamHandler, CustomFormatter)
import subprocess
import theano
from motor_imagery_learning.analysis.results import Result

def cartesian_dict_of_lists_product(params):
    # from stackoverflow somewhere (with different func name) :)
    value_product = [x for x in apply(itertools.product, params.values())]
    return [dict(zip(params.keys(), values)) for values in value_product]

def merge_dicts(a, b):
    return dict(a.items() + b.items())

def merge_lists(list_of_lists):
    return list(itertools.chain(*list_of_lists))

def merge_pairs_of_dicts_in_list(l):
    return [dict(a[0].items() + a[1].items()) for a in l]

def product_of_lists_of_dicts(a, b):
    ''' For two lists of dicts, first compute the product as list of dicts.
    First compute the cartesian product of both lists.
    Then merge the pairs of dicts into one list.
    If one of the lists is empty, return the other list.
    Examples:
    >>> a = [{'color':'red', 'number': 2}, {'color':'blue', 'number': 3}]
    >>> b = [{'name':'Jackie'}, {'name':'Hong'}]
    >>> product = product_of_lists_of_dicts(a,b)
    >>> from pprint import pprint
    >>> pprint(product)
    [{'color': 'red', 'name': 'Jackie', 'number': 2},
     {'color': 'red', 'name': 'Hong', 'number': 2},
     {'color': 'blue', 'name': 'Jackie', 'number': 3},
     {'color': 'blue', 'name': 'Hong', 'number': 3}]
    >>> a = [{'color':'red', 'number': 2}, {'color':'blue', 'number': 3}]
    >>> product_of_lists_of_dicts(a, [])
    [{'color': 'red', 'number': 2}, {'color': 'blue', 'number': 3}]
    >>> product_of_lists_of_dicts([], a)
    [{'color': 'red', 'number': 2}, {'color': 'blue', 'number': 3}]
    >>> product_of_lists_of_dicts([], [])
    []
    '''
    if (len(a) > 0 and len(b) > 0):
        product = itertools.product(a, b)
        merged_product = merge_pairs_of_dicts_in_list(product)
        return merged_product
    else:
        return (a if len(a) > 0 else b)
    
def product_of_list_of_lists_of_dicts(list_of_lists):
    '''
    >>> a = [{'color':'red', 'number': 2}, {'color':'blue', 'number': 3}]
    >>> b = [{'name':'Jackie'}, {'name':'Hong'}]
    >>> c = [{'hair': 'grey'}]
    >>> d = []
    >>> product = product_of_list_of_lists_of_dicts([a, b, c, d])
    >>> from pprint import pprint
    >>> pprint(product)
    [{'color': 'red', 'hair': 'grey', 'name': 'Jackie', 'number': 2},
     {'color': 'red', 'hair': 'grey', 'name': 'Hong', 'number': 2},
     {'color': 'blue', 'hair': 'grey', 'name': 'Jackie', 'number': 3},
     {'color': 'blue', 'hair': 'grey', 'name': 'Hong', 'number': 3}]
     '''
    return reduce(product_of_lists_of_dicts, list_of_lists)
#(Order may not be correct in this example)
class ExperimentsRunner:
    def __init__(self, experiments_file_name,  
        template_file_name='configs/train/motor_eeg.yaml',
        cross_validation=True, test=False, start_id=None, stop_id=None, 
        quiet=False, dry_run=False, parallel=False, early_stop=True,
        transfer_learning=False,
        command_line_params=[], processes=None, 
        sleep_between_processes=5,
        print_results=True, transform_models_to_cpu=True,
        first_five_sets=False):
        # If we are running on gpu typically we should not run in parallel...
        assert not (theano.config.device.startswith('gpu') and parallel)
        self._experiments_file_name = experiments_file_name
        self._template_file_name =  template_file_name
        self._cross_validation = cross_validation
        self._start_id = start_id
        self._stop_id = stop_id
        self._test = test
        self._quiet = quiet
        self._dry_run = dry_run
        self._parallel = parallel
        self._early_stop = early_stop
        self._transfer_learning = transfer_learning
        self._command_line_params = command_line_params
        self._num_processes = processes
        self._sleep_between_processes = sleep_between_processes
        self._do_print_results = print_results
        self._logger = logging.getLogger(__name__)
        self._transform_models_to_cpu = transform_models_to_cpu
        self._first_five_sets = first_five_sets
        
    def run(self):
        print("Running {:30s} on {:30s}".format(self._experiments_file_name,
                                                self._template_file_name))
        self.create_all_experiments()
        #print("All experiments:")
        #pprint(self._all_experiments)
        self._run_all_experiments()
        
    def create_all_experiments(self):
        self._read_train_template()
        self._read_yaml_object()
        self._store_templates()
        self._create_all_experiments()

    def _read_train_template(self):
        """ Read the template, probably motor_eeg.yaml """
        self.motor_eeg_template_str = open(self._template_file_name, 'r').read()
    
    def _read_yaml_object(self):
        yaml_string = open(self._experiments_file_name).read()
        self._yaml_obj = yaml_parse.load(yaml_string)
    
    def _store_templates(self):
        if (self._yaml_obj.has_key('templates')):
            self._templates = self._yaml_obj['templates']
        else:
            self._templates = None

    def _create_all_experiments(self):
        self._create_parameters_from_ranges()
        self._create_experiment_variants()
        self._combine_experiment_variants_and_parameter_ranges()
        self._store_save_paths_for_all_experiments()
        self._add_command_line_params()
        self._check_that_all_templates_are_used()
        
    def _create_parameters_from_ranges(self):
        if (self._yaml_obj.has_key('ranges')):
            parameter_ranges = self._yaml_obj['ranges']
            if self._first_five_sets:
                self._only_retain_first_five_sets(parameter_ranges)
            self._parameter_ranges = cartesian_dict_of_lists_product(
                parameter_ranges)
        else:
            self._parameter_ranges = None
    
    def _only_retain_first_five_sets(self, parameter_ranges):
        assert isinstance(parameter_ranges['dataset_filename'], list)
        assert len(parameter_ranges['dataset_filename']) == 18
        parameter_ranges['dataset_filename'] = \
            parameter_ranges['dataset_filename'][0:5]
    
    def _create_experiment_variants(self):
        if self._yaml_obj.has_key('variants'):
            variants = self._yaml_obj['variants']
            self._parameter_variants = self.create_variants_recursively(variants)
        else:
            self._parameter_variants = None
    
    @classmethod
    def create_variants_recursively(cls, variants):
        """
        Create Variants, variants are like structured like trees of ranges basically...
        >>> variant_dict_list = [[{'batches': [1, 2]}]]
        >>> ExperimentsRunner.create_variants_recursively(variant_dict_list)
        [{'batches': 1}, {'batches': 2}]
        >>> variant_dict_list = [[{'batches': [1, 2], 'algorithm': ['bgd', 'sgd']}]]
        >>> ExperimentsRunner.create_variants_recursively(variant_dict_list)
        [{'batches': 1, 'algorithm': 'bgd'}, {'batches': 1, 'algorithm': 'sgd'}, {'batches': 2, 'algorithm': 'bgd'}, {'batches': 2, 'algorithm': 'sgd'}]
        
        >>> variant_dict_list = [[{'batches': [1,2]}, {'algorithm': ['bgd', 'sgd']}]]
        >>> ExperimentsRunner.create_variants_recursively(variant_dict_list)
        [{'batches': 1}, {'batches': 2}, {'algorithm': 'bgd'}, {'algorithm': 'sgd'}]

        >>> variant_dict_list = [[{'algorithm': ['bgd'], 'variants': [[{'batches': [1, 2]}]]}]]
        >>> ExperimentsRunner.create_variants_recursively(variant_dict_list)
        [{'batches': 1, 'algorithm': 'bgd'}, {'batches': 2, 'algorithm': 'bgd'}]
        
        >>> variant_dict_list = [[{'batches': [1, 2]}], [{'algorithm': ['bgd', 'sgd']}]]
        >>> ExperimentsRunner.create_variants_recursively(variant_dict_list)
        [{'batches': 1, 'algorithm': 'bgd'}, {'batches': 1, 'algorithm': 'sgd'}, {'batches': 2, 'algorithm': 'bgd'}, {'batches': 2, 'algorithm': 'sgd'}]


        """
        list_of_lists_of_all_dicts = []
        for dict_list in variants:
            list_of_lists_of_dicts = []
            for param_dict in dict_list:
                param_dict = deepcopy(param_dict)
                variants = param_dict.pop('variants', None) # pop in case it exists
                param_dicts = cartesian_dict_of_lists_product(param_dict)
                if (variants is not None):
                    list_of_variant_param_dicts =  cls.create_variants_recursively(variants)
                    param_dicts = product_of_lists_of_dicts(param_dicts, list_of_variant_param_dicts)
                list_of_lists_of_dicts.append(param_dicts)
            list_of_dicts =  merge_lists(list_of_lists_of_dicts)
            list_of_lists_of_all_dicts.append(list_of_dicts)
        return product_of_list_of_lists_of_dicts(list_of_lists_of_all_dicts)
        
    def _combine_experiment_variants_and_parameter_ranges(self):
        all_experiments = self._parameter_ranges
        if (self._parameter_variants is not None):
            all_experiments = product_of_lists_of_dicts(
                self._parameter_ranges,
                self._parameter_variants)
        self._all_experiments = all_experiments
    
    def _store_save_paths_for_all_experiments(self):
        self._base_save_paths = []
        for i in range(len(self._all_experiments)):
                save_path = self._create_base_save_path(i)
                self._base_save_paths.append(save_path)

    def _get_model_save_path(self, experiment_index):
        return self._base_save_paths[experiment_index] + ".pkl"

    def _get_result_save_path(self, experiment_index):
        return self._base_save_paths[experiment_index] + ".result.pkl"
    
    def _create_base_save_path(self, experiment_index):
        folder_path = self._create_save_folder_path() 
        result_nr = experiment_index + 1
        # try not to overwrite existing models, instead
        # use higher numbers
        existing_result_files = glob(folder_path + "*[0-9].result.pkl")
        if (len(existing_result_files) > 0):
            # model nrs are last part of file name before .pkl
            existing_result_nrs = [int(file_name.split('/')[-1][:-len('.result.pkl')])\
                for file_name in existing_result_files]
            highest_result_nr = max(existing_result_nrs)
            result_nr = highest_result_nr + result_nr
        # Todo: use os.join to prevent mistakes? i.e. than always
        # folder path is a folder, no matter if slash or not at end
        return folder_path + str(result_nr)
    
    def _create_save_folder_path(self):
        folder_path = self._yaml_obj['save_model_path']
        if (self._test):
            folder_path += '/test/'
        if (self._cross_validation):
            folder_path += '/cross-validation/'
        if (self._transfer_learning):
            folder_path += '/transfer/'
        if (self._early_stop):
            folder_path += '/early-stop/'
        if self._first_five_sets:
            folder_path += "/first-5/"
        return folder_path
    
    def _add_command_line_params(self):
        for params in self._all_experiments:
            # command line parameters can override params from experiment file...
            params.update(self._command_line_params)

    def _check_that_all_templates_are_used(self):
        if self._templates is not None:
            template_keys = self._templates.keys()
            param_values = [params.values() for params in self._all_experiments]
            param_values = list(itertools.chain(*param_values))
            needed_templates = [val for val in param_values\
                if isinstance(val, basestring) and val[0] == '$']
            # remove dollar at start
            needed_templates = [name[1:] for name in needed_templates]
            for key in template_keys:
                assert key in needed_templates,\
                    "All templates should be used, {:s} seems not used".format(key)

    def _run_all_experiments(self):
        if (self._quiet):
            logging.getLogger("pylearn2").setLevel(logging.WARNING)
            logging.getLogger("motor_imagery_learning").setLevel(logging.WARN)
        # run in parallel
        if (self._parallel):
            self._processes = []
        
        for i in range(self._get_start_id(),  self._get_stop_id() + 1):
            # will create a subprocess
            self._run_experiment(i)
            if (self._parallel):
                self._wait_until_enough_processes_finished()            
            
        if (self._parallel):
            for p in self._processes:
                p.join()
        # lets mkae it faster for tests by not printing... 
        if (not self._dry_run and self._do_print_results):
            self._print_results()
    
    def _get_start_id(self):
        # Should have created experiments before
        assert(self._all_experiments is not None)
        return 0 if self._start_id is None else self._start_id 
        
    def _get_stop_id(self):
        # Should have created experiments before
        assert(self._all_experiments is not None)
        num_experiments = len(self._all_experiments)
        return num_experiments - 1 if self._stop_id is None else self._stop_id

    def _run_experiment(self, i):
        parameters = self._all_experiments[i]
        print("Now running {:d} of {:d}".format(i + 1,
            len(self._all_experiments)))
        processed_params = self._process_experiment_parameters(parameters, i)
        pprint(processed_params)
        if not(self._dry_run):
            self._run_experiments_with_parameters(i, processed_params)

    def _process_experiment_parameters(self, parameters, experiment_index):
        processed_templates, parameters = self._process_templates(parameters)
        processed_params = self._process_parameters_by_templates(parameters,
            processed_templates)
        processed_params = self._add_model_save_path(processed_params,
            experiment_index)
        # hackhack
        # TODELAY: maybe factor out when you refactor train experiments 
        # altogether?
        # If you need to adapt it for some new varaint quickly,
        # maybe just put it into overwritable method
        if self._test:
            processed_params['load_sensor_names'] = ['C3', 'C4']
            processed_params['sensor_names'] = None
        return processed_params
        
    def _process_templates(self, parameters):
        templates = copy(self._templates)
        processed_templates = {}
        # we need templates to replace placeholders in parameters
        # placeholders defined with $
        needed_template_names = filter(
            lambda value: isinstance(value, basestring) and value[0] == '$', 
            parameters.values())
        parameters_without_template_parameters = deepcopy(parameters)
        
        # remove $ at start! :)
        # e.g. ["$rect_lin", "$dropout"] => ["rect_lin", "dropout"]
        needed_template_names = [name[1:] for name in needed_template_names]
        
        # now for any needed template, first substitute any $-placeholders
        # _within_ the template with a value from the parameter.
        # Then replace the parameter itself with the template
        # e.g. parameters .. {layers: "$flat", hidden_neurons: 8,...},
        # template: flat: [...$hidden_neurons...]
        # 1 => template: flat: [...8...]
        # 2 => processed_parameters .. {layers: "[...8...]", hidden_neurons:8, ...}
        #   => parameters_without_template_parameters: .. {layers: "[...8...]",..}
        for template_name in needed_template_names:
            template_string = templates[template_name]
            for param in parameters_without_template_parameters.keys():
                if ('$' + param) in template_string:
                    parameters_without_template_parameters.pop(param)
            template_string = Template(template_string).substitute(parameters)
            processed_templates[template_name] = template_string
        return processed_templates, parameters_without_template_parameters
    
    def _process_parameters_by_templates(self, parameters, templates):
        processed_parameters = copy(parameters)
        for key in parameters.keys():
            value = parameters[key]
            if isinstance(value, basestring):
                value = Template(value).substitute(templates)
                processed_parameters[key] = value
        return processed_parameters

    def _add_model_save_path(self, processed_params, experiment_index):
        processed_params['save_path'] = self._get_model_save_path(
             experiment_index)
        return processed_params
        
    def _run_experiments_with_parameters(self, experiment_index, params):
        train_string = process_yaml_string(self.motor_eeg_template_str,
            command_line_train_parameters=params,
            parameter_list_name='default', 
            cross_validation=self._cross_validation,
            early_stop=self._early_stop,
            transfer_learning=self._transfer_learning)
                
        self._save_train_string(train_string, experiment_index)
        #self._generate_train_shell_command(experiment_index)
        if (self._parallel): # and not self._only_create_train_files
            new_process = Process(target=self.run_training_process, args=((
                experiment_index,)))
            new_process.start()
            # sleep 5 seconds to prevent memory
            # problems in case all problems are reading the same dataset
            # very fast...
            # because dataset will once be read in full before it is
            # made smaller...
            sleep(self._sleep_between_processes)
            # maybe check pid, wait till started? p.pid()
            # with small time sleeps inbetween
            self._processes.append(new_process)
        else: # and not self._only_create_train_files
            self.run_training_sequential(experiment_index)
            
    def _save_train_string(self, train_string, experiment_index):
        file_name = self._base_save_paths[experiment_index] + ".yaml"
        # create folder if necessary
        if not os.path.exists(os.path.dirname(file_name)):
            os.makedirs(os.path.dirname(file_name))
        yaml_train_file = open(file_name, 'w')
        yaml_train_file.write(train_string)
        yaml_train_file.close()

    def run_training_process(self, experiment_index):
        """ Run Training in a new subprocess, so it is possible to supply
        theano environment flags for different compile directories for
        the different processes"""
        file_name = self._base_save_paths[experiment_index] + ".yaml"
        script_file = "./train_scripts/train_training_object.py"
        command = script_file + " " + file_name
        if (self._quiet):
            command = command + " --quiet"
        if (self._parallel):
            # add theano environment flags for different compile dirs
            # dont use more than 18 different compile dirs for space reasons on hard disk..
            compiledir = "compile_dir_motor_eeg_{:d}".format(
                experiment_index % 18)
            env_string = "THEANO_FLAGS='compiledir_format={:s},exception_verbosity=high,floatX=float32'".format(compiledir)
            command = env_string + " " + command
        # Run Training, Measure Training time
        starttime = time.time()
        subprocess.call([command], shell=True)
        endtime = time.time()
        self.adapt_model_after_train(experiment_index,
            training_time=endtime-starttime)
        
    def run_training_sequential(self, experiment_index):
        """ basically only for tests to run fast :)"""
        starttime = time.time()
        file_name = self._base_save_paths[experiment_index] + ".yaml"
        train_obj = create_training_object_from_file(file_name)
        train_obj.main_loop()
        endtime = time.time()
        self.adapt_model_after_train(experiment_index,
            training_time=endtime-starttime)

    def adapt_model_after_train(self, experiment_index, training_time):
        self._create_result_file(experiment_index, training_time)
        self._store_parameter_info_to_file(experiment_index, 
            training_time)
        if self._transform_models_to_cpu and theano.config.device.startswith('gpu'):
            self.transform_gpu_to_cpu_pkl(experiment_index)

    def _create_result_file(self, experiment_index, training_time):
        train_state_path = self._get_model_save_path(experiment_index)
        train_state_or_states = serial.load(train_state_path)
        result = None
        if (self._cross_validation or self._transfer_learning):
            result = []
            for train_state in train_state_or_states:
                fold_result = self._create_result_for_model(train_state.model, 
                    experiment_index, training_time)
                result.append(fold_result)
        else:
            train_state = train_state_or_states
            result = self._create_result_for_model(train_state.model, 
                    experiment_index, training_time)
        result_path = self._get_result_save_path(experiment_index)
        # Overwrite shouldn't happen I think?
        serial.save(result_path, result, on_overwrite="ignore")

    def _create_result_for_model(self, model, experiment_index, training_time):
        result = Result(
            parameters=self._all_experiments[experiment_index],
            templates=self._templates,
            training_time=training_time,
            monitor_channels=model.monitor.channels,
            predictions=model.info_predictions,
            targets = model.info_targets)
        return result

    # TODO: remove
    def _store_parameter_info_to_file(self, experiment_index, training_time):
        model_path = self._get_model_save_path(experiment_index)
        model_or_models = serial.load(model_path)
        if (self._cross_validation or self._transfer_learning):
            for model in model_or_models:
                self._store_parameter_info_to_model(model, experiment_index,
                    training_time)
        else:
            model = model_or_models
            self._store_parameter_info_to_model(model, experiment_index, 
                training_time)
        serial.save(model_path, model_or_models, on_overwrite="ignore")
    
    def _store_parameter_info_to_model(self, model, experiment_index,
        training_time):
        parameters = self._all_experiments[experiment_index]
        model.info_templates = self._templates
        model.info_parameters = parameters
        model.training_time = training_time

    def transform_gpu_to_cpu_pkl(self, experiment_index):
        """ Just create a file in the appropriate folder so conversion will be done
        from outside"""
        model_path = self._get_model_save_path(experiment_index)
        model_path_as_file_name_part = model_path.replace("/", "_")
        job_filepath = "data/jobs/conversions/convert_" + model_path_as_file_name_part + ".torun"
        with open(job_filepath, "w") as job_file:
            job_file.write(model_path)

    def _wait_until_enough_processes_finished(self):
        # now check number of running processes,
        # wait if we have already reached number of allowed processes
        running_processes = sum([1 if process.exitcode is None else 0 \
                                 for process in self._processes])
        while(running_processes >= self._num_processes):
            time.sleep(5)
            running_processes = sum([1 if process.exitcode is None else 0 \
                                     for process in self._processes])
    
    def _print_results(self):
        # Print all results, respect start and stop id
        # Note: start id and stop-id is one-based not zero-based
        for experiment_index in range(self._get_start_id(), 
            self._get_stop_id() + 1):
            print ("Results for:")
            pprint(self._all_experiments[experiment_index])
            result_path = self._get_result_save_path(experiment_index)
            results = serial.load(result_path)
            if self._cross_validation or self._transfer_learning:
                print_cross_validation_results(results)
            else:
                print_train_test_result(results)
        
def parse_command_line_arguments():
    parser = argparse.ArgumentParser(
        description="""Launch an experiment from a YAML experiment file.
        Example: ./train_experiments.py yaml-scripts/experiments.yaml """
    )
    parser.add_argument('experiments_file_name', action='store',
                        choices=None,
                        help='A YAML configuration file specifying the '
                             'experiment')
    parser.add_argument('--template_file_name', action='store',
                        default='configs/train/motor_eeg.yaml',
                        help='A YAML configuration file specifying the '
                             'template for all experiments')
    parser.add_argument('--cv', action="store_true", 
        help="Use cross validation instead of train test split")
    parser.add_argument('--quiet', action="store_true",
        help="Run algorithm quietly without progress output")
    parser.add_argument('--test', action="store_true",
        help="Run experiment on less features and less data to test it")
    parser.add_argument('--dryrun', action="store_true",
        help="Only show parameters for experiment, don't train.")
    parser.add_argument('--earlystop', action="store_true", 
            help="Use early stop during cross validation instead of waiting for max epochs")
    parser.add_argument('--params', nargs='*', default=[],
                        help='''Parameters to override default values/other values given in experiment file.
                        Supply it in the form parameter1=value1 parameters2=value2, ...''')
    parser.add_argument('--processes', type=int, default=None,
                        help='''Run in parallel with several  processes.
                        Otherwise run sequentially (default)''')
    parser.add_argument('--startid', type=int,
                        help='''Start with experiment at specified id....''')
    parser.add_argument('--stopid', type=int,
                        help='''Stop with experiment at specified id....''')
    parser.add_argument('--transfer', action="store_true",
                        help='''Train on all datasets except one,
                        then use trained model and retrain on left-out dataset''')
    parser.add_argument('--first5sets', action="store_true", dest="first_five_sets",
                        help='''Use only first 5 datasets.''')
    args = parser.parse_args()
    
    # dictionary values are given with = inbetween, parse them here by hand
    param_dict =  dict([param_and_value.split('=') 
                        for param_and_value in args.params])
    args.params = param_dict
    # transfer learning always implies early stop(?) 
    # TODELAY: check if this is true/necessary when refactoring
    if args.transfer:
        args.earlystop = True
    if (args.startid is  not None):
        args.startid = args.startid - 1 # model ids printed are 1-based, python is zerobased
    if (args.stopid is  not None):
        args.stopid = args.stopid - 1 # model ids printed are 1-based, python is zerobased
    # If we are running on gpu typically we should not run in parallel...
    assert not (theano.config.device.startswith('gpu') and args.processes)
    return args

def setup_logging():
    """ Set up a root logger so that other modules can use logging
    Adapted from scripts/train.py from pylearn"""
        
    root_logger = logging.getLogger()
    prefix = '%(asctime)s '
    formatter = CustomFormatter(prefix=prefix, only_from='pylearn2')
    handler = CustomStreamHandler(formatter=formatter)
    root_logger.addHandler(handler)
    root_logger.setLevel(logging.INFO)

def main():
    args = parse_command_line_arguments()
    setup_logging()
    experiments_runner = ExperimentsRunner(
         args.experiments_file_name, args.template_file_name,
          cross_validation = args.cv, test=args.test,
          start_id = args.startid,
          stop_id = args.stopid,
          quiet=args.quiet,
          dry_run=args.dryrun, parallel = (args.processes is not None),
          early_stop=args.earlystop,
          transfer_learning =args.transfer,
          command_line_params = args.params,
          processes=args.processes,
          first_five_sets=args.first_five_sets)
    experiments_runner.run()
    
if __name__ == "__main__":
    main()
