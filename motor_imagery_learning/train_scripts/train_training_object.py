#!/usr/bin/env python
# I don't know how to properly import python packages, so I do it like this ;)
# http://stackoverflow.com/a/9806045/1469195
import os
import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
os.sys.path.insert(0,parentdir) 
import argparse
import logging
from train_with_params import create_training_object_from_file

def parse_command_line_arguments():
    parser = argparse.ArgumentParser(
        description="""Launch an experiment from a YAML experiment file.
        Example: ./train_experiments.py yaml-scripts/experiments.yaml """
    )
    parser.add_argument('training_file_name', action='store',
                        choices=None,
                        help='A YAML configuration file with a '
                             'training_object')
    parser.add_argument('--quiet', action="store_true",
        help="Run algorithm quietly without progress output")
    args = parser.parse_args()
    
    return args       

def setup_logging(quiet):
    if (quiet):
        logging.getLogger("pylearn2").setLevel(logging.WARNING)

def train_training_object_from_file(file_name):
    training_obj = create_training_object_from_file(file_name)
    training_obj.main_loop()
def main():
    args = parse_command_line_arguments()
    setup_logging(args.quiet)
    train_training_object_from_file(args.training_file_name)

if __name__ == "__main__":
    main()
