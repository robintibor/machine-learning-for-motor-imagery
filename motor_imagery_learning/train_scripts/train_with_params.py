#!/usr/bin/env python
from pylearn2.config import yaml_parse
from string import Template
import pylearn2.train
import logging
import re
import motor_imagery_learning.mypylearn2.train

def process_yaml_file(filename, command_line_train_parameters, 
                      parameter_list_name, cross_validation, early_stop,
                      transfer_learning):   
    # First read entire file, then process it
    train_and_parameter_string = open(filename, 'r').read()
    return process_yaml_string(train_and_parameter_string,
        command_line_train_parameters, parameter_list_name,
        cross_validation, early_stop, transfer_learning)

def process_yaml_string(train_template_string, command_line_train_parameters, 
                      parameter_list_name, cross_validation, early_stop,
                      transfer_learning):
    """ Replace parameters in train template string by given parameters or
    by (default) parameters in parameter from given parameter list
    """
    # Extract default parameters at start of file
    train_string, parameters = extract_yaml_parameters(train_template_string,
         parameter_list_name)
    # Merge default parameters with user parameters 
    # (user can override parameters)
    parameters.update(command_line_train_parameters)
    parameters = adjust_for_training_type(parameters, 
        cross_validation, early_stop, transfer_learning)
    parsed_train_string = process_parameters_in_string(train_string, parameters)
    return parsed_train_string

def adjust_for_training_type(parameters, 
        cross_validation, early_stop, transfer_learning):
    # Adjust dataset limits and training types
    if (cross_validation and not transfer_learning):
        if (early_stop):
            parameters['training_type'] = '*training_cross_validation_early_stop'
        else:
            parameters['training_type'] = '*training_cross_validation'
            
        # replace training start and training stop with
        # cross validation start and stop
        # and unset training_limits
        # necessary because we always use same dataset for both types of training
        # (otherwise we would have to load it twice)
        parameters['training_start'] = parameters['cross_validation_start']
        parameters['training_stop'] = parameters['cross_validation_stop']
        # preprocessors should be applied during cross validation
        # or directly on datasets
    if (transfer_learning):
        # TODO: fix transfer learning as this will not stop the loading
        # now anymore...
        parameters['training_start'] = 0
        parameters['training_stop'] =  1
        # preprocess in  transfer train object, not before
        parameters['preprocessor'] = None
        parameters['training_type'] = '*training_transfer'
        # don't load datasets, loading happens inside transfer train
        parameters['transfer_filenames'] = parameters['dataset_filename']
        parameters['dataset_filename'] = None
        
    parameters.pop('cross_validation_start')
    parameters.pop('cross_validation_stop')
    return parameters

def extract_yaml_parameter_list(filename, listname):
    train_and_parameter_string = open(filename, 'r').read()
    train_string, parameters = extract_yaml_parameters(
        train_and_parameter_string, listname)
    return train_string, parameters

def extract_yaml_parameters(train_and_parameter_string,
                               parameter_list_name):
    end_of_parameters = train_and_parameter_string.index('#ENDPARAMETERS')
    parameters_string = train_and_parameter_string[0:end_of_parameters]
    train_string = train_and_parameter_string[end_of_parameters + \
                                              len('#ENDPARAMETERS'):]
    parameters = yaml_parse.load(parameters_string)
    parameters = parameters[parameter_list_name]
    return train_string, parameters

def process_parameters_in_string(train_string, parameters):
    # replace any none values in parameters with yaml code for none
    for key, value in parameters.items():
        if value is None:
            parameters[key] = "!!python/none ''"
    # Fill in values for templates in string
    # Check that all parameters exist
    for key in parameters.keys():
        parameter_exists = False
        for allowed_suffix in [" ", "}", ",", "]", "\n"]:
            substituted_variable = "$" + key + allowed_suffix;
            if substituted_variable in train_string:
                parameter_exists = True
                break
        assert parameter_exists,\
            "every parameter should be a substitution for an existing" + \
            " variable, $" + key + " not found."
    parsed_train_string = Template(train_string).substitute(parameters)
    return parsed_train_string

def run_training(yaml_training_string, quiet):
    train = create_training_object(yaml_training_string)
    if (quiet):
        logging.getLogger('pylearn2.monitor').setLevel(logging.WARNING)
        logging.getLogger('pylearn2.train').setLevel(logging.WARNING)
        logging.getLogger('pylearn2.models.mlp').setLevel(logging.ERROR)
        logging.getLogger('pylearn2.models.model').setLevel(logging.ERROR)
    train.main_loop()
    return train

def create_training_object_from_file(filename):
    train_string = open(filename).read()
    return create_training_object(train_string)

def create_training_object(yaml_training_string):
    train_dict = yaml_parse.load(yaml_training_string)
    training_obj =  train_dict['training_object']
    datasets= train_dict['datasets']
    # add complete (train/valid/testset) yaml src only for train objects
    # cross validation everything is in train anyways...
    # TODELAY: do sth for transfer also
    if (isinstance(training_obj, pylearn2.train.Train) or 
        isinstance(training_obj, motor_imagery_learning.mypylearn2.train.Train)):
        add_yaml_src_to_model(training_obj.model, datasets)
    return training_obj

def add_yaml_src_to_model(model, datasets):
    train_dataset_yaml_src = datasets[0]['train_dataset'].yaml_src
    valid_dataset_yaml_src = datasets[1]['validation_dataset'].yaml_src
    test_dataset_yaml_src = datasets[2]['test_dataset'].yaml_src
    yaml_src = \
        "[train: {:s}, valid: {:s}, test: {:s}]".format(
            train_dataset_yaml_src, valid_dataset_yaml_src, 
            test_dataset_yaml_src)
    # have to format for preprocessor to work correctly
    # Setting anchor for train preprocessor
    # and using that preprocssor object also on valid and test
    # this way preprocessor object
    # can learn from train and apply on valid and test
    yaml_src = re.sub(r'(train:.*?preprocessor:)', r'\1 &preprocessor', 
        yaml_src, flags=re.DOTALL)
    yaml_src = re.sub(r'(valid:.*?preprocessor:).*?(fit_preprocessor)', r'\1 *preprocessor,\n \2',
        yaml_src, flags=re.DOTALL)
    
    yaml_src = re.sub(r'(test:.*?preprocessor:).*?(fit_preprocessor)', r'\1 *preprocessor,\n \2',
        yaml_src, flags=re.DOTALL)
    model.all_datasets_yaml_src = yaml_src
