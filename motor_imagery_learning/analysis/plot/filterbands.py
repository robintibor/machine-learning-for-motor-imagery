from matplotlib import pyplot
import numpy as np
from motor_imagery_learning.analysis.filterbands import (
    sum_abs_filterband_weights, compute_center_freqs)
def show_freq_importances(center_freqs, sum_abs_weight):
    pyplot.bar(center_freqs, sum_abs_weight, align='center')
    pyplot.xticks(np.array(center_freqs[:16:2].tolist() + center_freqs[16:].tolist()).astype(np.int))


def plot_filterband_weights_for_train_state(train_state):
    filename = train_state.info_parameters['dataset_filename']
    nice_filename = filename.split("/")[-1][:4]
    center_freqs = compute_center_freqs(train_state.model) # shd be same always, calculate just for check..
    sum_abs_weights = sum_abs_filterband_weights(train_state.model, center_freqs)
    pyplot.figure()
    show_freq_importances(center_freqs, sum_abs_weights)
    pyplot.title(nice_filename, fontsize=18)