from motor_imagery_learning.analysis.plot_util import plot_sensor_signals
import numpy as np
from matplotlib import pyplot


def plot_sensor_signals_two_classes(class_1_signals, class_2_signals, sensor_names=None, 
                                    class_1_name='1', class_2_name='2', figsize=(8,2),
                                    xvals=None, plot_divide_line=True):
    both_classes = np.concatenate((class_1_signals, class_2_signals))
    if sensor_names is None:
        sensor_names = map(str, range(1,len(class_1_signals) + 1))
    both_sensor_names = sensor_names + sensor_names
    fig = plot_sensor_signals(both_classes, both_sensor_names, figsize=figsize, xvals=xvals)
    pyplot.text(0.025,0.7,class_1_name,transform=fig.transFigure, fontsize=12, horizontalalignment='right')
    pyplot.text(0.025,0.3,class_2_name,transform=fig.transFigure, fontsize=12, horizontalalignment='right')

    if plot_divide_line == True:
        _ = pyplot.plot([0.125, 0.9], [0.51,0.51], color='grey',
            lw=1,transform=fig.transFigure, clip_on=False, linestyle="-")
    return fig




def plot_sensor_signals_multiple_classes(class_signals, sensor_names=None, 
                                    class_names=None, figsize=(8,2),
                                    xvals=None, plot_divide_line=True):
    n_classes = len(class_signals)
    n_sensors = len(class_signals[0])
    signals_reshaped = class_signals.reshape(
        class_signals.shape[0] * class_signals.shape[1],
        *class_signals.shape[2:])
    if sensor_names is None:
        sensor_names = map(str, xrange(1, n_sensors + 1))
    all_sensor_names = np.tile(sensor_names, n_classes)
    fig = plot_sensor_signals(signals_reshaped, all_sensor_names, 
                              figsize=figsize, xvals=xvals)
    if class_names is None:
        class_names = [str(i) for i in xrange(n_classes)]
    # make class texts for example for 3 classes at 1/4, 2/4, 3/4
    y_text = np.linspace(0,1,n_classes+2)[1:-1][::-1]
    for i_class in xrange(n_classes):
            pyplot.text(0.0,y_text[i_class],class_names[i_class],
                        transform=fig.transFigure, fontsize=12, 
                        horizontalalignment='right')


    if plot_divide_line == True:
        for i_ax in range(n_sensors - 1, n_sensors*(n_classes - 1), n_sensors):
            ax = fig.axes[i_ax]
            ymin, ymax = ax.get_ylim()
            ax.axhline(ymin, color='black', linewidth=4)
    return fig
