import numpy as np
from copy import deepcopy
from numpy.random import RandomState
import theano
import theano.tensor as T
from motor_imagery_learning.mypylearn2.unsmooth_cost import conv2d

def power_smoothness_cost(inputs):
    inputs_squared = T.sqr(inputs)
    return T.sum(T.sqr(inputs_squared[:,:,1:,:] - inputs_squared[:,:,:-1,:])) 

def power_smoothness_cost_scaled(inputs):
    inputs_squared = T.sqr(inputs)
    return T.sum(T.sqr(inputs_squared[:,:,1:,:] - inputs_squared[:,:,:-1,:])) / T.sum(T.sqr(inputs_squared[:,:,1:-1,:]))

def power_smoothness_cost_moving_avg(inputs, avg_num=50):
    average = power_mov_avg(inputs, avg_num=avg_num)
    return T.sum(T.sqr(average[:,:,1:,:] - average[:,:,:-1,:]))# * 0.01

def power_mov_avg(inputs, avg_num=200, subsample=50):
    """ at the moment not used:: first does a moving average,
    afterwards  a subsampling (you could also do block avg after, would probably
    be even more correct) """
    inputs_squared = T.sqr(inputs)
    filters = T.constant(np.ones(avg_num) / avg_num, dtype='floatX').dimshuffle('x', 'x', 0, 'x')
    average = conv2d(inputs_squared, filters, filter_stride=(1,1))[:,:,::subsample,:]
    return average

def power_block_avg(inputs, avg_num=50):
    inputs_squared = T.sqr(inputs)
    # expect last dimension to be empty anyways..
    inputs_cut = inputs_squared[:,:,:inputs_squared.shape[2] - (inputs_squared.shape[2] % avg_num),:]
    inputs_reshaped = inputs_cut.reshape((inputs_cut.shape[0], inputs_cut.shape[1], avg_num, -1))
    average = T.mean(inputs_reshaped, axis=3, keepdims=True)
    return average
    
def power_smoothness_cost_moving_avg_scaled(inputs, avg_num=50):
    average = power_block_avg(inputs, avg_num=avg_num)#, subsample=subsample)
    return T.sum(T.sqr(average[:,:,1:,:] - average[:,:,:-1,:])) / T.sum(T.sqr(average))#[:,:,1:-1,:])

class AdamNumpy():
    def __init__(self, alpha=2e-4, beta1=1e-1, beta2=1e-3, epsilon=10e-8):
        self.__dict__.update(locals())
        del self.self
        self._initialized = False

    def setup(self, first_grad):
        self.grad = first_grad * 0
        self.sq_grad = first_grad * 0
        self.time = 1
        self._initialized = True

    def update(self, cur_input, grad):
        """ Get updates according to the adam algorithm.
        Ignores sgd learning rate!"""

        if not self._initialized:
            self.setup(grad)
            
        new_grad = self.beta1 * grad + (1.0 - self.beta1) * self.grad
        new_sq_grad = self.beta2 * np.square(grad) + (1.0 - self.beta2) * self.sq_grad
        
        # return grad is what will be subtracted in update
        new_input = cur_input - (self.alpha * np.sqrt(1.0 - (1.0 - self.beta2)**self.time) * \
                    (1.0 - (1.0 - self.beta1)**self.time)**(-1.0)) * \
                    (new_grad / (np.sqrt(new_sq_grad) + self.epsilon))
        new_input = new_input.astype(np.float32)
        self.grad = new_grad
        self.sq_grad = new_sq_grad
        self.time = self.time + 1
        return new_input

def reconstruct_inputs_for_class(model, wanted_outputs, 
        num_batches=3, epochs=100, lrate=0.1, decay=0.99, learning_rule=None,
        input_cost_func=None):
    in_grad_func = create_input_grad_for_wanted_class_func(model, 
        input_cost_func=input_cost_func)
    start_in = create_rand_input(model, num_batches)
    descent_args = dict(lrate=lrate, decay=decay, epochs=epochs, 
        learning_rule=learning_rule)
    inputs_class = gradient_descent_input(start_in, in_grad_func, wanted_outputs,
        **descent_args)
    return inputs_class

def create_rand_input(model, batches):
    rng = RandomState(np.uint32(hash('randinput')))
    return rng.randn(batches, model.input_space.num_channels, 
        *model.input_space.shape).astype(np.float32)

def gradient_descent_input(start_in, in_grad_func, wanted_output, 
    lrate=10.0, decay=0.99, epochs=100, learning_rule=None):
    wanted_output = np.array(wanted_output).astype(np.float32)
    all_inputs = []
    cur_input = start_in
    all_inputs.append(deepcopy(cur_input))
    for _ in range(epochs):
        grad = in_grad_func(cur_input, wanted_output)
        if learning_rule is not None:
            grad += ((1 - decay) * cur_input)
            cur_input = learning_rule.update(cur_input, grad)
        else:
            cur_input = cur_input - lrate * grad - ((1 - decay) * cur_input)
        
        all_inputs.append(deepcopy(cur_input))
    return np.array(all_inputs)

def create_input_grad_for_wanted_class_func(model, input_cost_func=None):
    inputs = T.ftensor4()
    wanted_output = T.fvector()
    cost_wanted_class = compute_softmax_cost(model, inputs, wanted_output)
    if input_cost_func is None:
        cost_inputs = np.float32(0)
    else: 
        cost_inputs = input_cost_func(inputs)
    overall_cost = cost_wanted_class + cost_inputs
    grad_wanted_class = T.grad(overall_cost, inputs)
    grad_func = theano.function([inputs, wanted_output], grad_wanted_class)
    return grad_func

def compute_softmax_cost(model, inputs, wanted_output):
    class_out = model.fprop(inputs)
    # last layer is assumed to be softmax...
    cost_wanted_class = model.layers[-1].cost(wanted_output.dimshuffle('x', 0), class_out)
    return cost_wanted_class

def create_input_grad_with_smooth_penalty_for_wanted_class_func(model, smooth_weight):
    return create_input_grad_for_wanted_class_func(model, 
        input_cost_func=lambda inputs: unsmooth_costs(inputs,smooth_weight))

def unsmooth_costs(inputs, smooth_weight):
    return T.sum(T.square(inputs[:,:,1:,:] - inputs[:,:,:-1,:])) * smooth_weight