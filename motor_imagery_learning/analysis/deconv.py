import numpy as np
from scipy.interpolate import interp1d
from motor_imagery_learning.analysis.util import totopoview, bc01toinput
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import RectifiedLinear, ConvRectifiedLinear

def deconv_model(wanted_out_before_softmax, model, return_all=False):
    all_wanted_ins = []
    wanted_softmax_in = softmax_deconv(wanted_out_before_softmax, model.layers[-1])
    all_wanted_ins.append(wanted_softmax_in)
    ## remove values below zero
    #wanted_softmax_in =  wanted_softmax_in * np.float32(wanted_softmax_in > 0.)
    #undo square and log
    wanted_softmax_in =np.sqrt(np.exp(wanted_softmax_in))
    wanted_softmax_in = wanted_softmax_in - np.min(wanted_softmax_in)
    all_wanted_ins.append(wanted_softmax_in)

    wanted_conv_out = pool_layer_deconv(wanted_softmax_in, model.layers[-2], 
                                        model.layers[0].kernel_shape[0])
    all_wanted_ins.append(wanted_conv_out)

    wanted_conv_in = conv_layer_back_deconv(model.layers[-3], wanted_conv_out)

    if len(model.layers) > 3:
        assert len(model.layers) == 4
        wanted_conv_out = np.sum(wanted_conv_in, axis=0)
        all_wanted_ins.append(wanted_conv_out)
        wanted_conv_in = conv_layer_back_deconv(model.layers[0], wanted_conv_out)
    
    wanted_in = np.sum(wanted_conv_in, axis=0).squeeze().T
    all_wanted_ins.append(wanted_in)
    if (return_all):
        return all_wanted_ins[::-1] # reverse to have a bottomup layer order
    else:
        return wanted_in

def pool_layer_deconv(wanted_pool_out, pool_layer, kernel_0_length):
    return pool_deconv(wanted_pool_out, pool_layer.pool_shape,
                       pool_layer.pool_stride, kernel_0_length)

def pool_deconv(wanted_pool_out, pool_shape, pool_stride, kernel_0_length): 
    assert pool_shape[1] == 1
    assert pool_stride[1] == 1 
    number_pools = wanted_pool_out.shape[0]
    pool_length = pool_shape[0]
    pool_stride = pool_stride[0]
    pool_input_length = (number_pools - 1) * pool_stride + pool_length
    pool_centers = range(pool_length/2, pool_input_length - pool_length/2 + 1, pool_stride)

    # try to make the 0-axis time input like this:
    # wanted_pool_in_0, 0,0,0,(kernel_0_length time 0s), wanted_pool_in_1, 0,0,0,(kernel_0_length time 0s),...
    
    # for this interpolate pool values across whole input length
    # take center of pool regions as interpolation points (and wanted pool vals as interpolation values)
    # plus add start and end of input as interpolation points, there reuse first and last wanted pool vals
    interpolate_xs = np.concatenate(([0], pool_centers, [pool_input_length]))
    pool_vals = np.concatenate((wanted_pool_out[0:1], wanted_pool_out, wanted_pool_out[-1:]))
    interp_pool = interp1d(interpolate_xs,pool_vals, axis=0)
    
    # now sample always moving ahead one kernel length, fill rest with zeros
    input_xs = range(0,pool_input_length, kernel_0_length) + [pool_input_length - 1]

    interp_values = np.array([interp_pool(sample_i) for sample_i in input_xs])
    wanted_conv_out = np.zeros((pool_input_length+1, wanted_pool_out.shape[1], wanted_pool_out.shape[2]))
    wanted_conv_out[input_xs] = interp_values # 01c
    return wanted_conv_out

def conv_layer_back_deconv(conv_layer, expected_linear_outputs):
        #if (conv_layer.get_input_space().axes == ('b', 'c', 0, 1)):
        #    expected_outputs = expected_outputs - conv_layer.get_biases().transpose(1,0,2)
        return conv_back_deconv(conv_layer.get_weights_topo(), conv_layer.get_input_space(),
                               expected_linear_outputs)
    
def conv_back_deconv(conv_weights, input_space, expected_linear_outputs):
    """conv_weights in b01c and expected linear outputs format"""
    time_bins = input_space.shape[0]
    freq_bins = input_space.shape[1]
    num_chans = input_space.num_channels
    # flip time and freq dim, since we did convolution
    conv_weights = conv_weights[:, ::-1, ::-1, :]
    num_filters = conv_weights.shape[0]
    filt_time_bins = conv_weights.shape[1]
    filt_freq_bins = conv_weights.shape[2]
    filt_chans = conv_weights.shape[3]
    assert filt_chans == num_chans # always convolving over all chans
    expected_linear_input = np.zeros((num_filters, time_bins, freq_bins, num_chans))
    # could collect filterwise inputs.. and also return them as second argument...
    # to show variablity how class can be shown...
    assert(conv_weights.shape[3] == num_chans) # always convolve over all channels so far
    for filt_i in xrange(0, num_filters):
        # make sure not to run out of time shape.... can happen in case valid pool/conv didnt use all input
        for time_bin in xrange(0, min(time_bins - filt_time_bins + 1, 
            expected_linear_outputs.shape[0])):
            for freq_bin in xrange(0, freq_bins - filt_freq_bins + 1):
                    # expected output should be scalar
                    expected_output = expected_linear_outputs[time_bin,freq_bin, filt_i]
                    # only use filter if positive output is expected here/not equal 0 (just saves computation :))
                    #if (expected_output == 0): continue
                    #else: assert expected_output > 0
                    cur_filter = conv_weights[filt_i]
                    # Lets multiply expected output with weights!
                    expected_input = expected_output * cur_filter # or divided by weights? :)
                    expected_linear_input[filt_i, time_bin:time_bin + filt_time_bins, \
                                          freq_bin:freq_bin + filt_freq_bins, :] += expected_input
    return expected_linear_input


def softmax_deconv(wanted_out_before_softmax, softmax_layer):
    # add empty dims to class weights
    class_weights = np.array(wanted_out_before_softmax)[:,np.newaxis,np.newaxis,np.newaxis]
    # multiply weights for each class with softmax layer
    # sum along classes
    wanted_softmax_in = np.sum(softmax_layer.get_weights_topo()  * class_weights, axis=0)
    return wanted_softmax_in

def back_deconv(layers, wanted_linear_outputs):
    deconver = Deconv(layers, wanted_linear_outputs)
    return deconver.back_deconv()

def back_prop(layers, wanted_linear_outputs, inputs):
    deconver = Deconv(layers, wanted_linear_outputs)
    return deconver.back_prop(inputs)

def back_guided_deconv(layers, wanted_linear_outputs, inputs):
    deconver = Deconv(layers, wanted_linear_outputs)
    return deconver.back_guided_deconv(inputs)

def back_linear(layers, wanted_linear_outputs):
    deconver = Deconv(layers, wanted_linear_outputs)
    return deconver.back_linear()   

def back_guided_deconv_for_class(model, class_nr, inputs, view_converter):
    wanted = [-1] * 4
    wanted[class_nr] = 1
    wanted_input = back_guided_deconv(model.layers, wanted, bc01toinput(model, np.array([inputs]))[0])
    if (wanted_input.ndim == 1):
        wanted_input = totopoview(wanted_input, view_converter)
    return wanted_input

class Deconv:
    """ Class for the different variants of deconv...
    Hopefully heplful for visualizing what model learned and
    also how it reacts to different inputs"""
    def __init__(self, layers, wanted_linear_outputs):
        self.layers = layers
        self.wanted_linear_outputs = wanted_linear_outputs
        self.activations = None

    def conv_layer_back_deconv(self, conv_layer, expected_linear_outputs):
        #if (conv_layer.get_input_space().axes == ('b', 'c', 0, 1)):
        #    expected_outputs = expected_outputs - conv_layer.get_biases().transpose(1,0,2)
        
        input_space = conv_layer.get_input_space()
        time_bins = input_space.shape[0]
        freq_bins = input_space.shape[1]
        num_chans = input_space.num_channels
        conv_weights = conv_layer.get_weights_topo() # b 0 1 c
        # flip time and freq dim, since we did convolution
        conv_weights = conv_weights[:, ::-1, ::-1, :]
        num_filters = conv_weights.shape[0]
        filt_time_bins = conv_weights.shape[1]
        filt_freq_bins = conv_weights.shape[2]
        filt_chans = conv_weights.shape[3]
        assert filt_chans == num_chans # always convolving over all chans
        expected_linear_input = np.zeros((num_filters, time_bins, freq_bins, num_chans))
        # could collect filterwise inputs.. and also return them as second argument...
        # to show variablity how class can be shown...
        assert(conv_weights.shape[3] == num_chans) # always convolve over all channels so far
        for filt_i in xrange(0, num_filters):
            for time_bin in xrange(0, time_bins - filt_time_bins + 1):
                for freq_bin in xrange(0, freq_bins - filt_freq_bins + 1):
                        # expected output should be scalar
                        expected_output = expected_linear_outputs[time_bin,freq_bin, filt_i]
                        # only use filter if positive output is expected here/not equal 0 (just saves computation :))
                        #if (expected_output == 0): continue
                        #else: assert expected_output > 0
                        cur_filter = conv_weights[filt_i]
                        # Lets multiply expected output with weights!
                        expected_input = expected_output * cur_filter # or divided by weights? :)
                        expected_linear_input[filt_i, time_bin:time_bin + filt_time_bins, \
                                              freq_bin:freq_bin + filt_freq_bins, :] += expected_input
        return expected_linear_input
    
    def flat_layer_back_deconv(self, flat_layer, expected_linear_outputs):
        #expected_linear_outputs = expected_linear_outputs - flat_layer.get_biases()
        flat_weights = flat_layer.get_weights_topo()    
        expected_inputs = expected_linear_outputs * flat_weights.T
        # now 0c1b #timebins x#chans x#freqbins x #batches/neurons
        #  TODO: instead of transpose. do .T and then same as in back-devonc function (Conv2dSpace.convert...)
        # should give same reslt and then we would have consistent methods
        # change to bo1c =>#neuronsx #timex#freq#chan
        expected_inputs = np.transpose(expected_inputs, (3,0,2,1))
        return expected_inputs
    
    
    def back_linear(self, output_prune_function = lambda out,activations:out):
        """ Backprop linearly (ignore activation function/gradient).
        To use gradient or sth else to modify expected outputs for layer below,
        specify a output prune function"""
        last_layer = self.layers[-1]
        weights_in_topo_format = isinstance(last_layer.input_space, Conv2DSpace)
        last_weights = None
        if weights_in_topo_format:
            last_weights = last_layer.get_weights_topo()
        else:
            last_weights = last_layer.get_weights()
        # have to hackily change axes here in case of LinaerLayer... 
        # logically it makes no sense as we want b 0 1 c and not the input space
        # but somehow weights are not formatted right before.... so we undo
        # the transformation which caused the problem in the source code
        # W = Conv2DSpace.convert(W, self.input_space.axes, ('b', 0, 1, 'c')) in pylearn mlp.py code
        # get_weighs_topo function of LinearLayer
        if (isinstance(last_layer, RectifiedLinear) and weights_in_topo_format):      
            last_weights = Conv2DSpace.convert(last_weights, ('b', 0, 1, 'c'), last_layer.input_space.axes)
        # lets get linear output derived on inputs
        #ignoring bias is this ok? you could think aboutt using bias of wanted class (only adding this one or sth)
        
        #self.wanted_linear_outputs = self.wanted_linear_outputs - last_layer.get_biases()
        if (weights_in_topo_format):
            expected_inputs = self.wanted_linear_outputs * last_weights.T
        else:
            expected_inputs = self.wanted_linear_outputs * last_weights
        expected_inputs = expected_inputs.T
    
        # go backwards through layers
        for layer_i in xrange(len(self.layers) - 2,-1,-1):
            layer = self.layers[layer_i]
            # Sum expected outputs over all neurons in deeper layer
            expected_outputs = np.sum(expected_inputs, axis=0)
            activations = None if self.activations is None else self.activations[layer_i]
            expected_outputs = output_prune_function(expected_outputs, activations)
            if (isinstance(layer, RectifiedLinear)):
                expected_inputs = self.flat_layer_back_deconv(layer, expected_outputs)
            elif (isinstance(layer, ConvRectifiedLinear)):
                expected_inputs = self.conv_layer_back_deconv(layer, expected_outputs)            
            else:
                raise("unknown layer type for deconv") 
            #expected_inputs = expected_inputs - layer.get_biases()
        # average over all filters/neurons
        # filters are on axis 1...
        wanted_input =  np.sum(expected_inputs, axis=0)
        # transpose to #chans x #timebins x #freqbins
        if (isinstance(self.layers[0].input_space, Conv2DSpace)):
            wanted_input = np.transpose(wanted_input, (2, 0, 1))
        return wanted_input
    
    def fprop_remember_activations(self, inputs):
        self.activations = []
        output_below = [inputs]
        #self.activations.append(output_below)
        for layer in self.layers:
            # forward prop, first createsymbolic forward prop from theano
            # then do actual forwardprop with output from layer below
            X = layer.get_input_space().make_theano_batch()
            Y = layer.fprop(X)
            fprop = theano.function([X], Y)
            output_below = fprop(output_below)
            # reshape activation to 0 1 c if necessary
            activation = output_below
            # (1, 32, 15, 1)
            if (activation.ndim == 4):
                activation = Conv2DSpace.convert(activation, ('b', 'c', 0, 1), 
                    ('b', 0, 1, 'c'))
            activation = np.squeeze(activation)
            self.activations.append(activation)
        
    def back_prop(self, inputs):
        self.fprop_remember_activations(inputs)
        def remove_zero_activations(expected_outputs, activations):
            return expected_outputs * np.float32(activations > 0)
        return self.back_linear(remove_zero_activations)
    
    def back_deconv(self):
        def remove_zero_outputs(expected_outputs, activations):
            return expected_outputs * np.float32(expected_outputs > 0)
        return self.back_linear(remove_zero_outputs)
    
    def back_guided_deconv(self, inputs):
        self.fprop_remember_activations(inputs)
        def remove_zero_outputs_and_activations(expected_outputs, activations):
            return expected_outputs * np.float32(expected_outputs > 0) * np.float32(activations > 0)
        return self.back_linear(remove_zero_outputs_and_activations)
        
