from motor_imagery_learning.csp.train_csp import generate_filterbank
import numpy as np

def sum_abs_filterband_weights(model, center_freqs, virtual_chan=None):
    soft_weights = model.layers[1].get_weights_topo()
    if virtual_chan is None:
        sum_abs_weight = np.sum(np.abs(soft_weights), axis=(0,1,3))
    else:
        sum_abs_weight = np.sum(np.abs(soft_weights[:,:,:,virtual_chan]), axis=(0,1))
    return sum_abs_weight

def compute_center_freqs(model): 
    soft_weights = model.layers[1].get_weights_topo()
    if (soft_weights.shape[2] == 13):
        filterbank = generate_filterbank(min_freq=2, max_freq=50, last_low_freq=50, low_width=4, high_width=4)
    elif(soft_weights.shape[2] == 35):
        filterbank = generate_filterbank(min_freq=0, max_freq=142, last_low_freq=30, low_width=2, high_width=6)
    else:
        raise NotImplementedError("unknown filterbank")
    center_freqs = np.mean(filterbank, axis=1)
    return center_freqs

def compute_sum_abs_weights_for_csp_model(csp_model, center_freqs):
    fold_i = csp_model.filterbank_csp.clf.shape[0] -1 # use last fold
    for class_pair_i in range(0,6):
        single_clf = csp_model.filterbank_csp.clf[fold_i, class_pair_i]
        filterband_weights = single_clf[0].reshape(
            len(center_freqs), -1)

        sum_weight = np.sum(np.abs(filterband_weights), axis=1)
    return sum_weight