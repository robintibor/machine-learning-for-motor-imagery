import h5py
import numpy as np
import theano
from pylearn2.space import Conv2DSpace
from pylearn2.models.mlp import RectifiedLinear, ConvRectifiedLinear
from sklearn.cross_validation import KFold
from motor_imagery_learning.motor_eeg_dataset import (FFTMotorEEGDataset,
    RawMotorEEGDataset)
from pylearn2.config import yaml_parse
from sensor_positions import get_C_sensors
from motor_imagery_learning.mypylearn2.learning_rule_adam import Adam
from pylearn2.costs.mlp.dropout import Dropout
from motor_imagery_learning.mypylearn2.dataset_splitters import DatasetSingleFoldSplitter

from pylearn2.models.mlp import Softmax, MLP
from pylearn2.termination_criteria import EpochCounter
from sklearn.cross_validation import KFold
from motor_imagery_learning.mypylearn2.sgd import SimilarBatchSizeSGD
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
from numpy.random import RandomState

def mad(arr, axis=None):
    """ Median Absolute Deviation: a "Robust" version of standard deviation.
        Indices variabililty of the sample.
        https://en.wikipedia.org/wiki/Median_absolute_deviation 
        Adapted from http://stackoverflow.com/a/23535934/1469195
    """
    med = np.median(arr, axis=axis)
    return np.median(np.abs(arr - med), axis=axis)

def bps_and_freqs(weights, axis=1, sampling_rate = 150.0):
    bps = np.abs(np.fft.rfft(weights, axis=axis))
    freq_bins = np.fft.rfftfreq(weights.shape[axis], d=1.0/sampling_rate)
    return bps, freq_bins

def create_trainer(topo_view, y, layers=None, cost="dropout", max_epochs=1000, preprocessor=None):
    from motor_imagery_learning.mypylearn2.train import TrainEarlyStop
    if layers is None:
        layers = [Softmax(n_classes=2, layer_name='y', irange=0.001)]
    max_epochs_crit = EpochCounter(max_epochs=max_epochs)
    if cost == "dropout":
        cost = Dropout(input_include_probs={layers[0].layer_name: 0.8}, 
                       input_scales={layers[0].layer_name: 1/0.8})
    algorithm = SimilarBatchSizeSGD(learning_rate=0.1, batch_size=60,seed=np.uint64(hash('test_train_sgd')),
                                    termination_criterion=max_epochs_crit, learning_rule=Adam(),
                                   cost=cost)
    mlp = MLP(layers=layers, seed= np.uint64(hash('test_train_mlp')))
    dataset = DenseDesignMatrix(topo_view=topo_view, y=y, 
                                rng=RandomState(np.uint64(hash('batchhash?'))),
                                                         axes=('b', 'c', 0, 1))
    
    
    dataset_splitter = DatasetSingleFoldSplitter(dataset, num_folds=10, test_fold_nr=-1)
    trainer = TrainEarlyStop(dataset_splitter, mlp, algorithm=algorithm, save_path=None,
         preprocessor=preprocessor, 
                             keep_best_lrule_params=False, 
                             best_model_channel='valid_y_misclass', fraction_epochs_after_stop=1.0)
    return trainer

def check_prediction_correctness(train_state, fold_nr=None, 
    preprocessor=None):
    """ Check whether the predictions we compute match the last values stored in the monitor"""
    datasets = load_preprocessed_datasets(train_state, fold_nr=fold_nr, 
        preprocessor=None)
    for key in datasets:
        expected_accuracy = 1 - train_state.model.monitor.channels[key + '_y_misclass'].val_record[-1]
        dataset = datasets[key]
        preds = reshape_and_get_predictions(train_state.model,
            dataset.get_topological_view(),
            axes=dataset.view_converter.axes)
        predicted_labels = np.argmax(preds, axis=1)
        actual_labels = np.argmax(dataset.y, axis=1)
        accuracy = np.sum(np.equal(predicted_labels, actual_labels)) / float(len(actual_labels))
        assert np.isclose(accuracy, expected_accuracy), \
            "all accuracies should be same, {:s} accuracy is different, expected: {:f}, actual: {:f}".format(
            key, expected_accuracy, accuracy)
        print("{:5s} {:6.2f}% ok".format(key, accuracy * 100))
    print("all ok")

def check_prediction_correctness_for_datasets(train_state, datasets):
    for key in datasets:
        expected_accuracy = 1 - train_state.model.monitor.channels[key + '_y_misclass'].val_record[-1]
        dataset = datasets[key]
        preds = reshape_and_get_predictions(train_state.model,
            dataset.get_topological_view(),
            axes=dataset.view_converter.axes)
        predicted_labels = np.argmax(preds, axis=1)
        actual_labels = np.argmax(dataset.y, axis=1)
        accuracy = np.sum(np.equal(predicted_labels, actual_labels)) / float(len(actual_labels))
        assert np.isclose(accuracy, expected_accuracy), \
            "all accuracies should be same, {:s} accuracy is different, expected: {:f}, actual: {:f}".format(
            key, expected_accuracy, accuracy)
        print("{:5s} {:6.2f}% ok".format(key, accuracy * 100))
    print("all ok")

def get_trials_for_class(dataset, class_nr):
    return dataset.get_topological_view()[np.where(dataset.y[:,class_nr] == 1)]

def load_preprocessed_datasets(train_state, fold_nr=None, preprocessor=None):
    if fold_nr is None:
        # TODELAY: move into functio and merge with same part
        # in load_train_testdataset...
        params = train_state.info_parameters
        filename = params['dataset_filename']
        topo_view_channel = params.get('topo_view_channel',
            'auto_clean_trials_500Hz_500ms_len_250ms_stride_ps')
        target_channel = 'targets'
        sensor_names = params.get('sensor_names', None)
        if (sensor_names == '$C_sensors'): 
            sensor_names = get_C_sensors()
            dataset_type_string = params.get('dataset_type', '*fft_type')
        dataset_args = {'axes': params.get('axes', ('b', 'c', 0, 1)),
            'sensor_names': sensor_names}
        dataset_type_string = params.get('dataset_type', '*fft_type')

        if (dataset_type_string == '*fft_type'):
            dataset_type = FFTMotorEEGDataset
            dataset_args.update({'frequency_start': params['frequency_start'],
                'frequency_stop': params['frequency_stop']})
        elif(dataset_type_string == '*raw_type'):
            dataset_type = RawMotorEEGDataset
        else:
            assert(False)
        train_dataset = dataset_type(filename, topo_view_channel, 
                         target_channel=target_channel,
                         start=0., stop=0.9,
                         **dataset_args)
        test_dataset = dataset_type(filename, topo_view_channel, 
                         target_channel=target_channel, # have to do this atm as keyword arg
                         start=0.9, stop= 1.0, limits=None,
                         **dataset_args)
        apply_preprocessing(train_dataset, test_dataset, preprocessor)
        return {'train': train_dataset, 'test': test_dataset}
    else:
        train_dataset, test_dataset = load_train_test_datasets(train_state, fold_nr)
        apply_preprocessing(train_dataset, test_dataset, preprocessor)
        return {'train': train_dataset, 'test': test_dataset}

def apply_preprocessing(train_dataset, test_dataset, preprocessor):
    # for now assume standardization
    if preprocessor is not None:
        preprocessor.apply(train_dataset, can_fit=True)
        preprocessor.apply(test_dataset, can_fit= False)

def load_train_test_datasets(model, fold_nr=None):
    assert fold_nr != None # for now only use this for cross val datasets...
    ## Determine parameters for motor eeg constructor
    params = model.info_parameters
    filename = params['dataset_filename']
    topo_view_channel = params.get('topo_view_channel',
        'auto_clean_trials_500Hz_500ms_len_250ms_stride_ps')
    target_channel = 'targets'
    sensor_names = params.get('sensor_names', None)
    if (sensor_names == '$C_sensors'): 
        sensor_names = get_C_sensors()
    h5file = h5py.File(filename, 'r')
    num_trials = h5file[topo_view_channel].shape[0]
    # override by parameters if given
    h5file.close()
    num_trials = params.get('cross_validation_stop', num_trials)
    n_folds = params.get('cross_val_folds', 10)
    ## Determine train and test fold indices
    k_fold = KFold(num_trials, n_folds=n_folds, shuffle=False, random_state=None)
    k_folds = list(k_fold)
    test_fold = k_folds[fold_nr][1]
    test_start = test_fold[0]
    test_stop = test_fold[-1] + 1 # stop is not included(!)
    train_fold = k_folds[fold_nr][0]
    max_trial_nr = max(np.max(train_fold) + 1, test_stop)
    
    #if (frequency_start == 5): frequency_start = 2 # hack to correct earlier mislabeling
    # train limits are simply full dataset excluding the test fold part...
    train_limits = [[0, test_start],[test_stop, max_trial_nr]]
    dataset_type_string = params.get('dataset_type', '*fft_type')
    dataset_args = {'axes': params.get('axes', ('b', 'c', 0, 1)),
        'sensor_names': sensor_names}
    if (dataset_type_string == '*fft_type'):
        dataset_type = FFTMotorEEGDataset
        dataset_args.update({'frequency_start': params['frequency_start'],
            'frequency_stop': params['frequency_stop']})
    elif(dataset_type_string == '*raw_type'):
        dataset_type = RawMotorEEGDataset
    else:
        assert(False)
    test_dataset = dataset_type(filename, topo_view_channel, 
                     target_channel=target_channel, # have to do this atm as keyword arg
                     start=test_start, stop= test_stop, limits=None,
                     **dataset_args)
    train_dataset = dataset_type(filename, topo_view_channel, 
                     target_channel=target_channel,
                     limits=train_limits,
                     **dataset_args)
    return train_dataset, test_dataset

def totopoview(data, view_converter):
    return view_converter.design_mat_to_topo_view(np.array([data]))[0]

def bc01toinput(model, inputs):
    return Conv2DSpace.convert_numpy(inputs, ('b','c', 0, 1), 
        model.get_input_space().axes)

def get_dataset_predictions(model, dataset):
    """ Get predictions both if model is flat or topological """
    try:
        return get_predictions(model, dataset.X)
    except:
        return reshape_and_get_predictions(model, dataset.get_topological_view(),
            dataset.view_converter.axes)

def reshape_and_get_predictions(model, inputs, axes=('b','c', 0, 1)):
    ''' Assume inputs are in bc01 and need to be reshaped'''
    inputs = Conv2DSpace.convert_numpy(inputs, axes, 
        model.get_input_space().axes)
    return get_predictions(model, inputs)

def get_predictions(model, inputs):
    predict = create_prediction_func(model)
    # TODO: do it like this to avoid graphic card memory overflows
    predictions = [predict([trial]) for trial in inputs]
    predictions = np.squeeze(np.array(predictions))
    #predictions = predict( inputs )
    return predictions

def create_dataset(inputs, targets, filename):
    h5file = h5py.File('data/plot-inputs/GuJoMoSc1S001R01_ds10_1-11_autoclean_trials_ival_0_4000.mat', 'r')
    fake_file = h5py.File(filename, 'a')
    fake_file.create_dataset('inputs', data=inputs)
    fake_file.create_dataset('targets', data=targets)
    fake_file.create_dataset('sampling_rate', data=h5file['sampling_rate'])
    g = fake_file.create_group('#refs#')
    g.copy(h5file['channel_names'], '/channel_names', expand_refs=True)
    fake_file.close()
    h5file.close()
    
def create_prediction_func(model):
    X = model.get_input_space().make_theano_batch()
    Y = model.fprop( X )
    predict_func = theano.function( [X], Y , allow_input_downcast=True)
    return predict_func
def create_activations_func(model):
    X = model.get_input_space().make_theano_batch()
    Y = model.fprop( X , return_all=True )
    predict_func = theano.function( [X], Y , allow_input_downcast=True)
    return predict_func
    

def back_deconv(layers, wanted_linear_outputs):
    deconver = Deconv(layers, wanted_linear_outputs)
    return deconver.back_deconv()

def back_prop(layers, wanted_linear_outputs, inputs):
    deconver = Deconv(layers, wanted_linear_outputs)
    return deconver.back_prop(inputs)

def back_guided_deconv(layers, wanted_linear_outputs, inputs):
    deconver = Deconv(layers, wanted_linear_outputs)
    return deconver.back_guided_deconv(inputs)

def back_linear(layers, wanted_linear_outputs):
    deconver = Deconv(layers, wanted_linear_outputs)
    return deconver.back_linear()   

def back_guided_deconv_for_class(model, class_nr, inputs, view_converter):
    wanted = [-1] * 4
    wanted[class_nr] = 1
    wanted_input = back_guided_deconv(model.layers, wanted, bc01toinput(model, np.array([inputs]))[0])
    if (wanted_input.ndim == 1):
        wanted_input = totopoview(wanted_input, view_converter)
    return wanted_input

class Deconv:
    """ Class for the different variants of deconv...
    Hopefully heplful for visualizing what model learned and
    also how it reacts to different inputs"""
    def __init__(self, layers, wanted_linear_outputs):
        self.layers = layers
        self.wanted_linear_outputs = wanted_linear_outputs
        self.activations = None

    def conv_layer_back_deconv(self, conv_layer, expected_linear_outputs):
        #if (conv_layer.get_input_space().axes == ('b', 'c', 0, 1)):
        #    expected_outputs = expected_outputs - conv_layer.get_biases().transpose(1,0,2)
        
        input_space = conv_layer.get_input_space()
        time_bins = input_space.shape[0]
        freq_bins = input_space.shape[1]
        num_chans = input_space.num_channels
        conv_weights = conv_layer.get_weights_topo() # b 0 1 c
        # flip time and freq dim, since we did convolution
        conv_weights = conv_weights[:, ::-1, ::-1, :]
        num_filters = conv_weights.shape[0]
        filt_time_bins = conv_weights.shape[1]
        filt_freq_bins = conv_weights.shape[2]
        filt_chans = conv_weights.shape[3]
        assert filt_chans == num_chans # always convolving over all chans
        expected_linear_input = np.zeros((num_filters, time_bins, freq_bins, num_chans))
        # could collect filterwise inputs.. and also return them as second argument...
        # to show variablity how class can be shown...
        assert(conv_weights.shape[3] == num_chans) # always convolve over all channels so far
        for filt_i in xrange(0, num_filters):
            for time_bin in xrange(0, time_bins - filt_time_bins + 1):
                for freq_bin in xrange(0, freq_bins - filt_freq_bins + 1):
                        # expected output should be scalar
                        expected_output = expected_linear_outputs[time_bin,freq_bin, filt_i]
                        # only use filter if positive output is expected here/not equal 0 (just saves computation :))
                        #if (expected_output == 0): continue
                        #else: assert expected_output > 0
                        cur_filter = conv_weights[filt_i]
                        # Lets multiply expected output with weights!
                        expected_input = expected_output * cur_filter # or divided by weights? :)
                        expected_linear_input[filt_i, time_bin:time_bin + filt_time_bins, \
                                              freq_bin:freq_bin + filt_freq_bins, :] += expected_input
        return expected_linear_input
    
    def flat_layer_back_deconv(self, flat_layer, expected_linear_outputs):
        #expected_linear_outputs = expected_linear_outputs - flat_layer.get_biases()
        flat_weights = flat_layer.get_weights_topo()    
        expected_inputs = expected_linear_outputs * flat_weights.T
        # now 0c1b #timebins x#chans x#freqbins x #batches/neurons
        #  TODO: instead of transpose. do .T and then same as in back-devonc function (Conv2dSpace.convert...)
        # should give same reslt and then we would have consistent methods
        # change to bo1c =>#neuronsx #timex#freq#chan
        expected_inputs = np.transpose(expected_inputs, (3,0,2,1))
        return expected_inputs
    
    
    def back_linear(self, output_prune_function = lambda out,activations:out):
        """ Backprop linearly (ignore activation function/gradient).
        To use gradient or sth else to modify expected outputs for layer below,
        specify a output prune function"""
        last_layer = self.layers[-1]
        weights_in_topo_format = isinstance(last_layer.input_space, Conv2DSpace)
        last_weights = None
        if weights_in_topo_format:
            last_weights = last_layer.get_weights_topo()
        else:
            last_weights = last_layer.get_weights()
        # have to hackily change axes here in case of LinaerLayer... 
        # logically it makes no sense as we want b 0 1 c and not the input space
        # but somehow weights are not formatted right before.... so we undo
        # the transformation which caused the problem in the source code
        # W = Conv2DSpace.convert(W, self.input_space.axes, ('b', 0, 1, 'c')) in pylearn mlp.py code
        # get_weighs_topo function of LinearLayer
        if (isinstance(last_layer, RectifiedLinear) and weights_in_topo_format):      
            last_weights = Conv2DSpace.convert(last_weights, ('b', 0, 1, 'c'), last_layer.input_space.axes)
        # lets get linear output derived on inputs
        #ignoring bias is this ok? you could think aboutt using bias of wanted class (only adding this one or sth)
        
        #self.wanted_linear_outputs = self.wanted_linear_outputs - last_layer.get_biases()
        if (weights_in_topo_format):
            expected_inputs = self.wanted_linear_outputs * last_weights.T
        else:
            expected_inputs = self.wanted_linear_outputs * last_weights
        expected_inputs = expected_inputs.T
    
        # go backwards through layers
        for layer_i in xrange(len(self.layers) - 2,-1,-1):
            layer = self.layers[layer_i]
            # Sum expected outputs over all neurons in deeper layer
            expected_outputs = np.sum(expected_inputs, axis=0)
            activations = None if self.activations is None else self.activations[layer_i]
            expected_outputs = output_prune_function(expected_outputs, activations)
            if (isinstance(layer, RectifiedLinear)):
                expected_inputs = self.flat_layer_back_deconv(layer, expected_outputs)
            elif (isinstance(layer, ConvRectifiedLinear)):
                expected_inputs = self.conv_layer_back_deconv(layer, expected_outputs)            
            else:
                raise("unknown layer type for deconv") 
            #expected_inputs = expected_inputs - layer.get_biases()
        # average over all filters/neurons
        # filters are on axis 1...
        wanted_input =  np.sum(expected_inputs, axis=0)
        # transpose to #chans x #timebins x #freqbins
        if (isinstance(self.layers[0].input_space, Conv2DSpace)):
            wanted_input = np.transpose(wanted_input, (2, 0, 1))
        return wanted_input
    
    def fprop_remember_activations(self, inputs):
        self.activations = []
        output_below = [inputs]
        #self.activations.append(output_below)
        for layer in self.layers:
            # forward prop, first createsymbolic forward prop from theano
            # then do actual forwardprop with output from layer below
            X = layer.get_input_space().make_theano_batch()
            Y = layer.fprop(X)
            fprop = theano.function([X], Y)
            output_below = fprop(output_below)
            # reshape activation to 0 1 c if necessary
            activation = output_below
            # (1, 32, 15, 1)
            if (activation.ndim == 4):
                activation = Conv2DSpace.convert(activation, ('b', 'c', 0, 1), 
                    ('b', 0, 1, 'c'))
            activation = np.squeeze(activation)
            self.activations.append(activation)
        
    def back_prop(self, inputs):
        self.fprop_remember_activations(inputs)
        def remove_zero_activations(expected_outputs, activations):
            return expected_outputs * np.float32(activations > 0)
        return self.back_linear(remove_zero_activations)
    
    def back_deconv(self):
        def remove_zero_outputs(expected_outputs, activations):
            return expected_outputs * np.float32(expected_outputs > 0)
        return self.back_linear(remove_zero_outputs)
    
    def back_guided_deconv(self, inputs):
        self.fprop_remember_activations(inputs)
        def remove_zero_outputs_and_activations(expected_outputs, activations):
            return expected_outputs * np.float32(expected_outputs > 0) * np.float32(activations > 0)
        return self.back_linear(remove_zero_outputs_and_activations)
        

def get_activation(layers, one_input):
    activations_per_layer = []
    output_below = [one_input]
    for layer in layers:
    # to see problems, forward prop
        X = layer.get_input_space().make_theano_batch()
        Y = layer.fprop(X)
        fprop = theano.function([X], Y)
        output_below = fprop(output_below)
        # reshape activation to 0 1 c if necessary
        activation = output_below
        # (1, 32, 15, 1)
        if (activation.ndim == 4):
            activation = Conv2DSpace.convert(activation, ('b', 'c', 0, 1), 
                ('b', 0, 1, 'c'))
        activation = np.squeeze(activation)
        activations_per_layer.append(activation)
    return activations_per_layer

def get_activations(layers, all_inputs):
    activations_per_input = []
    for next_input in all_inputs:
        activations_per_layer = get_activation(layers, next_input)
        activations_per_input.append(activations_per_layer)
    return activations_per_input

def get_summed_activations(layers, all_inputs):
    activations_per_input = get_activations(layers, all_inputs)
    return np.sum(np.array(activations_per_input), axis=0)

