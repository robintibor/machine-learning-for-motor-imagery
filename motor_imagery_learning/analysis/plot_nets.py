from matplotlib import cm
import matplotlib.gridspec as gridspec
import numpy as np
import matplotlib.pyplot as plt
from motor_imagery_learning.analysis.sensor_positions import tight_C_positions


def plot_head_images(head_images, sensor_names, figsize=(12,3)):
    head_images = np.array(head_images)
    assert len(sensor_names) == head_images.shape[1]
    layer_plot = LayerPlot(head_images, sensor_names)
    layer_plot.plot(figsize)
    return layer_plot.fig

def plot_class_weights_and_head_images(head_images, sensor_names, class_weights, figsize=(12,3)):
    head_images = np.array(head_images)
    assert len(sensor_names) == head_images.shape[1]
    layer_plot = ClassProbLayerPlot(head_images, sensor_names, class_weights)
    layer_plot.plot(figsize)
    return layer_plot.fig

class LayerPlot(object):
    def __init__(self, neuron_images, sensor_names):
        self.__dict__.update(locals())
        del self.self
        
    def _setup_axes(self):
        self.neuron_axes = []
        outer_cols = min(8, len(self.neuron_images))
        outer_rows = int(np.ceil(len(self.neuron_images) / float(outer_cols)))
        outer_grid = gridspec.GridSpec(outer_rows, outer_cols, wspace=0.5, hspace=0.5)
        for neuron_i in range(len(self.neuron_images)):
            neuron_axes = []
            self.neuron_axes.append(neuron_axes)
            rows = len(tight_C_positions)
            cols = len(tight_C_positions[0])
            outer_row = int(neuron_i / outer_cols)
            outer_col = neuron_i % outer_cols
            inner_grid = gridspec.GridSpecFromSubplotSpec(rows, cols,
                subplot_spec=outer_grid[outer_row, outer_col], 
                wspace=0.0, hspace=0.0)
            for sensor_name in self.sensor_names:
                x, y = np.where(np.char.lower(np.array(tight_C_positions)) == sensor_name.lower())
                subplot_ind = x * cols + y + 1
                ax = plt.Subplot(self.fig, inner_grid[subplot_ind-1])
                neuron_axes.append(ax)
                
    def plot(self, figsize):
        self.fig = plt.figure(figsize=figsize)
        self._setup_axes()
        self._plot_head_images()
                
    def _plot_head_images(self):
        mean_abs_weight = np.mean(np.abs(self.neuron_images))
        for neuron_i in range(len(self.neuron_images)):
            mean_output_neuron = self.neuron_images[neuron_i]
            for sensor_i in xrange(len(self.sensor_names)):
                sensor_matrix = mean_output_neuron[sensor_i]
                ax = self.neuron_axes[neuron_i][sensor_i]
                ax.pcolorfast(sensor_matrix.T, cmap=cm.bwr, # @UndefinedVariable
                    vmin=-mean_abs_weight*2, vmax=mean_abs_weight*2,  
                    rasterized=False)
                ax.set_xticks([])
                ax.set_yticks([])
                self.fig.add_subplot(ax)
      
    
      

class ClassProbLayerPlot(LayerPlot):
    def __init__(self, neuron_images, sensor_names, class_weights):
        self.__dict__.update(locals())
        del self.self
    def _setup_axes(self):
        """ Always leave one empty row for class probs """
        self.neuron_axes = []
        self.class_prob_axes = []
        outer_cols = min(4, len(self.neuron_images))
        outer_rows = 2 * int(np.ceil(len(self.neuron_images) / float(outer_cols)))
        outer_grid = gridspec.GridSpec(outer_rows, outer_cols,
            height_ratios=[6,1] * (outer_rows / 2),
            wspace=0.2, hspace=0.3)
        for neuron_i in range(len(self.neuron_images)):
            neuron_axes = []
            self.neuron_axes.append(neuron_axes)
            rows = len(tight_C_positions)
            cols = len(tight_C_positions[0])
            outer_row = 2 * int(neuron_i / outer_cols)
            outer_col = neuron_i % outer_cols
            class_ax = plt.subplot(outer_grid[outer_row + 1, outer_col])
            self.class_prob_axes.append(class_ax)
            inner_grid = gridspec.GridSpecFromSubplotSpec(rows, cols,
                subplot_spec=outer_grid[outer_row, outer_col], 
                wspace=0.0, hspace=0.0)
            for sensor_name in self.sensor_names:
                x, y = np.where(np.char.lower(np.array(tight_C_positions)) == sensor_name.lower())
                #x[0] and y[0] necessary to prevent numpy deprecation
                # warnings... as x and y are one-element arrs...
                subplot_ind = x[0] * cols + y[0] + 1 
                ax = plt.Subplot(self.fig, inner_grid[subplot_ind-1])
                neuron_axes.append(ax)
                
    def plot(self, figsize):
        self.fig = plt.figure(figsize=figsize)
        self._setup_axes()
        self._plot_head_images()
        self._plot_class_probs()
        
    def _plot_class_probs(self):
        value_minmax = np.max(np.abs(self.class_weights))
        # hide normal x/y ticks but show some ticks for orientation in case two classes have almost same color
        for neuron_i in range(len(self.neuron_images)):
            class_weights = self.class_weights[neuron_i]
            class_ax = self.class_prob_axes[neuron_i]
            class_ax.pcolorfast(np.atleast_2d(class_weights),
                cmap=cm.bwr,  # @UndefinedVariable
                vmin=-value_minmax, vmax=value_minmax)
            if class_weights.size==4:
                class_ax.get_xaxis().set_ticks([0.5,1.5,2.5])
                class_ax.get_xaxis().set_ticklabels([])
            else:
                class_ax.set_xticks([])
            class_ax.set_yticks([])
        