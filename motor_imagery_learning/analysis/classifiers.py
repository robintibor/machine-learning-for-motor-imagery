from sklearn.covariance import LedoitWolf
import numpy as np
from sklearn.cross_validation import KFold

class LDA():
    def train(self, x,y):
        self.w, self.b = train_lda(x,y)

    def predict(self,x):
        return predict_lda(self.w,self.b, x)

    def predict_prob(self,x):
        return predict_prob_lda(self.w,self.b, x)
        
    def score(self, x,y):
        return score_lda(self.w, self.b,x,y)
        
    def fit_and_score(self, x,y):
        self.train(x,y)
        return self.score(x,y)
    
    def fit_and_score_train_test(self,x,y, sklearn_test_fold):
        """ Returns train and test scores after fitting on train
        (first 9 of 10 folds are used for train)."""
        assert x.shape[0] == y.shape[0]
        if sklearn_test_fold:
            train_inds, test_inds = list(KFold(x.shape[0], 10, shuffle=False))[-1]
        else:
            train_inds = range(int(x.shape[0] * 0.6))
            test_inds = range(train_inds[-1] +1, x.shape[0])
        self.train(x[train_inds],y[train_inds])
        return (self.score(x[train_inds],y[train_inds]), 
            self.score(x[test_inds], y[test_inds]))

    def fit_and_score_train_test_topo(self, topo_view,y, sklearn_test_fold=True):
        x = topo_view.reshape(topo_view.shape[0], -1)
        return self.fit_and_score_train_test(x,y, sklearn_test_fold)
        
        
def train_lda(x,y):
    class_0_inds = np.argmax(y, axis=1) == 0
    class_1_inds = np.argmax(y, axis=1) == 1
    mu1 = np.mean(x[class_0_inds], axis=0)
    mu2 = np.mean(x[class_1_inds], axis=0)
    # x' = x - m
    m = np.empty(x.shape)
    m[class_0_inds] = mu1
    m[class_1_inds] = mu2
    x2 = x - m
    # w = cov(x)^-1(mu2 - mu1)
    estimator = LedoitWolf(block_size=100000)
    covm = estimator.fit(x2).covariance_

    w = np.dot(np.linalg.pinv(covm), (mu2 - mu1))

    #  From matlab bbci toolbox:
    # https://github.com/bbci/bbci_public/blob/fe6caeb549fdc864a5accf76ce71dd2a926ff12b/classification/train_RLDAshrink.m#L133-L134
    #C.w= C.w/(C.w'*diff(C_mean, 1, 2))*2;
    #C.b= -C.w' * mean(C_mean,2);
    w = (w / np.dot(w.T, (mu2 - mu1))) * 2
    b = np.dot(-w.T, np.mean((mu1, mu2), axis=0))
    return w,b


def predict_prob_lda(w,b,x):
    return np.dot(x, w) + b

def predict_lda(w,b,x):
    return np.dot(x, w) + b >=0

def score_lda(w,b,x,y):
    predicted_1 = predict_lda(w,b,x)
    actual_1 = y[:,1]
    return sum(predicted_1 == actual_1) / float(len(predicted_1))

def evaluate_lda(x,y):
    w,b = train_lda(x,y)
    return score_lda(w,b,x,y)


def evaluate_lda_train_test(x,y):
    test_start = int(x.shape[0] * 0.7)
    w,b = train_lda(x[:test_start],y[:test_start])
    return score_lda(w,b,x[test_start:],y[test_start:])
def evaluate_topo_view_lda_train_test(topo_view,y):
    x = topo_view.reshape(topo_view.shape[0], -1)
    return evaluate_lda_train_test(x,y)

class OneFeatureScorer:
    def train(self, x, y):
        # Go throgh all possible x values for thresholds
        scores = np.array([score_threshold(thresh, x, y) for thresh in x])
        best_threshold_ind = np.argmax(np.maximum(scores, 1 -scores))
        self.threshold = x[best_threshold_ind]
        if scores[best_threshold_ind] < 0.5:
            self.downwards = 1
        else:
            self.downwards = 0
            
        
    def score(self, x,y):
        if (self.downwards):
            correct = (x <= self.threshold) == y[:,1]
        else:
            correct = (x > self.threshold) == y[:,1]
        
        fraction_correct = sum(correct) / float(len(y[:,1]))
        return fraction_correct    
    
    def fit_and_score_train_test_topo(self, topo_view, y, sklearn_test_fold=True):
        x = topo_view.flatten()
        return self.fit_and_score_train_test(x,y,sklearn_test_fold)
    
    def fit_and_score_train_test(self,x,y,sklearn_test_fold):
        if sklearn_test_fold:
            train_inds, test_inds = list(KFold(x.shape[0], 10, shuffle=False))[-1]
        else:
            train_inds = range(int(x.shape[0] * 0.6))
            test_inds = range(train_inds[-1] +1, x.shape[0])
        self.train(x[train_inds], y[train_inds])
        return self.score(x[test_inds], y[test_inds])
        
def score_threshold(threshold, x, y):
    correct = (x > threshold) == y[:,1]
    fraction_correct = sum(correct) / float(len(y[:,1]))
    return fraction_correct