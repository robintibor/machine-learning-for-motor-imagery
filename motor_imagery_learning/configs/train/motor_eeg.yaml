#STARTPARAMETERS
{
     default: &default {
        ### Common standard parameters for all experiments
         # only if you are using early stop, i.e. have a seperate validation fold
        ## Data loading and cleaining
        axes: ['b', 'c', 0, 1],
        rejection_var_ival: [0,4000],
        rejection_blink_ival: [-500,4000],
        max_min: 600,
        whisker_percent: 10, 
        whisker_length: 3,
        ## Data preprocessing
        cnt_preprocessors: "*resample_high_pass",
        low_cutoff: 0.5,
        resampling_fs: 300,
        epo_preprocessors: [],
        unsupervised_preprocessor: null,
        preprocessor: "*online_chan_freq_wise",
        new_factor_preproc: 1.,
        batch_transformer: null,
        ## Learning Algorithm
        algorithm: "*sgd",
        batch_size: 55,
        weight_initializer: null,
        sgd_callbacks: !!null '',
        learning_rule: "*adam",  
        adam_alpha: 0.0002,
        adam_beta1: 0.1,
        adam_beta2: 0.001,
        ## Early Stopping
        termination_criterion: "*early_stop_and_max_epochs",
        termination_decrease: 0.0001,
        max_increasing_epochs: 200,
        stopping_channel: "valid_y_misclass",
        max_epochs: 1500,
        best_model_channel: 'valid_y_misclass',
        keep_best_lrule_params: true,
     
     
        ### Parameters that should be adapted by individual experiment
        dataset_splitter: "*train_test_splitter",
        test_filename: null,
        train_cleaner: "*one_set_cleaner",
        sensor_names: !!null '',  
        segment_ival: [0,4000],        
        dataset_args: "*fft_dataset_args",
        costs: "*default_costs", 
        layers: "*flat",
        frequency_start: null,
        frequency_stop: null,
        hidden_neurons: 20,
        updates_per_batch: 25,

        ### Parameters only for FFT Net
        transform_function_and_args: "*power_func",
        divide_win_length: false,
        square_amplitude: false,
        phases_diff: true,

        ### Parameters only for Filterbank Net 
        min_freq: 2,
        max_freq: 14,
        last_low_freq: 20,
        low_width: 2,
        high_width: 4,
        
        ### Parameters for different learning rule or stopping variants
        learning_rate: 0.1,
        momentum: 0.5,        
        rms_prop_decay: 0.9,
        rms_prop_max_scaling: 1e5,
        fraction_after_stop: 1.,
        min_epochs: 100,
        factor_new_termination: 0.02,
        

        ### Parameters for predefined layers (for hyperopt mostly)
        max_col_norm_hidden_layer: 2.,
        max_col_norm_final_layer: 2.,
        max_kernel_norm: 2.,
        output_channels_conv: 16,
        kernel_shape_time: 2,
        kernel_shape_freq: 6,
        kernel_stride_time: 1,
        kernel_stride_freq: 1,
        pool_shape_time: 1,
        pool_shape_freq: 1,
        pool_stride_time: 1,
        pool_stride_freq: 1,
        first_layer_input_include_probs: 0.8,
        full_second_layer_input_include_probs: 0.5,
        final_layer_input_include_probs: 0.5,
        first_layer_input_scales: 1.,
        full_second_layer_input_scales: 2.,
        final_layer_input_scales: 2.,
        # raw and fb net
        input_first_probs: 0.5,
        input_first_scales: 2.,
        input_softmax_probs: 0.5,
        input_softmax_scales: 2,
        max_col_norm: 0.5,

        # fb net
        output_channels: 80,
        max_kernel_norm: 2.,
        pool_type: sumlogall,
        
        #raw net
        bp_nonlinearity: IdentityConvNonlinearity,
        bp_chans: 40,
        bp_kernel_shape_time: 30,
        bp_kernel_stride_time: 1,
        bp_max_kernel_norm: 2.0,
        spatial_chans: 40,
        square_max_kernel_norm: 2.,
        square_pool_shape_time: 100,
        square_pool_stride_time: 20,
        input_spatial_probs: 0.5,
        input_spatial_scales: 2.0,
        pool_func: log_sum,
        
        ### Parameters contorlling what data is trained and tested
        restricted_n_trials: null,
        # get full set for crossvalidation
        # values will replace training_start and training_stop!
        cross_validation_start: !!python/none '',
        cross_validation_stop: !!python/none '',
        # TODO: remove altogether? or think what to do with this :)
        # mix the dataset well into training valid test if you are running
        # just once (without cross validation)
        #will be set by script...
        training_start: !!python/none '',
        training_stop: !!python/none '',
        training_type: "*training_test_set",    
        cross_val_folds: 10,
        test_fold_nr: 9,
        
        ### Debug and save parameters 
        save_path: data/best_models.pkl,
        load_sensor_names: !!null '',
        #dataset_filename: "",# has to be specified, default would make no sense here!!
        save_freq: 1000, # only save at end basically :)

        ### Parameters for transfer learning, probably wont work anymore
        ### (as of 9th august 2015)
        transfer_filenames: !!null '',
        transfer_learning_rate: 0.01,
        transfer_start: !!python/none '',
        transfer_stop: !!python/none '',
        transfer_leave_out: !!python/none '', # will cause a crash if actually called...
        
        transfer_inner_folds: 10,
     },
}
#ENDPARAMETERS
{
    cnt_preprocessors: {
        resample_high_pass: &resample_high_pass [[
                !!python/name:motor_imagery_learning.mywyrm.processing.resample_cnt , 
                {newfs: $resampling_fs} 
            ],
            [
                 !!python/name:motor_imagery_learning.mywyrm.processing.highpass_cnt ,
                 {low_cut_off_hz: $low_cutoff}
            ]
        ],
        resample_high_pass_car: &resample_high_pass_car [[
                !!python/name:motor_imagery_learning.mywyrm.processing.resample_cnt , 
                {newfs: $resampling_fs} 
            ],
            [
                 !!python/name:motor_imagery_learning.mywyrm.processing.highpass_cnt ,
                 {low_cut_off_hz: $low_cutoff}
            ],
            [
                 !!python/name:motor_imagery_learning.mywyrm.processing.common_average_reference_cnt ,
                 {}
            ]
        ],
    },
    preprocessors:  {
        standardize: &standardize !obj:pylearn2.datasets.preprocessing.Standardize {},
        online_standardize: &online_standardize !obj:mypylearn2.preprocessing.OnlineStandardize {
            use_only_new: false,
            new_factor: $new_factor_preproc,
        },
        online_chan_freq_wise: &online_chan_freq_wise !obj:mypylearn2.preprocessing.OnlineAxiswiseStandardize {
            use_only_new: false,
            new_factor: $new_factor_preproc,
            axis: ['c', 1],
        },
    },
    unsupervised_preprocessors: {
        restrict_trials: &restrict_trials !obj:mypylearn2.preprocessing.RestrictTrials {
            number_of_trials: $restricted_n_trials,
        },
    },
    transform_funcs: {
        power_func: &power_func [
             !!python/name:motor_imagery_learning.bbci_pylearn_dataset.compute_power_spectra ,
             {
                divide_win_length: $divide_win_length,
                square_amplitude: $square_amplitude,
             }
        ],
        power_phase_func: &power_phase_func [
             !!python/name:motor_imagery_learning.bbci_pylearn_dataset.compute_power_and_phase ,
             {
                divide_win_length: $divide_win_length,
                square_amplitude: $square_amplitude,
                phases_diff: $phases_diff,
             }
            
        ],
    },
    sensors: {
        debug_sensors: &debug_sensors ['C3', 'C4', 'Cz'],
        C_sensors: &C_sensors ['FC5', 'FC1', 'FC2', 'FC6', 'C3', 'Cz', 'C4', 'CP5',
            'CP1', 'CP2', 'CP6', 'FC3', 'FCz', 'FC4', 'C5', 'C1', 'C2', 'C6',
            'CP3', 'CPz', 'CP4', 'FFC5h', 'FFC3h', 'FFC4h', 'FFC6h', 'FCC5h',
            'FCC3h', 'FCC4h', 'FCC6h', 'CCP5h', 'CCP3h', 'CCP4h', 'CCP6h', 'CPP5h',
            'CPP3h', 'CPP4h', 'CPP6h', 'FFC1h', 'FFC2h', 'FCC1h', 'FCC2h', 'CCP1h',
             'CCP2h', 'CPP1h', 'CPP2h'],
         all_EEG_sensors: &all_EEG_sensors ['Fp1', 'Fp2', 'Fpz', 'F7', 'F3', 'Fz', 'F4', 'F8',
            'FC5', 'FC1', 'FC2', 'FC6', 'M1', 'T7', 'C3', 'Cz', 'C4', 'T8', 'M2',
            'CP5', 'CP1', 'CP2', 'CP6', 'P7', 'P3', 'Pz', 'P4', 'P8', 'POz', 'O1',
            'Oz', 'O2', 'AF7', 'AF3', 'AF4', 'AF8', 'F5', 'F1', 'F2', 'F6', 'FC3',
            'FCz', 'FC4', 'C5', 'C1', 'C2', 'C6', 'CP3', 'CPz', 'CP4', 'P5', 'P1',
            'P2', 'P6', 'PO5', 'PO3', 'PO4', 'PO6', 'FT7', 'FT8', 'TP7', 'TP8',
            'PO7', 'PO8', 'FT9', 'FT10', 'TPP9h', 'TPP10h', 'PO9', 'PO10', 'P9',
            'P10', 'AFF1', 'AFz', 'AFF2', 'FFC5h', 'FFC3h', 'FFC4h', 'FFC6h', 'FCC5h',
            'FCC3h', 'FCC4h', 'FCC6h', 'CCP5h', 'CCP3h', 'CCP4h', 'CCP6h', 'CPP5h',
            'CPP3h', 'CPP4h', 'CPP6h', 'PPO1', 'PPO2', 'I1', 'Iz', 'I2', 'AFp3h',
            'AFp4h', 'AFF5h', 'AFF6h', 'FFT7h', 'FFC1h', 'FFC2h', 'FFT8h', 'FTT9h',
            'FTT7h', 'FCC1h', 'FCC2h', 'FTT8h', 'FTT10h', 'TTP7h', 'CCP1h', 'CCP2h',
            'TTP8h', 'TPP7h', 'CPP1h', 'CPP2h', 'TPP8h', 'PPO9h', 'PPO5h', 'PPO6h',
            'PPO10h', 'POO9h', 'POO3h', 'POO4h', 'POO10h', 'OI1h', 'OI2h']
    },
    # args for different types of datasets
    datasets_args: [
        common_args: &common_args {
            filenames: $dataset_filename,
            axes: $axes, # examples x channels x times x frequencies
            start: $training_start,
            stop: $training_stop,
            sensor_names: $sensor_names,
        },
        bbci_args: &bbci_dataset_args {
            <<: *common_args,
            type: !!python/name:bbci_pylearn_dataset.BBCIPylearnCleanDataset ,
            cnt_preprocessors: $cnt_preprocessors,
            epo_preprocessors: $epo_preprocessors,
            load_sensor_names: $load_sensor_names,
            segment_ival: $segment_ival,
            unsupervised_preprocessor: $unsupervised_preprocessor,
        },
        bbci_fft_args: &bbci_dataset_fft_args {
            <<: *bbci_dataset_args,
            type: !!python/name:bbci_pylearn_dataset.BBCIPylearnCleanFFTDataset ,
            frequency_start: $frequency_start,
            frequency_stop: $frequency_stop,
            transform_function_and_args: $transform_function_and_args,
        },
        bbci_filterbank_args: &bbci_filterbank_dataset_args {
            <<: *bbci_dataset_args,
            type: !!python/name:bbci_pylearn_dataset.BBCIPylearnCleanFilterbankDataset ,
            min_freq: $min_freq,
            max_freq: $max_freq,
            last_low_freq: $last_low_freq,
            low_width: $low_width,
            high_width: $high_width,
        }
    ],
    cleaner: [
        clean_args: &clean_args { 
            rejection_var_ival: $rejection_var_ival,
            rejection_blink_ival: $rejection_blink_ival,
            max_min: $max_min,
            whisker_percent: $whisker_percent, 
            whisker_length: $whisker_length,
        },
        one_set_cleaner: &one_set_cleaner !obj:motor_imagery_learning.mywyrm.clean.BBCISetCleaner { 
            <<: *clean_args
        },
        two_set_cleaner: &two_set_cleaner !obj:motor_imagery_learning.mywyrm.clean.BBCITwoSetsCleaner { 
            <<: *clean_args,
            second_filename: $test_filename,
            load_sensor_names: $load_sensor_names,
        },
        test_cleaner: &test_cleaner  !obj:motor_imagery_learning.mywyrm.clean.BBCISecondSetOnlyChanCleaner { 
            <<: *clean_args,
            first_set_cleaner: $train_cleaner,
        }
    ],
    datasets: [ # array to force yaml to load in correct order
        train_dataset: &train !obj:motor_eeg_dataset.get_dataset {
            <<: $dataset_args,
            cleaner: $train_cleaner,
        },
        # test is only in case we really have two different datasets for test and training
        # and not a split of one and the same dataset
        test_dataset: &test !obj:motor_eeg_dataset.get_dataset {
            <<: $dataset_args,
            cleaner: *test_cleaner,
            filenames: $test_filename,
        },
    ],
    dataset_splitters: [
        train_test_splitter: &train_test_splitter 
        !obj:motor_imagery_learning.mypylearn2.dataset_splitters.DatasetSingleFoldSplitter {
            dataset: *train,
            num_folds: 10,
            test_fold_nr: $test_fold_nr,
        },
        two_file_splitter: &two_file_splitter
         !obj:motor_imagery_learning.mypylearn2.dataset_splitters.DatasetTwoFileSingleFoldSplitter {
            train_set: *train,
            test_set: *test,
            num_folds: 10,
         }
    ],
    layers: [
        flat: &flat [
             !obj:pylearn2.models.mlp.RectifiedLinear {
                 layer_name: 'first_layer',
                 dim: $hidden_neurons,
                 sparse_init: 15,
                 max_col_norm: $max_col_norm_hidden_layer
             }, !obj:pylearn2.models.mlp.Softmax {
                 layer_name: 'y',
                 n_classes: 4,
                 irange: 0.5,
                 max_col_norm: $max_col_norm_final_layer
             }
        ],
        conv: &conv [
            !obj:pylearn2.models.mlp.ConvRectifiedLinear {
                 layer_name: 'first_layer',
                 output_channels: $output_channels_conv,
                 irange: .05,
                 kernel_shape: [$kernel_shape_time, $kernel_shape_freq],
                 kernel_stride: [$kernel_stride_time, $kernel_stride_freq],
                 pool_shape: [$pool_shape_time, $pool_shape_freq],
                 pool_stride: [$pool_stride_time, $pool_stride_freq],
                 max_kernel_norm: $max_kernel_norm,
                 tied_b: true, #!! default is false in pylearn...
             },
              !obj:pylearn2.models.mlp.RectifiedLinear {
                 layer_name: 'second_layer',
                 dim: $hidden_neurons,
                 sparse_init: 15,
                 max_col_norm: $max_col_norm_hidden_layer,
             },
              !obj:pylearn2.models.mlp.Softmax {
                 max_col_norm: $max_col_norm_final_layer,
                 layer_name: 'y',
                 n_classes: 4,
                 istdev: .05,
             }
        ],
          
        fb_net: &fb_net [
          !obj:mypylearn2.conv_squared.ConvSquared {
             layer_name: 'conv_spatial_squared',
             output_channels: $output_channels,
             irange: .05,
             kernel_shape: [1, 1],
             pool_shape: [1, 1],
             pool_stride: [1, 1],
             pool_type: $pool_type,
             max_kernel_norm: $max_kernel_norm,
             tied_b: true,
         },
          !obj:pylearn2.models.mlp.Softmax {
             layer_name: 'y',
             n_classes: 4,
             istdev: .05,
             max_col_norm: $max_col_norm,
         }
        ],
        raw_net: &raw_net [
            !obj:pylearn2.models.mlp.ConvElemwise {
                 layer_name: 'bandpass',
                 output_channels: $bp_chans,
                 irange: .05,
                 kernel_shape: [$bp_kernel_shape_time, 1],
                 kernel_stride: [$bp_kernel_stride_time, 1],
                 pool_type: null,
                 pool_shape: [1, 1],
                 pool_stride: [1, 1],
                 max_kernel_norm: $bp_max_kernel_norm,
                 tied_b: true,
                 nonlinearity: !obj:pylearn2.models.mlp.$bp_nonlinearity { },
         },
        !obj:motor_imagery_learning.mypylearn2.conv_squared.ConvSquaredFullHeight {
             layer_name: 'spatial_filt',
             output_channels: $spatial_chans,
             irange: .05,
             kernel_shape: [1,-1],
             kernel_stride: [1,1],
             pool_type: null,
             pool_shape: [-1,-1],
             pool_stride: [-1,-1],
             max_kernel_norm: $square_max_kernel_norm,
             tied_b: true,
         },
         !obj:motor_imagery_learning.mypylearn2.pool.PoolLayer {
            layer_name: 'square_pool',
            pool_shape: [$square_pool_shape_time, 1],
            pool_stride: [$square_pool_stride_time, 1],
            pool_func: !!python/name:motor_imagery_learning.mypylearn2.pool.$pool_func
         },
          !obj:pylearn2.models.mlp.Softmax {
             layer_name: 'y',
             n_classes: 4,
             istdev: .05,
             max_col_norm: $max_col_norm,
         }
      ]
    ],
    weight_initializers: {
        sine_initializer: &sine_initializer !obj:motor_imagery_learning.mypylearn2.weight_initializers.SineWeightInitializer {
            layer_names: ['bandpass'],
            sampling_freq: $resampling_fs,
        },
    },
    model: &model !obj:pylearn2.models.mlp.MLP {
        layers: $layers,
        seed: [2013, 1, 4], # taken from pylearn source
    },
    costs: {
        default_costs: &default_costs !obj:pylearn2.costs.mlp.Default {},
        # TODO: factor out the include probs and input_scales and then you could
        # merge both using the << syntax? maybe also too confusing ;)
        dropout_flat_costs: &dropout_flat_costs !obj:pylearn2.costs.mlp.dropout.Dropout {
                input_include_probs: { 
                    'first_layer' : $first_layer_input_include_probs,
                    'y': $final_layer_input_include_probs,
                },
                input_scales: { 
                    'first_layer': $first_layer_input_scales,
                    'y': $final_layer_input_scales,
                }
            },
        dropout_conv_costs: &dropout_conv_costs !obj:pylearn2.costs.mlp.dropout.Dropout {
                input_include_probs: { 
                    'first_layer' : $first_layer_input_include_probs,
                    'second_layer': $full_second_layer_input_include_probs,
                    'y': $final_layer_input_include_probs,
                },
                input_scales: {
                    'first_layer': $first_layer_input_scales,
                    'second_layer': $full_second_layer_input_scales,
                    'y': $final_layer_input_scales,
                }
            }
        ,
        dropout_fbnet: &dropout_fbnet !obj:pylearn2.costs.mlp.dropout.Dropout {
                    input_include_probs: { 
                        'conv_spatial_squared' : $input_first_probs,
                        'y' : $input_softmax_probs,
                    },
                    input_scales: {
                        'conv_spatial_squared': $input_first_scales,
                        'y': $input_softmax_scales,
                    },
            },
        dropout_raw_net: &dropout_raw_net !obj:pylearn2.costs.mlp.dropout.Dropout {
            input_include_probs: { 
                'bandpass' : $input_first_probs,
                'spatial_filt': $input_spatial_probs,
                'square_pool': 1.0,
                'y' : $input_softmax_probs,
            },
            input_scales: { 
                'bandpass': $input_first_scales,
                'spatial_filt': $input_spatial_scales,
                'square_pool': 1.0,
                'y': $input_softmax_scales,
            },
        },
    },
    learning_rules: {
        learning_rule: &momentum_rule !obj:motor_imagery_learning.mypylearn2.learning_rule_adam.Momentum {
            init_momentum: $momentum
        },
        learning_rule: &adam !obj:motor_imagery_learning.mypylearn2.learning_rule_adam.Adam {
           alpha: $adam_alpha, 
           beta1: $adam_beta1, 
           beta2: $adam_beta2
        },
        learning_rule: &rms_prop !obj:pylearn2.training_algorithms.learning_rule.RMSProp {
            decay: $rms_prop_decay,
            max_scaling: $rms_prop_max_scaling,
        },
    },
    termination_criteria: {
        termination_criterion: &early_stop_and_max_epochs !obj:pylearn2.termination_criteria.And {
            criteria: [
                !obj:pylearn2.termination_criteria.EpochCounter {
                    max_epochs: $max_epochs,
                },
                !obj:pylearn2.termination_criteria.MonitorBased {
                        channel_name: $stopping_channel,
                        prop_decrease: $termination_decrease,
                        N: $max_increasing_epochs
                }
            ]
        },
        termination_criterion: &max_epochs_only !obj:pylearn2.termination_criteria.EpochCounter {
            max_epochs: $max_epochs
        },
        termination_criterion: &running_min_and_max_epochs !obj:pylearn2.termination_criteria.And {
            criteria: [
                !obj:pylearn2.termination_criteria.EpochCounter {
                    max_epochs: $max_epochs,
                },
                !obj:motor_imagery_learning.mypylearn2.termination_criteria.MonitorBasedRunningMeanVarMin {
                        channel_name: $stopping_channel,
                        min_N: $min_epochs,
                        init_block_size: 20,
                        factor_new: $factor_new_termination,
                }
            ]
        },
    },
    bgd: &bgd !obj:pylearn2.training_algorithms.bgd.BGD {
        verbose_optimization: 0,
        batch_size: $batch_size,
        line_search_mode: 'exhaustive',
        conjugate: true,
        updates_per_batch: $updates_per_batch,
        cost: $costs,
        termination_criterion: $termination_criterion,
        seed: [2012, 10, 16],  # taken from pylearn source
    },
    sgd: &sgd !obj:mypylearn2.sgd.SimilarBatchSizeSGD {
        batch_transformer: $batch_transformer,
        batch_size: $batch_size,
        learning_rate: $learning_rate,
        cost: $costs,
        learning_rule: $learning_rule,
        termination_criterion: $termination_criterion,
        seed: [2012, 10, 5], # taken from pylearn source
        update_callbacks: $sgd_callbacks,
    },
    training_cross_validation_early_stop: &training_cross_validation_early_stop !obj:mypylearn2.train_cv_sequential_folds.TrainCVEarlyStop {
        dataset: *train,
        num_folds: $cross_val_folds,
        keep_best_lrule_params:  $keep_best_lrule_params,
        fraction_epochs_after_stop: $fraction_after_stop,
        model: *model,
        algorithm: $algorithm,
        save_path: $save_path,
        save_freq: $save_freq,
        preprocessor: $preprocessor,
        weight_initializer: $weight_initializer,
    },
    training_test_set: &training_test_set !obj:motor_imagery_learning.mypylearn2.train.TrainEarlyStop {
        dataset_splitter: $dataset_splitter,
        model: *model,
        algorithm: $algorithm,
        # for practical purposes not saving best but last model, as early stop is
        # very unreliable...
        save_path: $save_path,
        preprocessor: $preprocessor,
        keep_best_lrule_params: $keep_best_lrule_params, 
        best_model_channel: $best_model_channel,
        fraction_epochs_after_stop: $fraction_after_stop,
        weight_initializer: $weight_initializer,
    },
# Has to be redone since there were many code changes
#    training_transfer: &training_transfer !obj:mypylearn2.train_transfer.get_train_transfer_eeg {
#        leave_out: $transfer_leave_out,
#        dataset_args: {
#            <<: *train,
#            filenames: $transfer_filenames, # prevent loading of data before
#            start: $transfer_start,
#            stop: $transfer_stop
#        },
#        preprocessor: $preprocessor,
#        model: *model,
#        algorithm: $algorithm,
#        # for practical purposes not saving best but last model, as early stop is
#        # very unreliable...
#        save_path: $save_path,
#        transfer_learning_rate: $transfer_learning_rate,
#        inner_folds: $transfer_inner_folds,
#    },
    training_object: $training_type
}