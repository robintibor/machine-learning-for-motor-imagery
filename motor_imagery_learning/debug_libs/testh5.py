import shutil
import tempfile
 
import h5py
 
tmpdir = tempfile.mkdtemp()
i = 0
try:
    with h5py.File("test.h5", "w") as f:
        f.create_dataset("test", data=[[1,2],[3,4]], compression="gzip", shuffle=True)
    with h5py.File("test.h5", "r") as f:
        while True:
            f["test"][...]
            i += 1
finally:
    shutil.rmtree(tmpdir)
    print(i)
