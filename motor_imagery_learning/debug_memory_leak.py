import os
import site
site.addsitedir('/home/schirrmr/.local/lib/python2.7/site-packages/')
site.addsitedir('/usr/lib/pymodules/python2.7/')
import os
os.sys.path.insert(0, '/home/schirrmr/motor-imagery/code/')
assert 'THEANO_FLAGS' in os.environ
# switch to cpu
os.environ['THEANO_FLAGS'] = 'floatX=float32,device=cpu,nvcc.fastmath=True'
#fakechange

import numpy as np
import h5py
import time
from motor_imagery_learning.util import memory_usage
import psutil
import gc
gc.collect()
print "memory at start", memory_usage(), psutil.virtual_memory().percent
h5file = h5py.File('data/BBCI-without-last-runs/BhNoMoSc1S001R01_ds10_1-12.BBCI.mat',
     'r')#, driver='stdio')
for chan_i in [1,2,3,4,5,6,7,8,9,10,11,12]:
    chan = np.copy(h5file['ch' + str(chan_i)][0,:])
h5file.close()
del h5file
print "memory after load", memory_usage(), psutil.virtual_memory().percent
del chan
for i in xrange(5):
    gc.collect()
    time.sleep(0.1)
    #print "memory after del in loop", memory_usage()
print "memory after del", memory_usage(), psutil.virtual_memory().percent


