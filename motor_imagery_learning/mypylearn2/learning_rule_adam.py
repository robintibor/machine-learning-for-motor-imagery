import numpy as np
import theano

from theano import tensor as T
from theano.compat.python2x import OrderedDict

from pylearn2.train_extensions import TrainExtension
from pylearn2.training_algorithms.learning_rule import LearningRule
from pylearn2.utils import sharedX, wraps
from pylearn2.space import NullSpace
import six

class Adam(LearningRule):
    def __init__(self, alpha=2e-4, beta1=1e-1, beta2=1e-3, epsilon=10e-8):
        self.__dict__.update(locals())
        del self.self

        self.grad = OrderedDict()
        self.sq_grad = OrderedDict()
        self.time = sharedX(1, dtype='int64', name='z_timestep')

    @wraps(LearningRule.add_channels_to_monitor)
    def add_channels_to_monitor(self, monitor, monitoring_dataset):
        monitor.add_channel(name='z_time', ipt=None, val=self.time.astype(theano.config.floatX),
                data_specs=(NullSpace(), ''), dataset=monitoring_dataset)

    def get_updates(self, learning_rate, grads, lr_scalers=None):
        """ Get updates according to the adam algorithm.
        Ignores sgd learning rate!"""
        updates = OrderedDict()

        for param in grads:
            # Initialize the theano vectors for the estimated gradient and 
            # the squared gradient
            grad = sharedX(param.get_value() * 0.0)
            grad.name = 'z_grad_' + param.name
            self.grad[grad.name] = grad

            sq_grad = sharedX(param.get_value() * 0.0)
            sq_grad.name = 'z_sq_grad_' + param.name
            self.sq_grad[sq_grad.name] = sq_grad
            
            new_grad = self.beta1 * grads[param] + (1.0 - self.beta1) * grad
            new_sq_grad = self.beta2 * T.sqr(grads[param]) + (1.0 - self.beta2) * sq_grad

            #grad_corr = new_grad / (1.0 - (1.0 - self.beta1)**self.time)
            #sq_grad_corr = new_sq_grad / (1.0 - (1.0 - self.beta2)**self.time)
            # Why is it not float32? Epsilon probably doesn't fit into one
            #updates[param] = (param - self.alpha * \
            #    grad_corr / (T.sqrt(sq_grad_corr) + self.epsilon)).astype('float32')

            # More efficient version
            updates[param] = (param - (self.alpha * T.sqrt(1.0 - (1.0 - self.beta2)**self.time) * \
                    (1.0 - (1.0 - self.beta1)**self.time)**(-1.0)) * \
                    (new_grad / (T.sqrt(new_sq_grad) + self.epsilon))).astype(theano.config.floatX)
            updates[grad] = new_grad
            updates[sq_grad] = new_sq_grad

        updates[self.time] = self.time + 1

        return updates
    
class Momentum(LearningRule):
    """
    Difference to pylearn: velocities are member values, 
    to reconstruct them for best model after early stop
    Implements momentum as described in Section 9 of
    "A Practical Guide to Training Restricted Boltzmann Machines",
    Geoffrey Hinton.

    Parameters are updated by the formula:
    inc := momentum * inc - learning_rate * d cost / d param
    param := param + inc

    Parameters
    ----------
    init_momentum : float
        Initial value for the momentum coefficient. It remains fixed during
        training unless used with a `MomentumAdjustor`
        extension.
    nesterov_momentum: bool
        Use the accelerated momentum technique described in:
        "Advances in Optimizing Recurrent Networks", Yoshua Bengio, et al.

    """

    def __init__(self, init_momentum, nesterov_momentum=False):
        assert init_momentum >= 0.
        assert init_momentum < 1.
        self.momentum = sharedX(init_momentum, 'momentum')
        self.nesterov_momentum = nesterov_momentum
        self.vels = OrderedDict()

    def add_channels_to_monitor(self, monitor, monitoring_dataset):
        """
        Activates monitoring of the momentum.

        Parameters
        ----------
        monitor : pylearn2.monitor.Monitor
            Monitor object, to which the rule should register additional
            monitoring channels.
        monitoring_dataset : pylearn2.datasets.dataset.Dataset or dict
            Dataset instance or dictionary whose values are Dataset objects.
        """
        monitor.add_channel(
            name='momentum',
            ipt=None,
            val=self.momentum,
            data_specs=(NullSpace(), ''),
            dataset=monitoring_dataset)

    def get_updates(self, learning_rate, grads, lr_scalers=None):
        """
        Provides the updates for learning with gradient descent + momentum.

        Parameters
        ----------
        learning_rate : float
            Learning rate coefficient.
        grads : dict
            A dictionary mapping from the model's parameters to their
            gradients.
        lr_scalers : dict
            A dictionary mapping from the model's parameters to a learning
            rate multiplier.
        """

        updates = OrderedDict()

        for (param, grad) in six.iteritems(grads):
            vel = sharedX(param.get_value() * 0.)
            assert param.dtype == vel.dtype
            assert grad.dtype == param.dtype
            if param.name is not None:
                vel.name = 'vel_' + param.name
            self.vels[vel.name] = vel# for later reconstruction after early stop

            scaled_lr = learning_rate * lr_scalers.get(param, 1.)
            updates[vel] = self.momentum * vel - scaled_lr * grad

            inc = updates[vel]
            if self.nesterov_momentum:
                inc = self.momentum * inc - scaled_lr * grad

            assert inc.dtype == vel.dtype
            updates[param] = param + inc

        return updates