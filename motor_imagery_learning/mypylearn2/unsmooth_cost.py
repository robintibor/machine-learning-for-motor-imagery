from functools import wraps
import theano.tensor as T
import theano
from pylearn2.costs.cost import DefaultDataSpecsMixin, Cost
from theano.sandbox.cuda.dnn import dnn_available
from theano.sandbox.cuda import cuda_enabled
# get correct conv2d, either theano or cudnn
if cuda_enabled and dnn_available():
    from theano.sandbox.cuda.dnn import dnn_conv 
else:
    import theano.tensor.nnet.conv
def conv2d(inputs, filters, filter_stride):
    if cuda_enabled and dnn_available():
        return dnn_conv(img=inputs, kerns=filters, subsample=filter_stride, border_mode='valid')
    else:
        return theano.tensor.nnet.conv.conv2d(inputs, filters, 
           image_shape=None, filter_shape=None,
            subsample=filter_stride, border_mode='valid')
        
# all functions here assume weights are matrices i.e. #filters x #time
def moving_avg_theano(weights):
    weights_expanded = weights.dimshuffle(0,'x',1,'x')
    filters = T.constant([0.5,0,0.5], dtype='floatX').dimshuffle('x', 'x', 0, 'x')
    average = conv2d(weights_expanded, filters, filter_stride=(1,1))
    return average[:,0,:,0] # assumes average has only two dimensions: #weights x #time

def weight_unsmoothness_moving_avg_theano(weights):
    average = moving_avg_theano(weights)
    return T.sum(T.square(weights[:,1:-1] - average), axis=(1))


def weight_unsmoothness_moving_avg_scaled_theano(weights):
    unsmoothness = weight_unsmoothness_moving_avg_theano(weights)
    return unsmoothness / T.sum(T.square(weights), axis=(1))#T.sum(T.square(weights[:,1:-1]), axis=(1))

def weight_unsmoothness_moving_avg_no_square_theano(weights):
    average = moving_avg_theano(weights)
    return T.sum(T.abs_(weights[:,1:-1] - average), axis=(1))

def weight_unsmoothness_moving_avg_no_square_scaled_theano(weights):
    unsmoothness = weight_unsmoothness_moving_avg_no_square_theano(weights)
    return unsmoothness / T.sum(T.abs_(weights), axis=(1))#T.sum(T.square(weights[:,1:-1]), axis=(1))

def weight_unsmoothness_diff_square_theano(weights):
    return T.sum(T.square(weights[:, 1:] - weights[:, :-1]), axis=(1)) 

def weight_unsmoothness_diff_square_scaled_theano(weights):
    return T.sum(T.square(weights[:, 1:] - weights[:, :-1]), axis=(1)) / T.sum(T.square(weights), axis=(1))#T.sum(T.square(weights[:,1:-1]), axis=(1))

class UnsmoothCost(DefaultDataSpecsMixin, Cost):
    """
    
    Parameters
    ----------
    coeffs : dict
        Dictionary with layer names as its keys,
        specifying the coefficient to multiply
        with the cost defined by the unsmoothness penalty of the weights for
        each layer.
    """

    def __init__(self, coeffs):
        self.coeffs = coeffs

    @wraps(Cost.expr)
    def expr(self, model, data, ** kwargs):
        summed_costs = T.constant(0, dtype='floatX')
        all_layer_names = [l.layer_name for l in model.layers]
        assert all([name in all_layer_names for name in self.coeffs]), ("All "
          "layer names given in dictionary should exist")
        for layer in model.layers:
            layer_name = layer.layer_name
            if layer_name in self.coeffs:
                coeff = self.coeffs[layer_name]
                W, _= layer.get_params()
                #TODELAY: do this more elegant
                assert W.eval().shape[1] == 1
                assert W.eval().shape[3] == 1
                unsmooth_cost = weight_unsmoothness_moving_avg_scaled_theano(
                    W[:,0,:,0]) * coeff
                summed_costs += T.sum(unsmooth_cost)
        return summed_costs

    @wraps(Cost.is_stochastic)
    def is_stochastic(self):
        return False
