from theano.sandbox.cuda.dnn import dnn_available
from theano.sandbox.cuda import cuda_enabled
from pylearn2.utils import isfinite, contains_inf, sharedX
from theano.gof.op import get_debug_values
import numpy as np
import theano.tensor as T
from pylearn2.models.mlp import Layer, BadInputSpaceError
from functools import wraps
from collections import OrderedDict
from pylearn2.space import Conv2DSpace
import theano
import logging
# hack: make logging appear for output space
logger = logging.getLogger('pylearn2.mlp')

def use_dnn():
    if theano.config.device.startswith('gpu'):
        if theano.sandbox.cuda.dnn.dnn_available() and theano.sandbox.cuda.cuda_enabled:
            return True
    return False
if use_dnn():
    from theano.sandbox.cuda.dnn import dnn_pool


def max_pool(bc01, pool_shape, pool_stride, image_shape, try_dnn=True):
    """ Difference to pylearn2: valid pooling
    Theano's max pooling op only supports pool_stride = pool_shape
    so here we have a graph that does max pooling with strides

    Parameters
    ----------
    bc01 : theano tensor
        minibatch in format (batch size, channels, rows, cols)
    pool_shape : tuple
        shape of the pool region (rows, cols)
    pool_stride : tuple
        strides between pooling regions (row stride, col stride)
    image_shape : tuple
        avoid doing some of the arithmetic in theano
    try_dnn : bool
        Flag to set cuDNN use (default: True).

    Returns
    -------
    pooled : theano tensor
        The output of pooling applied to `bc01`

    See Also
    --------
    max_pool_c01b : Same functionality but with ('c', 0, 1, 'b') axes
    sandbox.cuda_convnet.pool.max_pool_c01b : Same functionality as
        `max_pool_c01b` but GPU-only and considerably faster.
    mean_pool : Mean pooling instead of max pooling
    """

    if try_dnn and bc01.dtype == "float32":
        use_dnn = dnn_available() and cuda_enabled
    else:
        use_dnn = False

    if use_dnn:
        return dnn_pool(bc01, pool_shape, pool_stride, 'max')


    if bc01.name is None:
        bc01.name = 'bc01_for_pooling'
    
    max_pooled_bc01 = None
    image_rows, image_cols = image_shape
    row_stride, col_stride = pool_stride
    pool_rows, pool_cols = pool_shape


    for bc01v in get_debug_values(bc01):
        assert not contains_inf(bc01v)
        assert bc01v.shape[2] == image_shape[0]
        assert bc01v.shape[3] == image_shape[1]

        

    for row_within_pool in xrange(pool_shape[0]):
        row_stop = image_rows - pool_rows + row_within_pool + 1
        for col_within_pool in xrange(pool_shape[1]):
            col_stop = image_cols - pool_cols + col_within_pool + 1
            cur = bc01[:,
                       :,
                       row_within_pool:row_stop:row_stride,
                       col_within_pool:col_stop:col_stride]
            cur.name = ('max_pool_cur_' + bc01.name)
            if max_pooled_bc01 is None:
                max_pooled_bc01 = cur
            else:
                max_pooled_bc01 = T.maximum(max_pooled_bc01, cur)
                max_pooled_bc01.name = ('max_pool_mx_' + bc01.name)
            

    max_pooled_bc01.name = 'max_pool(' + bc01.name + ')'

    for max_pooled_bc01val in get_debug_values(max_pooled_bc01):
        assert isfinite(max_pooled_bc01val)

    return max_pooled_bc01

def mean_pool(bc01, pool_shape, pool_stride, image_shape, try_dnn=True):
    """
    Difference to pylearn2: valid pooling
    Does mean pooling (aka average pooling) via a Theano graph.

    Parameters
    ----------
    bc01 : theano tensor
        minibatch in format (batch size, channels, rows, cols)
    pool_shape : tuple
        shape of the pool region (rows, cols)
    pool_stride : tuple
        strides between pooling regions (row stride, col stride)
    image_shape : tuple
        (rows, cols) tuple to avoid doing some arithmetic in theano

    Returns
    -------
    pooled : theano tensor
        The output of pooling applied to `bc01`

    See Also
    --------
    max_pool : Same thing but with max pooling

    Examples
    --------
    >>> import theano
    >>> import theano.tensor as T
    >>> from pylearn2.models.mlp import mean_pool
    >>> import numpy as np
    >>> t = np.array([[1, 1, 3, 3],
    ...               [1, 1, 3, 3],
    ...               [5, 5, 7, 7],
    ...               [5, 5, 7, 7],
    ...               [9, 9, 11, 11],
    ...               [9, 9, 11, 11]])

    >>> X = np.zeros((3, t.shape[0], t.shape[1]))
    >>> X[:] = t
    >>> X = X[np.newaxis]
    >>> X_sym = T.tensor4('X')
    >>> pool_it = mean_pool(X_sym, pool_shape=(2, 2), pool_stride=(2, 2),
    ...                     image_shape=(6, 4))
    >>> f = theano.function(inputs=[X_sym], outputs=pool_it)

    This will pool over over windows of size (2, 2) while also stepping by this
    same amount, shrinking the examples input to [[1, 3], [5, 7], [9, 11]].
    """
    if try_dnn and bc01.dtype == "float32":
        use_dnn = dnn_available() and cuda_enabled
    else:
        use_dnn = False

    if use_dnn:
        return dnn_pool(bc01, ws=pool_shape, stride=pool_stride, 
            mode='average_exc_pad')

    sum_pooled_bc01 = None
    image_rows, image_cols = image_shape
    row_stride, col_stride = pool_stride
    pool_rows, pool_cols = pool_shape

    if bc01.name is None:
        bc01.name = 'bc01_for_pooling'

    for bc01v in get_debug_values(bc01):
        assert not contains_inf(bc01v)
        assert bc01v.shape[2] == image_shape[0]
        assert bc01v.shape[3] == image_shape[1]

    for row_within_pool in xrange(pool_shape[0]):
        row_stop = image_rows - pool_rows + row_within_pool + 1
        for col_within_pool in xrange(pool_shape[1]):
            col_stop = image_cols - pool_cols + col_within_pool + 1
            cur = bc01[:,
                       :,
                       row_within_pool:row_stop:row_stride,
                       col_within_pool:col_stop:col_stride]
            cur.name = ('mean_pool_cur_' + bc01.name)
            if sum_pooled_bc01 is None:
                sum_pooled_bc01 = cur
            else:
                sum_pooled_bc01 = sum_pooled_bc01 + cur
                sum_pooled_bc01.name = ('mean_pool_mx_' + bc01.name)
            
    pool_size = np.prod(pool_shape).astype(np.float32)

    mean_pooled_bc01 = sum_pooled_bc01 / pool_size
    mean_pooled_bc01.name = 'mean_pool(' + bc01.name + ')'

    for mean_pooled_bc01val in get_debug_values(mean_pooled_bc01):
        assert isfinite(mean_pooled_bc01val)

    return mean_pooled_bc01

def root_sum(bc01, pool_shape, pool_stride, image_shape):
    pool_size = np.prod(pool_shape) 
    pooled = mean_pool(bc01, pool_shape, pool_stride, image_shape)
    pooled = T.sqrt(pooled * np.float32(pool_size))
    return pooled

def root_mean(bc01, pool_shape, pool_stride, image_shape):
    pooled = mean_pool(bc01, pool_shape, pool_stride, image_shape)
    pooled = T.sqrt(pooled)
    return pooled

def log_sum(bc01, pool_shape, pool_stride, image_shape):
    pool_size = np.prod(pool_shape) 
    pooled = mean_pool(bc01=bc01, pool_shape=pool_shape,
        pool_stride=pool_stride, image_shape=image_shape)
    # should be equivalent to log of sum....
    # as we have mean before, p * pool_size
    # should recover sum
    # cast size to float32 to prevent upcast to float64 of entire data
    # with max prevent log(0)
    eps = 1e-4
    pooled = T.log(T.maximum(eps, pooled * np.float32(pool_size)))
    return pooled

def log_mean(bc01, pool_shape, pool_stride, image_shape):
    pooled = mean_pool(bc01=bc01, pool_shape=pool_shape,
        pool_stride=pool_stride, image_shape=image_shape)
    # with max prevent log(0)
    eps = 1e-6
    pooled = T.log(T.maximum(eps, pooled))
    return pooled

def log_sum_all(bc01, pool_shape, pool_stride, image_shape):
    eps = 1e-6
    summed = T.sum(bc01, axis=2,  keepdims=True)
    pooled = T.log(T.maximum(eps, summed))
    return pooled

def log_mean_all(bc01, pool_shape, pool_stride, image_shape):
    eps = 1e-6
    meaned = T.mean(bc01, axis=2,  keepdims=True)
    pooled = T.log(T.maximum(eps, meaned))
    return pooled

def mean_all(bc01, pool_shape, pool_stride, image_shape):
    meaned = T.mean(bc01, axis=2,  keepdims=True)
    return meaned

class PoolLayer(Layer):

    def __init__(self,
                 pool_shape,
                 pool_stride,
                 layer_name,
                 pool_func):

        self.pool_shape = tuple(pool_shape)
        self.pool_stride = tuple(pool_stride)
        self.layer_name = layer_name
        self.pool_func = pool_func
        super(PoolLayer, self).__init__()


    def initialize_output_space(self):
        """
        Initializes the output space by doing dummy pooling.
        """
        dummy_batch_size = self.mlp.batch_size

        if dummy_batch_size is None:
            dummy_batch_size = 2
        dummy_input =\
            sharedX(self.input_space.get_origin_batch(dummy_batch_size))

        dummy_pooled = self.pool_func(bc01=dummy_input,
            pool_shape=self.pool_shape,
            pool_stride=self.pool_stride,
            image_shape=self.input_space.shape)
        
        dummy_pooled = dummy_pooled.eval()
        self.output_space = Conv2DSpace(shape=[dummy_pooled.shape[2],
                                            dummy_pooled.shape[3]],
                                            num_channels=self.input_space.num_channels,
                                            axes=('b', 'c', 0, 1))

        logger.info('Pool Output shape: {0}'.format(self.output_space.shape))

    @wraps(Layer.set_input_space)
    def set_input_space(self, space):
        self.input_space = space

        assert self.input_space.axes == ('b', 'c', 0, 1)
        if not isinstance(space, Conv2DSpace):
            raise BadInputSpaceError(self.__class__.__name__ +
                                     ".set_input_space "
                                     "expected a Conv2DSpace, got " +
                                     str(space) + " of type " +
                                     str(type(space)))
        logger.info('Pool Input shape: {0}'.format(self.input_space.shape))


        self.initialize_output_space()

    @wraps(Layer.get_params)
    def get_params(self):
        return []


    @wraps(Layer.get_layer_monitoring_channels)
    def get_layer_monitoring_channels(self, state_below=None,
                                      state=None, targets=None):

        rval = OrderedDict()
        return rval

    @wraps(Layer.fprop)
    def fprop(self, state_below):
        self.input_space.validate(state_below)
        pooled = self.pool_func(bc01=state_below,
            pool_shape=self.pool_shape,
            pool_stride=self.pool_stride,
            image_shape=self.input_space.shape)
        return pooled
