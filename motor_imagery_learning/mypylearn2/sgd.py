from pylearn2.training_algorithms.sgd import SGD
from pylearn2.utils import isfinite, wraps
from pylearn2.space import CompositeSpace
from pylearn2.utils.data_specs import DataSpecsMapping
from pylearn2.utils.iteration import (
    is_stochastic,
    FiniteDatasetIterator,
    SequentialSubsetIterator,
    SubsetIterator
)
from pylearn2.utils.rng import make_np_rng
from pylearn2.utils import safe_zip
import numpy as np

def create_iterator(dataset, batch_size, data_specs,
            return_tuple, rng):
    
        # If there is a view_converter, we have to use it to convert
        # the stored data for "features" into one that the iterator
        # can return.
        space, source = data_specs
        if isinstance(space, CompositeSpace):
            sub_spaces = space.components
            sub_sources = source
        else:
            sub_spaces = (space,)
            sub_sources = (source,)

        convert = []
        for sp, src in safe_zip(sub_spaces, sub_sources):
            if src == 'features' and \
               getattr(dataset, 'view_converter', None) is not None:
                conv_fn = (lambda batch, dataset=dataset, space=sp:
                           dataset.view_converter.get_formatted_batch(batch,
                                                                   space))
            else:
                conv_fn = None

            convert.append(conv_fn)

        assert batch_size is not None, "expect batch size to be specified " + \
            "in sgd"
        assert not hasattr(dataset, '_iter_num_batches'), \
            "dataset should not specify number of batches as dataset iterator" + \
            "will compute them"
        assert rng is not None, "expect rng to be given from sgd"
        return FiniteDatasetIterator(dataset,
                                     ShuffledSequentialBalancedBatchSizeSubsetIterator(
                                         dataset_size=dataset.X.shape[0],
                                          batch_size=batch_size,
                                          num_batches=None,
                                          rng=rng),
                                     data_specs=data_specs,
                                     return_tuple=return_tuple,
                                     convert=convert)
        


class ShuffledSequentialBalancedBatchSizeSubsetIterator(SequentialSubsetIterator):
    """ This is similar to ShuffledSequentialSubsetIterator, but with 
    balanced batch sizes.
    It uses batches which are similar in size:
    For given training samples and given batch size, it creates 
    floor(#samples/#batchsize) batches. 
    There should be floor(#samples/#batches) examples which can fit into the batches
    equally and mod(#samples, #batches) remaining samples (< #batches).
    These remaining samples will be split equally amoung the first
    mod(#samples, #batches) batches (each of these batches getting one more sample.
    So the maximum batch size difference is 1.
    Training samples are also randomly shuffled before batches with the given size
    are sequentially taken (so everything same as pylearn2 shuffledsequentialmode, except
    for the sizes of the batches).
    
    This is inspired by scikit-learns K-Fold Split:
    https://github.com/scikit-learn/scikit-learn/blob/38104ff4e8f1c9c39a6f272eff7818b75a27da46/sklearn/cross_validation.py#L266

    Notes
    -----
    Returns lists of indices (`fancy = True`).

    See :py:class:`SubsetIterator` for detailed constructor parameter
    and attribute documentation.
    """
    stochastic = True
    fancy = True
    uniform_batch_size = False

    def __init__(self, dataset_size, batch_size, num_batches, rng=None):
        
        super(ShuffledSequentialBalancedBatchSizeSubsetIterator, self).__init__(
            dataset_size,
            batch_size,
            num_batches,
            None
        )
        self._rng = make_np_rng(rng, which_method=["random_integers",
                                                   "shuffle"])
        self._shuffled = np.arange(self._dataset_size)
        self._rng.shuffle(self._shuffled)
        # Create batches with balanced sizes, 
        # see class documentation
        self._num_batches = int(np.floor(dataset_size /  batch_size))
        # need atleast one batch
        self._num_batches = int(np.max((1, self._num_batches)))
        # This will be minimum size for all batches
        min_batch_size = int(np.floor(dataset_size / self._num_batches))
        self._batch_sizes = np.ones(self._num_batches, dtype=np.int) * \
            min_batch_size
        # some batches will get one more sample, in case
        # dataset size is not exactly divisible by number of batches
        self._batch_sizes[:dataset_size % self._num_batches] += 1
        
        # all samples should be distributed
        assert np.sum(self._batch_sizes) == dataset_size
        # batches should be balanced, test by checking that
        # all batch sizes are maximum 1 different from the first batch
        assert np.all(self._batch_sizes[0] - self._batch_sizes <= 1)
        assert np.all(self._batch_sizes[0] - self._batch_sizes >= 0)
        
        
    @wraps(SubsetIterator.next)
    def next(self):
        if self._batch >= self.num_batches or self._idx >= self._dataset_size:
            raise StopIteration()
        batch_size = int(self._batch_sizes[self._batch])
        rval = self._shuffled[self._idx:self._idx + batch_size]
        self._idx += batch_size
        self._batch += 1
        return rval

    def __next__(self):
        return self.next()     


class SimilarBatchSizeSGD(SGD):
    """ This class is the same as the pylearn SGD, except using a different
    iterator for the subsets, see ShuffledSequentialBalancedBatchSizeSubsetIterator.
    """
    def __init__(self, batch_transformer=None, **kwargs):
        self.batch_transformer = batch_transformer
        super(SimilarBatchSizeSGD, self).__init__(**kwargs)
        
        
    def train(self, dataset):
        """
        Runs one epoch of SGD training on the specified dataset.

        Parameters
        ----------
        dataset : Dataset
        """
        if not hasattr(self, 'sgd_update'):
            raise Exception("train called without first calling setup")

        # Make sure none of the parameters have bad values
        for param in self.params:
            value = param.get_value(borrow=True)
            if not isfinite(value):
                raise Exception("NaN in " + param.name)

        self.first = False
        rng = self.rng
        if not is_stochastic(self.train_iteration_mode):
            rng = None

        data_specs = self.cost.get_data_specs(self.model)

        # The iterator should be built from flat data specs, so it returns
        # flat, non-redundent tuples of data.
        mapping = DataSpecsMapping(data_specs)
        space_tuple = mapping.flatten(data_specs[0], return_tuple=True)
        source_tuple = mapping.flatten(data_specs[1], return_tuple=True)
        if len(space_tuple) == 0:
            # No data will be returned by the iterator, and it is impossible
            # to know the size of the actual batch.
            # It is not decided yet what the right thing to do should be.
            raise NotImplementedError(
                "Unable to train with SGD, because "
                "the cost does not actually use data from the data set. "
                "data_specs: %s" % str(data_specs))
        flat_data_specs = (CompositeSpace(space_tuple), source_tuple)

        assert self.batches_per_iter is None, \
            "Expecting batchees per iter to be none, " + \
            "since we ignore the parameter"

        iterator = create_iterator(dataset,
            batch_size=self.batch_size, data_specs=flat_data_specs,
            return_tuple=True, rng=rng)

        on_load_batch = self.on_load_batch
        for batch in iterator:
            for callback in on_load_batch:
                callback(*batch)
            if self.batch_transformer is not None:
                batch = self.batch_transformer.transform(batch)
            self.sgd_update(*batch)
            # iterator might return a smaller batch if dataset size
            # isn't divisible by batch_size
            # Note: if data_specs[0] is a NullSpace, there is no way to know
            # how many examples would actually have been in the batch,
            # since it was empty, so actual_batch_size would be reported as 0.
            actual_batch_size = flat_data_specs[0].np_batch_size(batch)
            self.monitor.report_batch(actual_batch_size)
            for callback in self.update_callbacks:
                callback(self)

        # Make sure none of the parameters have bad values
        for param in self.params:
            value = param.get_value(borrow=True)
            if not isfinite(value):
                raise Exception("NaN in " + param.name)