from pylearn2.train_extensions import TrainExtension
import numpy as np
from copy import deepcopy, copy

class MonitorBasedRememberBest(TrainExtension):
    """
    A callback that stores a copy of the model in memory and the
    algorithm  every time it achieves a new
    minimal value of a monitoring channel. 

    Parameters
    ----------
    channel_name : str
        The name of the monitor channel we want to minimize.
    """
    def __init__(self, channel_name):
        self.channel_name = channel_name
        # placeholders
        self.best_epoch = None
        self.best_val = float('inf')
        self.best_model = None
        self.best_learning_rule = None
        

    def setup(self, model, dataset, algorithm):
        pass

    def on_monitor(self, model, dataset, algorithm):
        """
        Looks whether the model performs better than earlier. If it's the
        case, saves the modeal.

        Parameters
        ----------
        model : pylearn2.models.model.Model
            model.monitor must contain a channel with name given by
            self.channel_name
        dataset : pylearn2.datasets.dataset.Dataset
            Not used
        algorithm : TrainingAlgorithm
            Not used
        """
        monitor = model.monitor
        channels = monitor.channels
        channel = channels[self.channel_name]
        val_record = channel.val_record
        new_val = val_record[-1]
        if new_val <= self.best_val:
            self.best_epoch = len(val_record) - 1
            # deepcopy model without monitor
            best_model = copy(model)
            del best_model.monitor
            self.best_model = deepcopy(best_model)
            if hasattr(algorithm, 'learning_rule'):
                self.best_learning_rule = deepcopy(algorithm.learning_rule)
            self.best_val = new_val