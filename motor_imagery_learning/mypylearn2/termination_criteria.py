import numpy as np
from pylearn2.termination_criteria import TerminationCriterion


class MonitorBasedFractional(TerminationCriterion):
    """
    A termination criterion that pulls out the specified channel in
    the model's monitor and checks to see if it has decreased by a
    certain proportion of the lowest value in the last N epochs, where N
    is the maximum of a given minimum number of epochs to look back and a
    a fraction of the epochs until the best value.

    Parameters
    ----------
    prop_decrease : float
        The threshold factor by which we expect the channel value to have
        decreased
    min_N : int
        Minimum number of epochs to look back
    fraction_of_lowest_epoch : float
        What fraction of the epochs until the lowest value was reached to look back.
        For example, for 0.5 we will wait atleast half the number of epochs
        it took to reach the current lowest value.
    channel_name : string, optional
        Name of the channel to examine. If None and the monitor
        has only one channel, this channel will be used; otherwise, an
        error will be raised.
    """

    def __init__(self, prop_decrease=.01, min_N=5, fraction_of_lowest_epoch=0.5,
            channel_name=None):
        self._channel_name = channel_name
        self.prop_decrease = prop_decrease
        self.min_N = min_N
        self.countdown = min_N
        self.best_value = np.inf
        self.fraction_of_lowest_epoch = fraction_of_lowest_epoch

    def continue_learning(self, model):
        """
        The optimization should stop if the model has run for
        N epochs without sufficient improvement.

        Parameters
        ----------
        model : Model
            The model used in the experiment and from which the monitor
            used in the termination criterion will be extracted.

        Returns
        -------
        bool
            True if training should continue
        """
        monitor = model.monitor
        # In the case the monitor has only one channel, the channel_name can
        # be omitted and the criterion will examine the only channel
        # available. However, if the monitor has multiple channels, leaving
        # the channel_name unspecified will raise an error.
        if self._channel_name is None:
            v = monitor.channels['objective'].val_record
        else:
            v = monitor.channels[self._channel_name].val_record

        # The countdown decreases every time the termination criterion is
        # called unless the channel value is lower than the best value times
        # the prop_decrease factor, in which case the countdown is reset to N
        # and the best value is updated
        if v[-1] < (1. - self.prop_decrease) * self.best_value:
            self.countdown = np.max((self.min_N, 
                self.fraction_of_lowest_epoch * monitor.get_epochs_seen()))
            self.best_value = v[-1]
        else:
            self.countdown = self.countdown - 1

        # The optimization continues until the countdown has reached 0,
        # meaning that N epochs have passed without the model improving
        # enough.
        return self.countdown > 0
    
    
class MonitorBasedRunningVarMean(TerminationCriterion):
    """
    A termination criterion that pulls out the specified channel in
    the model's monitor and computes a running mean and variance
    of the differences between the epochs of this channel's value.
    If this std-adjusted mean goes above a certain threshold,
    learning should stop.

    Parameters
    ----------
    threshold : float
        The threshold, if std-adjusted mean is above this, it leads to a stop.
    init_block_size: int
        Init block for first estimate of mean and variance of differences
    min_N : int
        Minimum number of epochs before starting to check the criterion
    channel_name : string, optional
        Name of the channel to examine. If None and the monitor
        has only one channel, this channel will be used; otherwise, an
        error will be raised.
    """

    def __init__(self, threshold=0.0, min_N=50, init_block_size=20,
            channel_name=None, factor_new=0.02):
        self.threshold = threshold
        self.min_N = min_N
        self.init_block_size = init_block_size
        self._channel_name = channel_name
        self.factor_new = factor_new
        self.initial_vals = np.ones(init_block_size) * np.nan
        self.last_val = None
        self.current_diff_nr = 0
        self.current_var = None
        self.current_mean = None

    def continue_learning(self, model):
        """
        The optimization should stop if the running std-adjusted mean diff
        is above the threshold

        Parameters
        ----------
        model : Model
            The model used in the experiment and from which the monitor
            used in the termination criterion will be extracted.

        Returns
        -------
        bool
            True if training should continue
        """
        monitor = model.monitor
        new_val= monitor.channels[self._channel_name].val_record[-1]
        if self.last_val is not None:
            diff_val = new_val - self.last_val
            above_threshold=False
            # Just collect vals
            if (self.current_diff_nr < self.init_block_size):
                self.initial_vals[self.current_diff_nr] = diff_val
            # Compute first mean/var
            elif (self.current_diff_nr == self.init_block_size):
                assert not np.any(np.isnan(self.initial_vals)), ("Should "
                    "have values for all diffs to compute the initial variance"
                    " and mean")
                self.current_mean = np.mean(self.initial_vals)
                self.current_var = np.var(self.initial_vals)
                # TODELAY: instead use std, so np.sqrt(self.current_var?)
                # NOTE: if chaning also change in else case below
                std_adjusted_mean = self.current_mean / np.sqrt(self.current_var)
                above_threshold = std_adjusted_mean > self.threshold
            # Compute running mean/var
            else:
                assert self.current_var is not None
                assert self.current_mean is not None
                self.current_mean = self.factor_new * diff_val + (
                    (1 - self.factor_new) * self.current_mean)
                new_var = (diff_val - self.current_mean) ** 2
                self.current_var = self.factor_new * new_var + (
                    (1- self.factor_new) * self.current_var)
                std_adjusted_mean = self.current_mean / np.sqrt(self.current_var)
                above_threshold = std_adjusted_mean > self.threshold
            self.current_diff_nr += 1
            self.last_val = new_val
            enough_epochs_seen = monitor.get_epochs_seen() > self.min_N
            return (not above_threshold) or (not enough_epochs_seen)
        else: # nothing computed yet, first epoch
            assert self.current_diff_nr == 0
            self.last_val = new_val
            return True


class MonitorBasedRunningMeanVarMin(TerminationCriterion):
    
    def __init__(self, min_N=50, init_block_size=20,
            channel_name=None, factor_new=0.02):
        self.min_N = min_N
        self.init_block_size = init_block_size
        self._channel_name = channel_name
        self.factor_new = factor_new
        self.initial_vals = np.ones(init_block_size) * np.nan
        self.current_var = None
        self.current_mean = None
        self.current_val_nr = 0
        self.best_mean = float('inf')

    def continue_learning(self, model):
        """
        The optimization should stop if the running std-adjusted mean diff
        is above the threshold

        Parameters
        ----------
        model : Model
            The model used in the experiment and from which the monitor
            used in the termination criterion will be extracted.

        Returns
        -------
        bool
            True if training should continue
        """
        monitor = model.monitor
        new_val= monitor.channels[self._channel_name].val_record[-1]
        above_threshold=False
        # Just collect vals
        if (self.current_val_nr < self.init_block_size):
            self.initial_vals[self.current_val_nr] = new_val
            above_threshold=False
        else:
            # Compute first mean/var
            if (self.current_val_nr == self.init_block_size):
                assert not np.any(np.isnan(self.initial_vals)), ("Should "
                    "have values for all diffs to compute the initial variance"
                    " and mean")
                self.current_mean = np.mean(self.initial_vals)
                self.current_var = np.var(self.initial_vals)
            # Compute running mean/var
            else:
                assert self.current_var is not None
                assert self.current_mean is not None
                self.current_mean = self.factor_new * new_val + (
                    (1 - self.factor_new) * self.current_mean)
                new_var = (new_val - self.current_mean) ** 2
                self.current_var = self.factor_new * new_var + (
                    (1- self.factor_new) * self.current_var)
            current_std = np.sqrt(self.current_var)
            above_threshold = self.current_mean > self.best_mean + current_std
            if (self.current_mean < self.best_mean):
                self.best_mean = self.current_mean
        self.current_val_nr += 1
        enough_epochs_seen = monitor.get_epochs_seen() > self.min_N
        return (not above_threshold) or (not enough_epochs_seen)
