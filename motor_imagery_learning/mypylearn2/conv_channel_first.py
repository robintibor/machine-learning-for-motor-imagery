from theano.sandbox.cuda import cuda_enabled
from theano.sandbox.cuda.dnn import dnn_available, dnn_pool
if cuda_enabled and dnn_available():
    try:
        from theano.sandbox.cuda.dnn import dnn_conv as conv2d
    except ImportError:
        from theano.tensor.nnet.conv import conv2d
else:
    from theano.tensor.nnet.conv import conv2d
from pylearn2.linear.linear_transform import LinearTransform as P2LT
import functools
from theano.sandbox.cuda.basic_ops import gpu_contiguous, gpu_alloc_empty

from pylearn2.utils import sharedX
from pylearn2.utils.rng import make_np_rng
from pylearn2.models.mlp import ConvElemwise
import theano.tensor as T

class ConvChanKernelDot(ConvElemwise):
    def initialize_transformer(self, rng):
        """
        This function initializes the transformer of the class. Re-running
        this function will reset the transformer.
 
        Parameters
        ----------
        rng : object
            random number generator object.
        """
        if self.irange is not None:
            assert self.sparse_init is None
            self.transformer = make_seperable_conv(
                irange=self.irange,
                input_space=self.input_space,
                output_space=self.detector_space,
                kernel_shape=self.kernel_shape,
                subsample=self.kernel_stride,
                border_mode=self.border_mode,
                rng=rng)
    

default_seed = [2015, 6,14]
def make_seperable_conv(irange, input_space, output_space,
        kernel_shape, batch_size=None,
        subsample=(1, 1), border_mode='valid', message='', rng=None):
    rng = make_np_rng(rng, default_seed, which_method='uniform')

    # Flatten weights
    # because we want to unfold them later
    # otherwise we would have a lot of unused weights
    # (and we would have to either set them to zero 
    # or take care of col norm or sth)
    W = sharedX(rng.uniform(
        -irange, irange,
        (output_space.num_channels, 
            input_space.num_channels + kernel_shape[0] * kernel_shape[1],
            1, 1)
    ))

    return ConvChanKernelDotConv(
        filters=W,
        batch_size=batch_size,
        input_space=input_space,
        output_axes=output_space.axes,
        subsample=subsample,
        border_mode = border_mode,
        kernel_shape = kernel_shape
    )
    
class ConvChanKernelDotConv():
    """ Convolution which makes separate convolutions for each input channel.
    It is expected that there is exactly one filter for each input channel."""
    def __init__(self, filters, batch_size, input_space, kernel_shape,
            # default output axes left from pylearn... 
            # TODELAY: change to bc01?
            output_axes=('b', 0, 1, 'c'), subsample=(1,1), 
            border_mode='valid'):
        self.filters = filters
        self.input_space = input_space
        self.output_axes = output_axes
        # if not made into tuple, get unhashable type(list) errors...
        self.subsample = tuple(subsample) 
        self.border_mode = border_mode
        self.folded_filt_shape = filters.get_value(borrow=True).shape
        self.kernel_shape = kernel_shape
        
    def lmul(self, x):
        assert x.ndim == 4
        axes = self.input_space.axes
        assert len(axes) == 4

        op_axes = ('b', 'c', 0, 1)

        if tuple(axes) != op_axes:
            x = x.dimshuffle(
                    axes.index('b'),
                    axes.index('c'),
                    axes.index(0),
                    axes.index(1))

        x = gpu_contiguous(x)
        # Unfold weights first
        num_chans = self.input_space.num_channels
        channel_weights = self.filters[:, :num_chans,0,0]

        kernel_weights = self.filters[:, num_chans:,0,0]
        kernel_weights = kernel_weights.reshape((self.folded_filt_shape[0],
            self.kernel_shape[0], self.kernel_shape[1]))

        unfolded_filters = T.batched_tensordot(channel_weights.dimshuffle(0,1,'x'),
            kernel_weights.dimshuffle(0,'x', 1,2), axes=((1), (0)))

        rval = conv2d(x, unfolded_filters, subsample=self.subsample,
            border_mode=self.border_mode)

        axes = self.output_axes
        assert len(axes) == 4

        if tuple(axes) != op_axes:
            rval = rval.dimshuffle(
                op_axes.index(axes[0]),
                op_axes.index(axes[1]),
                op_axes.index(axes[2]),
                op_axes.index(axes[3])
            )

        return rval
    @functools.wraps(P2LT.get_params)
    def get_params(self):
        """
        Returns filters.
        -------
        """
        return [self.filters]

class ConvOneFiltPerChan(ConvElemwise):
    def initialize_transformer(self, rng):
        """
        This function initializes the transformer of the class. Re-running
        this function will reset the transformer.
 
        Parameters
        ----------
        rng : object
            random number generator object.
        """
        if self.irange is not None:
            assert self.sparse_init is None
            self.transformer = make_conv_keeping_chans(
                irange=self.irange,
                input_space=self.input_space,
                output_space=self.detector_space,
                kernel_shape=self.kernel_shape,
                subsample=self.kernel_stride,
                border_mode=self.border_mode,
                rng=rng)
    

default_seed = [2015, 6,14]
def make_conv_keeping_chans(irange, input_space, output_space,
        kernel_shape, batch_size=None,
        subsample=(1, 1), border_mode='valid', message='', rng=None):
    rng = make_np_rng(rng, default_seed, which_method='uniform')

    assert output_space.num_channels == input_space.num_channels
    W = sharedX(rng.uniform(
        -irange, irange,
        (output_space.num_channels,1, kernel_shape[0],kernel_shape[1])
    ))

    return ConvOneFiltPerChanConv(
        filters=W,
        batch_size=batch_size,
        input_space=input_space,
        output_axes=output_space.axes,
        subsample=subsample,
        border_mode = border_mode
    )
    
class ConvOneFiltPerChanConv():
    """ Convolution which makes separate convolutions for each input channel.
    It is expected that there is exactly one filter for each input channel."""
    def __init__(self, filters, batch_size, input_space,
            output_axes=('b', 0, 1, 'c'),subsample=(1,1), 
            border_mode='valid'):
        assert len(filters.eval()) == input_space.num_channels
        self._filters = filters
        self.input_space = input_space
        self.output_axes = output_axes
        # if not made into tuple, get unhashable type(list) errors...
        self.subsample = tuple(subsample) 
        self.border_mode = border_mode
        
    def lmul(self, x):
        assert x.ndim == 4
        axes = self.input_space.axes
        assert len(axes) == 4

        op_axes = ('b', 'c', 0, 1)


        if tuple(axes) != op_axes:
            x = x.dimshuffle(
                    axes.index('b'),
                    axes.index('c'),
                    axes.index(0),
                    axes.index(1))

        x = gpu_contiguous(x)

        rval = convolve_keeping_chans(x, self._filters, 
                self.input_space.num_channels,
            subsample=self.subsample, border_mode=self.border_mode)

        axes = self.output_axes
        assert len(axes) == 4

        if tuple(axes) != op_axes:
            rval = rval.dimshuffle(
                op_axes.index(axes[0]),
                op_axes.index(axes[1]),
                op_axes.index(axes[2]),
                op_axes.index(axes[3])
            )

        return rval
    @functools.wraps(P2LT.get_params)
    def get_params(self):
        """
        Returns filters and biases.
        Returns
        -------
        filters : theano shared variable
        bias : theano shared variable
        """
        return [self._filters]

def convolve_keeping_chans(inputs, filters, numfilters, subsample,
        border_mode):
    # inputs shape should be #batches #virtualchans x #0 x #1
    # loop through filters, make convolutions for 1-dimensional chans
    outshape = compute_out_shape(inputs.shape, filters.shape)
    rval = gpu_alloc_empty(inputs.shape[0], numfilters, outshape[2], outshape[3])
    for filter_i in range(numfilters):
        conved_by_filter_i = conv2d(inputs[:,filter_i:filter_i+1,:,:], 
            filters[filter_i:filter_i+1,:,:,:], subsample=subsample,
            border_mode=border_mode)
        rval = T.set_subtensor(rval[:,filter_i:filter_i+1,:,:], conved_by_filter_i)
    return rval
    
def compute_out_shape(inputs_shape, filters_shape):
    num_batches = inputs_shape[0]
    out_height = inputs_shape[2] - filters_shape[2] + 1;
    out_width = inputs_shape[3] - filters_shape[3] + 1;
    num_filters = filters_shape[0]
    return (num_batches, num_filters, out_height, out_width)