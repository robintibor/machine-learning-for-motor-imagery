from theano.sandbox.cuda import cuda_enabled
from theano.sandbox.cuda.dnn import dnn_available
if cuda_enabled and dnn_available():
    try:
        from theano.sandbox.cuda.dnn import dnn_conv as conv2d
    except ImportError:
        from theano.tensor.nnet.conv import conv2d
else:
    from theano.tensor.nnet.conv import conv2d
from pylearn2.linear.linear_transform import LinearTransform as P2LT
import functools
from theano.sandbox.cuda.basic_ops import gpu_contiguous

from pylearn2.utils import sharedX
from pylearn2.utils.rng import make_np_rng
from pylearn2.models.mlp import ConvElemwise
import theano.tensor as T
import theano
import numpy as np
from motor_imagery_learning.mypylearn2.conv_switch_axes import convert_conv_space

def init_bp_transformer(layer, rng): 
    """
    This function initializes the transformer of the class. Re-running
    this function will reset the transformer.

    Parameters
    ----------
    rng : object
        random number generator object.
    """
    if layer.irange is not None:
        assert layer.sparse_init is None
        layer.transformer = make_bandpass_conv(
            irange=layer.irange,
            input_space=layer.input_space,
            output_space=layer.detector_space,
            kernel_shape=layer.kernel_shape,
            subsample=layer.kernel_stride,
            border_mode=layer.border_mode,
            rng=rng)


class ConvBandpass(ConvElemwise):
    def initialize_transformer(self, rng):
        """
        This function initializes the transformer of the class. Re-running
        this function will reset the transformer.
 
        Parameters
        ----------
        rng : object
            random number generator object.
        """
        if self.irange is not None:
            assert self.sparse_init is None
            self.transformer = make_bandpass_conv(
                irange=self.irange,
                input_space=self.input_space,
                output_space=self.detector_space,
                kernel_shape=self.kernel_shape,
                subsample=self.kernel_stride,
                border_mode=self.border_mode,
                rng=rng)
    

default_seed = [2015, 6,14]
def make_bandpass_conv(irange, input_space, output_space,
        kernel_shape, batch_size=None,
        subsample=(1, 1), border_mode='valid', message='', rng=None):
    rng = make_np_rng(rng, default_seed, which_method='uniform')

    # make uniformly spread bandpass filters as start values
    if (irange < 0):
        # HACK:
        # irange below 0 to indicate we dont want to use random weights
        assert irange == -1
        start_freqs = np.linspace(0.01,0.8, output_space.num_channels)
        filter_vals = np.array([start_freqs, start_freqs + 0.1]).T
        filter_vals = filter_vals[:,:,np.newaxis, np.newaxis]
        W = sharedX(filter_vals)
    else:
        W = sharedX(rng.uniform(
            0, irange,
            (output_space.num_channels, 
                2, 
                1, 1)# 2 for start and stop
        ))

    return ConvBandpassConv(
        filters=W,
        batch_size=batch_size,
        input_space=input_space,
        output_axes=output_space.axes,
        subsample=subsample,
        border_mode = border_mode,
        kernel_shape = kernel_shape
    )
    
class ConvBandpassConv():
    """ Convolution which makes separate convolutions for each input channel.
    It is expected that there is exactly one filter for each input channel."""
    def __init__(self, filters, batch_size, input_space, kernel_shape,
            # default output axes left from pylearn... 
            # TODELAY: change to bc01?
            output_axes=('b', 0, 1, 'c'), subsample=(1,1), 
            border_mode='valid'):
        self._filters = filters
        self.input_space = input_space
        self.output_axes = output_axes
        # if not made into tuple, get unhashable type(list) errors...
        self.subsample = tuple(subsample) 
        self.border_mode = border_mode
        self.folded_filt_shape = filters.get_value(borrow=True).shape
        self.kernel_shape = kernel_shape
        assert input_space.num_channels == 1, ("inchans should be 1 for now... "
            "expecting chans to be in height dimension(1)")
        assert kernel_shape[1] == 1, ("For now kernel height should be 0.")
        
    def lmul(self, x):
        assert x.ndim == 4
        axes = self.input_space.axes
        assert len(axes) == 4

        op_axes = ('b', 'c', 0, 1)

        if tuple(axes) != op_axes:
            x = x.dimshuffle(
                    axes.index('b'),
                    axes.index('c'),
                    axes.index(0),
                    axes.index(1))

        x = gpu_contiguous(x)
        
        # Unfold weights first
        conv_bandpass_weights = create_all_bp_coeffs_start_len(
            self._filters[:,:,0,0], numtaps=self.kernel_shape[0])

        rval = conv2d(x, conv_bandpass_weights.dimshuffle(0, 'x', 1, 'x'), 
            subsample=self.subsample,
            border_mode=self.border_mode)

        axes = self.output_axes
        assert len(axes) == 4

        if tuple(axes) != op_axes:
            rval = rval.dimshuffle(
                op_axes.index(axes[0]),
                op_axes.index(axes[1]),
                op_axes.index(axes[2]),
                op_axes.index(axes[3])
            )

        return rval
    @functools.wraps(P2LT.get_params)
    def get_params(self):
        """
        Returns filters.
        -------
        """
        return [self._filters]
    

def create_all_bp_coeffs_start_len(start_len_per_filter, numtaps):
    # Generate the components of the polynomial
    eps = 1e-4
    start_len_per_filter = T.minimum(T.maximum(start_len_per_filter, eps), 
        1 - eps)
    start_stops_per_filter = T.set_subtensor(start_len_per_filter[:,1],
        start_len_per_filter[:,0] + start_len_per_filter[:,1])
    start_stops_per_filter = T.minimum(start_stops_per_filter, 1 - eps)
    components = create_all_bp_coeffs(start_stops_per_filter, numtaps)
    return components

def create_all_filters_all_bp_coeffs(start_stops_per_filter, numtaps):
    # Generate the components of the polynomial
    components, _ = theano.scan(fn=lambda start_stops: create_all_bp_coeffs(start_stops, numtaps),
                                      outputs_info=None,
                                      sequences=[start_stops_per_filter])
    return components

def create_all_bp_coeffs(start_stops, numtaps):
    # Generate the components of the polynomial
    components, _ = theano.scan(fn=lambda start_stop: create_bp_coeffs(start_stop[0], start_stop[1], numtaps),
                                      outputs_info=None,
                                      sequences=[start_stops])
    return components

def create_bp_coeffs(start, stop, numtaps):
    alpha = np.float32(0.5) * np.float32(numtaps - 1)
    m = np.float32(np.arange(0, numtaps) - alpha)
    coeffs = np.float32(0) 

    coeffs -= start * T.sin(start * m * np.pi) / (start * m * np.pi) 
    coeffs += stop * T.sin(stop * m * np.pi) / (stop * m * np.pi)
    return coeffs



class ConvBandpassSwitchAxes(ConvBandpass):
    def __init__(self,
                 output_channels,
                 kernel_shape,
                 layer_name,
                 switch_axes,
                 **kwargs):
        """ Remaining keyword arguments same as ConvSquared/ConvElemwise."""
        self.switch_axes = switch_axes
        #TODELAY: force that all needed arguments for super constructor are given,
        # e.g. pool shape/stride etc?
        super(ConvBandpassSwitchAxes, self).__init__(
            output_channels=output_channels,
            kernel_shape=kernel_shape,
            layer_name=layer_name, **kwargs)

    def set_input_space(self, space):
        assert tuple(space.axes) == tuple(self.switch_axes[0])
        space = convert_conv_space(space, self.switch_axes[0],
            self.switch_axes[1])
        super(ConvBandpassSwitchAxes, self).set_input_space(space)
    