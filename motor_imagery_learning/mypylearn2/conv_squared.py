import theano
from pylearn2.models.mlp import ConvElemwise
from pylearn2.models.mlp import ConvNonlinearity, Layer, \
    BadInputSpaceError, get_debug_values#, max_pool, mean_pool
from motor_imagery_learning.mypylearn2.pool import max_pool, mean_pool
from pylearn2.utils import wraps
from pylearn2.utils import sharedX
import theano.tensor as T
from pylearn2.space import Conv2DSpace
import logging
from pylearn2.utils.rng import make_np_rng
from pylearn2.linear.conv2d import Conv2D
import numpy as np
from motor_imagery_learning.mypylearn2.conv_switch_axes import convert_conv_space
# hack: make logging appear for output space
logger = logging.getLogger('pylearn2.mlp')
def use_dnn():
    if theano.config.device.startswith('gpu'):
        if theano.sandbox.cuda.dnn.dnn_available() and theano.sandbox.cuda.cuda_enabled:
            return True
    return False
if use_dnn():
    from theano.sandbox.cuda.dnn import dnn_pool


class SquaredConvNonlinearity(ConvNonlinearity):
    """
    A simple squared nonlinearity class for convolutional layers.
    """
    def __init__(self):
        self.non_lin_name = "squared"

    @wraps(ConvNonlinearity.apply)
    def apply(self, linear_response):
        """
        Sauares the linear output of the convolutional layer.
        """
        p = T.sqr(linear_response)
        return p


class ConvSquared(ConvElemwise):
    """
    A convolutional squared layer, based on theano's B01C
    formatted convolution.

    Parameters
    ----------
    output_channels : int
        The number of output channels the layer should have.
    kernel_shape : tuple
        The shape of the convolution kernel.
    pool_shape : tuple
        The shape of the spatial max pooling. A two-tuple of ints.
    pool_stride : tuple
        The stride of the spatial max pooling. Also must be square.
    layer_name : str
        A name for this layer that will be prepended to monitoring channels
        related to this layer.
    irange : float
        if specified, initializes each weight randomly in
        U(-irange, irange)
    border_mode : str
        A string indicating the size of the output:

        - "full" : The output is the full discrete linear convolution of the
            inputs.
        - "valid" : The output consists only of those elements that do not
            rely on the zero-padding. (Default)

    include_prob : float
        probability of including a weight element in the set of weights
        initialized to U(-irange, irange). If not included it is initialized
        to 0.
    init_bias : float
        All biases are initialized to this number
    W_lr_scale : float
        The learning rate on the weights for this layer is multiplied by this
        scaling factor
    b_lr_scale : float
        The learning rate on the biases for this layer is multiplied by this
        scaling factor
    max_kernel_norm : float
        If specifed, each kernel is constrained to have at most this norm.
    pool_type :
        The type of the pooling operation performed the the convolution.
        Default pooling type is max-pooling.
    tied_b : bool
        If true, all biases in the same channel are constrained to be the
        same as each other. Otherwise, each bias at each location is
        learned independently.
    detector_normalization : callable
        See `output_normalization`
    output_normalization : callable
        if specified, should be a callable object. the state of the
        network is optionally replaced with normalization(state) at each
        of the 3 points in processing:

        - detector: the rectifier units can be normalized prior to the
            spatial pooling
        - output: the output of the layer, after spatial pooling, can
            be normalized as well

    kernel_stride : tuple
        The stride of the convolution kernel. A two-tuple of ints.
    """

    def __init__(self,
                 output_channels,
                 kernel_shape,
                 pool_shape,
                 pool_stride,
                 layer_name,
                 transformer_func=None,
                 irange=None,
                 border_mode='valid',
                 sparse_init=None,
                 include_prob=1.0,
                 init_bias=0.,
                 W_lr_scale=None,
                 b_lr_scale=None,
                 left_slope=0.0,
                 max_kernel_norm=None,
                 pool_type='max',
                 tied_b=True,
                 detector_normalization=None,
                 output_normalization=None,
                 kernel_stride=(1, 1),
                 monitor_style="classification"):
        #maybe remove assertion here since I also put it in mlp.py of pylearn?
        assert tied_b == True, "Sure you dont want tied biases? :)"
        # prevent errors due to unhasbiblity of lists
        # by converting to tuples..
        kernel_shape = tuple(kernel_shape)
        kernel_stride = tuple(kernel_stride)
        pool_shape = tuple(pool_shape)
        pool_stride = tuple(pool_stride)
        self.transformer_func = transformer_func

        nonlinearity = SquaredConvNonlinearity()

        if (irange is None) and (sparse_init is None):
            raise AssertionError("You should specify either irange or "
                                 "sparse_init when calling the constructor of "
                                 "ConvRectifiedLinear.")
        elif (irange is not None) and (sparse_init is not None):
            raise AssertionError("You should specify either irange or "
                                 "sparse_init when calling the constructor of "
                                 "ConvRectifiedLinear and not both.")

        # Alias the variables for pep8
        mkn = max_kernel_norm
        dn = detector_normalization
        on = output_normalization
        super(ConvSquared, self).__init__(output_channels,
                                                  kernel_shape,
                                                  layer_name,
                                                  nonlinearity,
                                                  irange=irange,
                                                  border_mode=border_mode,
                                                  sparse_init=sparse_init,
                                                  include_prob=include_prob,
                                                  init_bias=init_bias,
                                                  W_lr_scale=W_lr_scale,
                                                  b_lr_scale=b_lr_scale,
                                                  pool_shape=pool_shape,
                                                  pool_stride=pool_stride,
                                                  max_kernel_norm=mkn,
                                                  pool_type=pool_type,
                                                  tied_b=tied_b,
                                                  detector_normalization=dn,
                                                  output_normalization=on,
                                                  kernel_stride=kernel_stride,
                                                  monitor_style=monitor_style)
        
    
    def initialize_output_space(self):
        """"
        Initializes the output space of the ConvElemwise layer by taking
        pooling operator and the hyperparameters of the convolutional layer
        into consideration as well.
        """
        dummy_batch_size = self.mlp.batch_size

        if dummy_batch_size is None:
            dummy_batch_size = 2
        dummy_detector =\
            sharedX(self.detector_space.get_origin_batch(dummy_batch_size))

        if self.pool_type is not None:
            assert self.pool_type in ['max', 'mean', 'maxall',
                'meanall', 'sumlog', 'sumlogall']
            if self.pool_type == 'max':
                dummy_p = max_pool(bc01=dummy_detector,
                                   pool_shape=self.pool_shape,
                                   pool_stride=self.pool_stride,
                                   image_shape=self.detector_space.shape)
            elif self.pool_type == 'mean':
                # HACK HERE, ignores pool shape / stride(!)
                # Compute mean, third axis(=2) should be timesample axis
                dummy_p = mean_pool(bc01=dummy_detector,
                                   pool_shape=self.pool_shape,
                                   pool_stride=self.pool_stride,
                                   image_shape=self.detector_space.shape)
            elif self.pool_type == 'maxall':
                # HACK HERE, ignores pool shape / stride(!)
                # Compute mean, third axis(=2) should be timesample axis
                dummy_p = T.max(dummy_detector, axis=2,  keepdims=True)
            elif self.pool_type == 'meanall':
                # HACK HERE, ignores pool shape / stride(!)
                # Compute mean, third axis(=2) should be timesample axis
                dummy_p = T.mean(dummy_detector, axis=2,  keepdims=True)
            elif self.pool_type == 'sumlog':
                dummy_p = pool_log_sum(dummy_detector, pool_shape=self.pool_shape, 
                    pool_stride=self.pool_stride, 
                    image_shape=self.detector_space.shape)
            elif self.pool_type == 'sumlogall':
                dummy_p = T.log(T.sum(dummy_detector, axis=2,
                    keepdims=True))
            else:
                raise NotImplementedError("Unknown pool type {:s}".format(
                    self.pool_type))
                    
                    
            dummy_p = dummy_p.eval()
            self.output_space = Conv2DSpace(shape=[dummy_p.shape[2],
                                                   dummy_p.shape[3]],
                                            num_channels=self.output_channels,
                                            axes=('b', 'c', 0, 1))
        else:
            dummy_detector = dummy_detector.eval()
            self.output_space = Conv2DSpace(shape=[dummy_detector.shape[2],
                                            dummy_detector.shape[3]],
                                            num_channels=self.output_channels,
                                            axes=('b', 'c', 0, 1))

        logger.info('Output space: {0}'.format(self.output_space.shape))
        
    @wraps(Layer.fprop)
    def fprop(self, state_below):

        self.input_space.validate(state_below)

        z = self.transformer.lmul(state_below)
        if not hasattr(self, 'tied_b'):
            self.tied_b = False

        if self.tied_b:
            b = self.b.dimshuffle('x', 0, 'x', 'x')
        else:
            b = self.b.dimshuffle('x', 0, 1, 2)

        z = z + b
        d = self.nonlin.apply(z)

        if self.layer_name is not None:
            d.name = self.layer_name + '_z'
            self.detector_space.validate(d)

        if self.pool_type is not None:
            if not hasattr(self, 'detector_normalization'):
                self.detector_normalization = None

            if self.detector_normalization:
                d = self.detector_normalization(d)

            assert self.pool_type in ['max', 'mean', 'maxall',
                'meanall', 'sumlog', 'sumlogall']

            if self.pool_type == 'max':
                p = max_pool(bc01=d, pool_shape=self.pool_shape,
                             pool_stride=self.pool_stride,
                             image_shape=self.detector_space.shape)
            elif self.pool_type == 'mean':
                p = mean_pool(bc01=d, pool_shape=self.pool_shape,
                             pool_stride=self.pool_stride,
                             image_shape=self.detector_space.shape)
            elif self.pool_type == 'maxall':
                p = T.max(d, axis=2,  keepdims=True)
            elif self.pool_type == 'meanall':
                p = T.mean(d, axis=2,  keepdims=True)
            elif self.pool_type == 'sumlog':
                p = pool_log_sum(d, pool_shape=self.pool_shape, 
                    pool_stride=self.pool_stride, 
                    image_shape=self.detector_space.shape)
            elif self.pool_type == 'sumlogall':
                p = T.log(T.sum(d, axis=2,  keepdims=True))
            self.output_space.validate(p)
        else:
            p = d

        if not hasattr(self, 'output_normalization'):
            self.output_normalization = None

        if self.output_normalization:
            p = self.output_normalization(p)

        return p
    
    def initialize_transformer(self, rng):
        if (self.transformer_func is None):
            super(ConvSquared, self).initialize_transformer(rng)
        else:
            self.transformer_func(self, rng)

""" TODO(Robin): DELETE    
def pool_log_sum(bc01, pool_shape, pool_stride, image_shape):
    pool_size = np.prod(pool_shape) 
    pool_shape = tuple(pool_shape)
    pool_stride = tuple(pool_stride)
    if use_dnn():
        pooled = dnn_pool(bc01, ws=pool_shape, stride=pool_stride, 
            mode='average_exc_pad')
    else:
        pooled = mean_pool(bc01=bc01, pool_shape=pool_shape,
            pool_stride=pool_stride, image_shape=image_shape, try_dnn=False)
    # should be equivalent to log of sum....
    # as we have mean before, p * pool_size
    # should recover sum
    # cast size to float32 to prevent upcast to float64 of entire data
    # with max prevent log(0)
    eps = 1e-6
    pooled = T.log(T.maximum(eps, pooled * np.float32(pool_size)))
    return pooled
"""

class ConvSquaredDot(ConvSquared):
    """ Use dot product instead of convolution. Maybe faster? """

    def initialize_transformer(self, rng):
        """
        This function initializes the transformer of the class. Re-running
        this function will reset the transformer.
 
        Parameters
        ----------
        rng : object
            random number generator object.
        """
        if self.irange is not None:
            assert self.sparse_init is None
            self.transformer = make_spatial_filter(
                irange=self.irange,
                input_space=self.input_space,
                output_space=self.detector_space,
                kernel_shape=self.kernel_shape,
                subsample=self.kernel_stride,
                border_mode=self.border_mode,
                rng=rng)

default_seed = [2015, 6,14]
def make_spatial_filter(irange, input_space, output_space,
        kernel_shape, batch_size=None,
        subsample=(1, 1), border_mode='valid', message='', rng=None):
    rng = make_np_rng(rng, default_seed, which_method='uniform')

    assert tuple(subsample) == (1,1)
    assert border_mode =='valid'
    assert tuple(kernel_shape) == (1,1)
    W = sharedX(rng.uniform(
        -irange, irange,
        (output_space.num_channels,input_space.num_channels, 1,1)
    ))

    return SpatialFilter(
        filters=W,
        batch_size=batch_size,
        input_space=input_space,
        output_axes=output_space.axes,
    )
    
class SpatialFilter(Conv2D):
    def __init__(self, filters, batch_size, input_space,
            output_axes=('b', 0, 1, 'c')):
        self._filters = filters
        self.input_space = input_space
        self.output_axes = output_axes
        
        #super(SpatialFilter, self).__init__(filters, batch_size, input_space,
        #        output_axes, subsample, border_mode, filters_shape, message)

        
    def lmul(self, x):
        assert x.ndim == 4
        axes = self.input_space.axes
        assert len(axes) == 4

        op_axes = ('b', 0, 1, 'c')


        if tuple(axes) != op_axes:
            x = x.dimshuffle(
                    axes.index('b'),
                    axes.index(0),
                    axes.index(1),
                    axes.index('c'))

        #x = gpu_contiguous(x)

        rval = T.dot(x, self._filters[:,:,0,0].T)

        axes = self.output_axes
        assert len(axes) == 4

        if tuple(axes) != op_axes:
            rval = rval.dimshuffle(
                op_axes.index(axes[0]),
                op_axes.index(axes[1]),
                op_axes.index(axes[2]),
                op_axes.index(axes[3])
            )

        return rval



class ConvSquaredFullHeight(ConvSquared):
    def set_input_space(self, space):
        assert self.kernel_shape[1] == -1, ("Should be set to -1 to indicate "
            "it should be replaced")
        self.kernel_shape = (self.kernel_shape[0], space.shape[1])
        super(ConvSquaredFullHeight, self).set_input_space(space)
        
class ConvSquaredSwitchAxes(ConvSquared):
    def __init__(self,
                 output_channels,
                 kernel_shape,
                 layer_name,
                 switch_axes,
                 **kwargs):
        """ Remaining keyword arguments same as ConvSquared/ConvElemwise."""
        self.switch_axes = switch_axes
        #TODELAY: force that all needed arguments for super constructor are given,
        # e.g. pool shape/stride etc?
        super(ConvSquaredSwitchAxes, self).__init__(
            output_channels=output_channels,
            kernel_shape=kernel_shape,
            layer_name=layer_name, **kwargs)

    def set_input_space(self, space):
        assert tuple(space.axes) == tuple(self.switch_axes[0])
        space = convert_conv_space(space, self.switch_axes[0],
            self.switch_axes[1])
        super(ConvSquaredSwitchAxes, self).set_input_space(space)
    