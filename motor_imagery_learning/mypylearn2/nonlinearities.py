from pylearn2.models.mlp import Layer, BadInputSpaceError
from pylearn2.space import Conv2DSpace
from functools import wraps
import theano.tensor as T
import logging
from collections import OrderedDict
# hack: make logging appear for output space
logger = logging.getLogger('pylearn2.mlp')

class NonlinearityLayer(Layer):

    def __init__(self,
                 layer_name,
                 nonlinearity_func):

        self.layer_name = layer_name
        self.nonlinearity_func = nonlinearity_func
        super(NonlinearityLayer, self).__init__()


    @wraps(Layer.set_input_space)
    def set_input_space(self, space):
        self.input_space = space

        assert self.input_space.axes == ('b', 'c', 0, 1)
        if not isinstance(space, Conv2DSpace):
            raise BadInputSpaceError(self.__class__.__name__ +
                                     ".set_input_space "
                                     "expected a Conv2DSpace, got " +
                                     str(space) + " of type " +
                                     str(type(space)))

        self.output_space = self.input_space

    @wraps(Layer.get_params)
    def get_params(self):
        return []


    @wraps(Layer.get_layer_monitoring_channels)
    def get_layer_monitoring_channels(self, state_below=None,
                                      state=None, targets=None):

        rval = OrderedDict()
        return rval

    @wraps(Layer.fprop)
    def fprop(self, state_below):
        self.input_space.validate(state_below)
        nonlinear_transformed = self.nonlinearity_func(state_below)
        return nonlinear_transformed

def square_nonlin(state_below):
    return T.sqr(state_below)

def square_plus_abs_nonlin(state_below):
    return T.sqr(state_below) + T.abs_(state_below)

def rectifier_nonlin(state_below):
    return state_below * (state_below > 0.)

def abs_nonlin(state_below):
    return T.abs_(state_below)

def abs_log_nonlin(state_below):
    eps = 1e-6
    return T.log(T.maximum(eps,T.abs_(state_below)))

def identity_nonlin(state_below):
    return state_below
