"""Module containing the Train class and support functionality."""
__authors__ = "Ian Goodfellow"
__copyright__ = "Copyright 2010-2012, Universite de Montreal"
__credits__ = ["Ian Goodfellow"]
__license__ = "3-clause BSD"
__maintainer__ = "LISA Lab"
__email__ = "pylearn-dev@googlegroups"
from pylearn2.termination_criteria import ChannelTarget, And, EpochCounter
from motor_imagery_learning.mypylearn2.learning_rule_adam import Adam
from motor_imagery_learning.mypylearn2.learning_rule_adam import Momentum
from datetime import datetime
import os
import logging
import warnings
from pylearn2.utils.string_utils import preprocess
from pylearn2.monitor import Monitor
from pylearn2.space import NullSpace
from pylearn2.utils.timing import log_timing, total_seconds
from pylearn2.utils import sharedX
from motor_imagery_learning.mypylearn2 import train_util
from copy import deepcopy
import numpy as np
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
from motor_imagery_learning.mypylearn2.remember_best_extension import MonitorBasedRememberBest

log = logging.getLogger(__name__)

class TrainEarlyStop(object):
    def __init__(self, dataset_splitter, model, algorithm=None, save_path=None,
            preprocessor=None, keep_best_lrule_params=False,
            best_model_channel='valid_y_misclass', 
            fraction_epochs_after_stop=1.,
            weight_initializer=None):
        self.__dict__.update(locals())
        del self.self

    def main_loop(self):
        """
        Create trainer (assumed with early stop), 
        continue training this trainer
        on merged train/valid set after early stop
        """
        self.run_until_earlystop()
        self.run_until_second_stop()
        if (self.save_path is not None):
            self.save()
            
    def run_until_earlystop(self):
        self.dataset_splitter.ensure_dataset_is_loaded()
        self._remember_original_algorithm()
        # TODELAY: assert early stop criterion?
        self._create_early_stop_trainer()
        self.early_stop_trainer.main_loop()
    
    def run_until_second_stop(self):
        self._remember_best_params()
        self._remove_early_stop_trainer()
        self._create_and_setup_after_stop_trainer()
        self.after_stop_trainer.run_training()
        self.check_correctness()

    def _remember_original_algorithm(self):
        self.orig_algorithm = deepcopy(self.algorithm)

    def _create_early_stop_trainer(self):
        # deep copy datasets so you can re-preprocess them 
        # when putting valid into training
        this_datasets = self.dataset_splitter.split_into_train_valid_test()
        self.dataset_splitter.free_memory_if_reloadable()
        if self.preprocessor is not None:
            self.preprocessor.apply(this_datasets['train'], can_fit=True)
            self.preprocessor.apply(this_datasets['valid'], can_fit=False)
            self.preprocessor.apply(this_datasets['test'], can_fit=False)
        
        # set input space here as preprocessor is allowed to modify dataset
        # space
        train_util.ensure_model_has_input_space(self.model, 
            this_datasets['train'], weight_initializer=self.weight_initializer)
        
        self.algorithm._set_monitoring_dataset(this_datasets)
        self.remember_best_extension = MonitorBasedRememberBest(
            self.best_model_channel)
        self.early_stop_trainer = Train(this_datasets['train'], 
                        self.model, self.algorithm,
                        save_path=None, save_freq=0,
                        extensions=[self.remember_best_extension])
        
    def _remember_best_params(self):
        self.best_monitor_chans = dict()
        best_model = self.remember_best_extension.best_model
        self.best_epoch = self.remember_best_extension.best_epoch
        # Recover monitor values until best epoch
        best_model.monitor = self.early_stop_trainer.model.monitor
        for key in best_model.monitor.channels:
            val_record =  best_model.monitor.channels[key].val_record
            # limit to values until best epoch (inclusive!)
            val_record = val_record[:self.best_epoch + 1] 
            self.best_monitor_chans[key] = val_record
        self.best_model = best_model
        self.best_learning_rule = self.remember_best_extension.best_learning_rule
        self.best_batches_seen = best_model.monitor._num_batches_seen
        self.best_examples_seen = best_model.monitor._examples_seen
        
        
    def _remove_early_stop_trainer(self):
        """ Try to free all possible memory, especially 
        try to delete all references to the datasets."""
        del self.early_stop_trainer
        del self.best_model.monitor
        del self.remember_best_extension
        del self.algorithm
        del self.model
        
    def _create_and_setup_after_stop_trainer(self):
        # try to always have dataset in memory at most two times
        self.dataset_splitter.reload_data()
        new_datasets = self.get_train_fitted_valid_test()
        # dataset freeing
        # already happens within get train fitted valid test function:
        # self.dataset_splitter.free_memory_if_reloadable()
        self.model = self.best_model
        self.algorithm = self.orig_algorithm
        self.algorithm._set_monitoring_dataset(new_datasets)
        
        self.after_stop_trainer = Train(new_datasets['train'], 
                        self.model, self.algorithm,
                        save_path=None, save_freq=0)
        
        self.after_stop_trainer.setup()
        
        # Use old parameters for learning rule,
        # for now just supporting adam and momentum
        
        assert self.keep_best_lrule_params, ("or else check if "
            "you need to always keep learning rule time atleast?")
        if self.keep_best_lrule_params:
            if hasattr(self.algorithm.learning_rule, 'grad'):
                assert isinstance(self.algorithm.learning_rule,
                    Adam)
                for key in self.algorithm.learning_rule.grad:
                    old_val = self.best_learning_rule.grad[key].get_value()
                    new_var = self.algorithm.learning_rule.grad[key]
                    new_var.set_value(old_val)
                
                for key in self.algorithm.learning_rule.sq_grad:
                    old_val = self.best_learning_rule.sq_grad[key].get_value()
                    new_var = self.algorithm.learning_rule.sq_grad[key]
                    new_var.set_value(old_val)
                self.algorithm.learning_rule.time.set_value(
                    self.best_learning_rule.time.get_value())
            else:
                print("learning rule class"), self.algorithm.learning_rule.__class__
                assert isinstance(self.algorithm.learning_rule,
                    Momentum)
                old_val = self.best_learning_rule.momentum.get_value()
                new_var = self.algorithm.learning_rule.momentum
                new_var.set_value(old_val)
                for key in self.algorithm.learning_rule.vels:
                    old_val = self.best_learning_rule.vels[key].get_value()
                    new_var = self.algorithm.learning_rule.vels[key]
                    new_var.set_value(old_val)
                
        del self.best_learning_rule
        
        # Adapt monitor, copy over old values
        new_chans = self.model.monitor.channels
        old_chan_val_records = self.best_monitor_chans
        
        for key in new_chans:
            old_val_record = old_chan_val_records[key]
            new_chans[key].val_record = old_val_record
            
        self.model.monitor._epochs_seen = self.best_epoch
        self.model.monitor._num_batches_seen = self.best_batches_seen
        self.model.monitor._examples_seen = self.best_examples_seen
        
        # Add new termination criterion
        # Lets use negative log likelihood
        # Replace early stop termination by termination when valid set has reached training
        # set negative log likelyhood from training before
        # use minimum tolerance also, i.e. if valid negative log likelihood
        # is below 1e-6, also stop training if we have done twice as many
        # epochs as before... this is more of a safety check...        
        train_nll = self.best_monitor_chans['train_y_nll'][-1]
        train_nll = float(train_nll)
        epochs_to_train = int(self.fraction_epochs_after_stop * self.best_epoch)
        self.algorithm.termination_criterion = And(
            criteria=[EpochCounter(max_epochs=epochs_to_train, new_epochs=True),
                ChannelTarget('valid_y_nll', max(1e-6, train_nll))])
    

    def get_train_fitted_valid_test(self):
        this_datasets = self.dataset_splitter.split_into_train_valid_test()
        self.dataset_splitter.free_memory_if_reloadable()
        train_valid_set = self.concatenate_sets(this_datasets['train'],
            this_datasets['valid'])
        test_set = this_datasets['test']
        train_set_num_trials = len(this_datasets['train'].y)
        del this_datasets['train']
        if self.preprocessor is not None:
            self.preprocessor.apply(train_valid_set, can_fit=True)
            self.preprocessor.apply(test_set, can_fit=False)
        _, valid_set = self.split_sets(train_valid_set, 
            train_set_num_trials, len(this_datasets['valid'].y))
        # train valid is the new train set!!
        return {'train': train_valid_set, 'valid': valid_set, 
            'test': test_set}

    def concatenate_sets(self, first_set, second_set):
        """ Concatenates topo views and y(targets)"""
        assert first_set.view_converter.axes == second_set.view_converter.axes,\
            "first set and second set should have same axes ordering"
        assert first_set.view_converter.axes[0] == 'b', ("Expect batch axis "
            "as first axis")
        merged_topo_view = np.concatenate((first_set.get_topological_view(),
            second_set.get_topological_view()))
        merged_y = np.concatenate((first_set.y, second_set.y)) 
        merged_set = DenseDesignMatrix(
            topo_view=merged_topo_view,
            y=merged_y,
            axes=first_set.view_converter.axes)
        return merged_set
    
    def split_sets(self, full_set, split_index, split_to_end_num):
        """ Assumes that full set may be doubled or tripled in size
        and split index refers to original size. So
        if we originally had 100 trials (set1) + 20 trials (set2) 
        merged to 120 trials, we get a split index of 100.
        If we later have 360 trials we assume that the 360 trials 
        consist of:
        100 trials set1 + 20 trials set2 + 100 trials set1 + 20 trials set2
        + 100 trials set1 + 20 trials set2
        (and not 300 trials set1 + 60 trials set2)"""
        full_topo = full_set.get_topological_view()
        full_y = full_set.y
        original_full_len = split_index + split_to_end_num
        topo_first = full_topo[:split_index]
        y_first = full_y[:split_index]
        topo_second = full_topo[split_index:original_full_len]
        y_second = full_y[split_index:original_full_len]
        next_start = original_full_len
        # Go through possibly appended transformed copies of dataset
        # If preprocessors did not change dataset size, this is not 
        # necessary
        for next_split in xrange(next_start + split_index, 
                len(full_set.y), original_full_len):
            next_end = next_split + split_to_end_num
            topo_first = np.concatenate((topo_first, 
                full_topo[next_start:next_split]))
            y_first = np.concatenate((y_first, full_y[next_start:next_split]))
            topo_second = np.concatenate((topo_second, 
                full_topo[next_split:next_end]))
            y_second =  np.concatenate((y_second, full_y[next_split:next_end]))
            next_start = next_end
        first_set = DenseDesignMatrix(
            topo_view=topo_first,
            y=y_first,
            axes=full_set.view_converter.axes)
        second_set = DenseDesignMatrix(
            topo_view=topo_second,
            y=y_second,
            axes=full_set.view_converter.axes)
        return first_set, second_set

    def check_correctness(self):
        """ For now, just checking that termination epoch criterion is ok. """
        # Don't know why +1
        assert (self.algorithm.termination_criterion._criteria[0]._epochs_offset 
             == self.best_epoch + 1), "Epochs offset should be correct"

    def save(self):
        train_saver = train_util.TrainStateSaver() 
        train_saver.add_trainer(self.after_stop_trainer)
        train_saver.save(self.save_path)

class Train(object):
    """
    THIS CLASS ADAPTS MODEL AFTER TRAINING(!!) FOR MY EXPERIMENTS :))))
    and also initlaizes output space of model
    A class representing the main loop of the training script.  Trains the
    specified model using the specified algorithm on the specified dataset.
    After each call to the training algorithm, the model is saved to
    `save_path`. May be enhanced with `TrainExtension` plugins.

    Parameters
    ----------
    dataset : `pylearn2.datasets.dataset.Dataset`
    model : `pylearn2.models.model.Model`
    algorithm : \
        `pylearn2.training_algorithms.training_algorithm.TrainingAlgorithm`, \
        optional
    save_path : str, optional
        Path to save (with pickle / joblib) the model.
    save_freq : int, optional
        Frequency of saves, in epochs. A frequency of zero disables
        automatic saving altogether. A frequency of 1 saves every
        epoch. A frequency of 2 saves every other epoch, etc.
        (default=0, i.e. never save). Note: when automatic saving is
        enabled (eg save_freq > 0), the model is always saved after
        learning, even when the final epoch is not a multiple of
        `save_freq`.
    extensions : iterable, optional
        A collection of `TrainExtension` objects whose callbacks are
        triggered at various points in learning.
    allow_overwrite : bool, optional
        If `True`, will save the model to save_path even if there is
        already something there. Otherwise, will raise an error if the
        `save_path` is already occupied.
    """

    def __init__(self, dataset, model, algorithm=None, save_path=None,
                 save_freq=0, extensions=None, allow_overwrite=True,
                 weight_initializer=None):
        self.allow_overwrite = allow_overwrite
        self.first_save = True
        self.dataset = dataset
        self.model = model
        self.algorithm = algorithm
        if save_path is not None:
            if save_freq == 0:
                warnings.warn('save_path specified but save_freq is 0 '
                              '(never save). Is this intentional?')
            self.save_path = preprocess(save_path)
        else:
            self.save_path = None
            if save_freq > 0:
                phase_variable = 'PYLEARN2_TRAIN_PHASE'
                if phase_variable in os.environ:
                    phase = 'phase%d' % os.environ[phase_variable]
                    tokens = [os.environ['PYLEARN2_TRAIN_FILE_FULL_STEM'],
                              phase, 'pkl']
                else:
                    tokens = os.environ['PYLEARN2_TRAIN_FILE_FULL_STEM'], 'pkl'
                self.save_path = '.'.join(tokens)
        self.save_freq = save_freq
        self.weight_initializer=None

        if hasattr(self.dataset, 'yaml_src'):
            self.model.dataset_yaml_src = self.dataset.yaml_src
        else:
            pass
            # We don't need this warning I think :)
            #warnings.warn("dataset has no yaml src, model won't know what " +
            #              "data it was trained on")

        self.extensions = extensions if extensions is not None else []
        self.training_seconds = sharedX(value=0,
                                        name='training_seconds_this_epoch')
        self.total_seconds = sharedX(value=0, name='total_seconds_last_epoch')
            
    def main_loop(self, time_budget=None):
        """
        Repeatedly runs an epoch of the training algorithm, runs any
        epoch-level callbacks, and saves the model.

        Parameters
        ----------
        time_budget : int, optional
            The maximum number of seconds before interrupting
            training. Default is `None`, no time limit.
        """
        self.setup()
        self.run_training()
        if (self.save_path is not None):
            self.save()

    def setup_extensions(self):
        """ Calls setup on all extensions."""
        for ext in self.extensions:
            ext.setup(self.model, self.dataset, self.algorithm)

    def exceeded_time_budget(self, t0, time_budget):
        """
        .. todo::

            WRITEME
        """
        dt = total_seconds(datetime.now() - t0)
        if time_budget is not None and dt >= time_budget:
            log.warning("Time budget exceeded (%.3f/%d seconds).",
                        dt, time_budget)
            self.model.monitor.time_budget_exceeded = True
            return True
        else:
            return False

    def setup(self):
        """
        Sets up the main loop. This is also called at the start of the
        main loop, so you need only call it if you're using a driver
        script that replaces the main loop with something else.
        """
        train_util.ensure_model_has_input_space(self.model, self.dataset,
            weight_initializer=self.weight_initializer)
        self.model.monitor = Monitor.get_monitor(self.model)
        self.model.monitor.time_budget_exceeded = False
        if self.algorithm is not None:
            self.algorithm.setup(model=self.model, dataset=self.dataset)
        self.setup_extensions()

        # Model.modify_updates is used by the training algorithm to
        # enforce constraints after each step of learning. Here we
        # make sure the constraints are enforced from the start.
        self.model.enforce_constraints()
        self.train_saver = train_util.TrainStateSaver() 
        if not hasattr(self.model, 'monitor'):
            # TODO: is this really necessary? I just put this error here
            # to prevent an AttributeError later, but I think we could
            # rewrite to avoid the AttributeError
            raise RuntimeError("The algorithm is responsible for setting"
                               " up the Monitor, but failed to.")
        if len(self.model.monitor._datasets) > 0:
            # This monitoring channel keeps track of a shared variable,
            # which does not need inputs nor data.
            self.training_seconds.__doc__ = """\
The number of seconds that were spent in actual training during the most
recent epoch. This excludes seconds that were spent running callbacks for
the extensions, computing monitoring channels, etc."""
            self.model.monitor.add_channel(
                name="training_seconds_this_epoch",
                ipt=None,
                val=self.training_seconds,
                data_specs=(NullSpace(), ''),
                dataset=self.model.monitor._datasets[0])
            self.total_seconds.__doc__ = """\
The number of seconds that were spent on the entirety of processing for the
previous epoch. This includes not only training but also the computation of
the monitoring channels, running TrainExtension callbacks, etc. This value
is reported for the *previous* epoch because the amount of time spent on
monitoring for this epoch is not known until the monitoring channels have
already been reported."""
            self.model.monitor.add_channel(
                name="total_seconds_last_epoch",
                ipt=None,
                val=self.total_seconds,
                data_specs=(NullSpace(), ''),
                dataset=self.model.monitor._datasets[0])
        self.run_callbacks_and_monitoring()
        
    def run_training(self):
        continue_learning = True
        while continue_learning:
            with log_timing(log, None, level=logging.DEBUG,
                            callbacks=[self.total_seconds.set_value]):
                continue_learning = self.run_one_epoch()
                
        self.model.monitor.training_succeeded = True
        
    def run_one_epoch(self):
        with log_timing(
                        log, None, final_msg='Time this epoch:',
                        callbacks=[self.training_seconds.set_value]):
            rval = self.algorithm.train(dataset=self.dataset)
            if rval is not None:
                raise ValueError("TrainingAlgorithm.train should not "
                                 "return anything. Use "
                                 "TrainingAlgorithm.continue_learning "
                                 "to control whether learning "
                                 "continues.")
        self.model.monitor.report_epoch()
        extension_continue = self.run_callbacks_and_monitoring()
        continue_learning = (
            self.algorithm.continue_learning(self.model) and
            extension_continue
        )
        assert continue_learning in [True, False, 0, 1]
        return continue_learning
        
    def run_callbacks_and_monitoring(self):
        """
        Runs the monitor, then calls Extension.on_monitor for all extensions.

        Returns
        -------
        continue_learning : bool
            If `False`, signals that at least one train
            extension wants to stop learning.
        """
        self.model.monitor()
        continue_learning = True
        for extension in self.extensions:
            try:
                extension.on_monitor(self.model, self.dataset, self.algorithm)
            except TypeError:
                logging.warning('Failure during callback ' + str(extension))
                raise
            # We catch an exception here instead of relying on return
            # values for backward compatibility. Lots of extensions
            # exist that don't return anything, currently.
            except StopIteration:
                log.info("Extension requested training halt.")
                continue_learning = False
        return continue_learning

    def save(self):
        """Saves the model."""
        assert self.save_path is not None
        self.train_saver.add_trainer(self)
        self.train_saver.save(self.save_path)
        del self.train_saver
