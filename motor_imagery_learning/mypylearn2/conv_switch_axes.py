from pylearn2.models.mlp import ConvElemwise
from pylearn2.space import Conv2DSpace

class ConvSwitchAxes(ConvElemwise):
    def __init__(self,
                 output_channels,
                 kernel_shape,
                 layer_name,
                 nonlinearity,
                 switch_axes,
                 tied_b=True,
                 **kwargs):
        """ Remaining keyword arguments same as ConvElemwise."""
        self.switch_axes = switch_axes
        super(ConvSwitchAxes, self).__init__(output_channels,
            kernel_shape,layer_name,nonlinearity, tied_b=tied_b, **kwargs)
        
        
    def set_input_space(self, space):
        assert tuple(space.axes) == tuple(self.switch_axes[0])
        space = convert_conv_space(space, self.switch_axes[0],
            self.switch_axes[1])
        super(ConvSwitchAxes, self).set_input_space(space)
        

def convert_conv_space(space, axesbefore, axesafter):
    chan_dim = axesbefore[axesafter.index('c')]

    assert axesbefore[0] == 'b'
    assert axesafter[0] == 'b'
    new_shape = [None, None]
    new_chans = None

    if chan_dim == 'c':
        new_chans = space.num_channels
    elif chan_dim == 0:
        new_chans = space.shape[0]
    elif chan_dim == 1:
        new_chans = space.shape[1]

    zero_dim = axesbefore[axesafter.index(0)]
    if zero_dim == 'c':
        new_shape[0] = space.num_channels
    elif zero_dim == 0:
        new_shape[0] = space.shape[0]
    elif zero_dim == 1:
        new_shape[0] = space.shape[1]

    one_dim = axesbefore[axesafter.index(1)]
    if one_dim == 'c':
        new_shape[1] = space.num_channels
    elif one_dim == 0:
        new_shape[1] = space.shape[0]
    elif one_dim == 1:
        new_shape[1] = space.shape[1]
    
    new_space = Conv2DSpace(shape=new_shape, num_channels=new_chans,
        axes=axesafter)
    return new_space
