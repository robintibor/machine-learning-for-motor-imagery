""" For Data Augmentation """
from numpy.random import RandomState
import numpy as np
import theano

class SetToZeroTransformer():
    def transform(self, batch):
        transformed_batch = (batch[0] * 0, batch[1] * 1)
        return transformed_batch
    

class GaussianNoiseTransformer():
    def __init__(self, noise_variance):
        self.noise_variance = noise_variance
        self.rng = RandomState(np.uint64(hash('gaussnoise')))
        
    def transform(self, batch):
        input_shape = batch[0].shape
        noise = self.rng.randn(*input_shape) * self.noise_variance
        noise = noise.astype(theano.config.floatX)
        transformed_batch = (batch[0] + noise, batch[1])
        return transformed_batch