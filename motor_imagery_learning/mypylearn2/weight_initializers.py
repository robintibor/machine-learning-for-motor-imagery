from motor_imagery_learning.analysis.data_generation import (
    create_sine_signal)
import numpy as np
import logging

log = logging.getLogger(__name__)

class SineWeightInitializer():
    def __init__(self, layer_names, sampling_freq):
        self.layer_names = layer_names
        self.sampling_freq = sampling_freq
        
    def initialize(self, model):
        # first check all layer names exist
        model_layer_names = [l.layer_name for l in model.layers]
        for own_layer_name in self.layer_names:
            assert own_layer_name in model_layer_names, "{:s} should be in" \
                "model layer names {:s}".format(own_layer_name, 
                    model_layer_names)
        for layer in model.layers:
            if layer.layer_name in self.layer_names:
                self.initialize_layer(layer)
                log.info("Initialized {:s} weights".format(layer.layer_name))
                
    def initialize_layer(self, layer):
        num_weights, num_samples = np.squeeze(layer.get_weights_topo()).shape
        freqs = np.linspace(1,self.sampling_freq/2.0,num_weights)
        sine_weights = [create_sine_signal(samples=num_samples, freq=freq,
                    sampling_freq=150) for freq in freqs]
        sine_weights = np.array(sine_weights).astype(np.float32)
        sine_weights = sine_weights.reshape((sine_weights.shape[0],1,sine_weights.shape[1],1))
        # TODELAY: separate parameter for initializer
        # either istdev or irange should be given in layer.. use it to scale
        # sine weights
        scale = max(layer.__dict__.get('istdev', -1), 
                layer.__dict__.get('irange', -1))
        assert scale != -1
        sine_weights = sine_weights * scale
        layer.set_weights(sine_weights)