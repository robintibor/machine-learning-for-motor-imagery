"""
Cross validation module that runs folds sequentially in order to save memory.
"""
from copy import deepcopy
from motor_imagery_learning.mypylearn2 import train_util
from motor_imagery_learning.mypylearn2.dataset_splitters import (
    DatasetSingleFoldSplitter)
from motor_imagery_learning.mypylearn2.train import TrainEarlyStop


class TrainCVEarlyStop(object):
    """
    Wrapper for Train that partitions the dataset according to a given
    cross-validation iterator, returning a Train object for each split.
    TODO: maybe instead of giving so many parameters, 
    create a train early stop fold class which is supplied to this one.
    Then it just needs to get its algorithm/model reset...
    But maybe also less safe since it is not guaranteed that it is isolated
     from other folds if you change code at some point...
    """
    def __init__(self, dataset, num_folds, model, 
        keep_best_lrule_params=False, fraction_epochs_after_stop = 1.0,
        algorithm=None, preprocessor=None, save_path=None, save_freq=0,
        extensions=None, allow_overwrite=True, save_folds=False, 
        cv_extensions=None, weight_initializer=None):
        self.dataset = dataset
        self.num_folds = num_folds
        self.model = model
        self.algorithm = algorithm
        self.save_path = save_path
        self.save_freq = save_freq
        self.extensions = extensions
        self.allow_overwrite = allow_overwrite
        self.save_folds = save_folds
        self.preprocessor = preprocessor
        self.keep_best_lrule_params = keep_best_lrule_params
        self.fraction_epochs_after_stop = fraction_epochs_after_stop
        if cv_extensions is None:
            self.cv_extensions = []
        else:
            self.cv_extensions = cv_extensions
        self.weight_initializer = weight_initializer

    def setup(self):
        """Set up the main loop."""
        self.setup_extensions()

    def setup_extensions(self):
        """Set up extensions."""
        for extension in self.cv_extensions:
            extension.setup(self.trainers)

    def main_loop(self, time_budget=None, parallel=False, client_kwargs=None,
                  view_flags=None):
        """
        Adapted from TrainCV main loop
        Run main_loop of each trainer until validation criterion failed.
        Then include validation set into training and run until validation
        negative log likelihood is same as train negative log likelihood
        when you stopped
        

        Parameters
        ----------
        time_budget : int, optional
            The maximum number of seconds before interrupting
            training. Default is `None`, no time limit.
        parallel : bool, optional
            Whether to train subtrainers in parallel using
            IPython.parallel (default False).
        client_kwargs : dict, optional
            Keyword arguments for IPython.parallel Client.
        view_flags : dict, optional
            Flags for IPython.parallel LoadBalancedView.
        """
        self.setup()
        self.train_saver = train_util.TrainStateSaver() 
        # Run first until supplied criterion fails (typically
        # negative log likelihood not improving for some time)
        # Then remember training error and run
        # until the negative log likelihood on validation set
        # is below remembered training negative log likelihood
        # see https://code.google.com/p/cuda-convnet/wiki/Methodology
        # for inspiration (we do not run any more epochs with smaller step size
        # after we reached same negative log likelihood on validation set)
        for fold_nr in range(0, self.num_folds):
            trainer = self._construct_train_object(fold_nr)
            trainer.main_loop()
            self.train_saver.add_trainer(trainer)
            del trainer # for memory usage..not checked if it helps
        if (self.save_path is not None):
            self.train_saver.save(self.save_path)

    def _construct_train_object(self, fold_nr):
        dataset_splitter = DatasetSingleFoldSplitter(self.dataset, 
            num_folds=self.num_folds, test_fold_nr=fold_nr)
        this_model = deepcopy(self.model)
        this_algorithm = deepcopy(self.algorithm)
        # Assumption: We never save individual folds
        trainer = TrainEarlyStop(dataset_splitter=dataset_splitter, 
            model=this_model, algorithm=this_algorithm, 
            preprocessor=self.preprocessor,
            keep_best_lrule_params=self.keep_best_lrule_params,
            fraction_epochs_after_stop=self.fraction_epochs_after_stop,
            save_path=None,
            weight_initializer=self.weight_initializer)
        return trainer
