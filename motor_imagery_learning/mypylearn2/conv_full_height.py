from pylearn2.models.mlp import ConvElemwise
class ConvElemwiseFullHeight(ConvElemwise):
    def set_input_space(self, space):
        assert self.kernel_shape[1] == -1, ("Should be set to -1 to indicate "
            "it should be replaced")
        self.kernel_shape = (self.kernel_shape[0], space.shape[1])
        super(ConvElemwiseFullHeight, self).set_input_space(space)
        