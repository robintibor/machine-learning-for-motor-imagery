from motor_imagery_learning.motor_eeg_dataset import (
    determine_available_sensors)
from motor_imagery_learning import motor_eeg_dataset
from pylearn2.train import Train
from motor_imagery_learning.mypylearn2.cross_validation_topo_iterator import DatasetValidationKFold
from motor_imagery_learning.mypylearn2.train_cv_sequential_folds import TrainCVEarlyStop
from copy import deepcopy
from pylearn2.utils import sharedX, serial
from motor_imagery_learning.mypylearn2 import train_util
import os
from timeit import default_timer as timer
from motor_imagery_learning.analysis.results import save_result

def get_train_transfer_eeg(dataset_args, leave_out, **train_args):
    """ Split files into train/valid/test and create traintransfer object.
    For hacky purposes actually wrap the datasets into  a function that 
    needs to be called. Prevents unnecessary loading if TrainTransfer is not 
    used"""
    def extract_filenames(dataset_args, leave_out):
        assert (isinstance(dataset_args['filenames'], list) and 
            len(dataset_args['filenames'])) > 2, ("Expect atleast"
                "three filenames")
        filenames = dataset_args.pop('filenames')
        # assume leave out is a string which matches part of leave out filename
        is_test_file = [leave_out in f for f in filenames]
        assert is_test_file.count(True) == 1, ("should leave out one file, "
            "Remove this assertion and adapt code if not true anymore")
        test_index = is_test_file.index(True)
        train_filenames = filenames[:test_index] + filenames[test_index + 1:]
        test_filename = filenames[test_index]
        assert not test_filename in train_filenames
        assert set(train_filenames + [test_filename]) == set(filenames)
        return filenames, train_filenames, test_filename
        
    def compute_set_args(dataset_args):
        """ Compute start and stop for train and valid set"""
        given_start = dataset_args['start']
        given_stop = dataset_args['stop']
        assert (given_start is None) == (given_stop is None), ("either give "
            "both transfer_start and transfer_stop or None of them")
        train_stop = 0.9 if given_stop is None else int(0.9 * given_stop) - 1
        valid_start = 0.9 if given_stop is None else int(0.9 * given_stop)
        train_set_args = dict(dataset_args, **{'stop': train_stop})
        valid_set_args = dict(dataset_args, **{'start': valid_start})
        test_set_args = deepcopy(dataset_args)
        return train_set_args, valid_set_args, test_set_args
    
    def set_sensor_names(filenames, dataset_args):
        # make sure sensor names exist for all sets
        sensor_names = dataset_args.pop('sensor_names')
        sensor_names = determine_available_sensors(filenames, sensor_names)
        dataset_args['sensor_names'] = sensor_names
    
    def return_dataset():
        filenames, train_filenames, test_filename = extract_filenames(
            dataset_args, leave_out)
        set_sensor_names(filenames, dataset_args)
        train_set_args, valid_set_args, test_set_args = compute_set_args(dataset_args)
        train_set = motor_eeg_dataset.get_dataset(filenames=train_filenames, 
            **train_set_args)
        # valid is same files as train, but different part of each file....
        valid_set = motor_eeg_dataset.get_dataset(filenames=train_filenames, 
            **valid_set_args)
        test_set = motor_eeg_dataset.get_dataset(filenames=test_filename, 
            **test_set_args)
        datasets = {'train': train_set, 'valid':valid_set, 'test': test_set}
        return datasets
    return TrainTransfer(datasets=return_dataset, **train_args)



class TrainTransfer(object):
    def __init__(self, datasets, model, algorithm, preprocessor, save_path,
        transfer_learning_rate, inner_folds=10):
        self.__dict__.update(locals())
        del self.self
    
    def main_loop(self):
        self.load_datasets()
        self.set_model_input_space()
        self.run_training_leave_out_one_dataset()
        self.save_model()
        self.run_training_on_left_out_dataset()
        
    def set_model_input_space(self):
        train_util.set_model_input_space(self.model, self.datasets['train'])
        
    def load_datasets(self):
        """ Load datasets in case they were given as a callback """
        if hasattr(self.datasets, '__call__'):
            self.datasets = self.datasets()
        self.train_set = self.datasets['train']
        self.valid_set = self.datasets['valid']
        self.test_set = self.datasets['test']

    def run_training_leave_out_one_dataset(self):
        # TODO: assert erly stop is termination criterion?
        start = timer()
        self.preprocessor.apply(self.train_set, can_fit=True)
        self.preprocessor.apply(self.valid_set, can_fit=False)
        this_algorithm = deepcopy(self.algorithm)
        this_algorithm._set_monitoring_dataset({'train': self.train_set,
            'valid': self.valid_set})
        trainer = Train(self.train_set, self.model, this_algorithm, 
            save_path=None, save_freq=0)
        trainer.main_loop()
        end = timer()
        self.pretrain_time = end - start
    
    def save_model(self):
        """ Save model and result after pretraining on all but one dataset."""
        if self.save_path is not None:
            model_to_save = deepcopy(self.model)
            train_util.adapt_model_after_training(model_to_save, self.datasets)
            base_path, filename = os.path.split(self.save_path)
            save_path = os.path.join(base_path, 'pretrained', filename)
            serial.save(save_path, model_to_save)
            result_save_path = save_path.replace('.pkl', '.result.pkl')
            save_result(model_to_save, result_save_path, 
                training_time = self.pretrain_time, 
                parameters={}, templates={})

    def run_training_on_left_out_dataset(self):
        #TODO: preprocessing should be done or no?
        self.preprocessor.apply(self.test_set, can_fit=False)
        dataset_iterator = DatasetValidationKFold(dataset=self.test_set,
            n_folds=self.inner_folds)
        dataset_iterator.dataset.yaml_src = None
        del self.model.monitor
        this_algorithm = deepcopy(self.algorithm)
        this_algorithm.learning_rate = sharedX(0.01, 'learning_rate')
        # should save by itself
        trainer = TrainCVEarlyStop(dataset_iterator, self.model, this_algorithm,
            preprocessor =self.preprocessor,
            save_path=self.save_path, save_freq=float('inf')) # only save at end
        trainer.main_loop()
