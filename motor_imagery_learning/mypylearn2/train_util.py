from motor_imagery_learning.analysis.util import get_dataset_predictions
import re
from pylearn2.models.mlp import Softmax, Linear
from pylearn2.utils import serial

class FakeMonitor:
    """ Needed for saving to reduce models...
    Wanted a class that also has channel attribute like monitor
    So printing works in case real monitor is there and also in case this
    fake monitor is there :)"""
    def __init__(self, channels):
        self.channels = channels

def ensure_model_has_input_space(model, dataset, weight_initializer):
    # set input space, but only in case they are not set yet =>
    # allows pretrained models to keep weights
    if not hasattr(model, 'input_space'):
        # need to do this to prevent assertion from being raised
        model._nested = False
        model.setup_rng() # to keep reproducible in case we need to change
        # first input space guess dimensions
        first_layer = model.layers[0]
        # do not check for convelemwise to keep gpucorr/manuels layer woring
        first_layer_topo = (not isinstance(first_layer, Softmax) and
            not isinstance(first_layer, Linear))
        if first_layer_topo:
            model.set_input_space(dataset.view_converter.topo_space)
        else:
            model.set_input_space(dataset.X_space)
        
        # After random initialization, initialize model
        if (weight_initializer is not None):
            weight_initializer.initialize(model)

class TrainStateSaver:
    def __init__(self):
        self._train_states = []
    
    def add_trainer(self, trainer):
        adapt_model_after_training(trainer.model, 
            trainer.algorithm.monitoring_dataset)
        train_state = TrainState(trainer.model)
        self._train_states.append(train_state)
        
    def save(self, save_path):
        """ Save either a list of several train states (probably because of 
        cross-validation) or a single train state"""
        if (len(self._train_states) == 1):
            serial.save(save_path, self._train_states[0], on_overwrite='backup')
        else:
            assert len(self._train_states) > 0
            serial.save(save_path, self._train_states, on_overwrite='backup')
            

class TrainState:
    """ Should encapsulate training state with relevant information """
    def __init__(self, model):
        self.__dict__.update(locals())
        del self.self

def adapt_model_after_training(model, datasets):
    # Add predictions and targets so you can make a confusion matrix later
    model.info_predictions = dict()
    model.info_targets = dict()
    for dataset_key in datasets:
        # get batched insead
        predictions = get_dataset_predictions(model, datasets[dataset_key])
        targets = datasets[dataset_key].get_targets()
        model.info_predictions[dataset_key] = predictions
        model.info_targets[dataset_key] = targets
    remove_unnecessary_monitor_data(model)

def remove_unnecessary_monitor_data(model):
        ''' Only keep the misclass channels and thats it :)'''
        # Remove uneeded channels, only keep misclass channels! :)
        chans_to_retain = '(.*misclass)|(.*nll)' #regexp pattern
        channels =  model.monitor.channels
        chan_names = channels.keys()
        for name in chan_names:
            if re.match(chans_to_retain, name) is None:
                channels.pop(name)    
        newmonitor = FakeMonitor(channels)
        model.monitor = newmonitor
