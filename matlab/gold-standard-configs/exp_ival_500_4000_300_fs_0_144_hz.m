preproc_opts.fs_after_cleaning = 300; % 100-500
segment_opts.decoding_ival = [500 4000];% ms
filt_opts.min_freq = 0;
filt_opts.max_freq = 144;
filt_opts.last_low_freq = 30;
filt_opts.low_width = 2;
filt_opts.high_width = 6;
result_folder = 'results_fixed_ival_500_4000_300_fs_0_144_hz';