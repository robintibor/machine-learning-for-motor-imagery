function dir_path = get_script_dir( script_path )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
if (strcmp(script_path, '') == 1) % can it even happen?
    dir_path = '.';
else
    sep_inds = strfind(script_path, filesep);
    dir_path = script_path(1:sep_inds(end));
end

end

