function results = filterbank_binary_csp(MRK, folds, pairs, binary_results)    
%% Set some useful variables
nfilterbands = size(binary_results, 1); % first dim should be filterbands
nfolds = numel(folds);
npairs = size(pairs,1);


%% Initialize result variables
cells = cell(nfolds, npairs);
results = struct('LDA_classifier', cells, ...
    'train_accuracy', cells, 'test_accuracy', cells, ...
    'test_labels', cells, 'test_predictions', cells);
%% Feature Computation
% Intialize feature vectors

features = struct();
features(nfolds, npairs).train = [];
features(nfolds, npairs).test = [];
labels = struct();
labels(nfolds, npairs).train = [];
labels(nfolds, npairs).test = [];

% Collect features
for filt_i = nfilterbands:-1:1
    for fold_i = nfolds:-1:1
        %% Select training and testing markers
        training_inds = folds(fold_i).train;
        test_inds = folds(fold_i).test;
        MRK_train = mrk_selectEvents(MRK, training_inds);
        MRK_test = mrk_selectEvents(MRK, test_inds);
        for pair_i = size(pairs,1):-1:1
            %% Get results from binary classifier
            train_inds_pair = binary_results(filt_i, fold_i, pair_i).train_inds_pair;
            test_inds_pair = binary_results(filt_i, fold_i, pair_i).test_inds_pair;

            train_features = binary_results(filt_i, fold_i, pair_i).train_features_full_fold;
            test_features = binary_results(filt_i, fold_i, pair_i).test_features_full_fold;
            train_features = train_features(:, train_inds_pair);
            test_features = test_features(:, test_inds_pair);
            
            %% Select markers for this classifier
            MRK_train_pair = mrk_selectEvents(MRK_train, train_inds_pair);
            MRK_test_pair = mrk_selectEvents(MRK_test, test_inds_pair);
            % Labels only needed once per fold+pair, so check if exist already
            if (isempty(labels(fold_i, pair_i).train))
                labels(fold_i, pair_i).train = MRK_train_pair.y;
                labels(fold_i, pair_i).test = MRK_test_pair.y;
            else                
                % Should be equal if not empty before...
                assert(isequal(MRK_train_pair.y, labels(fold_i, pair_i).train) && ...
                    isequal(MRK_test_pair.y, labels(fold_i, pair_i).test));
            end

            features(fold_i, pair_i).train = ...
                [features(fold_i, pair_i).train; train_features];
            features(fold_i, pair_i).test = ...
                [features(fold_i, pair_i).test; test_features];
        end
    end    
end

for fold_i = nfolds:-1:1
    fprintf('Fold %d\n', fold_i);
    for pair_i = size(pairs,1):-1:1
        train_features = features(fold_i, pair_i).train;
        train_labels = labels(fold_i, pair_i).train;
        test_features = features(fold_i, pair_i).test;
        test_labels = labels(fold_i, pair_i).test;
        
        LDA_classifier = train_RLDAshrink(train_features, ...
            train_labels, 'Gamma',0, 'Scaling', 1);
        
        classifications = ...
            apply_separatingHyperplane(LDA_classifier, ...
            train_features);
        loss_train = mean(loss_0_1(train_labels, classifications));
        classifications = ...
            apply_separatingHyperplane(LDA_classifier, ...
            test_features);
        loss_test = mean(loss_0_1(test_labels, classifications));
        %loss_train
        %loss_test
        
        results(fold_i, pair_i).LDA_classifier = LDA_classifier;
        results(fold_i, pair_i).train_accuracy = 1 - loss_train;
        results(fold_i, pair_i).test_accuracy = 1 - loss_test;        
        results(fold_i, pair_i).test_labels = test_labels;
        results(fold_i, pair_i).test_predictions = classifications;
    end
end
end