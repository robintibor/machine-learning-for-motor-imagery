function [EEG_DAT, MRK]  = preproc_after_cleaning(EEG_DAT, MRK, ...
    preproc_opts)

%% Resample and possibly Common Average Referencing
[EEG_DAT, MRK] = resample_data(EEG_DAT, MRK, preproc_opts.fs_after_cleaning);

if preproc_opts.car_after_cleaning
    EEG_DAT = proc_commonAverageReference(EEG_DAT,EEG_DAT.clab);
end


end
