function [epo_clean, rejected_trials, rejected_channels] = clean_data(...
    epo, epo_EOG, clean_opts)
%CLEAN_DATA Clean Data from blink trials and from channels and trials
% with high variance
epo_clean = epo;
rejected_trials = [];
rejected_channels = [];
trial_to_original_trial = linspace(1, size(epo_clean.y, 2), size(epo_clean.y, 2));
%% Remove EOG contaminated training trials based on min-max
if clean_opts.blinkrej
    %TODO (unclear what why):
    [~, rejected_trials] = proc_rejectArtifactsMaxMin(epo_EOG, clean_opts.max_min);
    fprintf('rejected %d trials due to maxmin\n', numel(rejected_trials));
    % [mrk, why] = find_artifacts(dat, channels, gradient, amplitude, maxmin, low)
    epo_clean = proc_selectEpochs(epo_clean,'not',rejected_trials);
    % reconstruct which trials were rejected from original trial indices
    trial_to_original_trial(rejected_trials) = [];
end

%% Remove outlier trials (EMG) + channels based on variance.
if clean_opts.varrej
    % need different variable name for RTrials, as epoch indices
    % might have already changed in step before, so cannot
    % just compute union of rejected_trials and reject them
    [rejected_channels, RTRIALS]= epo_reject_varEventsAndChannels(...
        epo_clean, 'RemoveChannelsFirst', true, 'doBandpass', false, ...
        'Whiskerperc', clean_opts.whisker_perc, ...
        'Whiskerlength', clean_opts.whisker_length);
    fprintf('rejected %d trials due to variance\n', numel(RTRIALS));
    fprintf('rejected %d channels due to variance\n', numel(rejected_channels));
    epo_clean = proc_selectEpochs(epo_clean,'not',RTRIALS);
    epo_clean = proc_selectChannels(epo_clean,'not',rejected_channels);
    % reconstruct which trials were rejected from original trial indices
    for i = 1:numel(RTRIALS)
        trial_ind = RTRIALS(i);
        trial_ind_in_original_epochs = trial_to_original_trial(trial_ind);
        rejected_trials(end + 1) = trial_ind_in_original_epochs;
    end
    rejected_trials = sort(rejected_trials);
    % sanitycheck: there should be no duplicates in rejected trials now
    assert(isequal(rejected_trials, unique(rejected_trials)));
end       

end

