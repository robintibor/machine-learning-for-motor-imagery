function files = get_result_files_in_folder(folder, filepattern)
%GET_RESULT_FILES_IN_FOLDER  Get result files in given folder,
% optionally specify filepattern
if (~exist('filepattern', 'var'))
    filepattern = '';
end
files = dir(fullfile(folder, strcat(filepattern, '*.mat')));


end

