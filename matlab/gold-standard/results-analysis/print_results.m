function print_results( results )
%PRINT_RESULTS Summary of this function goes here
%   Detailed explanation goes here
% reconstruct number of trials
num_trials = size(results.binary(1,1,1).train_features_full_fold,2) + ...
    size(results.binary(1,1,1).test_features_full_fold,2);
fprintf('num_trials %d\n', num_trials)
mean_binary = max(mean(...
    reshape([results.binary.test_accuracy], ...
    size(results.binary, 1), []), 2));
fprintf('best mean one filter binary accuracy: %.2f%%\n', mean_binary * 100);

% didnt find more elegant way :(
best_single_filter_mean = max(mean(...
    reshape([results.multiclass.test_accuracy], ...
    size(results.multiclass, 1), []), 2));
fprintf('best mean one filter multiclass accuracy: %.2f%%\n', best_single_filter_mean * 100);

mean_fb_binary = mean([results.filterbank.test_accuracy]);
fprintf('mean filterbank binary accuracy: %.2f%%\n',mean_fb_binary * 100);
mean_fb_multiclass_train = mean([results.filterbank_multiclass.train_accuracy]);
mean_fb_multiclass_test = mean([results.filterbank_multiclass.test_accuracy]);
fprintf('mean filterbank multiclass train accuracy: %.2f%%\n',mean_fb_multiclass_train * 100);
fprintf('mean filterbank multiclass test accuracy: %.2f%%\n',mean_fb_multiclass_test * 100);


end

