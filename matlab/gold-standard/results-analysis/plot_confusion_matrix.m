function plot_confusion_matrix(result_folder)
    %% Plot confusion matrix for all results in given folder.
    % Example:
    % plot_confusion_matrix('data/gold-standard-results/results_fixed_ival_300_4000_150_fs_2_50_hz')

    files = get_result_files_in_folder(result_folder);
    %% Collect all target and predicted labels
    target_labels = [];
    predicted_labels = [];
    num_classes = 4;
    for file_i = 1:numel(files)
        file_name = files(file_i).name;
        results = load(fullfile(result_folder, file_name));
        target_labels = [target_labels results.filterbank_multiclass.test_labels];
        predicted_labels = [predicted_labels ...
            convert_to_one_hot(...
            [results.filterbank_multiclass.test_predictions], num_classes)];
    end
    %% Plot
    plotconfusion(target_labels, predicted_labels);
    title('Common Spatial Patterns');
    ax = gca;
    ax.XTickLabel = {'Right', 'Left', 'Rest', 'Feet', ''};
    ax.YTickLabel = {'Right', 'Left', 'Rest', 'Feet', ''};
end