%% Plot results of loss function for all pairs
%% Plot train results
f = figure;
bh = bar(100-squeeze(mean(reshape([memo.lossTr],size(memo)),2))'*100);
hold on
plot(0:size(pairs,1)+1,repmat(50,1,size(pairs,1)+2),'--r','LineWidth',2)
plot(0:size(pairs,1)+1,repmat(70,1,size(pairs,1)+2),'--g','LineWidth',2)
s = mat2cell(filterbank',ones(1,size(filterbank,2)),size(filterbank,1));
s = cellfun(@(x)( [num2str(x(1)) '-' num2str(x(2)) 'Hz'] ),s,'uni',0);
s{end+1} = 'Chance';
s{end+1} = 'Usable';
set(get(bh(1),'Parent'),'XTickLabel',cellfun(@(x)( [num2str(x{1}) 'vs' num2str(x{2})] ),{memo(1,1,:).pair},'uni',0))
title(get(bh(1),'Parent'),[name_resultfiles ' Filterbank binary decoding train accuracies: 1=Right hand, 2=Left hand, 3=Rest, 4=Feet'])
legend(get(bh(1),'Parent'),s,'Orientation','vertical','Location','best')
xlabel(get(bh(1),'Parent'),'Binary pairs')
ylabel(get(bh(1),'Parent'),'Decoding accuracy in % (100-loss*100)')
colormap(f,parula(size(filterbank,2)))
% cb = colorbar(get(bh(1),'Parent'));
% set(cb,'YTick',1:size(filterbank,2))
% set(cb,'YTickLabel',[s(:,1:2) repmat('-',12,1) s(:,end-1:end)])
% set(cb,'YTickLabel',s(1:end-2,:))
% set(cb,'YTick',get(cb,'YTick')+.5)
saveas(f,[name_resultfiles 'Train'], 'fig')
saveas(f,[name_resultfiles 'Train'], 'jpg')

%% Plot test results
f = figure;
bh = bar(100-squeeze(mean(reshape([memo.lossTe],size(memo)),2))'*100);
hold on
plot(0:size(pairs,1)+1,repmat(50,1,size(pairs,1)+2),'--r','LineWidth',2)
plot(0:size(pairs,1)+1,repmat(70,1,size(pairs,1)+2),'--g','LineWidth',2)
s = mat2cell(filterbank',ones(1,size(filterbank,2)),size(filterbank,1));
s = cellfun(@(x)( [num2str(x(1)) '-' num2str(x(2)) 'Hz'] ),s,'uni',0);
s{end+1} = 'Chance';
s{end+1} = 'Usable';
set(get(bh(1),'Parent'),'XTickLabel',cellfun(@(x)( [num2str(x{1}) 'vs' num2str(x{2})] ),{memo(1,1,:).pair},'uni',0))
title(get(bh(1),'Parent'),[name_resultfiles ' Filterbank binary decoding test accuracies: 1=Right hand, 2=Left hand, 3=Rest, 4=Feet'])
legend(get(bh(1),'Parent'),s,'Orientation','vertical','Location','best')
xlabel(get(bh(1),'Parent'),'Binary pairs')
ylabel(get(bh(1),'Parent'),'Decoding accuracy in % (100-loss*100)')
colormap(f,parula(size(filterbank,2)))
% cb = colorbar(get(bh(1),'Parent'));
% set(cb,'YTick',1:size(filterbank,2))
% set(cb,'YTickLabel',[s(:,1:2) repmat('-',12,1) s(:,end-1:end)])
% set(cb,'YTickLabel',s(1:end-2,:))
% set(cb,'YTick',get(cb,'YTick')+.5)
saveas(f,[name_resultfiles 'Test'], 'fig')
saveas(f,[name_resultfiles 'Test'], 'jpg')

%% Save
save(name_resultfiles, 'memo', 'filterbank')