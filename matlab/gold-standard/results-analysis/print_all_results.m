function print_all_results( folder, filepattern)
%PRINT_ALL_RESULTS Print all Results from the given folder

if (~exist('filepattern', 'var'))
    filepattern = '';
end
files = get_result_files_in_folder(folder, filepattern);

all_printable_results = struct();
for file_i = 1:numel(files)
    file_name = files(file_i).name;
    results = load(fullfile(folder, file_name));
    printable_results = extract_printable_results(results);
    if (file_i == 1)
        all_printable_results = printable_results;
    else
        all_printable_results = [all_printable_results printable_results];
    end
    fprintf('Results for %s:\n', file_name);
    print_results(results);
end

%% Print Summary over all datasets
fprintf('\n\nOverall Summary\n');
fprintf('mean(std) filterbank multiclass train: %4.2f%% (+-%4.2f%%)\n', ...
    mean([all_printable_results.mean_fb_multiclass_train]) * 100, ...
    std([all_printable_results.mean_fb_multiclass_train]) * 100);
fprintf('median filterbank multiclass test: %4.2f%%\n', ...
    median([all_printable_results.mean_fb_multiclass_test])* 100);
fprintf('mean(std) filterbank multiclass test: %4.2f%% (+-%4.2f%%)\n', ...
    mean([all_printable_results.mean_fb_multiclass_test]) * 100, ...
    std([all_printable_results.mean_fb_multiclass_test]) * 100);
fprintf('min filterbank multiclass test: %4.2f%%\n', ...
    min([all_printable_results.mean_fb_multiclass_test]) * 100);
fprintf('max filterbank multiclass test: %4.2f%%\n', ...
    max([all_printable_results.mean_fb_multiclass_test]) * 100);
mean_running_time = mean([all_printable_results.running_time_sec]);
std_running_time = std([all_printable_results.running_time_sec]);
% have to transform to fraction of day to use datestr function
fprintf('Running time (std): %s (+-%s)\n', ...
    datestr(mean_running_time / (60 * 60 * 24), 'HH:MM:SS'),...
        datestr(std_running_time / (60 * 60 * 24), 'HH:MM:SS'));
fprintf('Num filters (std): %4.2f (+-%4.2f)\n', ...
    mean([all_printable_results.num_filters]), ...
    std([all_printable_results.num_filters]));
%% Plot results
% print file names (part before ival and without Sc1S...)
print_file_names = cellfun(@(x) strsplit(x,'ival'), {files.name}, ...
    'UniformOutput', false);
print_file_names = cellfun(@(x) regexprep(x{1}, 'Sc[0-9]{1,2}S0?[0-9]{2}R01_', ''), ...
    print_file_names, 'UniformOutput', false);

figure();
bar(1:numel(files),[all_printable_results.mean_fb_multiclass_test]);
xticklabel_rotate(0.5:numel(files), 90, print_file_names);
end

