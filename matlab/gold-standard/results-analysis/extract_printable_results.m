function  printable_results = extract_printable_results(results)
%printable_results Summary of this function goes here
%   Detailed explanation goes here

printable_results.num_trials = size(results.binary(1,1,1).train_features_full_fold,2) + ...
    size(results.binary(1,1,1).test_features_full_fold,2);
printable_results.mean_binary = max(mean(...
    reshape([results.binary.test_accuracy], ...
    size(results.binary, 1), []), 2));
printable_results.best_single_filter_mean = max(mean(...
    reshape([results.multiclass.test_accuracy], ...
    size(results.multiclass, 1), []), 2));
printable_results.running_time_sec = results.running_time_sec;
printable_results.mean_fb_binary = mean([results.filterbank.test_accuracy]);
printable_results.mean_fb_multiclass_train = mean([results.filterbank_multiclass.train_accuracy]);
printable_results.mean_fb_multiclass_test = mean([results.filterbank_multiclass.test_accuracy]);
% num of csp filters... per fold/classifier/filterbank
printable_results.num_filters = size([results.binary.CSP_weights], 2) / prod(size(results.binary));
end

