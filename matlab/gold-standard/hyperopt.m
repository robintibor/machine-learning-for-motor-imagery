function result = hyperopt(varargin)
%% Example Call
%TODO: update filt_opts to new paradigm
%{
params = strsplit(['--filename /media/schirrmr/CE68160D6815F4C5/MotorEEGdatasets4deeplearning/BBCI-without-last-runs-datasets/GuJoMoSc01S001R01_ds10_1-11.BBCI.mat' ...
' --params' ...
' -filt_opts__width 4 -filt_opts__step 4 -filt_opts__min_freq 4' ...
' -filt_opts__filterbank_length 8 -filt_opts__filt_order 3'...
' -segment_opts__decoding_ival_start 0 -segment_opts__decoding_ival_length 4000' ...
' -preproc_opts__detrend 0 -preproc_opts__car 0 -preproc_opts__fs_after_cleaning 100' ...
' -auc_opts__compute_auc 0 -auc_opts__movAvgMsec 400'...
' -auc_opts__mean_ival_length_ms 100 -auc_opts__auc_thr 0.3'...
' -auc_opts__num_best_chans 10']);

result = hyperopt(params{:})
%}

%% Setup paths
addpath('BBCI');
startup_bbci_toolbox;
%% Parse parameters and print them
params = parse_parameters(varargin);
load_ival = false; % Load all data, not just some part :)
% Optionally set some params to debug values (debug should always be set to
% false when running real hyperopt!)
debug = false;
if (debug == true)
    load_ival = [0 6 * 60 * 1000]; % for test, load first 6 minutes
    params.segment_opts.nfolds = 4;
end

% Print parameters
keys = fieldnames(params);
for key_i = 1:numel(keys)
	fprintf('%s\n', keys{key_i});
	params.(keys{key_i})
end

%% Load data and run classification
do_single_filter_multiclass = false; % not needed for filterbank multiclass
[EEG_DAT, EMG_DAT, EOG_DAT, MRK, MNT, pairs] = load_data(...
	params.dataset_filename, ...
    params.preproc_opts, load_ival);
results = filterbank_multiclass(EEG_DAT, EOG_DAT, MRK, ...
   params.filt_opts, params.clean_opts, ...
   params.preproc_opts.fs_after_cleaning, params.segment_opts, ...
   params.auc_opts, pairs, do_single_filter_multiclass);

mean_fb_multiclass_test = mean([results.filterbank_multiclass.test_accuracy]);
fprintf('mean filterbank multiclass test accuracy: %.2f%%\n',mean_fb_multiclass_test * 100);

% for hyperopt we return sth that should be minimized so we return 
% 1-accuracy= misclass
result = 1-mean_fb_multiclass_test;
end

function params = parse_parameters(command_line_strings)
	params = extract_after_params_string(command_line_strings);
    params.dataset_filename = extract_filename(command_line_strings);    
	params = split_params(params);
	params = adjust_params_from_hyperopt(params);
end

function params = extract_after_params_string(command_line_strings)
	%% Extract parameters.. should come after --params string
	params_start = find(strcmp(command_line_strings, '--params')) + 1;
    params = command_line_strings(params_start:end);
    %% Remove minuses from field names
    for i=1:2:numel(params)
        assert(params{i}(1) == '-', 'Expect a minus in front of the parameter key');
        params{i} = params{i}(2:end);
    end
    %% Convert to struct
	params = struct(params{:});
    %% Convert all parameters to numeric.. 
	param_keys = fieldnames(params);
	for i = 1:numel(param_keys)
        params.(param_keys{i}) = str2num(params.(param_keys{i}));
	end
end

function filename = extract_filename(command_line_strings)
    % Filename should come after --filename option...    
    filename_index = find(strcmp(command_line_strings, '--filename')) + 1;

    filename = command_line_strings{filename_index};
    assert(filename(1) ~= '-', 'not expecting a minus as the start of the filename');
    % if there is a '-' in front of the actual filename, change last two lines to this:
    %assert(command_line_strings{filename_index}(1) == '-', ...
    %    'expecting a minus before the filename');
    %filename = command_line_strings{filename_index}(2:end);
end

function params = split_params(params)
	param_keys = fieldnames(params);
	for i = 1:numel(param_keys)
		if ~isempty(strfind(param_keys{i}, '__'))
			param_group_and_key = strsplit(param_keys{i}, '__');
			assert(numel(param_group_and_key) == 2, ...
                sprintf(['Expect maximum one __ (double underscore)' ...
                ' per parameter key %s', param_keys{i}]));
			value = params.(param_keys{i});
			params.(param_group_and_key{1}).(param_group_and_key{2}) = value;
			params = rmfield(params, param_keys{i});
		end
	end
end

function params = adjust_params_from_hyperopt(params)
    %% Convert decoding ival start + length to start/stop interval
    params.segment_opts.decoding_ival = ...
		[params.segment_opts.decoding_ival_start ...
		 params.segment_opts.decoding_ival_start + ...
         params.segment_opts.decoding_ival_length];
	params.segment_opts = rmfield(params.segment_opts, 'decoding_ival_start');
	params.segment_opts = rmfield(params.segment_opts, 'decoding_ival_length');
    

	
	%% Generate Filterbank-bands
    
    %% First create max freq and min freq out of given min freq and length
    max_freq = params.filt_opts.min_freq + ...
        params.filt_opts.filterbank_length;
    % Check if values make sense and if filterband will be
    % created
    assert(params.filt_opts.filterbank_length >= ...
        params.filt_opts.width, ...
        sprintf(['Expect filterbank length %d' ...
        ' to be greater than filterband width %d'], ...
        params.filt_opts.filterbank_length, ...
        params.filt_opts.width));
    assert(max_freq < params.preproc_opts.fs_after_cleaning, ...
        sprintf(['Expect maximum frequency %d to be below '...
            'half the sampling frequency %d'], ...
            max_freq, params.preproc_opts.fs_after_cleaning));
    % Actually create filterbands
	params.filt_opts.filterbands = generate_filterbank(params.filt_opts);
    assert(size(params.filt_opts.filterbands,2) > 0, ['Expect filterbands' ...
        ' to have atleast one band'])

    % Remove unnecessary fields (since filterbands already created)
	params.filt_opts = rmfield(params.filt_opts, 'width');
	params.filt_opts = rmfield(params.filt_opts, 'step');
	params.filt_opts = rmfield(params.filt_opts, 'min_freq');
	params.filt_opts = rmfield(params.filt_opts, 'filterbank_length');
 
    %% Add clean_opts
    clean_opts.blinkrej = 1; % 0 or 1
    clean_opts.varrej = 1; % 0 or 1
    % split to start and stop, same ranges as segment decoding ival
    clean_opts.rejection_ival = [0 4000];
    clean_opts.max_min = 600; % 20-2000    �V 1000 only blinks, lower also wierd stuff (not only post blink), jump between 600-500
    clean_opts.rejection_freq_band = [2; 49]; % frequency band to filter signal, before rejecting trials(!)
    params.clean_opts = clean_opts;
 
	%% NFolds should always be 10....
	params.segment_opts.nfolds = 10;
end
