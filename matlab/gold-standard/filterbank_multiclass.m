function results = filterbank_multiclass(EEG_DAT, EOG_DAT, MRK,...
    filt_opts, clean_opts, preproc_opts, segment_opts, auc_opts, ...
    pairs, do_single_filter_multiclass)

[folds, rejected_channels] = ...
   generate_clean_train_test_folds(EEG_DAT, EOG_DAT, ...
       MRK, clean_opts, segment_opts.nfolds);
tic;
% resample _after_ cleaning so cleaning always leads to same results
%[EEG_DAT, MRK] = resample_data(EEG_DAT, MRK, fs_after_cleaning);

[EEG_DAT, MRK]  = preproc_after_cleaning(EEG_DAT, MRK, preproc_opts);
binary_results = one_filter_binary_csp(EEG_DAT, MRK, filt_opts, ...
    auc_opts, segment_opts.decoding_ival, rejected_channels, ...
       folds, pairs);
mean_binary = mean([binary_results.test_accuracy]);
fprintf('mean one filter binary accuracy: %.2f%%\n', mean_binary * 100);

% Optional, not needed for rest...
if (exist('do_single_filter_multiclass', 'var') && ...
	do_single_filter_multiclass == true)
	multiclass_results = one_filter_multiclass_csp(MRK, ...
	    folds, pairs, binary_results);
	% didnt find more elegant way :(
	best_single_filter_mean = max(mean(...
	    reshape([multiclass_results.test_accuracy], ...
	    size(multiclass_results, 1), []), 2));
	fprintf('best mean one filter multiclass accuracy: %.2f%%\n', best_single_filter_mean * 100);
end

filterbank_binary_results = ...
    filterbank_binary_csp(MRK, folds, pairs, binary_results);
mean_fb_binary = mean([filterbank_binary_results.test_accuracy]);
fprintf('mean filterbank binary accuracy: %.2f%%\n',mean_fb_binary * 100);

filterbank_multiclass_results = filterbank_multiclass_csp(...
    MRK, folds, pairs, binary_results, filterbank_binary_results);
mean_fb_multiclass = mean([filterbank_multiclass_results.test_accuracy]);
fprintf('mean filterbank multiclass accuracy: %.2f%%\n',mean_fb_multiclass * 100);


% if one filter multiclass not done, just put NaN
if ~exist('multiclass_results', 'var')
    multiclass_results = NaN;
end

elapsed_time = toc;
results = struct();
results.binary = binary_results;
results.multiclass = multiclass_results;
results.filterbank = filterbank_binary_results;
results.filterbank_multiclass = filterbank_multiclass_results;
num_trials = numel(folds(1).train) + numel(folds(1).test);
results.num_trials = num_trials;
results.folds = folds;
results.running_time_sec = elapsed_time;
end