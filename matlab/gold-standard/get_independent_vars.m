% Filter opts used for generating filter bank

filt_opts.min_freq = 0; %Hz
filt_opts.max_freq = 144; % Hz
filt_opts.last_low_freq = 30; % Hz
filt_opts.low_width = 2; % Hz
filt_opts.high_width = 6; % Hz

filt_opts.filt_order = 3; % 1-?(40?) h 

% nono, no hyperopt :)
filt_opts.filterbands = generate_filterbank(filt_opts);% [5:1:197;15:1:207];% [5,15,33,45,65,75;15,33,45,65,75,95];% [5:1:197;15:1:207];% [45,65,75;65,75,95];% [5,15,33;15,33,45];% [5:3:38;9:3:42];% [5:1:40;7:1:42]; % Hz
% also not
segment_opts.nfolds = 10; % for test:2

% splitted to start length for hyperopt ..
% segment_opts__decoding_ival_start [0 3000] (default 0)
% segment_opts__decoding_ival_length [50 4000] (default 4000)
segment_opts.decoding_ival = [0 4000];% ms

preproc_opts.detrend = 0; % 0 or 1 ... probably keep it fixed on 0 for now? h {0} [0]
preproc_opts.car_after_cleaning = 1; % 0 or 1
preproc_opts.low_cut_off_Hz = 0.5;
preproc_opts.fs_after_cleaning = 150; % 100-500


%% Maybe ignore clean opts for now? Would probably destroy comparability
% nonono hyperopt
% as long as we clean on train and test, especially
% if we clean simultaneously
clean_opts.blinkrej = 1; % 0 or 1
clean_opts.varrej = 1; % 0 or 1
% split to start and stop, same ranges as segment decoding ival
clean_opts.rejection_ival = [0 4000];%segment_opts.decoding_ival;
clean_opts.max_min = 600; % 20-2000    �V 1000 only blinks, lower also wierd stuff (not only post blink), jump between 600-500
% split to start and stop for hyperopt...
% start bigger stop... both come 
clean_opts.rejection_freq_band = [0.5; 148]; % frequency band to filter signal, before rejecting trials(!)
clean_opts.filt_order = 3;

clean_opts.whisker_perc = 10;
clean_opts.whisker_length = 3;

%% Not using auc-timecut atm
% if running auc, make sure that decoding ival starts at 0(!)
% otherwise, will fail due to bug (it seems, auc ival is computed in offsets within
% decoding ival but applied in offsets from marker!)
auc_opts.compute_auc = 0; % 0 or 1, leave fixed on 0 for now!
% Note: Following only relevent if auc on 1 (so not relevant for now)
auc_opts.movAvgMsec = 400; % ms % 1-(decoding_length)
auc_opts.mean_ival_length_ms = 100; % ms % 1-(decoding_length)
auc_opts.auc_thr = 0.3;% %/100 0-1, exclusive (should not be exactly 0 or 1)
auc_opts.num_best_chans = 10;% #number of channels, 1-128, not sure what happens if channels are missing
