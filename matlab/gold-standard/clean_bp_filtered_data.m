function [clean_trials, rejected_channels] = clean_bp_filtered_data(...
        EEG_DAT, EOG_DAT, MRK, clean_opts)
    [filter_a,filter_b] = generate_filter_coefficients(clean_opts.rejection_freq_band, ...
            clean_opts.filt_order, EEG_DAT.fs);

    %% Filter data
    EEG_fb = proc_filt(EEG_DAT,filter_b{1},filter_a{1});

    epo_fb = proc_segmentation(EEG_fb,MRK,clean_opts.rejection_ival);
    epo_EOG = proc_segmentation(EOG_DAT,MRK,clean_opts.rejection_ival);

    %Clean completely? remember clean_trials?
    %% Clean Data
    [~, rejected_trials, rejected_channels] = clean_data(...
                epo_fb, epo_EOG, clean_opts); 
    all_trials = 1:size(epo_fb.x, 3);
    clean_trials = setdiff(all_trials, rejected_trials);
    % trials should still be sorted ascendingly
    assert(isequal(clean_trials, sort(clean_trials)));
end