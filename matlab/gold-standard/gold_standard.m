%% Clear worspace
clear

custom_start_bbci_toolbox

debug_run = false;

%% Run through several possible configs sequentially
configs = {@exp_ival_500_4000_300_fs_0_144_hz};
files = get_files_without_last_two_runs();
for config = configs
    %% Get Default Independent Variables
    get_independent_vars
    %% Overwrite independent vars by next config
    config = config{1};
    config(); % should set result folder variable
    %% Regenerate filterbands in case it's necessary
    filt_opts.filterbands = generate_filterbank(filt_opts);
    mkdir(result_folder);

    if (debug_run == true)
        segment_opts.nfolds = 2;
        load_ival = [0 4 * 60 * 1000];
    else
        load_ival = false;
    end

    for file_cell = files
        file_path = file_cell{1};
        fprintf('Computing for %s\n', file_path);
        [EEG_DAT, EMG_DAT, EOG_DAT, MRK, MNT, pairs] = load_data(file_path, ...
            preproc_opts, load_ival);
        name_resultfile = construct_result_files_name(file_path, ...
            filt_opts.filterbands, segment_opts.decoding_ival);
        name_resultfile = fullfile(result_folder, name_resultfile); 
        if (debug_run == true)
            name_resultfile = strcat(name_resultfile, '.debug_test');
        end
        do_single_filter_multiclass = true;
        results = filterbank_multiclass(EEG_DAT, EOG_DAT, MRK, ...
           filt_opts, clean_opts, preproc_opts, ...
            segment_opts, auc_opts, ...
           pairs, do_single_filter_multiclass);

       % todo maybe change that you add all properties to save
       % to result object and then save it in one command?
       % then maybe funciton not needed or atleast much less parameters :)
        save_results(name_resultfile, results.binary, results.multiclass, ...
            results.filterbank, results.filterbank_multiclass, ...
            results.num_trials, results.folds, results.running_time_sec,...
            preproc_opts, clean_opts, filt_opts, segment_opts, auc_opts);

    end
end

%% Clear workspace to free memory! 
clear