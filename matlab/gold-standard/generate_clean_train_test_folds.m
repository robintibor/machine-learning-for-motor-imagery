function [folds, rejected_channels] = ...
    generate_clean_train_test_folds(EEG_DAT, EOG_DAT, MRK, ...
        clean_opts, nfolds)
   
    [clean_trials, rejected_channels] = clean_bp_filtered_data(...
        EEG_DAT, EOG_DAT, MRK, clean_opts);
    
    % split into folds exactly like scikit-learn
    folds = chron_k_fold_train_test(numel(clean_trials), ...
        nfolds);

    for fold_i = 1:nfolds
        training_inds = folds(fold_i).train;
        test_inds = folds(fold_i).test;
        % have to map these indices back to original indices in full set
        clean_training_inds = clean_trials(training_inds);
        clean_test_inds = clean_trials(test_inds);
        folds(fold_i).train = clean_training_inds;
        folds(fold_i).test = clean_test_inds;
    end    
end