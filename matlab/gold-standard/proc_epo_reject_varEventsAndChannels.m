function [epo, rClab, rTrials, nfo] = proc_epo_reject_varEventsAndChannels(epo, varargin)
% [epo, rClab, rTrials, nfo] = proc_epo_reject_varEventsAndChannels(epo, varargin)
% Wrapper function for epo_reject_varEventsAndChannels
% Author: Lukas Fiederer, lukas.fiederer@gmail.com, 2014
% See also:
% epo_reject_varEventsAndChannels reject_varEventsAndChannels
% proc_selectEpochs proc_selectChannels

[rClab, rTrials, nfo] = epo_reject_varEventsAndChannels(epo, varargin);
epo = proc_selectEpochs(epo,'not',rTrials);
epo = proc_selectChannels(epo,'not',rClab);