function folds = chron_k_fold_train_test( num_samples, num_folds )
%CHRON_K_FOLD_TRAIN_TEST Split given number of samples into given number of
%folds.
% Returns: Struct array of train/test splits
% this is replicating sklearn cross validation from,
% see https://github.com/scikit-learn/scikit-learn/blob/38104ff4e8f1c9c39a6f272eff7818b75a27da46/sklearn/cross_validation.py#L328


%%First set base size of all folds, then split remaining sampples
% if (number of samples not divisible by number of folds)
% into the folds, starting from the first
fold_sizes = floor(num_samples / num_folds)  * ones(num_folds, 1);
remaining_samples = mod(num_samples, num_folds);
fold_sizes(1:remaining_samples) = fold_sizes(1:remaining_samples) + 1;

folds = struct();
test_fold_start = 1;
for fold_i = 1:num_folds
    test_fold_stop = test_fold_start + fold_sizes(fold_i) - 1;
    test_fold = test_fold_start:test_fold_stop;    
    train_fold =  [1:test_fold_start-1 test_fold_stop+1:num_samples];
    test_fold_start = test_fold_stop + 1;
    folds(fold_i).train = train_fold;
    folds(fold_i).test = test_fold;
    assert(isempty(intersect(test_fold, train_fold)));
    assert(numel(train_fold) + numel(test_fold) == num_samples);
end


end
