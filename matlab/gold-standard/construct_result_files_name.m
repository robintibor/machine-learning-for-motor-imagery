function  name_resultfiles = construct_result_files_name(file_path, ...
    filterbands, decoding_ival)

%% Generate name of result files
%TODO(Robin): change to sth meaningful
[~, filename, ~] = fileparts(file_path);
file_name_and_dot_BBCI = strsplit(filename, '_');
filename = file_name_and_dot_BBCI{1};
ival_string = strcat('ival_', num2str(decoding_ival(1)), '_', ...
    num2str(decoding_ival(2)));
name_resultfiles = [get_name_resultfiles(filename, filterbands)  ...
    ival_string '_results'];
end

function name_resultfiles = get_name_resultfiles(filename, filterbank, steps)

name_resultfiles = [filename '_' num2str(size(filterbank,2)) 'filters' num2str(min(filterbank(:))) '-' num2str(max(filterbank(:)))];

if nargin >=3
    if isstruct(steps)
        fields = fieldnames(steps);
        for ii = 1:numel(fields)
            if eval(['steps.' fields{ii}])
                name_resultfiles = [name_resultfiles '_' fields{ii}];
            end
        end
    elseif iscell(steps)
        error('Automated name generation of result files not yet implemented for cells!')
    end
end
end