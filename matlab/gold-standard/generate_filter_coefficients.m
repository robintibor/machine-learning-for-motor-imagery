function  [a,b] = generate_filter_coefficients(filterbands, filt_order, sampling_rate)
    b = cell(size(filterbands,2),1);
    a = cell(size(filterbands,2),1);
    for i = 1:size(filterbands,2)
        [b{i},a{i}] = butter(filt_order, [filterbands(1,i), ...
            filterbands(2,i)]/sampling_rate*2 ,'bandpass');
    end
end