function grd= util_WaveguardGridForLaplacian
% UTIL_WAVEGUARDGRIDFORLAPLACIAN - Channel grid to define neighbors for Laplacians
%
%Synopsis:
%  grd= util_WaveguardGridForLaplacian

% 09-2012 Benjamin Blankertz
% 10-2014 Lukas Fiederer

str= [
      '  _     _     _     _   Fp1   Fpz   Fp2     _     _     _     _\n' ...
      '  <     _     _     _     _ AFp3h AFp4h     _     _     _     _\n' ...
      'AF9   AF7   AF5   AF3   AF1   AFz   AF2   AF4   AF6   AF8  AF10\n' ...
      '  <  AFF9  AFF7 AFF5h  AFF3  AFF1  AFF2  AFF4 AFF6h  AFF8 AFF10\n' ...
      ' F9    F7    F5    F3    F1    Fz    F2    F4    F6    F8   F10\n' ...
      '  < FFT9h FFT7h FFC5h FFC3h FFC1h FFC2h FFC4h FFC6h FFT8h FFT10h\n' ...
      'FT9   FT7   FC5   FC3   FC1   FCz   FC2   FC4   FC6   FT8  FT10\n' ...
      '  < FTT9h FTT7h FCC5h FCC3h FCC1h FCC2h FCC4h FCC6h FTT8h FTT10h\n' ...
      ' T9    T7    C5    C3    C1    Cz    C2    C4    C6    T8   T10\n' ...
      '  < TTP9h TTP7h CCP5h CCP3h CCP1h CCP2h CCP4h CCP6h TTP8h TTP10h\n' ...
      ' M1   TP7   CP5   CP3   CP1   CPz   CP2   CP4   CP6   TP8    M2\n' ...
      '  < TPP9h TPP7h CPP5h CPP3h CPP1h CPP2h CPP4h CPP6h TPP8h TPP10h\n' ...
      ' P9    P7    P5    P3    P1    Pz    P2    P4    P6    P8   P10\n' ...
      '  < PPO9h PPO7h PPO5h     _  PPO1  PPO2     _ PPO6h PPO8h PPO10h\n' ...
      'PO9   PO7   PO5   PO3   PO1   POz   PO2   PO4   PO6   PO8  PO10\n' ...
      '  <     _     _     _ POO9h POO3h POO4h POO10h    _     _     _\n' ...
      '  _     _     _     _    O1    Oz    O2     _     _     _     _\n' ...
      '  <     _     _     _     _  OI1h  OI2h     _     _     _     _\n' ...
      '  _     _     _     _     I1   Iz    I2     _     _     _     _'];

nCols= 11;
G= cell(1, nCols);
[G{:}]= strread(sprintf(str), repmat('%s', [1 nCols]));
grd= cat(2, G{:});
