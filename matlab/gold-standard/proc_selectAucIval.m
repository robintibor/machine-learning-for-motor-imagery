function [epo, auc_ival, auc_peak, auc_peak_clab, auc] = proc_selectAucIval(epo,threshold, varargin)
% Syntax:
% [epo, auc_ival, auc_peak, auc_peak_index] = proc_selectAucIval(epo,threshold, varargin)
%
% Description:
% The goal of this function is to find a time interval where the
% discriminability between two classes is greatest. To do so it first
% computes the envelope of the signals. Then the area under the curve
% between both classes is calculated. Then, the electrode with the highest
% AUC value is used to find a time interval where the AUC values are above
% a certain % from the peak value.
%
% INPUTS:
% epo = struct of epochs corresponding to two classes
% threshold = value between 0-1 to be multiplicated with the maximal AUC
% value
% varargin = arguments for proc_* functions listed in "See also"
%
% OUTPUTS:
% epo = same as input but only for computed interval
% auc_ival = time interval in ms
% auc_peak = value of maximal AUC
% auc_peak_clab = label of electrode with highest AUC
% auc = struct with results of auc calculation
%
% Author: Lukas Fiederer, lukas.fiederer@gmail.com, 2014
% 
% See also:
% proc_envelope proc_aucValues bwconncomp proc_selectIval

epo_env = proc_envelope(epo,varargin);% Calculate envelope
auc = proc_aucValues(epo_env, varargin);% Calculate area under the curve
[auc_peaks, auc_peaks_indices] = max(abs(auc.x));% Find peaks for each electrode
[auc_peak, auc_peak_index] = max(auc_peaks);% Find max peak
auc_threshold = auc_peak*threshold;% Calculate threshold value
auc_windows = bwconncomp((abs(auc.x(:,auc_peak_index))-auc_threshold) > 0);% Get intervals above threshold
auc_window = auc_windows.PixelIdxList{cellfun(@(x)( sum(x == auc_peaks_indices(auc_peak_index)) ), auc_windows.PixelIdxList)==1};% Get interval where peak is located (maybe could check if this is longest interval)
auc_ival = [auc_window(1) auc_window(end)]./epo_env.fs.*1000;% In ms!!!!!!!! Get start and end of interval
epo = proc_selectIval(epo,auc_ival,varargin);% Select interval in data
auc_peak_clab = epo.clab{auc_peak_index};
