function [classifications, accuracy] = weighted_voting(binary_out_test, ...
            labels, pairs)
npairs = size(pairs, 1);
class_sums = zeros(size(labels));
nclasses = size(labels, 1);
assert(nclasses == 4);
for class_nr = 1:nclasses
     for pair_i = npairs:-1:1
         this_class_ind = find(cell2mat(pairs(pair_i, :)) == class_nr);
         if (~isempty(this_class_ind))
             % we need to add or subtract the output
             % depending on if we are first or second class 
             % in binary classifier
             if (this_class_ind == 1)
                class_sums(class_nr, :) = class_sums(class_nr, :) -...
                    binary_out_test{pair_i};
             else
                assert(this_class_ind == 2);
                class_sums(class_nr, :) = class_sums(class_nr, :) + ...
                    binary_out_test{pair_i};
             end
         end
     end
end
[~,max_class] = max(class_sums);
classifications = max_class;

% Convert from one-hot labels to class numbers
[~, actual_class] = max(labels);
accuracy = sum(classifications == actual_class) / numel(actual_class); 
end