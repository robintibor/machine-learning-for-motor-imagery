function results = one_filter_multiclass_csp(MRK, ...
    folds, pairs, binary_results)
%% Set some useful variables
nfilterbands = size(binary_results,1);
nfolds = numel(folds);
npairs = size(pairs,1);

%% Initialize result variables
cells = cell(nfilterbands, nfolds);
results = struct('train_accuracy', cells, 'test_accuracy', cells, ...
    'test_labels', cells, 'test_predictions', cells);

for filt_i = nfilterbands:-1:1
    % predict all test folds with binary classifiers
    % make weighted voting
    for fold_i = nfolds:-1:1 
        binary_out_train = cell(1, npairs);
        binary_out_test = cell(1, npairs);
        % ========================================================================
        %% BEGIN LOOP OVER ALL POSSIBLE BINARY PAIRS, Make prediction
        % ========================================================================
        for pair_i = size(pairs,1):-1:1 %TODO (Robin): what does this comment mean?=> sum(sum(~cellfun(@isempty,pairs)))% Anything better than sum(sum())?!?!?!
            LDA_classifier = ...
                binary_results(filt_i, fold_i,pair_i).LDA_classifier;
            
            train_features = ...
                binary_results(filt_i, fold_i, pair_i).train_features_full_fold;
            out_train = apply_separatingHyperplane(LDA_classifier, ...
                train_features);
            binary_out_train{pair_i} = out_train;
            
            test_features = ...
                binary_results(filt_i, fold_i,pair_i).test_features_full_fold;
            out_test = apply_separatingHyperplane(LDA_classifier, ...
                test_features);          
            binary_out_test{pair_i} = out_test;
        end
        % now we want to compute probability for a class
        % by summing up all values of the binary classifiers
        
        %% Select training and testing markers,
        % compute weighted voting
        train_inds = folds(fold_i).train;
        MRK_train = mrk_selectEvents(MRK, train_inds);
        train_labels = MRK_train.y;
        [~, train_accuracy] = weighted_voting(binary_out_train, ...
            train_labels, pairs);

        test_inds = folds(fold_i).test;
        MRK_test = mrk_selectEvents(MRK, test_inds);
        test_labels = MRK_test.y;
        [test_predictions, test_accuracy] = weighted_voting(binary_out_test, ...
            test_labels, pairs);
        
        results(filt_i, fold_i).train_accuracy = train_accuracy;
        results(filt_i, fold_i).test_accuracy = test_accuracy;
        results(filt_i, fold_i).test_labels = test_labels;
        results(filt_i, fold_i).test_predictions = test_predictions;

    end
        % ###################################
        %% END 5-FOLD CROSS-VALIDATION LOOP
        % ###################################
    
end

end