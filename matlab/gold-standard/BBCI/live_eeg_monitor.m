function eeg_ax = live_eeg_monitor(bbci, varargin)
%LIVE_EEG_MONITOR - Display raw-EEG signals is real-time!
%
%This scripts opens a figure and plots the real-time EEG into as traces.
%Currently, one can stop the visualization bz Stop-Marker 255 or by Ctrl-C.
%When stopping with Ctrl-C, a ''bbci_apply_close(bbci)'' is advisable!
%
%Synopsis:
% live_eeg_monitor(BBCI, OPT)
% 
%Arguments:
%  BBCI -  the field 'calibrate' holds parameters that specify the EEG
%  acquisition. espeacially, bbci.source should be specified accordingly.
%
%  OPT  -  some further options (optional)
%  
% (firstVersion) 03-2014 JohannesHoehne

fprintf('Press <q> in activated figure to quit the EEG Monitor.\');
fig_set(1, 'GridSize',[1 1]);

props = {
	'v_position', 0.01
	'height', 0.98
	'h_gaps', [0.02 0 0.02]
	'band', [2 30]
	'clab', '*'
	'range', 300
	'trace_color', []
	'manual_refChan', []
	'zeroline_color', 0.7*[1 1 1]
	'monitor_fps', 25};

if nargin==0,
	eeg_ax =props; return
end

%% Process input arguments
opt= opt_proplistToStruct(varargin{:});
[props, isdefault]= opt_setDefaults(opt, props);


%% - this is the actual code
bbci.source.record_signals= 0;
bbci.feature = [];
bbci.feature.ival = [-100 0];
if isfield(bbci.source.acquire_param{1}, 'fs')
	[filt_b,filt_a]= cheby2(5, 20, props.band/bbci.source.acquire_param{1}.fs*2);
	bbci.signal = [];
	bbci.signal.proc= {{@online_filt, filt_b, filt_a}};
	fprintf('bandpass filter was set up\n');
else
	fprintf('no sampling freq found -- no filters')
end

bbci= bbci_apply_setDefaults(bbci);
[data, bbci]= bbci_apply_initData(bbci);



panel_eeg=  props;

winlen= 8000;
winlen_sa= winlen/1000*data.source.fs;
clearlen= 100;
clearlen_sa= clearlen/1000*data.source.fs;
xstep= data.source.fs/panel_eeg.monitor_fps;


eeg_ax= subplotxl(1, 1, 1, ...
	[panel_eeg.v_position 0 1-panel_eeg.v_position-panel_eeg.height], panel_eeg.h_gaps);
eeg_chans= util_chanind(bbci.source.acquire_param{1}.clab, panel_eeg.clab);
if isempty(panel_eeg.trace_color),
	panel_eeg.trace_color= cmap_rainbow(length(eeg_chans));
end
YLim= ([-2.5 2.5] +[0 length(eeg_chans)-1]) * panel_eeg.range;
hold on;
eeg_YData= zeros(length(eeg_chans), winlen_sa);
eeg_YOffset= ones(xstep,1)*[0:length(eeg_chans)-1] * panel_eeg.range;
for cc= 1:length(eeg_chans),
	y0= eeg_YOffset(1,cc);
	line([1 winlen_sa],[y0 y0], 'Color',panel_eeg.zeroline_color);
	eeg_trace(cc)= plot(y0 + nan(winlen_sa,1), ...
		'Color',panel_eeg.trace_color(1+mod(cc-1,length(eeg_chans)),:));
	eeg_YData(cc,:)= get(eeg_trace(cc), 'YData');
end
eeg_bar= line([1 1], YLim, 'Color','k', 'LineWidth',2);
set(eeg_ax, 'Box','on', ...
	'XLim', [1 winlen_sa], ...
	'YLim', YLim, ...
	'XTick',[], ...
	'YTick',[]);
set(gcf, 'KeyPressFcn',@live_eeg_monitor_keypress);

run= true;
x0= 1;
go_to_beginning  = 0; ptr_old = -10;
eeg_YOffsetBase= [0:length(eeg_chans)-1] * panel_eeg.range;
while run,
	[data.source, data.marker]= ...
		bbci_apply_acquireData(data.source, bbci.source, data.marker);
	if ~data.source.state.running,
		break;
	end

	xstep= size(data.source.x,1);
	eeg_YOffset = ones(xstep,1)*eeg_YOffsetBase;

	if xstep==0,
		continue;
	end
	x0= x0 + xstep;
	xiv= [x0-xstep+1:x0];
	ptr= 1+mod(x0, winlen_sa);

	if ptr_old > ptr,  go_to_beginning = 1; end
	ptr_old = ptr;
	if go_to_beginning
		eeg_YOffsetBase = eeg_YOffsetBase -  repmat(nanmean(eeg_YData,2),1, 1)' + ...
			[0:length(eeg_chans)-1] * panel_eeg.range				;
		go_to_beginning = 0;
	end
	ptr_iv= 1+mod(xiv, winlen_sa);
	ptr_clear= 1+mod(x0+1:x0+clearlen_sa, winlen_sa);

	% update EEG traces
	set(eeg_bar, 'XData', [1 1]*ptr);
	eeg_YData(:,ptr_iv)= (data.source.x(:, eeg_chans))';
	if ~isempty(props.manual_refChan)
		eeg_YData(:,ptr_iv) = eeg_YData(:,ptr_iv) - repmat(eeg_YData(props.manual_refChan,ptr_iv),length(eeg_chans),1);
	end
	eeg_YData(:,ptr_iv) = eeg_YData(:,ptr_iv) + eeg_YOffset';
	eeg_YData(:,ptr_clear)= NaN;
	for cc= 1:length(eeg_chans),
		set(eeg_trace(cc), 'YData',eeg_YData(cc,:));
	end
	drawnow;
	data.marker.current_time= data.source.time;
	run= bbci_apply_evalQuitCondition(data.marker, bbci);
end

bbci_apply_close(bbci, data);
close

end



function ax= subplotxl(m, n, p, mv, mh)
%ax= subplotxl(m, n, p, mv, mh)
%
% copied from toolbox
if ~exist('mv','var') || isempty(mv), mv= 0; end
if ~exist('mh','var') || isempty(mh), mh= 0; end

if length(mv)==1, mv= [mv mv mv]; end
if length(mv)==2, mv= [mv mv(1)]; end
if length(mh)==1, mh= [mh mh mh]; end
if length(mh)==2, mh= [mh mh(1)]; end

if iscell(p),
  p1= p{1};
  p2= p{2};
  if length(p1)==1 && length(p2)>1,
    p1= p1*ones(1, length(p2));
  elseif length(p2)==1 && length(p1)>1,
    p2= p2*ones(1, length(p1));
  end
  ax= zeros(1, length(p1));
  for ii= 1:length(p1),
    ax(ii)= subplotxl(m, n, [p1(ii) p2(ii)], mv, mh);
  end
  return;
end

if isempty(n),
  mn= m;
  m= floor(sqrt(mn));
  n= ceil(mn/m);
end

pv= ( 0.999 - mv(1) - mv(3) - mv(2)*(m-1) ) / m;
ph= ( 0.999 - mh(1) - mh(3) - mh(2)*(n-1) ) / n;
if length(p)==1,
  iv= m - 1 - floor((p-1)/n);
  ih= mod(p-1, n);
else
  iv= m - p(1);
  ih= p(2) - 1;
end
pos= [mh(1) + ih*(mh(2)+ph),  mv(1) + iv*(mv(2)+pv),  ph,  pv];
ax= axes('position', pos);

end