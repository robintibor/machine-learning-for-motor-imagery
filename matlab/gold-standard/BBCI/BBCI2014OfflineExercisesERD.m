%% BBCI2014 Offline exercise
startup_bbci_toolbox('Dir', 'A:\matlab_toolboxes\BBCI', 'DataDir', 'A:\matlab_toolboxes\BBCI'...
    , 'RawDir', 'A:\matlab_toolboxes\BBCI\demoRaw', 'MatDir', 'A:\matlab_toolboxes\BBCI\demoMat'...
    ,'TmpDir','A:\matlab_toolboxes\BBCI\tmp','FigDir','A:\matlab_toolboxes\BBCI\fig')

[cnt, mrk_orig, hdr] = file_readBV('VPkg_08_08_07\calibration_motorimageryVPkg*');

classDef= {1, 2; 'left','right'};
mrk = mrk_defineClasses(mrk_orig, classDef);

dat = proc_spectrum(cnt,[0 50]);
figure(1)
h = plot(dat.t,dat.x);
legend(dat.clab)

dat = proc_spectrum(proc_selectChannels(cnt,{'C3','C4','Cz','O1','P7'}),[0 50]);
figure(2)
h = plot(dat.t,dat.x);
legend(dat.clab)

epo= proc_segmentation(cnt, mrk, [-1000 5000]);
dat = proc_spectrum(epo,[0 50]);
figure(3)
h = plot_channel(dat, {'C3', 'C4'});

figure(4)
mnt = mnt_setElectrodePositions(epo.clab);
mnt= mnt_setGrid(mnt, 'L');
h = grid_plot(dat,mnt);
fv = proc_rSquareSigned(dat);
grid_addBars(fv)

band_bci = [10 12];
[b,a] = butter(5,band_bci/cnt.fs*2);
dat = proc_filt(cnt,b,a);
figure(5)
h = plot(cnt.x);
figure(6)
h = plot(dat.x);

epo = proc_segmentation(dat,mrk,[-1000 6000]);
figure(7)
h = grid_plot(epo,mnt);
epo = proc_envelope(epo);
figure(8)
h = grid_plot(epo,mnt);
fv = proc_rSquareSigned(epo);
grid_addBars(fv)

epo = proc_baseline(epo,[-1000 0]);
figure(9)
h = grid_plot(epo,mnt);
fv = proc_rSquareSigned(epo);
grid_addBars(fv)

figure(10)
plot_scalpEvolutionPlusChannel(epo,mnt,{'C3','C4'},[-1000 6000])

figure(11)
plot_channel(fv,{'C3','C4'})

%%
[mrk, rclab, rtrials]=  reject_varEventsAndChannels(cnt,mrk,[-1000 6000], ...
                                 'DoBandpass', 1, ...
                                 'Verbose', 1);
                             
dat = proc_spectrum(cnt,[0 50]);
figure(1)
h = plot(dat.t,dat.x);
legend(dat.clab)

dat = proc_spectrum(proc_selectChannels(cnt,{'C3','C4','Cz','O1','P7'}),[0 50]);
figure(2)
h = plot(dat.t,dat.x);
legend(dat.clab)

epo= proc_segmentation(cnt, mrk, [-1000 5000]);
dat = proc_spectrum(epo,[0 50]);
figure(3)
h = plot_channel(dat, {'C3', 'C4'});

figure(4)
mnt = mnt_setElectrodePositions(epo.clab);
mnt= mnt_setGrid(mnt, 'L');
h = grid_plot(dat,mnt);
fv = proc_rSquareSigned(dat);
grid_addBars(fv)

band_bci = [10 12];
[b,a] = butter(5,band_bci/cnt.fs*2);
dat = proc_filt(cnt,b,a);
figure(5)
h = plot(cnt.x);
figure(6)
h = plot(dat.x);

epo = proc_segmentation(dat,mrk,[-1000 6000]);
figure(7)
h = grid_plot(epo,mnt);
epo = proc_envelope(epo);
figure(8)
h = grid_plot(epo,mnt);
fv = proc_rSquareSigned(epo);
grid_addBars(fv)

epo = proc_baseline(epo,[-1000 0]);
figure(9)
h = grid_plot(epo,mnt);
fv = proc_rSquareSigned(epo);
grid_addBars(fv)

figure(10)
plot_scalpEvolutionPlusChannel(epo,mnt,{'C3','C4'},[-1000 6000])

figure(11)
plot_channel(fv,{'C3','C4'})

%%
figure, plot_scoreMatrix(fv, [1000 2000; 2000 3000; 3000 4000])

%% classification

% cls_ival = [2000 4000];
% cls_ival = [1000 4000];
cls_ival = [0 5000];
[b,a]= butter(5, band_bci/cnt.fs*2);
cnt_flt= proc_filt(cnt, b, a);

epo= proc_segmentation(cnt_flt, mrk, cls_ival);
fv_ival = proc_selectIval(epo, cls_ival);
[fv_csp, CSP_W, CSP_EIG, CSP_A] = proc_cspAuto(fv_ival, 6);
figure, 
subplot(2,2,1); plot_scalp(mnt, CSP_W(:,1), 'ScalePos', 'none'), title(['filter ' epo.className{1}])
subplot(2,2,2); plot_scalp(mnt, CSP_W(:,2), 'ScalePos', 'none'), title(['filter '  epo.className{2}])
subplot(2,2,3); plot_scalp(mnt, CSP_A(1,:), 'ScalePos', 'none'), title(['pattern '  epo.className{1}])
subplot(2,2,4); plot_scalp(mnt, CSP_A(2,:), 'ScalePos', 'none'), title(['pattern ' epo.className{2} ])

%% plot the variance and log-var features in 2D space
fv_csp = proc_variance(fv_csp);
figure, subplot(1,3,1)
xPlot = squeeze(fv_csp.x);
plot(xPlot(1,find(fv_csp.y(1,:))), xPlot(2,find(fv_csp.y(1,:))), 'ro'), hold on
plot(xPlot(1,find(fv_csp.y(2,:))), xPlot(2,find(fv_csp.y(2,:))), 'bo'), hold on
title('variance of CSP-channels')

subplot(1,3,2)
xPlot = log(squeeze(fv_csp.x));
plot(xPlot(1,find(fv_csp.y(1,:))), xPlot(2,find(fv_csp.y(1,:))), 'ro'), hold on
plot(xPlot(1,find(fv_csp.y(2,:))), xPlot(2,find(fv_csp.y(2,:))), 'bo'), hold on
title('log-var of CSP-channels')

subplot(1,3,3)
fv_varChan = proc_variance(proc_selectChannels(fv_ival, {'C3' 'C4'}));
xPlot = log(squeeze(fv_varChan.x));
plot(xPlot(1,find(fv_varChan.y(1,:))), xPlot(2,find(fv_varChan.y(1,:))), 'ro'), hold on
plot(xPlot(1,find(fv_varChan.y(2,:))), xPlot(2,find(fv_varChan.y(2,:))), 'bo'), hold on
title('log-var of channels C3 and C4')

%% classification

loss_overestimated = crossvalidation(fv_csp, @train_RLDAshrink)
% crossvalidation should not be done when CSP-filters were already computed

proc.train= {{'CSPW', @proc_cspAuto, 3}
    @proc_variance
    @proc_logarithm
    };
proc.apply= {{@proc_linearDerivation, '$CSPW'}
    @proc_variance
    @proc_logarithm
    };

loss_cspAll = crossvalidation(fv_ival, {@train_RLDAshrink}, ...
    'LossFcn', @loss_rocArea, ...
    'SampleFcn', {@sample_chronKFold, 4}, ...
    'Proc', proc)

loss_cspSub1 = crossvalidation(proc_selectChannels(fv_ival, 1:3:length(fv_ival.clab)), {@train_RLDAshrink}, ...
    'LossFcn', @loss_rocArea, ...
    'SampleFcn', {@sample_chronKFold, 4}, ...
    'Proc', proc)

loss_cspSub2 = crossvalidation(proc_selectChannels(fv_ival, {'Fz' 'C*' 'Pz'}), {@train_RLDAshrink}, ...
    'LossFcn', @loss_rocArea, ...
    'SampleFcn', {@sample_chronKFold, 4}, ...
    'Proc', proc)


loss_channelERD = crossvalidation(proc_flaten(proc_variance(fv_ival)), {@train_RLDAshrink}, ...
    'LossFcn', @loss_rocArea, ...
    'SampleFcn', {@sample_chronKFold, 4})


loss_subChannelERD = crossvalidation(proc_flaten(proc_variance(proc_selectChannels( fv_ival, {'C3' 'C4'} ))), {@train_RLDAshrink}, ...
    'LossFcn', @loss_rocArea, ...
    'SampleFcn', {@sample_chronKFold, 4})