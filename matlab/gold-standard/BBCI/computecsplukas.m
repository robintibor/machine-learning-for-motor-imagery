function [CSP_W, CSP_E, CSP_P] = computecsplukas(trials_class1, trials_class2, nFilters)

%% Calculate mean covariance of each class
c1 = NaN(size(trials_class1,2),size(trials_class1,2),size(trials_class1,3));
c2 = NaN(size(trials_class2,2),size(trials_class2,2),size(trials_class2,3));

for ii = 1:size(trials_class1,3)
    c1(:,:,ii) = cov(trials_class1(:,:,ii));
end
for ii = 1:size(trials_class2,3)
    c2(:,:,ii) = cov(trials_class2(:,:,ii));
end

c1 = mean(c1,3);
c2 = mean(c2,3);

%% Calculate eigenvectors
[W,D] = eig(c2,c1+c2);

score= diag(D);
[dd,di]= sort(score);
fi = [di(1:nFilters); di(size(W,2)-nFilters+1:end)];

CSP_E = score(fi);
CSP_W = W(:,fi);
P = pinv(W);
CSP_P = P(fi,:);
