function [dat, varargout]= proc_multiCSP(dat, varargin)
%PROC_CSPAUTO - Common Spatial Pattern Analysis with Auto Filter Selection
%
%Synopsis:
% [DAT, CSP_W, CSP_EIG, CSP_A]= proc_cspAuto(DAT, <OPT>);
% [DAT, CSP_W, CSP_EIG, CSP_A]= proc_cspAuto(DAT, NPATS);
%
%Arguments:
% DAT    - data structure of epoched data
% NPATS  - number of patterns per class to be calculated, 
%          default nChans/nClasses.
% OPT - struct or property/value list of optional properties:
%  .patterns - 'all' or matrix of given filters or number of filters. 
%      Selection depends on opt.selectPolicy.
%  .selectPolicy - determines how the patterns are selected. Valid choices are 
%      'all', 'equalperclass', 
%      'maxvalues' (attention: possibly not every class is represented!)
%      'maxvalueswithcut' (like maxvalues, but components with less than
%      half the maximal score are omitted),
%      'directorscut' (default, heuristic, similar to maxvalueswithcut, 
%      but every class will be represented), 
%      'matchfilters' (in that case opt.patterns must be a matrix),
%      'matchpatterns' (not implemented yet)
%  .covPolicy - 'normal', 'average' (default) or manually defined cov matrices
%      of size [nchans, nchans, 2] . 'average' calculates the average of the 
%      single-trial covariance matrices.
%  .score - 'eigenvalues', 'medianvar' (default) or 'auc' to
%       determine the components by the respective score
%
%Returns:
% DAT    - updated data structure
% CSP_W  - CSP projection matrix (filters)
% CSP_EIG- eigenvalue score of CSP projections (rows in W)
% CSP_A  - estimated mixing matrix (activation patterns)
%
%Description:
% calculate common spatial patterns (CSP).
% please note that this preprocessing uses label information. so for
% estimating a generalization error based on this processing, you must
% not apply csp to your whole data set and then do cross-validation
% (csp would have used labels of samples in future test sets).
% you should use the .proc (train/apply) feature in xvalidation, see
% demos/demo_validation_csp
%
%See also demos/demo_validate_csp

% Author(s): Benjamin Blankertz
props= { 'patterns'     3           'INT'
         'score'        'medianvar' '!CHAR(eigenvalues medianvar auc)'
         'covPolicy'    'average'   'CHAR|DOUBLE[- - 2]'
         'scaling'      'none'      'CHAR'
         'normalize'    0           'BOOL'
         'selectPolicy' 'directorscut'  'CHAR'
         'weight'       ones(1,size(dat.y,2))   'DOUBLE'
         'weightExp'    1           'BOOL'};

if nargin==0,
  dat = props; return
end

dat = misc_history(dat);
misc_checkType(dat, 'STRUCT(x clab y)'); 
if length(varargin)==1 && isnumeric(varargin{1}),
  opt= struct('patterns', varargin{1});
else
  opt= opt_proplistToStruct(varargin{:});
end
[opt, isdefault]= opt_setDefaults(opt, props);
opt_checkProplist(opt, props);



[T, nChans, nEpochs]= size(dat.x);

if size(dat.y,1)~=2,
  error('this function works only for 2-class data.');
end

%% calculate classwise covariance matrices
if isnumeric(opt.covPolicy),
  R= opt.covPolicy;
  if ~isequal(size(R), [nChans nChans 2]),
    error('precalculated covariance matrices have wrong size');
  end
else
  R= procutil_covClasswise(dat, opt);
end

%% do actual CSP calculation as generalized eigenvalues
path(path,'H:\lukasTMP\ffdiag_pack');

R = permute(R,[3 1 2]);
% Parameters
Classes = size(R,1); % number of classes
Chans = size(R,2); % number of channels
Pc = ones(1,Classes)./Classes; % uniform prior over classes

% Jointly diagonalize covariance matrices
disp('Jointly diagonalizing covariance matrices...');
[V,CD,stat] = ffdiag(shiftdim(R,1),eye(Chans));
V = V';

% Compute mutual information provided by each filter (approximation)
disp('Selecting spatial filters with maximum mutual information...');
for n1 = 1:1:Chans,
    w = V(:,n1);
    I(n1) = J_ApproxMI(w,R,Pc);
end
[dummy(n1,:), iMI] = sort(I,'descend');

W = V;
Wp = V(:,iMI(1:opt.patterns));

% score= diag(CD);
% la= score(iMI(1:opt.patterns));
la = 0; % Because CD is 3D and diag can only handle 2D

%% optional scaling of CSP filters to make solution unique
switch(lower(opt.scaling)),
 case 'maxto1',
  for kk= 1:size(Wp,2),
    [mm,mi]= max(abs(Wp(:,kk)));
    Wp(:,kk)= Wp(:,kk) / Wp(mi,kk);
  end
 case 'none',
 otherwise
  error('unknown scaling');
end


%% save old channel labels
if isfield(dat, 'clab'),
  dat.origClab= dat.clab;
end

%% apply CSP filters to time series
dat= proc_linearDerivation(dat, Wp, 'prependix','csp');

%% arrange optional output arguments
if nargout>1,
  varargout{1}= Wp;
  if nargout>2,
    varargout{2}= la;
    if nargout>3,
      A= pinv(W);
      varargout{3}= A(fi,:);
      if nargout>4,
        varargout{4}= fi;
      end
    end
  end
end

function I = J_ApproxMI(w,R,Pc)

% Set dimensions
M = size(R,1); % number of classes
N = size(R,2); % data dimension

% Compute marginalized covariance matrix
Rx = zeros(N,N);
for n1 = 1:1:M,
    Rx = Rx + Pc(n1)*reshape(R(n1,:,:),N,N);
end

% Normalize variance of x
wv = w'*Rx*w;
w = w./sqrt(wv);

% Compute gaussian mutual information
Ig = 1/2*log((w'*Rx*w));
for n1 = 1:1:M,
    Ig = Ig - 1/2*Pc(n1)*log(w'*reshape(R(n1,:,:),N,N)*w);
end

% Compute estimate of negentropy
J = 0;
for n1 = 1:1:M,
    J = J + Pc(n1)*(w'*reshape(R(n1,:,:),N,N)*w)^2;
end
J = (J - 1)^2;
J = 9/48*J;

% Compute estimate of mutual information with correction by negentropy
I = Ig - J;
