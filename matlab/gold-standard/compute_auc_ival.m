function auc_ival = compute_auc_ival(epo, movAvgMsec, ...
  num_best_chans, mean_ival_length_ms, auc_thr)
%COMPUTE_AUC_IVAL Summary of this function goes here
%   Detailed explanation goes here
    
    %% Compute envelope of the signal (assume it is bandpassed)
    epo_env = proc_envelope(epo,'MovAvgMsec',movAvgMsec);
    % Take the Mean Value of the envelope per trial and 
    % select 10 best chans by auc / class discriminability
    num_samples = size(epo_env.x, 1);
    epo_env_single_mean = proc_jumpingMeans(epo_env, num_samples);
    auc_per_chan = proc_aucValues(epo_env_single_mean);
    [~, best_chans] = sort(abs(auc_per_chan.x), 2, 'descend');
    epo_env = proc_selectChannels(epo_env, best_chans(1:num_best_chans));
    
    
    %% Compute means of envelope for best chans for given mean ival length
    num_samples_per_ival = round(mean_ival_length_ms * epo.fs / 1000.0);
    epo_env_means = proc_jumpingMeans(epo_env, num_samples_per_ival);
    
    %% Compute Auc values for all trials for all ivals
    % and select the best ival by looking at the peak auc value
    % and te largest segment around of ivals around it with a auc value
    % higher than a threshold (percentage * computed peak auc value)
    auc = proc_aucValues(epo_env_means);
    [auc_peaks_per_chan, auc_peaks_indices_per_chan] = max(abs(auc.x));
    [auc_peak, auc_peak_chan] = max(auc_peaks_per_chan);
    auc_threshold = auc_peak*auc_thr;
    auc_windows = bwconncomp((abs(auc.x(:,auc_peak_chan))-auc_threshold) > 0);
    auc_window = auc_windows.PixelIdxList{...
        cellfun(@(x)( sum(x == auc_peaks_indices_per_chan(auc_peak_chan))), ...
        auc_windows.PixelIdxList)==1};
    % -1 necessary since matlab has one-based indexing...
    % but time starts at 0 :)
    auc_window_in_ms = (auc_window - 1) *  mean_ival_length_ms;

   % also include full last mean-interval ...
   % so + mean_ival_length_ms at end...
   % i.e. if intervalls of 500ms, and intervals 1 and 2
   % => start 0 and 500 are selected, go from 0 to 1000...
    auc_ival = [auc_window_in_ms(1), ...
        auc_window_in_ms(end) + mean_ival_length_ms];
end

