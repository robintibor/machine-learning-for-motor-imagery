function filterbankCSP_patternAnalysis(memo, pair_info, filterbank, FV, divTr, filename)
% filterbank CSP filters and patterns analysis

if iscell(memo)
    for ii = 1:size(memo, 2)
        for yy = 1:size(memo, 1)
            plot_cspAnalysis(proc_selectEpochs FV
            
        end
    end
elseif isstruct(memo)
    for ii = 1:size(memo, 1)
        for yy = 1:size(memo, 2)
            for zz = 1:size(memo, 3)
                FV = proc_selectChannels
                FV = proc_filt
                FV = proc_segmentation
                FV = proc_selectClasses
                plot_cspAnalysis(proc_selectEpochs FV
            end
        end
    end
end
figure
plot_cspAnalysis(...
    proc_selectClasses(...
    proc_segmentation(...
    proc_filt(...
    proc_selectChannels(EEOG_DAT,'not','E*'),...
    b{filterbank(1,:)==65},a{filterbank(1,:)==65}),...
    MRK,decoding_ival),...
    [1,4]),...
    createWaveguardBBCImontage(MNT.clab),memo{5,filterbank(1,:)==65}{2,2},memo{5,filterbank(1,:)==65}{2,4},memo{5,filterbank(1,:)==65}{2,3});