function filterbank = generate_filterbank(filt_opts)
    %% Create filterbands with different widths
    % in lower frequencies and higher frequencies
    % Given Frequencies are assumed to be centers of
    % the bandpass i.e. a min frequency of 3 and
    % a low width of 2 would result in a
    % filterband from 1-3
    % Minimum of any filterband is 0.5
    % e.g. a min freq of 0 and a low_width of 2
    % result in a filterband from 0.5 to 1
    % (and not from -1 to 1)
    
    % Get Variables
    min_freq = filt_opts.min_freq;
    max_freq = filt_opts.max_freq;
    last_low_freq = filt_opts.last_low_freq;
    low_width = filt_opts.low_width;
    high_width = filt_opts.high_width;
    % Make low filterbank
    low_centers = min_freq:low_width:last_low_freq;
    low_filterbank = [low_centers - low_width/2; low_centers + low_width/2];
    low_filterbank = max(low_filterbank, 0.5);
    % Make high filterbank
    high_center_start = last_low_freq + low_width / 2 + high_width / 2;
    high_centers = high_center_start:high_width:max_freq;
    high_filterbank = [high_centers - high_width/2; ...
        high_centers + high_width/2];
    filterbank = [low_filterbank high_filterbank];
end