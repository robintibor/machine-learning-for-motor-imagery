%% Clear worspace
clear

%% Independent Variables
get_independent_vars

custom_start_bbci_toolbox

%% Load all runs but last one/two (hold-back data)
test = true;
file_path = get_data_file_path();
file_path = 'R:\BCI2000\4ClassMotor3-4sPause\08-Oct-2014_MaGlMoSc2\RawData\MaGlMoSc2S001R01_ds10_1-2.BBCI.mat';
file_path = 'R:\BCI2000\4ClassMotor3-4sPause\20-Aug-2014_MaVoMoSc1\RawData\MaVoMoSc1S001R01_ds10_1-11.BBCI.mat';

[EEG_DAT, EMG_DAT, EOG_DAT, MRK, MNT, pairs] = load_data(file_path, ...
    preproc_opts, test);

name_resultfiles = construct_result_files_name(file_path, ...
    filt_opts.filterbands, segment_opts.decoding_ival);