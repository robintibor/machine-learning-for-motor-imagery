function [EEG_DAT, EMG_DAT, EOG_DAT, MRK, MNT, pairs] = ...
    load_data(file_path, preproc_opts, ival)

if (nargin < 3 || ~ismatrix(ival) || numel(ival) ~= 2)
    [DAT, MRK, MNT] = file_loadMatlab(file_path);
else
    [DAT, MRK, MNT] = file_loadMatlab(file_path, 'Ival', ival);
end


%% Detrend data, OK before crossval because can be done online
% Robin: I think it is problematic because online would lead to different
% results...
if preproc_opts.detrend
     DAT.x = detrend(DAT.x);
end

%% Remove EMG channels
EEG_DAT = proc_selectChannels(DAT,{'not','E*'});
EMG_DAT = proc_selectChannels(DAT,{'EMG*'});
EOG_DAT = proc_selectChannels(DAT,{'EOG*'});
clear DAT

%% Highpass, cut off low frequencies
filt_order = 3;
[b,a] = butter(filt_order, preproc_opts.low_cut_off_Hz/(EEG_DAT.fs/2),'high');
EEG_DAT = proc_filt(EEG_DAT, b,a);



%% Generate all possible pairs
classes = unique(MRK.event.desc);
nclasses = numel(classes);
pairs = cell(nclasses);% cell(nclasses+1);
for i = 1:nclasses
    for yy = i+1:nclasses% if i==nclasses then the loop does not initiate :-)
        pairs{i, yy} = {i yy};
    end
end
pairs = vertcat(pairs{:});



end