%% Get root folder
p = mfilename('fullpath');
sep = regexp(p,filesep);
rootf = p(1:sep(end));

%% Start BBCI toolbox
old_dir = cd([rootf 'BBCI']);
startup_bbci_toolbox
cd(old_dir)