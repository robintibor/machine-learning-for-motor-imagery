function [ EEG_DAT, MRK ] = resample_data( EEG_DAT, MRK, fs )
% Only resample if necessary = frequency actually different
if (EEG_DAT.fs ~= fs)    
    [EEG_DAT, MRK] = proc_resample(EEG_DAT, fs, 'mrk', MRK);
end
end

