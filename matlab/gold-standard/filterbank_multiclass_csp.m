function results = filterbank_multiclass_csp(MRK, folds, pairs, ...
    binary_results, filterbank_binary_results)
%% Set some useful variables
nfilterbands = size(binary_results,1);
nfolds = numel(folds);
npairs = size(pairs,1);

%% Initialize result variables
cells = cell(1, nfolds);
results = struct('train_accuracy', cells, 'test_accuracy', cells, ...
    'test_labels', cells, 'test_predictions', cells);

%% Collect Features and Labels
% Initialize feature vectors
features = struct();
features(nfolds, size(pairs, 1)).train = [];
features(nfolds, size(pairs, 1)).test = [];
labels = struct();
labels(nfolds).train = [];
labels(nfolds).test = [];

for filt_i = nfilterbands:-1:1
    for fold_i = nfolds:-1:1
        fprintf('Fold %d\n', fold_i);
        %% Select training and testing epochs
        training_inds = folds(fold_i).train;
        test_inds = folds(fold_i).test;
        MRK_train = mrk_selectEvents(MRK, training_inds);
        MRK_test = mrk_selectEvents(MRK, test_inds);
        % Remember labels
        if (isempty(labels(fold_i).train))
            labels(fold_i).train = MRK_train.y;
            labels(fold_i).test = MRK_test.y;
        else
            % Should be equal if not empty before...
            assert(isequal(MRK_train.y, labels(fold_i).train) && ...
                isequal(MRK_test.y, labels(fold_i).test));            
        end
        for pair_i = npairs:-1:1
            %% Collect actual features
            train_features = binary_results(filt_i, fold_i, pair_i).train_features_full_fold;
            test_features = binary_results(filt_i, fold_i, pair_i).test_features_full_fold;
            features(fold_i, pair_i).train = ...
                [features(fold_i, pair_i).train; train_features];
            features(fold_i, pair_i).test = ...
                [features(fold_i, pair_i).test; test_features];
        end
    end    
end

% #####################################
%% BEGIN N-FOLD CROSS-VALIDATION LOOP
% #####################################
for fold_i = nfolds:-1:1 
    binary_out_train = cell(1, size(pairs,1));
    binary_out_test = cell(1, size(pairs,1));
    % ========================================================================
    %% BEGIN LOOP OVER ALL POSSIBLE BINARY PAIRS, Make prediction
    % ========================================================================
    for pair_i = npairs:-1:1 %TODO (Robin): what does this comment mean?=> sum(sum(~cellfun(@isempty,pairs)))% Anything better than sum(sum())?!?!?!
        LDA_classifier = filterbank_binary_results(fold_i,pair_i).LDA_classifier;
        train_features = features(fold_i, pair_i).train;
        out_train = apply_separatingHyperplane(LDA_classifier, ...
            train_features);
        binary_out_train{pair_i} = out_train;
        test_features = features(fold_i, pair_i).test;
        out_test = apply_separatingHyperplane(LDA_classifier, ...
            test_features);          
        binary_out_test{pair_i} = out_test;
    end
    % now we want to compute probability for a class
    % by summing up all values of the binary classifiers
    train_labels = labels(fold_i).train;
    test_labels = labels(fold_i).test;
    
    [~, train_accuracy] = weighted_voting(binary_out_train, ...
        train_labels, pairs);
    [test_predictions, test_accuracy] = weighted_voting(binary_out_test, ...
        test_labels, pairs);
    results(fold_i).train_accuracy = train_accuracy;
    results(fold_i).test_accuracy = test_accuracy;
    results(fold_i).test_labels = test_labels;
    results(fold_i).test_predictions = test_predictions;
    
    % ###################################
    %% END 5-FOLD CROSS-VALIDATION LOOP
    % ###################################

end

end