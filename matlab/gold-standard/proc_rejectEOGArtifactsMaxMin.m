function [epoEEG, IARTE] = proc_rejectEOGArtifactsMaxMin(epoEEG, epoEOG, threshold, varargin)
% [epoEEG, IARTE] = proc_rejectEOGArtifactsMaxMin(epoEEG, epoEOG, MaxMin)
% Wrapper function for proc_rejectArtifactsMaxMin
% Author: Lukas Fiederer, lukas.fiederer@gmail.com, 2014
% See also:
% proc_rejectArtifactsMaxMin proc_selectEpochs

[~, IARTE] = proc_rejectArtifactsMaxMin(epoEOG, threshold, varargin);
epoEEG = proc_selectEpochs(epoEEG,'not',IARTE);