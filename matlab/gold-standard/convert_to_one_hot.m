function one_hot_labels = convert_to_one_hot(classifications, nr_of_classes)

    one_hot_labels = zeros(nr_of_classes, size(classifications,2));
    for trial_i = 1:size(classifications, 2)
       one_hot_labels(classifications(trial_i), trial_i) = 1; 
    end

end