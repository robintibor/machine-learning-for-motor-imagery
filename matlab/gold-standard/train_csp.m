function [CSP_filters, LDA_classifier] = train_csp(epo, auc_ival)
%TRAIN_CSP Train Common Spatial Patterns, return auc ival, 
% learned filters and weights

epo_cut = epo;
if nargin > 1 && ~isempty(auc_ival)...
    %% Extract auc interval
    epo_cut = proc_selectIval(epo, auc_ival);
end

%% Compute CSPs and select best using directors cut
[epo_cut, CSP_filters] = proc_cspAuto(epo_cut);

%% Take log(var(feature_vector))
epo_cut = proc_logarithm(proc_variance(epo_cut));

%% Train rLDA on features, oversampling still needs to be implemented!!!
x_size = size(epo_cut.x);
feature_size = [prod(x_size(1:end-1)) x_size(end)];
% TODO(Robin): what is this gamma for? it seems it will be ignored?
LDA_classifier = train_RLDAshrink(reshape(epo_cut.x, feature_size), ...
    epo_cut.y, 'Gamma',0, 'Scaling', 1);
end

