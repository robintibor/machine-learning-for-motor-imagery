function results = ...
    one_filter_binary_csp(EEG_DAT, MRK, filt_opts, auc_opts,...
        decoding_ival, rejected_channels, folds, pairs)
%ONE_FILTER_BINARY_CSP Compute the common spatial pattern weights for
% all class combinations, also learn the lda weights and compute a test
% error. Makes it possible to plot an see which frequency bands are good.

%% Set some useful variables
filterbands = filt_opts.filterbands;
nfilterbands = size(filterbands,2);
nfolds = numel(folds);
npairs = size(pairs,1);

%% Generate filters...
[filter_a,filter_b] = generate_filter_coefficients(filterbands, ...
    filt_opts.filt_order, EEG_DAT.fs);

%% Initialize result variables
cells = cell(nfilterbands, nfolds, npairs);
results = struct('auc_ival', cells, 'CSP_weights', cells, ...
    'LDA_classifier', cells, ...
    'train_inds_pair', cells, 'test_inds_pair', cells, ...
    'train_features_full_fold', cells, 'test_features_full_fold', cells, ...
    'train_accuracy', cells, 'test_accuracy', cells, ...
    'test_labels', cells, 'test_predictions', cells);

% ------------------------
%% BEGIN FILTERBANDS LOOP
% ------------------------
for filt_i = nfilterbands:-1:1
    fprintf('Filter bank from %d to %d (%d/%d)\n', ...
        filterbands(1, filt_i), filterbands(2,filt_i), ...
        nfilterbands - filt_i + 1, nfilterbands);

    %% Filter data
    EEG_fb = proc_filt(EEG_DAT,filter_b{filt_i},filter_a{filt_i});
    
    %% Epoch data
    epo_fb = proc_segmentation(EEG_fb,MRK,decoding_ival);

    % ========================================================================
    %% BEGIN LOOP OVER ALL FOLDS
    % ========================================================================
    for fold_i = nfolds:-1:1 
        %% Select training and testing epochs
        fprintf('Fold %d\n', fold_i);
        training_inds = folds(fold_i).train;
        test_inds = folds(fold_i).test;
        epo_train = proc_selectEpochs(epo_fb, training_inds);
        epo_test = proc_selectEpochs(epo_fb, test_inds);
        
        % Remove cleaned channels
        epo_train = proc_selectChannels(epo_train,'not',rejected_channels);
        epo_test = proc_selectChannels(epo_test,'not',rejected_channels);
        % ========================================================================
        %% BEGIN LOOP OVER ALL POSSIBLE BINARY CLASS PAIRS
        % ========================================================================
        for pair_i = size(pairs,1):-1:1
            %% Select training and testing epochs
            fprintf('Classes %d and %d\n', pairs{pair_i, 1}, pairs{pair_i, 2});
            [epo_train_pair, train_inds_pair] =  proc_selectClasses(epo_train, [pairs{pair_i,:}]);
            [epo_test_pair, test_inds_pair] = proc_selectClasses(epo_test, [pairs{pair_i,:}]);            
            
            %% Run actual csp training
            auc_ival = [];
            if (auc_opts.compute_auc)
                auc_ival = compute_auc_ival(epo_train_pair, ...
                    auc_opts.movAvgMsec, auc_opts.num_best_chans,...
                    auc_opts.mean_ival_length_ms, auc_opts.auc_thr);
                    fprintf('auc ival from %4d to %4d\n', auc_ival(1), auc_ival(2)); 
            end
            [CSP_weights, LDA_classifier] = train_csp(epo_train_pair, auc_ival);  
            
            %% Compute Train and Test features, also for multiclass
            % Multiclass is needed later and this way, we prevent
            % recomputation of the csp features...
            train_features_multi_class = compute_csp_features(epo_train, ...
                auc_ival, CSP_weights);
            test_features_multi_class = compute_csp_features(epo_test, ...
                auc_ival, CSP_weights);
            train_features_pair = train_features_multi_class(:, train_inds_pair);
            test_features_pair = test_features_multi_class(:, test_inds_pair);
            
            %% Predict CSP on test data
            [~, loss_train] = predict_rlda(train_features_pair, ...
                epo_train_pair.y, LDA_classifier);

            %% Predict CSP on test data
            [test_predictions, loss_test] = predict_rlda(test_features_pair,...
                epo_test_pair.y, LDA_classifier);
            
            
            %% Store some results            
            results(filt_i,fold_i,pair_i).auc_ival = auc_ival;
            results(filt_i,fold_i,pair_i).CSP_weights = CSP_weights;
            results(filt_i,fold_i,pair_i).LDA_classifier = LDA_classifier;            
            results(filt_i,fold_i,pair_i).train_features_full_fold = ...
                train_features_multi_class;
            results(filt_i, fold_i, pair_i).test_features_full_fold = ...
                test_features_multi_class;
            results(filt_i, fold_i, pair_i).train_inds_pair = ...
                train_inds_pair;
            results(filt_i, fold_i, pair_i).test_inds_pair = ...
                test_inds_pair;
            
            results(filt_i,fold_i,pair_i).train_accuracy = 1 - loss_train;
            results(filt_i, fold_i, pair_i).test_accuracy = 1 - loss_test;
            results(filt_i,fold_i, pair_i).test_labels = epo_test_pair.y;
            results(filt_i,fold_i, pair_i).test_predictions = test_predictions;
            
            %loss_train
            %loss_test
        end
    end
end 

end

