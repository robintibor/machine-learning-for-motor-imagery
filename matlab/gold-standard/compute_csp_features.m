function features = compute_csp_features(epo, auc_ival, CSP_W)
%COMPUTE_CSP_FEATURES Compute CSP Features for given weights and auc ival
%   auc_ival can be empty, then it is ignored...

if (~isempty(auc_ival))
    epo = proc_selectIval(epo, auc_ival);
end

epo = proc_linearDerivation(epo, CSP_W);
epo = proc_logarithm(proc_variance(epo));

x_size = size(epo.x);
features = reshape(epo.x, [prod(x_size(1:end-1)) x_size(end)]);

end

