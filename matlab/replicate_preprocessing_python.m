%% Create comparable preprocessing stuff for python


clear;
custom_start_bbci_toolbox;
get_independent_vars;
[DAT, MRK, MNT] = file_loadMatlab(...
    '/nfs/raid5/schirrmr/work/machine-learning-for-motor-imagery/data/BBCI-without-last-runs-datasets/JoBeMoSc01S001R01_ds10_1-11.BBCI.mat', ...
    'Ival', [0 20 * 60 * 1000]);

% only take top 3 chans
DAT = proc_selectChannels(DAT, 1:7);

epo = proc_segmentation(DAT, MRK, [0 4000]);

x = epo.x;
save('epo_x_jobe.mat', 'x');
event_desc = epo.event.desc;
save('event_jobe', 'event_desc');