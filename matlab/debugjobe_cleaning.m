addpath(genpath('gold-standard'))
addpath(genpath('debug_cleaning'))
custom_start_bbci_toolbox
get_independent_vars
filt_order = 3;
file_path = '/home/robintibor/work/motor-imagery/machine-learning-for-motor-imagery/data/BBCI-without-last-runs/JoBeMoSc01S001R01_ds10_1-11.BBCI.mat'
file_path = '/home/robintibor/work/motor-imagery/machine-learning-for-motor-imagery/data/BBCI-without-last-runs/MaJaMoSc1S001R01_ds10_1-11.BBCI.mat'


load_ival = false;
load_ival = [60 * 60 * 1000 120 * 60 * 1000]; 
preproc_opts.car_after_cleaning = 0;
[EEG_DAT, EMG_DAT, EOG_DAT, MRK, MNT, pairs] = load_data(file_path, ...
    preproc_opts, load_ival);

%% Filter data
[filter_a,filter_b] = generate_filter_coefficients(clean_opts.rejection_freq_band, ...
            filt_order, EEG_DAT.fs);
EEG_DAT = proc_filt(EEG_DAT,filter_b{1},filter_a{1});
EEG_DAT_NO_CAR = EEG_DAT;
EEG_DAT_CAR = proc_commonAverageReference(EEG_DAT, EEG_DAT.clab);
% select
EEG_DAT = EEG_DAT_CAR
epo = proc_segmentation(EEG_DAT,MRK,clean_opts.rejection_ival);
epo_EOG = proc_segmentation(EOG_DAT,MRK,clean_opts.rejection_ival);


[~, rejected_trials, rejected_channels] = debug_clean_data(...
            epo, epo_EOG, ...
            clean_opts.blinkrej, clean_opts.varrej, clean_opts.max_min); 
    







        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
[clean_trials, rejected_channels] = clean_bp_filtered_data(...
EEG_DAT, EOG_DAT, MRK, filt_order, clean_opts);

epo = proc_segmentation(EEG_DAT, MRK, [0 4000]);
fv= proc_variance(epo);
figure, plot(squeeze(fv.x)')



%Load data
%Plot threshold and variances in crucial step
% do this also after common average reference ...
% later chekc if same problem for other datasets
% if this is indeed the problem, 
% then first look at visualizations from rejct method
% also maynbe than plot channelwise with subplot
% also print thresholds
% answer question: do later signals get higher variances
% or is threshold lower?

% if nothing conclusive until then, 
% repeat everything for another dataset

epo_car = proc_commonAverageReference(epo, epo.clab);


fvcar = proc_variance(epo_car);
figure, plot(squeeze(fvcar.x)')

mean_nocar = mean(squeeze(fv.x(:, 180:end)))
mean_car = mean(squeeze(fvcar.x(:, 180:end)))


mean_nocar_before = mean(squeeze(fv.x(:, 1:150)))
mean_car_before = mean(squeeze(fvcar.x(:, 1:150)))
%% Old stuffs

rejected_trials_no_car = setdiff(1:size(epo.x, 3), clean_trials);
rejected_channels_no_car = rejected_channels;

preproc_opts.car_after_cleaning = 1;

[EEG_DAT, EMG_DAT, EOG_DAT, MRK, MNT, pairs] = load_data(file_path, ...
    preproc_opts, load_ival);
[clean_trials, rejected_channels] = clean_bp_filtered_data(...
EEG_DAT, EOG_DAT, MRK, filt_order, clean_opts);

rejected_trials_car = setdiff(1:size(epo.x, 3), clean_trials);
rejected_channels_car = rejected_channels;

%% Investigate variance/cleaning etc    
[filter_a,filter_b] = generate_filter_coefficients(clean_opts.rejection_freq_band, ...
            filt_order, EEG_DAT.fs);
% Filter data
EEG_DAT = proc_filt(EEG_DAT,filter_b{1},filter_a{1});
epo = proc_segmentation(EEG_DAT, MRK, [0 4000]);
% Cleaning part
fv= proc_variance(epo);
V= squeeze(fv.x);
perc= stat_percentiles(V(:), [0 100] + [1 -1]*whiskerperc);
thresh= perc(2) + whiskerlength*diff(perc);
EX= ( V > thresh );
rTrials= find( mean(EX,1)>0.2 );

V(:,rTrials)= [];
evGood(rTrials)= [];
%rejected_chan_inds = util_chanind(epo.clab, rejected_channels);
%V(rejected_chan_inds,:) = [];
whiskerperc = 10;
whiskerlength  = 3;
perc= stat_percentiles(V(:), [0 100] + [1 -1]*whiskerperc);
%    8.6967   77.6136
thresh= perc(2) + whiskerlength*diff(perc);
% 284.3645

%remove chans

nEvents = size(epo.x, 3);
rC= [];
isout= (V > thresh);
if sum(isout(:))>0.05*nEvents,
    qu= sum(isout,2)/sum(isout(:));
    rC= find( qu>0.1 & mean(isout,2)>0.05 );
    V(rC,:)= [];
    %% re-calculate threshold for updated V
    perc= stat_percentiles(V(:), [0 100] + [1 -1]*whiskerperc);
    thresh= perc(2) + whiskerlength*diff(perc);
end
rTr= find(any(V > thresh, 1));
figure, plot(V')
hold on
plot(1:406, repmat(thresh,1,406))

figure, plot(squeeze(fv.x)')