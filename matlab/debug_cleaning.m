addpath(genpath('gold-standard'))
custom_start_bbci_toolbox
%% Try out whether cleanings work ok with 0.5-144 clean bandpass
folder = 'results_fixed_ival_500_4000_150_fs_2_50_hz';
result_files = get_result_files_in_folder(folder);

results_before = struct();
for file_i = 1:numel(result_files)
    file_name = result_files(file_i).name;
    results = load(fullfile(folder, file_name));
    printable_results = extract_printable_results(results);
    if (file_i == 1)
        results_before = printable_results;
    else
        results_before = [results_before printable_results];
    end
end

data_files = get_files_without_last_two_runs();

%% Sort data files by subject
load_ival = false;
splitted_data_files = arrayfun(@(x) strsplit(x{1}, '\'), ...
    data_files, 'UniformOutput', false);
last_parts = arrayfun(@(x) x{1}{end}, splitted_data_files, ...
    'UniformOutput', false);
[~, inds] = sort(last_parts);

sorted_data_files = data_files(inds);

get_independent_vars
%clean_opts.rejection_freq_band = [0.5 144];
clean_opts.whisker_length=5;
clean_opts.whisker_perc = 10;
preproc_opts.car_after_cleaning = 1;
num_old_trials = ones(numel(sorted_data_files), 1) * NaN;
num_new_trials = ones(numel(sorted_data_files), 1) * NaN;
for subject_i = 1:numel(sorted_data_files)
  data_file = sorted_data_files{subject_i};
    fprintf('Computing for %s\n', data_file);
    [EEG_DAT, EMG_DAT, EOG_DAT, MRK, MNT, pairs] = load_data(data_file, ...
        preproc_opts, load_ival);
    [clean_trials, rejected_channels] = clean_bp_filtered_data(EEG_DAT, ...
        EOG_DAT, MRK, clean_opts);
    num_clean_trials = numel(clean_trials);
    num_clean_trials_before = results_before(subject_i).num_trials;
    fprintf('Difference: %d (new: %d, old: %d)\n', ...
        num_clean_trials - num_clean_trials_before, ...
        num_clean_trials, num_clean_trials_before);
    num_old_trials(subject_i) = num_clean_trials_before;
    num_new_trials(subject_i) = num_clean_trials;
end

