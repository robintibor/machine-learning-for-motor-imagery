nfo

nfo = 

            fs: 100
          clab: {1x58 cell}
             T: 70812
       nEpochs: 1
        length: 708.1200
        format: 'DOUBLE'
    resolution: [1x58 double]
          file: '/archive/matlab_toolboxes/BBCI/demoMat/VPkg_08_08_07/calibration_motorimageryVPkg'
       nEvents: 75
      nClasses: 3
     className: {'left'  'right'  'foot'}

diary off
