function [hdr, mrk, mnt, nfo, dat] = BBCI_import(filename,format, overwrite)
% Use BBCI_import to create header, marker and data .mat files compatible
% with the BBCI toolbox.
%
% INPUTS
%
% filename = string containing the path to the data file. If empty user
%            will be promted to select a file.
%
% format = string containing the format of the data. If empty format will
%          be detected from file extention. Support is planned for NeurOne,
%          Blackrock and BCI2000
%
% overwrite = boolean controling whether already existing files should be
%             overwritten or not.
%
% OUTPUTS structure as follows:
%
% nfo = struct(...
%     'fs', [], ... % Sampling frequency
%     'clab', {}, ... % Channel labels
%     'T', [], ... % Number of sampling points
%     'nEpochs', [], ... % Number of epochs
%     'length', [], ... % Duration in seconds
%     'format', '', ... % Data format, e.g. 'DOUBLE'
%     'resolution', [], ... % ?? Full of NaN [1xnChannels double]
%     'file', [], ... % Full path to file without extension
%     'nEvents', [], ... % Number of events/markers
%     'nClasses', [], ... % Number of BCI classes
%     'className', {} ... % Names of BCI classes {'left'  'right'  'foot'}
%     );
%
% mrk = struct(...
%     'time', [], ... % Millisecond of each marker
%     'event', struct(... % [1x1 struct]
%     'desc', []),... % Marker value [75x1 double]
%     'y', [], ... % State of each maker at each event (0/1)[3x75 double]
%     'className', {}, ... % Names of BCI classes {'left'  'right'  'foot'}
%     'orig', struct(... % Original marker struct with all events [1x1 struct]
%     )...
%     );
%
%
% mnt = struct(...
%     'x', [], ... % Coordinates centered on middline ?-1:1?[58x1 double]
%     'y', [], ... % Coordinates centered on central sulcus ?-1:1?[58x1 double]
%     'pos_3d', [], ... % 3D coordinates, different from above but still centered on middline and central sulcus. -1:1 (extreme electrodes)[3x58 double]
%     'clab', {}, ... % Channel labels {1x58 cell}
%     'box', [], ... % ? [2x59 double]
%     'box_sz', [], ... % ? [2x59 double]
%     'scale_box', [], ... % ? [2x1 double]
%     'scale_box_sz', [] ... % ? [2x1 double]
%     );
%
% dat = struct(...
%     'clab', [], ... % Channel labels {1x58 cell}
%     'fs', [], ... % Sampling frequency
%     'title', [], ... % Partent folder and filename without extension 'VPkg_08_08_07/calibration_motorimageryVPkg'
%     'file', [], ... % Full path to file without extension '/archive/matlab_toolboxes/BBCI/demoMat/VPkg_08_08_07/calibration_motorimageryVPkg'
%     'T', [], ... % Number of sample points, integer
%     'yUnit', [] ... % Unit 'V'
%     );
%
% hdr = struct(...
%     'DataFile', '', ... % Name of data file, with extension 'calibration_motorimageryVPkg.eeg'
%     'MarkerFile', '', ... % Name of marker file, with extension 'calibration_motorimageryVPkg.vmrk'
%     'DataFormat', '', ... % 'BINARY'
%     'DataOrientation', '', ... % 'MULTIPLEXED'
%     'DataType', [], ... % []
%     'NumberOfChannels', [], ... % Number of channels
%     'DataPoints', '', ... % Number of sample points as string
%     'SamplingInterval', [], ... % 10000
%     'BinaryFormat', '', ... % 'INT_16'
%     'UseBigEndianOrder', '', ... % 'NO'
%     'fs', [], ... % Sampling frequency
%     'len', [], ... % Duration of data in seconds
%     'endian', '', ... % 'l'
%     'clab', {}, ... % Channel labels
%     'clab_ref', {}, ... % ?? Empty {1x58 cell}
%     'scale', [], ... % ?? Scaling factor of amplifier? [1x58 double]
%     'unitOfClab', {}, ... % Per channel units {1x58 cell}
%     'unit', '' ... % Unit 'V'
%     );
%
%
% Author: Lukas Fiederer, lukas.fiederer@gmail.com, 2014.06.02
%
% ToDo:
%   - Add support for multiple files for Blackrock
%   - Add markers support for Blackrock and NeurOne?
%   - Add downsampling option
%   - Add ITmed support (c.f. BD_import_script.m, Importscript_RestingState_beb.m)
%   - Add feedback on progress
%
% Trackchange:
%   - 2014.10.13 Lukas:Started tracking changes
%   - 2014.10.13 Lukas:Replaced channelwise saving of data by file_saveMatlab
%   - 2014.10.13 Lukas:Now uses bciParams.Stimuli.Value(1,:) as marker text
%   - 2014.10.13 Lukas:Corrected bug of mrk.time which has to be in ms!!!
%   - 2014.10.13 Lukas:Added dialog for overwritting
%   - 2014.10.13 Lukas:Added automatic correction of wrong channel names
%   - 2014.10.14 Lukas:Added ascending sort of filenames
%   - 2014.10.24 Lukas: Changed path policy, now external toolboxes are in parent
%   folder
%   - 2014.10.28 Lukas: Reactivated channelwise saving as file_saveMatlab
%   does exactly the same thing, only taking longer and spamming.

% Add BBCI toolbox to path
[mfilePath, ~, ~] = fileparts(mfilename('fullpath'));
sep = regexp(mfilePath,filesep);
trd_prty_rt = mfilePath(1:sep(end));
if ~exist('BTB','var')
    addpath([trd_prty_rt 'BBCI'])
    startup_bbci_toolbox('DataDir','')
end

% If no filename is given, promt user to select file
if ~exist('filename','var') || isempty(filename)
    [filename,path,~] = uigetfile({'*.dat';'*.ns5';'*.bin';'*.hdr.mat'},'MultiSelect','On');
    if ~iscell(filename)
        filename = {filename};
    end
    if ~filename{1}
        return
    end
    filename = sort(filename);
    filename = cellfun(@(x)( [path x] ),filename,'UniformOutput',false);
end

% If no format is given, use filename extension as format
if ~exist('format','var') || isempty(format)
    [path, name, format] = cellfun(@fileparts,filename,'UniformOutput',false);
    format = cellfun(@(x)( x(2:end) ),format,'UniformOutput',false);% Remove '.'
else
    [path, name, ~] = cellfun(@fileparts,filename,'UniformOutput',false);
end
file = [path{1} filesep name{1} sprintf('_%i-%i',1,numel(filename))];

% If no overwite option is given, use default
if ~exist('overwrite','var') || isempty(overwrite)
    overwrite = 'ask';
end

% Check if .mat files already exist, if so notify user, load and return
if exist([file '.BBCI.mat'], 'file') && ischar(overwrite)
    choise = questdlg(sprintf('The file you are trying to generate already exists!\nDo you want to overwrite it?'),'File already exists','Cancel');
    switch choise
        case 'Yes'
            % Continue normaly
        case 'No'
            fprintf([...
                '###########################\n' ...
                'BBCI file already exists!\n' ...
                'Loading and returning instead of overwriting.\n' ...
                '###########################\n' ...
                ])
            load([file '.BBCI.mat'])
            return
        otherwise
            error('Wrong choise!')
    end
    
elseif exist([file '.BBCI.mat'], 'file') && ~overwrite
    fprintf([...
        '###########################\n' ...
        'BBCI file already exists!\n' ...
        'Loading and returning instead of overwriting.\n' ...
        '###########################\n' ...
        ])
    load([file '.BBCI.mat'])
    return
end

% Load data with tools appropriate for format
switch format{1}
    case {'dat', 'BCI2000'}
        addpath([trd_prty_rt 'BCI2000mex'])
        for ii = numel(filename):-1:1
            filestruct(ii).name = filename{ii};
        end
        [signal, states, bciParams]=load_bcidat(filestruct.name,'-calibrated');
        
        %         for n = numel(bciParams.ChannelNames.Value):-1:1
        %             eval(['ch' num2str(n) ' = signal(:,n);']);% Recorded data
        %         end
        
        nfo.fs = bciParams.SamplingRate.NumericValue; % Sampling frequency
        nfo.clab = bciParams.ChannelNames.Value'; % Channel labels
        nfo.T = size(signal, 1); % Number of sampling points
        nfo.nEpochs = 1; % Number of epochs
        nfo.length = nfo.T/nfo.fs; % Duration in seconds
        nfo.format = 'DOUBLE'; % Data format, e.g. 'DOUBLE'
        nfo.resolution = NaN(1,size(signal, 2)); % ?? Full of NaN [1xnChannels double]
        nfo.file = file; % Full path to file without extension
        nfo.nEvents = []; % Number of events/markers
        nfo.nClasses = []; % Number of BCI classes
        nfo.className = {}; % Names of BCI classes {'left'  'right'  'foot'}
        
        tmp = bwconncomp(states.StimulusBegin);
        mrk.time = cellfun(@(x) x(1)*1000/nfo.fs,tmp.PixelIdxList); % Millisecond of each marker
        mrk.event.desc = cellfun(@(x) states.StimulusCode(x(1)),tmp.PixelIdxList)'; % Marker value [75x1 double]
        % mrk.className = num2cell(num2str(unique(mrk.event.desc)))'; % Names of BCI classes {'left'  'right'  'foot'}
        mrk.className = bciParams.Stimuli.Value(1,:); % Names of BCI classes {'left'  'right'  'foot'}
        mrk.y = zeros(numel(mrk.className),numel(mrk.time)); % State of each maker at each event (0/1)[3x75 double]
        tmp = unique(mrk.event.desc);
        for ii = 1:numel(mrk.time)
            mrk.y(find(mrk.event.desc(ii) == tmp),ii) = 1;% Need to use find.
        end
        
        mnt = createWaveguardBBCImontage(nfo.clab);
        
        dat.x = signal;
        dat.clab = nfo.clab; % Channel labels {1x58 cell}
        dat.fs = nfo.fs; % Sampling frequency
        dat.title = []; % Partent folder and filename without extension 'VPkg_08_08_07/calibration_motorimageryVPkg'
        dat.file = nfo.file; % Full path to file without extension '/archive/matlab_toolboxes/BBCI/demoMat/VPkg_08_08_07/calibration_motorimageryVPkg'
        dat.T = nfo.T; % Number of sample points, integer
        dat.yUnit = '�V'; % Unit 'V'
        
        hdr.DataFile = filename; % Name of data file, with extension 'calibration_motorimageryVPkg.eeg'
        hdr.MarkerFile = ''; % Name of marker file, with extension 'calibration_motorimageryVPkg.vmrk'
        hdr.DataFormat = 'BINARY'; % 'BINARY'
        hdr.DataOrientation = ''; % 'MULTIPLEXED'
        hdr.DataType = []; % []
        hdr.NumberOfChannels = size(signal, 2); % Number of channels
        hdr.DataPoints = num2str(nfo.T); % Number of sample points as string
        hdr.SamplingInterval = []; % 10000
        hdr.BinaryFormat = ''; % 'INT_16'
        hdr.UseBigEndianOrder = ''; % 'NO'
        hdr.fs = nfo.fs; % Sampling frequency
        hdr.len = nfo.length; % Duration of data in seconds
        hdr.endian = ''; % 'l'
        hdr.clab = nfo.clab; % Channel labels
        hdr.clab_ref = cell(1,hdr.NumberOfChannels); % ?Reference? Empty {1x58 cell}
        hdr.scale = ones(1,hdr.NumberOfChannels); % ?? Scaling factor of amplifier? [1x58 double]
        hdr.unitOfClab = repmat({dat.yUnit},1,hdr.NumberOfChannels); % Per channel units {1x58 cell}
        hdr.unit = dat.yUnit; % Unit 'V'
        
    case {'ns5', 'Blackrock'}
        addpath(genpath([trd_prty_rt 'NPMK']))
        recording = openNSx(filename, 'read');
        
        %         for n = recording.MetaTags.ChannelCount:-1:1
        %             eval(['ch' num2str(n) ' = recording.Data(n,:)'';']);% Recorded data
        %         end
        
        nfo.fs = recording.MetaTags.SamplingFreq; % Sampling frequency
        nfo.clab = {recording.ElectrodesInfo(:).Label}; % Channel labels
        nfo.T = recording.MetaTags.DataPoints; % Number of sampling points
        nfo.nEpochs = 1; % Number of epochs
        nfo.length = nfo.T/nfo.fs; % Duration in seconds
        nfo.format = 'DOUBLE'; % Data format, e.g. 'DOUBLE'
        nfo.resolution = NaN(1,recording.MetaTags.ChannelCount); % ?? Full of NaN [1xnChannels double]
        nfo.file = file; % Full path to file without extension
        nfo.nEvents = []; % Number of events/markers
        nfo.nClasses = []; % Number of BCI classes
        nfo.className = {}; % Names of BCI classes {'left'  'right'  'foot'}
        
        
        mrk.time = []; % Sample point of each marker
        mrk.event.desc = []; % Marker value [75x1 double]
        mrk.className = {}; % Names of BCI classes {'left'  'right'  'foot'}
        mrk.y = []; % State of each maker at each event (0/1)[3x75 double]
        
        mnt = createWaveguardBBCImontage(nfo.clab);
        
        dat.x = recording.Data';
        dat.clab = nfo.clab; % Channel labels {1x58 cell}
        dat.fs = nfo.fs; % Sampling frequency
        dat.title = []; % Partent folder and filename without extension 'VPkg_08_08_07/calibration_motorimageryVPkg'
        dat.file = file; % Full path to file without extension '/archive/matlab_toolboxes/BBCI/demoMat/VPkg_08_08_07/calibration_motorimageryVPkg'
        dat.T = nfo.T; % Number of sample points, integer
        dat.yUnit = recording.ElectrodesInfo(1).AnalogUnits; % Unit 'V'
        
        hdr.DataFile = filename; % Name of data file, with extension 'calibration_motorimageryVPkg.eeg'
        hdr.MarkerFile = ''; % Name of marker file, with extension 'calibration_motorimageryVPkg.vmrk'
        hdr.DataFormat = 'BINARY'; % 'BINARY'
        hdr.DataOrientation = ''; % 'MULTIPLEXED'
        hdr.DataType = []; % []
        hdr.NumberOfChannels = recording.MetaTags.ChannelCount; % Number of channels
        hdr.DataPoints = num2str(nfo.T); % Number of sample points as string
        hdr.SamplingInterval = []; % 10000
        hdr.BinaryFormat = ''; % 'INT_16'
        hdr.UseBigEndianOrder = ''; % 'NO'
        hdr.fs = nfo.fs; % Sampling frequency
        hdr.len = nfo.length; % Duration of data in seconds
        hdr.endian = ''; % 'l'
        hdr.clab = nfo.clab; % Channel labels
        hdr.clab_ref = cell(1,hdr.NumberOfChannels); % ?Reference? Empty {1x58 cell}
        hdr.scale = ones(1,hdr.NumberOfChannels); % ?? Scaling factor of amplifier? [1x58 double]
        hdr.unitOfClab = {recording.ElectrodesInfo(:).AnalogUnits}; % Per channel units {1x58 cell}
        hdr.unit = dat.yUnit; % Unit 'V'
        
    case {'bin', 'NeurOne'}
        addpath([trd_prty_rt 'neurone_tools_for_matlab_1.1.3.5'])
        filesepPos = strfind(path{1}, filesep);
        recording = module_read_neurone(path{1}(1:filesepPos(end)), 'sessionPhaseNumber', str2double(path(filesepPos(end)+1:end)));
        
        nfo.clab = fieldnames(recording.signal); % Channel labels
        %         for n = numel(nfo.clab):-1:1
        %             eval(['ch' num2str(n) ' = recording.signal.(nfo.clab{n}).data'';']);% Recorded data
        %         end
        
        nfo.fs = recording.properties.samplingRate; % Sampling frequency
        nfo.T = size(D.data,2); % Number of sampling points
        nfo.nEpochs = 1; % Number of epochs
        nfo.length = recording.properties.length; % Duration in seconds
        nfo.format = 'DOUBLE'; % Data format, e.g. 'DOUBLE'
        nfo.resolution = NaN(1,numel(nfo.clab)); % ?? Full of NaN [1xnChannels double]
        nfo.file = file; % Full path to file without extension
        nfo.nEvents = []; % Number of events/markers
        nfo.nClasses = []; % Number of BCI classes
        nfo.className = {}; % Names of BCI classes {'left'  'right'  'foot'}
        
        
        mrk.time = []; % Sample point of each marker
        mrk.event.desc = []; % Marker value [75x1 double]
        mrk.className = {}; % Names of BCI classes {'left'  'right'  'foot'}
        mrk.y = []; % State of each maker at each event (0/1)[3x75 double]
        
        mnt = createWaveguardBBCImontage(nfo.clab);
        
        dat.x = recording.signal(:).data';
        dat.clab = nfo.clab; % Channel labels {1x58 cell}
        dat.fs = nfo.fs; % Sampling frequency
        dat.title = []; % Partent folder and filename without extension 'VPkg_08_08_07/calibration_motorimageryVPkg'
        dat.file = file; % Full path to file without extension '/archive/matlab_toolboxes/BBCI/demoMat/VPkg_08_08_07/calibration_motorimageryVPkg'
        dat.T = nfo.T; % Number of sample points, integer
        dat.yUnit = recording.signal.Input1.Unit; % Unit 'V'
        
        hdr.DataFile = filename; % Name of data file, with extension 'calibration_motorimageryVPkg.eeg'
        hdr.MarkerFile = ''; % Name of marker file, with extension 'calibration_motorimageryVPkg.vmrk'
        hdr.DataFormat = 'BINARY'; % 'BINARY'
        hdr.DataOrientation = ''; % 'MULTIPLEXED'
        hdr.DataType = []; % []
        hdr.NumberOfChannels = numel(nfo.clab); % Number of channels
        hdr.DataPoints = num2str(nfo.T); % Number of sample points as string
        hdr.SamplingInterval = []; % 10000
        hdr.BinaryFormat = ''; % 'INT_16'
        hdr.UseBigEndianOrder = ''; % 'NO'
        hdr.fs = nfo.fs; % Sampling frequency
        hdr.len = nfo.length; % Duration of data in seconds
        hdr.endian = ''; % 'l'
        hdr.clab = nfo.clab; % Channel labels
        hdr.clab_ref = cell(1,hdr.NumberOfChannels); % ?Reference? Empty {1x58 cell}
        hdr.scale = ones(1,hdr.NumberOfChannels); % ?? Scaling factor of amplifier? [1x58 double]
        hdr.unitOfClab = repmat({dat.yUnit},1,hdr.NumberOfChannels); % Per channel units {1x58 cell}
        hdr.unit = dat.yUnit; % Unit 'V'
        
    case {'mat', 'EEGtv'}
        
        load([file '.mat'], 'H')
        load([file(1:end-4) '.marker.mat'], 'M')
        load([file(1:end-4) '.data.mat'], 'D')
        %         for n = H.noc:-1:1
        %             eval(['ch' num2str(n) ' = D.data(n,:)'';']);% Recorded data
        %         end
        
        nfo.fs = H.sf; % Sampling frequency
        nfo.clab = H.cn'; % Channel labels
        nfo.T = H.nop; % Number of sampling points
        nfo.nEpochs = 1; % Number of epochs
        nfo.length = nfo.T/nfo.fs; % Duration in seconds
        nfo.format = 'DOUBLE'; % Data format, e.g. 'DOUBLE'
        nfo.resolution = NaN(1,numel(nfo.clab)); % ?? Full of NaN [1xnChannels double]
        nfo.file = file; % Full path to file without extension
        nfo.nEvents = []; % Number of events/markers
        nfo.nClasses = []; % Number of BCI classes
        nfo.className = {}; % Names of BCI classes {'left'  'right'  'foot'}
        
        
        mrk.time = M.xtick*1000/nfo.fs; % Millisecond of each marker
        mrk.event.desc = cellfun(@(x) sum(double(x)), M.xtickl); % Marker value [75x1 double]
        mrk.className = M.xtickl; % Names of BCI classes {'left'  'right'  'foot'}
        mrk.y = zeros(numel(mrk.className),numel(mrk.time)); % State of each maker at each event (0/1)[3x75 double]
        tmp = unique(mrk.event.desc);
        for ii = 1:numel(mrk.time)
            mrk.y(find(mrk.event.desc(ii) == tmp),ii) = 1;% Need to use find.
        end
        %%%%%%%%%%%%%%%%%%%%%%% BIG MISTAKES IN MRK.Y!!!!!!!!!!!!!!!!!
        % Matrix gets created!!!!
        
        mnt = createWaveguardBBCImontage(nfo.clab);
        
        dat.x = D.data';
        dat.clab = nfo.clab; % Channel labels {1x58 cell}
        dat.fs = nfo.fs; % Sampling frequency
        dat.title = []; % Partent folder and filename without extension 'VPkg_08_08_07/calibration_motorimageryVPkg'
        dat.file = file; % Full path to file without extension '/archive/matlab_toolboxes/BBCI/demoMat/VPkg_08_08_07/calibration_motorimageryVPkg'
        dat.T = nfo.T; % Number of sample points, integer
        dat.yUnit = '�V'; % Unit 'V'
        
        hdr.DataFile = filename; % Name of data file, with extension 'calibration_motorimageryVPkg.eeg'
        hdr.MarkerFile = ''; % Name of marker file, with extension 'calibration_motorimageryVPkg.vmrk'
        hdr.DataFormat = 'BINARY'; % 'BINARY'
        hdr.DataOrientation = ''; % 'MULTIPLEXED'
        hdr.DataType = []; % []
        hdr.NumberOfChannels = numel(nfo.clab); % Number of channels
        hdr.DataPoints = num2str(nfo.T); % Number of sample points as string
        hdr.SamplingInterval = []; % 10000
        hdr.BinaryFormat = ''; % 'INT_16'
        hdr.UseBigEndianOrder = ''; % 'NO'
        hdr.fs = nfo.fs; % Sampling frequency
        hdr.len = nfo.length; % Duration of data in seconds
        hdr.endian = ''; % 'l'
        hdr.clab = nfo.clab; % Channel labels
        hdr.clab_ref = cell(1,hdr.NumberOfChannels); % ?Reference? Empty {1x58 cell}
        hdr.scale = ones(1,hdr.NumberOfChannels); % ?? Scaling factor of amplifier? [1x58 double]
        hdr.unitOfClab = repmat({dat.yUnit},1,hdr.NumberOfChannels); % Per channel units {1x58 cell}
        hdr.unit = dat.yUnit; % Unit 'V'
        
    otherwise
        error('File format not supported!')
end

% Some Channels have wrong names, correct here
[startIndex,endIndex] = regexp(dat.clab,'Cp');
errors = find(cellfun(@(x)( ~isempty(x) ), startIndex));
if ~isempty(errors)
    for ii = errors
        dat.clab{ii}(startIndex{ii}:endIndex{ii}) = upper(dat.clab{ii}(startIndex{ii}:endIndex{ii}));
    end
    nfo.clab = dat.clab;
    hdr.clab = dat.clab;
end

errors = strcmp('FFT9h',dat.clab);
if any(errors)
    dat.clab{errors} = 'FTT9h';
    nfo.clab = dat.clab;
    hdr.clab = dat.clab;
end

errors = strcmp('EOG_H',dat.clab);
if any(errors)
    dat.clab{errors} = 'EOGh';
    nfo.clab = dat.clab;
    hdr.clab = dat.clab;
end

errors = strcmp('EOG_V',dat.clab);
if any(errors)
    dat.clab{errors} = 'EOGv';
    nfo.clab = dat.clab;
    hdr.clab = dat.clab;
end

% Update Montage
mnt = createWaveguardBBCImontage(dat.clab);

% Save all in one mat file
% save([file '.BBCI.mat'], 'hdr', 'mrk', 'mnt', 'nfo', 'dat', 'ch*', '-v7.3')
opt.folder = path{1};
file_saveMatlab([name{1} sprintf('_%i-%i',1,numel(filename)) 'BBCI.mat'], ...
    dat, mrk, mnt,opt,'AddChannels',false, 'SaveParam', {'-v7.3'});

%Finished
return