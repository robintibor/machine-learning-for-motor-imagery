function closeAllPlots( )
% Close all figures that are not the BCILAB or EEGLAB Menu
    figHandles = get(0,'Children');
    for handleNr = 1:size(figHandles)
        handle = figHandles(handleNr);
        firstPartOfFigureName = get(handle, 'Name');
        firstPartOfFigureName = firstPartOfFigureName(1:min(end,6));
        if (sum(strcmp(firstPartOfFigureName,{'BCILAB', 'EEGLAB'})) == 0)
            close(handle)
        end
    end
end

