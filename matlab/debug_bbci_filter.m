fs = 150;
db_attenuation = 60;
Wps = [6 4]/(fs/2);
[n, Wn]= cheb2ord(Wps(1), Wps(2) , 3, db_attenuation);
[z,p,k] = cheby2(n, db_attenuation, Wn, 'high'); 
fvtool(z,p,k)


db_attenuation = 40;
Wps = [2 0.5]/(150/2);
[n, Wn]= cheb2ord(Wps(1), Wps(2) , 5, db_attenuation);
[z,p,k] = cheby2(n, db_attenuation, Wn);

fvtool(z,p,k)