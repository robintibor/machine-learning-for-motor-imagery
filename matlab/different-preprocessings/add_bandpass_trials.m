function add_bandpass_trials(filename, EEG_DAT, MRK, rejected_channels, ...
    clean_trials, filt_opts, decoding_ival)

assert(isequal(decoding_ival, [0,4000])); %assuming always like that

filterbands = filt_opts.filterbands;
nfilterbands = size(filterbands,2);
samples_per_trial = (decoding_ival(2) - decoding_ival(1)) * EEG_DAT.fs / 1000.0;
num_chans = numel(EEG_DAT.clab) - numel(rejected_channels);
%% Generate filters...
[filter_a,filter_b] = generate_filter_coefficients(filterbands, ...
    filt_opts.filt_order, EEG_DAT.fs);

%% Loop over filterbands
% ------------------------
%% BEGIN FILTERBANDS LOOP
% ------------------------
epo_clean_bandpass_matrix =  ones(nfilterbands, samples_per_trial, ...
    num_chans, numel(clean_trials));
for filt_i = nfilterbands:-1:1
    fprintf('Filter bank from %d to %d (%d/%d)\n', ...
        filterbands(1, filt_i), filterbands(2,filt_i), ...
        nfilterbands - filt_i + 1, nfilterbands);
        %% Filter data
        EEG_fb = proc_filt(EEG_DAT,filter_b{filt_i},filter_a{filt_i});

        %% Segment and retain only clean trials and channels
        epo_clean = proc_segmentation(EEG_fb, MRK, decoding_ival);
        epo_clean = proc_selectChannels(epo_clean, 'not', rejected_channels);
        epo_clean = proc_selectEpochs(epo_clean, clean_trials);
        epo_clean_bandpass_matrix(filt_i,:,:,:) = epo_clean.x;
end

%% Convert to single precision
epo_clean_bandpass_matrix = single(epo_clean_bandpass_matrix);

channel_set_name = strcat('auto_clean_trials_', num2str(epo_clean.fs), ...
    'Hz_', num2str(filt_opts.min_freq), '_', ...
    num2str(filt_opts.max_freq), '_filterbank');
struct_to_save = struct();
struct_to_save = setfield(struct_to_save, channel_set_name, ...
    epo_clean_bandpass_matrix);
save(filename,'-struct', 'struct_to_save', '-mat', '-append');