%% Copy over BBCI files

%out_dir= 'I:\MotorEEGdatasets4deeplearning\BBCI-without-last-runs-datasets\';
%processed_files = get_files_without_last_two_runs();
out_dir= 'R:\tmp\Robin\bbci-last-two-runs';
processed_files = get_files_last_two_runs();
for file_cell = processed_files
    in_filename = file_cell{1};
    [pathstr, filename, ext] = fileparts(in_filename);
    out_filename = strcat(fullfile(out_dir, filename), ext);
    copy_if_not_exists(in_filename,  out_filename);
end
