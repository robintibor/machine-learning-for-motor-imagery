function create_clean_trials_file(EEG_DAT, MRK, rejected_channels, ...
    clean_trials, decoding_ival, output_file_name)

%% Segment and retain only clean trials and channels
epo_clean = proc_segmentation(EEG_DAT, MRK, decoding_ival);
epo_clean = proc_selectChannels(epo_clean, 'not', rejected_channels);
epo_clean = proc_selectEpochs(epo_clean, clean_trials);

%% Save as h5 matlab file!
save_clean_trials_to_h5_matlab(epo_clean, output_file_name);
end
