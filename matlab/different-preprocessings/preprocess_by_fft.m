function [power_spectra, complex_spectra] = preprocess_by_fft( ...
    trials, window_length, window_stride, SamplingRate)

    w = blackmanharris(window_length);
    w=w./norm(w);
    w = repmat(w,1,size(trials,2),size(trials,3));
    tcnt=1;
    ts=window_length:window_stride:size(trials,1);
    f=nan(window_length,numel(ts),size(trials,2),size(trials,3));

    for t=ts    
        q=w.*trials(t-window_length+1:t,:,:);
        [f(:,tcnt,:,:)]=fft(q);
        tcnt=tcnt+1;
    end
    if mod(size(f,1),2)==0
        f=f(1:end/2+1,:,:,:);
    else
        m=ceil(size(f,1)/2);
        f=f(1:m,:,:,:);
    end
    power_spectra=abs(f).^2/SamplingRate;
    complex_spectra=f;
end