function files = get_files_last_two_runs()
    data_folders = get_data_folders();
    files = {};
for folder = data_folders
    %% Determine the BBCI data file with all the runs except last two
    folder = folder{1};
    raw_data_folder = fullfile(folder, 'RawData', filesep);
    
        data_file = ls(fullfile(raw_data_folder, '*10_1-2BBCI.mat'));
    if (isempty(data_file))
        warning('could not find runs for %s\n', folder);
        continue;
    end
    % There should only be 1 correct datafile
    assert(size(data_file, 1) == 1);
    file_path = fullfile(raw_data_folder, data_file);
    files{end + 1} = file_path;
end
end

