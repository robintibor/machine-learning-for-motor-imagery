%% Remove processed files for pylearn2
%% (Warning: Actually really removes processed files :)))

processed_files = get_files_from_raw_data_folder('*autoclean_trials*.mat');

for i = 1:length(processed_files)
    processed_file = processed_files{i};
    fprintf('Removing %s\n', processed_file);
    delete(processed_file)
end