%% Preprocess with same cleaning like gold standard

%% Set Paths
dir_path = get_script_dir(mfilename('fullpath'));
addpath(dir_path);
addpath(fullfile(dir_path, '../gold-standard'));
addpath(fullfile(dir_path, '../gold-standard/BBCI/'));

%% Independent Variables
clear;
custom_start_bbci_toolbox;
get_independent_vars;
preproc_opts.fs_after_cleaning = 300;
% Take whole trial, can do feature selection also before training
% TODELAY: create configs for this?
segment_opts.decoding_ival = [0 4000];% ms
clean_opts.rejection_ival = segment_opts.decoding_ival;
preproc_opts.low_cut_off_Hz = 0.5;
% here other variant:
%filt_opts.max_freq = 73;
%filt_opts.low_width = 4;
%preproc_opts.fs_after_cleaning = 150;

%% Regenerate filterbank
filt_opts.filterbands = generate_filterbank(filt_opts);
%% What preprocessings to make
create_clean_trials = true;
add_band_pass = true;
add_fft = false;


%% Loop through all files without last two runs and create clean trials with fft transformations
debug_run = false;
if (debug_run == true)
    load_ival = [0 2 * 60 * 1000];
else
    load_ival = false;
end
files = get_files_without_last_two_runs();
% prevent that this preprocessing overwrites other preprocessing
%TODO: remove
files = files(1:14);
%files = {'R:\BCI2000\4ClassMotor3-4sPause\08-Oct-2014_MaGlMoSc2\RawData\MaGlMoSc2S001R01_ds10_1-12.BBCI.mat'};
%files = {'/media/robintibor/CE68160D6815F4C5/MotorEEGdatasets4deeplearning/BBCI-without-last-runs-datasets/LaKaMoSc1S001R01_ds10_1-9.BBCI.mat'};

for file_cell = files
    %% Get File and determine output file name
    file_path = file_cell{1};    
    fprintf('Now processing %s\n', file_path);
    output_file_name  = get_result_h5_filename(file_path, ...
        segment_opts.decoding_ival);
    if(debug_run == true)
        output_file_name = strcat(output_file_name, '.debug_test');
    end
    %% Load data and clean it (not necessary for fft since it
    % transforms existing raw trials)
    if (create_clean_trials || add_band_pass)      
        fprintf('Loading...\n');
        [EEG_DAT, ~, EOG_DAT, MRK, ~, ~] = load_data(file_path, ...
            preproc_opts, load_ival);
        fprintf('Cleaning...\n');
        [clean_trials, rejected_channels] = clean_bp_filtered_data(...
            EEG_DAT, EOG_DAT, MRK, clean_opts);
        [EEG_DAT, MRK] = preproc_after_cleaning(EEG_DAT, MRK, ...
            preproc_opts);
    end
    %% Do the actual creation of the file and bandpass and fft transformations
    if (create_clean_trials)
        fprintf('Creating clean trials...\n');
        create_clean_trials_file(EEG_DAT, MRK, rejected_channels, ...
            clean_trials, segment_opts.decoding_ival, output_file_name);
    end
    if (add_band_pass)
        fprintf('Adding filter bank...\n');
        add_bandpass_trials(output_file_name, EEG_DAT, MRK, rejected_channels, ...
            clean_trials, filt_opts, segment_opts.decoding_ival);
    end
    if (add_fft)
        fprintf('Adding FFTs...\n');
        add_fft_transformations(output_file_name, ...
            preproc_opts.fs_after_cleaning);
    end
end
%% Clear workspace to free memory! 
clear