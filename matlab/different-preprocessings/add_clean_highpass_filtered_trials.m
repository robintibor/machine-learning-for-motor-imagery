function  add_clean_highpass_filtered_trials(EEG_DAT, MRK, ...
            rejected_channels, clean_trials, decoding_ival, ...
            low_cut_off_Hz, filt_order, output_file_name)
EEG_filtered = proc_filt(EEG_DAT, a, b);

%% Segment and retain only clean trials and channels
epo_clean = proc_segmentation(EEG_filtered, MRK, decoding_ival);
epo_clean = proc_selectChannels(epo_clean, 'not', rejected_channels);
epo_clean = proc_selectEpochs(epo_clean, clean_trials);


%% Redo with filtfilt
EEG_filtered = proc_filtfilt(EEG_DAT, a, b);
epo_clean_filtfilt = proc_segmentation(EEG_filtered, MRK, decoding_ival);
epo_clean_filtfilt = proc_selectChannels(epo_clean_filtfilt, 'not', rejected_channels);
epo_clean_filtfilt = proc_selectEpochs(epo_clean_filtfilt, clean_trials);


%% Create struct and save
struct_to_save = struct();

struct_to_save = setfield(struct_to_save, strcat('auto_clean_trials_', ...
    num2str(epo_clean.fs),'Hz_highpass_', num2str(low_cut_off_Hz), 'Hz'),...
    epo_clean.x);

struct_to_save = setfield(struct_to_save, strcat('auto_clean_trials_', ...
    num2str(epo_clean_filtfilt.fs),'Hz_highpass_filtfilt_', ...
    num2str(low_cut_off_Hz), 'Hz'),...
    epo_clean_filtfilt.x);

%% Save to output file
save(output_file_name,'-struct', 'struct_to_save', '-mat', '-append');

end