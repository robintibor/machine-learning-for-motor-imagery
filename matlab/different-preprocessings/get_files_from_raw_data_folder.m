function files = get_files_from_raw_data_folder(file_pattern)
    data_folders = get_data_folders();
    files = {};
for folder = data_folders
    %% Determine the BBCI data file with all the runs except last two
    %(just ot know if there _should_ be a processed file)
    folder = folder{1};
    raw_data_folder = fullfile(folder, 'RawData', filesep);
    data_file = ls(fullfile(raw_data_folder, '*10_1-*.BBCI.mat'));
    processed_files = dir(fullfile(raw_data_folder, file_pattern));
    names = {processed_files.name};
    if (numel(names) > 0)
        if (isempty(data_file))
             warning('no data file, but a processed file?: %s\n', ...
                 raw_data_folder);
        end
        full_names = strcat(raw_data_folder, names);
        files = [files full_names];       
    elseif isempty(data_file) % no data file, no processed file => just continue
        continue
    else % data file but no processed file!
        warning('no processed file, even though data file exists: %s\n', ...
            raw_data_folder);
    end
end
end