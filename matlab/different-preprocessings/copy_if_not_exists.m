function copy_if_not_exists( filename, out_filename )
if (exist(out_filename, 'file') == 0)
    fprintf('Copying %s\n', filename);
    copyfile(filename, out_filename);
else
    warning('File %s already exists or is not writable', out_filename);
end
end

