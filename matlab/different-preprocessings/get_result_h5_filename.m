function output_file_path = get_result_h5_filename(file_path, ...
    decoding_ival)
assert(~isempty(strfind(file_path, '.BBCI')));
[path, input_file_name, ~] = fileparts(file_path);
filepart_and_dot_bbci = strsplit(input_file_name, '.');
input_file_name = filepart_and_dot_bbci{1};
output_file_name = strcat(input_file_name, '_autoclean_trials_ival_',...
    num2str(decoding_ival(1)), '_', num2str(decoding_ival(2)), ...
    '.mat');
output_file_path = fullfile(path, output_file_name);
end