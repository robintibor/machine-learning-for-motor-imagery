
%% Set Paths
dir_path = get_script_dir(mfilename('fullpath'));
addpath(dir_path);
addpath(fullfile(dir_path, '../BBCI_import'));
addpath(fullfile(dir_path, '../gold-standard'));
addpath(fullfile(dir_path, '../gold-standard/BBCI/'));
addpath('C:\MATLAB\matlab_scripts_agball');
addpath('C:\Program Files (x86)\BCI2000\tools\mex');

%% Independent Variables
clear;
custom_start_bbci_toolbox;
debug_run = false;

basefolder = 'R:\BCI2000\4ClassMotor3-4sPause\';
user_folders = dir(fullfile(basefolder, '*Sc*'));
user_folder_names = {user_folders.name};
% remove lukas data ?
%user_folder_names(ismember(user_folder_names, '20-Nov-2014_LuFiMoSc3')) = [];
ignored_folders = struct();
for user_folder = user_folder_names
    user_folder = user_folder{1};
    fprintf('now processing %s\n', user_folder);
    % transpose folder to have horizontal string...
    folder = fullfile(basefolder, user_folder, 'RawData\');
    files = dir(fullfile(folder, '*_ds10.dat'));

    %{ 
    enable this in case you only want datasets with >= 11 runs
    if(numel(files) < 11)
        warning('ignoring folder %s, only %d runs \n', user_folder, numel(files));
        ignored_folders(end + 1).name = user_folder;
        ignored_folders(end + 1).runs = numel(files);
        continue;
    end
    %}
    %% Remove last 2 runs
    files = files(1:end-2);
    % for debug, just load first 2 runs
    if (debug_run == true)
        files = files(1:2);
    end
    % enable this in case you only chose datasets with >= 11 runs
    % there should be atleast 9 and maximum 14 files (14 would imply 3
    % half-cancelled runs)
    %assert(debug_run  || (numel(files) >= 9 && numel(files) <= 14));
    absolute_file_names = strcat(folder,  {files.name});
    BBCI_import(absolute_file_names); 
    % if you want to overwrite old files:
    %BBCI_import(absolute_file_names, [], true);
end

