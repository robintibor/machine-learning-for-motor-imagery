%% Copy over processed files
out_dir= 'I:\MotorEEGdatasets4deeplearning\with-bandpass';
processed_files = get_files_from_raw_data_folder('*autoclean_trials*.mat');

for file_cell = processed_files
    file = file_cell{1};
    [pathstr, filename, ext] = fileparts(file);
    out_filename = strcat(fullfile(out_dir, filename), ext);
    copy_if_not_exists(file,  out_filename);    
end