function user_folder_paths = get_data_folders()
%GET_RAWDATA_FOLDERS Summary of this function goes here
%   Detailed explanation goes here

basefolder = 'R:\BCI2000\4ClassMotor3-4sPause\';
user_folders = dir(fullfile(basefolder, '*Sc*'));
user_folder_names = {user_folders.name};
user_folder_paths = strcat(basefolder, user_folder_names);
end

