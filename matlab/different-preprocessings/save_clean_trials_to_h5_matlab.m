function save_clean_trials_to_h5_matlab(epo_clean, output_file_name)
%% Convert trials to single precision
epo_clean.x = single(epo_clean.x);

%% Create struct with clean trials, channel names and targets
struct_to_save = struct();
struct_to_save = setfield(struct_to_save, strcat('auto_clean_trials_', ...
    num2str(epo_clean.fs),'Hz'), epo_clean.x);
struct_to_save = setfield(struct_to_save, 'channel_names', epo_clean.clab);
[~, targets] = max(epo_clean.y);
struct_to_save = setfield(struct_to_save, 'targets', targets);

%% Save/append it as v7.3=h5
if ~exist(output_file_name, 'file')
    save(output_file_name,'-struct', 'struct_to_save', '-v7.3', '-mat');
else % Append is in case you rerun everything with different resampling...
    save(output_file_name,'-struct', 'struct_to_save', '-mat', '-append');
end
end