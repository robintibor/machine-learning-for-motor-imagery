function add_fft_transformations(filename, SamplingRate)
% Preprocess with fft using different window lengths and strides

%% Here we define all the variables needed for preprocessing/file reading
batch_size = 10;
window_length_sec = 0.5; % in sec
window_stride_sec = 0.25; % in sec

%% Determine some values from the file, like number of trials,
% Sampling Rate, number of channels etc.

h5file = h5info(filename);
trial_set_name = strcat('auto_clean_trials_', num2str(SamplingRate), 'Hz');
trial_set_ind = find(strcmp({h5file.Datasets.Name}, trial_set_name));
data_size = h5file.Datasets(trial_set_ind).Dataspace.Size;
%SamplingRate = h5read(filename, '/params_500Hz/SamplingRate/NumericValue'); 
%SamplingRate = h5read(filename, '/params_100Hz/SamplingRate/NumericValue'); 
num_trials = data_size(3);
num_channels = data_size(2);
num_samples = data_size(1);
start_indices = 1:batch_size:num_trials;
window_length = SamplingRate * window_length_sec; % 500 ms 750; % 1500ms
window_stride =  SamplingRate * window_stride_sec; %250 ms 375;% 750ms
num_windows = ((num_samples - window_length) / window_stride) + 1;
num_freqs = window_length / 2  + 1;

%% Now lets also define the names for the new datasets and create the
% datasets if they don't already exist 
dataset_base_name = strcat('/', trial_set_name, '_',...
    num2str(window_length * 2), 'ms_len_',...
    num2str(window_stride * 2), 'ms_stride' );
complex_dataset = strcat(dataset_base_name, '_complex_spectrum');
ps_dataset = strcat(dataset_base_name, '_ps');
if (~ismember(ps_dataset, strcat('/', {h5file.Datasets.Name})))
    h5create(filename, ps_dataset, ...
        [num_freqs num_windows num_channels num_trials]);
else 
    disp('Dataset exists, rerunning preprocessing for power spectra!');
end
if (~ismember(complex_dataset, strcat('/', {h5file.Datasets.Name})))
    h5create(filename, complex_dataset, ...
        [num_freqs num_windows num_channels num_trials]);
else 
    disp('Dataset exists, rerunning preprocessing for complex spectra!');
end

%% Read batches of trials, preprocess them (by fft with given parameters 
% from above), and immediately save them (otherwise we might run out of
% memory)
for start_trial = start_indices
    tic;
    start_trial
    end_trial = min(start_trial + batch_size -1, num_trials);
    trials_to_read = end_trial - start_trial  + 1;
    trials_to_read
    
    trials = h5read(filename, strcat('/', trial_set_name),...
        [1 1 start_trial], ...
        [num_samples num_channels trials_to_read]);
    [power_spectra, complex_spectra] = ...
        preprocess_by_fft(trials, window_length, window_stride, SamplingRate);
    h5write(filename, complex_dataset, complex_spectra, ...
        [1 1 1 start_trial], ...
        [num_freqs num_windows num_channels trials_to_read]);
    h5write(filename, ps_dataset, power_spectra, ...
        [1 1 1 start_trial], ...
        [num_freqs num_windows num_channels trials_to_read]);
    toc
end


end