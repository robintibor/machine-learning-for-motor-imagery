% TODO: make test out of this
filt_opts.min_freq = 0;
filt_opts.max_freq = 144;
%filt_opts.max_freq = 20;
filt_opts.last_low_freq = 30;
%filt_opts.freq_last_low_freq = 8;
filt_opts.low_width = 2;
filt_opts.high_width = 6;

filterbank = generate_filterbank(filt_opts);

%% expected
%filterbank = [0.5 1; 1 3; 3 5; 5 7; 7 9; 9 15; 15 21;] ;

% 0 2 4 6 8 10 12 14 16 18 20
% 0 2 5 6 8    12       18
% 0-1, 1-3, 3-5, 5-7, 7-9, 9-15, 15-21, ...