%load('results_from_net.m', '-mat')
[value, cross_val_classes] = max(crossvaloutputs);
[value, target_classes] = max(targets_for_neural_net);

block_sizes = [5 10 20 30 40 50 75 100 150 200];
all_accuracies = zeros(numel(target_classes), numel(block_sizes));
for i = 1:numel(block_sizes)
    block_size = block_sizes(i);
    block_size % for progress
    step_size= 1;
    start_indices = [1:step_size:numel(target_classes)];
    accuracies = zeros(numel(start_indices), 1);
    for j = 1:numel(start_indices)
        start_index = start_indices(j);
        end_index = min(start_index+block_size, numel(target_classes));
        target_part = target_classes(start_index:end_index);
        cross_val_part = cross_val_classes(start_index:end_index);
        accuracy = sum(target_part == cross_val_part) / numel(target_part);
        accuracies(j) = accuracy;
        all_accuracies(start_index:start_index+step_size, i) = accuracy;
    end
end
accuracies_avg = mean(all_accuracies, 2);
figure('name', 'Accuracies average');
plot(accuracies_avg);
