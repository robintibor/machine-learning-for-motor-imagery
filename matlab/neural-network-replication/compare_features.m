% look at features and see if they are equal

inputoriginal = h5read('../../data/motor-imagery-tonio/MaVoMoSc1S001/MaVoMoSc1S001R01-S001R13Sorted500Hz.mat', '/good_trials_500Hz_ps');
neuralnet = load('../../data/motor-imagery-tonio/neural-networks/FFT5-15/2014-08-20 MaVoMoSc1.mat');

%channelNames = h5read('../../data/motor-imagery-tonio/MaVoMoSc1S001/MaVoMoSc1S001R01-S001R13Sorted500Hz.mat', '/params_500Hz/ChannelNames/Value';)
%cellfun(@char,channelNames(53:55), 'UniformOutput', false) => CP3 / CP4 sind channels 53 und 55
inputoriginalpart = inputoriginal(5:15, :, [53 55], :);
inputneuralnet = neuralnet.inputs{1};
logicalmatrix =  (abs(inputoriginalpart - 0.9504) < 0.0001) | (abs(inputoriginalpart - 0.4013) < 0.0001);


intersectL = linspace(1, 937, 937);
for feature = inputneuralnet(1:10,2)'
    feature
    [~, ~, ~, l] = ind2sub(size(inputoriginalpart), find(abs(inputoriginalpart - feature) < 0.000001));
    intersectL = intersect(intersectL, l)
end

% no intersections :(