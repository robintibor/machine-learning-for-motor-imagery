inputs = h5read('../../data/motor-imagery-tonio/MaVoMoSc1S001/MaVoMoSc1S001R01-S001R13Sorted500Hz.mat', '/good_trials_500Hz_ps');
inputs = inputs(9:31, :, [53 55], :);
results_file_name = 'replication_results.m'
crossinds = crossvalind('Kfold', size(inputs, 4), 10);
inputs_for_neural_net = ...
    zeros(size(inputs, 3)*size(inputs, 2)*size(inputs, 1), size(inputs, 4));
targets = h5read('../../data/motor-imagery-tonio/MaVoMoSc1S001/MaVoMoSc1S001R01-S001R13Sorted500Hz.mat', '/targets');
net = load('../../data/motor-imagery-tonio/neural-networks/FFT9-31/2014-08-20 MaVoMoSc1.mat');
net = net.net{1};
% revert nets weights...
net = revert(net);
%net.layers{1}.transferFcn = 'poslin'
%net.layers{2}.transferFcn = 'softmax'
net.trainParam.showCommandLine = true
net.trainParam.show = 1
net.trainParam.showWindow = false

%{
for comparison/replication (create net instead of just reiniting)
    .. havent tested this yet...
net = patternnet(20);
% copies from nets that were sent
net.trainFcn = 'trainbr';
net.divideFcn = 'dividetrain';
net.performFcn = 'mse';
net.trainParam.epochs = 20;
net.trainParam.max_fail = 6;
%this is transfer function from them, but did they really set this by hand? :) 
net.layers{2}.transferFcn = 'tansig'
% poslin for rectified linear units(!)

%}

for epoch_nr = 1:size(inputs, 4)
    % inputs for neural net are #features x #examples
    inputs_for_neural_net(:, epoch_nr) = reshape(inputs(:, :, :, epoch_nr), [], 1);
end

% convert to one hot encoding
targets_for_neural_net = zeros(4, size(inputs_for_neural_net, 2));
for class_nr = 1:4
    targets_for_neural_net(class_nr, :) = (targets == class_nr);
end

% for debugging tests, take less data
%{
inputs_for_neural_net = inputs_for_neural_net(1:10, 1:100);
targets_for_neural_net = targets_for_neural_net(:, 1:100);
crossinds = crossvalind('Kfold', size(inputs_for_neural_net, 2), 4);
net = patternnet(5);
net.trainParam.showCommandLine = true
net.trainParam.show = 1
net.trainParam.showWindow = false
%}
% check whether values still exist
for tests = 1:100
    random_epoch = randi(size(inputs_for_neural_net, 2), 1);
    random_feature = randi(size(inputs_for_neural_net, 1), 1);
    result = find(inputs(:, :, :, random_epoch) == ...
        inputs_for_neural_net(random_feature, random_epoch));
    assert(numel(result) == 1);    
end

crossvaloutputs = ones(4, size(targets_for_neural_net, 2)) * -1;
for fold_nr = 1:10
    fold_nr
    inputs_for_fold = inputs_for_neural_net(:, find(crossinds ~= fold_nr));
    targets_for_fold = targets_for_neural_net(:, find(crossinds ~= fold_nr));
    [trained_net, performance] = train(net, inputs_for_fold, targets_for_fold);
    test_inputs = inputs_for_neural_net(:, find(crossinds == fold_nr));
    test_targets = targets_for_neural_net(:, find(crossinds == fold_nr));
    outputs_fold = trained_net(test_inputs);
    crossvaloutputs(:, find(crossinds == fold_nr)) = outputs_fold;
    %plotconfusion(test_targets, outputs_fold);
end
%plotconfusion(targets_for_neural_net, crossvaloutputs)

save(results_file_name, 'targets_for_neural_net', 'crossvaloutputs')
