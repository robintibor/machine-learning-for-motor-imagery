processed_files = get_files_from_raw_data_folder('*autoclean_trials_ival_0_4000.mat');
only_extra_parts = false;
for processed_file = processed_files;
    processed_file = processed_file{1};
    fprintf('Copying %s\n', processed_file);
    if (~only_extra_parts)
        copyfile(processed_file, 'R:\BCI2000\4ClassMotor3-4sPause\tmp_for_upload_delete_this\')
    else
        % for only copying extra parts
        processed_struct = load(processed_file, '-mat');
        struct_to_save = struct;
        struct_to_save = setfield(struct_to_save, ...
            'auto_clean_trials_150Hz_highpass_2Hz', ...
            processed_struct.auto_clean_trials_150Hz_highpass_2Hz);
        struct_to_save = setfield(struct_to_save, ...
            'auto_clean_trials_150Hz_highpass_filtfilt_2Hz', ...
            processed_struct.auto_clean_trials_150Hz_highpass_filtfilt_2Hz);
        [dir, file_base] = fileparts(processed_file);
        output_file_name = fullfile('R:\BCI2000\4ClassMotor3-4sPause\tmp_for_upload_delete_this\', ...
            strcat(file_base, '_extra_parts', '.mat'));
        save(output_file_name,'-struct', 'struct_to_save', '-v7.3', '-mat');
    end
end
