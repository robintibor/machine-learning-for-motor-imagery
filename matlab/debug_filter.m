testa = load('R:BCI2000\4ClassMotor3-4sPause\04-Dec-2014_NaMaMoSc1\RawData\NaMaMoSc1S001R01_ds10_1-11_autoclean_trials_ival_0_4000.mat', '-mat')
figure, plot(squeeze(mean(testa.auto_clean_trials_150Hz, 1))')
figure, plot(squeeze(mean(testa.auto_clean_trials_150Hz_highpass_2Hz, 1))')
mean_trials_filtered = squeeze(mean(testa.auto_clean_trials_150Hz_highpass_2Hz, 1));
% problem:
figure, plot(testa.auto_clean_trials_150Hz_highpass_2Hz(:,:,514))
% another problem
figure, plot(testa.auto_clean_trials_150Hz_highpass_2Hz(:,:,519))

files_imported = get_files_without_last_two_runs();
magl_file = files_imported{4};

[EEG_DAT, ~, EOG_DAT, MRK, ~, ~] = load_data(magl_file, preproc_opts, false);

[clean_trials, rejected_channels] = clean_bp_filtered_data(...
    EEG_DAT, EOG_DAT, MRK, clean_opts);


[EEG_DAT, MRK] = resample_data(EEG_DAT, MRK, ...
    preproc_opts.fs_after_cleaning);


[a,b] = butter(3, 0.5/(150/2),'high');
fvtool(a,b)

EEG_filtered = proc_filt(EEG_DAT, a,b);

tic
db_attenuation = 40;
Wps = [2 0.5]/(150/2);
[n, Wn]= cheb2ord(Wps(1), Wps(2) , 3, db_attenuation);
[z,p,k] = cheby2(n, db_attenuation, Wn, 'high');
EEG_filtered = proc_filt(EEG_DAT, z,p,k);
toc

decoding_ival = [0 4000];


epo_clean_filt = proc_segmentation(EEG_filtered, MRK, decoding_ival);
epo_clean_filt = proc_selectChannels(epo_clean_filt, 'not', rejected_channels);
epo_clean_filt = proc_selectEpochs(epo_clean_filt, clean_trials);
mean_of_trials_filt = squeeze(mean(epo_clean_filt.x, 1));


%% Segment and retain only clean trials and channels
epo_clean = proc_segmentation(EEG_DAT, MRK, decoding_ival);
epo_clean = proc_selectChannels(epo_clean, 'not', rejected_channels);
epo_clean = proc_selectEpochs(epo_clean, clean_trials);

mean_of_trials = squeeze(mean(epo_clean.x, 1));

%{
[sos_var,g] = zp2sos(z,p,k);
Hd  = dfilt.df2sos(sos_var, g);
fvtool(Hd)
freqz(Hd)
%}