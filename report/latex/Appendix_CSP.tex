



    
        
    \chapter{Common Spatial Patterns}\label{common-spatial-patterns}
    
    \section{Mathematical Derivation}\label{mathematical-derivation}
    
    \begin{keypointbox}
\item  Common spatial patterns seeks to optimize ratio of variances 
\item  Generalized eigenvalue decomposition can be used to compute spatial filters 
\end{keypointbox}
    
    We explain one way to compute spatial filters with the common spatial
patterns algorithm, and why this is mathematically sound, mostly
following \cite{blankertz_optimizing_2008}.

We assume we have multivariate signals \(X_1\) and \(X_2\) for two
classes 1 and 2. For EEG decoding, the multivariate signals would be
signals from several sensors and the classes could be left hand and
right hand. We want to find a spatial filter \(w\) that maximizes the
ratio of the variances:

\[w = argmax_w \frac{variance(w^{\top} X_1)}{variance(w^{\top} X_2)} = argmax_w \frac{||w^{\top} X_1||^2}{||w^{\top} X_2||^2} = 
argmax_w \frac{w^{\top} X_1 X_1^{\top}w}{w^{\top} X_2 X_2^{\top}w}\] (we
assume \(X_1\) and \(X_2\) are mean-subtracted)

We compute covariance matrices \(\Sigma_{1}\) and \(\Sigma_{2}\) for
classes 1 and 2 by averaging over the covariances for all trials for
each class and rewrite our formula as:
\[w = argmax_w \frac{w^{\top} \Sigma_{1} w}{w^{\top} \Sigma_{2}w}\]
    
    We put a further restriction on the possible \(w\)s by setting
\(w^{\top} \Sigma_2w = 1\). This should not affect the result \(w\) as
this term is only the denominator of our ratio.

Now we can solve for w using this generalized eigenvalue problem:

\[\Sigma_1w = \lambda \Sigma_2 w \]

This will not give us a single spatial filter w, but a matrix of spatial
filters W, the generalized eigenvectors. However, as we will see, this
matrix also contains the desired spatial filter w that maximizes our
variance ratio. The variance ratios of class 1 to class 2 for the
signals filtered with the (generalized) eigenvector \(w_i\) will be
equal to the corresponding eigenvalues of the eigenvectors:

\[\frac{w_i^{\top} \Sigma_1 w_i}{w_i^{\top} \Sigma_2 w_i} = \lambda_i\]

The vectors with the highest eigenvalues have a high variance for class
1 compared to class 2 and the vectors with the lowest eigenvalues have a
low variance for class 1 compared to class 2. The spatial filter
maximizing this ratio is then the \(w_i\) with the largest eigenvalue.
Conversely, the spatial filter minimizing this ratio is the \(w_i\) with
the smallest eigenvalue.

Furthermore, all the eigenvectors applied to our data lead to
uncorrelated signals, i.e.:

\[W^{\top}\Sigma_1 W=\Lambda_1 \\
W^{\top}\Sigma_2 W=\Lambda_2\]

and \[(\Lambda_1 \text{ , } \Lambda_2 \text{ diagonal })\]

\begin{comment}
TODECIDE
 put proof somewhere or at least link to proof when you say the spatial filter maximizing this ratio is then...., using http://www.sciencedirect.com/science/article/pii/001346949190163X http://csl.anthropomatik.kit.edu/downloads/vorlesungsinhalte/MBS12_CommonSpatialPatterns.pdf http://cdn.preterhuman.net/texts/science_and_technology/artificial_intelligence/Pattern_recognition/Introduction%20to%20Statistical%20Pattern%20Recognition%202nd%20Ed%20-%20%20Keinosuke%20Fukunaga.pdf
\end{comment}
    
    \section{Computation Example}\label{computation-example}
    
    We illustrate the computation with an artificial example. We use one
source signal, a sine signal \(x\) plus some gaussian noise. It is
projected to two sensors, differently for each class. For class 1, the
signal is: \(Sensor_1 = 0.6 x\), \(Sensor_2 =0.8x\). For class 2,
\(Sensor_1 = 0.8 x\), \(Sensor_2 =0.6x\). We show the mean-subtracted
signals. You can see that the sensor 2 has a slightly larger variance
than sensor 1 for class 1 and vice versa for class 2.
    
    %\begin{figure}[ht]
        \begin{center}
        \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Appendix_CSP_files/Appendix_CSP_11_0.pdf}
        \end{center}
        %\end{figure}
        %{ \hspace*{\fill} \\}
    
    %\begin{figure}[ht]
        \begin{center}
        \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Appendix_CSP_files/Appendix_CSP_11_1.pdf}
        \end{center}
        %\end{figure}
        %{ \hspace*{\fill} \\}
    
    Now we visualize how CSP spatially filters the sensor signals to make
them more separable by their variance. In the first plot, you see a
scatterplot of all signal values for class 1 (red) and class 2 (violet).
Each dot represents one sample of the signal, and the axes show the
value of sensor 1 and the value of sensor 2 for that sample. You again
see that sensor 2 has a larger variance than sensor 1 for class 1, since
the red dots are more spread in y-direction than in x-direction. The
spatial filters computed by CSP are shown as two lines in red and
violet. The red spatial filter maximizes the variance for class 1 and
the violet one for class 2. Note, how the spatial filters are almost
orthogonal to the direction where the \emph{opposite} class has the
maximum variance. For example, the red spatial filter for class 1 is
almost orthogonal to the direction of maximum variance for the violet
dots of class 2. In the second plot, we show the same scatterplots for
the signal values after they are filtered with the CSP-filters. Note
that the two axes for the two CSP filters correspond to the directions
of the largest variance for the corresponding class.
    
    %\begin{figure}[ht]
        \begin{center}
        \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Appendix_CSP_files/Appendix_CSP_13_0.pdf}
        \end{center}
        %\end{figure}
        %{ \hspace*{\fill} \\}
    
    %\begin{figure}[ht]
        \begin{center}
        \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Appendix_CSP_files/Appendix_CSP_13_1.pdf}
        \end{center}
        %\end{figure}
        %{ \hspace*{\fill} \\}
    
    You again see the effect of the CSP filters in the plots of the
CSP-filtered signals. For class 1, CSP1 has a large variance, whereas
for class 2, CSP2 has a large variance.
    
    
    %\begin{figure}[ht]
        \begin{center}
        \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Appendix_CSP_files/Appendix_CSP_15_1.pdf}
        \end{center}
        %\end{figure}
        %{ \hspace*{\fill} \\}
    
    %\begin{figure}[ht]
        \begin{center}
        \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Appendix_CSP_files/Appendix_CSP_15_2.pdf}
        \end{center}
        %\end{figure}
        %{ \hspace*{\fill} \\}
    
    