



    
        
    \section{Filter Bank Net
Visualizations}\label{sec:filter-bank-net-visualizations}
    
    \begin{keypointbox}

\item Filter bank net shows sensible spatial filters
\item Transformation to spatial patterns looks plausible
\item Filter bank net uses certain frequency bands more and others less

\end{keypointbox}
    
    For the filter bank net, we can get a good idea of what the network is
learning by analyzing the weights of the net. Therefore, we will show
the weights itself and also try to transform the weights to a more
interpretable form using the covariances of the sensors.
    
    \subsection{Spatial Filters and Softmax
Weights}\label{spatial-filters-and-softmax-weights}
    
    First, we visualize the spatial filters together with their softmax
weights for all 4 classes. The softmax weights for a filter show how
that filter is weighted for different frequencies. We show the 20
spatial filters with the highest sum of absolute softmax weights in
order to show those filters most influential on the classification
result. As always with weights from our nets, note that these not
straightforward to interpret. The weights might also reflect
class-independent information such as the covariance of the noise.


Nevertheless, we do see plausible structures in our weights shown in 
Figure \ref{fig:fb-net-spatial-filters-softmax-weights}. 
We see some lateralized filters such as filter 3 and 4. They also show
plausible weights for the frequencies. Whereas the left hand is weighted
negatively in the lower frequencies until 30 Hz, the left hand is
weighted positively in the higher frequencies between 60 and 80 Hz. We
also see centralized filters such as filters 1 and 2, that show a
similar behavior for the rest and feet classes. We can also observe some
difficult to interpret weights, such as the very low frequency weights
for filter 1.
Note that a filter with flipped colors will lead to the same result in
our network due to the squaring after the filtering. See for example
filter 8 and 11.
    
\begin{figure}
        \begin{center}
        \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{FB_Net_Visualizations_files/FB_Net_Visualizations_14_0.pdf}
        \end{center}
        \caption{Spatial filters and softmax weights of the filter bank net. 
        Values go from blue (negative) over grey (zero) to red (positive).
        Black circles highlight positive or negative peaks mentioned in the text.}
	\label{fig:fb-net-spatial-filters-softmax-weights}
\end{figure}
    
    \subsection{Spatial Patterns and Softmax
Patterns}\label{spatial-patterns-and-softmax-patterns}
    
    We now want to transform the weights so that they show how strongly a
    presumed source signal is projected to the sensors and do not reflect the noise
structure as much anymore. However, we cannot use our regular
transformation to a pattern for the first layer, since the weights are
convolved over signals from several filterbands. We therefore resort to
only multiplying with the covariances of the sensors. The intuition here
is that a source probably projects similarly to neighbouring sensors with a high
covariance and the classifier might give positive weights to the
sensor(s) where the source is strongest and negative weights to the
others to remove the common noise. Multiplying with the covariances will
then more accurately reflect how the source is projected to the sensors,
probably transforming the negative weights into small positive weights.
We will call these transformed spatial filters spatial patterns.

However, it is not clear how to compute the covariance for the sensors
as we have several filterbands. The different filterbands might have
very different sensor covariances. Therefore, we first plot the sensor
covariances per filterband to check if we can safely use the mean sensor
covariance across all filterbands, see Figure \ref{fig:fb-sensor-covariances}. 
The diagonal lines show that sensors on the same side of the head have high covariances.
    Overall, we can see that the covariances are somewhat similar between
all filterbands, and therefore we will use the mean covariance across
all filterbands shown in Figure \ref{fig:fb-mean-sensor-covariances}.
The softmax weights can be transformed into softmax patterns as
described in the section \ref{sec:using-the-covariance-to-compute-activation-patterns}.
    
\begin{figure}
\begin{subfigure}[t]{0.75\linewidth}
  \begin{center}
  \adjustimage{max size={0.9\linewidth}{0.35\paperheight}}{FB_Net_Visualizations_files/FB_Net_Visualizations_18_0.pdf}
  \end{center}
  \caption{
  Sensor covariances for individual filterbands. 
Number above each plot is the center of the respective filterband
(exception: 0 Hz goes from 0.5 Hz to 1 Hz).
}
\label{fig:fb-sensor-covariances}
\end{subfigure}
\begin{subfigure}[t]{0.2\linewidth}
  \begin{center}
  \adjustimage{max size={0.9\linewidth}{0.1\paperheight}}{FB_Net_Visualizations_files/FB_Net_Visualizations_20_0.pdf}
  \end{center}
  \caption{Mean across all filterbands.}
\label{fig:fb-mean-sensor-covariances}
\end{subfigure}
\caption{Sensor covariances for filterbands.
  The rows within one plot are sorted from top to
bottom like this (same for the columns from left to right): 
first (front) sensor row, from left to right, second sensor row, from left to right, \ldots
For example, the red top left and bottom right blocks for 2 Hz in the left figure show that
the sensors in the front and the sensors in the back have high
covariances among each other.}
\end{figure}
    


    
    
    
    Putting it all together we get the visualization for the spatial
pattens and the softmax patterns shown in Figure \ref{fig:fb-net-spatial-patterns-softmax-patterns}.
This results in spatially much
smoother patterns. We see clearly centralized patterns for example for
pattern 1 and 2. We also see lateralized patterns, for example pattern 4
and 5. The softmax patterns for rest and feet as well as for right hand
and left hand have fairly high correlations for most spatial filters.
This indicates right hand vs left hand and rest vs feet are more difficult to
distinguish than right hand or left hand vs rest or feet.
    
\begin{figure}[ht]
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{FB_Net_Visualizations_files/FB_Net_Visualizations_27_0.pdf}
    \end{center}
    \caption{Spatial patterns and softmax patterns of the filter bank net. 
        Values go from blue (negative) over grey (zero) to red (positive).}
	\label{fig:fb-net-spatial-patterns-softmax-patterns}
\end{figure}
    
    \subsection{Used Filterbands}\label{used-filterbands}
    
    Now, we want to determine which filterbands our network uses the most
for different subjects. An easy way to visualize this is to show the sum
of the absolute weights for each filterband. We do this for two subjects
in Figure \ref{fig:filterband-weights-two-persons}. 
You can see that for one (VP2), the net uses the higher frequencies around 76 Hz
and for the other one not.

    
\begin{figure}
\begin{subfigure}{0.9\linewidth}
        \begin{center}
        \adjustimage{max size={1\linewidth}{0.9\paperheight}}{FB_Net_Visualizations_files/FB_Net_Visualizations_32_0.pdf}
        \end{center}
\end{subfigure}

\begin{subfigure}{0.9\linewidth}
        \begin{center}
        \adjustimage{max size={1\linewidth}{0.9\paperheight}}{FB_Net_Visualizations_files/FB_Net_Visualizations_32_1.pdf}
        \end{center}
\end{subfigure}
\caption{Sum of absolute softmax weights for different filterbands for two subjects.
Values on x-axis are centers of the filterband (exception: 0 Hz goes from 0.5 Hz to 1 Hz).}
\label{fig:filterband-weights-two-persons}
\end{figure}
    
    We also show the same plots for the mean and standard deviation across
our 18 datasets in Figure \ref{fig:filterband-weights-all-persons}. 
As you can see, across all datasets
there is a clear peak for 70 and 76 Hz. Also, for these frequencies, the
standard deviation is quite high, which is consistent with our
observation that for some subjects, the network uses these frequencies
quite strongly and for others, not at all.
    
\begin{figure}
  \begin{center}
  \adjustimage{max size={0.9\linewidth}{0.2\paperheight}}{FB_Net_Visualizations_files/FB_Net_Visualizations_35_0.pdf}
  \end{center}
\caption{Sum of absolute softmax weights for different filterbands for all subjects.
Blue dots are means across subjects and black bars represent one standard deviation.
Values on x-axis are centers of the filterbands (exception: 0 Hz goes from 0.5 Hz to 1 Hz).}
\label{fig:filterband-weights-all-persons}
\end{figure}

\clearpage