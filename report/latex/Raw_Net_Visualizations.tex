



    
        
    \section{Raw Net Visualizations}\label{sec:raw-net-visualizations}
    
    \begin{keypointbox}

\item Raw net's visualizations indicate raw net uses oscillatory signals
\item Visualizations show expected lateralization
\item Raw net is able to use single potentials

\end{keypointbox}

    We will show that the raw nets can learn to use:

\begin{itemize}
\tightlist
\item
  oscillations with different frequencies at specific locations
\item
  a signal potential at the beginning of the trial
\end{itemize}

For that, we will show the weights and patterns of the network and
reconstruct the desired inputs for a class, starting from random input
and from actual trials of our dataset.
    
    \subsection{Weights}\label{weights}
    
    We will use a network trained on our 4 classes with input downsampled to
150 Hz. Keep in mind, that the weights of our network are not
straightforward to interpret as they can reflect a mix of how the signal
is shaped for the different classes and how the noise is structured.
Nevertheless, we believe the weights of our network show some plausible
expected structures. 

To get an idea what the network has learned, we will go backwards. We
will first look at the softmax weights to see what kind of filters might
discriminate between what classes. Then we will look closer at what
these filters might detect.

The softmax layer has one weight per class per filter from the layer
before per pooled time interval. Since we have 4 classes, 40 filters in
the layer before and 53 pooled time intervals, we have 8480 weights.
They can be nicely visualized by plotting them as time courses (over the
pooled time intervals) per class per filter of the layer before.
    
    We show the weights of the softmax layer for 12 combined filters of the
layer before (combined, since they combine a spatial and a temporal
filter) in Figure \ref{fig:raw-net-softmax_weights}.
We show the 12 filters with the highest absolute sum of the
softmax weights to show those filters with the highest influence on the
classification. As said, the softmax weights show you a time course over
the pooled time intervals for each class. 
You can see that a lot of filters seem to discriminate between
right hand and left hand and between rest and feet. See, for example, weights for
combined filters 2 and 8 for right vs left and 3 and 11 for rest vs
feet.
\begin{figure}[H]
  \begin{subfigure}{0.47\linewidth}
    \begin{center}
    \adjustimage{max size={\linewidth}{0.9\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_14_0.pdf}
    \end{center}
  \end{subfigure}
  \begin{subfigure}{0.53\linewidth}
    \begin{center}
    \adjustimage{max size={\linewidth}{0.9\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_14_1.pdf}
  \end{center}
  \end{subfigure}
  \caption{The 12 softmax weights of raw net with the highest absolute sum.
  Parts highlighted in red discriminate right hand vs left hand and in green, rest vs feet.}
  \label{fig:raw-net-softmax_weights}
\end{figure}    
    
    Now we want to look at combined filters that are strongly discriminative
between right hand vs left hand and rest vs feet. We therefore compute the mean of the
softmax weights across all pool regions (so we have one mean per class
and per combined filter). We then compute where the difference between
the mean for right and the mean for left is the largest and vice versa.
We do the same for rest vs feet. Now we should have combined filters
that are strongly discriminating between right hand vs left hand and rest vs feet. In
the following, we will look at these combined filters together with their
softmax weights. 

We start with a filter that indicates the right hand in Figure 
\ref{fig:filters-right-vs-left}.
     We see that this filter is strongly lateralized. For example, we see a
low-frequency sine-like shape for FCC4h and C2 on the right side, while
we see almost no low-frequency shapes on the left side. This makes
sense, as we expect a \emph{decrease} of variance in a low
frequency on the \emph{left} side for the right hand. 
Since our net is a discriminative model, this can also be reflected in
filters with \emph{increased} variance in a low frequency on the
\emph{right} side.
The softmax weights also show that the network has learned a time course,
for example mostly ignoring the start of the trial.
    
\begin{figure}[H]
  \begin{subfigure}{\linewidth}
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.13\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_20_0.pdf}
    \end{center}
  \end{subfigure}
    
  \begin{subfigure}{\linewidth}
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_20_1.pdf}
  \end{center}
  \end{subfigure}
  \caption{Combined filter discriminating right hand from left hand (bottom) and corresponding softmax weights (top).
  Filters highlighted in red show a high amplitude low frequency shape.
  The filters have a length of 30 samples, i.e., 200 milliseconds.}
  
  \label{fig:filters-right-vs-left}
\end{figure}
    
    \clearpage
    To find out what frequencies this filter represents, we plot the
frequency amplitudes of the Fourier transformation in Figure \ref{fig:amplitudes-right-vs-left}. 
We see that the filters mostly contain frequencies around 10-15 Hz, and frequencies
above 65 Hz. We know that we have useful information for our
classification task in the the 10-15 Hz range, so our network seems to
learn to match at least some relevant frequencies. The higher frequncies
above 65 Hz can also contain important information, but we would
typically expect an increase on the left side for the right hand.
    
\begin{figure}[H]
  \begin{center}
  \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_22_0.pdf}
  \end{center}
  \caption{Frequency amplitudes for combined filter discriminating right hand from left hand (x axis is in Hz).}
  \label{fig:amplitudes-right-vs-left}
\end{figure}

    \clearpage
    We show the same visualization for a filter that indicates the left hand in Figure \ref{fig:filters-left-vs-right}. 
    Here we
also see a strong lateralization, now to the left side.

\begin{figure}[H]
  \begin{subfigure}{\linewidth}
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.13\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_24_0.pdf}
    \end{center}
  \end{subfigure}
    
  \begin{subfigure}{\linewidth}
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_24_1.pdf}
  \end{center}
  \end{subfigure}
  \caption{Combined filter discriminating left hand from right hand (bottom) and corresponding softmax weights (top).
  Filters highlighted in red show a high amplitude low frequency shape.
  The filters have a length of 30 samples, i.e., 200 milliseconds.}
  
  \label{fig:filters-left-vs-right}
\end{figure}

    \clearpage
    
    The combined filter indicating the rest class looks different, 
    it seems to mostly match a high frequency (see Figure \ref{fig:filters-rest-vs-feet}).
    It has the largest amplitudes around the central Cz electrode.
    Indeed, looking at the amplitudes of the Fourier transform in Figure \ref{fig:amplitudes-rest-vs-feet}, 
    we see peaks mostly around 40 Hz.
\begin{figure}[H]
  \begin{subfigure}{\linewidth}
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.13\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_27_0.pdf}
    \end{center}
  \end{subfigure}
    
  \begin{subfigure}{\linewidth}
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_27_1.pdf}
  \end{center}
  \end{subfigure}
  \caption{Combined filter discriminating rest from feet (bottom) and corresponding softmax weights (top).
  Filters highlighted in red show a high variance.
  The filters have a length of 30 samples, i.e., 200 milliseconds.}  
  \label{fig:filters-rest-vs-feet}
\end{figure}
    \clearpage
    
    
\begin{figure}[H]
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_29_0.pdf}
    \end{center}
  \caption{Frequency amplitudes for combined filter discriminating rest from feet (x axis is in Hz).}
    \label{fig:amplitudes-rest-vs-feet}
\end{figure}
    \clearpage
    
    Our final filter for this dataset shows a filter indicative for feet in 
    Figure \ref{fig:filters-feet-vs-rest}.
The filters show a high variance for the central Cz and CPz sensors. The
filter seems to match several frequencies. It shows peaks around 30, 45
and 65 Hz in Figure \ref{fig:amplitudes-feet-vs-rest}.
    
\begin{figure}[H]
  \begin{subfigure}{\linewidth}
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.13\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_31_0.pdf}
    \end{center}
  \end{subfigure}
    
  \begin{subfigure}{\linewidth}
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_31_1.pdf}
  \end{center}
  \end{subfigure}
  \caption{Combined filter discriminating feet from rest (bottom) and corresponding softmax weights (top).
  Filters highlighted in red show a high variance.
  The filters have a length of 30 samples, i.e., 200 milliseconds.}  
  \label{fig:filters-feet-vs-rest}
\end{figure}
    \clearpage
    
\begin{figure}[H]
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_32_0.pdf}
    \end{center}
  \caption{Frequency amplitudes for combined filter discriminating feet from rest (x axis is in Hz).}
  \label{fig:amplitudes-feet-vs-rest}
\end{figure}
    \clearpage
    
    In conclusion, we can see that the network does learn to match
oscillations in specific frequencies at specific locations and also
learns a time course over the time of the trial.
    \clearpage
    \subsection{Patterns}\label{patterns}
    
    \begin{keypointbox}

\item Patterns look more smooth spatially and temporally

\end{keypointbox}
    
    We now show the corresponding patterns. The signal is now more
``smeared'', i.e.~more spatially smooth, and higher frequencies are not
present anymore. For the right hand, we can still see the larger
amplitudes on the right side compared to the left side in 
Figure \ref{fig:patterns-right-vs-left}. We also see that
while this features discriminates between right and left, it is more
strongly activated by the rest and feet classes.
    
\begin{figure}[H]
  \begin{subfigure}{\linewidth}
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.13\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_39_0.pdf}
    \end{center}
  \end{subfigure}
    
  \begin{subfigure}{\linewidth}
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_39_1.pdf}
  \end{center}
  \end{subfigure}
  \caption{Combined pattern for right hand vs left hand (bottom) and corresponding softmax pattern (top).
  Patterns highlighted in red show a higher variance than those highlighted in green on the other side.
  The combined patterns have a length of 30 samples, i.e., 200 milliseconds.}  
  \label{fig:patterns-right-vs-left}
\end{figure}
    \clearpage
    
    For the left hand, we also see the pattern 
    has higher amplitudes on the expected side and is more
strongly activated by rest and feet, 
see Figure \ref{fig:patterns-left-vs-right}.

\begin{figure}[H]
  \begin{subfigure}{\linewidth}
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.13\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_41_0.pdf}
    \end{center}
  \end{subfigure}
    
  \begin{subfigure}{\linewidth}
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_41_1.pdf}
  \end{center}
  \end{subfigure}
  \caption{Combined pattern for left hand vs right hand (bottom) and corresponding softmax pattern (top).
  Patterns highlighted in red show a higher variance than those highlighted in green on the other side.
  The combined patterns have a length of 30 samples, i.e., 200 milliseconds.}  
  \label{fig:patterns-left-vs-right}
\end{figure}
    \clearpage
    
    For the rest class, we see mostly high amplitude low frequency signals,
with some surprising exceptions such as C2 and CCP4h, see Figure \ref{fig:patterns-rest-vs-feet}.
 
\begin{figure}[H]
  \begin{subfigure}{\linewidth}
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.13\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_43_0.pdf}
    \end{center}
  \end{subfigure}
    
  \begin{subfigure}{\linewidth}
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_43_1.pdf}
  \end{center}
  \end{subfigure}
  \caption{Combined pattern for rest vs feet (bottom) and corresponding softmax pattern (top).
  Patterns highlighted show some unexpected shape.
  The combined patterns have a length of 30 samples, i.e., 200 milliseconds.}  
  \label{fig:patterns-rest-vs-feet}
\end{figure} 
 
    \clearpage
    
    For the feet class, we see some signals which have a large positive mean, for
example Cz, in Figure \ref{fig:patterns-feet-vs-rest}. 
We also see that this pattern is most activated in the first
half of the trial for the feet class. This might indicate a
discriminative low frequency component in the first half of the trial.
    \begin{figure}[H]
  \begin{subfigure}{\linewidth}
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.13\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_45_0.pdf}
    \end{center}
  \end{subfigure}
    
  \begin{subfigure}{\linewidth}
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_45_1.pdf}
  \end{center}
  \end{subfigure}
  \caption{Combined pattern for feet vs rest (bottom) and corresponding softmax pattern (top).
  Patterns highlighted have means clearly above zero.
  The combined patterns have a length of 30 samples, i.e., 200 milliseconds.}  
  \label{fig:patterns-feet-vs-rest}
\end{figure} 

    \clearpage
    
    \subsection{Gradient Descent}\label{gradient-descent}
    
    \begin{keypointbox}

\item Gradient descent reconstruction also shows expected lateralization

\end{keypointbox}

    Now we use gradient descent to reconstruct inputs for the different
classes.
    
    Starting with the right hand class, we see a high variance for FCC4h on
    the right side in Figure \ref{fig:gradient-descent-right-hand}. This is consistent with a
\emph{decrease} on the \emph{left} side). 
We also see some rather confusing low-frequency shapes.
The frequency decomposition shows a peak around 12 Hz in 
Figure \ref{fig:gradient-descent-amplitude-right-hand}. 
    
\begin{figure}[H]
  \begin{center}
  \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_51_0.pdf}
  \end{center}
  \caption{Gradient descent reconstruction for right hand (x axis is in sec). Part highlighted in red has high variance.}
  \label{fig:gradient-descent-right-hand}
\end{figure}
    
    \clearpage
\begin{figure}[H]
        \begin{center}
        \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_52_0.pdf}
        \end{center}
\caption{Frequency amplitudes for gradient descent reconstruction for right hand (x axis is in Hz). 
Part highlighted in red is an interesting peak.}
  \label{fig:gradient-descent-amplitude-right-hand}
\end{figure}
    
    \clearpage
    For the left hand class, we see that the reconstructed input shows a
strong lateralization to the left side in Figure \ref{fig:gradient-descent-left-hand}.
\begin{figure}[H]
  \begin{center}
  \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_54_0.pdf}
  \end{center}
  \caption{Gradient descent reconstruction for left hand (x axis is in sec). Parts highlighted in red have high variance.}
  \label{fig:gradient-descent-left-hand}
\end{figure}
    
    \clearpage
    
    For the rest class, we see a lot of variance on most sensors, with
higher frequencies on Cz and CCP1h, see Figures \ref{fig:gradient-descent-rest} and 
\ref{fig:gradient-descent-amplitude-rest}.

\begin{figure}[H]
  \begin{center}
  \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_57_0.pdf}
  \end{center}
  \caption{Gradient descent reconstruction for the rest class (x axis is in sec). Parts highlighted in red have high variance.}
  \label{fig:gradient-descent-rest}
\end{figure}

    \clearpage
\begin{figure}[H]
  \begin{center}
  \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_58_0.pdf}
  \end{center}
\caption{Frequency amplitudes for gradient descent reconstruction for the rest class (x axis is in Hz). 
Parts highlighted in red are interesting peaks.}
  \label{fig:gradient-descent-amplitude-rest}
\end{figure}

    \clearpage
    
    The reconstructed feet input shows high amplitudes on the left and the right
side and low amplitudes in the center.
    
\begin{figure}[H]
  \begin{center}
  \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_61_0.pdf}
  \end{center}
  \caption{Gradient descent reconstruction for the feet class (x axis is in sec). 
  Parts highlighted in red have high variance, in green low variance.}
  \label{fig:gradient-descent-feet}
\end{figure}
    \clearpage
    \subsection{Recognizing Signal
Potentials}\label{recognizing-signal-potentials}
    
    Now we go on to show a case where the network has possibly learned to
recognize a signal potential (or at least a low-frequency oscillation
with just one period) and not an oscillation, which oscillates for a
longer time. We show a combined filter (i.e., a filter from the second
layer) that has a large difference between the weights for the right
hand at the left hand at the start of the trial
 in Figure \ref{fig:start-trial-example-weights}. 
 If the filter learns to
use the variance of an oscillatory signal typical for motor behavior, we would expect it to not use
the start of the trial. 
The variance changes usually only some time
after the start of the trial (e.g., 500 ms after the start).
Furthermore, for some sensors, the filter is not oscillating around the
dotted zero-line, for example it is clearly above the line for CCP2h and
below for CCP4h.

\begin{figure}[H]
  \begin{subfigure}{\linewidth}
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.13\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_66_0.pdf}
    \end{center}
  \end{subfigure}
    
  \begin{subfigure}{\linewidth}
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_66_1.pdf}
  \end{center}
  \end{subfigure}
  \caption{Combined filter (bottom) and corresponding softmax weights (top) with large weight differences at the start of the trial.
  Filters highlighted in red is largely above zero and in green largely below zero, both show a low frequency shape.
  The filters have a length of 30 samples, i.e., 200 milliseconds.}
  \label{fig:start-trial-example-weights}
\end{figure}
  
    \clearpage
    
    In order to better understand what this filter might encode, we can
again use gradient descent on the input. We want to see how this filter
might help recognize a trial for the left hand class. First, we take
some random input, remove all other filters from the second layer from
the model and iteratively optimize the input to lead to a high
prediction for the left hand class and low predictions for the other classes as
before.
We show this for six different random initializations. You can see a
recognizable shape at the start of the trial in 
Figure \ref{fig:start-trial-example-reconstruction}.
    
\begin{figure}[H]
  \begin{center}
  \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_76_0.pdf}
  \end{center}
  \caption{Gradient descent reconstructed input for the combined filter active at the start of the trial.
  Parts highlighted in red show a recognizable shape we will attempt to match in the real data.}
  \label{fig:start-trial-example-reconstruction}
\end{figure}
    
    \clearpage
    
    To confirm that this could indeed be a useful feature, we compute the
mean of all trials for the left hand and the trial mean for all trials
of the other classes. We show the difference
\(mean(Trials_{left}) - mean(Trials_{non-left})\) in Figure \ref{fig:start-trial-example-means}.
Indeed we see a recognizable shape at least partially consistent with our
reconstruction. For example the negativity for sensor C4 matches the
reconstruction and the missing negativity for CCP2h also matches the
reconstruction.

\begin{figure}[H]
  \begin{center}
  \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Raw_Net_Visualizations_files/Raw_Net_Visualizations_79_0.pdf}
  \end{center}
  \caption{Mean of left hand minus mean of other classes across all trials of one person.
  Parts highlighted in red show a recognizable shape we can match with our reconstruction.}
  \label{fig:start-trial-example-means}
\end{figure}

    \clearpage
    
    
All in all, we have shown that the network can also recognize a signal
potential or at least recognize a single period in a low frequency.

\begin{comment}
TODECIDE
maybe rewrite this sentence?
\end{comment}
    
\clearpage