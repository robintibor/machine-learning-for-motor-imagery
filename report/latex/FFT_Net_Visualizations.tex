
    \section{FFT Net Visualizations}\label{sec:fft-net-visualizations}
    
    \begin{keypointbox}

\item FFT net shows plausible patterns
\item  FFT net learns to use higher frequencies
\item  FFT net weights more smooth than softmax weights

\end{keypointbox}
    
    We now show some visualizations of our FFT Net. Since our FFT Net has no
activation functions between the layers, it is essentially a linear
classifier. So we can transform our weights for the different layers
into just one weight vector per class. We will show these weight vectors
directly and transform the weight vectors into more interpretable
patterns.
    
    \subsection{Weights}\label{weights}

We first show the weights for the right hand class. Note that, for the
frequencies until 30 Hz, the bins have height 2 Hz and for higher
frequencies, the bins have height 6 Hz. However, the images are scaled
so that the y axis always has the same scale for any interval, i.e., the
distance between 20 Hz and 30 Hz is the same as the distance between 30
Hz and 40 Hz. Therefore, the weights from 30 Hz onwards will occupy
three times the height per weight in each image. Also note that our
network was trained on standardized data. So, for each channel X
frequency row we computed the mean and standard deviation and for all
features of this chan and frequency, we subtracted this mean and divided
by this standard deviation.

You can see positive weights for the higher frequencies on the left side
and negative weights on the right side, as expected (see 
Figure \ref{fig:fft-net-weights-right}. 
Also you can see
some negative weights for the lower frequencies on the left side, for
example on C3 and CCP3h. These are less visible on the right side, also
as expected.
    
\begin{figure}[H]
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{FFT_Net_Visualizations_files/FFT_Net_Visualizations_13_0.pdf}
    \end{center}
    \caption{FFT net weights for right hand. X axis is in time bins, y axis is in Hz.
    From negative blue over grey to positive red. 
    Circles highlight interesting differences across sides.}
    \label{fig:fft-net-weights-right}
\end{figure}
    
    \clearpage
    
    The weights for the left hand also show what we expect 
    in Figure \ref{fig:fft-net-weights-left}.
    They look like a
color-flipped version of the right hand weights. For example, the
sensors on the right side have positive weights on the higher
frequencies.
    
\begin{figure}[H]
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{FFT_Net_Visualizations_files/FFT_Net_Visualizations_15_0.pdf}
    \end{center}
    \caption{FFT net weights for left hand. 
    From negative blue over grey to positive red. X axis is in time bins, y axis is in Hz.
    Circles highlight interesting differences across sides.}
    \label{fig:fft-net-weights-left}
\end{figure}
    
    \clearpage
    The weights for the rest class also look plausible, 
    see Figure \ref{fig:fft-net-weights-rest}. 
    They show negative
weights for the higher frequencies, and higher weights on the low
frequencies, more pronounced on the central and centre-right electrodes.
    
\begin{figure}[H]
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{FFT_Net_Visualizations_files/FFT_Net_Visualizations_17_0.pdf}
    \end{center}
    \caption{FFT net weights for rest class. 
    From negative blue over grey to positive red. X axis is in time bins, y axis is in Hz.
    Circles highlight interesting differences from central electrodes to an outer electrode.}
    \label{fig:fft-net-weights-rest}
\end{figure}
    
    \clearpage
    Finally, the feet class weights are mostly the color-flipped weights of
the rest class, for example positive instead of negative weights on the
higher frequencies for the central electrodes.
    
\begin{figure}[H]
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{FFT_Net_Visualizations_files/FFT_Net_Visualizations_19_0.pdf}
    \end{center}
    \caption{FFT net weights for feet class. 
    From negative blue over grey to positive red. X axis is in time bins, y axis is in Hz.
    Circles highlight interesting differences from central electrodes to an outer electrode.}
    \label{fig:fft-net-weights-feet}
\end{figure}
    
    \clearpage
    
    \subsection{Patterns}\label{patterns}

We now transform the weights to more interpretable patterns by
multiplying the covariance matrix of all features with the weights. For
the right hand, we now see that the negative low frequencies are present
on both sides, just a little stronger on the left side, seems
Figure \ref{fig:fft-net-patterns-right}.
The difference
for the higher frequencies seems more pronounced if you compare C3 and
C4 or FCC3h and FCC4h.

\begin{figure}[H]
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.33\paperheight}}{FFT_Net_Visualizations_files/FFT_Net_Visualizations_26_0.pdf}
    \end{center}
    \caption{FFT net patterns for right hand. 
    From negative blue over grey to positive red. X axis is in time bins, y axis is in Hz.
    Circles highlight interesting differences across sides.}
    \label{fig:fft-net-patterns-right}
\end{figure}

    \clearpage
    
We see the high-frequency pattern on the right side for the left hand 
in Figure \ref{fig:fft-net-patterns-left}.
    
\begin{figure}[H]
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.6\paperheight}}{FFT_Net_Visualizations_files/FFT_Net_Visualizations_28_0.pdf}
    \end{center}
    \caption{FFT net patterns for left hand. 
    From negative blue over grey to positive red. X axis is in time bins, y axis is in Hz.
    Circles highlight interesting differences across sides.}
    \label{fig:fft-net-patterns-left}
\end{figure}
    
    \clearpage
    The rest class has large low-frequency patterns across all sensors and
some decrease in the high frequencies.
    
\begin{figure}[H]
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.6\paperheight}}{FFT_Net_Visualizations_files/FFT_Net_Visualizations_30_0.pdf}
    \end{center}
    \caption{FFT net patterns for the rest class. 
    From negative blue over grey to positive red. X axis is in time bins, y axis is in Hz.
    Circles highlight interesting high frequency patterns.}
    \label{fig:fft-net-patterns-rest}
\end{figure}
    
    \clearpage
    For the feet class we see some high frequency patterns mostly on the
central electodes Cz, CCP1h, CCP2h
in Figure \ref{fig:fft-net-patterns-feet}.
    
    \begin{figure}[H]
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.33\paperheight}}{FFT_Net_Visualizations_files/FFT_Net_Visualizations_30_0.pdf}
    \end{center}
    \caption{FFT net patterns for the feet class. 
    From negative blue over grey to positive red. X axis is in time bins, y axis is in Hz.
    Circles highlight interesting low and high frequency patterns.}
    \label{fig:fft-net-patterns-feet}
\end{figure}
    
    \clearpage
    
    \subsection{Comparison to Softmax}\label{comparison-to-softmax}
    
    To show that our network successfully regularizes the weights, we also
show the weights for the regular softmax trained on the same data. We
will only show the weights for the right hand here. We see that they
indeed have a much more irregular, ``noisy'', structure.
    \begin{figure}[H]
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.33\paperheight}}{FFT_Net_Visualizations_files/FFT_Net_Visualizations_37_0.pdf}
    \end{center}
    \caption{Softmax weights for the right hand. 
    From negative blue over grey to positive red. X axis is in time bins, y axis is in Hz.
    Circles highlight interesting low and high frequency patterns.}
    \label{fig:softmax-weights}
\end{figure}
    
    All in all, you can see that the FFT net uses higher frequencies and
spatial structure and seems to successfully regularize the weights.
    
    