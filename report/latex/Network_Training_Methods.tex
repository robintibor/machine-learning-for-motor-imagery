



    
        
    \chapter{Network Training Methods}\label{chap:network-training-methods}
    
    \begin{keypointbox}

\item We use the adaptive learning rule Adam for our gradients
\item We use dropout regularization on all layers except pooling and input layers
\item We use early stopping and continue to train on the validation set after stopping

\end{keypointbox}
    
    We aim to have a network training procedure that:

\begin{itemize}
\tightlist
\item
  leads to reasonable performance for all of our architectures
\item
  keeps training time short
\item
  does not require a lot of manual tuning of hyperparameters
\end{itemize}

We describe the learning rule and the regularizations including the
termination procedure that should achieve this in the following.
    
    \section{Learning Rule - Adam}\label{learning-rule---adam}
    
    \begin{keypointbox}

\item Adam favors gradients with stable directions

\end{keypointbox}

    We train our networks with stochastic gradient descent. When optimizing
a neural network with stochastic gradient descent, you compute gradients
of your loss function for all weights of the network on mini-batches
from your training set. You now need a learning rule to tell you how to
apply these gradients to your weights. The simplest learning rule is to
subtract the gradients, maybe multiplied with a learning rate, from your
weights.

This has several problems:

\begin{itemize}
\tightlist
\item
  training might slow down when the gradients become small, even if they
  are always pointing in the same direction (i.e., you are on a plateau
  of the error function)
\item
  gradients for the different weights can be on very different scales
  which might disrupt learning
\end{itemize}

One learning rule to combat these problems is Adam
\cite{kingma_adam:_2014}. Adam uses an exponentially moving average of
the gradients and an exponential moving average of the squared gradients
for each weight to compute the weight update. It essentially updates a
weight \(w_i\) at timestep \(t + 1\) like this: \[
weight_{i, t+1} = weight_{i, t} - learning \ rate \cdot \frac{Exponential \ moving \ average \ of \ gradients_{t+1,i}}{\sqrt{Exponential \ moving \ average \ of \ squared \ gradients_{t+1,i}}}
\]

It initializes the estimates of the moving averages to be zero and
accounts for the bias of this initialization. The fraction in the
equation above is supposed to capture the certainty about the whether
the negative moving average of the gradient points in a direction that
will minimize our loss. Those weights, whose negative gradient is more
likely to point a loss-minimizing direction, can be updated more
strongly. If the computed gradients on the mini-batches switch
directions often, this ratio will become small: the moving average of
the gradient will become relatively small compared to the moving average
of the squared gradient. Conversely, if the computed gradients have
roughly the same direction, the ratio will be larger. Also, Adam will
give the same updates for gradients that are just scaled to be larger or
smaller: The scale factor will cancel out and the ``certainty'' ratio
will remain the same.

Adam has three hyperparameters (the learning rate and two decay factors
for the moving averages). In practice, Adam often achieves good results
when leaving these hyperparameters to the default values suggested by
the authors in their paper. Therefore, it is a quite useful learning
rule for us. It especially helped training the raw nets, sometimes
improving their accuracy by more than 10\%. We train SGD with Adam with
a batch size of 55.
    
    \section{Regularization - Dropout and Weight
Norms}\label{sec:regularization---dropout-and-weight-norms}

        \begin{keypointbox}

\item Dropout randomly turns off different units in each epoch to prevent co-adaptations

\end{keypointbox}

    Regularization is typically important for any machine learning approach.
This is especially true for neural networks due to their large number of
parameters, which makes them easy to overfit to your training data.

We mainly use dropout \cite{srivastava_dropout:_2014} to regularize our
networks. Dropout tries to prevent units in the network from co-adapting
too much and can also be seen as a form of model averaging.

During training, in every epoch, dropout randomly drops units from the
network when computing the gradient. Therefore, it is more difficult for
a unit to overfit by co-adapting to another unit, since this other unit
will sometimes be turned off (with dropout probability 0.5, it will be
turned off in half of the epochs in expectation). Another way to
interpret dropout is to see it as an online training procedure for
\(2^{|Units|}\) networks with shared weights, namely the networks for
all combinations of dropped out and not dropped out units.

It is easily possible to have a single network that uses all units at
test time. For this, during training time, we weigh the inputs to any
layer by the inverse of their dropout probability (i.e., we multiply
them by 2 for dropout probability 0.5). At test time, we do not weigh
the inputs and instead multiply all weights by 0.5. This is similar to
averaging over the outputs of all the \(2^{|Units|}\) networks.

In our case, we do not use dropout on the inputs and the pooling layers.
Applying dropout to inputs almost always caused either no improvement or
worsening of the accuracies of our models. For pooling layers,
typically, dropout does not help, although now techniques exist to also
use dropout on pooling layers, which we have not tried yet
\cite{gal_bayesian_2015} \cite{gal_what_2015}.

We use dropout with probability 0.5 on all other layers.

To further regularize our networks, we enforce that our convolutional
kernels have a maximum L2-norm of 2 and our softmax weight vectors have
a maximum L2-norm of 0.5. That means, in case our weights grow to a norm
larger than 2 or 0.5, they are projected to weights with a norm of 2 or
0.5. Constraining the weight vectors with max norms is known to work
well with dropout \cite{srivastava_dropout:_2014}. These values of 2 and
0.5 could certainly be further optimized for the different
architectures, but using other hand-picked values usually did not
improve accuracies for us.
    
    \section{Early Stopping}\label{early-stopping}
    
        \begin{keypointbox}

\item We stop based on the negative log likelihood on the validation set
\item We continue to train on the validation set to make use of all possible data

\end{keypointbox}

Typically, during training, networks will first learn relationships that
mostly generalize from the training data to the test data and later
start to overfit and learn relationships only present in the training
data. One way to prevent the networks from overfitting and to reduce
training time is to use early stopping. We use a procedure inspired by a
\href{https://code.google.com/p/cuda-convnet/wiki/Methodology}{successful
early stopping procedure for image classification}\footnote{\url{https://code.google.com/p/cuda-convnet/wiki/Methodology}}.

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Split the original train fold into a partial train fold, using 90\% of
  the data (as a time-block, i.e., respecting the time-ordering of the
  trials), and a validation fold, using 10\% of the data (i.e., the last
  10\%).
\item
  Train on the partial train fold until the misclassification rate on
  the validation fold has not increased for 200 epochs.
\item
  Across all epochs, pick the weights with the best misclassification
  rate on the validation fold (in case there are several weights with
  the same rate, use the last network).
\item
  Remember the negative log likelihood \(partial\_train_{nll}\) of these
  best weights on the partial train fold
\item
  Add the validation fold to the training fold restoring the original
  train fold.
\item
  Train on the original training fold (includes the validation fold)
  until the negative log likelihood on the validation fold is smaller or
  equal to \(partial\_train_{nll}\) stored before.
\end{enumerate}

Additionally, we include two hard stopping criteria to prevent overly
long training times. We always stop the first phase, before the
reintegration of the validation fold, after 1500 epochs. Also, we stop
the second phase once we have trained twice as many epochs as it took to
reach the best weights in the first phase.

Without the latter hard stopping criterion, the second phase could run
infinitely long, if the validation fold is more difficult, i.e.~less
separable, than the partial training fold.

Stopping in the second phase when the negative log likelihood is going
below a certain value has another advantage for us: Typically our
negative log likelihood values as well as our misclassification rates
can vary quite a lot from epoch to epoch. The validation negative log
likelihood is still reasonably correlated to the test misclassification
rate. Therefore, if we stop when the validation negative log likelihood
has just dropped compared to the epoch before, we will often stop at an
epoch where the test misclassification rate has also just dropped.

To illustrate this effect, we show the mean of the test
misclassification rates and the validation negative log likelihood
values for the last 20 epochs of all our final network experiments in Figure \ref{fig:early-stop-misclass}. 
Note that, in the final epoch, the validation negative log likelihood drops
together with the test misclassification rate.
    
\begin{figure}[ht]
  \begin{center}
  \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Network_Training_Methods_files/Network_Training_Methods_15_0.pdf}
  \end{center}
  \caption{Test misclassification rate and validation negative log likelihood in the final 20 epochs. 
  Averaged across all subjects and all networks (filter bank, raw, FFT, FFT + phase).}
  \label{fig:early-stop-misclass}
\end{figure}
    
    