



    
        
    \chapter{Experiments}\label{chap:experiments}

        \begin{keypointbox}

\item We automatically clean, resample, highpass, and standardize our data 
\item We evaluate on an unused unclean test fold and a barely used clean test fold

\end{keypointbox}

In this chapter, we will describe the experiments, including:

\begin{itemize}
\tightlist
\item
  datasets
\item
  cleaning methods
\item
  preprocessing
\item
  evaluation method
\end{itemize}
    
    \section{Datasets}\label{datasets}

        \begin{keypointbox}

\item We have 18 4-class motor execution datasets with 1040  trials each

\end{keypointbox}

The datasets are 18 EEG recordings of motor executions provided by 
the iEEG and Brain Imaging Group of the University Freiburg. 
Subjects were
instructed to either tap their right hand fingers, tap their left hand
fingers, do nothing (resting state) or clench their toes in each trial.
Trials lasted 4 seconds, with 3-4 seconds pause inbetween. The sequences
of instructions were pseudorandomized.

There was a total of 1040 trials, divided into 13 runs with 80 trials
each. A recording typically lasted around 4 hours. For some datasets,
there are less trials, e.g., if the subject got too tired to continue.
Subjects were all healthy students or PhD candidates.
    
    \section{Cleaning}\label{cleaning}
    
        \begin{keypointbox}

\item We clean using the EOG channels and the variances of the EEG channels

\end{keypointbox}

We cleaned our datasets, trying to remove noisy channels and trials as
well as trials with artifacts.

\subsection{Trial Removal by EOG
Artifacts}\label{trial-removal-by-eog-artifacts}

For all EOG channels (EOGh and EOGv in our case) and all trials, we
compute the difference between the minimum and the maximum value on the
EOG channel. We remove all trials where the difference between the
maximum and the minimum value is greater than 600 mV.

\subsection{Channel and Trial Removal by
Variance}\label{channel-and-trial-removal-by-variance}

We first bandpass our signal to 0.5 Hz to 150 Hz using a 4th-order
butterworth filter. Then we compute the variance for all trials and
channels. Now we have one variance value per trial per channel. We then
determine a critical threshold for the variance as shown in Figure \ref{fig:variance-threshold-pseudocode}.
Above this threshold, we consider a trial/channel combination unclean.

\begin{figure}[H]
\begin{verbatim}
Function compute_variance_threshold
Input: variances, whisker_percentage, whisker_length
Output: variance_threshold
1. low_variance := variance value at percentile whisker_percentage 
2. high_variance := variance value at percentile 100 - whisker_percentage
3. variance_threshold := high_variance + (high_variance - low_variance) * whisker_length
\end{verbatim}
\caption{Pseudocode for determining the variance threshold. \\
We use whisker percentage 10 and whisker length 3. This means our
variance threshold will be at the 90th percentile of the variance plus 3
times the difference between the 90th and the 10th percentile of the
variance.}
\label{fig:variance-threshold-pseudocode}
\end{figure}



Then we use the following cleaning algorithm:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  \textbf{Excessive Trial Removal}: Do once:

  \begin{enumerate}
  \def\labelenumii{\arabic{enumii}.}
  \tightlist
  \item
    Remove all trials where more than 20 percent of the channels exceed
    the variance threshold
  \item
    Recompute variance threshold from remaining trials x channels
  \end{enumerate}
\item
  \textbf{Channel Removal}: Repeat until no more channels removed:

  \begin{enumerate}
  \def\labelenumii{\arabic{enumii}.}
  \tightlist
  \item
    If the number of trial x channel variances above the threshold is
    larger than 5\% of the number of remaining trials:
  \begin{enumerate}
  \def\labelenumii{\arabic{enumii}.}
  \item
    For all channels, compute number of remaining trials above the
    variance threshold
  \item
    Remove channels where the number of trials above the threshold is
    larger than 5\% of all remaining trials and larger than 10\% of all
    remaining unclean trial x channel combinations
  \item
    Recompute variance threshold from remaining trials x channels
  \end{enumerate}
  \end{enumerate}
\item
  \textbf{Trial removal}: Repeat until no more trials removed:

  \begin{enumerate}
  \def\labelenumii{\arabic{enumii}.}
  \tightlist
  \item
    Remove all trials where at least one channel is above the variance
    threshold
  \item
    Recompute variance threshold from remaining trials x channels
  \end{enumerate}
\item
  \textbf{Unstable channel removal}: Do once:

  \begin{enumerate}
  \def\labelenumii{\arabic{enumii}.}
  \tightlist
  \item
    Compute variance of trial variances for each channel
  \item
    Compute variance threshold for these trial-variance variances
  \item
    Remove all channels with a trial-variance variance above the
    threshold
  \end{enumerate}
\end{enumerate}

This is largely following the
\href{https://github.com/bbci/bbci_public/blob/45cd9349dc60cb317e260b10d46de20bf9075afa/processing/utils/reject_varEventsAndChannels.m}{cleaning
procedure of the BBCI Toolbox}\footnote{\url{https://github.com/bbci/bbci_public/blob/45cd9349dc60cb317e260b10d46de20bf9075afa/processing/utils/reject_varEventsAndChannels.m}} with small modifications to prefer
removing channels to removing trials. We remove channels first, because
we expect that a lot of the noisy channels are of less interest to us
for decoding motor execution, e.g., channels in the frontal area. By
cleaning channels first, we might be able to retain more trials. The
condition in 2.1 might be in the BBCI Toolbox by accident, a more logical
check would be to check if the number of trial x channel variances above the threshold is larger
than than 5\% of all \emph{trial x channel} combinations (see also this
\href{https://github.com/bbci/bbci_public/issues/66}{github issue}\footnote{\url{https://github.com/bbci/bbci_public/issues/66}}).
This might lead to less channels being cleaned for some cases, but
should have no further negative consequences. The unstable channel
removal should delete channels where the variance changes a lot from
trial to trial.
    
    \section{Preprocessing}\label{preprocessing}

        \begin{keypointbox}

\item We resample to 150 or 300 Hz, highpass above 0.5 Hz
\item We standardize trials in an online-compatible fashion

\end{keypointbox}

Our preprocessing pipeline first preprocesses the continuous data, and
then standardizes the epoched data (i.e.. the trials).

\subsection{Continuous Data
Preprocessing}\label{continuous-data-preprocessing}

The continuous preprocessing is done across the entire dataset, ignoring
train/test splits, like this:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Resample to 150 Hz or 300 Hz
\item
  Highpass above 0.5 Hz with butterworth filter of order 3 (only forward filtering)
\item
  For visualizations, subtract the common average of all channels for
  each sample
\end{enumerate}

We subtract the common average for visualizations, since the original
data is referenced to Cz and therefore the information from Cz will be
spread to all sensors, while Cz itself will contain no information. We
do not do the same for decoding, as for some very few datasets, this
seems to worsen decoding performance. We think this is because some
noisy channels were not removed by the cleaning procedure and then
strongly affect the common average.

\subsection{Trial Standardization}\label{trial-standardization}

After the continuous preprocessing, we cut this data into trials and
standardize like this:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  For the train fold, compute means and standard deviations for all
  channels x frequency combinations
\item
  Subtract the means from the train fold and divide by the standard
  deviations
\item
  For the test fold, go through the time-ordered trials. For each trial:

  \begin{enumerate}
  \def\labelenumii{\arabic{enumii}.}
  \tightlist
  \item
    Compute means and standard deviations from the trials of the train
    fold and the trials of the test fold until that trial
  \item
    Subtract the means from that trial and divide by the standard
    deviations
  \end{enumerate}
\end{enumerate}

For the filter bank net and filter bank common spatial patterns, the frequency refers
to the filterband, whereas for the FFT net, the frequency refers to the
frequency bin. The raw net does not have a frequency dimension and means
and standard deviations are only computed for the channels.

This standardization could also be applied online as it only uses
information up until the current trial for the test fold. It can also be
implemented efficiently, since you only need the last means and standard
deviations and the next trial to compute the next means and standard
deviations (see
\href{https://en.wikipedia.org/w/index.php?title=Algorithms_for_calculating_variance&oldid=674740274\#Online_algorithm}{this
article}\footnote{\url{https://en.wikipedia.org/w/index.php?title=Algorithms_for_calculating_variance&oldid=674740274\#Online_algorithm}}, we use the formulas from
\href{http://stats.stackexchange.com/a/43183/56289}{here}\footnote{\url{http://stats.stackexchange.com/a/43183/56289}}). Another
option would be to do exponentially running standardization, i.e., weigh the newer
trials exponentially higher when computing the means and standard
deviations. This might better account for changes in the variance.
However, as we did not see large improvements for the decoding
performance and a exponentially running standardization would introduce a new
parameter (how strongly to favor the newer trials), we stuck with the
aforementioned standardization procedure.
    
    \section{Evaluation Method}\label{evaluation-method}

        \begin{keypointbox}

\item We never used the last runs of our dataset during development
\item We evaluate on the uncleaned last runs, or on a cleaned fold mostly consisting of them

\end{keypointbox}

We decided to use the last two runs (i.e., 160 trials for a complete
dataset) for our final evaluation. During the development of our
architectures, including the optimization of our hyperparameters, these
two runs were never used. This should ensure we are not fitting our
architectures or hyperparameters to our test data.

\subsection{Cleaning Evaluation Data}\label{cleaning-evaluation-data}

For ease of implementation, during development, we decided to always
clean the entire dataset (disrespecting train/test splits). For our
final evaluation, we decided to do two cleaning variants. The first one
is the same as our development cleaning procedure:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Clean entire dataset together
\item
  Use last 10\% of the dataset as the test fold
\end{enumerate}

In this case, we only have clean trials. However, we might have some
trials of runs before the last two runs in our test set. This happens in
case a lot of the trials in the test fold are removed by the cleaning
procedure.

The second cleaning variant does not clean the trials of the test fold
at all:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Split dataset into train fold (first 11 runs) and test fold (last two
  runs)
\item
  Compute cleaned trials and channels of entire dataset together
\item
  Remove rejected channels from train and test fold
\item
  Remove rejected trials from train fold, but keep all trials in the
  test fold
\end{enumerate}

This ensures the trials we are predicting were never used during
development. However, we now predict on unclean trials. Unfortunately,
it was not possible to clean the entire dataset, and only keep cleaned
trials of the last two runs as our test fold. The problem here was, for
at least one dataset, all trials in the test fold were removed by the
cleaning procedure.

Nevertheless, these two variants also have the advantage of showing the
decoding accuracies for clean and for unclean trials for our different
decoding methods.

\subsection{Sensors and Sampling
Rates}\label{sensors-and-sampling-rates}

We also tried using different sensor and sampling rates for our final
evaluation. We either used all EEG sensors or only the EEG sensors with
a C in their name. This allows us to see if adding more, and probably
less informative, sensors to our input improves or degrades the
accuracies for the different decoding methods.

We also varied the sampling rate, either using 150 Hz or 300 Hz. This
should show us how important the frequencies above 75 Hz (the Nyquist
frequency for 150 Hz sampling rate) are for the decoding.

Also, the variants with 150 Hz and/or only using the C-sensors allow us
to see how much time we can save for the training (as our data becomes
smaller) and might show a tradeoff between accuracy and training time.
    
    \subsection{Machine Specifications (CPU, GPU,
RAM)}\label{machine-specifications-cpu-gpu-ram}
    
    The networks were trained on
\href{http://www.geforce.com/hardware/desktop-gpus/geforce-gtx-titan-black/specifications}{Nvidia
Geforce GTX Titan Black}\footnote{\url{http://www.geforce.com/hardware/desktop-gpus/geforce-gtx-titan-black/specifications}} graphic cards, which have 6 GB memory. The CPU
was a Intel(R) Xeon(R) CPU E5-2650 v2 @ 2.60GHz with 32 cores and the machines had 128
GB main memory. Filter Bank CSP was trained on machines with the same
CPU and 64 GB main memory (main memory was not critical for any of the
approaches).
    
    