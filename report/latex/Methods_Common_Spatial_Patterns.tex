



    
        
    \section{Filter Bank Common Spatial
Patterns}\label{filter-bank-common-spatial-patterns}
    
    \subsection{Overview}\label{overview}
    
    \begin{keypointbox}
\item  Common spatial patterns has separate feature extraction, feature selection and classification steps
\item  Can be extended to multiple filterbands by filter bank common spatial patterns

\end{keypointbox}
    
    Filter bank common spatial patterns is a decoding approach
specifically designed for EEG decoding and very successful for movement
decoding \cite{chin_multi-class_2009}.

Our common spatial patterns (CSP) pipeline broadly works like this:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  \textbf{Extract Features:}

  \begin{enumerate}
  \def\labelenumii{\arabic{enumii}.}
  \tightlist
  \item
    Find spatial filters (weight vectors for the sensors), that
    create more class-discriminative signals (this is
    the actual CSP part)
  \item
    Apply the spatial filters to all trials
  \item
    Compute the variance for each spatially filtered trial
  \end{enumerate}
\item
  \textbf{Select Features}:

  \begin{enumerate}
  \def\labelenumii{\arabic{enumii}.}
  \tightlist
  \item
    Select features using a cross validation on the training part of our
    data
  \end{enumerate}
\item
  \textbf{Classify:}

  \begin{enumerate}
  \def\labelenumii{\arabic{enumii}.}
  \tightlist
  \item
    Classify the features using linear discriminant analysis (LDA)
  \end{enumerate}
\end{enumerate}

These steps would only work for one filterband and two classes. We
extend them to multiple filterbands by extracting features from multiple
filterbands (that is why it is called filter bank common spatial
patterns). We extend to four classes by training all pairwise
classifiers and using a voting procedure to determine the winning class. 
In Figure \ref{fig:fbcsp-overview}, we show a graphical overview over our filter bank common spatial
patterns classification pipeline to give you a preview of what we will
explain in the following sections.

\begin{figure}[H]
\begin{center}
\adjustimage{max size={0.9\linewidth}{0.45\paperheight}}{Methods_Common_Spatial_Patterns_files/csp_explanation_deep_2.pdf}
\end{center}
\caption{Filter bank common spatial patterns overview.}
\label{fig:fbcsp-overview}
\end{figure}
    
    \subsection{Finding Spatial Filters with Common Spatial
Patterns}\label{finding-spatial-filters-with-common-spatial-patterns}
    
    \begin{keypointbox}
\item  Common spatial patterns amplifies variance differences between classes with spatial filters
\end{keypointbox}
    
    CSP is the current state-of-the-art method for
finding spatial filters to extract features for motor behavior decoding
from EEG signals \cite{blankertz_optimizing_2008}. CSP calculates
optimal weights for the sensors so that the variance of the weighted
signals is high for one class and low for the other class. Then the
classes can be separated by the variances of the weighted signals.

We will first explain common spatial patterns for a single filterband
and two classes and later explain how to extend it to multiple
filterbands and multiple classes. Common spatial patterns, in a
BCI context, seeks to answer this question: Given signals from several
sensors for two classes (e.g., right hand and left hand), how to weight
these sensors, so that the ratio
\(\frac{variance(\text{weighted signals class 1})}{variance(\text{weighted signals class 2})}\)
is maximal (or minimal). The weights are called spatial filters and the
weighted signals spatially filtered signals. Now you can learn to
separate class 1 from class 2:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Apply the spatial filters to all signals
\item
  Compute the variance for each spatially filtered signal for each trial
\item
  Train a classifier (e.g., linear discriminant analysis) on these
  variances.
\end{enumerate}
    
    \subsection{Example}\label{example}
    
    Let's look at a concrete example. We use 9-13 Hz bandpassed-filtered
data for the sensors Cz, CP1 and CP2. We look at right hand and left
hand trials. We expect that the left side (i.e., CP1) shows a variance
decrease for the right hand and the right side (i.e., CP2) shows a
variance decrease for the left hand.
You can indeed see that in the two trials shown in Figure \ref{fig:csp-before-csp-trials}. 

    
\begin{figure}[ht]
  \begin{center}
  \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Methods_Common_Spatial_Patterns_files/Methods_Common_Spatial_Patterns_14_0.pdf}
  
  \caption{Trials before spatial filtering. Parts in red have higher variance, parts in green have lower variance. This is more difficult to see for the left hand.}
  
  \label{fig:csp-before-csp-trials}
  \end{center}
\end{figure}
    
CSP amplifies these variance differences. 
For two CSP filters computed
on this dataset and shown in Figure \ref{fig:csp-example-filters}, you can clearly see that they are sensitive to the
different sides.  In the left plot, the red weight on CP2 has the largest
magnitude, in the right plot, the sensor CP1 has the largest magnitude.
While plotting the filters for this dataset is informative, you have to
be very careful when interpreting them as we will point out in the
visualization chapter \ref{chap:visualizations}.
    
    \begin{figure}[H]
        \begin{center}
        \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Methods_Common_Spatial_Patterns_files/Methods_Common_Spatial_Patterns_16_0.pdf}
        \end{center}
  \caption{CSP filters. 
  Positive weights are shown in red and negative weights
are shown in blue, with grey inbetween at zero. Stronger colors indicate
larger absolute values, e.g., a more saturated red indicates a larger
positive value.}
  
  \label{fig:csp-example-filters}
\end{figure}
    
    If we use these filters to weigh the sensors, we see in Figure \ref{fig:csp-example-filtered-signals},
    that the variance difference between the right and the left hand trials is larger for the
spatially filtered signals than for the raw sensors.
    
   \begin{figure}[ht]
        \begin{center}
        \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Methods_Common_Spatial_Patterns_files/Methods_Common_Spatial_Patterns_18_0.pdf}
        \end{center}
  \caption{Original signals and signals spatially filtered with the CSP filters.
  Parts in red have higher variance, parts in green have lower variance.}
  
  \label{fig:csp-example-filtered-signals}
\end{figure}
    
    \subsection{Mathematical Formulation}\label{mathematical-formulation}
    
    \begin{keypointbox}
\item  Common spatial patterns seeks to maximize ratio of variances 
\item  Generalized eigenvalue decomposition can be used to compute spatial filters 
\end{keypointbox}
    
    Mathematically, CSP aims to maximize the ratio of variances between two
classes. We assume we have multivariate signals \(X_1\) and \(X_2\) for
two classes 1 and 2. For EEG decoding, the multivariate signals would be
signals from several sensors and the classes could be left hand and
right hand. We want to find a spatial filter \(w\) that maximizes the
ratio of the variances:

\[w = argmax_w \frac{variance(w^{\top} X_1)}{variance(w^{\top} X_2)} = argmax_w \frac{||w^{\top} X_1||^2}{||w^{\top} X_2||^2} = 
argmax_w \frac{w^{\top} X_1 X_1^{\top}w}{w^{\top} X_2 X_2^{\top}w}\] (we
assume \(X_1\) and \(X_2\) are mean-subtracted)

Now we can solve for w using this generalized eigenvalue problem:

\[\Sigma_1w = \lambda \Sigma_2 w \]

This will not give us a single spatial filter w, but a matrix of spatial
filters W, the generalized eigenvectors. The variance ratios of class 1
to class 2 for the signals filtered with the (generalized) eigenvector
\(w_i\) will be equal to the corresponding eigenvalues of the
eigenvectors:

\[\frac{w_i^{\top} \Sigma_1 w_i}{w_i^{\top} \Sigma_2 w_i} = \lambda_i\]

The vectors with the highest eigenvalues have a high variance for class
1 compared to class 2 and the vectors with the lowest eigenvalues have a
low variance for class 1 compared to class 2. The spatial filter
maximizing this ratio is then the \(w_i\) with the largest eigenvalue.
Conversely, the spatial filter minimizing this ratio is the \(w_i\) with
the smallest eigenvalue.

In practice, we use the filters with the 5 largest and 5 smallest
eigenvalues.

See the appendix for further explanations and a computation example.
    
    \subsection{Feature Selection}\label{feature-selection}
    
    \begin{keypointbox}

\item  Feature selection is especially important, because spatial filters can be overfitted 
\item  We use a coarse preselection mostly to speed up computation time
\item  Additionally, we use a wrapper approach evaluating feature subsets with our classifier 

\end{keypointbox}
    
    \subsubsection{Motivation}\label{motivation}

We can get a large number of features and feature selection can be
beneficial, since the spatial filters can be overfitted.
The spatial filters can overfit since we use the class labels when we
compute them. Also, we could have a fairly large number of features if
we used all filters. For example, if we have 128 sensors and 35
filterbands, we would have 128 filters per filterband and
\(128 \cdot 35=4480\) features.

Many ways to reduce the number of features have been tried for motor
imagery decoding, e.g.~mutual information based methods
\cite{chin_multi-class_2009}, schemes selecting a subset of CSP filters
and reapplying CSP to them\\
\cite{meng_automated_2009}, etc. No clear consensus is known to us
regarding the best feature selection for CSP-based motor behavior
decoding, so we felt free to add our own variant.

We aimed to ensure several things in our feature selection:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  The estimates for the feature importances should be close to what we
  optimize, i.e., should reflect how much it contributes to the
  classification accuracy
\item
  Use the information we already have from CSP, i.e., the magnitude of
  the eigenvalues of the filters
\item
  Reduce computation time by ignoring features that are very unlikely to
  be important for classification
\end{enumerate}
    
    \subsubsection{Pre-Selection}\label{pre-selection}

We aim to achieve points 1 and 2 with the first steps of our feature
selection pipeline:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  For each filterband, only keep the spatial filters with the 5 largest
  and the 5 smallest eigenvalues, and only compute the features for them
\item
  Only keep the 20 filterbands with the largest mean accuracy over all
  classifier pairs and all train folds
\end{enumerate}

Pre-selecting filters by their eigenvalues is a standard procedure for
motor behavior decoding. The second step is mostly meant to save
computation time. Both steps together reduce the number of features to
\(10 \cdot 20 = 200\). Besides saving computation time, only keeping the
best 20 filterbands modestly improved accuracy in many of our tests.
    
    \subsubsection{Main Selection}\label{main-selection}

We then proceed with the main part of our feature selection. It
evaluates feature subsets by training and evaluating our classifier (see
next section) on the training fold with a 5-fold cross validation.
Selecting features by the accuracies of feature subsets is called 
a wrapper approach \cite{kohavi_wrappers_1997}. We hope directly using
the classifier in the feature selection leads to robust estimates of the
feature importances. Using a wrapper approach is also motivated by the
fact that the authors of the winning entry for a motor imagery dataset
of the most popular BCI competition mentioned they are mainly not
considering wrapper approaches due to their computational costs
\cite{ang_filter_2008}.

Now, the main problem is which feature subsets to consider. Since there
are \(2^{|\text{Features}|}=2^{200}\) of them, it would be
computationally intractable to consider all of them. We instead start
with an empty feature set and iteratively add and remove those features
from the set that most improve classification accuracy, until we have 80
features. Concretely, this is the addition/forward step and the
removal/backward step in our feature selection:

\vspace{0.2cm}
\textbf{Forward Step:} (Given current best feature set \(F\))

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  For each filterband \(i\):
  \begin{enumerate}
  \def\labelenumii{\arabic{enumii}.}
  \tightlist
\item
  Add the two filters \(f_{i,1}\),\(f_{i,2}\) with the largest and
  smallest eigenvalue that are not in F yet
\item
  Run 5-fold cross validation with these features added, i.e.~with
  \(F \cup \{f_{i,1},f_{i,2}\}\)
\item
  Add the two filters \(f_{best1}\),\(f_{best2}\) that led to the
  highest cross validation classification accuracy,
  \(F:=F\cup \{f_{best1},f_{best2}\}\)
  \end{enumerate}
\end{enumerate}

The backward step is the same, except that that in step 1.1, the filters
are removed:

\vspace{0.2cm}
\textbf{Backward Step:} (Given current best feature set \(F\))

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  For each filterband \(i\):
  \begin{enumerate}
  \def\labelenumii{\arabic{enumii}.}
  \tightlist
\item
  Remove the two filters \(f_{i,1}\),\(f_{i,2}\) with the largest and
  smallest eigenvalue that are in F
\item
  Run 5-fold cross validation with these features removed, i.e.~with
  \(F \setminus \{f_{i,1},f_{i,2}\}\)
\item
  Remove the two filters \(f_{worst1}\),\(f_{worst2}\) whose removal led
  to the highest cross validation classification accuracy,
  \(F:=F\setminus \{f_{worst1},f_{worst2}\}\)
  \end{enumerate}
\end{enumerate}

We combine these steps by making 4 forward steps and 2 backward steps
until we have 80 features. In our tests, this
combination worked best. Our approach is a variation of the plus l take
away r algorithm \cite{ferri_comparative_1994}
\cite{kudo_comparison_2000}, making special use of the eigenvalues of
the spatial filters. Note that since the filters and by implication the features are possibly
overfitted on the training data, our feature selection is not guaranteed
to remove features unhelpful on the test data.
    
    \subsection{Linear Discriminant
Analysis}\label{linear-discriminant-analysis}
    
    \begin{keypointbox}
\item  Linear discriminant analysis (LDA) tries to find a projection that separates the classes well
\item  LDA optimizes the ratio of class mean differences and within class variances 
\item  We use shrinkage to better estimate the covariances
\end{keypointbox}
    
    Once we have selected features, we need to classify them. We use linear
discriminant analysis (LDA) as our classifier
\cite{wikipedia_linear_2015}. LDA tries to find a weight vector \(w\)
for the features that maximizes the ratio:
\[\frac{\text{difference between class means}}{\text{variances within classes}} = \frac{mean(w^{\top} X_2) - mean(w^{\top} X_1)}{variance(w^{\top}X_2) + variance(w^{\top}X_1)}\]

where \(X_1\) and \(X_2\) are our features for classes 1 and 2. If you
then multiply the weight vector \(w\) with the features you should get
high values for class 2 and low values for class 1. An artificial
example is shown in Figure \ref{fig:lda-example}, where we randomly generated features
for two classes and compute the LDA vector. You can see that the LDA
vector nicely separates the classes: If you project the samples to this
vector, the red dots will be on the left upper part of the vector and
the violet dots on the lower right.
    
\begin{figure}[ht]
  \begin{center}
  \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Methods_Common_Spatial_Patterns_files/Methods_Common_Spatial_Patterns_30_0.pdf}
  \end{center}
  \caption{LDA example. Each dot represents one sample. 
The arrow is the LDA weight vector, the dotted line shows its extension in negative direction. 
  LDA  projects samples on the black solid/dotted line for classification.}
  \label{fig:lda-example}
\end{figure}
    
    You can compute the LDA vector w as:

\[ w = \Sigma^{-1} (mean(X_2) - mean(X_1))\]

where \(\Sigma\) is the covariance matrix of our features. You can then
multiply this weight vector with a sample, sum it and you get large
values for class 2 and small values for class 1. To have values above
zero for class 2 and below zero for class 1, you still need a bias b:

\[b = -w^{\top} mean(X)\]

Linear discriminant analysis assumes that the covariances are equal for
both classes, however it often works well even if this assumption is not
completely fulfilled \cite{li_using_2006} \cite{duda_pattern_2012}.
The main difficulty in the LDA approach is the estimation of the
covariance matrix. We use shrinkage regularization as described in
\cite{ledoit_well-conditioned_2004}.

LDA is widely used for classifications in BCI contexts and was therefore
also our choice here, although other choices such as support vector
machines or logistic regression could be equally sensible.
    
    \subsection{Multiple Filterbands}\label{multiple-filterbands}
    
    \begin{keypointbox}
\item  We compute spatial filters for multiple filterbands
\item  We use features from all filterbands together to train classifier 
\end{keypointbox}

We extend CSP to multiple filterbands by computing spatial filters for
multiple filterbands independently. We compute their corresponding
features (i.e., the logarithms of their variances) for all spatial
filters of all filterbands. Then we train a single classifier with all
of these features. Our filterbands might for example be 0-4Hz, 4-8 Hz,
\ldots{}, 48-52Hz. This approach is called filter bank common spatial
pattern \cite{ang_filter_2008}.
    
    \subsection{Multiple Classes}\label{multiple-classes}
    
    \begin{keypointbox}
\item  We compute all pairwise classifiers and use weighted voting 
\end{keypointbox}
    
    We extend our two-class filter bank CSP to multiple classes by training
all pairwise classifiers and using weighted voting.

In our 4-class case, we will train classifiers 1vs2, 1vs3, 1vs4, 2vs3,
2vs4 and 3vs4. When we want to classify a trial, we sum the classifier
outputs for each class from all classifiers and select the class with
the highest sum. The classifier outputs are not the labels, i.e., class
1 or class 2, but the LDA outputs that could range from negative
infinity (strongly predicting class 1) over zero (both classes equally
probable) to positive infinity (strongly predicting class 2). We have to
make sure that all our classifiers have outputs in a similar range.
Otherwise, our sum may not be meaningful, a classifier with an output
with a larger range would dominate the other classifier outputs in the
sum. Therefore, we scale weights w of our trained linear discriminant
analysis classifier, so that the classifier outputs for the means of
class 1 and 2 are -1 and 1.

\[w_{i\_scaled} = 2\frac{w_i}{(w_i^{\top}(mean(X_1) - mean(X_2))}\]

\[\text{($mean$ computes the trial-wise mean)}\]

The equation for \(w_{i\_scaled}\) forces the LDA outputs for the means
to have a difference of 2. Since we compute the bias afterwards so that
our overall mean is at 0, we will get -1 and 1 as outputs for the means
of class 1 and 2.

Training several binary classifiers is a well-established way to deal
with multi-class problems \cite{galar_overview_2011}, also used for
motor behavior decoding \cite{chin_multi-class_2009}. We preferred one vs
one classifiers to one vs rest classifiers, as at least one study found
them to be more robust \cite{meng_automated_2009}. However, one vs rest
might work equally well and could also be considered in the future.
    
    \subsection{Parameters}\label{parameters}
    
    As a basic version, we use 35 non-overlapping filterbands, with 2 Hz
width between 0 Hz and 30 Hz and 6 Hz width beween 30 Hz and 142 Hz. So
our filterbands are: 0.5-1, 1-3, \ldots{}, 29-31, 31-37, \ldots{},
139-145. We use all EEG sensors. Preprocessing details and variants are
described in the experiments chapter. We use a fixed 500 ms - 4000 ms
time interval of our 4000 ms trials, as other methods to automatically
determine the time interval (see for example in
\cite{blankertz_optimizing_2008}) did not improve accuracies in our
tests.
    
    