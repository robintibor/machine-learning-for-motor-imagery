



    
        
    \chapter{Visualizations}\label{chap:visualizations}
    
    \begin{keypointbox}

\item It can be difficult to show what a trained model has learned
\item Interpreting weights has to be done carefully
\item Weights can sometimes be transformed to more interpretable patterns
\item Visualizations can give clues what a model might have learned

\end{keypointbox}
    
    In this chapter, we will:

\begin{itemize}
\tightlist
\item
  motivate why we want to understand what our models have learned in section \ref{sec:visualize-motivation}.
\item
  explain one of the difficulties of understanding (discriminatively)
  trained models in section \ref{sec:example-for-difficulties}
\item
  show how this difficulty can be alleviated using the covariance of the
  features in section \ref{sec:using-the-covariance-to-compute-activation-patterns}
\item
  explain the additional difficulties of understanding convolutional
  networks and show what we can visualize from them in section \ref{sec:visualizing-networks-methods}
\end{itemize}

Building on this, we will visualize our networks in sections \ref{sec:filter-bank-net-visualizations}, 
\ref{sec:raw-net-visualizations} and \ref{sec:fft-net-visualizations}. We aim to
show indications that:

\begin{itemize}
\tightlist
\item
  all of them learn spatially localized event-related
  synchronization/desynchronization features
\item
  the raw net additionally learns to use the temporal dynamics of the
  ERD/ERS
\item
  the raw net additionally learns use a specific potential at the start
  of a trial
\end{itemize}
    
    \section{Motivation}\label{sec:visualize-motivation}
    
    Once you have trained a machine learning model, you are often interested
to know what it has learned. In our case, we are interested to know how
our model uses the signal:

\begin{itemize}
\tightlist
\item
  How is the model weighing the sensors?
\item
  What frequencies is it using for which class?
\item
  Or in the case of the raw net, is it using only oscillations or
  something else?
\end{itemize}

Also, we want to know what we can learn from the model about the brain
and motor behavior. For example, can the sensor weights show us where in
the brain the important signals are generated?
    
    \section{Example for Difficulties}\label{sec:example-for-difficulties}
    
    It is often not straightforward to interpret what a model has learned.
We will present a simple example where looking at the weights of the
model might lead to misinterpretations due to a distractor signal
affecting several sensors.

Consider a case with two sensors S1 and S2 with values \(x_1(t)\) and
\(x_2(t)\), where you want to classify each sample \(x_1(t)\),
\(x_2(t)\) as either class left or right. Assume, all the class-relevant
information is contained in S1: For class left, \(x_1\) will always be
positive and for class right, \(x_1\) will always be negative. Further
assume, \(x_2\) is always just zero. We show this in Figure \ref{fig:example-no-distract}.
    
\begin{figure}[ht]
        \begin{center}
        \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Visualizations_files/Visualizations_13_0.pdf}
        \end{center}
        \caption{Example without distractor.}
        \label{fig:example-no-distract}
\end{figure}
    
    This case can be completely unproblematic. A linear classifier could
assign weight 1 to sensor 1 and weight 0 to sensor 2 (and use a
threshold of zero to distinguish class right from left). The
interpretation would be easy: The information is contained in sensor 1.
Now assume, we add the same distractor signal to both sensors, which has
no correlation to the class at all. In our case, we will add the same
sine wave to both sensors as shown in Figure \ref{fig:example-distract}. 
In EEG motor decoding, this kind of ``noise''
could arise if we have an oscillation like the heartbeat or some
oscillation within in the brain that is not related to the movement.
    
\begin{figure}[ht]
        \begin{center}
        \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Visualizations_files/Visualizations_16_0.pdf}
        \end{center}
        \caption{Example with distractor.}
        \label{fig:example-distract}
\end{figure}
    
    Now, merely assigning a positive weight to sensor 1 and weight 0 to
sensor 2 could not separate the classes anymore (sensor 1 has both
positive and negative samples for the left class and also for the right
class). Instead, a linear classifier could still classify this perfectly
by assigning weight 1 to sensor 1 and weight -1 to sensor 2. This
removes the distractor and leads to a perfect prediction again, as shown in Figure \ref{fig:example-distractor-removed}.
    
\begin{figure}[H]
        \begin{center}
        \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Visualizations_files/Visualizations_18_0.pdf}
        \end{center}
        \caption{Example with distractor removed.}
        \label{fig:example-distractor-removed}
\end{figure}
    
    Just looking at the weights of the model can now lead to
misinterpretations. We know that sensor 1 contains the information and
is actually the same as the target, plus our distractor signal, whereas
sensor 2 only contains the distractor signal. However, the negative
weight could also imply that sensor 2 contains a class-discriminative
signal, namely that sensor 2 is typically negative for class right and
positive for class left.
    
    To sum up, the weights in a model trained only to discriminate between
classes typically reflect at least two things: the relationship between
the weighted features and the output and the covariances of the noise
among the features. For a longer discussion about interpreting weights
of machine learning models in the context of EEG, see
\cite{blankertz_optimizing_2008}, \cite{blankertz_single-trial_2011} and
\cite{haufe_interpretation_2014}.
    
    \section{Using the Covariance to Compute Activation
Patterns}\label{sec:using-the-covariance-to-compute-activation-patterns}
    
    \begin{keypointbox}

\item Covariance of the signals can be used to compute interpretable patterns

\end{keypointbox}

As we have shown, a linear discriminative model can have
misinterpretable weights. This is due to covariances between the
sensors. We can compute a more interpretable \emph{activation pattern}
from our weight vector. One computes the entry in the activation pattern
for a specific sensor by multiplying the weight of that sensor with its
variance and the weights of the other sensors with their covariance like
this: \[ a_i = \sum_{j} w_i \cdot cov(x_i,x_j) \]

We get our entire activation pattern in matrix notation like this:

\[ A = \Sigma_Xw\]

Intuitively, this makes the activation for a sensor reflect a
combination its own weight and the weights of sensors it has high
covariances with. It can be shown that this transforms a discriminative
model into a corresponding generative model
\cite{haufe_interpretation_2014}. While the discriminative model answers
the question how to reconstruct the source/class from given data, the
generative model answers the question we are more interested in: Given a
source/class, how does the data look like?

For our example, we have \(w_1= 1\), \(w_2=-1\), \(var(x_1) = 4.35\),
\(var(x_2) = 3.29\) and \(cov(x_1,x_2) = 3.29\). If we use these to
compute the activation pattern, we get \(a_1 = 1.05\) and \(a_2 = 0\),
correctly showing that the class is not encoded in sensor 2 at all.
    
    \section{Visualizing Networks
Methods}\label{sec:visualizing-networks-methods}
    
    \begin{keypointbox}

\item It is difficult to show what exactly a network has learned
\item You can try to visualize the network directly or show its reaction to different inputs

\end{keypointbox}
    
    Understanding what a convolutional neural network has learned can be
difficult. A network can have a large number of weights and additionally,
weights of one unit interact with weights of other units to produce the
net's output.

To show what the network has learned, one can either only use the
network itself (\emph{network-centric}) or use the network together with
input data (\emph{dataset-centric}, e.g., to highlight what parts of the
input the network focusses on) \cite{yosinski_understanding_2015}. For
our networks, we will show several things to get an idea how the network
classifies our data. First, we will show the weights of the network and
transform them to activation patterns. Second, we will reconstruct the
desired input of a unit using the gradient with regard to the input.
    
    We will use a running examples to illustrate the techniques: A raw net
trained on data from two sensors, C3 and C4 for the classes right hand
and left hand.
    
    \subsection{Weights}\label{weights}
    
    \begin{keypointbox}

\item Expected lateralization of the signal reflected in weights

\end{keypointbox}
    
    We will visualize weights despite the pitfalls mentioned before. We do
so, since we expect at least very broad phenomena like the lateralization
of the signal to not be completely obscured by the
covariance of distractor signals/noise.

For the raw net, we can combine the weights of the first two layers,
since there is no activation inbetween. For that, we have to compute the
dot product of the two weight tensors. This leads to ``combined
filters'' with the dimensions $|sensors| \cdot kernel\_time\_length$. Our
kernel time length is 30. You can see a lateralization when comparing combined filters 1 and 4 to
combined filters 2 and 3 in Figure \ref{fig:raw-net-example-combined-weights}. 
Also note that combined filter 1 is pretty much a vertically flipped version of combined filter 4.
    
\begin{figure}
\begin{subfigure}[]{.4\linewidth}
\centering
      \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Visualizations_files/Visualizations_29_0.pdf}
\caption{Combined filters learned by the raw net.
Filters highlighted in red show higher variance.}
\label{fig:raw-net-example-combined-weights}
\end{subfigure}
\begin{subfigure}[]{.45\linewidth}
\begin{subfigure}[]{\linewidth}
\centering
      \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Visualizations_files/Visualizations_31_0.pdf}
\end{subfigure}

\begin{subfigure}[]{\linewidth}
\centering
      \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Visualizations_files/Visualizations_31_1.pdf}
\end{subfigure}
\caption{Softmax weights of the raw net. Weights highlighted in red have larger values.}
\label{fig:raw-net-example-softmax-weights}
\end{subfigure}
\caption{Combined filters and softmax weights of the raw net.}
\label{fig:raw-net-example-weights}
\end{figure}

    
    Now we can look at the final weights of the softmax layer in Figure \ref{fig:raw-net-example-softmax-weights}. The softmax
layer has weights for each filter and each time-pooled output from the
layer before. You can see that the net seems more sensitive to the first
half of the trial. The weights are plausible. For example, filter 1,
which matches an oscillation in sensor C3, has larger weights for left
hand and smaller weights for right hand. This is consistent with a
\emph{decreasing} oscillation on the left-sided C3 sensor for the right
hand.
    
    
    \subsection{Patterns}\label{patterns}
    
    \begin{keypointbox}

\item Patterns retain lateralization, look more smooth

\end{keypointbox}

    We now also attempt to answer the question: How is the output of a
certain unit encoded in the input (either the raw input or the input of
the layer before)?

We cannot just multiply the covariance of the sensors with out weights
anymore, since we have two differences to our linear classifier example.
First, our weights also have a temporal dimension, they span 30 samples.
Second, the outputs of the different units of our layer are not
uncorrelated. We solve the first problem by adding new virtual channels
V, that are time-translated. For every sample number between 1 and 30 we
add new channels with the original input, translated forwards in time by
the respective number of samples. We solve the second problem by also
multiplying with the inverse of the layer outputs' covariance (see
\cite{haufe_interpretation_2014} for the derivation of this). The
computed patterns then answer the question we posed at the start,
i.e.~they estimate the pattern matrix in:

\[ input = Pattern \cdot outputs + noise\]
\[ x(n) = A o(n) + \epsilon(n)\]

We show these patterns in Figure \ref{fig:raw-net-example-combined-patterns}. 
We still see the lateralization, but we see
that some of the signal is always present in both sensors. The signal is
also more smooth, with the higher frequencies removed.We can transform our weights in the softmax layer in the same way as in
the example, since it is a linear classifier. The transformed patterns in Figure \ref{fig:raw-net-example-softmax-patterns}
are more smooth, while still showing a similar time course.
    
\begin{figure}
\begin{subfigure}[]{.4\linewidth}
\centering
      \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Visualizations_files/Visualizations_35_0.pdf}
\caption{Combined patterns of the raw net.
Patterns highlighted in red show higher variance.}
\label{fig:raw-net-example-combined-patterns}
\end{subfigure}
\begin{subfigure}[]{.45\linewidth}
\begin{subfigure}[]{\linewidth}
\centering
      \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Visualizations_files/Visualizations_39_0.pdf}
\end{subfigure}

\begin{subfigure}[]{\linewidth}
\centering
      \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Visualizations_files/Visualizations_39_1.pdf}
\end{subfigure}
\caption{Softmax patterns of the raw net. Patterns highlighted in red have larger values.}
\label{fig:raw-net-example-softmax-patterns}
\end{subfigure}
\caption{Combined patterns and softmax patterns of the raw net.}
\label{fig:raw-net-example-patterns}
\end{figure}   
   
    

    
    \subsection{Reconstructing Input with the
Gradient}\label{reconstructing-input-by-the-gradient}
    
    \begin{keypointbox}

\item To get a "desired" input of a unit, you can use the gradient of the activation of that unit on random input

\end{keypointbox}
    
    We want to try another, more generic way to visualize what kind of
feature a particular unit might encode. To do that, one can show what
kind of input would lead to a high activation \cite{simonyan_deep_2013}.

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Start with random input
\item
  Compute the gradient of the unit on that input
\item
  Weight gradient by a learning rate and add it to the input
\item
  Repeat steps 2-3 (until you reach a specified activation or after a
  certain number of repetitions)
\end{enumerate}

Now we again run into the problems of visualizing discriminatively
trained models. Since the covariance, including the noise covariance,
influences our model, the reconstructed input will also be affected by
it. The approach to solving this here is putting a prior on the
reconstructed input to ensure it is resembling a naturally occuring
input. We try to use just a simple prior that penalizes the squared sum
of the inputs. Only using this simple prior is motivated by the fact
that we only want to reconstruct some coarse structure of the input,
such as the lateralization and do not want to make many assumptions on
natural inputs. We only do the reconstruction for the last softmax layer
units, also penalizing if the wrong class has a high output.
So, our variation works like this:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Start with random input
\item
  Compute the gradient of the softmax cost for the desired class on that
  input
\item
  Decay the input by a factor (e.g 0.99)
\item
  Weight gradient by a learning rate and \textbf{subtract} it from the
  input
\item
  Repeat steps 2-4 for a predefined number of repetitions
\end{enumerate}

The decay in step 3 is mathematically equivalent to adding the squared
sum of the input to the costs, i.e.~rewarding inputs with a lower
squared sum.
    
    \subsubsection{Artificial Data}\label{artificial-data}
    
    \begin{keypointbox}

\item Artificial data roughly shows the expected reconstruction
\item Valid convolution affects reconstruction

\end{keypointbox}
    
    We will start with one artificial example dataset to emphasize some
precautions you have to make when interpreting the reconstruction. Then
we continue showing the reconstruction for our running example.

    We show one example for an artificial dataset with just one channel,
each trial has 600 samples:

\begin{itemize}
\tightlist
\item
  Class 1: flat zeroline + random gaussian noise
\item
  Class 2: sine of frequency 11 + random gaussian noise
\end{itemize}

Our net consists of:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Convolution + sumlog pooling
\item
  Softmax
\end{enumerate}
    
    We show the visualizations for class 0 and class 1 after 1000 epochs
with decay factor 0.99 and learning rate 0.1 in Figure \ref{fig:reconstruct-artificial}.
We can nicely see the recovered sine wave for class 2. Note the much
smaller overall variance for class 1. You have to be cautious when
interpreting the start and the end of the input. The smaller amplitude
at the start and end can be explained by the fact that we use
\emph{valid} convolutions: We convolve over the input without any
padding. The first sample and last sample therefore only contribute to
one convolution output, whereas a sample in the middle will contribute
\(\#kernel\_shape\) times to the convolutional output, in our case, 150
times.
    
\begin{figure}[H]
        \begin{center}
        \adjustimage{max size={0.9\linewidth}{0.2\paperheight}}{Visualizations_files/Visualizations_50_0.pdf}
        \end{center}
        \caption{Artificial gradient descent reconstruction.}
        \label{fig:reconstruct-artificial}
\end{figure}
    
    
    
    \subsubsection{Real Two-Class Data}\label{real-two-class-data}
    
    \begin{keypointbox}

\item For real right hand vs left hand data, the lateralization is visible

\end{keypointbox}
    
    We now show the same visualization for our running right vs left
example in Figure \ref{fig:reconstruct-real}.    
We see that the left hand shows a higher variance overall, especially
for sensor C3. This is consistent with a variance decrease for sensor C3
(on the left side) for the right hand. Note that it correctly only shows
an increase of variance after around 500 milliseconds, this is longer
than could be explained just by the valid convolution/border effect. We
also can see that the network seems to focus mostly on the first half of
the trial.
    
\begin{figure}[H]
        \begin{center}
        \adjustimage{max size={0.9\linewidth}{0.2\paperheight}}{Visualizations_files/Visualizations_56_0.pdf}
        \end{center}
        \caption{Real data gradient descent reconstruction.}
        \label{fig:reconstruct-real}
\end{figure}
    

    
    