
 \chapter{Introduction}\label{introduction}
    
    We try to use convolutional networks to improve the decoding part of
brain computer interfaces (BCIs). We will first explain what brain
computer interfaces are and how improving the decoding could make them
more reliable and affordable. We then go on to explain what features a
machine learning model has to use to decode movements and how a
convolutional network might learn them.
    
    \section{Brain Computer Interfaces and
Decoding}\label{brain-computer-interfaces-and-decoding}
    
    \begin{keypointbox}

\item Brain computer interfaces could help people with paralysis

\end{keypointbox}

Brain computer interfaces decode signals from the brain and can allow
persons to control something using their thoughts. A prominent
long-term-vision is for people with paralysis to control their own limbs
or robotic limbs. Ideally, this way they could be able to eat by
themselves or do other things that normally require control over your
body. Many people with paralysis show strong interest in being able to
do such activities by themselves \cite{anderson_targeting_2004}
\cite{zickler_bci_2009}
    
    \begin{keypointbox}

\item Making brain computer interfaces reliable and affordable is a big challenge

\end{keypointbox}

Lowering costs and improving reliability are two major challenges
towards practically usable brain-computer-interfaces. For example, in
the last years, BCIs have been developed that allowed people with
paralysis to control the movements of a robotic arm and drink by
themselves \cite{pruszynski_reading_2015}
\cite{collinger_high-performance_2013} \cite{hochberg_reach_2012}. These
systems, including the research on them, are quite expensive
\cite{jackson_neuroscience:_2012}. To allow a lot of people to profit
from possible future BCIs, it seems good to investigate cheaper BCIs. At
the same time, increasing the reliability of brain computer interfaces
matters a lot to make them usable in daily life - the system should
always do what the user intends so that it's safe and convenient to use.
    
    \begin{keypointbox}

\item We want to improve the decoding using convolutional neural networks

\end{keypointbox}

In this thesis, we try to improve the decoding part of an EEG brain
computer interface. Our BCI tries to detect which part of the body a
person is moving. We try to improve the decoding by using convolutional
neural networks, a machine learning approach which has made great
progress in recent years. Progress has been especially fast in fields,
where - like in BCI decoding - natural signals are classified, such as
object detection in images or speech recognition \cite{lecun_deep_2015}
\cite{schmidhuber_deep_2015}. Improving the decoding could make cheaper
BCIs - like EEG-based BCIs- more reliable and might also improve
reliability for other types of BCIs. Also, convolutional neural networks
could lead to new insights into what parts of the brain signal are
helpful for decoding the movement. They are less constrained in what
information they can use compared to the learning algorithms that are
currently state of the art for decoding EEG signals.
    
    \section{Signals for Motor Decoding}\label{signals-for-motor-decoding}
    
    \begin{keypointbox}

\item Doing or imagining movements causes fairly well-understood changes in the EEG signal

\end{keypointbox}

Imagining movements is a well-established way to control brain computer
interfaces. Whenever you imagine to move or actually move a part of your
body you can typically see a characteristic change in the EEG signal.
For certain sensors (typically over the motor cortex area), in some
frequency bands (for example in the mu-band between 9-13Hz), the
variance of the signal will decrease \cite{pfurtscheller_mu_2006}. The
sensor which shows the decreased variance is normally opposite to the
side of the body the person wants to move. So the decrease would often
be visible on a sensor on the left side for a movement of the right hand
and vice versa. As an example, we show two trials, one trial where a
person rested and another trial, where the same person moved the right
hand. In Figure \ref{fig:low-freq-example}, you see the signal recorded by the CP3-sensor 
which is on the left side of the head. Note the decrease of variance for the right hand trial.
    
\begin{figure}[H]
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Introduction_files/Introduction_8_0.pdf}
    \end{center}
    \caption{Two trials for the rest and the right hand class. Signals are bandpass-filtered to 9-13 Hz. }
    \label{fig:low-freq-example}
\end{figure}
    
    One of the main hypotheses for explaining the decrease of variance
states it is caused by a desynchronization of the firing rates of
neurons. Concretely, according to this hypothesis, the decrease is
caused by groups of neurons in a specific area of the motor cortex (see
Figure \ref{fig:head-schema} for a schematic view) desynchronizing their firing rate
\cite{steriade_basic_1990} \cite{pfurtscheller_event-related_1999}. When
you are not moving, the neurons in the motor cortex synchronize their
firing rates with each other. Once you plan a movement with a body part,
according to the desynchronization hypothesis, the neurons responsible
for that body part get new inputs to process and this stops them from
firing in the same rhythm.

\begin{figure}[ht]
  \begin{center}
  \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Introduction_files/4408441-fig-3-small.jpg}
  \end{center}
    \caption{Schematic view of the brain and the motor cortex. From \cite{blankertz_optimizing_2008}, © 2008 IEEE. }
    \label{fig:head-schema}
\end{figure}


At the same time, with some persons, you can also observe a higher
variance in the gamma frequency bands, from 30Hz up to 150Hz
\cite{ball_movement_2008}. This might indicate smaller groups of neurons
synchronizing their fast firing rates. This can be seen in Figure \ref{fig:high-freq-example} using the same trials as above. 
Note the increase of variance for the right hand trial.
    
\begin{figure}[ht]
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Introduction_files/Introduction_10_0.pdf}
    \end{center}
    
    \caption{\small{Two trials for the rest and the right hand class. Signals are bandpass-filtered to 72-78 Hz. 
    Parts highlighted in red have higher variance, in green lower variance.}}
    \label{fig:high-freq-example}
\end{figure}
    
    \subsection{Decoding Difficulties}\label{decoding-difficulties}
    
    \begin{keypointbox}

\item In reality signals are not clean enough to always easily classify which body part the person wanted to move

\end{keypointbox}

In principle, this could allow you to reliably detect which bodypart
somebody wants to move. In practice, there are several complications:

\begin{itemize}
\tightlist
\item
  the signal is ``smeared'' across several electrodes, especially if the
  the source is not very close to the sensors (volume conduction)
  \cite{blankertz_optimizing_2008}
\item
  other unrelated sources also affect the sensor up other brain activity
  at the same time
\item
  the sensors also pick up electrical noise from muscle movements from
  the subject
\item
  the sensors also pick up electrical noise from the environment
\item
  the sensor-skin contact can get worse during a recording, leading to a
  higher sensor-skin impedance and a lower signal quality
\item
  the desynchronization might not always happen at exactly the same
  location with exactly the same delay after movement intention
\end{itemize}
    
    \section{Machine Learning}\label{machine-learning}
    
    \begin{keypointbox}
\item  With machine learning, we try to automatically learn which brain signal patterns indicate which intention
\end{keypointbox}

Most brain computer interfaces try to overcome these complications,
at least in part, by machine learning. You tell the user which body part
to move and record the users' brain signals. Then you use this data to
automatically learn what brain signal patterns are typical for what
class of movement (for that user).

The machine learning motor decoding approaches typically consist of some
or all of these parts, whose order may vary:

\begin{itemize}
\tightlist
\item
  \textbf{Bandpass filters} - filtering the signal to one or several frequency
  bands, removing some non-task-related activities and noise
\item
  \textbf{Spatial filters} - creating new virtual channels using weights for each
  sensor, filters should try to reconstruct a class-discriminative
  source signal inside the brain
\item
  \textbf{Frequency domain transformation} - transforming the signal into power or
  power/phase representations
\item
  \textbf{Classification} - classifying the filtered transformed signal
\end{itemize}

The spatial filters and frequency filters may be learned from training
data or fixed before.
    
    \section{Convolutional neural
networks}\label{convolutional-neural-networks}
    
    \begin{keypointbox}
\item  Convolutional neural networks convolve filters smaller than the input over the input
\item  They can learn patterns at different scales/levels of abstraction
\end{keypointbox}

Convolutional neural networks are a type of machine learning algorithm.
Their basic idea is to learn useful representations at different levels
of abstraction. This can be well explained with the task of classifying
numbers in images. An image of a number on the lowest level, when you
zoom into it, consists of edges in different directions. At a slightly
higher level, you see combinations of these edges form shapes such
as half-circles and circles. Finally, at the highest level, you can
recognize the entire number. Therefore, it would be useful for a machine
learning algorithm to simultaneously learn these representations at
different levels - edges, circles, etc. - and learn how they are
combined to form the different numbers. This is what a convolutional
network typically does. At the first layer it shifts, i.e., convolves, a
small kernel over the entire image. This filter can, for example, learn
to detect edges in different directions. The output is then a feature
image, where each point tells you how strongly a certain edge is present
in that part of the image. The next layer again convolves a small kernel
over this feature image. Since each point in the feature image already
has information from a patch of the image, the filters in the second
layer cover a larger region of the image than the ones in the first
layer and can learn representations at the next higher level of
abstraction. Repeating this for several layers, you eventually cover
large enough regions of your image to have features useful for
classifying the image. In the common supervised case, the network learns
these filters by directly trying to use its own learned features to classify the image. 
This way, a convolutional network:

\begin{itemize}
\tightlist
\item
  learns features itself, alleviating the burden of feature engineering
\item
  can learn to fairly robustly detect an object at different locations
  within the image
\end{itemize}

Convolutional networks have been successfully used for a wide range of
tasks, including classifying images into 1000 different classes,
recognizing speech from audio, detecting cells indicative of breast
cancer, etc. \cite{ciresan_mitosis_2013} \cite{schmidhuber_deep_2015}
\cite{lecun_deep_2015}. To illustrate the usefulness of convolutional neural networks for
classifying brain signals, we use the same trials as before. First, we
show the raw signal and our filter in Figure \ref{fig:raw-and-filter}.
The kernel of the filter will be convolved over the input. This means, it will be flipped from right to left and
shifted over the input,
multiplying the input with the kernel at each location. In this example,
our kernel (which the network would have needed to learn) is a sine
signal with 11 Hz frequency.
    
\begin{figure}[ht]
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Introduction_files/Introduction_17_0.pdf}
    \end{center}
    \caption{Raw signal for two trials and a hand-engineered kernel (sine signal with 11 Hz), that we will convolve over the input.}
    \label{fig:raw-and-filter}
\end{figure}
    
    Now we show the output of the convolution in Figure \ref{fig:conv-output}: You again see the decrease in
the variance for the right hand, just as in the bandpass-filtered version before.
\begin{figure}[ht]
  \begin{center}
  \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{Introduction_files/Introduction_19_0.pdf}
  \end{center}
    \caption{Output of the convolution with our hand-engineered kernel.}
    \label{fig:conv-output}
\end{figure}    
    To differentiate between rest and right hand trial, we could now compute
the variance. Since our signal is roughly zero-mean, we can also just
square it and take the sum. Concretely, we take the sum of squares for a
certain number of neighbouring samples, in this case for 50 neighbours.
This will give us a time course of the squared sum (i.e., the uncentered
variance) over the trial. Pooling several samples together also has the
advantage that our results will not change much if the whole signal is
shifted by a few milliseconds forward or backward. After squared sum
pooling, we arrive at the output in Figure \ref{fig:pool-output}.
    
\begin{figure}[H]
  \begin{center}
  \adjustimage{max size={0.9\linewidth}{0.2\paperheight}}{Introduction_files/Introduction_21_0.pdf}
  \end{center}
    \caption{Output after pooling.}
    \label{fig:pool-output}
\end{figure}
    
    Now we could either already use the sum of these outputs as a feature to
distinguish rest from right hand (the rest trial would have a much
higher sum) or we could again convolve with another kernel, for example
a filter looking for a lasting decrease that would match the decrease
from 0 to 2000 milliseconds in the right hand trial. This approach is
shown in Figure \ref{fig:final-filter-and-output}.
\begin{figure}
\begin{subfigure}[t]{.2\linewidth}
\centering

\adjustimage{max size={1\linewidth}{0.15\paperheight}}{Introduction_files/Introduction_23_0.pdf}
\caption{Second kernel we convolve over our previous output. Shown in flipped form, the form it will be shifted over the input in.}
\label{fig:final-filter}
\end{subfigure}%
\begin{subfigure}[t]{.8\linewidth}
\centering
\adjustimage{max size={1\linewidth}{1\paperheight}}{Introduction_files/Introduction_23_1.pdf}
%\includegraphics[scale=0.6]{Introduction_files/Introduction_23_1.pdf}
\caption{Output after the second convolution.}
\label{fig:final-output}
\end{subfigure}
\caption{Second kernel and final convolution output.}
\label{fig:final-filter-and-output}
\end{figure}
These outputs for rest and right hand could now be distinguished by a
linear classifier, for example a softmax classifier.

In practical convolutional networks, we would typically have much more
than one filter per layer/step to be able to capture different types of
patterns (such as different frequencies in the first step). Furthermore,
the convolutional filters can not only convolve over time, but also over
sensors, or in case of Fourier-transformed signals, over frequencies.
    
    